<?php

use App\Http\Controllers\Account\SettingsController;
use App\Http\Controllers\ActivityLogController;
use App\Http\Controllers\Approval\ApprovalController;
use App\Http\Controllers\Approval\StatusController;
use App\Http\Controllers\Auth\SocialiteLoginController;
use App\Http\Controllers\CityProvince\CityProvinceController;
use App\Http\Controllers\Documentation\LayoutBuilderController;
use App\Http\Controllers\Documentation\ReferencesController;
use App\Http\Controllers\ElectronicReport\FinishedReprotController;
use App\Http\Controllers\ElectronicReport\PenugasanReportController;
use App\Http\Controllers\EngineeringDatabase\Port\PortDrawingController;
use App\Http\Controllers\EngineeringDatabase\Port\TechnicalQualityStandardController;
use App\Http\Controllers\EngineeringDatabase\Port\FacilityStandardController;
use App\Http\Controllers\EngineeringDatabase\Port\PortSpecificationController;
use App\Http\Controllers\EngineeringDatabase\Port\DocumentOfQualityController;
use App\Http\Controllers\EngineeringDatabase\Fleet\EquipmentBoardController;
use App\Http\Controllers\Logs\AuditLogsController;
use App\Http\Controllers\Logs\SystemLogsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Port\RegionController;
use App\Http\Controllers\Port\BranchController;
use App\Http\Controllers\Port\BranchSubController;
use App\Http\Controllers\Port\PortController;
use App\Http\Controllers\Port\DockController;
use App\Http\Controllers\Port\Form\PortFormController;
use App\Http\Controllers\Port\Form\PortFormGroupController;
use App\Http\Controllers\Port\Form\PortFormGroupItemController;

use App\Http\Controllers\Fleet\RegionController as FleetRegion;
use App\Http\Controllers\Fleet\BranchController as FleetBranch;
use App\Http\Controllers\Fleet\FleetController;
use App\Http\Controllers\Fleet\RouteController;
use App\Http\Controllers\Fleet\SubBranchController;
use App\Http\Controllers\Fleet\TypeOfTrackController;
use App\Http\Controllers\Fleet\TrackController;
use App\Http\Controllers\Fleet\Form\FleetFormController;
use App\Http\Controllers\Fleet\Form\FleetFormGroupController;
use App\Http\Controllers\Fleet\Form\FleetFormGroupItemController;


use App\Http\Controllers\ElectronicReport\ProgressReportController;
use App\Http\Controllers\EngineeringDatabase\Fleet\EngineeringDocumentController;
use App\Http\Controllers\EngineeringDatabase\Fleet\FleetFacilityStandardController;
use App\Http\Controllers\EngineeringDatabase\Fleet\FleetTechnicalStandardController;
use App\Http\Controllers\EngineeringDatabase\Fleet\ManualBookController;
use App\Http\Controllers\EngineeringDatabase\Fleet\ShipDocumentController;
use App\Http\Controllers\ERCM\Fleet\FleetERCMController;
use App\Http\Controllers\ERCM\Port\PortERCMController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProjectMonitoring\Fleet\BasicDataController;

use App\Http\Controllers\ProjectMonitoring\Fleet\DocumentController as FleetDocumentController;
use App\Http\Controllers\ProjectMonitoring\Fleet\ProjectController as FleetProjectController;
use App\Http\Controllers\ProjectMonitoring\Fleet\ProgressController as FleetProgressController;
use App\Http\Controllers\ProjectMonitoring\Port\BasicDataController as PortBasicDataController;
use App\Http\Controllers\ProjectMonitoring\Fleet\RecomendationController as FleetRecomendationController;
use App\Http\Controllers\ProjectMonitoring\Fleet\SummaryController as FleetSummaryController;
use App\Http\Controllers\ProjectMonitoring\Fleet\QualityController as FleetQualityController;
use App\Http\Controllers\ProjectMonitoring\Fleet\CompletionController as FleetCompletionController;

use App\Http\Controllers\ProjectMonitoring\Port\DocumentController as PortDocumentController;
use App\Http\Controllers\ProjectMonitoring\Port\ProjectController as PortProjectController;
use App\Http\Controllers\ProjectMonitoring\Port\ProgressController as PortProgressController;
use App\Http\Controllers\ProjectMonitoring\Port\RecomendationController as PortRecomendationController;
use App\Http\Controllers\ProjectMonitoring\Port\SummaryController as PortSummaryController;
use App\Http\Controllers\ProjectMonitoring\Port\CompletionController as PortCompletionController;
use App\Http\Controllers\ProjectMonitoring\Port\QualityController as PortQualityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    dd("hei");
    return redirect('index');
});

$menu = theme()->getMenu();
array_walk($menu, function ($val) {
    if (isset($val['path'])) {
        $route = Route::get($val['path'], [PagesController::class, 'index']);

        // Exclude documentation from auth middleware
        if (!Str::contains($val['path'], 'documentation')) {
            $route->middleware('auth');
        }

        // Custom page demo for 500 server error
        if (Str::contains($val['path'], 'error-500')) {
            Route::get($val['path'], function () {
                abort(500, 'Something went wrong! Please try again later.');
            });
        }
    }
});

// Documentations pages
Route::prefix('documentation')->group(function () {
    Route::get('getting-started/references', [ReferencesController::class, 'index']);
    Route::get('getting-started/changelog', [PagesController::class, 'index']);
    Route::resource('layout-builder', LayoutBuilderController::class)->only(['store']);
});

Route::middleware('auth')->group(function () {
    // Account pages
    Route::prefix('account')->group(function () {
        Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
        Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
        Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
        Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
    });

    // Logs pages
    Route::prefix('log')->name('log.')->group(function () {
        Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
        Route::resource('audit', AuditLogsController::class)->only(['index', 'destroy']);
    });

    Route::resource('/users', UserController::class)->except(['create', 'edit', 'update']);
    Route::post('users/{user}/update', [UserController::class, 'update'])->name('users.update');
    Route::post('users/{user}/update-status', [UserController::class, 'updateStatus'])->name('users.updateStatus');
    Route::resource('/roles', RoleController::class)->except(['create', 'edit', 'update']);
    Route::post('roles/{role}/update', [RoleController::class, 'update'])->name('roles.update');
    Route::resource('/permissions', PermissionController::class)->except(['create', 'edit', 'update']);
    Route::post('permissions/{permission}/update', [PermissionController::class, 'update'])->name('permissions.update');

    //Notification
    Route::resource('/notifications', NotificationController::class)
        ->only(['index', 'update']);

    // Port
    Route::prefix('port')->name('port.')->group(function () {
        // Region
        Route::get('region', [RegionController::class, 'index'])->name('region.index');
        Route::post('region/store', [RegionController::class, 'store'])->name('region.store');
        Route::get('region/{region}', [RegionController::class, 'show'])->name('region.show');
        Route::post('region/{region}/update', [RegionController::class, 'update'])->name('region.update');
        Route::delete('region/{region}/delete', [RegionController::class, 'destroy'])->name('region.destroy');

        // Branch
        Route::get('branch', [BranchController::class, 'index'])->name('branch.index');
        Route::post('branch/store', [BranchController::class, 'store'])->name('branch.store');
        Route::get('branch/{branch}', [BranchController::class, 'show'])->name('branch.show');
        Route::post('branch/{branch}/update', [BranchController::class, 'update'])->name('branch.update');
        Route::delete('branch/{branch}/delete', [BranchController::class, 'destroy'])->name('branch.destroy');

        // Sub Branch
        Route::get('sub_branch', [BranchSubController::class, 'index'])->name('sub_branch.index');
        Route::post('sub_branch/store', [BranchSubController::class, 'store'])->name('sub_branch.store');
        Route::get('sub_branch/{branch_sub}', [BranchSubController::class, 'show'])->name('sub_branch.show');
        Route::post('sub_branch/{branch_sub}/update', [BranchSubController::class, 'update'])->name('sub_branch.update');
        Route::delete('sub_branch/{branch_sub}/delete', [BranchSubController::class, 'destroy'])->name('sub_branch.destroy');
        Route::get('ajax_get_city', [BranchSubController::class, 'ajax_get_city'])->name('ajax_get_city');

        // Port
        Route::get('port', [PortController::class, 'index'])->name('port.index');
        Route::post('port/store', [PortController::class, 'store'])->name('port.store');
        Route::get('port/{port}', [PortController::class, 'show'])->name('port.show');
        Route::post('port/{port}/update', [PortController::class, 'update'])->name('port.update');
        Route::delete('port/{port}/delete', [PortController::class, 'destroy'])->name('port.destroy');
        Route::get('ajax_get_branch', [PortController::class, 'ajax_get_branch'])->name('ajax_get_branch');

        // Dock
        Route::get('dock', [DockController::class, 'index'])->name('dock.index');
        Route::post('dock/store', [DockController::class, 'store'])->name('dock.store');
        Route::get('dock/{dock}', [DockController::class, 'show'])->name('dock.show');
        Route::post('dock/{dock}/update', [DockController::class, 'update'])->name('dock.update');
        Route::delete('dock/{dock}/delete', [DockController::class, 'destroy'])->name('dock.destroy');

        Route::prefix('{port}/form')->name('form.')->group(function () {
            Route::get('/', [PortFormController::class, 'index'])->name('index');
            Route::post('/', [PortFormController::class, 'store'])->name('store');
            Route::delete('/', [PortFormController::class, 'destroy'])->name('delete');
            Route::get('detail', [PortFormController::class, 'show'])->name('show');
            Route::post('update', [PortFormController::class, 'update'])->name('update');

            Route::prefix('sub-group')->name('sub-group.')->group(function () {
                Route::get('/', [PortFormController::class, 'indexSubGroup'])->name('index');
                Route::post('/', [PortFormController::class, 'storeSubGroup'])->name('store');
                Route::get('ungroup', [PortFormController::class, 'ungroupSubGroup'])->name('ungroup');
                Route::get('detail', [PortFormController::class, 'showSubGroup'])->name('show');
                Route::post('update', [PortFormController::class, 'updateSubGroup'])->name('update');
                Route::get('{sub}/detail-parameter', [PortFormController::class, 'showSubGroupParameter'])->name('show-param');
                Route::post('{sub}/update-parameter', [PortFormController::class, 'updateSubGroupParameter'])->name('update-param');
                Route::post('{sub}/copy-parameter', [PortFormController::class, 'copySubGroupParameter'])->name('copy-param');
            });

            Route::prefix('{form}/group')->name('group.')->group(function () {
                Route::get('/', [PortFormGroupController::class, 'index'])->name('index');
                Route::post('/', [PortFormGroupController::class, 'store'])->name('store');
                Route::delete('/', [PortFormGroupController::class, 'destroy'])->name('delete');
                Route::get('detail', [PortFormGroupController::class, 'show'])->name('show');
                Route::post('update', [PortFormGroupController::class, 'update'])->name('update');
                Route::post('update', [PortFormGroupController::class, 'update'])->name('update');

                Route::prefix('{group}/item')->name('item.')->group(function () {
                    Route::get('/', [PortFormGroupItemController::class, 'index'])->name('index');
                    Route::get('landing', [PortFormGroupItemController::class, 'landing'])->name('landing');
                    Route::get('confirmation', [PortFormGroupItemController::class, 'confirmation'])->name('confirmation');
                    Route::post('upload', [PortFormGroupItemController::class, 'upload'])->name('upload');
                    Route::post('grouped', [PortFormGroupItemController::class, 'grouped'])->name('grouped');
                    Route::get('grouped-detail', [PortFormGroupItemController::class, 'groupedDetail'])->name('grouped-detail');
                    Route::delete('grouped', [PortFormGroupItemController::class, 'groupedDestroy'])->name('grouped-delete');

                    Route::get('{sub}/sub-parameter', [PortFormGroupItemController::class, 'showSubGroupParameter'])->name('sub-param');
                    Route::post('{sub}/update-sub-parameter', [PortFormGroupItemController::class, 'updateSubGroupParameter'])->name('update-sub-param');

                    Route::get('{component}/component-parameter', [PortFormGroupItemController::class, 'showComponentParameter'])->name('component-param');
                    Route::get('{component}/preview-parameter', [PortFormGroupItemController::class, 'previewComponentParameter'])->name('preview-component-param');
                    Route::post('{component}/update-component-parameter', [PortFormGroupItemController::class, 'updateComponentParameter'])->name('update-component-param');

                    Route::prefix('selected')->name('selected.')->group(function () {
                        Route::get('/', [PortFormGroupItemController::class, 'selected'])->name('index');
                        Route::get('detail', [PortFormGroupItemController::class, 'componentSelected'])->name('component');
                    });
                });
            });
        });
    });

    //Fleet
    Route::prefix('fleet')->name('fleet.')->group(function () {
        // Region
        Route::get('region', [FleetRegion::class, 'index'])->name('region.index');
        Route::post('region/store', [FleetRegion::class, 'store'])->name('region.store');
        Route::get('region/{region}', [FleetRegion::class, 'show'])->name('region.show');
        Route::post('region/{region}/update', [FleetRegion::class, 'update'])->name('region.update');
        Route::delete('region/{region}/delete', [FleetRegion::class, 'destroy'])->name('region.destroy');

        // Branch
        Route::get('branch', [FleetBranch::class, 'index'])->name('branch.index');
        Route::post('branch/store', [FleetBranch::class, 'store'])->name('branch.store');
        Route::get('branch/{branch}', [FleetBranch::class, 'show'])->name('branch.show');
        Route::post('branch/{branch}/update', [FleetBranch::class, 'update'])->name('branch.update');
        Route::delete('branch/{branch}/delete', [FleetBranch::class, 'destroy'])->name('branch.destroy');

        // Sub Branch
        Route::get('sub_branch', [SubBranchController::class, 'index'])->name('sub_branch.index');
        Route::post('sub_branch/store', [SubBranchController::class, 'store'])->name('sub_branch.store');
        Route::get('sub_branch/{branch_sub}', [SubBranchController::class, 'show'])->name('sub_branch.show');
        Route::post('sub_branch/{branch_sub}/update', [SubBranchController::class, 'update'])->name('sub_branch.update');
        Route::delete('sub_branch/{branch_sub}/delete', [SubBranchController::class, 'destroy'])->name('sub_branch.destroy');

        // Route
        Route::get('route', [RouteController::class, 'index'])->name('route.index');
        Route::post('route/store', [RouteController::class, 'store'])->name('route.store');
        Route::get('route/{route}', [RouteController::class, 'show'])->name('route.show');
        Route::post('route/{route}/update', [RouteController::class, 'update'])->name('route.update');
        Route::delete('route/{route}/delete', [RouteController::class, 'destroy'])->name('route.destroy');

        // Type Of Track
        Route::get('typeoftrack', [TypeOfTrackController::class, 'index'])->name('typeoftrack.index');
        Route::post('typeoftrack/store', [TypeOfTrackController::class, 'store'])->name('typeoftrack.store');
        Route::get('typeoftrack/{typeoftrack}', [TypeOfTrackController::class, 'show'])->name('typeoftrack.show');
        Route::post('typeoftrack/{typeoftrack}/update', [TypeOfTrackController::class, 'update'])->name('typeoftrack.update');
        Route::delete('typeoftrack/{typeoftrack}/delete', [TypeOfTrackController::class, 'destroy'])->name('typeoftrack.destroy');

        // Track
        Route::get('track', [TrackController::class, 'index'])->name('track.index');
        Route::post('track/store', [TrackController::class, 'store'])->name('track.store');
        Route::get('track/{track}', [TrackController::class, 'show'])->name('track.show');
        Route::post('track/{track}/update', [TrackController::class, 'update'])->name('track.update');
        Route::delete('track/{track}/delete', [TrackController::class, 'destroy'])->name('track.destroy');

        // Fleet
        Route::get('fleet', [FleetController::class, 'index'])->name('fleet.index');
        Route::post('fleet/store', [FleetController::class, 'store'])->name('fleet.store');
        Route::get('fleet/{fleet}', [FleetController::class, 'show'])->name('fleet.show');
        Route::post('fleet/{fleet}/update', [FleetController::class, 'update'])->name('fleet.update');
        Route::delete('fleet/{fleet}/delete', [FleetController::class, 'destroy'])->name('fleet.destroy');

        Route::prefix('{fleet}/form')->name('form.')->group(function () {
            Route::get('/', [FleetFormController::class, 'index'])->name('index');
            Route::post('/', [FleetFormController::class, 'store'])->name('store');
            Route::delete('/', [FleetFormController::class, 'destroy'])->name('delete');
            Route::get('detail', [FleetFormController::class, 'show'])->name('show');
            Route::post('update', [FleetFormController::class, 'update'])->name('update');

            Route::prefix('sub-group')->name('sub-group.')->group(function () {
                Route::get('/', [FleetFormController::class, 'indexSubGroup'])->name('index');
                Route::post('/', [FleetFormController::class, 'storeSubGroup'])->name('store');
                Route::get('detail', [FleetFormController::class, 'showSubGroup'])->name('show');
                Route::get('ungroup', [FleetFormController::class, 'ungroupSubGroup'])->name('ungroup');
                Route::post('update', [FleetFormController::class, 'updateSubGroup'])->name('update');
                Route::get('{sub}/detail-parameter', [FleetFormController::class, 'showSubGroupParameter'])->name('show-param');
                Route::post('{sub}/update-parameter', [FleetFormController::class, 'updateSubGroupParameter'])->name('update-param');
                Route::post('{sub}/copy-parameter', [FleetFormController::class, 'copySubGroupParameter'])->name('copy-param');
            });

            Route::prefix('{form}/group')->name('group.')->group(function () {
                Route::get('/', [FleetFormGroupController::class, 'index'])->name('index');
                Route::post('/', [FleetFormGroupController::class, 'store'])->name('store');
                Route::delete('/', [FleetFormGroupController::class, 'destroy'])->name('delete');
                Route::get('detail', [FleetFormGroupController::class, 'show'])->name('show');
                Route::post('update', [FleetFormGroupController::class, 'update'])->name('update');
                Route::post('update', [FleetFormGroupController::class, 'update'])->name('update');

                Route::prefix('{group}/item')->name('item.')->group(function () {
                    Route::get('/', [FleetFormGroupItemController::class, 'index'])->name('index');
                    Route::get('landing', [FleetFormGroupItemController::class, 'landing'])->name('landing');
                    Route::get('confirmation', [FleetFormGroupItemController::class, 'confirmation'])->name('confirmation');
                    Route::post('upload', [FleetFormGroupItemController::class, 'upload'])->name('upload');
                    Route::post('grouped', [FleetFormGroupItemController::class, 'grouped'])->name('grouped');
                    Route::get('grouped-detail', [FleetFormGroupItemController::class, 'groupedDetail'])->name('grouped-detail');
                    Route::delete('grouped', [FleetFormGroupItemController::class, 'groupedDestroy'])->name('grouped-delete');

                    Route::get('{sub}/sub-parameter', [FleetFormGroupItemController::class, 'showSubGroupParameter'])->name('sub-param');
                    Route::post('{sub}/update-sub-parameter', [FleetFormGroupItemController::class, 'updateSubGroupParameter'])->name('update-sub-param');

                    Route::get('{component}/component-parameter', [FleetFormGroupItemController::class, 'showComponentParameter'])->name('component-param');
                    Route::get('{component}/preview-parameter', [FleetFormGroupItemController::class, 'previewComponentParameter'])->name('preview-component-param');
                    Route::post('{component}/update-component-parameter', [FleetFormGroupItemController::class, 'updateComponentParameter'])->name('update-component-param');

                    Route::prefix('selected')->name('selected.')->group(function () {
                        Route::get('/', [FleetFormGroupItemController::class, 'selected'])->name('index');
                        Route::get('detail', [FleetFormGroupItemController::class, 'componentSelected'])->name('component');
                    });
                });
            });
        });
    });

    //Master Province & City
    Route::prefix('region')->name('region.')->group(function () {
        Route::get('province', [CityProvinceController::class, 'get_province'])->name('province');
        Route::get('city', [CityProvinceController::class, 'get_cities'])->name('city');
    });

    // Approval
    Route::prefix('approval')->name('approval.')->group(function () {
        Route::prefix('status')->name('status.')->group(function () {
            Route::get('/', [StatusController::class, 'index'])->name('index');
            Route::get('detail/{approval_id}', [StatusController::class, 'detail'])->name('detail');
            Route::post('approve/{approval}', [StatusController::class, 'approve'])->name('approve');
            Route::post('reject/{approval}', [StatusController::class, 'reject'])->name('reject');
            Route::delete('delete/{approval}', [StatusController::class, 'delete'])->name('delete');
        });

        Route::prefix('persetujuan')->name('persetujuan.')->group(function () {
            Route::get('/', [ApprovalController::class, 'index'])->name('index');
            Route::get('detail/{approval_id}', [ApprovalController::class, 'detail'])->name('detail');
            Route::get('keterangan/{approval}', [ApprovalController::class, 'keterangan'])->name('keterangan');
            Route::get('detail/{approval_id}', [ApprovalController::class, 'detail'])->name('detail');
            Route::get('document/list', [ApprovalController::class, 'datatablesDocument'])->name('document-list');
            Route::get('document/download', [ApprovalController::class, 'downloadDocument'])->name('document-download');
        });
    });

    //Engineering Database
    Route::prefix('ed')->name('ed.')->group(function () {
        //Fleet

        //Equipment Board
        Route::resource('{vessel_id}/equipment-board', EquipmentBoardController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['equipment-board' => 'equipmentBoard']);
        Route::get('equipment-board', [EquipmentBoardController::class, 'indexFleet'])
            ->name('equipment-board.indexFleet');
        Route::get('equipment-board/detail/{vessel_id}', [EquipmentBoardController::class, 'detailFleet'])
            ->name('equipment-board.detailFleet');
        Route::get('equipment-board/print/{vessel_id}', [EquipmentBoardController::class, 'print'])
            ->name('equipment-board.print');
        Route::post('{vessel_id}/equipment-board/{equipmentBoard}/update', [EquipmentBoardController::class, 'update'])
            ->name('equipment-board.update');
        Route::post('{vessel_id}/equipment-board/send', [EquipmentBoardController::class, 'sendApproval'])
            ->name('equipment-board.sendApproval');
        Route::get('equipment-board/document/{equipmentBoard}', [EquipmentBoardController::class, 'getDocument'])
            ->name('equipment-board.getDocument');
        Route::get('equipment-board/document/{document_id}/view', [EquipmentBoardController::class, 'viewDocument'])
            ->name('equipment-board.viewDocument');

        //Engineering Document
        Route::resource('{vessel_id}/engineering-document', EngineeringDocumentController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['engineering-document' => 'engineeringDocument']);
        Route::get('engineering-document', [EngineeringDocumentController::class, 'indexFleet'])
            ->name('engineering-document.indexFleet');
        Route::get('engineering-document/detail/{vessel_id}', [EngineeringDocumentController::class, 'detailFleet'])
            ->name('engineering-document.detailFleet');
        Route::get('engineering-document/print/{vessel_id}', [EngineeringDocumentController::class, 'print'])
            ->name('engineering-document.print');
        Route::post('{vessel_id}/engineering-document/{engineeringDocument}/update', [EngineeringDocumentController::class, 'update'])
            ->name('engineering-document.update');
        Route::post('{vessel_id}/engineering-document/send', [EngineeringDocumentController::class, 'sendApproval'])
            ->name('engineering-document.sendApproval');
        Route::get('engineering-document/document/{engineeringDocument}', [EngineeringDocumentController::class, 'getDocument'])
            ->name('engineering-document.getDocument');
        Route::get('engineering-document/document/{document_id}/view', [EngineeringDocumentController::class, 'viewDocument'])
            ->name('engineering-document.viewDocument');

        //Manual Book
        Route::resource('{vessel_id}/manual-book', ManualBookController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['manual-book' => 'manualBook']);
        Route::get('manual-book', [ManualBookController::class, 'indexFleet'])
            ->name('manual-book.indexFleet');
        Route::get('manual-book/detail/{vessel_id}', [ManualBookController::class, 'detailFleet'])
            ->name('manual-book.detailFleet');
        Route::get('manual-book/print/{vessel_id}', [ManualBookController::class, 'print'])
            ->name('manual-book.print');
        Route::post('{vessel_id}/manual-book/{manualBook}/update', [ManualBookController::class, 'update'])
            ->name('manual-book.update');
        Route::post('{vessel_id}/manual-book/send', [ManualBookController::class, 'sendApproval'])
            ->name('manual-book.sendApproval');
        Route::get('manual-book/document/{manualBook}', [ManualBookController::class, 'getDocument'])
            ->name('manual-book.getDocument');
        Route::get('manual-book/document/{document_id}/view', [ManualBookController::class, 'viewDocument'])
            ->name('manual-book.viewDocument');

        //Ship Document
        Route::resource('{vessel_id}/ship-document', ShipDocumentController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['ship-document' => 'shipDocument']);
        Route::get('ship-document', [ShipDocumentController::class, 'indexFleet'])
            ->name('ship-document.indexFleet');
        Route::get('ship-document/detail/{vessel_id}', [ShipDocumentController::class, 'detailFleet'])
            ->name('ship-document.detailFleet');
        Route::get('ship-document/print/{vessel_id}', [ShipDocumentController::class, 'print'])
            ->name('ship-document.print');
        Route::post('{vessel_id}/ship-document/{shipDocument}/update', [ShipDocumentController::class, 'update'])
            ->name('ship-document.update');
        Route::post('{vessel_id}/ship-document/send', [ShipDocumentController::class, 'sendApproval'])
            ->name('ship-document.sendApproval');
        Route::get('ship-document/document/{shipDocument}', [ShipDocumentController::class, 'getDocument'])
            ->name('ship-document.getDocument');
        Route::get('ship-document/document/{document_id}/view', [ShipDocumentController::class, 'viewDocument'])
            ->name('ship-document.viewDocument');

        //Fleet Technical Standard
        Route::get('fleet-technical-standard/detail', [FleetTechnicalStandardController::class, 'detail'])
            ->name('fleet-technical-standard.detail');
        Route::get('fleet-technical-standard/print', [FleetTechnicalStandardController::class, 'print'])
            ->name('fleet-technical-standard.print');
        Route::resource('fleet-technical-standard', FleetTechnicalStandardController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['fleet-technical-standard' => 'fleetTechnicalStandard']);
        Route::post('fleet-technical-standard/{fleetTechnicalStandard}/update', [FleetTechnicalStandardController::class, 'update'])
            ->name('fleet-technical-standard.update');
        Route::post('fleet-technical-standard/send', [FleetTechnicalStandardController::class, 'sendApproval'])
            ->name('fleet-technical-standard.sendApproval');
        Route::get('fleet-technical-standard/document/{fleetTechnicalStandard}', [FleetTechnicalStandardController::class, 'getDocument'])
            ->name('fleet-technical-standard.getDocument');
        Route::get('fleet-technical-standard/document/{document_id}/view', [FleetTechnicalStandardController::class, 'viewDocument'])
            ->name('fleet-technical-standard.viewDocument');

        //Fleet Facility Standard
        Route::get('fleet-facility-standard/detail', [FleetFacilityStandardController::class, 'detail'])
            ->name('fleet-facility-standard.detail');
        Route::get('fleet-facility-standard/print', [FleetFacilityStandardController::class, 'print'])
            ->name('fleet-facility-standard.print');
        Route::resource('fleet-facility-standard', FleetFacilityStandardController::class)
            ->except(['create', 'edit', 'update'])
            ->parameters(['fleet-facility-standard' => 'fleetFacilityStandard']);
        Route::post('fleet-facility-standard/{fleetFacilityStandard}/update', [FleetFacilityStandardController::class, 'update'])
            ->name('fleet-facility-standard.update');
        Route::post('fleet-facility-standard/send', [FleetFacilityStandardController::class, 'sendApproval'])
            ->name('fleet-facility-standard.sendApproval');
        Route::get('fleet-facility-standard/document/{fleetFacilityStandard}', [FleetFacilityStandardController::class, 'getDocument'])
            ->name('fleet-facility-standard.getDocument');
        Route::get('fleet-facility-standard/document/{document_id}/view', [FleetFacilityStandardController::class, 'viewDocument'])
            ->name('fleet-facility-standard.viewDocument');

        // Port - Port Drawing
        Route::prefix('port-drawing')->name('port-drawing.')->group(function () {
            Route::get('/', [PortDrawingController::class, 'indexPort'])->name('indexPort');
            Route::get('detail/{dock_id}', [PortDrawingController::class, 'detailPort'])->name('detailPort');
            Route::get('print/{dock_id}', [PortDrawingController::class, 'print'])->name('print');
            Route::delete('delete/{portDrawing}', [PortDrawingController::class, 'destroy'])->name('destroy');
            Route::get('document/{portDrawing}', [PortDrawingController::class, 'getDocument'])->name('getDocument');
            Route::get('document/{document_id}/view', [PortDrawingController::class, 'viewDocument'])->name('viewDocument');
            Route::get('{dock_id}/add', [PortDrawingController::class, 'index'])->name('index');
            Route::post('{dock_id}/send', [PortDrawingController::class, 'sendApproval'])->name('sendApproval');
            Route::get('{portDrawing}/show', [PortDrawingController::class, 'show'])->name('show');
            Route::post('{portDrawing}/update', [PortDrawingController::class, 'update'])->name('update');
            Route::post('{dock_id}/store', [PortDrawingController::class, 'store'])->name('store');
            Route::get('total/{dock_id}', [PortDrawingController::class, 'countPortDrawing'])->name('countPortDrawing');
        });

        // Port - Technical Quality Standard
        Route::prefix('technical-quality-standard')->name('technical-quality-standard.')->group(function () {
            Route::get('/', [TechnicalQualityStandardController::class, 'index'])->name('index');
            Route::post('store', [TechnicalQualityStandardController::class, 'store'])->name('store');
            Route::get('show/{technicalQualityStandard}', [TechnicalQualityStandardController::class, 'show'])->name('show');
            Route::post('update/{technicalQualityStandard}', [TechnicalQualityStandardController::class, 'update'])->name('update');
            Route::delete('delete/{technicalQualityStandard}', [TechnicalQualityStandardController::class, 'destroy'])->name('destroy');
            Route::get('count-technical-quality-standard', [TechnicalQualityStandardController::class, 'countTechnicalQualityStandard'])->name('countTechnicalQualityStandard');
            Route::get('document/{technicalQualityStandard}', [TechnicalQualityStandardController::class, 'getDocument'])->name('getDocument');
            Route::get('document/{document_id}/view', [TechnicalQualityStandardController::class, 'viewDocument'])->name('viewDocument');
            Route::post('send-approval', [TechnicalQualityStandardController::class, 'sendApproval'])->name('sendApproval');
            Route::get('documents-approved', [TechnicalQualityStandardController::class, 'documentsApproved'])->name('documentsApproved');
            Route::get('print', [TechnicalQualityStandardController::class, 'print'])->name('print');
        });

        // Port - Facility Standard
        Route::prefix('facility-standard')->name('facility-standard.')->group(function () {
            Route::get('/', [FacilityStandardController::class, 'index'])->name('index');
            Route::post('store', [FacilityStandardController::class, 'store'])->name('store');
            Route::get('show/{facilityStandard}', [FacilityStandardController::class, 'show'])->name('show');
            Route::post('update/{facilityStandard}', [FacilityStandardController::class, 'update'])->name('update');
            Route::delete('delete/{facilityStandard}', [FacilityStandardController::class, 'destroy'])->name('destroy');
            Route::get('count-facility-standard', [FacilityStandardController::class, 'countFacilityStandard'])->name('countFacilityStandard');
            Route::get('document/{facilityStandard}', [FacilityStandardController::class, 'getDocument'])->name('getDocument');
            Route::get('document/{document_id}/view', [FacilityStandardController::class, 'viewDocument'])->name('viewDocument');
            Route::post('send-approval', [FacilityStandardController::class, 'sendApproval'])->name('sendApproval');
            Route::get('documents-approved', [FacilityStandardController::class, 'documentsApproved'])->name('documentsApproved');
            Route::get('print', [FacilityStandardController::class, 'print'])->name('print');
        });

        // Port - Port Specification
        Route::prefix('port-specification')->name('port-specification.')->group(function () {
            Route::get('/', [PortSpecificationController::class, 'index'])->name('index');
            Route::get('count-specification/{dock_id}', [PortSpecificationController::class, 'countPortSpecification'])->name('countPortSpecification');
            Route::get('detail/{dock_id}', [PortSpecificationController::class, 'indexDetail'])->name('indexDetail');
            Route::get('detail-specification/{portSpecification}', [PortSpecificationController::class, 'show'])->name('show');
            Route::get('detail-approved/{dock_id}', [PortSpecificationController::class, 'detailApproved'])->name('detailApproved');
            Route::post('store/{dock_id}', [PortSpecificationController::class, 'store'])->name('store');
            Route::post('update/{portSpecification}', [PortSpecificationController::class, 'update'])->name('update');
            Route::get('document/{portSpecification}', [PortSpecificationController::class, 'getDocument'])->name('getDocument');
            Route::get('document/{document_id}/view', [PortSpecificationController::class, 'viewDocument'])->name('viewDocument');
            Route::post('send-approval/{dock_id}', [PortSpecificationController::class, 'sendApproval'])->name('sendApproval');
            Route::get('print/{dock_id}', [PortSpecificationController::class, 'print'])->name('print');
            Route::delete('delete/{portSpecification}', [PortSpecificationController::class, 'destroy'])->name('destroy');
        });

        // Port - Document OF Quality
        Route::prefix('document-of-quality')->name('document-of-quality.')->group(function () {
            Route::get('/', [DocumentOfQualityController::class, 'index'])->name('index');
            Route::get('detail/{dock_id}', [DocumentOfQualityController::class, 'indexDetail'])->name('indexDetail');
            Route::post('store/{dock_id}', [DocumentOfQualityController::class, 'store'])->name('store');
            Route::get('count-document/{dock_id}', [DocumentOfQualityController::class, 'countDocumentOfQuality'])->name('countDocumentOfQuality');
            Route::get('detail-document/{documentOfQuality}', [DocumentOfQualityController::class, 'show'])->name('show');
            Route::get('detail-approved/{dock_id}', [DocumentOfQualityController::class, 'detailApproved'])->name('detailApproved');
            Route::post('update/{documentOfQuality}', [DocumentOfQualityController::class, 'update'])->name('update');
            Route::get('document/{documentOfQuality}', [DocumentOfQualityController::class, 'getDocument'])->name('getDocument');
            Route::get('document/{document_id}/view', [DocumentOfQualityController::class, 'viewDocument'])->name('viewDocument');
            Route::post('send-approval/{dock_id}', [DocumentOfQualityController::class, 'sendApproval'])->name('sendApproval');
            Route::delete('delete/{documentOfQuality}', [DocumentOfQualityController::class, 'destroy'])->name('destroy');
            Route::get('print/{dock_id}', [DocumentOfQualityController::class, 'print'])->name('print');
        });
    });

    // E-Report
    Route::prefix('e-report')->name('e-report.')->group(function () {
        Route::prefix('selesai')->name('finish.')->group(function () {
            Route::get('/', [FinishedReprotController::class, 'index'])->name('index');
            Route::get('datatables', [FinishedReprotController::class, 'datatables'])->name('datatables');
            Route::get('calendar', [FinishedReprotController::class, 'dataCalendar'])->name('calendar');
            Route::get('detail/{id}', [FinishedReprotController::class, 'detail'])->name('detail');
            Route::get('datatables/statistic', [FinishedReprotController::class, 'datatablesStatistic'])->name('datatables.statistic');
            Route::get('datatables/history/{id}', [FinishedReprotController::class, 'datatablesHistory'])->name('datatables.history');
            Route::get('print/assignment', [FinishedReprotController::class, 'printAssignment'])->name('print.assignment');
        });

        Route::prefix('progress')->name('progress.')->group(function () {
            Route::get('/', [ProgressReportController::class, 'index'])->name('index');
            Route::get('detail/{id}', [ProgressReportController::class, 'detail'])->name('detail');
            Route::get('detail-catatan/{id}', [ProgressReportController::class, 'detail_catatan'])->name('detail.catatan');
            Route::get('datatables-vp', [ProgressReportController::class, 'datatablesVP'])->name('datatables.vp');
            Route::get('datatables-manager', [ProgressReportController::class, 'datatablesManager'])->name('datatables.manager');
            Route::get('datatables-staff', [ProgressReportController::class, 'datatablesStaff'])->name('datatables.staff');
            Route::get('datatables-admin', [ProgressReportController::class, 'datatablesAdmin'])->name('datatables.admin');

            Route::post('upload-hasil-penugasan', [ProgressReportController::class, 'upload_hasil_penugasan'])->name('upload.hasil_penugasan');
            Route::post('complete-progres', [ProgressReportController::class, 'complete_progress'])->name('complete_progress');
            Route::post('catatan-revisi', [ProgressReportController::class, 'catatan_revisi'])->name('catatan_revisi');
            Route::post('catatan-revisi-perbaikan', [ProgressReportController::class, 'catatan_revisi_perbaikan'])->name('catatan_revisi_perbaikan');
            Route::post('catatan-revisi-approve', [ProgressReportController::class, 'approve_revision'])->name('approve_revision');
            Route::post('catatan-revisi-penolakan', [ProgressReportController::class, 'penolakan_revision'])->name('penolakan_revision');
            Route::post('catatan-revisi-delete', [ProgressReportController::class, 'delete_revision'])->name('delete_revision');
            Route::post('catatan-revisi-approve-all', [ProgressReportController::class, 'approve_all_revision'])->name('approve_all_revision');
            Route::post('penolakan-vp', [ProgressReportController::class, 'penolakan_vp'])->name('penolakan_vp');
        });
        Route::prefix('penugasan')->name('penugasan.')->group(function () {
            Route::get('/', [PenugasanReportController::class, 'index'])->name('index');
            Route::post('store', [PenugasanReportController::class, 'store'])->name('store');
            Route::get('show/{id}', [PenugasanReportController::class, 'show'])->name('show');
            Route::get('detail/{id}', [PenugasanReportController::class, 'detail'])->name('detail');
            Route::post('update/{id}', [PenugasanReportController::class, 'update'])->name('update');
            Route::delete('delete/{id}', [PenugasanReportController::class, 'destroy'])->name('destroy');
            Route::get('penugasan-staff', [PenugasanReportController::class, 'datatbStaff'])->name('datatables.staff');
            Route::post('disposisi', [PenugasanReportController::class, 'disposisi'])->name('disposisi.staff');
            Route::post('pengajuan/{id}', [PenugasanReportController::class, 'sendPenugasan'])->name('pengajuan');
            Route::post('status/{id}', [PenugasanReportController::class, 'statusUpdate'])->name('status.update');
            Route::post('manager-status/{id}', [PenugasanReportController::class, 'managerStatusUpdate'])->name('status.manager.update');
        });
    });

    // Project Monitoring
    Route::prefix('project-monitoring')->name('ipm.')->group(function () {
        Route::prefix('fleet')->name('fleet.')->group(function () {
            Route::get('/', [FleetProjectController::class, 'index'])->name('index');
            Route::post('/', [FleetProjectController::class, 'store'])->name('store');
            Route::get('detail/{project}', [FleetProjectController::class, 'show'])->name('detail');
            Route::post('update/{project}', [FleetProjectController::class, 'update'])->name('update');
            Route::post('reopen', [FleetProjectController::class, 'reopen'])->name('reopen');
            Route::delete('{project}', [FleetProjectController::class, 'destroy'])->name('delete');

            Route::prefix('progress')->name('progress.')->group(function () {
                Route::get('{project}', [FleetProgressController::class, 'index'])->name('index');
                Route::post('/store', [FleetProgressController::class, 'store'])->name('store');
                Route::post('/update/{project}', [FleetProgressController::class, 'update'])->name('update');
                Route::post('/duplicate/{project}', [FleetProgressController::class, 'duplicate'])->name('duplicate');
                Route::delete('{project}', [FleetProgressController::class, 'destroy'])->name('delete');
                Route::post('/update-project/{project}', [FleetProgressController::class, 'update_project'])->name('update_project');
                Route::post('get-progress', [FleetProgressController::class, 'progress_get'])->name('progress_get');
                Route::post('reopen-approval-progress', [FleetProgressController::class, 'reopen_progress'])->name('reopen_progress');
                Route::post('approval-status', [FleetProgressController::class, 'approval_status'])->name('approval_status');

                //Dokumentasi(Evidence)
                Route::post('get-dokumentasi', [FleetProgressController::class, 'evidence_get'])->name('dokumentasi_get');
                Route::post('add-dokumentasi', [FleetProgressController::class, 'evidence_store'])->name('dokumentasi_store');
                Route::post('delete-dokumentasi/{evidence}', [FleetProgressController::class, 'evidence_destroy'])->name('dokumentasi_delete');
            });
            Route::prefix('progress_periode')->name('progress_periode.')->group(function () {
                Route::get('{project}', [FleetProgressController::class, 'index_periode'])->name('index_periode');
                Route::post('{project}/print', [FleetProgressController::class, 'print_periode'])->name('print_periode');
                Route::post('add-main-item', [FleetProgressController::class, 'main_item_store'])->name('store_main_item');
                Route::post('edit-main-item/{main_item}', [FleetProgressController::class, 'main_item_update'])->name('update_main_item');
                Route::post('delete-main-item/{main_item}', [FleetProgressController::class, 'main_item_destroy'])->name('delete_main_item');
                Route::get('{project}/edit/{sub_item}', [FleetProgressController::class, 'edit_periode_item'])->name('edit_periode_item');
                Route::post('add-sub-item', [FleetProgressController::class, 'sub_item_store'])->name('store_sub_item');
                Route::post('add-task-item', [FleetProgressController::class, 'task_item_store'])->name('store_task_item');
                Route::post('delete-sub-item/{sub_item}', [FleetProgressController::class, 'sub_item_destroy'])->name('delete_sub_item');
            });


            Route::prefix('quality')->name('quality.')->group(function () {
                Route::get('{project}', [FleetQualityController::class, 'index'])->name('index');
                Route::post('/{project}/store', [FleetQualityController::class, 'store_quality'])->name('store');
                Route::post('/{project}/store-periode', [FleetQualityController::class, 'store_periode'])->name('store_periode');
                Route::post('/{visual}/store-bobot', [FleetQualityController::class, 'store_bobot'])->name('store_bobot');
                Route::get('/{project}/detail/{periode}', [FleetQualityController::class, 'detail'])->name('detail');
                Route::get('/{project}/detail/{periode}/print', [FleetQualityController::class, 'detail_print'])->name('detail_print');
                Route::post('/{project}/detail/{periode}', [FleetQualityController::class, 'store_group_remarks'])->name('store_group_remarks');
                Route::post('/{project}/detail/{periode}/duplicate-section', [FleetQualityController::class, 'duplicate_section'])->name('duplicate_section');
                Route::post('/{project}/detail/{periode}/duplicate-code', [FleetQualityController::class, 'duplicate_group'])->name('duplicate_code');
                Route::put('/{project}/detail/{periode}', [FleetQualityController::class, 'update_group_remarks'])->name('update_group_remarks');
                Route::delete('/{project}/detail/{periode}', [FleetQualityController::class, 'destroy_detail'])->name('delete_group_remarks');
                Route::get('/visual-index', [FleetQualityController::class, 'index_quality_by_periode'])->name('visual-index');
                Route::delete('/{project}/delete', [FleetQualityController::class, 'delete_quality'])->name('delete');

                Route::prefix('{project}/milestone')->name('milestone.')->group(function () {
                    Route::get('/', [FleetQualityController::class, 'indexMilestone'])->name('index');
                    Route::post('/', [FleetQualityController::class, 'storeMilestone'])->name('store');
                    Route::delete('/', [FleetQualityController::class, 'destroyMilestone'])->name('delete');
                    Route::post('update', [FleetQualityController::class, 'updateMilestone'])->name('update');
                    Route::get('detail', [FleetQualityController::class, 'showMilestone'])->name('show');
                });


                Route::prefix('{project}/inspection/{periode}')->name('inspection.')->group(function () {
                    Route::get('/', [FleetQualityController::class, 'indexInspection'])->name('index');
                    Route::post('/', [FleetQualityController::class, 'storeInspection'])->name('store');
                    Route::delete('/', [FleetQualityController::class, 'destroyInspection'])->name('delete');
                    Route::get('detail', [FleetQualityController::class, 'showInspection'])->name('detail');
                    Route::post('update', [FleetQualityController::class, 'updateInspection'])->name('update');
                });


                Route::prefix('{project}/drawing')->name('drawing.')->group(function () {
                    Route::get('/', [FleetQualityController::class, 'indexDrawing'])->name('index');
                    Route::post('/', [FleetQualityController::class, 'storeDrawing'])->name('store');
                    Route::post('/store-group', [FleetQualityController::class, 'storeDrawingGroup'])->name('store_group');
                    Route::delete('/', [FleetQualityController::class, 'destroyDrawing'])->name('delete');
                    Route::get('detail', [FleetQualityController::class, 'showDrawing'])->name('detail');
                    Route::post('update', [FleetQualityController::class, 'updateDrawing'])->name('update');
                });
            });

            //Basic Data
            Route::prefix('basic-data')->name('basic_data.')->group(function () {
                Route::get('/', [BasicDataController::class, 'index'])->name('index');
                Route::post('/store', [BasicDataController::class, 'store'])->name('store');
                Route::delete('/{basic_data}', [BasicDataController::class, 'destroy'])->name('delete');
            });

            Route::prefix('{project}/recomendation')->name('recomendation.')->group(function () {
                Route::get('/', [FleetRecomendationController::class, 'index'])->name('index');
                Route::post('/', [FleetRecomendationController::class, 'store'])->name('store');
                Route::get('print', [FleetRecomendationController::class, 'print'])->name('print');

                Route::prefix('{period}/notice')->name('notice.')->group(function () {
                    Route::get('/', [FleetRecomendationController::class, 'indexNotice'])->name('index');
                    Route::post('/', [FleetRecomendationController::class, 'storeNotice'])->name('store');
                    Route::delete('/', [FleetRecomendationController::class, 'destroyNotice'])->name('delete');
                    Route::get('detail', [FleetRecomendationController::class, 'showNotice'])->name('show');
                    Route::post('update', [FleetRecomendationController::class, 'updateNotice'])->name('update');
                    Route::get('print', [FleetRecomendationController::class, 'printNotice'])->name('print');
                });
            });

            Route::prefix('{project}/document')->name('document.')->group(function () {
                Route::get('select-type', [FleetDocumentController::class, 'selectDocumentType'])->name('select-type');
                Route::get('/', [FleetDocumentController::class, 'index'])->name('index');
                Route::post('/', [FleetDocumentController::class, 'store'])->name('store');
                Route::delete('/', [FleetDocumentController::class, 'destroy'])->name('delete');
                Route::post('update', [FleetDocumentController::class, 'update'])->name('update');
                Route::get('print', [FleetDocumentController::class, 'print'])->name('print');
                Route::get('{document}', [FleetDocumentController::class, 'show'])->name('show');

                Route::prefix('{document}/sub')->name('sub.')->group(function () {
                    Route::get('/', [FleetDocumentController::class, 'indexSub'])->name('index');
                    Route::post('/', [FleetDocumentController::class, 'storeSub'])->name('store');
                    Route::delete('/', [FleetDocumentController::class, 'destroySub'])->name('delete');
                    Route::get('detail', [FleetDocumentController::class, 'showSub'])->name('show');
                    Route::post('update', [FleetDocumentController::class, 'updateSub'])->name('update');
                    Route::get('print', [FleetDocumentController::class, 'printSub'])->name('print');
                });
            });

            Route::prefix('{project}/summary')->name('summary.')->group(function () {
                Route::get('/', [FleetSummaryController::class, 'index'])->name('index');
                Route::get('print', [FleetSummaryController::class, 'print'])->name('print');
                Route::post('send-approval', [FleetSummaryController::class, 'sendApproval'])->name('send-approval');
                Route::get('history-approval', [FleetSummaryController::class, 'historyApproval'])->name('history-approval');
            });

            Route::prefix('{project}/completion')->name('completion.')->group(function () {
                Route::get('/', [FleetCompletionController::class, 'index'])->name('index');
                Route::post('send-approval', [FleetCompletionController::class, 'sendApproval'])->name('send-approval');
            });
        });

        Route::prefix('port')->name('port.')->group(function () {
            Route::get('/', [PortProjectController::class, 'index'])->name('index');
            Route::post('/', [PortProjectController::class, 'store'])->name('store');
            Route::delete('{project}', [PortProjectController::class, 'destroy'])->name('delete');
            Route::post('reopen', [PortProjectController::class, 'reopen'])->name('reopen');

            Route::prefix('progress')->name('progress.')->group(function () {
                Route::get('{project}', [PortProgressController::class, 'index'])->name('index');
                Route::post('/store', [PortProgressController::class, 'store'])->name('store');
                Route::post('/update/{project}', [PortProgressController::class, 'update'])->name('update');
                Route::post('/duplicate/{project}', [PortProgressController::class, 'duplicate'])->name('duplicate');
                Route::delete('{project}', [PortProgressController::class, 'destroy'])->name('delete');
                Route::post('/update-project/{project}', [PortProgressController::class, 'update_project'])->name('update_project');
                Route::post('get-progress', [PortProgressController::class, 'progress_get'])->name('progress_get');
                Route::post('reopen-approval-progress', [PortProgressController::class, 'reopen_progress'])->name('reopen_progress');
                Route::post('approval-status', [PortProgressController::class, 'approval_status'])->name('approval_status');

                //Dokumentasi(Evidence)
                Route::post('get-dokumentasi', [PortProgressController::class, 'evidence_get'])->name('dokumentasi_get');
                Route::post('add-dokumentasi', [PortProgressController::class, 'evidence_store'])->name('dokumentasi_store');
                Route::post('delete-dokumentasi/{evidence}', [PortProgressController::class, 'evidence_destroy'])->name('dokumentasi_delete');
            });

            Route::prefix('progress_periode')->name('progress_periode.')->group(function () {
                Route::get('{project}', [PortProgressController::class, 'index_periode'])->name('index_periode');
                Route::post('{project}/print', [PortProgressController::class, 'print_periode'])->name('print_periode');
                Route::get('{project}/edit/{main_item}', [PortProgressController::class, 'edit_periode_item'])->name('edit_periode_item');
                Route::post('delete-main-item/{main_item}', [PortProgressController::class, 'main_item_destroy'])->name('delete_main_item');
                Route::post('add-main-item', [PortProgressController::class, 'main_item_store'])->name('store_main_item');
                Route::post('edit-main-item', [PortProgressController::class, 'main_item_update'])->name('update_main_item');
            });

            Route::prefix('{project}/quality')->name('quality.')->group(function () {
                Route::get('/', [PortQualityController::class, 'index'])->name('index');
                Route::get('detail-inspection', [PortQualityController::class, 'detail_inspection'])->name('detail-inspection');
                Route::get('print', [PortQualityController::class, 'print'])->name('print');

                Route::prefix('group')->name('group.')->group(function () {
                    Route::post('/', [PortQualityController::class, 'storeVisualQuality'])->name('store');
                    Route::delete('/', [PortQualityController::class, 'destroyVisualQuality'])->name('delete');
                    Route::get('detail', [PortQualityController::class, 'showVisualQuality'])->name('detail');
                    Route::post('update', [PortQualityController::class, 'updateVisualQuality'])->name('update');
                    Route::get('type', [PortQualityController::class, 'selectVisualQuality'])->name('type');
                });

                Route::prefix('period')->name('period.')->group(function () {
                    Route::get('/', [PortQualityController::class, 'indexPeriod'])->name('index');
                    Route::post('/', [PortQualityController::class, 'storePeriod'])->name('store');
                    Route::post('update', [PortQualityController::class, 'updatePeriod'])->name('update');
                });

                Route::prefix('remark')->name('remark.')->group(function () {
                    Route::get('/', [PortQualityController::class, 'indexRemark'])->name('index');
                    Route::delete('/', [PortQualityController::class, 'destroyRemark'])->name('delete');
                });

                Route::prefix('drawing')->name('drawing.')->group(function () {
                    Route::get('/', [PortQualityController::class, 'indexDrawing'])->name('index');
                    Route::post('/', [PortQualityController::class, 'storeDrawing'])->name('store');
                    Route::delete('/', [PortQualityController::class, 'destroyDrawing'])->name('delete');
                    Route::get('detail', [PortQualityController::class, 'showDrawing'])->name('detail');
                    Route::post('update', [PortQualityController::class, 'updateDrawing'])->name('update');
                    Route::post('period', [PortQualityController::class, 'periodDrawing'])->name('period');
                });

                Route::prefix('{sub}/inspection')->name('inspection.')->group(function () {
                    Route::get('/', [PortQualityController::class, 'indexInspection'])->name('index');
                    Route::post('/', [PortQualityController::class, 'storeInspection'])->name('store');
                    Route::delete('/', [PortQualityController::class, 'destroyInspection'])->name('delete');
                    Route::get('detail', [PortQualityController::class, 'showInspection'])->name('detail');
                    Route::post('update', [PortQualityController::class, 'updateInspection'])->name('update');
                });

                Route::get('{period}/detail', [PortQualityController::class, 'detail'])->name('detail');

                Route::prefix('{period}/item')->name('item.')->group(function () {
                    Route::get('/', [PortQualityController::class, 'indexItem'])->name('index');
                    Route::post('/', [PortQualityController::class, 'storeItem'])->name('store');
                    Route::get('detail', [PortQualityController::class, 'showItem'])->name('detail');
                    Route::post('update', [PortQualityController::class, 'updateItem'])->name('update');
                    Route::get('print', [PortQualityController::class, 'printItem'])->name('print');

                    Route::prefix('duplicate')->name('duplicate.')->group(function () {
                        Route::post('group', [PortQualityController::class, 'duplicateItemToOtherType'])->name('group');
                        Route::post('code', [PortQualityController::class, 'duplicateItemCode'])->name('code');
                    });
                });
            });

            //Basic Data
            Route::prefix('basic-data')->name('basic_data.')->group(function () {
                Route::get('/', [PortBasicDataController::class, 'index'])->name('index');
                Route::post('/store', [PortBasicDataController::class, 'store'])->name('store');
                Route::delete('/{basic_data}', [PortBasicDataController::class, 'destroy'])->name('delete');
            });

            Route::prefix('{project}/recomendation')->name('recomendation.')->group(function () {
                Route::get('/', [PortRecomendationController::class, 'index'])->name('index');
                Route::post('/', [PortRecomendationController::class, 'store'])->name('store');
                Route::get('print', [PortRecomendationController::class, 'print'])->name('print');

                Route::prefix('{period}/notice')->name('notice.')->group(function () {
                    Route::get('/', [PortRecomendationController::class, 'indexNotice'])->name('index');
                    Route::post('/', [PortRecomendationController::class, 'storeNotice'])->name('store');
                    Route::delete('/', [PortRecomendationController::class, 'destroyNotice'])->name('delete');
                    Route::get('detail', [PortRecomendationController::class, 'showNotice'])->name('show');
                    Route::post('update', [PortRecomendationController::class, 'updateNotice'])->name('update');
                    Route::get('print', [PortRecomendationController::class, 'printNotice'])->name('print');
                });
            });

            Route::prefix('{project}/document')->name('document.')->group(function () {
                Route::get('select-type', [PortDocumentController::class, 'selectDocumentType'])->name('select-type');
                Route::get('/', [PortDocumentController::class, 'index'])->name('index');
                Route::post('/', [PortDocumentController::class, 'store'])->name('store');
                Route::delete('/', [PortDocumentController::class, 'destroy'])->name('delete');
                Route::post('update', [PortDocumentController::class, 'update'])->name('update');
                Route::get('print', [PortDocumentController::class, 'print'])->name('print');
                Route::get('{document}', [PortDocumentController::class, 'show'])->name('show');

                Route::prefix('{document}/sub')->name('sub.')->group(function () {
                    Route::get('/', [PortDocumentController::class, 'indexSub'])->name('index');
                    Route::post('/', [PortDocumentController::class, 'storeSub'])->name('store');
                    Route::delete('/', [PortDocumentController::class, 'destroySub'])->name('delete');
                    Route::get('detail', [PortDocumentController::class, 'showSub'])->name('show');
                    Route::post('update', [PortDocumentController::class, 'updateSub'])->name('update');
                    Route::get('print', [PortDocumentController::class, 'printSub'])->name('print');
                });
            });

            Route::prefix('{project}/summary')->name('summary.')->group(function () {
                Route::get('/', [PortSummaryController::class, 'index'])->name('index');
                Route::get('print', [PortSummaryController::class, 'print'])->name('print');
                Route::post('send-approval', [PortSummaryController::class, 'sendApproval'])->name('send-approval');
                Route::get('history-approval', [PortSummaryController::class, 'historyApproval'])->name('history-approval');
            });

            Route::prefix('{project}/completion')->name('completion.')->group(function () {
                Route::get('/', [PortCompletionController::class, 'index'])->name('index');
                Route::post('send-approval', [PortCompletionController::class, 'sendApproval'])->name('send-approval');
            });
        });
    });

    Route::prefix('e-rcm')->name('e-rcm.')->group(function () {
        Route::prefix('fleet')->name('fleet.')->group(function () {
            Route::get('/', [FleetERCMController::class, 'index'])->name('index');
            Route::post('/', [FleetERCMController::class, 'store'])->name('store');
            Route::delete('/', [FleetERCMController::class, 'destroy'])->name('delete');
            Route::post('/detail/period', [FleetERCMController::class, 'upsertFleetPeriod'])->name('upsert-period');
            Route::delete('/detail/period', [FleetERCMController::class, 'destroyFleetPeriod'])->name('delete-period');
            Route::get('/detail/{id}', [FleetERCMController::class, 'show'])->name('show');
            Route::get('/summary/{id}', [FleetERCMController::class, 'summaryIndex'])->name('summary');
            Route::get('/system-parameter/{id}', [FleetERCMController::class, 'systemParameterIndex'])->name('system-parameter');
            Route::get('/detail-system-parameter/{id}', [FleetERCMController::class, 'detailSystemParameterIndex'])->name('detail-system-parameter');
            Route::get('/sub-detail-system-parameter/{id}', [FleetERCMController::class, 'subDetailSystemParameterIndex'])->name('sub-detail-system-parameter');
            Route::post('send-approval', [FleetERCMController::class, 'submitApproval'])->name('submit-approval');
            Route::post('{data}/label', [FleetERCMController::class, 'label'])->name('label');
        });

        Route::prefix('port')->name('port.')->group(function () {
            Route::get('/', [PortERCMController::class, 'index'])->name('index');
            Route::post('/', [PortERCMController::class, 'store'])->name('store');
            Route::delete('/', [PortERCMController::class, 'destroy'])->name('delete');
            Route::post('/detail/period', [PortERCMController::class, 'upsertPortPeriod'])->name('upsert-period');
            Route::delete('/detail/period', [PortERCMController::class, 'destroyPortPeriod'])->name('delete-period');
            Route::get('/detail/{id}', [PortERCMController::class, 'show'])->name('show');
            Route::get('/summary/{id}', [PortERCMController::class, 'summaryIndex'])->name('summary');
            Route::get('/system-parameter/{id}', [PortERCMController::class, 'systemParameterIndex'])->name('system-parameter');
            Route::get('/detail-system-parameter/{id}', [PortERCMController::class, 'detailSystemParameterIndex'])->name('detail-system-parameter');
            Route::get('/sub-detail-system-parameter/{id}', [PortERCMController::class, 'subDetailSystemParameterIndex'])->name('sub-detail-system-parameter');
            Route::post('send-approval', [PortERCMController::class, 'submitApproval'])->name('submit-approval');
            Route::post('{data}/label', [PortERCMController::class, 'label'])->name('label');
        });
    });

    // Activity Log
    Route::resource('/activity-log', ActivityLogController::class)
        ->only(['index', 'show']);
});

/**
 * QR Code validation page without auth
 */
Route::prefix('qr-code')->name('qr-code.')->group(function () {
    Route::get('e-report/selesai/statistik', [FinishedReprotController::class, 'qrCodeValidation'])->name('e-report.selesai.statistik');
    Route::get('e-report/selesai/hasil-penugasan', [FinishedReprotController::class, 'qrCodeValidation'])->name('e-report.selesai.hasil-penugasan');
});

// Route::resource('users', UsersController::class);

/**
 * Socialite login using Google service
 * https://laravel.com/docs/8.x/socialite
 */
Route::get('/auth/redirect/{provider}', [SocialiteLoginController::class, 'redirect']);

require __DIR__ . '/auth.php';
