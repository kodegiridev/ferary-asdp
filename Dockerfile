# Use a base image
FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
# Update package list and install necessary packages
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-get install -y curl && \
    apt-get install nano && \
    add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y nginx && \
    apt-get install -y php8.2 php8.2-fpm php8.2-mysql php8.2-mbstring php8.2-xml php8.2-gd php8.2-zip php8.2-bcmath php8.2-curl php8.2-calendar && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && \
    apt-get install -y nodejs


COPY ./docker/php/php.ini /etc/php/8.2/fpm/


# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer


# Create a directory for the Laravel app
RUN mkdir -p /var/www/html

# Copy Laravel app files into the container
COPY . /var/www/html

# Set working directory
WORKDIR /var/www/html

# Install Laravel dependencies
RUN composer update && composer install

# Configure Nginx
RUN rm -rf /etc/nginx/site-enable/default
COPY ./docker/nginx/nginx.conf /etc/nginx/sites-available/default


RUN php artisan key:generate

# Build and install Node.js dependencies
COPY package.json /var/www/html
RUN npm install
RUN npm run dev

EXPOSE 80



# Start services
CMD service php8.2-fpm start && nginx -g 'daemon off;'



