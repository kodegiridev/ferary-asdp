<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component_condition_criterias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_components_id')->nullable();
            $table->tinyInteger('up1')->nullable();
            $table->tinyInteger('down1')->nullable();
            $table->tinyInteger('status1')->nullable();
            $table->tinyInteger('up2')->nullable();
            $table->tinyInteger('down2')->nullable();
            $table->tinyInteger('status2')->nullable();
            $table->tinyInteger('up3')->nullable();
            $table->tinyInteger('down3')->nullable();
            $table->tinyInteger('status3')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('ercm_components_id')->references('id')->on('ercm_components')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component_condition_criterias');
    }
};
