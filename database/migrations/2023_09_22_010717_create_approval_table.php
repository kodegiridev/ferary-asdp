<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pengaju_id');
            $table->unsignedBigInteger('penyetuju_id')->nullable();
            $table->string('jenis_modul');
            $table->string('sub_modul');
            $table->date('tgl_pengajuan');
            $table->date('tgl_penyetujuan')->nullable();
            $table->date('tgl_tolak_penyetuju')->nullable();
            $table->string('keterangan_pengaju')->nullable();
            $table->string('keterangan_penyetuju')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval');
    }
};
