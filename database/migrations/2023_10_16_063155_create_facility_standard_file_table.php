<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_facility_standard_file', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('facility_standard_id');
            $table->string('file');
            $table->timestamps();
            $table->foreign('facility_standard_id', 'facility_standard_id_foreign')->references('id')
                ->on('ed_facility_standard')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_facility_standard_file');
    }
};
