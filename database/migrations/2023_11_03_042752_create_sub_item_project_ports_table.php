<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_item_project_ports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('main_item_id');
            $table->string('sub_item');
            $table->timestamps();
            $table->foreign('main_item_id')->references('id')->on('main_item_project_ports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_item_project_ports');
    }
};
