<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_machinery_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vqf_machinery_id');
            $table->string('nama_staging');
            $table->integer('status');
            $table->text('catatan');
            $table->timestamps();

            $table->foreign('vqf_machinery_id')->references('id')->on('vqf_machinery')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_machinery_remarks');
    }
};
