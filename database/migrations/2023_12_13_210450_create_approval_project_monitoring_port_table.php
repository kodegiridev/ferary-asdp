<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_project_monitoring_port', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->unsignedBigInteger('project_monitoring_port_id')->nullable();
            $table->unsignedBigInteger('progres_project_port_id')->nullable();
            $table->string('judul_dokumen')->nullable()->comment('on summary');
            $table->text('catatan_project')->nullable()->comment('on completion');
            $table->integer('type')->nullable()->default(1)->comment('1=Summary,2=Completion');
            $table->integer('status')->nullable()->default(1)->comment('0=Inactive,1=Active,2=Approved');
            $table->timestamps();

            $table->foreign('approval_id')->references('id')->on('approval')->onDelete('cascade');
            $table->foreign('project_monitoring_port_id', 'fk_ipm_port')->references('id')->on('project_monitoring_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_project_monitoring_port');
    }
};
