<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestone_quality_fleet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->integer('tipe_milestone')->nullable()->comment('1.KickOfMeeting|2.FirstSteelCulting|3.KeelLaying|4.EngineLoading|5.Launching|6.SeaTrial|7.Delivery');
            $table->string('item_inspeksi')->nullable();
            $table->string('task_list')->nullable();
            $table->integer('progres')->nullable();
            $table->text('evidence')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('project_monitoring_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milestone_quality_fleet');
    }
};
