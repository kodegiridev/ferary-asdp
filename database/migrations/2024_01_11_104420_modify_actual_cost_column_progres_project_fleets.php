<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('progres_project_fleets', function (Blueprint $table) {
            $table->unsignedBigInteger('actual_cost')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('progres_project_fleets', function (Blueprint $table) {
            $table->integer('actual_cost')->change();
        });
    }
};
