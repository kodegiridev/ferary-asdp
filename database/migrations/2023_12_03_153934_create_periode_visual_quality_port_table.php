<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periode_visual_quality_port', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sub_visual_quality_port_id')->nullable();
            $table->integer('periode')->nullable()->default(1);
            $table->integer('status_progres')->nullable()->default(0);
            $table->text('document_drawing')->nullable();
            $table->timestamps();

            $table->foreign('sub_visual_quality_port_id')->references('id')->on('sub_visual_quality_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periode_visual_quality_port');
    }
};
