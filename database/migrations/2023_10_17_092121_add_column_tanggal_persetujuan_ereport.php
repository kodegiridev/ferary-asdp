<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('e_report', function($table) {
            $table->date('tgl_penyetujuan')->nullable();
            $table->string('keterangan_penolakan_vp')->nullable();
            $table->text('file_penolakan_vp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('e_report', function($table) {
            $table->dropColumn('tgl_penyetujuan');
            $table->dropColumn('keterangan_penolakan_vp');
            $table->dropColumn('file_penolakan_vp');
        });
    }
};
