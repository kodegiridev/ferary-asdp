<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_periods', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_id');
            $table->date('period');
            $table->string('contractor');
            $table->integer('effectiveness');
            $table->string('effectiveness_doc')->nullable();
            $table->integer('cost');
            $table->string('cost_doc')->nullable();
            $table->integer('before_score')->nullable();
            $table->integer('after_score')->nullable();
            $table->integer('delta_score')->nullable();
            $table->integer('repair_ticket_execute')->nullable();
            $table->integer('repair_ticket_accept')->nullable();
            $table->integer('repair_ticket_reject')->nullable();
            $table->integer('repair_ticket_postpone')->nullable();
            $table->integer('period_status');
            $table->integer('approval_status')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('ercm_id')->references('id')->on('base_ercm')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_periods');
    }
};
