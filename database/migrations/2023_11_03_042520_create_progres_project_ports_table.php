<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progres_project_ports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->string('status_kontrak');
            $table->date('tgl_update');
            $table->integer('actual_cost');
            $table->text('remarks');
            $table->float('plan')->nullable();
            $table->float('aktual')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('project_monitoring_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progres_project_ports');
    }
};
