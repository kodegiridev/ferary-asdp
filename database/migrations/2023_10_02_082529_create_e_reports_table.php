<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_report', function (Blueprint $table) {
            $table->id();
            $table->string('judul_penugasan');
            $table->text('keterangan_penugasan')->nullable();
            $table->string('dokumen_penugasan')->nullable();
            $table->date('target_waktu')->nullable();
            $table->string('target_pencapaian')->nullable();
            $table->string('dok_hasil_penugasan')->nullable();
            $table->unsignedBigInteger('pembuat_id');
            $table->unsignedBigInteger('penerima1_id')->nullable();
            $table->unsignedBigInteger('penerima2_id')->nullable();
            $table->integer('status_penugasan')->default(0);
            $table->integer('status_progres')->default(0);
            $table->integer('status_penyelesaian')->default(0);
            $table->foreign('pembuat_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->foreign('penerima1_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->foreign('penerima2_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_report');
    }
};
