<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_ipm_fleet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sub_dokumen_id');
            $table->string('judul_dokumen')->nullable();
            $table->tinyInteger('status_pemenuhan')->nullable()->default(0);
            $table->string('remarks')->nullable();
            $table->string('nomor_dokumen')->nullable();
            $table->text('file')->nullable();
            $table->timestamps();

            $table->foreign('sub_dokumen_id')->references('id')->on('sub_dokumen_ipm_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_ipm_fleet');
    }
};
