<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_approvals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->unsignedBigInteger('ercm_period_id')->nullable();
            $table->unsignedBigInteger('ercm_component_id')->nullable();
            $table->timestamps();

            $table->foreign('approval_id')->references('id')->on('approval')->onDelete('cascade');
            $table->foreign('ercm_period_id')->references('id')->on('ercm_periods')->onDelete('cascade');
            $table->foreign('ercm_component_id')->references('id')->on('ercm_components')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_approvals');
    }
};
