<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_mapping_data_id')->nullable();
            $table->string('component_name')->nullable();
            $table->integer('level')->nullable()->default(1);
            $table->integer('key')->nullable()->comment('actual key on .glb');
            $table->timestamps();

            $table->foreign('ercm_mapping_data_id')->references('id')->on('ercm_mapping_data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component');
    }
};
