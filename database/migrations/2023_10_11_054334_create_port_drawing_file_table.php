<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_port_drawing_file', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('port_drawing_id');
            $table->string('file');
            $table->timestamps();
            $table->foreign('port_drawing_id')->references('id')->on('ed_port_drawing')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_port_drawing_file');
    }
};
