<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_item_project_ports', function (Blueprint $table) {
            $table->double('bobot')->after('sub_item');
            $table->double('plan')->after('bobot');
            $table->double('progres')->after('plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_item_project_ports', function (Blueprint $table) {
            $table->dropColumn('bobot');
            $table->dropColumn('plan');
            $table->dropColumn('progres');
        });
    }
};
