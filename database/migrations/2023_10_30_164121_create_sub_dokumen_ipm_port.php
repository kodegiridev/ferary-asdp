<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_dokumen_ipm_port', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jenis_dokumen_id');
            $table->string('sub_dokumen')->nullable();
            $table->float('progres')->nullable();
            $table->timestamps();

            $table->foreign('jenis_dokumen_id')->references('id')->on('jenis_dokumen_ipm_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_dokumen_ipm_port');
    }
};
