<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ercm_groups', function (Blueprint $table) {
            $table->renameColumn('ercm_system_id', 'ercm_systems_id');
            $table->renameColumn('nama', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ercm_groups', function (Blueprint $table) {
            $table->renameColumn('ercm_systems_id', 'ercm_system_id');
            $table->renameColumn('name', 'nama');
        });
    }
};
