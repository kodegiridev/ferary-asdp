<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_port_specification', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dermaga_id');
            $table->string('nama_komponen');
            $table->string('sub_komponen')->nullable();
            $table->string('parameter')->nullable();
            $table->string('spesifikasi');
            $table->string('referensi')->nullable();
            $table->text('keterangan')->nullable();
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->timestamps();
            $table->foreign('dermaga_id')->references('id')->on('mst_dermaga')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_port_specification');
    }
};
