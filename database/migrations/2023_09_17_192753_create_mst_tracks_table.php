<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_lintasan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pelabuhan_id');
            $table->unsignedBigInteger('rute_id');
            $table->unsignedBigInteger('tipe_lintasan_id');
            $table->string('nama');
            $table->foreign('pelabuhan_id')
                ->references('id')->on('mst_pelabuhan')->onDelete('cascade');
            $table->foreign('rute_id')
                ->references('id')->on('mst_rute')->onDelete('cascade');
            $table->foreign('tipe_lintasan_id')
                ->references('id')->on('mst_tipe_lintasan')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_lintasan');
    }
};
