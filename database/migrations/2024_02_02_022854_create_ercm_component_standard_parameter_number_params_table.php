<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component_standard_parameter_number_params', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_component_standard_parameters_id')->nullable();
            $table->integer('std')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->integer('mid1')->nullable();
            $table->integer('mid2')->nullable();
            $table->integer('less')->nullable();
            $table->integer('valuation_type')->nullable()->comment('1=increased,2=decreased');
            $table->timestamps();

            // $table->foreign('ercm_component_standard_parameters_id')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
            $table->foreign('ercm_component_standard_parameters_id', 'fknumber_component_standard_parameters_id')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component_standard_parameter_number_params');
    }
};
