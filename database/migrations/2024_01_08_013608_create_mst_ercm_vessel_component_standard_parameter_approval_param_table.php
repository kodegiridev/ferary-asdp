<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_vessel_component_standard_parameter_approval_param', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_vessel_component_standard_parameter_id')->nullable();
            $table->string('param')->nullable();
            $table->timestamps();

            $table->foreign('mst_ercm_vessel_component_standard_parameter_id', 'fk_approval_to_component_param')->references('id')->on('mst_ercm_vessel_component_standard_parameter')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_vessel_component_standard_parameter_approval_param');
    }
};
