<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_hc_repair_work', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visual_quality_fleet_id');
            $table->unsignedBigInteger('periode_visual_quality_id');
            $table->string('grup');
            $table->string('kodefikasi');
            $table->string('evidence');
            $table->string('jenis_proyek');
            $table->integer('jenis_data');
            $table->timestamps();

            $table->foreign('visual_quality_fleet_id')->references('id')->on('visual_quality_fleet')->onDelete('cascade');
            $table->foreign('periode_visual_quality_id')->references('id')->on('periode_visual_quality_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_hc_repair_work');
    }
};
