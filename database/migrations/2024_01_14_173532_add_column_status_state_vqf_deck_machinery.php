<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('vqf_deck_machinery', function (Blueprint $table) {
            $table->integer('status_state')->after('status_inspeksi')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('vqf_deck_machinery', function (Blueprint $table) {
            $table->dropColumn('status_plan');
        });
    }
};
