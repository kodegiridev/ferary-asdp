<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component_standard_parameter_approval_params', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_component_standard_parameters_id')->nullable();
            $table->string('param')->nullable();
            $table->timestamps();

            // $table->foreign('ercm_component_standard_parameters_id', 'fk_approval_to_sub_std_param')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
            $table->foreign('ercm_component_standard_parameters_id', 'fkap_component_standard_parameters_id')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component_standard_parameter_approval_params');
    }
};
