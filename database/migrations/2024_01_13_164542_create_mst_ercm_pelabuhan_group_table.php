<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_pelabuhan_group', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_pelabuhan_system_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('value')->nullable();
            $table->text('attachment')->nullable();
            $table->text('attachment_temp')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('mst_ercm_pelabuhan_system_id')->references('id')->on('mst_ercm_pelabuhan_system')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_pelabuhan_group');
    }
};
