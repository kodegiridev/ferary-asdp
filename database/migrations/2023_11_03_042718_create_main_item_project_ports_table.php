<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_item_project_ports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('progres_id');
            $table->string('main_item');
            $table->double('batas_bobot');
            $table->timestamps();

            $table->foreign('progres_id')->references('id')->on('progres_project_ports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_item_project_ports');
    }
};
