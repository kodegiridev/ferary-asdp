<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_port_drawing', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pelabuhan_id');
            $table->string('nama_komponen');
            $table->string('nama_gambar')->nullable();
            $table->string('nomor_gambar')->nullable();
            $table->string('tipe_gambar')->nullable();
            $table->string('status_gambar')->nullable();
            $table->text('keterangan')->nullable();
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->timestamps();
            $table->foreign('pelabuhan_id')->references('id')->on('mst_pelabuhan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_port_drawing');
    }
};
