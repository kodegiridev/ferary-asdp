<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_pelabuhan_component_standard_parameter_number_param', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_pelabuhan_component_standard_parameter_id')->nullable();
            $table->integer('std')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->integer('mid1')->nullable();
            $table->integer('mid2')->nullable();
            $table->integer('less')->nullable();
            $table->integer('valuation_type')->nullable()->comment('1=increased,2=decreased');
            $table->timestamps();

            $table->foreign('mst_ercm_pelabuhan_component_standard_parameter_id', 'fk_pelabuhan_number_to_component_param')->references('id')->on('mst_ercm_pelabuhan_component_standard_parameter')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_pelabuhan_component_standard_parameter_number_param');
    }
};
