<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_equipment_board', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vessel_id');
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->string('nama_peralatan');
            $table->string('merek');
            $table->integer('quantity');
            $table->string('tipe_sertifikat');
            $table->string('diterbitkan_oleh')->nullable();
            $table->string('nomor_sertifikat')->nullable();
            $table->date('tgl_sertifikat')->nullable();
            $table->date('tgl_habis')->nullable();
            $table->string('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->foreign('vessel_id')
                ->references('id')->on('mst_vessel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_equipment_board');
    }
};
