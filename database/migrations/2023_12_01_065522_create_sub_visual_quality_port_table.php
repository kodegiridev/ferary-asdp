<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_visual_quality_port', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visual_quality_port_id')->nullable();
            $table->string('sub_fasilitas')->nullable();
            $table->integer('bobot')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('visual_quality_port_id')->references('id')->on('visual_quality_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_visual_quality_port');
    }
};
