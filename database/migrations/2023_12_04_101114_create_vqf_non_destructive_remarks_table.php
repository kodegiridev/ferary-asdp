<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_non_destructive_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vqf_non_destructive_id');
            $table->string('remarks');
            $table->integer('status_plan');
            $table->integer('status_inspeksi');
            $table->integer('status_state');
            $table->text('catatan_inspeksi');
            $table->date('tanggal');
            $table->timestamps();

            $table->foreign('vqf_non_destructive_id')->references('id')->on('vqf_non_destructive')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_non_destructive_remarks');
    }
};
