<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqp_facility_group_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vqp_facility_group_id')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('status_plan')->nullable();
            $table->integer('status_inspeksi')->nullable();
            $table->text('catatan_inspeksi')->nullable();
            $table->date('tanggal')->nullable();
            $table->timestamps();

            $table->foreign('vqp_facility_group_id')->references('id')->on('vqp_facility_group')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqp_facility_group_remarks');
    }
};
