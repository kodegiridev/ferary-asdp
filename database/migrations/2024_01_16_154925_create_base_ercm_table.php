<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_ercm', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vessel_id')->nullable();
            $table->unsignedBigInteger('port_id')->nullable();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('vessel_id')->references('id')->on('mst_vessel')->onDelete('cascade');
            $table->foreign('port_id')->references('id')->on('mst_pelabuhan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('base_ercm');
        // Schema::enableForeignKeyConstraints();
    }
};
