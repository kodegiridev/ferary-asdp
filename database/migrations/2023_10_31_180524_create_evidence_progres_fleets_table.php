<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidence_progres_fleets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('progres_id');
            $table->string('nama');
            $table->string('file');
            $table->timestamps();

            $table->foreign('progres_id')->references('id')->on('progres_project_fleets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evidence_progres_fleets');
    }
};
