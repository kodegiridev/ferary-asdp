<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ercm_components', function (Blueprint $table) {
            $table->renameColumn('ercm_subgroup_id', 'ercm_subgroups_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ercm_components', function (Blueprint $table) {
            $table->renameColumn('ercm_subgroups_id', 'ercm_subgroup_id');
        });
    }
};
