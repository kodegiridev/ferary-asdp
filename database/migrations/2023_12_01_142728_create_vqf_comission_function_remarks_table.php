<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_comission_function_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vqf_comission_function_id');
            $table->string('nama_staging');
            $table->integer('status');
            $table->text('catatan');
            $table->timestamps();

            $table->foreign('vqf_comission_function_id')->references('id')->on('vqf_comission_function')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_comission_function_remarks');
    }
};
