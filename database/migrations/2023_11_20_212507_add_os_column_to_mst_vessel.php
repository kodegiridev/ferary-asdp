<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('mst_vessel', 'os')) {
            Schema::table('mst_vessel', function (Blueprint $table) {
                $table->unsignedBigInteger('os')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('mst_vessel', 'os')) {
            Schema::table('mst_vessel', function (Blueprint $table) {
                $table->dropColumn('os');
            });
        }
    }
};
