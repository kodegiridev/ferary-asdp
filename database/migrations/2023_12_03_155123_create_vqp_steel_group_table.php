<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqp_steel_group', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visual_quality_port_id')->nullable();
            $table->unsignedBigInteger('periode_visual_quality_port_id')->nullable();
            $table->string('sub_grup')->nullable();
            $table->string('kode')->nullable();
            $table->string('item')->nullable();
            $table->text('evidence')->nullable();
            $table->integer('jenis_data')->nullable();
            $table->timestamps();

            $table->foreign('visual_quality_port_id')->references('id')->on('visual_quality_port')->onDelete('cascade');
            $table->foreign('periode_visual_quality_port_id')->references('id')->on('periode_visual_quality_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqp_steel_group');
    }
};
