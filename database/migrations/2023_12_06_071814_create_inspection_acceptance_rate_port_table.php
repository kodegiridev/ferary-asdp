<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_acceptance_rate_port', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visual_quality_port_id')->nullable();
            $table->unsignedBigInteger('sub_visual_quality_port_id')->nullable();
            $table->integer('periode')->nullable();
            $table->integer('jml_undangan')->nullable();
            $table->integer('reinspect')->nullable();
            $table->integer('acc_konsultan')->nullable();
            $table->integer('acc_owner')->nullable();
            $table->integer('acc_both')->nullable();
            $table->text('evidence')->nullable();
            $table->timestamps();

            $table->foreign('visual_quality_port_id', 'visual_quality_id_foreign')->references('id')->on('visual_quality_port')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_acceptance_rate_port');
    }
};
