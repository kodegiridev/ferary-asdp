<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component_standard_parameter_area_params', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_component_standard_parameters_id')->nullable();
            $table->integer('std')->nullable();
            $table->integer('up1')->nullable();
            $table->integer('down1')->nullable();
            $table->integer('color1')->nullable();
            $table->integer('up2')->nullable();
            $table->integer('down2')->nullable();
            $table->integer('color2')->nullable();
            $table->integer('up3')->nullable();
            $table->integer('down3')->nullable();
            $table->integer('color3')->nullable();
            $table->integer('valuation_type')->nullable()->comment('1=increased,2=decreased');
            $table->timestamps();

            // $table->foreign('ercm_component_standard_parameters_id', 'fk_area_to_component_param')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
            $table->foreign('ercm_component_standard_parameters_id', 'fkarea_component_standard_parameters_id')->references('id')->on('ercm_component_standard_parameters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component_standard_parameter_area_params');
    }
};
