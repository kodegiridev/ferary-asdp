<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_pelabuhan_subgroup_standard_parameter_area_param', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_pelabuhan_subgroup_standard_parameter_id')->nullable();
            $table->integer('std')->nullable();
            $table->integer('up1')->nullable();
            $table->integer('down1')->nullable();
            $table->integer('color1')->nullable();
            $table->integer('up2')->nullable();
            $table->integer('down2')->nullable();
            $table->integer('color2')->nullable();
            $table->integer('up3')->nullable();
            $table->integer('down3')->nullable();
            $table->integer('color3')->nullable();
            $table->integer('valuation_type')->nullable()->comment('1=increased,2=decreased');
            $table->timestamps();

            $table->foreign('mst_ercm_pelabuhan_subgroup_standard_parameter_id', 'fk_pelabuhan_area_to_sub_std_param')->references('id')->on('mst_ercm_pelabuhan_subgroup_standard_parameter')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_pelabuhan_subgroup_standard_parameter_area_param');
    }
};
