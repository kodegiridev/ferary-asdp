<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periode_visual_quality_port', function (Blueprint $table) {
            $table->tinyInteger('outstanding')->nullable()->comment('1=Yes,0=No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periode_visual_quality_port', function (Blueprint $table) {
            $table->dropColumn('outstanding');
        });
    }
};
