<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ereport_catatan_revisi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ereport_id');
            $table->unsignedBigInteger('pembuat_id');
            $table->text('catatan');
            $table->string('referensi');
            $table->string('dokumen_tanggapan');
            $table->date('tanggal_pembuatan');
            $table->string('arahan');
            $table->string('dok_hasil_perbaikan')->nullable();
            $table->string('catatan_hasil_perbaikan')->nullable();
            $table->date('tgl_hasil_perbaikan')->nullable();
            $table->text('keterangan_penolakan')->nullable();
            $table->integer('status_perbaikan')->nullable()->default(0);
            $table->foreign('ereport_id')
                ->references('id')->on('e_report')->onDelete('cascade');
            $table->foreign('pembuat_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ereport_catatan_revisi');
    }
};
