<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_component_task_lists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_components_id');
            $table->string('task_list');
            $table->date('due_date');
            $table->timestamps();

            $table->foreign('ercm_components_id')->references('id')->on('ercm_components')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_component_task_lists');
    }
};
