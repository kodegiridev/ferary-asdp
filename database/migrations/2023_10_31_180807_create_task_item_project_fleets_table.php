<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_item_project_fleets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sub_item_id');
            $table->string('task');
            $table->double('bobot');
            $table->double('plan');
            $table->double('progres');
            $table->timestamps();

            $table->foreign('sub_item_id')->references('id')->on('sub_item_project_fleets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_item_project_fleets');
    }
};
