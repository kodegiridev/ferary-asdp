<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vqf_hc_production_work', function (Blueprint $table) {
            $table->boolean('status_ident_material')->after('evidence')->nullable();
            $table->text('catatan_ident_material')->after('status_scantiling_check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vqf_hc_production_work', function (Blueprint $table) {
            $table->dropColumn('status_ident_material');
            $table->dropColumn('catatan_ident_material');
        });
    }
};
