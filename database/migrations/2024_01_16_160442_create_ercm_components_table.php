<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_components', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_subgroup_id');
            $table->string('component3d_id');
            $table->string('component');
            $table->string('color')->nullable();
            $table->string('measurement')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('classification')->nullable();
            $table->integer('measurement_score')->nullable();
            $table->integer('measurement_status')->nullable();
            $table->integer('repair_status')->nullable();
            $table->integer('postpone_status')->nullable();
            $table->integer('approval_status')->nullable();
            $table->integer('stage_type')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('ercm_subgroup_id')->references('id')->on('ercm_subgroups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_components');
    }
};
