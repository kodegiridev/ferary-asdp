<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_machinery', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visual_quality_fleet_id');
            $table->unsignedBigInteger('periode_visual_quality_id');
            $table->string('kode');
            $table->string('peralatan');
            $table->string('evidence');
            $table->integer('status_plan');
            $table->integer('status_inspeksi');
            $table->integer('status_state');
            $table->string('jenis_proyek');
            $table->timestamps();

            $table->foreign('visual_quality_fleet_id')->references('id')->on('visual_quality_fleet')->onDelete('cascade');
            $table->foreign('periode_visual_quality_id')->references('id')->on('periode_visual_quality_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_machinery');
    }
};
