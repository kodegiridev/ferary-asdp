<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_notice_recomendation_fleet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('periode_id')->nullable();
            $table->string('notice')->nullable();
            $table->text('to_do_list')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->string('status', 10)->nullable();
            $table->unsignedBigInteger('pic')->nullable();
            $table->text('file')->nullable();
            $table->timestamps();

            $table->foreign('pic')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('periode_id')->references('id')->on('data_periode_recomendation_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_notice_recomendation_fleet');
    }
};
