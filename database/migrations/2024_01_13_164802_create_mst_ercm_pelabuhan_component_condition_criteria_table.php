<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_pelabuhan_component_condition_criteria', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_pelabuhan_component_id')->nullable();
            $table->tinyInteger('up1')->nullable();
            $table->tinyInteger('down1')->nullable();
            $table->tinyInteger('status1')->nullable();
            $table->tinyInteger('up2')->nullable();
            $table->tinyInteger('down2')->nullable();
            $table->tinyInteger('status2')->nullable();
            $table->tinyInteger('up3')->nullable();
            $table->tinyInteger('down3')->nullable();
            $table->tinyInteger('status3')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('mst_ercm_pelabuhan_component_id', 'fk_pelabuhan_condition_criteria_to_component')->references('id')->on('mst_ercm_pelabuhan_component')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_pelabuhan_component_condition_criteria');
    }
};
