<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drawing_visual_quality_fleet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->string('drawing');
            $table->string('deskripsi');
            $table->date('tanggal');
            $table->integer('jenis_drawing');
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('project_monitoring_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drawing_visual_quality_fleet');
    }
};
