<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_technical_quality_standard', function (Blueprint $table) {
            $table->id();
            $table->string('nama_dokumen');
            $table->text('keterangan')->nullable();
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_technical_quality_standard');
    }
};
