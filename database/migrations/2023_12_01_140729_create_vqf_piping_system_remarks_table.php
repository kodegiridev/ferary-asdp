<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vqf_piping_system_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vqf_piping_system_id');
            $table->string('remarks');
            $table->integer('status');
            $table->integer('status_inspeksi');
            $table->text('catatan_inspeksi');
            $table->date('tanggal');
            $table->timestamps();

            $table->foreign('vqf_piping_system_id')->references('id')->on('vqf_piping_system')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vqf_piping_system_remarks');
    }
};
