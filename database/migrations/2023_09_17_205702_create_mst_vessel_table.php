<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_vessel', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('regional_id');
            $table->unsignedBigInteger('cabang_id');
            $table->unsignedBigInteger('lintasan_id');
            $table->string('nama');
            $table->integer('grt');
            $table->integer('panjang');
            $table->integer('daya');
            $table->string('satuan');
            $table->date('usia_teknis');
            $table->string('status_survey');
            $table->integer('pic');
            $table->foreign('regional_id')
                ->references('id')->on('mst_regional')->onDelete('cascade');
            $table->foreign('cabang_id')
                ->references('id')->on('mst_cabang')->onDelete('cascade');
            $table->foreign('lintasan_id')
                ->references('id')->on('mst_lintasan')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_vessel');
    }
};
