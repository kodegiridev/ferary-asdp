<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ereport_tanggapan_penugasan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ereport_id');
            $table->unsignedBigInteger('pembuat_id');
            $table->text('keterangan');
            $table->string('status');
            $table->foreign('ereport_id')
                ->references('id')->on('e_report')->onDelete('cascade');
            $table->foreign('pembuat_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ereport_tanggapan_penugasan');
    }
};
