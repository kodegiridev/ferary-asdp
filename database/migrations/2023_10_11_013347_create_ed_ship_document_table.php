<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_ship_document', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vessel_id');
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->string('nama_dokumen');
            $table->string('nomor_dokumen');
            $table->date('tgl_dokumen');
            $table->string('instansi');
            $table->string('keterangan');
            $table->timestamps();
            $table->foreign('vessel_id')
                ->references('id')->on('mst_vessel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_ship_document');
    }
};
