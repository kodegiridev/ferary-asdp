<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_document_of_quality_file', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('document_of_quality_id');
            $table->string('file');
            $table->timestamps();
            $table->foreign('document_of_quality_id')->references('id')->on('ed_document_of_quality')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_document_of_quality_file');
    }
};
