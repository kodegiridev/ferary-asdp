<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ercm_groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ercm_system_id');
            $table->string('nama');
            $table->integer('value');
            $table->string('attachment')->nullable();
            $table->integer('before_score')->nullable();
            $table->integer('after_score')->nullable();
            $table->integer('delta_score')->nullable();
            $table->integer('repair_ticket_execute')->nullable();
            $table->integer('repair_ticket_accept')->nullable();
            $table->integer('repair_ticket_reject')->nullable();
            $table->integer('repair_ticket_postpone')->nullable();
            $table->integer('status');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('ercm_system_id')->references('id')->on('ercm_systems')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ercm_groups');
    }
};
