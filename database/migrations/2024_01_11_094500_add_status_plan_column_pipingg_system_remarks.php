<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vqf_piping_system_remarks', function (Blueprint $table) {
            $table->integer('status_plan')->after('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vqf_piping_system_remarks', function (Blueprint $table) {
            $table->dropColumn('status_plan');
        });
    }
};
