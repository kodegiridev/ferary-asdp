<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('ed_port_drawing', function (Blueprint $table) {
            $table->dropForeign(['pelabuhan_id']);
            $table->dropColumn('pelabuhan_id');
            $table->unsignedBigInteger('dermaga_id')->after('id');
            $table->foreign('dermaga_id')->references('id')->on('mst_dermaga')->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('ed_port_drawing', function (Blueprint $table) {
            $table->dropForeign(['dermaga_id']);
            $table->dropColumn('dermaga_id');
            $table->unsignedBigInteger('pelabuhan_id')->after('id');
            $table->foreign('pelabuhan_id')->references('id')->on('mst_pelabuhan')->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
};
