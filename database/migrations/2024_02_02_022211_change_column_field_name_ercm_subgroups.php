<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ercm_subgroups', function (Blueprint $table) {
            $table->renameColumn('ercm_group_id', 'ercm_groups_id');
            $table->renameColumn('nama', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ercm_subgroups', function (Blueprint $table) {
            $table->renameColumn('ercm_groups_id', 'ercm_group_id');
            $table->renameColumn('nama', 'name');
        });
    }
};
