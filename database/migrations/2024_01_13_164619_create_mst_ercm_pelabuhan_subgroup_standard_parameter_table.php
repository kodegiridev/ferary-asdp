<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_pelabuhan_subgroup_standard_parameter', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_pelabuhan_subgroup_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('value')->nullable();
            $table->tinyInteger('assestment_type')->nullable()->comment('1=initial,2=before,3=after');
            $table->tinyInteger('order_type')->nullable()->comment('1=area,2=number,3=ranges,4=approval');
            $table->tinyInteger('type_stage')->nullable()->comment('1=initial,2=before');
            $table->tinyInteger('parameter_type')->nullable()->comment('1=kell,2=shell,3=plate');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('mst_ercm_pelabuhan_subgroup_id', 'fk_pelabuhan_std_param_to_sub_group')->references('id')->on('mst_ercm_pelabuhan_subgroup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_pelabuhan_subgroup_standard_parameter');
    }
};
