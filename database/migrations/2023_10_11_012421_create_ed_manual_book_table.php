<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ed_manual_book', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vessel_id');
            $table->unsignedBigInteger('approval_id')->nullable();
            $table->string('nama_dokumen');
            $table->string('komponen')->nullable();
            $table->string('merek')->nullable();
            $table->string('tipe_komponen')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->foreign('vessel_id')
                ->references('id')->on('mst_vessel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ed_manual_book');
    }
};
