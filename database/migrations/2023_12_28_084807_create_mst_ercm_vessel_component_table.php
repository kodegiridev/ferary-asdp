<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_ercm_vessel_component', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mst_ercm_vessel_group_id')->nullable();
            $table->unsignedBigInteger('mst_ercm_vessel_subgroup_id')->nullable();
            $table->string('component3d_id')->nullable();
            $table->string('component')->nullable();
            $table->string('color', 50)->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('mst_ercm_vessel_subgroup_id')->references('id')->on('mst_ercm_vessel_subgroup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_ercm_vessel_component');
    }
};
