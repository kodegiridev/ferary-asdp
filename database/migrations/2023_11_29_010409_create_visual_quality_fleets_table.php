<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visual_quality_fleet', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->string('status_grup')->nullable();
            $table->string('nama_grup');
            $table->integer('bobot')->nullable();
            $table->string('jenis_project')->nullable();
            $table->integer('jenis_form')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('project_monitoring_fleet')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visual_quality_fleet');
    }
};
