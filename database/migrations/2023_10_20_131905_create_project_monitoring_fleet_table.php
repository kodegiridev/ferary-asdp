<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_monitoring_fleet', function (Blueprint $table) {
            $table->id();
            $table->string('kontraktor', 100);
            $table->string('nama_project', 100);
            $table->integer('nilai_project')->nullable()->default(0);
            $table->string('jenis_periode')->nullable()->comment('hari|minggu|bulan');
            $table->string('nomor_kontrak', 50)->nullable();
            $table->string('supervisor', 50)->nullable();
            $table->unsignedBigInteger('pic')->nullable()->comment('role staff');
            $table->tinyInteger('status')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('pic')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_monitoring_fleet');
    }
};
