<?php

namespace Database\Seeders;

use App\Models\Fleet\Fleet;
use App\Models\Fleet\Route;
use App\Models\Fleet\Track;
use App\Models\Fleet\TypeOfTrack;
use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\Region;
use App\Models\Port\Port;
use Faker\Generator;
use Illuminate\Database\Seeder;

class FleetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        $demoRegion = [
            [
                'nama'      => 'Regional I',
                'kategori'  => 2
            ],
            [
                'nama'      => 'Regional II',
                'kategori'  => 2
            ],
            [
                'nama'      => 'Regional III',
                'kategori'  => 2
            ],
            [
                'nama'      => 'Regional IV',
                'kategori'  => 2
            ],
        ];

        Region::insert($demoRegion);

        $demoCabang = [
            [
                'regional_id'   => 1,
                'nama'          => 'Banda Aceh',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 1,
                'nama'          => 'Bangka',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 1,
                'nama'          => 'Danau Toba',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 2,
                'nama'          => 'Balikpapan',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 2,
                'nama'          => 'Pontianak',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 2,
                'nama'          => 'Merak',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 3,
                'nama'          => 'Kayangan',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 3,
                'nama'          => 'Kupang',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 3,
                'nama'          => 'Sape',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 4,
                'nama'          => 'Ambon',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 4,
                'nama'          => 'Sorong',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 4,
                'nama'          => 'Ternate',
                'cabang_kelas'  => 'Kelas A'
            ],
            [
                'regional_id'   => 4,
                'nama'          => 'Luwuk',
                'cabang_kelas'  => 'Kelas A'
            ],
        ];

        Branch::insert($demoCabang);

        $demoRute = [
            [
                'nama'  => 'Rute I'
            ],
            [
                'nama'  => 'Rute II'
            ],
            [
                'nama'  => 'Rute III'
            ],
        ];

        Route::insert($demoRute);

        $demoTipeLintasan = [
            [
                'nama'  => 'Tipe Lintasan I'
            ],
            [
                'nama'  => 'Tipe Lintasan II'
            ],
            [
                'nama'  => 'Tipe Lintasan III'
            ],
        ];

        TypeOfTrack::insert($demoTipeLintasan);

        $demoPelabuhan = [
            [
                'cabang_id' => 1,
                'nama'      => 'Pelabuhan I',
                'pic'       => 3
            ],
            [
                'cabang_id' => 2,
                'nama'      => 'Pelabuhan II',
                'pic'       => 3
            ],
            [
                'cabang_id' => 3,
                'nama'      => 'Pelabuhan III',
                'pic'       => 3
            ],
        ];

        Port::insert($demoPelabuhan);

        $demoLintasan = [
            [
                'pelabuhan_id'      => 1,
                'rute_id'           => 1,
                'tipe_lintasan_id'  => 1,
                'nama'              => 'Komersil'
            ],
            [
                'pelabuhan_id'      => 2,
                'rute_id'           => 2,
                'tipe_lintasan_id'  => 2,
                'nama'              => 'Perintis'
            ],
            [
                'pelabuhan_id'      => 3,
                'rute_id'           => 3,
                'tipe_lintasan_id'  => 3,
                'nama'              => 'Eksekutif'
            ],
        ];

        Track::insert($demoLintasan);

        $demoFleet = [
            [
                'regional_id'   => 1,
                'cabang_id'     => 1,
                'lintasan_id'   => 1,
                'nama'          => 'KPM. Papuyu',
                'grt'           => 284,
                'panjang'       => 31,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 1,
                'cabang_id'     => 2,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Belanak',
                'grt'           => 1163,
                'panjang'       => 71,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 1,
                'cabang_id'     => 3,
                'lintasan_id'   => 2,
                'nama'          => 'KMP. Pora-Pora',
                'grt'           => 462,
                'panjang'       => 60,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 2,
                'cabang_id'     => 4,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Manta',
                'grt'           => 627,
                'panjang'       => 61,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 2,
                'cabang_id'     => 5,
                'lintasan_id'   => 2,
                'nama'          => 'KMP. Semah',
                'grt'           => 227,
                'panjang'       => 51,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 2,
                'cabang_id'     => 6,
                'lintasan_id'   => 3,
                'nama'          => 'KMP. Portlink III',
                'grt'           => 11000,
                'panjang'       => 118,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 3,
                'cabang_id'     => 7,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Raja Enggano',
                'grt'           => 783,
                'panjang'       => 65,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 3,
                'cabang_id'     => 8,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Jatra I',
                'grt'           => 3871,
                'panjang'       => 91,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 3,
                'cabang_id'     => 9,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Cakalang',
                'grt'           => 1483,
                'panjang'       => 78,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 4,
                'cabang_id'     => 10,
                'lintasan_id'   => 1,
                'nama'          => 'KMP. Wayangan',
                'grt'           => 1029,
                'panjang'       => 69,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 4,
                'cabang_id'     => 11,
                'lintasan_id'   => 2,
                'nama'          => 'KMP. Kalabia',
                'grt'           => 1167,
                'panjang'       => 68,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 4,
                'cabang_id'     => 12,
                'lintasan_id'   => 2,
                'nama'          => 'KMP. Gorango',
                'grt'           => 617,
                'panjang'       => 55,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
            [
                'regional_id'   => 4,
                'cabang_id'     => 13,
                'lintasan_id'   => 2,
                'nama'          => 'KMP. Teluk Tolo',
                'grt'           => 510,
                'panjang'       => 45,
                'daya'          => 1,
                'satuan'        => 'KW',
                'usia_teknis'   => '2023-10-02',
                'status_survey' => 'AS1',
                'pic'           => 3
            ],
        ];

        Fleet::insert($demoFleet);
    }
}
