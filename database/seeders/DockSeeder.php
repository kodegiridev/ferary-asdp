<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Port\Dock;
use App\Models\Port\Port;

class DockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ports = Port::orderBy('nama', 'asc')->get();
        $dock_type = [
            'Ponton',
            'Quay Wall',
            'System Jetty',
            'Dolphin'
        ];

        if ($ports) {
            foreach ($ports as $port) {
                $params = [
                    [
                        'pelabuhan_id' => $port->id,
                        'tipe' => $dock_type[array_rand($dock_type)],
                        'nama' => 'Dermaga 1 - '.$port->nama,
                        'kapasitas' => rand(10, 50)
                    ],
                    [
                        'pelabuhan_id' => $port->id,
                        'tipe' => $dock_type[array_rand($dock_type)],
                        'nama' => 'Dermaga 2 - '.$port->nama,
                        'kapasitas' => rand(10, 50)
                    ]
                ];
                Dock::insert($params);
            }
        }
    }
}
