<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\EngineeringDatabase\Port\PortSpecification;
use App\Models\Port\Dock;

class PortSpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dermaga = Dock::orderBy('id', 'ASC')->get();

        if ($dermaga) {
            $parameters = [
                'Parameter 1',
                'Parameter 2',
                'Parameter 3'
            ];

            foreach ($dermaga as $item) {
                $params = [
                    'dermaga_id' => $item->id,
                    'nama_komponen' => 'Lampu '.$item->nama,
                    'sub_komponen' => 'Tiang '.$item->nama,
                    'parameter' => $parameters[array_rand($parameters)],
                    'spesifikasi' => 'Spesifikasi lampu '.$item->nama,
                    'referensi' => 'Lorem ipsum '.$item->nama,
                    'keterangan' => '',
                    'created_at' => date('Y-m-d H:i:s')
                ];

                PortSpecification::insert($params);
            }
        }
    }
}
