<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PortPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->data();
        DB::table('permissions')->insert($data);
    }

    public function data()
    {
        $data = [];
        // list of model permission
        $model = ['port', 'port regional', 'port branch', 'port sub branch', 'port dock'];

        foreach ($model as $value) {
            foreach ($this->crudActions($value) as $action) {
                $data[] = $action;
            }
        }

        return $data;
    }

    public function crudActions($name)
    {
        $actions = [];
        // list of permission actions
        $crud = ['create', 'read', 'update', 'delete'];

        foreach ($crud as $value) {
            $actions[] = ['name' => $value.'_'.str_replace(" ", "_", $name), 'menu_name' => $name, 'action' => $value];
        }

        return $actions;
    }
}
