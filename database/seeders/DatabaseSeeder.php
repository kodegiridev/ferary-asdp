<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            FleetSeeder::class,
            DockSeeder::class,
            PortDrawingSeeder::class,
            PortSpecificationSeeder::class,
            DocumentOfQualitySeeder::class,
            // PermissionsSeeder::class,
            // RolesSeeder::class,
            QualitySubProject::class
        ]);
    }
}
