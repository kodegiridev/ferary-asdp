<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\EngineeringDatabase\Port\PortDrawing;
use App\Models\Port\Dock;

class PortDrawingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dermaga = Dock::orderBy('id', 'ASC')->get();

        if ($dermaga) {
            $status_gambar = [
                'belum disetujui',
                'disetujui'
            ];
            $tipe_gambar = [
                'shop drawing',
                'approval drawing',
                'as build drawing'
            ];

            foreach ($dermaga as $item) {
                $params = [
                    'dermaga_id' => $item->id,
                    'nama_komponen' => 'Tiang '.$item->nama,
                    'nama_gambar' => 'Premium',
                    'nomor_gambar' => random_int(10000, 99999),
                    'tipe_gambar' => $tipe_gambar[array_rand($tipe_gambar)],
                    'status_gambar' => $status_gambar[array_rand($status_gambar)],
                    'keterangan' => 'As Built Drawing',
                    'created_at' => date('Y-m-d H:i:s')
                ];

                PortDrawing::insert($params);
            }
        }
    }
}
