<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Position;
use App\Models\Role;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        $demoPermission = [
            [
                'name'              => 'create_user',
                'menu_name'         => 'user',
                'action'            => 'create'
            ],
            [
                'name'              => 'create_role',
                'menu_name'         => 'role',
                'action'            => 'create'
            ],
            [
                'name'              => 'create_permission',
                'menu_name'         => 'permission',
                'action'            => 'create'
            ]
        ];

        Permission::insert($demoPermission);

        $demoRole = [
            [
                'name'  => 'admin'
            ],
            [
                'name'  => 'Vice President'
            ],
            [
                'name'  => 'Manager Mutu Armada & Pelabuhan'
            ],
            [
                'name'  => 'Manager Asset Produksi'
            ],
            [
                'name'  => 'Staff Mutu Pelabuhan 1'
            ],
            [
                'name'  => 'Staff Mutu Pelabuhan 2'
            ],
            [
                'name'  => 'Staff Mutu Pelabuhan 3'
            ],
            [
                'name'  => 'Staff Mutu Armada 1'
            ],
            [
                'name'  => 'Staff Mutu Armada 2'
            ],
            [
                'name'  => 'Staff Mutu Armada 3'
            ],
            [
                'name'  => 'Staff Asset Produksi 1'
            ],
            [
                'name'  => 'Staff Asset Produksi 2'
            ],
            [
                'name'  => 'Staff Asset Produksi 3'
            ],
        ];

        Role::insert($demoRole);

        $demoUser = [
            [
                'name'              => $faker->name,
                'email'             => 'admin@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'admin',
                'role_id'           => 1,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => $faker->name,
                'email'             => 'admin2@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'admin2',
                'role_id'           => 1,
                'is_active'         => 0,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Andre Soetresno',
                'email'             => 'andre@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'andre',
                'role_id'           => 2,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Rieska Firdausy Laily',
                'email'             => 'rieska@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'rieska',
                'role_id'           => 3,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Annisa Karismatika Sariang',
                'email'             => 'annisa@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'annisa',
                'role_id'           => 4,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Ghea Sanyanan S.',
                'email'             => 'ghea@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'ghea',
                'role_id'           => 5,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Aisyah Sugiastu Putri',
                'email'             => 'aisyah@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'aisyah',
                'role_id'           => 6,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Muchammat Alvinur',
                'email'             => 'muchammat@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'muchammat',
                'role_id'           => 7,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Rindu Fajar Kusuma',
                'email'             => 'rindu@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'rindu',
                'role_id'           => 8,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Rachmat Hidayat',
                'email'             => 'rachmat@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'rachmat',
                'role_id'           => 9,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Abi Rachman P.',
                'email'             => 'abi@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'abi',
                'role_id'           => 10,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Tiara Talentia',
                'email'             => 'tiara@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'tiara',
                'role_id'           => 11,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Tezar Pratama',
                'email'             => 'tezar@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'tezar',
                'role_id'           => 12,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'VP',
                'email'             => 'vp@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'vp',
                'role_id'           => 2,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Manager',
                'email'             => 'manager@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'manager',
                'role_id'           => 3,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
            [
                'name'              => 'Staff',
                'email'             => 'staff@demo.com',
                'password'          => Hash::make('admin'),
                'username'          => 'staff',
                'role_id'           => 5,
                'is_active'         => 1,
                'created_by'        => 1,
                'updated_by'        => 1
            ],
        ];

        User::insert($demoUser);
    }
}
