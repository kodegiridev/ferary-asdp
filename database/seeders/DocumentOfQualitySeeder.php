<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\EngineeringDatabase\Port\DocumentOfQuality;
use App\Models\Port\Dock;

class DocumentOfQualitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dermaga = Dock::orderBy('id', 'ASC')->get();

        if ($dermaga) {
            foreach ($dermaga as $item) {
                $params = [
                    'dermaga_id' => $item->id,
                    'nama_dokumen' => 'Dokumen '.$item->nama,
                    'nama_komponen' => 'Komponen '.$item->nama,
                    'keterangan' => '',
                    'created_at' => date('Y-m-d H:i:s')
                ];

                DocumentOfQuality::insert($params);
            }
        }
    }
}
