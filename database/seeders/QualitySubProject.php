<?php

namespace Database\Seeders;

use App\Models\MasterQuality;
use Illuminate\Database\Seeder;

class QualitySubProject extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterQuality::insert([
            [
                'name' => 'Blasting & Painting (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfBlastingPainting'
            ],
            [
                'name' => 'Commisioning & Function Test',
                'jenis_project' => 'nb',
                'model' => 'VqfComissionFunction'
            ],
            [
                'name' => 'Deck Machinery (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfDeckMachinery'
            ],
            [
                'name' => 'Electrical (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfElectrical'
            ],
            [
                'name' => 'Electrical Outfiting (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfElectricalOutfiting'
            ],
            [
                'name' => 'Facility (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfFacility'
            ],
            [
                'name' => 'Hull Construction Production Work',
                'jenis_project' => 'nb',
                'model' => 'VqfHcProductionWork'
            ],
            [
                'name' => 'Hull Construction',
                'jenis_project' => 'nb',
                'model' => 'VqfHullConstruction'
            ],
            [
                'name' => 'Hull Outfiting',
                'jenis_project' => 'nb',
                'model' => 'VqfHullOutfiting'
            ],
            [
                'name' => 'Insulation (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfInsulation'
            ],
            [
                'name' => 'Life Saving Appliances (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfIsAppliances'
            ],
            [
                'name' => 'Machinery',
                'jenis_project' => 'nb',
                'model' => 'VqfMachinery'
            ],
            [
                'name' => 'Machinery Outfiting (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfMachineryOutfiting'
            ],
            [
                'name' => 'Navigation Communication Equipment (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfNcEquipment'
            ],
            [
                'name' => 'Non Destructive Test',
                'jenis_project' => 'nb',
                'model' => 'VqfNonDestructive'
            ],
            [
                'name' => 'Piping System (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfPipingSystem'
            ],
            [
                'name' => 'Propulsion (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfPropultionNb'
            ],
            [
                'name' => 'Tank Test (New Building)',
                'jenis_project' => 'nb',
                'model' => 'VqfTank'
            ],
            [
                'name' => 'Blasting & Painting',
                'jenis_project' => 'rp',
                'model' => 'VqfBlastingPainting'
            ],
            [
                'name' => 'Commisioning & Function Test',
                'jenis_project' => 'rp',
                'model' => 'VqfComissionFunction'
            ],
            [
                'name' => 'Deck Machinery',
                'jenis_project' => 'rp',
                'model' => 'VqfDeckMachinery'
            ],
            [
                'name' => 'Electrical',
                'jenis_project' => 'rp',
                'model' => 'VqfElectrical'
            ],
            [
                'name' => 'Electrical Outfiting',
                'jenis_project' => 'rp',
                'model' => 'VqfElectricalOutfiting'
            ],
            [
                'name' => 'Facility',
                'jenis_project' => 'rp',
                'model' => 'VqfFacility'
            ],
            [
                'name' => 'Hull Construction Production Work',
                'jenis_project' => 'rp',
                'model' => 'VqfHcProductionWork'
            ],
            [
                'name' => 'Hull Construction',
                'jenis_project' => 'rp',
                'model' => 'VqfHullConstruction'
            ],
            [
                'name' => 'Hull Outfiting',
                'jenis_project' => 'rp',
                'model' => 'VqfHullOutfiting'
            ],
            [
                'name' => 'Hull Construction Repair Work',
                'jenis_project' => 'rp',
                'model' => 'VqfHcRepairWork'
            ],
            [
                'name' => 'Insulation',
                'jenis_project' => 'rp',
                'model' => 'VqfInsulation'
            ],
            [
                'name' => 'Life Saving Appliances',
                'jenis_project' => 'rp',
                'model' => 'VqfIsAppliances'
            ],
            [
                'name' => 'Machinery',
                'jenis_project' => 'rp',
                'model' => 'VqfMachinery'
            ],
            [
                'name' => 'Machinery Outfiting',
                'jenis_project' => 'rp',
                'model' => 'VqfMachineryOutfiting'
            ],
            [
                'name' => 'Navigation Communication Equipment',
                'jenis_project' => 'rp',
                'model' => 'VqfNcEquipment'
            ],
            [
                'name' => 'Non Destructive Test',
                'jenis_project' => 'rp',
                'model' => 'VqfNonDestructive'
            ],
            [
                'name' => 'Piping System',
                'jenis_project' => 'rp',
                'model' => 'VqfPipingSystem'
            ],
            [
                'name' => 'Propulsion',
                'jenis_project' => 'rp',
                'model' => 'VqfPropultion'
            ],
            [
                'name' => 'Tank Test',
                'jenis_project' => 'rp',
                'model' => 'VqfTank'
            ]
        ]);
    }
}
