<!--begin::App settings toggle-->
<button id="kt_app_layout_builder_toggle" class="btn btn-dark app-layout-builder-toggle">
    {!! theme()->getSvgIcon('icons/duotune/coding/cod009.svg', 'svg-icon-3') !!}
    Customize
</button>
<!--end::App settings toggle-->
