<div class="modal fade" tabindex="-1" id="create_role_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Role') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('roles.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('name') is-invalid @enderror"
                                placeholder="Ketik disini" name="name" value="{{ old('name') }}"/>

                                @error('name')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="permission_id" class="required form-label">
                                {{ __('Permission') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive @error('permission_id') is-invalid @enderror">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="fw-bold fs-6 text-gray-800">
                                                <th>{{ __('No') }}</th>
                                                <th>
                                                    <div class="form-check">
                                                        <input
                                                        class="form-check-input check-all"
                                                        type="checkbox"/>
                                                    </div>
                                                </th>
                                                <th>
                                                    {{ __('Judul Menu') }}
                                                </th>
                                                @foreach ($actions as $action)
                                                    <th>
                                                        <div class="form-check">
                                                            <input
                                                            class="form-check-input check-action"
                                                            type="checkbox"
                                                            data-action="{{ $action }}"/>
                                                            {{ $action }}
                                                        </div>
                                                    </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($menus as $menu)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>
                                                        <div class="form-check">
                                                            <input
                                                            class="form-check-input check-menu"
                                                            type="checkbox"
                                                            data-menu="{{ $menu }}"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{ $menu }}
                                                    </td>
                                                    @foreach ($permissions->where('menu_name', $menu) as $permission)
                                                        <td>
                                                            <div class="form-check">
                                                                <input
                                                                class="form-check-input check-permission"
                                                                type="checkbox"
                                                                name="permission_id[{{ $permission->id }}]"
                                                                data-id="{{ $permission->id }}"
                                                                data-action="{{ $permission->action }}"
                                                                data-menu="{{ $permission->menu_name }}"/>
                                                            </div>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                @error('permission_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
