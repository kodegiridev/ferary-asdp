@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data Role') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Role') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Role') }}</h3>
                <div class="card-toolbar">
                    <button type="button"
                    id="btn_create_role_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_role_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    <button type="button"
                    id="btn_edit_role_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_role_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="role_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.user-management.role.modals.create')
@include('pages.user-management.role.modals.edit')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_role_modal').trigger('click');
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#role_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('roles.index') }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $(document).on('click', '.btn-edit-role', function () {
            var url = `{{ route('roles.show', ':role') }}`;
            url = url.replace(':role', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var action = `{{ route('roles.update', ':role') }}`;
                    action = action.replace(':role', data.role.id);

                    $('#edit_role_modal form').prop('action', action);

                    $('#edit_role_modal input[name="name"]').val(data.role.name);

                    $('#edit_role_modal .check-permission').each(function (index, element) {
                        if ($.inArray($(this).data('id'), data.permissions) >= 0) {
                            $(this).prop('checked', true);
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-delete-role', function () {
            var url = `{{ route('roles.destroy', ':role') }}`;
            url = url.replace(':role', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_role_modal', function () {
            $('#create_role_modal input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('#create_role_modal select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });

        $(document).on('change', '.check-all', function () {
            var modal = $(this).closest('.modal');

            if ($(this).prop('checked')) {
                modal.find('input[type="checkbox"]').each(function (index, element) {
                    $(this).prop('checked', true);
                });
            }

            if (! $(this).prop('checked')) {
                modal.find('input[type="checkbox"]').each(function (index, element) {
                    $(this).prop('checked', false);
                });
            }
        });

        $(document).on('change', '.check-action', function () {
            var modal = $(this).closest('.modal');

            if ($(this).prop('checked')) {
                modal.find('input[type="checkbox"][data-action="'+$(this).data('action')+'"]')
                .each(function (index, element) {
                    $(this).prop('checked', true);
                });
            }

            if (! $(this).prop('checked')) {
                modal.find('input[type="checkbox"][data-action="'+$(this).data('action')+'"]')
                .each(function (index, element) {
                    $(this).prop('checked', false);
                });
            }
        });

        $(document).on('change', '.check-menu', function () {
            var modal = $(this).closest('.modal');

            if ($(this).prop('checked')) {
                modal.find('input[type="checkbox"][data-menu="'+$(this).data('menu')+'"]')
                .each(function (index, element) {
                    $(this).prop('checked', true);
                });
            }

            if (! $(this).prop('checked')) {
                modal.find('input[type="checkbox"][data-menu="'+$(this).data('menu')+'"]')
                .each(function (index, element) {
                    $(this).prop('checked', false);
                });
            }
        });

        $(document).on('change', '.check-permission', function () {
            var modal = $(this).closest('.modal');
            var menu = $(this).data('menu');
            var action = $(this).data('action');

            var amountNotCheckedByMenu = modal.find('.check-permission[data-menu="'+menu+'"]')
                .not(':checked')
                .length;
            var amountNotCheckedByAction = modal.find('.check-permission[data-action="'+action+'"]')
                .not(':checked')
                .length;
            var amountNotCheckedTotal = modal.find('.check-permission')
                .not(':checked')
                .length;

            if (amountNotCheckedByMenu > 0) {
                modal.find('.check-menu[data-menu="'+menu+'"]')
                    .prop('checked', false);
            }

            if (amountNotCheckedByMenu == 0) {
                modal.find('.check-menu[data-menu="'+menu+'"]')
                    .prop('checked', true);
            }

            if (amountNotCheckedByAction > 0) {
                modal.find('.check-action[data-action="'+action+'"]')
                    .prop('checked', false);
            }

            if (amountNotCheckedByAction == 0) {
                modal.find('.check-action[data-action="'+action+'"]')
                    .prop('checked', true);
            }

            if (amountNotCheckedTotal > 0) {
                modal.find('.check-all')
                    .prop('checked', false);
            }

            if (amountNotCheckedTotal == 0) {
                modal.find('.check-all')
                    .prop('checked', true);
            }
        });
    })
</script>
@endsection