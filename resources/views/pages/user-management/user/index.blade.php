@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data User') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('User') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data User') }}</h3>
                <div class="card-toolbar">
                    <button type="button"
                    id="btn_create_user_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_user_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    <button type="button"
                    id="btn_edit_user_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_user_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="user_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama') }}</th>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Role') }}</th>
                                <th>{{ __('Aktif') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.user-management.user.modals.create')
@include('pages.user-management.user.modals.edit')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_user_modal').trigger('click');
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#user_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('users.index') }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'role_id',
                    name: 'role_id'
                },
                {
                    data: 'is_active',
                    name: 'is_active',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $(document).on('click', '.btn-edit-user', function () {
            var url = `{{ route('users.show', ':user') }}`;
            url = url.replace(':user', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var action = `{{ route('users.update', ':user') }}`;
                    action = action.replace(':user', data.id);

                    $('#edit_user_modal form').prop('action', action);

                    $('#edit_user_modal input[name="name"]').val(data.name);
                    $('#edit_user_modal input[name="username"]').val(data.username);
                    $('#edit_user_modal input[name="email"]').val(data.email);
                    $('#edit_user_modal select[name="role_id"]').val(data.role_id);
                    $('#edit_user_modal input[name="is_active"]')
                    .val(data.is_active)
                    .prop('checked', data.is_active ? true : false);

                }
            });
        });

        $(document).on('click', '.btn-delete-user', function () {
            var url = `{{ route('users.destroy', ':user') }}`;
            url = url.replace(':user', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('change', '.is_active', function () {
            var url = `{{ route('users.updateStatus', ':user') }}`;
            url = url.replace(':user', $(this).data('id'));

            $.ajax({
                url: url,
                type: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    "is_active": $(this).prop('checked') ? 1 : 0,
                },
                success: function () {
                    location.reload();
                }
            });
        });

        $(document).on('change', '#edit_user_modal input[name="is_active"]', function () {
            $(this).prop('checked') ? $(this).val(1) : $(this).val(0);
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_user_modal', function () {
            $('#create_user_modal input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('#create_user_modal select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });
    })
</script>
@endsection