<div class="modal fade" tabindex="-1" id="create_user_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah User') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('name') is-invalid @enderror"
                                placeholder="Ketik disini" name="name" value="{{ old('name') }}"/>

                                @error('name')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="username" class="required form-label">
                                {{ __('Username') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('username') is-invalid @enderror"
                                placeholder="Ketik disini" name="username" value="{{ old('username') }}"/>

                                @error('username')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="email" class="required form-label">
                                {{ __('Email') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="email"
                                class="form-control form-control-solid @error('email') is-invalid @enderror"
                                placeholder="Ketik disini" name="email" value="{{ old('email') }}"/>

                                @error('email')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="password" class="required form-label">
                                {{ __('Kata Sandi') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="password"
                                class="form-control form-control-solid @error('password') is-invalid @enderror"
                                placeholder="Ketik disini" name="password" value="{{ old('password') }}"/>

                                @error('password')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="role_id" class="required form-label">
                                {{ __('Role') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="role_id"
                                class="form-select form-select-solid @error('role_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}"
                                        {{ old('role_id') == $role->id ? 'selected' : '' }}>
                                            {{ $role->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('role_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
