<div class="modal fade" tabindex="-1" id="create_permission_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Permission') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('permissions.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('name') is-invalid @enderror"
                                placeholder="Ketik disini" name="name" value="{{ old('name') }}"/>

                                @error('name')
                                    <span class="invalid-feedback mb-2" permission="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="menu_name" class="required form-label">
                                {{ __('Nama Menu') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="menu_name"
                                class="form-select form-select-solid @error('menu_name') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($listMenu as $menu)
                                        <option value="{{ $menu }}"
                                        {{ old('menu_name') == $menu ? 'selected' : '' }}>
                                            {{ $menu }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('menu_name')
                                    <span class="invalid-feedback mb-2" permission="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="action_permission" class="required form-label">
                                {{ __('Aksi Permission') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="action_permission"
                                class="form-select form-select-solid @error('action_permission') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($listAction as $action)
                                        <option value="{{ $action }}"
                                        {{ old('action_permission') == $action ? 'selected' : '' }}>
                                            {{ $action }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('action_permission')
                                    <span class="invalid-feedback mb-2" permission="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
