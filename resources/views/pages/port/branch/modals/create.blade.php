<div class="modal fade" tabindex="-1" id="create_branch_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Branch') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('port.branch.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama" class="required form-label">
                                {{ __('Nama') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama" value="{{ old('nama') }}"/>

                                @error('nama')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="regional_id" class="required form-label">
                                {{ __('Regional') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="regional_id"
                                class="form-select form-select-solid @error('regional_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($regions as $region)
                                        <option value="{{ $region->id }}"
                                        {{ old('regional_id') == $region->id ? 'selected' : '' }}>
                                            {{ $region->nama }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('regional_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="cabang_kelas" class="required form-label">
                                {{ __('Kelas Cabang') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="cabang_kelas" class="form-select form-select-solid @error('cabang_kelas') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    <option value="Kelas A" {{ old('cabang_kelas') == 'Kelas A' ? 'selected' : '' }}>Kelas A</option>
                                    <option value="Kelas B" {{ old('cabang_kelas') == 'Kelas B' ? 'selected' : '' }}>Kelas B</option>
                                    <option value="Kelas C" {{ old('cabang_kelas') == 'Kelas C' ? 'selected' : '' }}>Kelas C</option>
                                    <option value="Kelas Utama" {{ old('cabang_kelas') == 'Kelas Utama' ? 'selected' : '' }}>Kelas Utama</option>
                                </select>

                                @error('cabang_kelas')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
