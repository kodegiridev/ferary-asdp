@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data Sub Branch') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Port') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Sub Branch') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Sub Branch') }}</h3>
                <div class="card-toolbar">
                    <button type="button"
                    id="btn_create_sub_branch_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_sub_branch_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    <button type="button"
                    id="btn_edit_sub_branch_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_sub_branch_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="sub_branch_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Cabang') }}</th>
                                <th>{{ __('Provinsi') }}</th>
                                <th>{{ __('Kota') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.port.branch_sub.modals.create')
@include('pages.port.branch_sub.modals.edit')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    $("#create_province_id").on('change', function() {
        let province_id = $(this).val();
        $.ajax({
            url: "{{ route('port.ajax_get_city') }}",
            type: 'GET',
            data: {
                id: province_id
            },
            beforeSend: function() {
              $("#create_kota_id").empty();  
              let html = '<option value="">Pilih data</option>';
              $("#create_kota_id").html(html);
            },
            success: function (data) {
                let html = '<option value="">Pilih data</option>';
                if (data) {
                    $.each(data, function (kota_id, kota_name) {
                        html += `<option value="${kota_id}">${kota_name}</option>`;
                    });
                }
                $("#create_kota_id").html(html);
            }
        });
    });

    $("#update_province_id").on('change', function() {
        let province_id = $(this).val();
        $.ajax({
            url: "{{ route('port.ajax_get_city') }}",
            type: 'GET',
            data: {
                id: province_id
            },
            beforeSend: function() {
              $("#update_kota_id").empty();  
            },
            success: function (data) {
                let html = '<option value="">Pilih data</option>';
                let sub_branch_kota_id = $('#edit_sub_branch_modal input[name="kota_id_source"]').val();
                if (data) {
                    $.each(data, function (kota_id, kota_name) {
                        let selected = '';
                        if (kota_id == sub_branch_kota_id) {
                            selected = 'selected';
                        }
                        html += `<option ${selected} value="${kota_id}">${kota_name}</option>`;
                    });
                }
                $("#update_kota_id").html(html);
            }
        });
    });

    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_sub_branch_modal').trigger('click');
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#sub_branch_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('port.sub_branch.index') }}"
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'cabang_id',
                        name: 'cabang_id'
                    },
                    {
                        data: 'province_name',
                        name: 'province_name'
                    },
                    {
                        data: 'city_name',
                        name: 'city_name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });

        $(document).on('click', '.btn-edit-sub-branch', function () {
            var url = `{{ route('port.sub_branch.show', ':sub_branch') }}`;
            url = url.replace(':sub_branch', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var action = `{{ route('port.sub_branch.update', ':sub_branch') }}`;
                    action = action.replace(':sub_branch', data.id);

                    $('#edit_sub_branch_modal form').prop('action', action);
                    $('#edit_sub_branch_modal input[name="kota_id_source"]').val(data.kota_id);
                    $('#edit_sub_branch_modal select[name="cabang_id"]').val(data.cabang_id);
                    $('#edit_sub_branch_modal select[name="province_id"]').val(data.province_id);
                    $('#update_province_id').trigger('change');

                }
            });
        });

        $(document).on('click', '.btn-delete-sub-branch', function () {
            var url = `{{ route('port.sub_branch.destroy', ':sub_branch') }}`;
            url = url.replace(':sub_branch', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_sub_branch_modal', function () {
            $('input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });
    })
</script>
@endsection