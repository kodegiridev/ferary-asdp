<div class="modal fade" tabindex="-1" id="create_sub_branch_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Sub Branch') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('port.sub_branch.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="cabang_id" class="required form-label">
                                {{ __('Cabang') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="cabang_id"
                                class="form-select form-select-solid @error('cabang_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($branch as $b)
                                        <option value="{{ $b->id }}"
                                        {{ old('cabang_id') == $b->id ? 'selected' : '' }}>
                                            {{ $b->nama }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('cabang_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="province_id" class="required form-label">
                                {{ __('Provinsi') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_province_id" name="province_id"
                                class="form-select form-select-solid @error('province_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($provinsi as $p)
                                        <option value="{{ $p->id }}"
                                        {{ old('province_id') == $p->id ? 'selected' : '' }}>
                                            {{ $p->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('province_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="kota_id" class="required form-label">
                                {{ __('Kota') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_kota_id" name="kota_id"
                                class="form-select form-select-solid @error('kota_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                </select>

                                @error('kota_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>