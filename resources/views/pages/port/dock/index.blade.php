@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data Dock') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Port') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Dock') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Dock') }}</h3>
                <div class="card-toolbar">
                    <button type="button"
                    id="btn_create_dock_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_dock_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    <button type="button"
                    id="btn_edit_dock_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_dock_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dock_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Dermaga') }}</th>
                                <th>{{ __('Pelabuhan') }}</th>
                                <th>{{ __('Tipe') }}</th>
                                <th>{{ __('Kapasitas Dermaga (Ton)') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.port.dock.modals.create')
@include('pages.port.dock.modals.edit')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_dock_modal').trigger('click');
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#dock_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('port.dock.index') }}"
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'pelabuhan_id',
                        name: 'pelabuhan_id'
                    },
                    {
                        data: 'tipe',
                        name: 'tipe'
                    },
                    {
                        data: 'kapasitas',
                        name: 'kapasitas'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });

        $(document).on('click', '.btn-edit-dock', function () {
            var url = `{{ route('port.dock.show', ':dock') }}`;
            url = url.replace(':dock', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var action = `{{ route('port.dock.update', ':dock') }}`;
                    action = action.replace(':dock', data.id);

                    $('#edit_dock_modal form').prop('action', action);
                    $('#edit_dock_modal input[name="nama"]').val(data.nama);
                    $('#edit_dock_modal input[name="kapasitas"]').val(data.kapasitas);
                    $('#edit_dock_modal select[name="pelabuhan_id"]').val(data.pelabuhan_id);
                    $('#edit_dock_modal select[name="tipe"]').val(data.tipe);
                }
            });
        });

        $(document).on('click', '.btn-delete-dock', function () {
            var url = `{{ route('port.dock.destroy', ':dock') }}`;
            url = url.replace(':dock', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_dock_modal', function () {
            $('input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });

        $(document).on('keypress', 'input[type="number"]', function (e) {
            if (["e", "E", "+", "-"].includes(e.key)) {
                e.preventDefault();
            }
        });

        $(document).on('paste', 'input[type="number"]', function (e) {
            e.preventDefault();
        });
    })
</script>
@endsection