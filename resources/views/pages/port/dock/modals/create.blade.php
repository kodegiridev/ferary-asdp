<div class="modal fade" tabindex="-1" id="create_dock_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Dermaga') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('port.dock.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama" class="required form-label">
                                {{ __('Nama Dermaga') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama" value="{{ old('nama') }}"/>

                                @error('nama')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="pelabuhan_id" class="required form-label">
                                {{ __('Pelabuhan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="pelabuhan_id"
                                class="form-select form-select-solid @error('pelabuhan_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($port as $p)
                                        <option value="{{ $p->id }}"
                                        {{ old('pelabuhan_id') == $p->id ? 'selected' : '' }}>
                                            {{ $p->nama }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('pelabuhan_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe" class="required form-label">
                                {{ __('Tipe Dermaga') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="tipe"
                                class="form-select form-select-solid @error('tipe') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($dock_type as $type)
                                        <option value="{{ $type }}"
                                        {{ old('tipe') == $type ? 'selected' : '' }}>
                                            {{ $type }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('tipe')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="kapasitas" class="required form-label">
                                {{ __('Kapasitas Dermaga (Ton)') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="number"
                                class="form-control form-control-solid @error('kapasitas') is-invalid @enderror"
                                placeholder="Ketik disini" name="kapasitas" value="{{ old('kapasitas') }}"/>

                                @error('kapasitas')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
