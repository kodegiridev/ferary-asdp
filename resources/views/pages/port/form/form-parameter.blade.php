@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <style>
        .form-radio-button {
            border: 1.75px solid var(--bs-gray-800) !important;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Sub Group') }} - {{ $data['sub_group']['name'] }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('port.port.index') }}" class="text-muted text-hover-primary">
                            {{ __('Port') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('port.form.index', $data['port']['id']) }}" class="text-muted text-hover-primary">
                            {{ __('Form') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        {{ __('Form Parameter') }}
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <form class="form" action="{{ route('port.form.sub-group.update-param', [$data['port']['id'], $data['sub_group']['id']]) }}" method="POST" enctype="multipart/form-data">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Form Parameter') }}</h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-12 mb-10">
                                <div class="card border rounded shadow-sm">
                                    <div class="card-header">
                                        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x my-5 fs-6">
                                            @foreach ($data['assestment_types'] as $label => $value)
                                                <li class="nav-item">
                                                    <a class="nav-link @if ($loop->iteration == 1) active @endif" data-bs-toggle="tab" href="{{ '#tab_' . $label }}">{{ strtoupper($label) }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach ($data['assestment_types'] as $assestmentLabel => $assestmentValue)
                                                <input type="hidden" name="{{ $assestmentLabel }}[assestment_type]" value="{{ $assestmentValue }}">
                                                <div class="tab-pane fade @if ($loop->iteration == 1) show active @endif" id="{{ 'tab_' . $assestmentLabel }}" role="tabpanel">
                                                    @if ($assestmentLabel == 'initial')
                                                        <div class="form-group mb-3">
                                                            <label class="form-label required">{{ __('Stage From') }}</label>
                                                            <div class="row">
                                                                <div class="col-md-6 col-12">
                                                                    <div class="row d-inline-flex">
                                                                        @php
                                                                            $typeStage = old($assestmentLabel)['type_stage'] ?? ($data['forms'][$assestmentLabel]['type_stage'] ?? null);
                                                                        @endphp
                                                                        @foreach ($data['type_stages'] as $label => $value)
                                                                            <div class="col-6">
                                                                                <div class="form-check form-check-custom">
                                                                                    <input class="form-check-input form-radio-button" name="{{ $assestmentLabel }}[type_stage]" type="radio" value="{{ $value }}" @if ($typeStage == $value) checked @endif />
                                                                                    <label class="form-check-label fs-18px text-gray-800">
                                                                                        {{ strtoupper($label) }}
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mb-3">
                                                            <label class="form-label required">{{ __('Standard Parameter') }}</label>
                                                            <div class="row">
                                                                <div class="col-10">
                                                                    <select class="form-select select2-parameter">
                                                                        @foreach ($data['sub_groups'] as $subGroup)
                                                                            <option value="{{ $subGroup->id }}" @if ($subGroup->id == $data['sub_group']['id']) selected @endif>{{ $subGroup->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-2 d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-icon btn-warning" id="btn_copy_existing">
                                                                        <i class="ki-duotone ki-copy fs-2"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="form-group">
                                                        <label class="form-label">{{ __('Order') }}</label>
                                                        <div class="row">
                                                            <div class="col-10">
                                                                <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[order_qty]" placeholder="Masukan jumlah order" />
                                                            </div>
                                                            <div class="col-2 d-flex justify-content-end">
                                                                <a class="btn btn-icon btn-primary btn_add_order" data-type="{{ $assestmentLabel }}">
                                                                    <i class="ki-duotone ki-plus fs-1"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--begin::Repeater-->
                                                    <div id="{{ $assestmentLabel }}_kt_docs_repeater_nested">
                                                        <div class="mt-3" data-repeater-list="{{ $assestmentLabel }}[parameters]">
                                                            @php
                                                                $formParameters = old($assestmentLabel . '.parameters') ?? ($data['forms'][$assestmentLabel]['parameters'] ?? []);
                                                            @endphp
                                                            @if (count($formParameters) > 0)
                                                                @foreach ($formParameters as $formKey => $formValue)
                                                                    <div data-repeater-item @if ($loop->iteration > 1) style @endif>
                                                                        <!--begin::Form Parameter-->
                                                                        <div class="{{ $assestmentLabel . '_parameter' }}">
                                                                            <div class="form-group my-3">
                                                                                <label class="form-label">{{ __('Tipe Order') }}</label>
                                                                                <div class="row">
                                                                                    <div class="col-10">
                                                                                        <select class="form-select select2-order" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_type]" data-type="{{ $assestmentLabel }}" data-placeholder="Pilih tipe order">
                                                                                            <option></option>
                                                                                            @foreach ($data['order_types'] as $label => $value)
                                                                                                <option value="{{ $value }}" @if ($value == $formValue['order_type']) selected @endif>{{ ucfirst($label) }}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-2">
                                                                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger btn_delete_order" style="float: right;">
                                                                                            <i class="ki-duotone ki-trash fs-1">
                                                                                                <span class="path1"></span>
                                                                                                <span class="path2"></span>
                                                                                                <span class="path3"></span>
                                                                                                <span class="path4"></span>
                                                                                                <span class="path5"></span>
                                                                                            </i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--begin::Form Area-->
                                                                            <div class="1_form_parameter form_parameter @if ($formValue['order_type'] != 1) d-none @endif">
                                                                                <div class="form-group mb-3">
                                                                                    <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                    <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][name_1]" value="{{ $formValue['name_1'] ?? ($formValue['name'] ?? '') }}" placeholder="Masukan nama parameter" />
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label">{{ __('Value') }}</label>
                                                                                            <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][value_1]" value="{{ $formValue['order_type'] == 1 ? $formValue['value_1'] ?? ($formValue['value'] ?? 0) : 0 }}" placeholder="Masukan nilai value" />
                                                                                            <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label mb-6">{{ __('Type') }}</label>
                                                                                            <div class="row">
                                                                                                <div class="row d-inline-flex">
                                                                                                    @foreach ($data['valuation_types'] as $label => $value)
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-check">
                                                                                                                <input class="form-check-input form-radio-button" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][valuation_type_1]" type="radio" value="{{ $value }}" @if ($value == ($formValue['valuation_type_1'] ?? ($formValue['valuation_type'] ?? ''))) checked @endif />
                                                                                                                <label class="form-check-label text-gray-800">
                                                                                                                    {{ ucfirst($label) }}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--begin::Area Parameter-->
                                                                                <div class="inner-repeater border rounded p-5">
                                                                                    <div data-repeater-list="order_parameters" class="mb-5">
                                                                                        @if ($formValue['order_type'] == 1 && count($formValue['order_parameters']) > 0)
                                                                                            @foreach ($formValue['order_parameters'] as $subFormKey => $subFormVal)
                                                                                                <div data-repeater-item @if ($subFormKey > 0) style @endif>
                                                                                                    <div class="form-group mb-3">
                                                                                                        <label class="form-label">{{ __('STD') }} (<span class="area_parameter_index"></span>)</label>
                                                                                                        <div class="row">
                                                                                                            <div class="col-10">
                                                                                                                <input type="text" class="form-control input-number" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][{{ $subFormKey }}][std]" value="{{ $subFormVal['std'] }}" placeholder="Nilai std" />
                                                                                                            </div>
                                                                                                            <div class="col-2">
                                                                                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                                </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    @foreach ($data['colors'] as $label => $value)
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-4 col-12 mb-3">
                                                                                                                <div class="form-group">
                                                                                                                    <label class="form-label">{{ __('Up') }}</label>
                                                                                                                    <input type="text" class="form-control input-number" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][{{ $subFormKey }}][up{{ $value }}]" value="{{ $subFormVal['up' . $value] ?? 0 }}" placeholder="Nilai atas" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-4 col-12 mb-3">
                                                                                                                <div class="form-group">
                                                                                                                    <label class="form-label">{{ __('Down') }}</label>
                                                                                                                    <input type="text" class="form-control input-number" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][{{ $subFormKey }}][down{{ $value }}]" value="{{ $subFormVal['down' . $value] ?? 0 }}" placeholder="Nilai bawah" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-4 col-12">
                                                                                                                <div class="form-group">
                                                                                                                    <label class="form-label">{{ __('Color') }}</label>
                                                                                                                    <select class="form-select select2-color" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][{{ $subFormKey }}][color{{ $value }}]" data-placeholder="Pilih warna">
                                                                                                                        <option></option>
                                                                                                                        @foreach ($data['colors'] as $colorName => $colorVal)
                                                                                                                            <option value="{{ $colorVal }}" @if ($colorVal == $subFormVal['color' . $value]) selected @endif>{{ ucfirst($colorName) }}</option>
                                                                                                                        @endforeach
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <div data-repeater-item>
                                                                                                <div class="form-group mb-3">
                                                                                                    <label class="form-label">{{ __('STD') }} (<span class="area_parameter_index"></span>)</label>
                                                                                                    <div class="row">
                                                                                                        <div class="col-10">
                                                                                                            <input type="text" class="form-control input-number" name="std" placeholder="Nilai std" />
                                                                                                        </div>
                                                                                                        <div class="col-2">
                                                                                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                @foreach ($data['colors'] as $label => $value)
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-4 col-12 mb-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="form-label">{{ __('Up') }}</label>
                                                                                                                <input type="text" class="form-control input-number" name="up{{ $value }}" placeholder="Nilai atas" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-4 col-12 mb-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="form-label">{{ __('Down') }}</label>
                                                                                                                <input type="text" class="form-control input-number" name="down{{ $value }}" placeholder="Nilai bawah" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-4 col-12 mb-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="form-label">{{ __('Color') }}</label>
                                                                                                                <select class="form-select select2-color" name="color{{ $value }}" data-placeholder="Pilih warna">
                                                                                                                    <option></option>
                                                                                                                    @foreach ($data['colors'] as $colorName => $colorVal)
                                                                                                                        <option value="{{ $colorVal }}">{{ ucfirst($colorName) }}</option>
                                                                                                                    @endforeach
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="row mt-3">
                                                                                        <div class="col-12">
                                                                                            <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                                {{ __('Tambah STD') }}
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end::Area Parameter-->
                                                                            </div>
                                                                            <!--end::Form Area-->
                                                                            <!--begin::Form Number-->
                                                                            <div class="2_form_parameter form_parameter @if ($formValue['order_type'] != 2) d-none @endif">
                                                                                <div class="form-group mb-3">
                                                                                    <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                    <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][name_2]" value="{{ $formValue['name_2'] ?? ($formValue['name'] ?? '') }}" placeholder="Masukan nama parameter" />
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label">{{ __('Value') }}</label>
                                                                                            <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][value_2]" value="{{ $formValue['order_type'] == 2 ? $formValue['value_2'] ?? ($formValue['value'] ?? 0) : 0 }}" placeholder="Masukan nilai value" />
                                                                                            <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label mb-6">{{ __('Type') }}</label>
                                                                                            <div class="row">
                                                                                                <div class="row d-inline-flex">
                                                                                                    @foreach ($data['valuation_types'] as $label => $value)
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-check">
                                                                                                                <input class="form-check-input form-radio-button" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][valuation_type_2]" type="radio" value="{{ $value }}" id="{{ $assestmentLabel . '_number_' . $label }}" @if ($value == ($formValue['valuation_type_2'] ?? ($formValue['valuation_type'] ?? ''))) checked @endif />
                                                                                                                <label class="form-check-label text-gray-800" for="{{ $assestmentLabel . '_number_' . $label }}">
                                                                                                                    {{ ucfirst($label) }}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <div class="col-6 mb-3">
                                                                                        <label class="form-label">{{ __('STD') }}</label>
                                                                                        <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][std]" value="{{ $formValue['std'] ?? ($formValue['order_parameters']['std'] ?? 0) }}" placeholder="Nilai standar" />
                                                                                    </div>
                                                                                    <div class="col-6 mb-3">
                                                                                        <label class="form-label">{{ __('Min') }}</label>
                                                                                        <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][min]" value="{{ $formValue['min'] ?? ($formValue['order_parameters']['min'] ?? 0) }}" placeholder="Nilai minimal" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <div class="col-12">
                                                                                        <label class="form-label">{{ __('Condition') }}</label>
                                                                                        <div class="row border rounded p-3">
                                                                                            <div class="col-md-3 col-6 mb-3">
                                                                                                <label class="form-label">{{ __('> (%)') }}</label>
                                                                                                <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][max]" value="{{ $formValue['max'] ?? ($formValue['order_parameters']['max'] ?? 0) }}" />
                                                                                            </div>
                                                                                            <div class="col-md-2 col-6 mb-3">
                                                                                                <label class="form-label">&nbsp;</label>
                                                                                                <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][mid1]" value="{{ $formValue['mid1'] ?? ($formValue['order_parameters']['mid1'] ?? 0) }}" />
                                                                                            </div>
                                                                                            <div class="col-md-2 col-12 p-0 mb-3">
                                                                                                <label class="form-label">&nbsp;</label>
                                                                                                <div class="d-flex align-items-center justify-content-center">
                                                                                                    <span class="bullet bg-gray-400 w-25px h-5px"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-6 mb-3">
                                                                                                <label class="form-label">&nbsp;</label>
                                                                                                <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][mid2]" value="{{ $formValue['mid2'] ?? ($formValue['order_parameters']['mid2'] ?? 0) }}" />
                                                                                            </div>
                                                                                            <div class="col-md-3 col-6 mb-3">
                                                                                                <label class="form-label">{{ __('< (%)') }}</label>
                                                                                                <input type="text" class="form-control input-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][less]" value="{{ $formValue['less'] ?? ($formValue['order_parameters']['less'] ?? 0) }}" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Form Number-->
                                                                            <!--begin::Form Ranges-->
                                                                            <div class="3_form_parameter form_parameter @if ($formValue['order_type'] != 3) d-none @endif">
                                                                                <div class="form-group row">
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                            <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][name_3]" value="{{ $formValue['name_3'] ?? ($formValue['name'] ?? '') }}" placeholder="Masukan nama parameter" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label">{{ __('Value') }}</label>
                                                                                            <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][value_3]" value="{{ $formValue['order_type'] == 3 ? $formValue['value_3'] ?? ($formValue['value'] ?? 0) : 0 }}" placeholder="Masukan nilai value" />
                                                                                            <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--begin::Range Parameter-->
                                                                                <div class="inner-repeater border rounded p-5">
                                                                                    <div data-repeater-list="range_parameters" class="mb-5">
                                                                                        @foreach ($data['colors'] as $label => $value)
                                                                                            @php
                                                                                                $rangeCriteria = [];
                                                                                                if ($formValue['order_type'] == 3) {
                                                                                                    $rangeCriteria = $formValue['range_parameters'][$value - 1]['criterias'] ?? [];
                                                                                                }
                                                                                            @endphp
                                                                                            <div class="border rounded p-3 mb-5">
                                                                                                <div data-repeater-item @if ($value > 1) style @endif>
                                                                                                    <div class="row">
                                                                                                        <div class="col-md-6 col-12 my-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="form-label">{{ __('Ranges') }}</label>
                                                                                                                <input type="text" class="form-control form-control-solid range-parameter-{{ $value }}" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][range_parameters][{{ $value - 1 }}][range]" value="{{ $formValue['range_parameters'][$value - 1]['range'] ?? '' }}" readonly />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-md-6 col-12 my-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="form-label">{{ __('Color') }}</label>
                                                                                                                <select class="form-select select2-color" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][range_parameters][{{ $value - 1 }}][color]" value="{{ $formValue['range_parameters'][$value - 1]['color'] ?? '' }}" data-placeholder="Pilih warna">
                                                                                                                    @php
                                                                                                                        $rangeColor = $formValue['range_parameters'][$value - 1]['color'] ?? '';
                                                                                                                    @endphp
                                                                                                                    <option></option>
                                                                                                                    @foreach ($data['colors'] as $colorName => $colorVal)
                                                                                                                        <option value="{{ $colorVal }}" @if ($colorVal == $rangeColor) selected @endif>{{ ucfirst($colorName) }}</option>
                                                                                                                    @endforeach
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="form-group">
                                                                                                        <div class="row">
                                                                                                            <label class="form-label">{{ __('Criteria') }}</label>
                                                                                                        </div>
                                                                                                        <div class="inner-child-repeater border rounded p-5">
                                                                                                            <div data-repeater-list="criterias" class="mb-5">
                                                                                                                @if (count($rangeCriteria) > 0)
                                                                                                                    @foreach ($rangeCriteria as $rangeCriteriaKey => $rangeCriteriaVal)
                                                                                                                        <div data-repeater-item @if ($rangeCriteriaKey > 0) style @endif>
                                                                                                                            <div class="row mb-3">
                                                                                                                                <div class="col-10">
                                                                                                                                    <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][range_parameters][{{ $value - 1 }}][criterias][{{ $rangeCriteriaKey }}][criteria]" value="{{ $rangeCriteriaVal['criteria'] ?? '' }}" placeholder="Masukan kriteria" />
                                                                                                                                </div>
                                                                                                                                <div class="col-2">
                                                                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                                                    </a>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    @endforeach
                                                                                                                @else
                                                                                                                    <div data-repeater-item>
                                                                                                                        <div class="row mb-3">
                                                                                                                            <div class="col-10">
                                                                                                                                <input type="text" class="form-control" name="criteria" placeholder="Masukan kriteria" />
                                                                                                                            </div>
                                                                                                                            <div class="col-2">
                                                                                                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                @endif
                                                                                                            </div>
                                                                                                            <div class="row">
                                                                                                                <div class="col-12">
                                                                                                                    <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                                                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                                                        {{ __('Tambah Criteria') }}
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                                <!--end::Range Parameter-->
                                                                            </div>
                                                                            <!--end::Form Ranges-->
                                                                            <!--begin::Form Approval-->
                                                                            <div class="4_form_parameter form_parameter @if ($formValue['order_type'] != 4) d-none @endif">
                                                                                <div class="form-group row">
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                            <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][name_4]" value="{{ $formValue['name_4'] ?? ($formValue['name'] ?? '') }}" placeholder="Masukan nama parameter" />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6 col-12 mb-3">
                                                                                        <div class="form-group">
                                                                                            <label class="form-label">{{ __('Value') }}</label>
                                                                                            <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][value_4]" value="{{ $formValue['order_type'] == 4 ? $formValue['value_4'] ?? ($formValue['value'] ?? 0) : 0 }}" placeholder="Masukan nilai value" />
                                                                                            <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="form-label">{{ __('Approval') }}</label>
                                                                                </div>
                                                                                <div class="inner-repeater border rounded p-5">
                                                                                    <div data-repeater-list="order_parameters" class="mb-5">
                                                                                        @if ($formValue['order_type'] == 4)
                                                                                            @forelse($formValue['order_parameters'] ?? [] as $subFormKey => $subFormValue)
                                                                                                <div data-repeater-item @if ($subFormKey > 0) style @endif>
                                                                                                    <div class="form-group row">
                                                                                                        <div class="col-10 mb-3">
                                                                                                            <input type="text" class="form-control" name="{{ $assestmentLabel }}[parameters][{{ $formKey }}][order_parameters][{{ $subFormKey }}][param]" value="{{ $subFormValue['param'] ?? '' }}" placeholder="Masukan parameter" />
                                                                                                        </div>
                                                                                                        <div class="col-2 mb-3">
                                                                                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @empty
                                                                                                <div data-repeater-item>
                                                                                                    <div class="form-group row">
                                                                                                        <div class="col-10 mb-3">
                                                                                                            <input type="text" class="form-control" name="param" placeholder="Masukan parameter" />
                                                                                                        </div>
                                                                                                        <div class="col-2 mb-3">
                                                                                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                            </a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforelse
                                                                                        @else
                                                                                            <div data-repeater-item>
                                                                                                <div class="form-group row">
                                                                                                    <div class="col-10 mb-3">
                                                                                                        <input type="text" class="form-control" name="param" placeholder="Masukan parameter" />
                                                                                                    </div>
                                                                                                    <div class="col-2 mb-3">
                                                                                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                            {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif

                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-12">
                                                                                            <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                                {{ __('Tambah') }}
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Form Approval-->
                                                                        </div>
                                                                        <!--end::Form Parameter-->
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div data-repeater-item>
                                                                    <!--begin::Form Parameter-->
                                                                    <div class="{{ $assestmentLabel . '_parameter' }}">
                                                                        <div class="form-group my-3">
                                                                            <label class="form-label">{{ __('Tipe Order') }}</label>
                                                                            <div class="row">
                                                                                <div class="col-10">
                                                                                    <select class="form-select select2-order" name="order_type" data-type="{{ $assestmentLabel }}" data-placeholder="Pilih tipe order">
                                                                                        <option></option>
                                                                                        @foreach ($data['order_types'] as $label => $value)
                                                                                            <option value="{{ $value }}">{{ ucfirst($label) }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-2">
                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1 btn_delete_order" style="float: right;">
                                                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--begin::Form Area-->
                                                                        <div class="1_form_parameter form_parameter d-none">
                                                                            <div class="form-group mb-3">
                                                                                <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                <input type="text" class="form-control" name="name_1" value="" placeholder="Masukan nama parameter" />
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label">{{ __('Value') }}</label>
                                                                                        <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="value_1" placeholder="Masukan nilai value" />
                                                                                        <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label mb-6">{{ __('Type') }}</label>
                                                                                        <div class="row">
                                                                                            <div class="row d-inline-flex">
                                                                                                @foreach ($data['valuation_types'] as $label => $value)
                                                                                                    <div class="col-6">
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input form-radio-button" name="valuation_type_1" type="radio" value="{{ $value }}" id="{{ $assestmentLabel . '_area_' . $label }}" />
                                                                                                            <label class="form-check-label text-gray-800" for="{{ $assestmentLabel . '_area_' . $label }}">
                                                                                                                {{ ucfirst($label) }}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--begin::Area Parameter-->
                                                                            <div class="inner-repeater border rounded p-5">
                                                                                <div data-repeater-list="order_parameters" class="mb-5">
                                                                                    <div data-repeater-item>
                                                                                        <div class="form-group mb-3">
                                                                                            <label class="form-label">{{ __('STD') }} (<span class="area_parameter_index"></span>)</label>
                                                                                            <div class="row">
                                                                                                <div class="col-10">
                                                                                                    <input type="text" class="form-control input-number" name="std" placeholder="Nilai std" />
                                                                                                </div>
                                                                                                <div class="col-2">
                                                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        @foreach ($data['colors'] as $label => $value)
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 col-12 mb-3">
                                                                                                    <div class="form-group">
                                                                                                        <label class="form-label">{{ __('Up') }}</label>
                                                                                                        <input type="text" class="form-control input-number" name="up{{ $value }}" placeholder="Nilai atas" />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-12 mb-3">
                                                                                                    <div class="form-group">
                                                                                                        <label class="form-label">{{ __('Down') }}</label>
                                                                                                        <input type="text" class="form-control input-number" name="down{{ $value }}" placeholder="Nilai bawah" />
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-4 col-12 mb-3">
                                                                                                    <div class="form-group">
                                                                                                        <label class="form-label">{{ __('Color') }}</label>
                                                                                                        <select class="form-select select2-color" name="color{{ $value }}" data-placeholder="Pilih warna">
                                                                                                            <option></option>
                                                                                                            @foreach ($data['colors'] as $colorName => $colorVal)
                                                                                                                <option value="{{ $colorVal }}">{{ ucfirst($colorName) }}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row mt-3">
                                                                                    <div class="col-12">
                                                                                        <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                            {{ __('Tambah STD') }}
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Area Parameter-->
                                                                        </div>
                                                                        <!--end::Form Area-->
                                                                        <!--begin::Form Number-->
                                                                        <div class="2_form_parameter form_parameter d-none">
                                                                            <div class="form-group mb-3">
                                                                                <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                <input type="text" class="form-control" name="name_2" value="" placeholder="Masukan nama parameter" />
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label">{{ __('Value') }}</label>
                                                                                        <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="value_2" placeholder="Masukan nilai value" />
                                                                                        <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label mb-6">{{ __('Type') }}</label>
                                                                                        <div class="row">
                                                                                            <div class="row d-inline-flex">
                                                                                                @foreach ($data['valuation_types'] as $label => $value)
                                                                                                    <div class="col-6">
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input form-radio-button" name="valuation_type_2" type="radio" value="{{ $value }}" id="{{ $assestmentLabel . '_number_' . $label }}" />
                                                                                                            <label class="form-check-label text-gray-800" for="{{ $assestmentLabel . '_number_' . $label }}">
                                                                                                                {{ ucfirst($label) }}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-6 mb-3">
                                                                                    <label class="form-label">{{ __('STD') }}</label>
                                                                                    <input type="text" class="form-control input-value" name="std" placeholder="Nilai standar" />
                                                                                </div>
                                                                                <div class="col-6 mb-3">
                                                                                    <label class="form-label">{{ __('Min') }}</label>
                                                                                    <input type="text" class="form-control input-value" name="min" placeholder="Nilai minimal" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-12">
                                                                                    <label class="form-label">{{ __('Condition') }}</label>
                                                                                    <div class="row border rounded p-3">
                                                                                        <div class="col-md-3 col-6 mb-3">
                                                                                            <label class="form-label">{{ __('> (%)') }}</label>
                                                                                            <input type="text" class="form-control input-value" name="max" />
                                                                                        </div>
                                                                                        <div class="col-md-2 col-6 mb-3">
                                                                                            <label class="form-label">&nbsp;</label>
                                                                                            <input type="text" class="form-control input-value" name="mid1" />
                                                                                        </div>
                                                                                        <div class="col-md-2 col-12 p-0 mb-3">
                                                                                            <label class="form-label">&nbsp;</label>
                                                                                            <div class="d-flex align-items-center justify-content-center">
                                                                                                <span class="bullet bg-gray-400 w-25px h-5px"></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-6 mb-3">
                                                                                            <label class="form-label">&nbsp;</label>
                                                                                            <input type="text" class="form-control input-value" name="mid2" />
                                                                                        </div>
                                                                                        <div class="col-md-3 col-6 mb-3">
                                                                                            <label class="form-label">{{ __('< (%)') }}</label>
                                                                                            <input type="text" class="form-control input-value" name="less" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Form Number-->
                                                                        <!--begin::Form Ranges-->
                                                                        <div class="3_form_parameter form_parameter d-none">
                                                                            <div class="form-group row">
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                        <input type="text" class="form-control" name="name_3" value="" placeholder="Masukan nama parameter" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label">{{ __('Value') }}</label>
                                                                                        <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="value_3" placeholder="Masukan nilai value" />
                                                                                        <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--begin::Range Parameter-->
                                                                            <div class="inner-repeater border rounded p-5">
                                                                                <div data-repeater-list="range_parameters" class="mb-5">
                                                                                    @foreach ($data['colors'] as $label => $value)
                                                                                        <div class="border rounded p-3 mb-5">
                                                                                            <div data-repeater-item @if ($value > 1) style @endif>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-6 col-12 my-3">
                                                                                                        <div class="form-group">
                                                                                                            <label class="form-label">{{ __('Ranges') }}</label>
                                                                                                            <input type="text" class="form-control form-control-solid range-parameter-{{ $value }}" name="range" value="{{ $value }}" readonly />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-6 col-12 my-3">
                                                                                                        <div class="form-group">
                                                                                                            <label class="form-label">{{ __('Color') }}</label>
                                                                                                            <select class="form-select select2-color" name="color" data-placeholder="Pilih warna">
                                                                                                                <option></option>
                                                                                                                @foreach ($data['colors'] as $colorName => $colorVal)
                                                                                                                    <option value="{{ $colorVal }}">{{ ucfirst($colorName) }}</option>
                                                                                                                @endforeach
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="row">
                                                                                                        <label class="form-label">{{ __('Criteria') }}</label>
                                                                                                    </div>
                                                                                                    <div class="inner-child-repeater border rounded p-5">
                                                                                                        <div data-repeater-list="criterias" class="mb-5">
                                                                                                            <div data-repeater-item>
                                                                                                                <div class="row mb-3">
                                                                                                                    <div class="col-10">
                                                                                                                        <input type="text" class="form-control" name="criteria" placeholder="Masukan kriteria" />
                                                                                                                    </div>
                                                                                                                    <div class="col-2">
                                                                                                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                                            {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                            <div class="col-12">
                                                                                                                <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                                                    {{ __('Tambah Criteria') }}
                                                                                                                </a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Range Parameter-->
                                                                        </div>
                                                                        <!--end::Form Ranges-->
                                                                        <!--begin::Form Approval-->
                                                                        <div class="4_form_parameter form_parameter d-none">
                                                                            <div class="form-group row">
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label parameter-name">{{ __('Nama Parameter Ke-') }}<span class="parameter_name_index"></span></label>
                                                                                        <input type="text" class="form-control" name="name_4" value="" placeholder="Masukan nama parameter" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-12 mb-3">
                                                                                    <div class="form-group">
                                                                                        <label class="form-label">{{ __('Value') }}</label>
                                                                                        <input type="text" class="form-control input-value {{ $assestmentLabel }}-value" name="value_4" placeholder="Masukan nilai value" />
                                                                                        <span class="text-muted {{ $assestmentLabel }}-info-remaining-value">(Remaining <span class="{{ $assestmentLabel }}-remaining-value"></span>/100)</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="form-label">{{ __('Approval') }}</label>
                                                                            </div>
                                                                            <div class="inner-repeater border rounded p-5">
                                                                                <div data-repeater-list="order_parameters" class="mb-5">
                                                                                    <div data-repeater-item>
                                                                                        <div class="form-group row">
                                                                                            <div class="col-10 mb-3">
                                                                                                <input type="text" class="form-control" name="param" placeholder="Masukan parameter" />
                                                                                            </div>
                                                                                            <div class="col-2 mb-3">
                                                                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-danger mt-1" style="float: right;">
                                                                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-12">
                                                                                        <a href="javascript:;" class="btn btn-sm btn-outline btn-outline-primary" data-repeater-create>
                                                                                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-1') !!}
                                                                                            {{ __('Tambah') }}
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Form Approval-->
                                                                    </div>
                                                                    <!--end::Form Parameter-->
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="form-group" hidden>
                                                            <a href="javascript:;" data-repeater-create class="btn btn-flex btn-light-primary" id="{{ $assestmentLabel }}_add_form">
                                                                <i class="ki-duotone ki-plus fs-3"></i>
                                                                Add Row
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!--end::Repeater-->
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="card border rounded shadow-sm">
                                    <div class="card-header">
                                        <h3 class="card-title text-gray-800 fw-bold">Condition Criteria</h3>
                                    </div>
                                    <div class="card-body">
                                        @foreach ($data['criteria_status'] as $label => $value)
                                            @php
                                                $up = old('up' . $value) ?? ($data['condition_criteria']['up' . $value] ?? 0);
                                                $down = old('down' . $value) ?? ($data['condition_criteria']['down' . $value] ?? 0);
                                            @endphp
                                            <div class="form-group row">
                                                <div class="col-md-4 col-12 mb-3">
                                                    <div class="form-group">
                                                        <label class="required form-label">{{ __('Up') }}</label>
                                                        <input type="text" name="up{{ $value }}" class="form-control input-number" value="{{ $up }}" placeholder="Nilai atas" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12 mb-3">
                                                    <div class="form-group">
                                                        <label class="required form-label">{{ __('Down') }}</label>
                                                        <input type="text" name="down{{ $value }}" class="form-control input-number" value="{{ $down }}" placeholder="Nilai bawah" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-12 mb-3">
                                                    <div class="form-group">
                                                        <label class="required form-label">{{ __('Status') }}</label>
                                                        <input type="text" name="status{{ $value }}" class="form-control form-control-solid" value="{{ ucfirst($label) }}" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ route('port.form.index', $data['port']['id']) }}" class="btn btn-sm btn-secondary">
                            {{ __('Batal') }}
                        </a>
                        &nbsp;&nbsp;
                        <button type="submit" class="btn btn-sm btn-primary">
                            {{ __('Simpan') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--begin::Duplicate Form-->
    <form class="form" id="duplicate_form" action="{{ route('port.form.sub-group.copy-param', [$data['port']['id'], $data['sub_group']['id']]) }}" method="POST">
        @csrf
        <input type="hidden" name="source_id">
    </form>
    <!--end::Duplicate Form-->
@endsection
@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }

        function initInputMaskValue() {
            Inputmask({
                rightAlign: false,
                alias: "numeric",
                min: 1,
                max: 100,
            }).mask(".input-value");
        }

        function initInputMaskNumeric() {
            Inputmask({
                rightAlign: false,
                alias: "numeric",
                min: 0,
            }).mask(".input-number");
        }

        function initSelect2(element) {
            element.select2();
        }

        function initRangeParameter(element) {
            for (var i = 1; i <= 3; i++) {
                element.find(`.range-parameter-${i}`).val(i);
            }
        }

        function selectOrderEvent() {
            var selectedOption = $(this).val();
            var type = $(this).data('type');
            var item = $(this).closest(`.${type}_parameter`);

            item.find(`.${selectedOption}_form_parameter`).removeClass("d-none");
            item.find(".form_parameter").not(`.${selectedOption}_form_parameter`).addClass("d-none");

            if (selectedOption == 3) {
                initRangeParameter(item.find(`.3_form_parameter`));
            }
        }

        function initValueValidation() {
            var total = {!! json_encode($data['total_value']) !!}
            var stages = ['initial', 'before', 'after'];
            stages?.forEach(function(stage) {
                $(`:submit`).prop('disabled', false);
                $(`.invalid-remaining-value`).remove();
                $(`.${stage}-remaining-value`).text(total?.[stage]);
                if (total?.[stage] > 100) {
                    $(`.${stage}-value`).addClass('is-invalid');
                    $(`.${stage}-info-remaining-value`).addClass('text-danger');
                    $(`.${stage}-info-remaining-value`).removeClass('text-muted');
                    $(`.${stage}-info-remaining-value`).after(`
                        <div class="row invalid-remaining-value">
                            <span class="text-danger mt-1">
                                <strong>Value melebihi batas maksimum</strong>
                            </span>
                        </div>
                    `);
                    $(`:submit`).prop('disabled', true);
                } else {
                    $(`.${stage}-value`).removeClass('is-invalid');
                    $(`.${stage}-info-remaining-value`).removeClass('text-danger');
                    $(`.${stage}-info-remaining-value`).addClass('text-muted');
                }
            });
        }

        function valueValidationEvent(type) {
            var totalValue = 0;
            var fields = document.querySelectorAll(`.${type}-value`);
            fields?.forEach(function(field) {
                totalValue += parseInt(field?.value) || 0;
            });

            $(`:submit`).prop('disabled', false);
            $(`.invalid-remaining-value`).remove();
            $(`.${type}-remaining-value`).text(totalValue);
            if (totalValue > 100) {
                $(`.${type}-value`).addClass('is-invalid');
                $(`.${type}-info-remaining-value`).addClass('text-danger');
                $(`.${type}-info-remaining-value`).removeClass('text-muted');
                $(`.${type}-info-remaining-value`).after(`
                    <div class="row invalid-remaining-value">
                        <span class="text-danger mt-1">
                            <strong>Value melebihi batas maksimum</strong>
                        </span>
                    </div>
                `);
                $(`:submit`).prop('disabled', true);
            } else {
                $(`.${type}-value`).removeClass('is-invalid');
                $(`.${type}-info-remaining-value`).removeClass('text-danger');
                $(`.${type}-info-remaining-value`).addClass('text-muted');
            }
        }

        function ucfirst(str) {
            if (str) {
                return str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
            }
            return str;
        }

        function repeterEvent(assestmentType) {
            const count = {
                'initial': {{ count(old('initial') ?? ($data['forms']['initial']['parameters'] ?? [])) }},
                'before': {{ count(old('before') ?? ($data['forms']['before']['parameters'] ?? [])) }},
                'after': {{ count(old('after') ?? ($data['forms']['after']['parameters'] ?? [])) }}
            }

            return {
                initEmpty: count?.[assestmentType] == 0,
                repeaters: [{
                    selector: '.inner-repeater',
                    repeaters: [{
                        selector: '.inner-child-repeater',
                        show: function() {
                            $(this).slideDown();
                        },
                        hide: function(deleteElement) {
                            $(this).slideUp(deleteElement);
                        }
                    }],
                    show: function() {
                        $(this).slideDown();
                        $(this).find('.area_parameter_index').text($(this).index() + 1);

                        initSelect2($(this).find(`.select2-color`))
                        initInputMaskValue();
                        initInputMaskNumeric();
                    },
                    hide: function(deleteElement) {
                        $(this).slideUp(deleteElement);
                    },
                    ready: function() {
                        $('.area_parameter_index').text(1);

                        initSelect2($(this).get(0).$parent.find(`.select2-color`))
                    }
                }],
                show: function() {
                    $(this).slideDown();
                    $(this).find('.parameter_name_index').text($(this).index() + 1);
                    $(this).find(`.select2-order`).on("change", selectOrderEvent);
                    $(this).find(`.form_parameter`).addClass('d-none');
                    $(this).find(`.initial-value`).on('keyup', () => valueValidationEvent('initial'));
                    $(this).find(`.before-value`).on('keyup', () => valueValidationEvent('before'));
                    $(this).find(`.after-value`).on('keyup', () => valueValidationEvent('after'));

                    initSelect2($(this).find(`.select2-order`))
                    initInputMaskValue();
                    initInputMaskNumeric();
                    initValueValidation();
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
                ready: function() {
                    $('.parameter_name_index').text(1);
                    $('.initial-value').on('keyup', () => valueValidationEvent('initial'));
                    $('.before-value').on('keyup', () => valueValidationEvent('before'));
                    $('.after-value').on('keyup', () => valueValidationEvent('after'));
                }
            }
        }

        function initRepeater() {
            $('#initial_kt_docs_repeater_nested').repeater(repeterEvent('initial'));
            $('#before_kt_docs_repeater_nested').repeater(repeterEvent('before'));
            $('#after_kt_docs_repeater_nested').repeater(repeterEvent('after'));
        }

        $(document).ready(function() {
            $('.select2-order').on("change", selectOrderEvent);
            $('.select2-parameter').select2();

            checkSession();
            initSelect2($(`.select2-order`));
            initValueValidation();
        });

        $(document).on('click', '.btn_add_order', function() {
            var type = $(this).data('type');
            var qty = $(this).closest('.form-group').find('.input-value').val();

            for (var i = 0; i < qty; i++) {
                $(`#${type}_add_form`).trigger('click');
            }
        });

        $(document).on('click', '#btn_copy_existing', function() {
            var id = {{ $data['sub_group']['id'] }};
            var copied = $('.select2-parameter').val();
            if (id != copied) {
                $('input[name="source_id"]').val(copied);
                Swal.fire({
                    title: "{{ __('Apakah yakin menyalin data?') }}",
                    text: "{{ __('Jika data sebelumnya telah ada, maka akan tergantikan!') }}",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Ya') }}",
                    cancelButtonText: "{{ __('Batal') }}",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#duplicate_form').submit();
                    }
                });
            }
        });

        initRepeater();
    </script>
@endsection
