<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="sub_group_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Sub Group') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>
            <div class="modal-body">
                <div class="card card-custom shadow-sm mb-10">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Data Sub Group') }}</h3>
                        <div class="card-toolbar rotate-180">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="datatable_sub_group">
                                <thead>
                                    <tr class="fw-bold text-center">
                                        <th class="mw-35px">{{ __('No') }}</th>
                                        <th>{{ __('Sub Group') }}</th>
                                        <th class="mw-80px">{{ __('Aksi') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card card-custom shadow-sm">
                    <form class="from" action="{{ route('port.form.sub-group.store', $data['port']['id']) }}" method="POST">
                        @csrf
                        <input type="hidden" name="mst_ercm_pelabuhan_system_id">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Tambah Sub Group') }}</h3>
                            <div class="card-toolbar">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="required form-label">{{ __('Sub Group') }}</label>
                                <input type="text" name="name" class="form-control" placeholder="Masukan nama sub group" />
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                            &nbsp; &nbsp;
                            <button type="submit" class="btn btn-sm btn-primary">{{ __('Next') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
