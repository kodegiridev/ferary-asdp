<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="sub_group_edit_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form form-sub-group" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="url" value="{{ route('port.form.sub-group.update', $data['port']['id']) }}">
                <input type="hidden" name="modal" value="sub_group_edit_modal">
                <input type="hidden" name="mst_ercm_pelabuhan_system_id">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ubah Sub Group') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Sub Group') }}</label>
                        <input type="text" class="form-control" name="name" placeholder="Nama Sub Group" required />
                    </div>
                    <div class="form-group col-10">
                        <input type="hidden" name="sub_used_value">
                        <label class="form-label">{{ __('Proportional Value') }}</label>
                        <label class="form-label subgroup-remaining">({{ __('Remaining') }} <span class="create_value_sub">0</span>/100)</label>
                        <input type="text" class="form-control input-value subgroup-value" name="value" placeholder="Masukan proportional value" />
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
