<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="parameter_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="/" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="parameter_form_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Parameter') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card card-custom shadow-sm mb-10">
                        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#initial_form">
                            <h3 class="card-title">{{ __('Initial Condition') }}</h3>
                            <div class="card-toolbar rotate-180">
                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr073.svg', 'svg-icon-1') !!}
                            </div>
                        </div>
                        <div id="initial_form" class="collapse">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Condition') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('condition') is-invalid @enderror" name="condition" value="{{ old('condition') ?? 'Good' }}" placeholder="Condition" />
                                        @error('condition')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Proportional Value') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('proportional_value') is-invalid @enderror" name="proportional_value" value="{{ old('proportional_value') ?? '15' }}" placeholder="Nilai proportional value" />
                                        @error('proportional_value')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="required form-label">{{ __('Formula') }}</label>
                                    <input type="text" class="form-control form-control-solid @error('formula') is-invalid @enderror" name="formula" value="{{ old('formula') ?? 'Good' }}" placeholder="Formula" />
                                    @error('formula')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label class="required form-label">{{ __('Standard') }}</label>
                                    <input type="text" class="form-control form-control-solid @error('standard') is-invalid @enderror" name="standard" value="{{ old('standard') ?? 'Good' }}" placeholder="Standard" />
                                    @error('standard')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <input class="form-check-input" type="checkbox" />
                                    <label class="ms-2">Yes or No</label>
                                </div>
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Original') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('original') is-invalid @enderror" name="original" value="{{ old('original') }}" placeholder="Original" />
                                        @error('original')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Minimum') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('minimum') is-invalid @enderror" name="minimum" value="{{ old('minimum') }}" placeholder="Minimum" />
                                        @error('minimum')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Order') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('order') is-invalid @enderror" name="order" value="{{ old('order') }}" placeholder="Order" />
                                        @error('order')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Slug') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug') }}" placeholder="Slug" />
                                        @error('slug')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-sm btn-primary btn-initial" data-bs-toggle="collapse" data-bs-target="#initial_form">
                                <i class="fa fa-plus"></i>
                                {{ __('Tambah') }}
                            </button>
                        </div>
                    </div>
                    <div class="card card-custom shadow-sm">
                        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#defect_form">
                            <h3 class="card-title">{{ __('Defect') }}</h3>
                            <div class="card-toolbar rotate-180">
                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr073.svg', 'svg-icon-1') !!}
                            </div>
                        </div>
                        <div id="defect_form" class="collapse">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Defect') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('defect') is-invalid @enderror" name="defect" value="{{ old('defect') ?? 'Good' }}" placeholder="Defect" />
                                        @error('defect')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Proportional Value') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('proportional_value') is-invalid @enderror" name="proportional_value" value="{{ old('proportional_value') ?? '15' }}" placeholder="Nilai proportional value" />
                                        @error('proportional_value')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="required form-label">{{ __('Formula') }}</label>
                                    <input type="text" class="form-control form-control-solid @error('formula') is-invalid @enderror" name="formula" value="{{ old('formula') ?? 'Good' }}" placeholder="Formula" />
                                    @error('formula')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <label class="required form-label">{{ __('Standard') }}</label>
                                    <input type="text" class="form-control form-control-solid @error('standard') is-invalid @enderror" name="standard" value="{{ old('standard') ?? 'Good' }}" placeholder="Standard" />
                                    @error('standard')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3">
                                    <input class="form-check-input" type="checkbox" />
                                    <label class="ms-2">Yes or No</label>
                                </div>
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Original') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('original') is-invalid @enderror" name="original" value="{{ old('original') }}" placeholder="Original" />
                                        @error('original')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Minimum') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('minimum') is-invalid @enderror" name="minimum" value="{{ old('minimum') }}" placeholder="Minimum" />
                                        @error('minimum')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Order') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('order') is-invalid @enderror" name="order" value="{{ old('order') }}" placeholder="Order" />
                                        @error('order')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label class="required form-label">{{ __('Slug') }}</label>
                                        <input type="text" class="form-control form-control-solid @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug') }}" placeholder="Slug" />
                                        @error('slug')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-sm btn-primary btn-defect" data-bs-toggle="collapse" data-bs-target="#defect_form">
                                <i class="fa fa-plus"></i>
                                {{ __('Tambah') }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
