<div class="modal fade" tabindex="-1" id="create_port_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Port') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('port.port.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama" class="required form-label">
                                {{ __('Nama Pelabuhan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama" value="{{ old('nama') }}"/>

                                @error('nama')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="regional_id" class="required form-label">
                                {{ __('Regional') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_regional_id" name="regional_id"
                                class="form-select form-select-solid @error('regional_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($regional as $p)
                                        <option value="{{ $p->id }}"
                                        {{ old('regional_id') == $p->id ? 'selected' : '' }}>
                                            {{ $p->nama }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('regional_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="cabang_id" class="required form-label">
                                {{ __('Cabang') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_cabang_id" name="cabang_id"
                                class="form-select form-select-solid @error('cabang_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                </select>

                                @error('cabang_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="pic" class="required form-label">
                                {{ __('PIC') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_pic" name="pic"
                                class="form-select form-select-solid @error('pic') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($pic as $p)
                                        <option value="{{ $p->id }}"
                                        {{ old('pic') == $p->id ? 'selected' : '' }}>
                                            {{ $p->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('pic')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>