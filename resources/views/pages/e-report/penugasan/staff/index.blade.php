@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
    <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Penugasan') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Penugasan') }}</li>
            </ul>
        </div>
    </div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Penugasan Staff') }}</h3>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="progress_staff_report_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Judul Penugasan') }}</th>
                                <th>{{ __('Keterangan Penugasan') }}</th>
                                <th>{{ __('Pengirim') }}</th>
                                <th>{{ __('Status Penugasan') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.e-report.penugasan.staff.modals.detail')
@include('pages.e-report.penugasan.staff.modals.keterangan')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }
    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_penugasan_modal').trigger('click');
    }
    $(document).ready(function() {
        checkSession();
        $('#progress_staff_report_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('e-report.penugasan.index') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'judul_penugasan',
                    name: 'judul_penugasan'
                },
                {
                    data: 'keterangan_penugasan',
                    name: 'keterangan_penugasan'
                },
                {
                    data: 'pembuat.name',
                    name: 'pengirim'
                },
                {
                    data: 'status_penugasan',
                    name: 'status_penugasan'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            "columnDefs": [{
                "targets": 2,
                "createdCell": function(td, cellData, rowData, row, col) {
                    if (rowData.status == 'ACTIVE') {
                        $(td).css('background-color', '#fff3cd')
                    } else if (rowData.status == 'EXPIRED') {
                        $(td).css('background-color', '#f8d7da')
                    }
                }
            }]
        });

        $(document).on('click', '.btn-detail-penugasan', function() {
            var url = `{{ route('e-report.penugasan.show', ':id') }}`;
            url = url.replace(':id', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    var action = `{{ route('e-report.penugasan.pengajuan', ':id') }}`;
                    action = action.replace(':id', data.id);

                    $('#detail_penugasan_modal form').prop('action', action);

                    $('#detail_penugasan_modal p[name="title"]').text(data.judul_penugasan);
                    $('#keterangan_penugasan_modal input[name="id"]').val(data.id);
                    $('#detail_penugasan_modal p[name="keterangan"]').text(data.keterangan_penugasan);
                    $('#detail_penugasan_modal p[name="pic"]').text(data.pembuat.name);
                    $('#detail_penugasan_modal a').attr('href', data.dokumen_penugasan)
                    $('#detail_penugasan_modal p[name="status_penugasan"]').text(data.status_penugasan);
                    if (data.status > 0 && data.status != 2) {
                        $('#detail_penugasan_modal input[name="target_waktu"]').val(data.target_waktu).prop('disabled', true);
                        $('#detail_penugasan_modal input[name="target_pencapaian"]').val(data.target_pencapaian).prop('disabled', true);
                        $('#detail_penugasan_modal button[name="submit"]').hide();
                    } else {
                        $('#detail_penugasan_modal input[name="target_waktu"]').val(data.target_waktu).prop('disabled', false);
                        $('#detail_penugasan_modal input[name="target_pencapaian"]').val(data.target_pencapaian).prop('disabled', false);
                        $('#detail_penugasan_modal button[name="submit"]').show();
                    }

                    if (data.status == 2) {
                        $('#detail_penugasan_modal button[name="aksi_keterangan"]').show();
                    } else {
                        $('#detail_penugasan_modal button[name="aksi_keterangan"]').hide();
                    }

                }
            });
        });

        $(document).on('click', '.btn-keterangan-penugasan', function(e) {
            id = $('#keterangan_penugasan_modal input[name="id"]').val();

            var url = `{{ route('e-report.penugasan.show', ':id') }}`;
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    var action = `{{ route('e-report.penugasan.status.update', ':id') }}`;
                    action = action.replace(':id', data.id);
                    $('#keterangan_penugasan_modal form').prop('action', action);
                    $('#keterangan_penugasan_modal input[name="status"]').val(e.target.value);
                    if (data.tanggapan) {
                        $('#keterangan_penugasan_modal textarea[name="keterangan"]').val(data.tanggapan[0].keterangan);
                    }
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function() {
            $('.is-invalid').each(function(index, element) {
                $(this).removeClass('is-invalid');
            });
        });
    })
</script>
@endsection