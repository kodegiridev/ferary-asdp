<div class="modal fade" tabindex="-1" id="create_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Penugasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('e-report.penugasan.store') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" value="create" name="action">
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="required form-label">
                                {{ __('Judul Penugasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text" class="form-control form-control-solid @error('title') is-invalid @enderror" placeholder="Ketik disini" name="title" value="{{ old('title') }}" />

                                @error('title')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="manager_id" class="required form-label">
                                {{ __('Pilih Manager') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="manager_id" class="form-select form-select-solid @error('manager_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach($manager as $m)
                                    <option value="{{ $m->id }}" {{ old('manager_id') == $m->id ? 'selected' : '' }}>
                                        {{ $m->name }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('manager_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan" class="required form-label">
                                {{ __('Keterangan Penugasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea type="text" class="form-control form-control-solid @error('keterangan') is-invalid @enderror" placeholder="Ketik disini" name="keterangan" value="{{ old('keterangan') }}"></textarea>

                                @error('keterangan')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="dokumen" class="required form-label">
                                {{ __('Pilih Dokumen') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="file" class="form-control form-control-solid @error('dokumen') is-invalid @enderror" accept ="application/pdf" placeholder="Ketik disini" name="dokumen" value="{{ old('dokumen') }}" />

                                @error('dokumen')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>