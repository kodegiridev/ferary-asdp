<div class="modal fade" tabindex="-1" id="detail_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Detail Penugasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="table-responsive">
                        <input type="hidden" value="edit" name="action">
                        <input type="hidden" value="1" name="status">
                        <table class="table table-bordered" id="detail_data_penugasan">
                            <tbody>
                                <tr>
                                    <td class="col-4">
                                        <h3>Judul</h3>
                                    </td>
                                    <td>
                                        <p name="title"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Keterangan</h3>
                                    </td>
                                    <td>
                                        <p name="keterangan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Pengirim</h3>
                                    </td>
                                    <td>
                                        <p name="pic"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Dokumen</h3>
                                    </td>
                                    <td>
                                        <a target="_blank">Doc Penugasan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Status</h3>
                                    </td>
                                    <td>
                                        <p name="status_penugasan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Durasi Penyelesaian</h3>
                                    </td>
                                    <td>
                                        <input class="form-control form-control-solid" type="date" name="target_waktu" @error('target_waktu') is-invalid @enderror" placeholder="Ketik disini" value="{{ old('target_waktu') }}" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Pencapaian</h3>
                                    </td>
                                    <td>
                                        <input class="form-control form-control-solid" type="text" name="target_pencapaian" @error('target_pencapaian') is-invalid @enderror" placeholder="Ketik disini" value="{{ old('target_pencapaian') }}" required>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" name="aksi_keterangan" class="btn btn-danger btn-keterangan-penugasan" data-bs-toggle="modal" data-bs-target="#keterangan_penugasan_modal">Lihat Keterangan</button>
                    <button name="submit" type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>