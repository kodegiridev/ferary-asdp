<div class="modal fade" tabindex="-1" id="detail_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Detail Penugasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form action="">
                        <table class="table table-bordered" id="detail_data_penugasan">
                            <tbody>
                                <tr>
                                    <td class="col-4">
                                        <h3>Judul</h3>
                                    </td>
                                    <td>
                                        <p name="title"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Keterangan</h3>
                                    </td>
                                    <td>
                                        <p name="keterangan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>PIC</h3>
                                    </td>
                                    <td>
                                        <p name="pic"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Dokumen</h3>
                                    </td>
                                    <td>
                                        <a target="_blank">Doc Penugasan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Status</h3>
                                    </td>
                                    <td>
                                        <p name="status_penugasan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Durasi Penyelesaian</h3>
                                    </td>
                                    <td>
                                        <input type="date" class="form-control form-control-solid @error('target_waktu') is-invalid @enderror" placeholder="Ketik disini" name="target_waktu" value="{{ old('target_waktu') }}" disabled/>

                                        @error('target_waktu')
                                        <span class="invalid-feedback mb-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Pencapaian</h3>
                                    </td>
                                    <td>
                                        <input class="form-control form-control-solid" type="text" name="target_pencapaian" @error('target_pencapaian') is-invalid @enderror" placeholder="Ketik disini" value="{{ old('target_pencapaian') }}" disabled>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" name="aksi_keterangan" value="2" class="btn btn-danger btn-keterangan-penugasan" data-bs-toggle="modal" data-bs-target="#keterangan_penugasan_modal">Tolak</button>
                <button type="button" name="aksi_keterangan" value="3" class="btn btn-primary btn-keterangan-penugasan" data-bs-toggle="modal" data-bs-target="#keterangan_penugasan_modal">Terima</button>
            </div>
        </div>
    </div>
</div>