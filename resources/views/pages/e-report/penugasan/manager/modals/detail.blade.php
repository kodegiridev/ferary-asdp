<div class="modal fade" tabindex="-1" id="detail_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Detail Penugasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="table-responsive">
                        <input type="hidden" value="1" name="status">
                        <table class="table table-bordered" id="detail_data_penugasan">
                            <tbody>
                                <tr>
                                    <td class="col-4">
                                        <h3>Judul</h3>
                                    </td>
                                    <td>
                                        <p name="title"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Keterangan</h3>
                                    </td>
                                    <td>
                                        <p name="keterangan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Pengirim</h3>
                                    </td>
                                    <td>
                                        <p name="pic"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Dokumen</h3>
                                    </td>
                                    <td>
                                        <a target="_blank">Doc Penugasan</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Status</h3>
                                    </td>
                                    <td>
                                        <p name="status_penugasan"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Durasi Penyelesaian</h3>
                                    </td>
                                    <td>
                                        <p name="target_waktu"></p>
                                        {{-- <input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="form-control @error('target_waktu') is-invalid @enderror" placeholder="Ketik disini" name="target_waktu" value="{{ old('target_waktu') }}" required /> --}}
                                        <input type="date" class="form-control @error('target_waktu') is-invalid @enderror" placeholder="Ketik disini" name="target_waktu" value="{{ old('target_waktu') }}" required />

                                        @error('target_waktu')
                                        <span class="invalid-feedback mb-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3>Target Pencapaian</h3>
                                    </td>
                                    <td>
                                        <p name="target_pencapaian"></p>
                                        <input class="form-control" type="text" name="target_pencapaian" @error('target_pencapaian') is-invalid @enderror" placeholder="Ketik disini" value="{{ old('target_pencapaian') }}" required>
                                        @error('target_pencapaian')
                                        <span class="invalid-feedback mb-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="btn_keterangan" type="button" class="btn btn-danger btn-detail-keterangan-penugasan" data-bs-toggle="modal" data-bs-target="#tolak_penugasan_modal">Lihat Keterangan</button>
                    <button name="kirim" type="submit" class="btn btn-primary">Kirim</button>
                    <button name="disposisi" type="button" class="btn btn-warning btn-disposisi-penugasan" data-bs-toggle="modal" data-bs-target="#disposisi_penugasan_modal">Disposisi</button>
                    <button name="aksi_keterangan" type="button" value="2" class="btn btn-danger btn-keterangan-penugasan" data-bs-toggle="modal" data-bs-target="#keterangan_penugasan_modal">Tolak</button>
                    <button name="aksi_keterangan" type="button" class="btn btn-primary btn-terima-penugasan">Terima</button>
                </div>
            </form>
        </div>
    </div>
</div>