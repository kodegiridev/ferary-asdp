<div class="modal fade" tabindex="-1" id="disposisi_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Disposisi Penugasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('e-report.penugasan.disposisi.staff') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <div class="row">
                            <label for="user_id" class="required form-label">
                                {{ __('Pilih Staff') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="user_id" class="form-select form-select-solid @error('user_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach($staff as $m)
                                    <option value="{{ $m->id }}" {{ old('user_id') == $m->id ? 'selected' : '' }}>
                                        {{ $m->name }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('user_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>