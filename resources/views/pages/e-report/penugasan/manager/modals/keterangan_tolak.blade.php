<div class="modal fade" tabindex="-1" id="tolak_penugasan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Detail Penolakan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" value="create" name="action">
                    <input type="hidden" name="id">
                    <input type="hidden" name="status">
                    <input type="hidden" name="pembuat_id">
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan" class="required form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea type="text" class="form-control form-control-solid @error('keterangan') is-invalid @enderror" placeholder="Ketik disini" name="keterangan" value="{{ old('keterangan') }}" disabled></textarea>

                                @error('keterangan')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>