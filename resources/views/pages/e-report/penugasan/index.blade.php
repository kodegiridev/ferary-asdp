@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
    <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Penugasan') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Penugasan') }}</li>
            </ul>
        </div>
    </div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Penugasan Manajer') }}</h3>
                <div class="card-toolbar">
                    <button type="button" id="btn_create_penugasan_modal" class="btn btn-sm btn-light" data-bs-toggle="modal" data-bs-target="#create_penugasan_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="progress_manajer_report_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Judul Penugasan') }}</th>
                                <th>{{ __('Keterangan Penugasan') }}</th>
                                <th>{{ __('Pengirim') }}</th>
                                <th>{{ __('Status Penugasan') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.e-report.penugasan.modals.create')
@include('pages.e-report.penugasan.modals.edit')
@include('pages.e-report.penugasan.modals.detail')
@include('pages.e-report.penugasan.modals.keterangan')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }
    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_penugasan_modal').trigger('click');
    }
    $(document).ready(function() {
        checkSession();
        $('#progress_manajer_report_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('e-report.penugasan.index') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: true,
                },
                {
                    data: 'judul_penugasan',
                    name: 'judul_penugasan'
                },
                {
                    data: 'keterangan_penugasan',
                    name: 'keterangan_penugasan'
                },
                {
                    data: 'pembuat.name',
                    name: 'pembuat.name'
                },
                {
                    data: 'status_penugasan',
                    name: 'status_penugasan'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            "columnDefs": [{
                "targets": 2,
                "createdCell": function(td, cellData, rowData, row, col) {
                    if (rowData.status == 'ACTIVE') {
                        $(td).css('background-color', '#fff3cd')
                    } else if (rowData.status == 'EXPIRED') {
                        $(td).css('background-color', '#f8d7da')
                    }
                }
            }]
        });

        $('#progress_staff_report_table').DataTable({
            proccesing: true,
            serverSide: true,
            lengthChange: false,
            bFilter: false,
            paging: false,
            bInfo: false,
            order: [],
            ajax: {
                url: "{{ route('e-report.progress.datatables.staff') }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: true,
                },
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'deadline_at',
                    name: 'deadline_at'
                },
                {
                    data: 'pic',
                    name: 'pengirim'
                },
                {
                    data: 'assignment_status',
                    name: 'assignment_status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            "columnDefs": [{
                "targets": 2,
                "createdCell": function(td, cellData, rowData, row, col) {
                    if (rowData.status == 'ACTIVE') {
                        $(td).css('background-color', '#fff3cd')
                    } else if (rowData.status == 'EXPIRED') {
                        $(td).css('background-color', '#f8d7da')
                    }
                }
            }]
        });

        $(document).on('click', '.btn-detail-penugasan', function() {
            var url = `{{ route('e-report.penugasan.show', ':id') }}`;
            url = url.replace(':id', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    $('#detail_penugasan_modal form').prop('action', action);

                    $('#detail_penugasan_modal p[name="title"]').text(data.judul_penugasan);
                    $('#keterangan_penugasan_modal input[name="id"]').val(data.id);
                    $('#detail_penugasan_modal p[name="keterangan"]').text(data.keterangan_penugasan);
                    $('#detail_penugasan_modal p[name="pic"]').text(data.pembuat.name);
                    $('#detail_penugasan_modal a').attr('href', data.dokumen_penugasan)
                    $('#detail_penugasan_modal p[name="status_penugasan"]').text(data.status_penugasan);
                    $('#detail_penugasan_modal input[name="target_waktu"]').val(data.target_waktu);
                    $('#detail_penugasan_modal input[name="target_pencapaian"]').val(data.target_pencapaian);

                    if (data.status == 1) {
                        $('#detail_penugasan_modal button[name="aksi_keterangan"]').show();
                    } else {
                        $('#detail_penugasan_modal button[name="aksi_keterangan"]').hide();
                    }
                }
            });
        });

        $(document).on('click', '.btn-edit-penugasan', function() {
            var url = `{{ route('e-report.penugasan.detail', ':id') }}`;
            url = url.replace(':id', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    var action = `{{ route('e-report.penugasan.update', ':id') }}`;
                    action = action.replace(':id', data.id);

                    $('#edit_penugasan_modal form').prop('action', action);

                    $('#edit_penugasan_modal input[name="title"]').val(data.judul_penugasan);
                    $('#edit_penugasan_modal select[name="manager_id"]').val(data.penerima1_id);
                    $('#edit_penugasan_modal textarea[name="keterangan"]').val(data.keterangan_penugasan);
                    $('#edit_penugasan_modal input[name="dokumen"]').prop('title', data.doc_penugasan_filename);
                    $('#nama_doc').text('File yang telah diunggah: ' + data.doc_penugasan_filename);

                    if (data.status_penugasan > 0) {
                        $('#edit_penugasan_modal input[name="title"]').prop('disabled', true);
                        $('#edit_penugasan_modal select[name="manager_id"]').prop('disabled', true);
                    }
                }
            });
        });

        $(document).on('click', '.btn-delete-penugasan', function() {
            var url = `{{ route('e-report.penugasan.destroy', ':id') }}`;
            url = url.replace(':id', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-keterangan-penugasan', function(e) {
            id = $('#keterangan_penugasan_modal input[name="id"]').val();

            var url = `{{ route('e-report.penugasan.show', ':id') }}`;
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    console.log(data)
                    var action = `{{ route('e-report.penugasan.status.update', ':id') }}`;
                    action = action.replace(':id', data.id);
                    $('#keterangan_penugasan_modal form').prop('action', action);
                    $('#keterangan_penugasan_modal input[name="status"]').val(e.target.value);
                    $('#keterangan_penugasan_modal textarea[name="keterangan"]').val('');
                    if (data.tanggapan && e.target.value == 2) {
                        $('#keterangan_penugasan_modal textarea[name="keterangan"]').val(data.tanggapan[0].keterangan);
                    }

                    if (e.target.value == 2) {
                        $('#keterangan_penugasan_modal h3[id="ket"]').text('Keterangan Penolakan');
                        $('#keterangan_penugasan_modal button[name="aksi"]').attr('class', 'btn btn-danger');
                        $('#keterangan_penugasan_modal button[name="aksi"]').text('Tolak');
                    } else {
                        $('#keterangan_penugasan_modal h3[id="ket"]').text('Keterangan');
                        $('#keterangan_penugasan_modal button[name="aksi"]').attr('class', 'btn btn-primary');
                        $('#keterangan_penugasan_modal button[name="aksi"]').text('Kirim');
                    }
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function() {
            $('.is-invalid').each(function(index, element) {
                $(this).removeClass('is-invalid');
            });
        });
    })
</script>
@endsection