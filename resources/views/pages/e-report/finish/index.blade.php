@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Selesai') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Selesai') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::List-->
        @if (isStaff())
            @include('pages.e-report.finish.data.staff')
        @elseif(isManager())
            @include('pages.e-report.finish.data.manager')
        @elseif(isVP())
            @include('pages.e-report.finish.data.vp')
        @else
            @include('pages.e-report.finish.data.admin')
        @endif
        <!--end::List-->

        <!--begin::Summary-->
        @if (isStaff())
            @include('pages.e-report.finish.summaries.staff')
        @elseif(isManager())
            @include('pages.e-report.finish.summaries.manager')
        @elseif(isVP())
            @include('pages.e-report.finish.summaries.vp')
        @else
            @include('pages.e-report.finish.summaries.admin')
        @endif
        <!--end::Summary-->

        <!--begin::Calendar-->
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="card card-custom shadow-sm">
                <div class="card-body">
                    <div id="kt_docs_fullcalendar_basic"></div>
                </div>
            </div>
        </div>
        <!--end::Calendar-->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();
        });

        var KTGeneralFullCalendarBasicDemos = {
            init: function() {
                var e, t, i, n, r, o, nt;
                e = moment().startOf("day"), t = e.format("YYYY-MM"), i = e.clone().subtract(1, "day").format("YYYY-MM-DD"), n = e.format("YYYY-MM-DD"), r = e.clone().add(1, "day").format("YYYY-MM-DD"), nt = moment().toDate(), o = document.getElementById("kt_docs_fullcalendar_basic"), new FullCalendar.Calendar(o, {
                    headerToolbar: {
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay"
                    },
                    height: 800,
                    contentHeight: 780,
                    aspectRatio: 3,
                    nowIndicator: !0,
                    now: nt,
                    views: {
                        dayGridMonth: {
                            buttonText: "Bulan"
                        },
                        timeGridWeek: {
                            buttonText: "Minggu"
                        },
                        timeGridDay: {
                            buttonText: "Hari"
                        }
                    },
                    initialView: "dayGridMonth",
                    initialDate: n,
                    editable: !0,
                    dayMaxEvents: !0,
                    navLinks: !0,
                    events: "{{ route('e-report.finish.calendar') }}",
                    eventContent: function(e) {
                        var t = $(e.el);
                        e.event.extendedProps && e.event.extendedProps.description && (t.hasClass("fc-day-grid-event") ? (t.data("content", e.event.extendedProps.description), t.data("placement", "top"), KTApp.initPopover(t)) : t.hasClass("fc-time-grid-event") ? t.find(".fc-title").append('<div class="fc-description">' + e.event.extendedProps.description + "</div>") : 0 !== t.find(".fc-list-item-title").lenght && t.find(".fc-list-item-title").append(
                            '<div class="fc-description">' + e.event.extendedProps.description + "</div>"))
                    }
                }).render()
            }
        };
        KTUtil.onDOMContentLoaded((function() {
            KTGeneralFullCalendarBasicDemos.init()
        }));

        function checkSession() {
            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>

    @stack('page-scripts')
@endsection
