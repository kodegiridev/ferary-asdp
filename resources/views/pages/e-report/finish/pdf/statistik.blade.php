<!DOCTYPE html>
<html>

<head>
    <title>E-Report|Selesai-PDF</title>
    <style>
        @page {
            size: A4 landscape;
        }

        .title {
            text-align: center;
            margin-bottom: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            text-align: center;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .date-place {
            text-align: left;
            font-style: bold;
            margin-top: 10px;
            padding: 0;
        }

        /* Signature box styling */
        .signature-box {
            border-top: 1px solid black;
            padding: 8px;
            width: 200px;
            text-align: center;
        }

        .signature-role {
            font-style: bold;
        }

        .signature-name {
            font-weight: bold;
            text-decoration: underline;
        }

        .signature-container {
            width: 100%;
        }

        .signature-content {
            float: right;
        }
    </style>
</head>

<body>
    <div class="title">
        <h3>Laporan Penugasan Electronic Report</h3>
    </div>
    <table>
        <thead>
            <tr>
                <th style="max-wdith: 10px;">No.</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Jumlah Tugas</th>
                <th>Early</th>
                <th>On Time</th>
                <th>Overdue</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $i => $dt)
                <tr>
                    <td class="center">{{ $i + 1 }}</td>
                    <td class="left">{{ $dt->name }}</td>
                    <td class="left">{{ $dt->role_name }}</td>
                    <td class="center">{{ $dt->total }}</td>
                    <td class="right">{{ $dt->early . ' (' . $dt->early_pct . '%)' }}</td>
                    <td class="right">{{ $dt->on_time . ' (' . $dt->on_time_pct . '%)' }}</td>
                    <td class="right">{{ $dt->over_due . ' (' . $dt->over_due_pct . '%)' }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" style="text-align: center">
                        Data tidak ditemukan
                    </td>
                </tr>
            @endforelse

        </tbody>
    </table>

    <div class="signature-container">
        <div class="signature-content">
            <div class="date-place">
                <p>
                    Dikeluarkan: Jakarta <br>
                    Pada Tanggal: {{ now()->locale('id')->translatedFormat('d F Y') }}
                </p>
            </div>
            <div class="signature-box">
                <p class="signature-role">{{ $user->role->name }}</p>
                <img src="data:image/png;base64,{{ base64_encode($qrCode) }}" alt="Signature QR Code">
                <p class="signature-name">{{ $user->name }}</p>
            </div>
        </div>
    </div>
</body>

</html>
