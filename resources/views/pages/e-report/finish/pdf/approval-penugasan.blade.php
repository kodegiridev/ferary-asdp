<!DOCTYPE html>
<html>

<head>
    <title>E-Report|Hasil Penugasan</title>
    <style>
        @page {
            size: A4;
        }

        .title-box {
            width: 100%;
            height: 65%;
        }

        .title {
            vertical-align: middle;
            text-align: center;
            line-height: 350px;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .date-place {
            text-align: left;
            font-style: bold;
            margin-top: 10px;
            padding: 0;
        }

        /* Signature box styling */
        .signature-box {
            border-top: 1px solid black;
            padding: 8px;
            width: 200px;
            text-align: center;
        }

        .signature-role {
            font-style: bold;
        }

        .signature-name {
            font-weight: bold;
            text-decoration: underline;
        }

        .signature-container {
            width: 100%;
        }

        .signature-content {
            float: right;
        }
    </style>
</head>

<body>
    <div class="title-box">
        <div class="title">
            <h1>FILE HASIL PENUGASAN (pdf)</h1>
        </div>
    </div>
    <div class="signature-container">
        <div class="signature-content">
            <div class="date-place">
                <p>
                    Dikeluarkan: Jakarta <br>
                    Pada Tanggal: {{ now()->locale('id')->translatedFormat('d F Y') }}
                </p>
            </div>
            <div class="signature-box">
                <p class="signature-role">{{ $e_report->pembuat->role->name }}</p>
                <img src="data:image/png;base64,{{ base64_encode($qr_code) }}" alt="Signature QR Code">
                <p class="signature-name">{{ $e_report->pembuat->name }}</p>
            </div>
        </div>
    </div>
</body>

</html>
