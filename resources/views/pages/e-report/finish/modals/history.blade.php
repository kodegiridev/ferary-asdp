<div class="modal fade" tabindex="-1" id="riwayat_pekerjaan_modal">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Catatan Revisi') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>

            <form method="" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="history_report_table">
                            <thead>
                                <tr class="fw-bold fs-6 text-gray-800">
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Catatan') }}</th>
                                    <th>{{ __('Referensi') }}</th>
                                    <th>{{ __('File') }}</th>
                                    <th>{{ __('Tanggal') }}</th>
                                    <th>{{ __('Yang Harus Dilakukan') }}</th>
                                    <th>{{ __('Update Perbaikan') }}</th>
                                    <th>{{ __('Status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
