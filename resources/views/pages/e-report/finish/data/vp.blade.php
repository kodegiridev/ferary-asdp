<div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
    <div class="card shadow-sm">
        <div class="card-header">
            <h3 class="card-title">{{ __('Data E-Report') }}</h3>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="staff_report_table">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th>{{ __('No') }}</th>
                            <th>{{ __('Judul Penugasan') }}</th>
                            <th>{{ __('Tanggal') }}</th>
                            <th>{{ __('Tanggal Selesai') }}</th>
                            <th>{{ __('PIC') }}</th>
                            <th>{{ __('Status Penugasan') }}</th>
                            <th>{{ __('Status Penyelesaian') }}</th>
                            <th>{{ __('Aksi') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
        </div>
    </div>
</div>

@push('page-scripts')
    <script>
        $(document).ready(function() {
            $('#staff_report_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-report.finish.datatables') }}",
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'judul_penugasan',
                        name: 'judul_penugasan'
                    },
                    {
                        data: 'target_waktu',
                        name: 'target_waktu'
                    },
                    {
                        data: 'target_pencapaian',
                        name: 'target_pencapaian'
                    },
                    {
                        data: 'pengirim_pic',
                        name: 'pengirim_pic'
                    },
                    {
                        data: 'status_penugasan',
                        name: 'status_penugasan'
                    },
                    {
                        data: 'status_penyelesaian',
                        name: 'status_penyelesaian'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
        })
    </script>
@endpush
