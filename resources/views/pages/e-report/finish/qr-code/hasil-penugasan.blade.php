@extends('base.base')

@section('content')
    <div class="d-flex flex-column flex-root" id="kt_app_root">
        <div class="d-flex flex-column flex-center flex-column-fluid">
            <!--begin::Content-->
            <div class="d-flex flex-column flex-center p-10">
                <!--begin::Wrapper-->
                <div class="card card-flush w-lg-650px">
                    <div class="card-body">
                        <div class="text-center">
                            <!--begin::Logo-->
                            <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" class="mh-100px" alt="">
                            <!--end::Logo-->
                            <!--begin::Title-->
                            <h2 class="fw-bolder text-gray-900 my-5">{{ strtoupper(theme()->getOption('meta', 'title')) }} - QR Code Signature Validation</h2>
                            <!--end::Title-->
                        </div>
                        <div class="table-responsive mt-10">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="fw-bold fs-6 text-gray-800">
                                        Nomor ID
                                    </th>
                                    <td>
                                        {{ $data->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="fw-bold fs-6 text-gray-800">
                                        Nama Dokumen
                                    </th>
                                    <td>
                                        {{ $data->judul_penugasan }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="fw-bold fs-6 text-gray-800">
                                        Nama Approval
                                    </th>
                                    <td>
                                        {{ $data->pembuat->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="fw-bold fs-6 text-gray-800">
                                        Jabatan
                                    </th>
                                    <td>
                                        {{ $data->pembuat->role->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="fw-bold fs-6 text-gray-800">
                                        Tanggal Disetujui
                                    </th>
                                    <td>
                                        {{ $data->target_waktu }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Content-->
        </div>
    </div>
@endsection
