<div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
    <div class="card shadow-sm">
        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#kt_docs_card_collapsible">
            <h3 class="card-title">{{ __('Statistik E-Report') }}</h3>
            <div class="card-toolbar rotate-180">
                {!! theme()->getSvgIcon('icons/duotune/arrows/arr072.svg', 'svg-icon-1') !!}
            </div>
        </div>
        <div id="kt_docs_card_collapsible" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <span class="d-block font-size-sm" id="total-penugasan"></span>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="statistic_report_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Status Penyelesaian Tugas') }}</th>
                                <th>{{ __('Jumlah Tugas') }}</th>
                                <th>{{ __('Persentase (%)') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>

@push('page-scripts')
    <script>
        $(document).ready(function() {
            $('#statistic_report_table').DataTable({
                proccesing: true,
                serverSide: true,
                lengthChange: false,
                bFilter: false,
                paging: false,
                bInfo: false,
                order: [],
                ajax: {
                    url: "{{ route('e-report.finish.datatables.statistic') }}",
                    data: function(d) {
                        d.type = 'own'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'total',
                        name: 'total'
                    },
                    {
                        data: 'percentage',
                        name: 'percentage'
                    }
                ],
            }).on('xhr.dt', function(e, settings, json, xhr) {
                var total = json?.data?.reduce(function(sum, row) {
                    return sum + row.total;
                }, 0);

                $('#total-penugasan').text(`Total Penugasan Selesai: ${total || 0}`)
            });
        })
    </script>
@endpush
