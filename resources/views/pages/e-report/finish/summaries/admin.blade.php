<div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
    <div class="card shadow-sm">
        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#kt_docs_card_collapsible">
            <h3 class="card-title">{{ __('Statistik E-Report') }}</h3>
            <div class="card-toolbar rotate-180">
                {!! theme()->getSvgIcon('icons/duotune/arrows/arr072.svg', 'svg-icon-1') !!}
            </div>
        </div>
        <div id="kt_docs_card_collapsible" class="collapse show">
            <div class="card-body">
                <div class="row align-items-center mb-5">
                    <div class="col-6">
                        <h5>Ringkasan Kinerja Manager</h5>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <a href="{{ route('e-report.finish.print.assignment') }}" target="blank" class="btn btn-sm btn-primary">
                            {!! theme()->getSvgIcon('icons/duotune/general/gen016.svg', 'fs-1') !!}
                            Cetak Data
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="manager_statistic_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Manager') }}</th>
                                <th>{{ __('Jumlah Tugas Terselesaikan') }}</th>
                                <th>{{ __('Early') }}</th>
                                <th>{{ __('On Time') }}</th>
                                <th>{{ __('Overdue') }}</th>
                                <th>{{ __('Persentase (%)') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-body">
                <div class="row align-items-center mb-5">
                    <div class="col-6">
                        <h5>Ringkasan Kinerja Staff</h5>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="staff_statistic_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Staf') }}</th>
                                <th>{{ __('Jumlah Tugas Terselesaikan') }}</th>
                                <th>{{ __('Early') }}</th>
                                <th>{{ __('On Time') }}</th>
                                <th>{{ __('Overdue') }}</th>
                                <th>{{ __('Persentase (%)') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>

@push('page-scripts')
    <script>
        $(document).ready(function() {
            $('#manager_statistic_table').DataTable({
                proccesing: true,
                serverSide: true,
                lengthChange: false,
                bFilter: false,
                paging: false,
                bInfo: false,
                order: [],
                ajax: {
                    url: "{{ route('e-report.finish.datatables.statistic') }}",
                    data: function(d) {
                        d.type = 'assignee'
                        d.role = 'manager'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'assignee',
                        name: 'assignee'
                    },
                    {
                        data: 'total',
                        name: 'total'
                    },
                    {
                        data: 'early',
                        name: 'early'
                    },
                    {
                        data: 'on_time',
                        name: 'on_time'
                    },
                    {
                        data: 'over_due',
                        name: 'over_due'
                    },
                    {
                        data: 'percentage',
                        name: 'percentage'
                    }
                ],
            });

            $('#staff_statistic_table').DataTable({
                proccesing: true,
                serverSide: true,
                lengthChange: false,
                bFilter: false,
                paging: false,
                bInfo: false,
                order: [],
                ajax: {
                    url: "{{ route('e-report.finish.datatables.statistic') }}",
                    data: function(d) {
                        d.type = 'assignee'
                        d.role = 'staff'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'assignee',
                        name: 'assignee'
                    },
                    {
                        data: 'total',
                        name: 'total'
                    },
                    {
                        data: 'early',
                        name: 'early'
                    },
                    {
                        data: 'on_time',
                        name: 'on_time'
                    },
                    {
                        data: 'over_due',
                        name: 'over_due'
                    },
                    {
                        data: 'percentage',
                        name: 'percentage'
                    }
                ],
            });
        })
    </script>
@endpush
