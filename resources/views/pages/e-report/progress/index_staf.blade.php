@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Progres') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Progres') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">

        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#kt_docs_card_collapsible">
                    <h3 class="card-title">{{ __('Progres Staf') }}</h3>
                    <div class="card-toolbar rotate-180">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr072.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div id="kt_docs_card_collapsible" class="collapse show">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="progress_staff_report_table">
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800">
                                        <th>{{ __('No') }}</th>
                                        <th>{{ __('Judul Penugasan') }}</th>
                                        <th>{{ __('Tenggat Waktu') }}</th>
                                        <th>{{ __('Pengirim') }}</th>
                                        <th>{{ __('Status Penugasan') }}</th>
                                        <th>{{ __('Aksi') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('#progress_staff_report_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-report.progress.datatables.staff') }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'judul_penugasan',
                        name: 'judul_penugasan'
                    },
                    {
                        data: 'target_waktu',
                        name: 'target_waktu'
                    },
                    {
                        data: 'pengirim',
                        name: 'pengirim'
                    },
                    {
                        data: 'status_penugasan',
                        name: 'status_penugasan'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "columnDefs": [
                    {
                        "targets": 2,
                        "createdCell": function (td, cellData, rowData, row, col) {
                            if ( rowData.status == 'ACTIVE' ) {
                                $(td).css('background-color', '#fff3cd')
                            }
                            else if ( rowData.status == 'EXPIRED' ) {
                                $(td).css('background-color', '#f8d7da')
                            }
                        }
                    }    
                ]
            });
        })
    </script>
@endsection
