<div class="modal fade" tabindex="-1" id="tolak_catatan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tanggapan Perbaikan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('e-report.progress.penolakan_revision') }}" enctype="multipart/form-data">
                <input type="hidden" name="ereport_id" class="report_id">
                <input type="hidden" name="id" class="catatan_id">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea class="form-control" name="keterangan_penolakan" cols="30" rows="5" placeholder="Ketik disini" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
