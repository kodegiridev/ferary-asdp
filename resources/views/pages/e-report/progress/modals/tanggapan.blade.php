<div class="modal fade" tabindex="-1" id="tanggapi_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tanggapan Perbaikan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('e-report.progress.catatan_revisi_perbaikan') }}" enctype="multipart/form-data">
                <input type="hidden" name="id" class="catatan_id">
                <input type="hidden" name="report_id" class="report_id">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Catatan Hasil Perbaikan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea class="form-control" name="catatan_hasil_perbaikan" cols="30" rows="5" placeholder="Ketik disini" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="file" class="required form-label">
                                {{ __('Dokumen Perbaikan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="file" name="file" class="form-control" accept="application/pdf" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe" class="required form-label">
                                {{ __('Tanggal Tanggapan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text" name="tgl_hasil_perbaikan" class="form-control" value="{{ date('d-m-Y') }}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
