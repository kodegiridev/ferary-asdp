<div class="modal fade" tabindex="-1" id="tambah_revisi_modal">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Catatan Revisi') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>

            <form action="{{ route('e-report.progress.catatan_revisi') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" class="report_id">
                <div class="modal-body">
                    <div class="table-responsive">
                        <button id="btn-tambah-catatan" type="button" class="btn btn-primary btn-sm mb-2">{{ __('+ Tambah Catatan') }}</button>
                        <table class="table table-bordered">
                            <thead>
                                <tr class="fw-bold fs-6 text-gray-800">
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Catatan *') }}</th>
                                    <th>{{ __('Referensi') }}</th>
                                    <th>{{ __('File') }}</th>
                                    <th>{{ __('Tanggal') }}</th>
                                    <th>{{ __('Yang Harus Dilakukan *') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody id="catatan_revisi_table_modal">
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-tutup-modal" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
