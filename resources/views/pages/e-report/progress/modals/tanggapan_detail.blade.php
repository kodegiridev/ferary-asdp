<div class="modal fade" tabindex="-1" id="tanggapi_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tanggapan Perbaikan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="ereport_id" class="report_id">
                <input type="hidden" name="id" class="catatan_id">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Catatan Hasil Perbaikan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea class="form-control" id="catatan_hasil_perbaikan" cols="30" rows="5" placeholder="Ketik disini" readonly></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="file" class="required form-label">
                                {{ __('Dokumen Perbaikan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a href="javascript:void(0)" class="btn btn-secondary" style="width:100%" id="dok_hasil_perbaikan" target="_blank">Lihat File</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe" class="required form-label">
                                {{ __('Tanggal Tanggapan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"  class="form-control" id="tgl_hasil_perbaikan" readonly/>
                            </div>
                        </div>
                    </div>
                    <div id="div_status_perbaikan" class="form-group" style="display: none">
                        <div class="row">
                            <label class="required form-label">
                                {{ __('Status Perbaikan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"  class="form-control" id="status_perbaikan" readonly value="Perbaikan ditolak"/>
                            </div>
                        </div>
                    </div>
                    <div id="div_keterangan_penolakan" class="form-group" style="display: none">
                        <div class="row">
                            <label class="required form-label">
                                {{ __('Keterangan Penolakan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"  class="form-control" id="keterangan_penolakan" readonly/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
