@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .file-upload{display:block;text-align:center;}
        .file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload .file-select.file-select-disabled{opacity:0.65;}
        .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Progres') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('e-report.finish.index') }}" class="text-muted text-hover-primary">{{ __('Progres') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Detail Penugasan') }}</h3>
                    <div class="card-toolbar"></div>
                </div>
                <form action="{{ route('e-report.progress.upload.hasil_penugasan') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Judul</td>
                                        <td>{{ $data->judul_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td>{{ $data->keterangan_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pengirim</td>
                                        <td>{{ !empty($data->penerima2_id) ? $data->penerima1->name : $data->pembuat->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dokumen</td>
                                        <td>
                                            <a href="javascript:void(0)">{{ $data->dokumen_penugasan }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{{ $data->status_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Target Durasi Penyelesaian</td>
                                        <td>{{ $data->target_waktu }}</td>
                                    </tr>
                                    <tr>
                                        <td>Target Pencapaian</td>
                                        <td>{{ $data->target_pencapaian }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dokumen Hasil Penugasan</td>
                                        <td>
                                            @if (empty($data->dok_hasil_penugasan))
                                                <div class="file-upload">
                                                    <div class="file-select">
                                                        <div class="file-select-button" id="fileName">Unggah</div>
                                                        <div class="file-select-name" id="noFile"></div> 
                                                        <input type="file" name="chooseFile" id="chooseFile">
                                                    </div>
                                                </div>
                                            @else
                                            <a target="_blank" href="{{ asset('ereport/progres/'.$data->dok_hasil_penugasan) }}">{{ $data->dok_hasil_penugasan }}</a>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-sm btn-secondary m-1" href="{{ route('e-report.progress.index') }}">
                                {{ __('Kembali') }}
                            </a>
                            {{-- <button style="margin-left: auto !important" type="button" class="btn btn-sm btn-primary m-1 btn-riwayat-pekerjaan" data-bs-toggle="modal" data-bs-target="#riwayat_pekerjaan_modal" data-id="">
                                {{ __('Riwayat Pekerjaan') }}
                            </button> --}}
                            @if (empty($data->dok_hasil_penugasan))
                                <button type="submit" class="btn btn-sm btn-primary m-1 btn-riwayat-pekerjaan">
                                    {{ __('Kirim') }}
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@include('pages.e-report.finish.modals.history')

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $('#chooseFile').bind('change', function () {
            var filename = $("#chooseFile").val();
            if (/^\s*$/.test(filename)) {
                $(".file-upload").removeClass('active');
                $("#noFile").text(""); 
            } else {
                $(".file-upload").addClass('active');
                $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
            }
        });
        $(document).ready(function() {
            $('#history_report_table').DataTable({
                proccesing: true,
                serverSide: true,
                lengthChange: false,
                bFilter: false,
                paging: false,
                bInfo: false,
                order: [],
                ajax: {
                    url: "{{ route('e-report.finish.datatables.history') }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'note',
                        name: 'note'
                    },
                    {
                        data: 'reference',
                        name: 'reference'
                    },
                    {
                        data: 'file',
                        name: 'file'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'to_do',
                        name: 'to_do'
                    },
                    {
                        data: 'updated_file',
                        name: 'updated_file'
                    }
                ],
            });
        })
    </script>
@endsection
