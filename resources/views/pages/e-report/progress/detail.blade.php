@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .file-upload{display:block;text-align:center;}
        .file-upload .file-select{display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select .file-select-button{background:#dce4ec;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
        .file-upload .file-select:hover{border-color:#34495e;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select:hover .file-select-button{background:#34495e;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select{border-color:#3fa46a;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload.active .file-select .file-select-button{background:#3fa46a;color:#FFFFFF;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload .file-select.file-select-disabled{opacity:0.65;}
        .file-upload .file-select.file-select-disabled:hover{cursor:default;display:block;border: 2px solid #dce4ec;color: #34495e;cursor:pointer;height:40px;line-height:40px;margin-top:5px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select.file-select-disabled:hover .file-select-button{background:#dce4ec;color:#666666;padding:0 10px;display:inline-block;height:40px;line-height:40px;}
        .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('E-Report Progres') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('E-Report') }}</li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('e-report.progress.index') }}" class="text-muted text-hover-primary">{{ __('Progres') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Detail Progres') }}</h3>
                    <div class="card-toolbar"></div>
                </div>
                <form action="{{ route('e-report.progress.upload.hasil_penugasan') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Judul</td>
                                        <td>{{ $data->judul_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td>{{ $data->keterangan_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pengirim</td>
                                        <td>{{ !empty($data->penerima2_id) ? $data->penerima1->name : $data->pembuat->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dokumen</td>
                                        <td>
                                            <?php 
                                                $path = asset('upload/dokumen_penugasan'); 
                                                $real_file  = $data->dokumen_penugasan;
                                                $explode    = explode("/", $real_file);
                                                $filename   = end($explode);
                                            ?>
                                            <a href="{{ $path."/".$filename }}">{{ $filename }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{{ $data->status_penugasan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Target Durasi Penyelesaian</td>
                                        <td>{{ $data->target_waktu }}</td>
                                    </tr>
                                    <tr>
                                        <td>Target Pencapaian</td>
                                        <td>{{ $data->target_pencapaian }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dokumen Hasil Penugasan</td>
                                        <td>
                                            @if (empty($data->dok_hasil_penugasan) && $data->flow['penerima_id'] == $user['user']->id)
                                                <div class="file-upload">
                                                    <div class="file-select">
                                                        <div class="file-select-button" id="fileName">Unggah <span class="text-danger">*</span></div>
                                                        <div class="file-select-name" id="noFile"></div> 
                                                        <input type="file" name="chooseFile" id="chooseFile" accept="application/pdf" required>
                                                    </div>
                                                </div>
                                            @else
                                            <a target="_blank" href="{{ asset('upload/ereport/progres/'.$data->dok_hasil_penugasan) }}">{{ $data->dok_hasil_penugasan }}</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @if (!empty($data->keterangan_penolakan_vp))
                                    <tr>
                                        <td>Keterangan Penolakan</td>
                                        <td>{{ $data->keterangan_penolakan_vp }}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-start mt-5 mb-2">
                            <a class="btn btn-sm btn-secondary btn-sm m-1" href="{{ route('e-report.progress.index') }}">{{ __('Kembali') }}</a>
                            @if (empty($data->dok_hasil_penugasan) && $data->flow['penerima_id'] == $user['user']->id)
                                <button type="submit" class="btn btn-sm btn-primary m-1">
                                    {{ __('Kirim') }}
                                </button>
                            @endif
                            @if ($data->status_progres == 1 && $data->flow['pembuat_id'] == $user['user']->id)
                                <button style="margin-left: auto !important" type="button" class="btn btn-sm btn-danger m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#tambah_revisi_modal" data-id="">
                                    {{ __('Tolak') }}
                                </button>
                                <button type="button" class="btn btn-sm btn-primary m-1 btn-terima">
                                    {{ __('Terima') }}
                                </button>
                            @endif

                            @if ($data->status_progres == 4 && $data->flow['pembuat_id'] == $user['user']->id)
                                <button style="margin-left: auto !important" type="button" class="btn btn-sm btn-danger m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#tolak_vp_modal" data-id="">
                                    {{ __('Tolak') }}
                                </button>
                                <button type="button" class="btn btn-sm btn-primary m-1 btn-terima">
                                    {{ __('Terima') }}
                                </button>
                            @endif
                        </div>
                        @if ($data->status_progres > 1)
                        <?php $still_open = 0; ?>
                            <hr>
                            <div class="d-flex justify-content-start mt-5 mb-2">
                            <h3>Catatan Revisi</h3>
                                @if ($data->flow['pembuat_id'] == $user['user']->id && $data->status_progres != 4)
                                <button style="margin-left: auto !important" type="button" class="btn btn-sm btn-primary m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#tambah_revisi_modal" data-id="">
                                    {{ __('Tambah Catatan Revisi') }}
                                </button>
                                @endif
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="fw-bold fs-6 text-gray-600">
                                            <th>{{ __('No') }}</th>
                                            <th>{{ __('Catatan') }}</th>
                                            <th>{{ __('Referensi') }}</th>
                                            <th>{{ __('File') }}</th>
                                            <th>{{ __('Tanggal') }}</th>
                                            <th>{{ __('Yang harus dilakukan') }}</th>
                                            <th>{{ __('Status Perbaikan') }}</th>
                                            <th>{{ __('Update Perbaikan') }}</th>
                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Aksi') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($dataCatatan))
                                            @foreach ($dataCatatan as $item)
                                            <?php if ($item->status_perbaikan != 2) { $still_open = 1; } ?>
                                                <tr class="fw-bold fs-6 text-gray-800">
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->catatan }}</td>
                                                    <td>{{ $item->referensi }}</td>
                                                    <td>
                                                        @if (!empty($item->dokumen_tanggapan))
                                                        <a class="text-hover-primary" target="_blank" href="{{ asset('ereport/progres/catatan/'.$item->dokumen_tanggapan) }}">Lihat</a>
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->tanggal_pembuatan }}</td>
                                                    <td>{{ $item->arahan }}</td>
                                                    <td>
                                                        <?php $pembuat_role_name = $data->flow['pembuat_role']; if ($data->flow['pembuat_role'] == 'Vice') { $pembuat_role_name = 'Vice President'; } ?>
                                                        @if (!empty($item->dok_hasil_perbaikan) && $item->status_perbaikan == 0)
                                                            {{ 'Diajukan ke '.$pembuat_role_name }}
                                                        @elseif (empty($item->dok_hasil_perbaikan) && $item->status_perbaikan == 1)
                                                            {{ 'Diajukan ke '.$pembuat_role_name }}
                                                        @elseif (!empty($item->dok_hasil_perbaikan) && $item->status_perbaikan == 1 && !empty($item->keterangan_penolakan))
                                                            {{ 'Perbaikan Ditolak' }}    
                                                        @elseif ($item->status_perbaikan == 2)
                                                            {{ 'Sudah Disetujui' }}
                                                        @endif
                                                        {{-- {{ empty($item->dok_hasil_perbaikan) || $item->status_perbaikan == 1 ? 'Diajukan ke '.$data->flow['penerima_role'] : 'Diajukan ke '.$data->flow['pembuat_role'] }} --}}
                                                    </td>
                                                    <td>
                                                        @if (empty($item->dok_hasil_perbaikan))
                                                            <a class="text-muted">Belum ada perbaikan</a>
                                                        @else
                                                            <a href="/" class="text-hover-primary btn-detail" data-bs-toggle="modal" data-bs-target="#tanggapi_detail_modal" data-id="{{ $item->id }}">Lihat perbaikan</a>
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->status_perbaikan == 2 ? "CLOSE" : "OPEN" }}</td>
                                                    <td>
                                                        @if ((empty($item->dok_hasil_perbaikan) || $item->status_perbaikan == 1) && $data->flow['penerima_id'] == $user['user']->id)
                                                            <button type="button" class="btn btn-sm btn-primary m-1 btn-tanggapi" data-bs-toggle="modal" data-bs-target="#tanggapi_modal" data-id="{{ $item->id }}">
                                                                {{ __('Tanggapi') }}
                                                            </button>
                                                        @elseif ((!empty($item->dok_hasil_perbaikan) && $item->status_perbaikan == 0) && $data->flow['pembuat_id'] == $user['user']->id)
                                                        <div class="d-flex justify-content-start">
                                                            <button class="btn btn-sm btn-delete-note" type="button" data-id="{{ $item->id }}"><i class="fa fa-trash text-danger"></i></button>
                                                            <button class="btn btn-sm btn-reject-note" type="button"  data-bs-toggle="modal" data-bs-target="#tolak_catatan_modal" data-id="{{ $item->id }}"><i class="fa fa-xmark text-warning"></i></button>
                                                            <button class="btn btn-sm btn-approve-note" type="button" data-id="{{ $item->id }}"><i class="fa fa-check-square-o text-success"></i></button>
                                                        </div>
                                                        @elseif ($data->flow['pembuat_id'] == $user['user']->id && $item->status_perbaikan != 2)
                                                        <button class="btn btn-sm btn-delete-note" type="button" data-id="{{ $item->id }}"><i class="fa fa-trash text-danger"></i></button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            @if (($data->flow['pembuat_id'] == $user['user']->id && $data->status_progres != 4) && (!empty($dataCatatan) && $still_open == 0))
                            <div class="d-flex justify-content-end mt-5">
                                <button class="btn btn-sm btn-primary btn-approve-all-note" type="button">Setujui Semua</button>
                            </div>
                            @endif
                            
                        @endif
                        
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if (in_array($data->status_progres, [1, 2, 4]))
    <form id="form-finish" action="{{ route('e-report.progress.complete_progress') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $data->id }}">
    </form>

    <form id="form-approve-revision" action="{{ route('e-report.progress.approve_revision') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" id="catatan_id_approve">
    </form>

    <form id="form-delete-revision" action="{{ route('e-report.progress.delete_revision') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" id="catatan_id_delete">
    </form>

    <form id="form-approve-all-revision" action="{{ route('e-report.progress.approve_all_revision') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="ereport_id" value="{{ $data->id }}">
    </form>
    @endif
@endsection

@include('pages.e-report.progress.modals.catatan_revisi')
@include('pages.e-report.progress.modals.tanggapan')
@include('pages.e-report.progress.modals.tanggapan_detail')
@include('pages.e-report.progress.modals.tanggapan_tolak')
@include('pages.e-report.progress.modals.tanggapan_tolak_vp')

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: '{{ Session::get('success') }}',
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: '{{ Session::get('failed') }}',
                    icon: "error"
                });
            }
        }

        $(document).ready(function() {
            checkSession();

            var report_id = "{{ $data->id }}";
            $(".report_id").val(report_id);
            if ($('#chooseFile').length > 0) {
                $('#chooseFile').bind('change', function () {
                    var filename = $("#chooseFile").val();
                    if (/^\s*$/.test(filename)) {
                        $(".file-upload").removeClass('active');
                        $("#noFile").text(""); 
                    } else {
                        $(".file-upload").addClass('active');
                        $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
                    }
                });
            }

            $(".btn-terima").on('click', function() {
                $("#form-finish").submit();
            });
            $(".btn-approve-all-note").on('click', function() {
                $("#form-approve-all-revision").submit();
            });
            

            $(".btn-tanggapi").on('click', function() {
                var catatan_id = $(this).data('id');
                $(".catatan_id").val(catatan_id);
            });

            $(".btn-detail").on('click', function() {
                $("#div_keterangan_penolakan").hide();
                $("#keterangan_penolakan").val('');
                // var url = `{{ route('port.port.show', ':port') }}`;
                // url = url.replace(':port', $(this).data('id'));

                var url = `{{ route('e-report.progress.detail.catatan', ':id') }}`;
                url = url.replace(':id', $(this).data('id'));

                $.ajax({
                    url: url,
                    type: "GET",
                    data:{
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (data) {
                        var action = `{{ asset('ereport/progres/catatan_revisi/:filename') }}`;
                        action = action.replace(':filename', data.dok_hasil_perbaikan);

                        $('#dok_hasil_perbaikan').prop('href', action);
                        $('#catatan_hasil_perbaikan').val(data.catatan_hasil_perbaikan);
                        $('#tgl_hasil_perbaikan').val(data.tgl_hasil_perbaikan);
                        if (data.keterangan_penolakan != null) {
                            $("#keterangan_penolakan").val(data.keterangan_penolakan);
                            $("#div_keterangan_penolakan").show();
                        }
                    }
                });
            });
            
            $(".btn-approve-note").on('click', function() {
                var catatan_id = $(this).data('id');
                $("#catatan_id_approve").val(catatan_id);
                $("#form-approve-revision").submit();
            });

            $(".btn-delete-note").on('click', function() {
                var catatan_id = $(this).data('id');
                $("#catatan_id_delete").val(catatan_id);
                $("#form-delete-revision").submit();
            });

            $(".btn-reject-note").on('click', function() {
                var catatan_id = $(this).data('id');
                $(".catatan_id").val(catatan_id);
            });
        })
    </script>

    <script>
        let today = '{{ date("d-m-Y") }}'
        $("#btn-tambah-catatan").on('click', function() {
            let item_id = Math.floor(Math.random() * 1000)
            let nomor   = $(".row-tambah-catatan").length + 1
            let html = `
                <tr class='row-tambah-catatan' id='${item_id}'>
                    <td id='nomor-${item_id}'>${nomor}</td>
                    <td><input type="text" class="form-control" name="catatan[]" required /></td>
                    <td><input type="text" class="form-control" name="referensi[]"/></td>
                    <td><input type="file" class="form-control" name="file[]" accept="application/pdf"/></td>
                    <td><input type="text" class="form-control" name="tanggal[]" value="${today}" readonly/></td>
                    <td><input type="text" class="form-control" name="to_do[]" required /></td>
                    <td><button type="button" class="btn btn-sm" onclick="delete_row('${item_id}')"><i class="fa fa-trash text-danger"></i></button></td>
                </tr>
            `

            $("#catatan_revisi_table_modal").append(html)
        })
        $(".btn-tutup-modal").on('click', function() {
            $("#catatan_revisi_table_modal").empty()
        })

        function delete_row(item_id) {
            console.log(item_id)
            $("#" + item_id).remove()
            let no = 1
            $(".row-tambah-catatan").each(function(i, obj) {
                let item_id = $(this).attr('id')
                $(`#nomor-${item_id}`).text(no)
                no += 1
            })
        }
    </script>
@endsection
