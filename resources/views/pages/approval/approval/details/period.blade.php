<div class="table-responsive">
    <table class="table table-bordered" id="detail_table">
        <thead>
            <tr>
                <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Last Date') }}</th>
                <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Contractor') }}</th>
                <th class="text-center align-middle min-w-75px" colspan="3"> {{ __('Score (%)') }}</th>
                <th class="text-center align-middle min-w-125px" colspan="4">{{ __('Repair Ticket') }}</th>
                <th class="text-center align-middle" rowspan="2">{{ __('Effectiveness(%)') }}</th>
                <th class="text-center align-middle" rowspan="2">{{ __('Cost(Rp)') }}</th>
            </tr>
            <tr>
                <th class="text-center align-middle">{{ __('Before') }}</th>
                <th class="text-center align-middle">{{ __('After') }}</th>
                <th class="text-center align-middle"><i class="fa fa-caret-up"></i></th>
                <th class="text-center align-middle">{{ __('Excecute') }}</th>
                <th class="text-center align-middle">{{ __('Accept') }}</th>
                <th class="text-center align-middle">{{ __('Reject') }}</th>
                <th class="text-center align-middle">{{ __('Postpone') }}</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@push('table-scripts')
    <script>
        $(document).ready(function() {
            $('#detail_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('approval.persetujuan.detail', $data['approval']['id']) }}",
                    data: {
                        tableType: 'ongoing'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'w-20px'
                    },
                    {
                        data: 'period',
                        name: 'period'
                    },
                    {
                        data: 'contractor',
                        name: 'contractor'
                    },
                    {
                        data: 'before_score',
                        name: 'before_score'
                    },
                    {
                        data: 'after_score',
                        name: 'after_score'
                    },
                    {
                        data: 'delta_score',
                        name: 'delta_score'
                    },
                    {
                        data: 'repair_ticket_execute',
                        name: 'repair_ticket_execute'
                    },
                    {
                        data: 'repair_ticket_accept',
                        name: 'repair_ticket_accept'
                    },
                    {
                        data: 'repair_ticket_reject',
                        name: 'repair_ticket_reject'
                    },
                    {
                        data: 'repair_ticket_postpone',
                        name: 'repair_ticket_postpone'
                    },
                    {
                        data: 'effectiveness',
                        name: 'effectiveness'
                    },
                    {
                        data: 'cost',
                        name: 'cost'
                    }
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        })
    </script>
@endpush
