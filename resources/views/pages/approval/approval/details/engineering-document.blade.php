<div class="table-responsive">
    <table class="table table-bordered" id="detail_table">
        <thead>
            <tr class="fw-bold fs-6 text-gray-800">
                <th>{{ __('No') }}</th>
                <th>{{ __('Tipe Gambar') }}</th>
                <th>{{ __('Nama Gambar') }}</th>
                <th>{{ __('Nomor Gambar') }}</th>
                <th>{{ __('Status Gambar') }}</th>
                <th>{{ __('Keterangan') }}</th>
                <th>{{ __('Dokumen') }}</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

@push('table-scripts')
    <script>
        $(document).ready(function() {
            var table = $('#detail_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('approval.persetujuan.detail', $data['approval']['id']) }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'tipe_gambar',
                        name: 'tipe_gambar'
                    },
                    {
                        data: 'nama_gambar',
                        name: 'nama_gambar'
                    },
                    {
                        data: 'nomor_gambar',
                        name: 'nomor_gambar'
                    },
                    {
                        data: 'status_gambar',
                        name: 'status_gambar'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'document',
                        name: 'document',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ],
            })
        })
    </script>
@endpush
