<div class="table-responsive">
    <table class="table table-bordered" id="detail_table">
        <thead>
            <tr class="fw-bold fs-6 text-gray-800">
                <th>{{ __('No') }}</th>
                <th>{{ __('Nama Dokumen') }}</th>
                <th>{{ __('Nomor Dokumen') }}</th>
                <th>{{ __('Tgl. Dokumen') }}</th>
                <th>{{ __('Instansi') }}</th>
                <th>{{ __('Keterangan') }}</th>
                <th>{{ __('Dokumen') }}</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

@push('table-scripts')
    <script>
        $(document).ready(function() {
            var table = $('#detail_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('approval.persetujuan.detail', $data['approval']['id']) }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'nama_dokumen',
                        name: 'nama_dokumen'
                    },
                    {
                        data: 'nomor_dokumen',
                        name: 'nomor_dokumen'
                    },
                    {
                        data: 'tgl_dokumen',
                        name: 'tgl_dokumen'
                    },
                    {
                        data: 'instansi',
                        name: 'instansi'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'document',
                        name: 'document',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ],
            })
        })
    </script>
@endpush
