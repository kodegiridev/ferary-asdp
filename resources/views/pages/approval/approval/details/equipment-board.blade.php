<div class="table-responsive">
    <table class="table table-bordered" id="detail_table">
        <thead>
            <tr class="fw-bold fs-6 text-gray-800">
                <th>{{ __('No') }}</th>
                <th>{{ __('Peralatan') }}</th>
                <th>{{ __('Merek') }}</th>
                <th>{{ __('Quantity') }}</th>
                <th>{{ __('Tipe Sertifikat') }}</th>
                <th>{{ __('Diterbitkan Oleh') }}</th>
                <th>{{ __('Nomor Sertifikat') }}</th>
                <th>{{ __('Tanggal Sertifikat') }}</th>
                <th>{{ __('Tanggal Habis') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Keterangan') }}</th>
                <th>{{ __('Dokumen') }}</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

@push('table-scripts')
    <script>
        $(document).ready(function() {
            var table = $('#detail_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('approval.persetujuan.detail', $data['approval']['id']) }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'nama_peralatan',
                        name: 'nama_peralatan'
                    },
                    {
                        data: 'merek',
                        name: 'merek'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity'
                    },
                    {
                        data: 'tipe_sertifikat',
                        name: 'tipe_sertifikat'
                    },
                    {
                        data: 'diterbitkan_oleh',
                        name: 'diterbitkan_oleh'
                    },
                    {
                        data: 'nomor_sertifikat',
                        name: 'nomor_sertifikat'
                    },
                    {
                        data: 'tgl_sertifikat',
                        name: 'tgl_sertifikat'
                    },
                    {
                        data: 'tgl_habis',
                        name: 'tgl_habis'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },
                    {
                        data: 'document',
                        name: 'document',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ],
            })
        })
    </script>
@endpush
