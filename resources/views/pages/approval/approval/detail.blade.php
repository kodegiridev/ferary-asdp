@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Detail Persetujuan') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('approval.persetujuan.index') }}" class="text-muted text-hover-primary">{{ __('Persetujuan') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data Detail') }}</h3>
                    <div class="card-toolbar"></div>
                </div>
                <div class="card-body">
                    @include($data['content_at'])
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>

    @include('pages.approval.status.modals.document', $data)
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).on('click', '.btn-document', function() {
            var id = $(this).data('id');
            var table = $('#document_table').DataTable({
                proccesing: true,
                serverSide: true,
                paging: false,
                lengthChange: false,
                bInfo: false,
                destroy: true,
                order: [],
                ajax: {
                    url: `{{ route('approval.persetujuan.document-list') }}`,
                    data: function(d) {
                        d.modul = `{{ $data['approval']['sub_modul'] }}`
                        d.modul_id = id
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'file',
                        name: 'file'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ],
            });
        });
    </script>

    @stack('table-scripts')
@endsection
