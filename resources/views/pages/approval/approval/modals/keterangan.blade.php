<div class="modal fade" tabindex="-1" id="keterangan_approval_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Keterangan Data') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>
            <div class="modal-body">
                <div id="verifikators"></div>
                <div class="form-group mb-5">
                    <label>{{ __('Keterangan Pengaju') }}</label>
                    <input type="text" class="form-control form-control-solid" name="keterangan_pengaju" disabled />
                </div>
                <div class="form-group mt-5">
                    <label>{{ __('Keterangan Penyetuju') }}</label>
                    <input type="text" class="form-control form-control-solid" name="keterangan_penyetuju" disabled />
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
            </div>
        </div>
    </div>
</div>
