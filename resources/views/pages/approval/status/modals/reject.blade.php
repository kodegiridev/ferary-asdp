<div class="modal fade" tabindex="-1" id="reject_status_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('approval.status.reject', $data['approval']['id'] ?? '') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tolak') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{ __('Keterangan') }}</label>
                        <input type="text" class="form-control form-control-solid" name="keterangan_verifikator" value="{{ old('keterangan_verifikator') }}" />
                        <span class="text-muted">Pastikan kembali bahwa data valid</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">{{ __('Tolak') }}</button>
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
