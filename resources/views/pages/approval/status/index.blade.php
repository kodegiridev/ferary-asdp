@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Status') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Status') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data Status') }}</h3>
                    <div class="card-toolbar"></div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered nowrap" id="status_table">
                            <thead>
                                <tr class="fw-bold fs-6 text-gray-800">
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Dokumen') }}</th>
                                    <th>{{ __('Item') }}</th>
                                    <th>{{ __('Modul') }}</th>
                                    <th>{{ __('Pembuat') }}</th>
                                    <th>{{ __('Pemeriksa') }}</th>
                                    <th>{{ __('Tanggal') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Proses') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>

    @include('pages.approval.status.modals.keterangan')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();

            var table = $('#status_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('approval.status.index') }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'jenis_modul',
                        name: 'jenis_modul'
                    },
                    {
                        data: 'item',
                        name: 'item'
                    },
                    {
                        data: 'sub_modul',
                        name: 'sub_modul'
                    },
                    {
                        data: 'pembuat',
                        name: 'pembuat'
                    },
                    {
                        data: 'pemeriksa',
                        name: 'pemeriksa'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'proses',
                        name: 'proses',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 8, 9],
                        className: 'text-center'
                    }
                ]
            });

            $(document).on('click', '.btn-keterangan-approval', function() {
                var url = `{{ route('approval.persetujuan.keterangan', ':approval') }}`;
                url = url.replace(':approval', $(this).data('id'));

                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(data) {
                        $('#keterangan_approval_modal input[name="keterangan_pengaju"]').val(data?.keterangan_pengaju);
                        $('#keterangan_approval_modal input[name="keterangan_penyetuju"]').val(data?.keterangan_penyetuju);
                        var verifikatorsDiv = document.getElementById("verifikators");
                        while (verifikatorsDiv.firstChild) {
                            verifikatorsDiv.removeChild(verifikatorsDiv.firstChild);
                        }

                        data?.verifikators?.forEach((verifikator, i) => {
                            $("#verifikators").append(`
                                <div class="form-group mb-5 keterangan_verifikator">
                                    <label>{{ __('Keterangan Verifikator ${i+1}') }}</label>
                                    <input type="text" class="form-control form-control-solid" value="${verifikator?.keterangan_verifikator ?? ''}" disabled />
                                </div>
                            `);
                        });
                    }
                });
            });
        })

        $(document).on('click', '.btn-delete-approval', function() {
            var url = `{{ route('approval.status.delete', ':approval') }}`;
            url = url.replace(':approval', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>
@endsection
