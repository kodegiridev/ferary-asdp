@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Detail Status') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('approval.status.index') }}" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('approval.status.index') }}" class="text-muted text-hover-primary">{{ __('Status') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data Detail') }}</h3>
                    <div class="card-toolbar">
                        @if ($data['can_approve_reject'])
                            <button type="button" id="btn_approve_status_modal" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#approve_status_modal">
                                {{ __('Terima') }}
                            </button>
                            &nbsp;&nbsp;
                            <button type="button" id="btn_reject_status_modal" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#reject_status_modal">
                                {{ __('Tolak') }}
                            </button>
                        @else
                            <div></div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @include($data['content_at'])
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>

    @include('pages.approval.status.modals.approve', $data)
    @include('pages.approval.status.modals.reject', $data)
    @include('pages.approval.status.modals.document')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();
        })

        $(document).on('click', '.btn-document', function() {
            var id = $(this).data('id');
            var table = $('#document_table').DataTable({
                proccesing: true,
                serverSide: true,
                paging: false,
                lengthChange: false,
                bInfo: false,
                destroy: true,
                order: [],
                ajax: {
                    url: `{{ route('approval.persetujuan.document-list') }}`,
                    data: function(d) {
                        d.modul = `{{ $data['approval']['sub_modul'] }}`
                        d.modul_id = id
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'file',
                        name: 'file'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ],
            });
        });

        function checkSession() {
            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>

    @stack('table-scripts')
@endsection
