@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Log Aktivitas') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Log Aktivitas') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm p-0">
            <div class="accordion" id="kt_accordion_1">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="kt_accordion_1_header_1">
                        <button class="accordion-button fs-4 fw-semibold" type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#kt_accordion_1_body_1"
                        aria-expanded="true"
                        aria-controls="kt_accordion_1_body_1">
                            <i class="fa fa-filter me-3"></i>
                            {{ __('Filter') }}
                        </button>
                    </h2>
                    <div id="kt_accordion_1_body_1" class="accordion-collapse collapse show"
                    aria-labelledby="kt_accordion_1_header_1"
                    data-bs-parent="#kt_accordion_1">
                        <div class="accordion-body">
                            <form id="filter_form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="description" class="form-label">
                                                    {{ __('Nama Aktivitas') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="text"
                                                    class="form-control form-control-solid"
                                                    name="description"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="causer_id" class="form-label">
                                                    {{ __('Dilakukan oleh') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <select name="causer_id"
                                                    class="form-select form-select-solid">
                                                        <option value="">Pilih data</option>
                                                        @foreach ($users as $user)
                                                            <option value="{{ $user->id }}">
                                                                {{ $user->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="start_date" class="form-label">
                                                    {{ __('Dari Tanggal') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="date"
                                                    class="form-control form-control-solid"
                                                    name="start_date"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="end_date" class="form-label">
                                                    {{ __('Sampai Tanggal') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="date"
                                                    class="form-control form-control-solid"
                                                    name="end_date"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-footer mt-4">
                                    <button class="btn btn-sm btn-primary" type="button" id="submit_filter">
                                        {{ __('Filter') }}
                                    </button>
                                    <button class="btn btn-sm btn-warning" type="button" id="reset_filter">
                                        {{ __('Reset') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Log Aktivitas') }}</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="activity_log_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Aktivitas') }}</th>
                                <th>{{ __('Dilakukan oleh') }}</th>
                                <th>{{ __('Waktu') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var table = $('#activity_log_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('activity-log.index') }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'causer_id',
                    name: 'causer_id'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $(document).on('click', '#submit_filter', function () {
            var description = $('input[name="description"]').val();
            var causer_id = $('select[name="causer_id"]').val();
            var start_date = $('input[name="start_date"]').val();
            var end_date = $('input[name="end_date"]').val();

            var url = `{{ route('activity-log.index', [
                'description' => 'description_value',
                'causer_id' => 'causer_id_value',
                'start_date' => 'start_date_value',
                'end_date' => 'end_date_value'
            ]) }}`;
            url = url.replace('description_value', description);
            url = url.replace('causer_id_value', causer_id);
            url = url.replace('start_date_value', start_date);
            url = url.replace('end_date_value', end_date);
            url = url.replaceAll('amp;', '');

            table.ajax.url(url).load();
        });

        $(document).on('click', '#reset_filter', function () {
            $('input[name="description"]').val('');
            $('select[name="causer_id"]').val('').trigger('change');
            $('input[name="start_date"]').val('');
            $('input[name="end_date"]').val('');

            var url = "{{ route('activity-log.index') }}";

            table.ajax.url(url).load();
        });
    })
</script>
@endsection