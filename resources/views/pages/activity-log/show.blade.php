@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data User') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                    <a href="{{ route('activity-log.index') }}"
                    class="text-muted text-hover-primary">
                        {{ __('Log Aktivitas') }}
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Detail Log Aktivitas') }}</h3>
                <div class="card-toolbar">
                    <a href="{{ url()->previous() }}"
                    class="btn btn-sm btn-light">
                        <i class="fa fa-arrow-left"></i>
                        {{ __(' Kembali') }}
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="detail_activity_log_table">
                        <tr>
                            <td class="fw-bold fs-6 text-gray-800">
                                {{ __('Nama Aktivitas') }}
                            </td>
                            <td>
                                {{ $activityLog->description }}
                            </td>
                        </tr>
                        <tr>
                            <td class="fw-bold fs-6 text-gray-800">
                                {{ __('Dilakukan oleh') }}
                            </td>
                            <td>
                                {{ $activityLog->user->name }}
                            </td>
                        </tr>
                        <tr>
                            <td class="fw-bold fs-6 text-gray-800">
                                {{ __('Waktu') }}
                            </td>
                            <td>
                                {{
                                    \Carbon\Carbon::parse($activityLog->created_at)
                                    ->locale('id')
                                    ->translatedFormat('d F Y H:i:s') ?? ''
                                }}
                            </td>
                        </tr>
                        <tr>
                            <td class="fw-bold fs-6 text-gray-800">
                                {{ __('Properti') }}
                            </td>
                            <td>
                                <span class="hidden-properties" hidden>{{ $activityLog->properties }}</span>
                                <pre class="log-properties">
                                </pre>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var json = JSON.parse($('.hidden-properties').text());
        $('.log-properties').html(JSON.stringify(json, undefined, 4));
    })
</script>
@endsection