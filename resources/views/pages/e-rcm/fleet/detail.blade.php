@extends('layout.demo1.master')

@inject('ctrl', '\App\Http\Controllers\Controller')

@section('styles')
    <link href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    @include('/layout/partials/babylon')
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Detail Fleet Electronic Record Condition Monitoring') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item">
                <a href="{{ route('e-rcm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('e-RCM') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
                <a href="{{ route('e-rcm.fleet.index') }}" class="breadcrumb-item text-muted text-hover-primary">{{ __('Fleet') }}</a>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
                <li class="breadcrumb-item">{{ $fleet->nama }}</li>
            </ul>
        </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid h-min">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered nowrap">
                                <tbody>
                                    <tr class="bg-black text-white">
                                        <td>Tonate (Ton)</td>
                                        <td>:</td>
                                        <td>2</td>
                                    </tr>
                                    <tr>
                                        <td>Panjang (m)</td>
                                        <td>:</td>
                                        <td>{{ $fleet->panjang }}</td>
                                    </tr>
                                    <tr>
                                        <td>Usia Teknis</td>
                                        <td>:</td>
                                        <td>{{ $fleet->usia_teknis }}</td>
                                    </tr>
                                    <tr>
                                        <td>Survey status</td>
                                        <td>:</td>
                                        <td>{{ $fleet->status_survey }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div id="container"></div>
                            <div class="bg-primary h-full col-md-6 mx-auto p-2 text-center">
                                <p class="my-auto fw-bold">Cost/Condition: Rp 20.000.000</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-10">
                        <div class="col-md-12">
                            <h1>{{ $fleet->nama }}</h1>
                            <div id="canvasZone"><canvas id="modelCanvas1" style="touch-action: none; width: 100%;" ></canvas></div>
                        </div>
                    </div>
                    <div class="form-group row mt-10">
                        <div class="col-md-3">
                            <label for="status" class="form-label">
                                {{ __('Status') }}
                            </label>
                            <select
                                name="status"
                                class="form-select form-select-solid"
                            >
                                <option value="close">Close</option>
                                <option value="ongoing">On Going</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="status" class="form-label">
                                {{ __('Period') }}
                            </label>
                            <select
                                name="status"
                                class="form-select form-select-solid"
                            >
                                <option value="10-10-2023">10-10-2023</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="status" class="form-label">
                                {{ __('Stage') }}
                            </label>
                            <select
                                name="status"
                                class="form-select form-select-solid"
                            >
                                <option value="after">After</option>
                                <option value="before">Before</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="status" class="form-label">
                                {{ __('System') }}
                            </label>
                            <select
                                name="status"
                                class="form-select form-select-solid"
                            >
                                <option value="hull">Hull Construction</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="kt_app_table_ongoing_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                 <h3 class="card-title font-extrabold">{{ __('On Going') }}</h3>
                </div>
                <div class="card-body">
                    <div class="flex mb-10">
                        <button type="submit" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#create_periode_modal">{{ __('+ Tambah') }}</button>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="ongoing_table">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                                    <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Last Date') }}</th>
                                    <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Contractor') }}</th>
                                    <th class="text-center align-middle min-w-75px"  colspan="3"> {{ __('Score (%)') }}</th>
                                    <th class="text-center align-middle min-w-125px" colspan="4">{{ __('Repair Ticket') }}</th>
                                    <th class="text-center align-middle" rowspan="2">{{ __('Effectiveness(%)') }}</th>
                                    <th class="text-center align-middle" rowspan="2">{{ __('Cost(Rp)') }}</th>
                                    <th class="text-center align-middle min-w-75px" rowspan="2">{{ __('Status') }}</th>
                                    <th class="text-center align-middle min-w-200px" rowspan="2">{{ __('Aksi') }}</th>
                                    <th class="text-center align-middle min-w-100px" rowspan="2"></th>
                                </tr>
                                <tr>
                                    <th>{{ __('Before') }}</th>
                                    <th>{{ __('After') }}</th>
                                    <th><i class="fa fa-caret-up"></i></th>
                                    <th>{{ __('Excecute') }}</th>
                                    <th>{{ __('Accept') }}</th>
                                    <th>{{ __('Reject') }}</th>
                                    <th>{{ __('Postpone') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-xl-end mt-10">
                        <button href="#" class="btn btn-primary" id="submit-approval">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="kt_app_table_record_realibity_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('Record Realibilty Gallery') }}</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="record_table">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                                    <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Last Date') }}</th>
                                    <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Contractor') }}</th>
                                    <th class="text-center align-middle min-w-75px"  colspan="3"> {{ __('Score (%)') }}</th>
                                    <th class="text-center align-middle min-w-125px" colspan="4">{{ __('Repair Ticket') }}</th>
                                    <th class="text-center align-middle" rowspan="2">{{ __('Effectiveness(%)') }}</th>
                                    <th class="text-center align-middle" rowspan="2">{{ __('Cost(Rp)') }}</th>
                                    <th class="text-center align-middle min-w-75px" rowspan="2">{{ __('Status') }}</th>
                                    <th class="text-center align-middle min-w-200px" rowspan="2">{{ __('Aksi') }}</th>
                                </tr>
                                <tr>
                                    <th>{{ __('Before') }}</th>
                                    <th>{{ __('After') }}</th>
                                    <th><i class="fa fa-caret-up"></i></th>
                                    <th>{{ __('Excecute') }}</th>
                                    <th>{{ __('Accept') }}</th>
                                    <th>{{ __('Reject') }}</th>
                                    <th>{{ __('Postpone') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('e-rcm.fleet.index') }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    @include('pages.e-rcm.fleet.modals.create_periode')
    @include('pages.e-rcm.fleet.modals.edit_periode')
    @include('pages.e-rcm.fleet.modals.submit_periode')
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();

            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 1,
                prefix: "Rp. "
            }).mask(".nilai_project");

            $(document).on('click', '#btn_add_verificator', function() {
                var modal = $(this).closest('.modal');
                var boxInputVerificator = modal.find('.box-input-verificator');
                var options = ''

                $('select[name="user_verifikator_id[]"]').each(function(index, element) {
                    if (index == 0) {
                        options = $(this).html();
                    }
                });

                boxInputVerificator.append(`
                    <div class="row input-verificator mt-2">
                        <div class="col-10">
                            <select name="user_verifikator_id[]" class="form-select form-select-solid verificator">
                                ${options}
                            </select>
                        </div>
                        <div class="col-1 d-flex align-items-center">
                            <button type="button" class="btn btn-sm btn-danger btn-delete-verificator">
                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr090.svg', 'svg-icon-3') !!}
                            </button>
                        </div>
                    </div>
                `);
            });

            $(document).on('click', '.btn-delete-verificator', function() {
                var inputVerificator = $(this).closest('.input-verificator');
                var selectElement = inputVerificator.find('select').val();

                if (selectElement != '') {
                    var approver = $('select[name="penyetuju_id"]');
                    var verificator = $('select[name="user_verifikator_id[]"]');

                    $.each(approver.find('option'), function(key, value) {
                        if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                            $(this).prop('hidden', false);
                        }
                    });

                    verificator.each(function(index, element) {
                        var selectVerificator = $(this);
                        $.each(selectVerificator.find('option'), function(key, value) {
                            if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                                $(this).prop('hidden', false);
                            }
                        });
                    });
                }
                inputVerificator.remove();
            });

            var tableOngoing = $('#ongoing_table').DataTable({
              proccesing: true,
              serverSide: true,
              order: [],
              ajax: {
                  url: "{{ route('e-rcm.fleet.show', [request()->id]) }}",
                  data: { tableType: 'ongoing' }
              },
              columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: 'w-20px'
                  },
                  {
                      data: 'period',
                      name: 'period'
                  },
                  {
                      data: 'contractor',
                      name: 'contractor'
                  },
                  {
                      data: 'before_score',
                      name: 'before_score'
                  },
                  {
                      data: 'after_score',
                      name: 'after_score'
                  },
                  {
                      data: 'delta_score',
                      name: 'delta_score'
                  },
                  {
                      data: 'repair_ticket_execute',
                      name: 'repair_ticket_execute'
                  },
                  {
                      data: 'repair_ticket_accept',
                      name: 'repair_ticket_accept'
                  },
                  {
                      data: 'repair_ticket_reject',
                      name: 'repair_ticket_reject'
                  },
                  {
                      data: 'repair_ticket_postpone',
                      name: 'repair_ticket_postpone'
                  },
                  {
                      data: 'effectiveness',
                      name: 'effectiveness'
                  },
                  {
                      data: 'cost',
                      name: 'cost'
                  },
                  {
                      data: 'status',
                      name: 'status'
                  },
                  {
                      data: 'action',
                      name: 'action',
                      orderable: false,
                      searchable: false,
                      className: 'w-200px'
                  },
                  {
                      data: 'checkbox',
                      name: 'checkbox',
                      orderable: false,
                      searchable: false,
                  },
              ],
              columnDefs: [{
                  target: '_all',
                  className: 'text-center align-middle'
              }]
            });

            var tableRecord = $('#record_table').DataTable({
              proccesing: true,
              serverSide: true,
              order: [],
              ajax: {
                  url: "{{ route('e-rcm.fleet.show', [request()->id]) }}",
                  data: { tableType: 'record' }
              },
              columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: 'w-20px'
                  },
                  {
                      data: 'period',
                      name: 'period'
                  },
                  {
                      data: 'contractor',
                      name: 'contractor'
                  },
                  {
                      data: 'before_score',
                      name: 'before_score'
                  },
                  {
                      data: 'after_score',
                      name: 'after_score'
                  },
                  {
                      data: 'delta_score',
                      name: 'delta_score'
                  },
                  {
                      data: 'repair_ticket_execute',
                      name: 'repair_ticket_execute'
                  },
                  {
                      data: 'repair_ticket_accept',
                      name: 'repair_ticket_accept'
                  },
                  {
                      data: 'repair_ticket_reject',
                      name: 'repair_ticket_reject'
                  },
                  {
                      data: 'repair_ticket_postpone',
                      name: 'repair_ticket_postpone'
                  },
                  {
                      data: 'effectiveness',
                      name: 'effectiveness'
                  },
                  {
                      data: 'cost',
                      name: 'cost'
                  },
                  {
                      data: 'status',
                      name: 'status'
                  },
                  {
                      data: 'action',
                      name: 'action',
                      orderable: false,
                      searchable: false,
                      className: 'w-200px'
                  }
              ],
              columnDefs: [{
                  target: '_all',
                  className: 'text-center align-middle'
              }]
            });

            // setTimeout(() => {
            //     $('button.edit-data-period').on('click', function (e) {
            //         // Make sure the click of the button doesn't perform any action
            //         e.preventDefault();

            //         // Get the modal by ID
            //         var modal = $('#edit_periode_modal');

            //         // Set the value of the input fields
            //         modal.find('#vessel_period_id').val($(this).data('id'));
            //         modal.find('#contractor').val($(this).data('contractor'));
            //         modal.find('#effectiveness').val($(this).data('effectiveness'));
            //         modal.find('#cost').val($(this).data('cost'));

            //         if ($(this).data('effectiveness_doc')) {
            //             modal.find('.effectiveness_doc').css('display', 'block');
            //             modal.find('.effectiveness_doc').attr('href', $(this).data('effectiveness_doc'));
            //             modal.find('.effectiveness_doc').attr('target', '_blank');
            //         } else {
            //             modal.find('.effectiveness_doc').css('display', 'none');
            //         }

            //         if ($(this).data('cost_doc')) {
            //             modal.find('.cost_doc').css('display', 'block');
            //             modal.find('.cost_doc').attr('href', $(this).data('cost_doc'));
            //             modal.find('.cost_doc').attr('target', '_blank');
            //         } else {
            //             modal.find('.cost_doc').css('display', 'none');
            //         }

            //         // Update the action of the form
            //         modal.find('#form_edit_periode_modal').attr('action', "{{ route('e-rcm.fleet.upsert-period') }}");

            //     });
            // }, 1000);

            $(document).on('click', 'button.edit-data-period', function(e) {
                e.preventDefault();

                // Get the modal by ID
                var modal = $('#edit_periode_modal');

                // Set the value of the input fields
                modal.find('#vessel_period_id').val($(this).data('id'));
                modal.find('#contractor').val($(this).data('contractor'));
                modal.find('#effectiveness').val($(this).data('effectiveness'));
                modal.find('#cost').val($(this).data('cost'));

                if ($(this).data('effectiveness_doc')) {
                    modal.find('.effectiveness_doc').css('display', 'block');
                    modal.find('.effectiveness_doc').attr('href', $(this).data('effectiveness_doc'));
                    modal.find('.effectiveness_doc').attr('target', '_blank');
                    modal.find('input[name="effectiveness_doc"]').removeAttr('required');
                    modal.find('label[for="effectiveness_doc"]').removeClass('required');
                } else {
                    modal.find('label[for="effectiveness_doc"]').addClass('required');
                    modal.find('input[name="effectiveness_doc"]').attr('required', true);
                    modal.find('.effectiveness_doc').css('display', 'none');
                }

                if ($(this).data('cost_doc')) {
                    modal.find('.cost_doc').css('display', 'block');
                    modal.find('.cost_doc').attr('href', $(this).data('cost_doc'));
                    modal.find('.cost_doc').attr('target', '_blank');
                    modal.find('input[name="cost_doc"]').removeAttr('required');
                    modal.find('label[for="cost_doc"]').removeClass('required');
                } else {
                    modal.find('label[for="cost_doc"]').addClass('required');
                    modal.find('input[name="cost_doc"]').attr('required', true);
                    modal.find('.cost_doc').css('display', 'none');
                }

                // Update the action of the form
                modal.find('#form_edit_periode_modal').attr('action', "{{ route('e-rcm.fleet.upsert-period') }}");
            });

            $(document).on('click', '.btn-delete', function() {
                Swal.fire({
                    title: "{{ __('Apakah anda yakin?') }}",
                    text: "{{ __('Data akan terhapus secara permanen!') }}",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Ya') }}",
                    cancelButtonText: "{{ __('Batal') }}",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('e-rcm.fleet.delete-period') }}",
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id": $(this).data('id')
                            },
                            success: function(res) {
                                location.reload();
                            }
                        });
                    }
                });
            });


        });

        $(document).on('click', '#submit-approval', function() {
            var selected = $('input[name="flexRadioChecked"]:checked').val();
            if (!selected) {
                Swal.fire({
                    text: 'Pilih satu period untuk di sumbit!',
                    icon: "error"
                });
            } else {
                $('#submit_periode_modal input[name="ercm_period_id"]').val(selected);
                $('#submit_periode_modal').modal('show');
            }
        });


        var highChart = Highcharts.chart('container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Realibility Performance Summary'
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                accessibility: {
                    description: 'Months of the year'
                }
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                labels: {
                    format: '{value}°'
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'After',
                data: [5.2, 5.7, 8.7, 13.9, 18.2, 21.4, 25.0, {
                    y: 26.4,
                    accessibility: {
                        description: 'Sunny symbol, this is the warmest point in the chart.'
                    }
                }, 22.8, 17.5, 12.1, 7.6]

            },{
                name: 'Minimum',
                data: [{
                    y: 1.5,
                    accessibility: {
                        description: 'Snowy symbol, this is the coldest point in the chart.'
                    }
                }, 1.6, 3.3, 5.9, 10.5, 13.5, 14.5, 14.4, 11.5, 8.7, 4.7, 2.6]
            }, {
                name: 'Before',
                data: [{
                    y: 1.5,
                    accessibility: {
                        description: 'Snowy symbol, this is the coldest point in the chart.'
                    }
                }, 2, 1, 1, 12, 4, 10, 4, 1, 1, 1, 1]
            }]
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>
    <script href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script type="module" src="{{ asset('vendor/model-viewer/model-viewer.min.js') }}"></script>
    <script>
        var asset = "{{ asset('e-rcm/sample/') }}";

        var startRenderLoop = function (engine, scene) {
            engine.runRenderLoop(function () {
                if (scene && scene.activeCamera) {
                    scene.render();
                }
            });
        };

        var createDefaultEngine = function (canvas) {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };

        var createScene = async function (canvasId) {
            const canvas = document.getElementById(canvasId);
            const engine = createDefaultEngine(canvas);
            const scene = new BABYLON.Scene(engine);

            await BABYLON.SceneLoader.AppendAsync(`${asset}/`, "hull-construction.glb", scene);

            scene.createDefaultCameraOrLight(true, true, true);
            scene.activeCamera.alpha += Math.PI;
            scene.clearColor = BABYLON.Color3.White();

            startRenderLoop(engine, scene);

            window.addEventListener("resize", function () {
                engine.resize();
            });

            return { engine, scene };
        };

        createScene("modelCanvas1");
    </script>
@endsection
