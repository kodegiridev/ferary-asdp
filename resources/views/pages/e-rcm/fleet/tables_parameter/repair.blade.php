<form id="form-repair" enctype="multipart/form-data">
    @csrf
    <div class="mt-10 d-flex gap-4">
        <button type="button" class="btn btn-icon-black text-black" style="background-color:#f2ac13;" id="exportButton">
            <i class="fa fa-file text-black" aria-hidden="true"></i>
            {{ __('Export') }}
        </button>
        <button type="button" class="btn btn-icon-black btn-dark" id="refreshButton">
            <i class="fa fa-arrows-rotate"></i>
        </button>
        <button type="button" class="btn btn-icon-black btn-success" id="printButton">
            <i class="fa fa-print"></i>
        </button>
        <button type="button" class="btn btn-icon-black btn-warning" data-bs-toggle="modal" data-bs-target="#riwayat_pengajuan_modal" id="infoButton">
            <i class="fa fa-info-circle"></i>
        </button>
    </div>
    <div class="table-responsive mt-10">
        <table class="table table-bordered nowrap" id="repair_table">
            <thead style="background-color:#3cc9f0;">
                <th style="color:black !important;" class="p-5">{{ __('No') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Group') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Sub Group') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Component') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Score') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Status') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Task List') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Classification') }}</th>
                <th style="color:black !important;" class="p-5 min-w-150px">{{ __('Status') }}</th>
                <th style="color:black !important;" class="p-5">{{ __('Status Approved') }}</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="mt-10 d-flex justify-content-end gap-4">
        <button type="submit" id="save-draft-initial" class="btn btn-warning text-black" id="resetButton">{{ __('Save as a Draft') }}</button>
        <button type="button" id="openModalSubmitButton" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#submit_system_modal">{{ __('Submit') }}</button>
    </div>
</form>
@include('pages.e-rcm.fleet.modals.create_tasklist')