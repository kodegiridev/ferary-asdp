<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_tasklist_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <form class="form" enctype="multipart/form-data" id="form-tasklist">
            @csrf
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Task list') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>
            <div class="modal-body">
              <button type="button" class="btn btn-warning mb-4">{{ __('+ Tambah') }}</button>
              <div>
                <table class="table table-bordered nowrap" id="tasklist_table">
                  <thead>
                      <th>{{ __('No') }}</th>
                      <th>{{ __('Task List') }}</th>
                      <th>{{ __('Aksi') }}</th>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-dark" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
                <button type="submit" id="submitAddTaskList" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
            </div>
          </form>
        </div>
    </div>
</div>
