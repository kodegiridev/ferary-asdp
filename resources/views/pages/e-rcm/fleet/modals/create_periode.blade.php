<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_periode_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('e-rcm.fleet.upsert-period') }}" class="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Periode') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <label for="contractor" class="required form-label">
                                {{ __('Contractor') }}
                            </label>
                            <input type="text"
                            class="form-control form-control-solid required"
                            placeholder="Ketik disini" name="contractor"/>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-6">
                            <label for="effectiveness" class="required form-label">
                                {{ __('Effectiveness (%)') }}
                            </label>
                            <input type="number"
                            class="form-control form-control-solid required"
                            placeholder="Ketik disini" name="effectiveness"/>
                        </div>
                          <div class="col-6">
                            <label for="effectiveness_doc" class="form-label required">
                                {{ __('Document') }}
                            </label>
                            <input name="effectiveness_doc" type="file" class="form-control required" accept="pdf/*" required/>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-6">
                            <label for="cost" class="required form-label">
                                {{ __('Cost (Rp)') }}
                            </label>
                            <input type="text"
                            class="form-control form-control-solid nilai_project required"
                            placeholder="Ketik disini" name="cost"/>
                        </div>
                          <div class="col-6">
                            <label for="cost_doc" class="form-label required">
                                {{ __('Document') }}
                            </label>
                            <input name="cost_doc" type="file" class="form-control required" accept="pdf/*" required/>
                        </div>
                    </div>
                </div>
                 <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
                <input type="hidden" name="ercm_id" value="{{ request()->id }}" />
                <input type="hidden" name="post_type" value="create" />
            </form>
        </div>
    </div>
</div>
