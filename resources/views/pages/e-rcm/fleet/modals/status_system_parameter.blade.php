<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="status_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="d-flex flex-column p-10">
              <h1 class="mb-10">Status</h1>
              <div>
                <table class="table table-bordered nowrap">
                  <thead>
                      <th>{{ __('No') }}</th>
                      <th>{{ __('Group') }}</th>
                      <th>{{ __('Sub Group') }}</th>
                      <th>{{ __('Status') }}</th>
                  </thead>
                  <tbody>
                    <td>1</td>
                    <td>Framing</td>
                    <td>Keel</td>
                    <td>Initial</td>
                  </tbody>
                </table>
              </div>
              <div class="d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
              </div>
            </div>
        </div>
    </div>
</div>
