<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="submit_system_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form-executive" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Submit Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <label for="approved" class="required form-label">
                                {{ __('Approved') }}
                            </label>
                            <select 
                                name="approved"
                                id="approved"
                                class="form-select form-select-solid"
                            >
                            <option value="">Pilih data</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row align-items-center justify-content-between my-3">
                        <div class="col-6">
                            <label for="user_verifikator_id" class="required form-label">
                                {{ __('Verifikator') }}
                            </label>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="box-input-verificator @error('user_verifikator_id.*') is-invalid @enderror">
                            <div class="row input-verificator">
                                <div class="col-9">
                                    <select name="user_verifikator_id[]" class="form-select form-select-solid verificator" required>
                                        <option value="">Pilih data</option>
                                            <option value="1">
                                                Andi
                                            </option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <button type="button" class="btn btn-sm btn-dark float-end" id="btn_add_verificator">
                                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-3') !!}
                                        {!! theme()->getSvgIcon('icons/duotune/communication/com013.svg', 'svg-icon-1') !!}
                                    </button>
                                </div>
                            </div>
                        </div>
                        @error('user_verifikator_id.*')
                            <span class="invalid-feedback mb-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <label for="remarks" class="required form-label">
                                {{ __('Remarks') }}
                            </label>
                            <input type="text"
                            id="remarks"
                            class="form-control form-control-solid required"
                            placeholder="Ketik disini" name="remarks"/>
                        </div>
                    </div>
                 <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" id="submitButtonInitial" class="btn btn-sm btn-primary">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
