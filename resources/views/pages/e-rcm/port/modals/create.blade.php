<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('e-rcm.port.store') }}" class="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Kapal') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <label for="port_id" class="required form-label">
                        {{ __('Kapal') }}
                    </label>
                    <select
                        id="port_id"
                        name="port_id"
                        class="form-select form-select-solid"
                    >
                    <option value="">Pilih Pelabuhan</option>
                    @foreach ($ports as $port)
                        <option value="{{ $port->id }}">{{ $port->nama }}</option>
                    @endforeach
                    </select>
                </div>
                 <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
