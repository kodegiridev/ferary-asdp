<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="riwayat_pengajuan_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="d-flex flex-column p-10">
              <h1 class="mb-10">Riwayat Pengajuan</h1>

              <div>
                <div class="row">
                  <p class="fw-bolder col-md-2">PIC</p>
                  <p class="fw-bolder col-md-1">:</p>
                  <p class="fw-bolder col-md-9">Annisa K</p>
                </div>
  
                <div class="row">
                  <p class="fw-bolder col-md-2">Date</p>
                  <p class="fw-bolder col-md-1">:</p>
                  <p class="fw-bolder col-md-9">07-10-2023</p>
                </div>
  
                <div class="row">
                  <p class="fw-bolder col-md-2">Remarks</p>
                  <p class="fw-bolder col-md-1">:</p>
                  <p class="fw-bolder col-md-9">Mohon direview dan ditanggapi</p>
                </div>
  
                <div class="row">
                  <p class="fw-bolder col-md-2  ">Status</p>
                  <p class="fw-bolder col-md-1">:</p>
                  <p class="fw-bolder col-md-9">Draft</p>
                </div>
              </div>

              <div>
                <table class="table table-bordered nowrap">
                  <thead>
                      <th>{{ __('No') }}</th>
                      <th>{{ __('Verificator') }}</th>
                      <th>{{ __('Status') }}</th>
                      <th>{{ __('Date') }}</th>
                      <th>{{ __('Remarks') }}</th>
                  </thead>
                  <tbody>
                    <td>1</td>
                    <td>Budi</td>
                    <td>Waiting for Approval</td>
                    <td>12-02-2023</td>
                    <td>-</td>
                  </tbody>
                </table>
              </div>

              <div>
                <table class="table table-bordered nowrap">
                  <thead>
                      <th>{{ __('No') }}</th>
                      <th>{{ __('Approval') }}</th>
                      <th>{{ __('Status') }}</th>
                      <th>{{ __('Date') }}</th>
                      <th>{{ __('Remarks') }}</th>
                  </thead>
                  <tbody>
                    <td>1</td>
                    <td>Kusno</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                  </tbody>
                </table>
              </div>

              <div class="d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
              </div>
            </div>
        </div>
    </div>
</div>
