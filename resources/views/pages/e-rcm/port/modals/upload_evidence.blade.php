<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="upload_evidence_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <form class="form" enctype="multipart/form-data" id="form-evidence">
            @csrf
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Evidence: K5') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>
            <div class="modal-body">
              <div>
                <table class="table table-bordered nowrap">
                  <thead>
                      <th>{{ __('No') }}</th>
                      <th>{{ __('Dokumen') }}</th>
                      <th>{{ __('Aksi') }}</th>
                  </thead>
                  <tbody>
                    <td>1</td>
                    <td>Dokumen 1</td>
                    <td><a href="#" class="btn btn-bg-light">Lihat</a></td>
                  </tbody>
                </table>
              </div>
              <div class="row">
                  <div class="col-12">
                    <label for="document_effectiveness" class="required form-label">
                        {{ __('Document') }}
                    </label>
                    <div class="upload-repeat">
                      <div class="box-input-document">
                        <div class="row items-align-center mb-4">
                          <div class="col-10">
                            <input name="document_effectiveness[]" type="file" class="form-control" accept=".pdf, .png, .jpg, .jpeg" required/>
                          </div>
                          <div class="col-2">
                            <button type="button"
                                class="btn btn-sm btn-icon btn-success"
                                id="btn_add_document"
                            >
                                <i class="fa fa-plus"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                <button type="submit" id="submitSendEvidence" class="btn btn-sm btn-primary">{{ __('Kirim') }}</button>
            </div>
          </form>
        </div>
    </div>
</div>
