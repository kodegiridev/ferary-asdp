@extends('layout.demo1.master')

@section('styles')
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"
    />
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <link href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    @include('/layout/partials/babylon')
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('System Parameter Port Electronic Record Condition Monitoring') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item">
                        <a href="{{ route('e-rcm.port.index') }}" class="text-muted text-hover-primary">{{ __('e-RCM') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <a href="{{ route('e-rcm.port.index') }}" class="breadcrumb-item text-muted text-hover-primary">{{ __('Port') }}</a>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <a href="{{ route('e-rcm.port.show', $meta['ercm_id']) }}" class="breadcrumb-item text-muted text-hover-primary">{{ $meta['port_name'] }}</a>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <a href="{{ route('e-rcm.port.show', $meta['periode_ercm_id']) }}" class="breadcrumb-item text-muted text-hover-primary">{{ $meta['periode'] }}</a>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('System Parameter') }}</li>
                    <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ $meta['system_paramater_name'] }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ $meta['port_name'] }}</h3>
                </div>
                <div class="card-body">
                  <div class="swiper">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                            <div id="canvasZone"><canvas id="modelCanvas1" style="touch-action: none; width: 100%;" ></canvas></div>
                            <div class="text-center fw-bold">
                                <p>Hull Construction</p>
                                <p>Before</p>
                            </div>
                      </div>
                      <div class="swiper-slide">
                            <div id="canvasZone"><canvas id="modelCanvas2" style="touch-action: none; width: 100%;" ></canvas></div>
                            <div class="text-center fw-bold">
                                <p>Hull Construction</p>
                                <p>After</p>
                            </div>
                      </div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                  </div>
                </div>
            </div>
        </div>

        <div id="kt_app_table_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('System Parameter') }}</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered nowrap" id="detail_system_table">
                        <thead style="background-color:#3cc9f0;">
                            <tr>
                                <th class="text-center align-middle" style="color:black !important;" rowspan="2">{{ __('No') }}</th>
                                <th class="text-center align-middle" style="color:black !important;" rowspan="2"></th>
                                <th class="text-center align-middle" style="color:black !important;" rowspan="2">{{ __('Group') }}</th>
                                <th class="text-center align-middle min-w-100px"  style="color:black !important;" rowspan="2">{{ __('Value') }}</th>
                                <th class="text-center align-middle min-w-200px"  style="color:black !important;" colspan="3"> {{ __('Score (%)') }}</th>
                                <th class="text-center align-middle min-w-150px"  style="color:black !important;" colspan="4">{{ __('Repair Ticket') }}</th>
                                <th class="text-center align-middle min-w-75px" rowspan="2"  style="color:black !important;">{{ __('Status') }}</th>
                                <th class="text-center align-middle" rowspan="2"  style="color:black !important;">{{ __('Aksi') }}</th>
                            </tr>
                            <tr>
                                <th class="text-center align-middle min-w-50px">{{ __('Before') }}</th>
                                <th class="text-center align-middle min-w-50px">{{ __('After') }}</th>
                                <th class="text-center align-middle min-w-50px"><i class="fa fa-caret-up"></i></th>
                                <th class="text-center align-middle min-w-50px">{{ __('Excecute') }}</th>
                                <th class="text-center align-middle min-w-50px">{{ __('Accept') }}</th>
                                <th class="text-center align-middle min-w-50px">{{ __('Reject') }}</th>
                                <th class="text-center align-middle min-w-50px">{{ __('Postpone') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('e-rcm.port.detail-system-parameter', 1) }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

  @include('pages.e-rcm.port.modals.status_system_parameter')
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
            $(document).ready(function() {
                checkSession();

                const swiper = new Swiper('.swiper', {
                direction: 'horizontal',

                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                });


                var table = $('#detail_system_table').DataTable({
                    proccesing: true,
                    serverSide: true,
                    order: [],
                    ajax: {
                        url: "{{ route('e-rcm.port.sub-detail-system-parameter', [request()->id]) }}"
                    },
                    columns: [
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false,
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'index-'+ rowData.DT_RowIndex);
                            }
                        },
                        {
                            className: 'details-control',
                            orderable: false,
                            data: null,
                            defaultContent: '<button class="btn btn-sm btn-icon btn-success"><i class="fa fa-plus"></i></button>',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'button-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'group',
                            name: 'group',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'group-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'value',
                            name: 'value',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'value-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'before_score',
                            name: 'before_score',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'before-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'after_score',
                            name: 'after_score',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'after-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'delta_score',
                            name: 'delta_score',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'alpha-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'repair_ticket_execute',
                            name: 'repair_ticket_execute',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'excecute-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'repair_ticket_accept',
                            name: 'repair_ticket_accept',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'accept-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'repair_ticket_reject',
                            name: 'repair_ticket_reject',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'reject-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'repair_ticket_postpone',
                            name: 'repair_ticket_postpone',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'postpone-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'status',
                            name: 'status',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'status-'+rowData.DT_RowIndex);
                            }
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false,
                            className: 'w-100px',
                            createdCell: function (td, cellData, rowData, row, col) {
                                $(td).attr('id', 'action-'+rowData.DT_RowIndex);
                            }
                        },
                    ],
                    columnDefs: [{
                        target: '_all',
                        className: 'text-center align-middle'
                    }]
                });

                $('#detail_system_table tbody').on('click', 'td.details-control button', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                        row.child.hide();
                        tr.removeClass('shown');
                        $(this).html('<i class="fa fa-plus"></i>');
                    } else {
                        row.child(format(row.data()), 'no-padding p-0').show();
                        tr.addClass('shown');
                        $(this).html('<i class="fa fa-minus"></i>');
                    }
                });

                function format(d) {
                    var tdWidthIndex = document.getElementById('index-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthButton = document.getElementById('button-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthGroup = document.getElementById('group-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthValue =document.getElementById('value-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthBefore = document.getElementById('before-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthAfter = document.getElementById('after-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthAlpha = document.getElementById('alpha-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthExcecute = document.getElementById('excecute-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthAccept = document.getElementById('accept-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthReject = document.getElementById('reject-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthPostpone = document.getElementById('postpone-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthStatus = document.getElementById('status-' + d.DT_RowIndex).getBoundingClientRect().width
                    var tdWidthAction = document.getElementById('action-' + d.DT_RowIndex).getBoundingClientRect().width


                    var subGroups = d.ercm_subgroups;

                    var detailContent = ''
                    var urlRoute = '{{ route('e-rcm.port.system-parameter', ":id") }}';

                    subGroups.forEach(it=>{
                        detailContent += `
                            <div class="d-flex subgroup-${it.ercm_group_id}">
                                <div style="width: ${tdWidthIndex}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;"></div>
                                <div style="width: ${tdWidthButton}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;"></div>
                                <div style="width: ${tdWidthGroup}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center; margin:auto;">${it.nama}</div>
                                <div style="width: ${tdWidthValue}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;"></div>
                                <div style="width: ${tdWidthBefore}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.before_score ?? ''}</div>
                                <div style="width: ${tdWidthAfter}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.after_score ?? ''}</div>
                                <div style="width: ${tdWidthAlpha}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.delta_score ?? ''}</div>
                                <div style="width: ${tdWidthExcecute}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.repair_ticket_execute ?? ''}</div>
                                <div style="width: ${tdWidthAccept}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.repair_ticket_accept ?? ''}</div>
                                <div style="width: ${tdWidthReject}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.repair_ticket_reject ?? ''}</div>
                                <div style="width: ${tdWidthPostpone}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">${it.repair_ticket_postpone ?? ''}</div>
                                <div style="width: ${tdWidthStatus}px; border-right: 1px solid #F3F4F4; padding: 5px; text-align:center;">
                                    <button type="button"
                                        class="btn btn-sm btn-icon btn-success"
                                        data-bs-toggle="modal"
                                        data-bs-target="#status_modal"
                                    >
                                        <i class="fa fa-info"></i>
                                    </button>
                                </div>
                                <div style="width: ${tdWidthAction}px; padding: 5px; text-align:center;">
                                    <a href="${urlRoute.replace(':id', it.ercm_group_id)}" class="btn btn-sm btn-icon bg-gray-500">
                                    <i class="ki-duotone ki-barcode text-white fs-2">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                        <span class="path3"></span>
                                        <span class="path4"></span>
                                        <span class="path5"></span>
                                        <span class="path6"></span>
                                        <span class="path7"></span>
                                        <span class="path8"></span>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        `;
                    });

                    return detailContent
                }
            });

            $(document).on('click', '.btn-delete', function() {
                Swal.fire({
                    title: "{{ __('Apakah anda yakin?') }}",
                    text: "{{ __('Data akan terhapus secara permanen!') }}",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Ya') }}",
                    cancelButtonText: "{{ __('Batal') }}",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('e-rcm.fleet.delete') }}",
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id": $(this).data('id')
                            },
                            success: function() {
                                location.reload();
                            }
                        });
                    }
                });
            });

            function checkSession() {
                if ('{{ Session::has('success') }}') {
                    Swal.fire({
                        text: `{{ Session::get('success') }}`,
                        icon: "success"
                    });
                }

                if ('{{ Session::has('failed') }}') {
                    Swal.fire({
                        text: `{{ Session::get('failed') }}`,
                        icon: "error"
                    });
                }
            }
    </script>
    <script>
        var asset = "{{ asset('e-rcm/sample/') }}";

        var startRenderLoop = function (engine, scene) {
            engine.runRenderLoop(function () {
                if (scene && scene.activeCamera) {
                    scene.render();
                }
            });
        };

        var createDefaultEngine = function (canvas) {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };

        var createScene = async function (canvasId) {
            const canvas = document.getElementById(canvasId);
            const engine = createDefaultEngine(canvas);
            const scene = new BABYLON.Scene(engine);

            await BABYLON.SceneLoader.AppendAsync(`${asset}/`, "hull-construction.glb", scene);

            scene.createDefaultCameraOrLight(true, true, true);
            scene.activeCamera.alpha += Math.PI;
            scene.clearColor = BABYLON.Color3.White();

            startRenderLoop(engine, scene);

            window.addEventListener("resize", function () {
                engine.resize();
            });

            return { engine, scene };
        };

        createScene("modelCanvas1");
        createScene("modelCanvas2");
    </script>
@endsection
