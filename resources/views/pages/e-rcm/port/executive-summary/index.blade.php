@extends('layout.demo1.master')

@inject('ctrl', '\App\Http\Controllers\Controller')

@section('styles')
    <link href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        th, td {
            text-align: left;
            padding: 8px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    @include('/layout/partials/babylon')
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
      <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Port Electronic Record Condition Monitoring') }}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item">
                <a href="/" class="text-muted text-hover-primary">{{ __('e-RCM') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Port') }}</li>
            </ul>
        </div>
      </div>
    </div>
    <div class="app-toolbar py-3 py-lg-6">
      <div class="app-container w-50 d-flex flex-column">
        <div class="row">
          <p class="fw-bolder col-3">Type of Asset</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">Vessel</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">Name of Asset</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">KMP Goare</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">Region</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">Region 1</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">Contractor</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">Samudra Marine Indonesia</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">Before Score</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">64%</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">After Score</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">89%</p>
        </div>
        <div class="row">
          <p class="fw-bolder col-3">△ Score</p>
          <p class="fw-bolder col-1">:</p>
          <p class="fw-bolder col-5">+25%</p>
        </div>
      </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Filter') }}</h3>
                </div>
                <div class="card-body">
                <form method="POST" enctype="multipart/form-data" id="form-filter">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="periodType" class="required form-label">
                                {{ __('Periode Type') }}
                            </label>
                            <select 
                                id="periodType"
                                name="periodType"
                                class="form-select form-select-solid"
                            >
                                <option value="period">Period</option>
                                <option value="range">Range</option>
                            </select>

                            <div class="d-flex align-items-center justify-content-center">
                                <div class="w-100 period-selector">
                                    <label for="status" class="required form-label mt-4">
                                        {{ __('Period') }}
                                    </label>
                                    <select 
                                        name="status"
                                        class="form-select form-select-solid"
                                    >
                                    <option value="">Pilih data</option>
                                    </select>
                                </div>
                                <div class="col-1 align-items-center justify-content-center mt-12 period-selector"> 
                                    <p class="text-center m-auto h-100 fs-lg-2x"> - </p>
                                </div>
                                <div class="w-100">
                                    <label for="status" class="required form-label mt-4">
                                        {{ __('Period') }}
                                    </label>
                                    <select 
                                        name="status"
                                        class="form-select form-select-solid"
                                    >
                                    <option value="">Pilih data</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="mb-4">System Parameter</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="all" id="all" />
                                        <label class="form-check-label" for="all">
                                            All
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="hull" id="hull" />
                                        <label class="form-check-label" for="hull">
                                            Hull Construction
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="machinery" id="machinery" />
                                        <label class="form-check-label" for="machinery">
                                            Machinery
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="propusion" id="propusion" />
                                        <label class="form-check-label" for="propusion">
                                            Propusion
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="facility" id="facility" />
                                        <label class="form-check-label" for="facility">
                                            Facility
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="piping" id="piping" />
                                        <label class="form-check-label" for="piping">
                                            Piping System
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="navcom" id="navcom" />
                                        <label class="form-check-label" for="navcom">
                                            Navcom
                                        </label>
                                    </div>
                                     <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="safety" id="safety" />
                                        <label class="form-check-label" for="safety">
                                            Safety
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                     <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="deck" id="deck" />
                                        <label class="form-check-label" for="deck">
                                            Deck Machinery
                                        </label>
                                    </div>
                                    <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="outfitting" id="outfitting" />
                                        <label class="form-check-label" for="outfitting">
                                            Outfitting
                                        </label>
                                    </div>
                                     <div class="form-check mb-4">
                                        <input class="form-check-input custom-checkbox" type="checkbox" value="blasting" id="blasting" />
                                        <label class="form-check-label" for="blasting">
                                            Blasting & Painting
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-10 d-flex justify-content-end gap-4">
                        <button type="button" class="btn btn-warning text-black" id="resetButton">{{ __('Reset') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('Filter') }}</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div id="kt_app_table_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div id="container"></div>
                    <p class="fw-bold">Period: 01-08-2023 s.d 08-09-2023</p>
                </div>
            </div>
        </div>

        <div id="kt_app_table_scoring_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('Scoring') }}</h3>
                </div>
                <div class="card-body mt-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <h3 class="mb-10">
                                    Machinery
                                </h3>
                                <table>
                                    <tr>
                                        <th style="border:1px solid; border-width: 0 1px 1px 0;">M/A</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Class Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Safety Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Owner Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Total</th>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Before</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Accept</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Execute</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Postpone</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">After</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                    </tr>
                                </table>  
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <h3 class="mb-10">
                                    Hull Construction
                                </h3>
                                <table>
                                    <tr>
                                        <th style="border:1px solid; border-width: 0 1px 1px 0;">M/A</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Class Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Safety Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Owner Meter</th>
                                        <th style="border:1px solid; border-width: 0 0px 1px 0;">Total</th>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Before</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Accept</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Execute</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">Postpone</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                        <td>Number</td>
                                    </tr>
                                    <tr>
                                        <td style="border:1px solid; border-width: 0 1px 0 0;">After</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                        <td>%</td>
                                    </tr>
                                </table>  
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div id="kt_app_documentation_postpone_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('Postpone Item') }}</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered nowrap" id="postpone_table">
                            <thead>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('System') }}</th>
                                <th>{{ __('Component') }}</th>
                                <th>{{ __('Task List') }}</th>
                                <th>{{ __('Due Date') }}</th>
                                <th>{{ __('Status') }}</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div id="kt_app_documentation_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('Documentation') }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6" style="padding: 25px;">
                            <div class="row" style="border: solid 0.5px;">
                                <div class="row" style="padding: 15px;">
                                    <h3>Machinery</h3>
                                    <div class="col-md-6">
                                        <div id="canvasZone"><canvas id="modelCanvas1" style="touch-action: none; width: 100%;" ></canvas></div>
                                        <div class="text-center">
                                            <h3>Before</h3>
                                            <h6>72%</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="canvasZone2"><canvas id="modelCanvas2" style="touch-action: none; width: 100%;" ></canvas></div>
                                        <div class="text-center">
                                            <h3>After</h3>
                                            <h6>72%</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 25px;">
                            <div class="row" style="border: solid 0.5px;">
                                <div class="row" style="padding: 15px;">
                                    <h3>Hull Construction</h3>
                                    <div class="col-md-6">
                                        <div id="canvasZone"><canvas id="modelCanvas3" style="touch-action: none; width: 100%;" ></canvas></div>
                                        <div class="text-center">
                                            <h3>Before</h3>
                                            <h6>72%</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="canvasZone2"><canvas id="modelCanvas4" style="touch-action: none; width: 100%;" ></canvas></div>
                                        <div class="text-center">
                                            <h3>After</h3>
                                            <h6>72%</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('e-rcm.port.show', 1) }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
       $(document).ready(function() {
            checkSession();
            renderChart();

            displayPosptoneTable();
            
            onHandleSelectPeriod();
            onHandleCheckbox();
            onHandleSubmitFilter();
        });

        function onHandleSelectPeriod(){
            var periodTypeSelector = $('#periodType');
            var periodSelectors = $('.period-selector');
            $(periodSelectors[0]).hide(); 
            $(periodSelectors[1]).hide(); 

            periodTypeSelector.change(function() {
                if ($(this).val() === 'period') {
                    $(periodSelectors[0]).hide(); 
                    $(periodSelectors[1]).hide(); 
                } else {
                    periodSelectors.show();
                }
            });
        }

        function onHandleSubmitFilter(){
            $('#form-filter').on('submit', function(e) {
                e.preventDefault();

                var checkedValues = $('.custom-checkbox:checked').not('#all').map(function() {
                    return this.value; 
                }).get();

                console.log("Checked Values: ", checkedValues); 
            })

             $('#resetButton').click(function() {
                $('.custom-checkbox').prop('checked', false).change();  
            });
        }

        function onHandleCheckbox(){
            $('.custom-checkbox').prop('checked', true).change().css('background-color', 'purple');

            $('.custom-checkbox').change(function() {
                if ($(this).is(':checked')) {
                    $(this).css('background-color', 'purple');
                } else {
                    $(this).css('background-color', 'white');
                }
            });
            
            $('#all').change(function() {
                var isChecked = $(this).is(':checked');

                $('.custom-checkbox').not('#all').prop('checked', isChecked).trigger('change');
            });

            $('.custom-checkbox').not('#all').change(function() {
                if (!$(this).is(':checked')) {
                    $('#all').prop('checked', false);
                }

                if ($(this).is(':checked')) {
                    $(this).css('background-color', 'purple');
                } else {
                    $(this).css('background-color', 'white');
                }
            });
        }

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }

        function displayPosptoneTable(){
            var table = $('#postpone_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.summary', 1) }}",
                    dataSrc: function(json) {
                        var return_data = new Array();
                        for (var i = 0; i < json.data.length; i++) {
                            var item = json.data[i];
                            if (item.tasks && item.tasks.length > 0) {
                                for (var j = 0; j < item.tasks.length; j++) {
                                    var task = item.tasks[j];
                                    return_data.push({
                                         'no': j === 0 ? item.no : task.task,
                                        'system': j === 0 ? item.system : task.dueDate,
                                        'component': j === 0 ? item.component : task.status,
                                        'task':  task.task,
                                        'dueDate':  task.dueDate,
                                        'status':  task.status,
                                        'rowSpanFlag': j === 0,
                                        'rowSpanCount': item.tasks.length
                                    });
                                }
                            }
                        }
                        return return_data;
                    }
                },
                columns: [
                    { data: 'no', name: 'no' },
                    { data: 'system', name: 'system' },
                    { data: 'component', name: 'component' },
                    { data: 'task', name: 'task' },
                    { data: 'dueDate', name: 'dueDate' },
                    { data: 'status', name: 'status' }
                ],
                createdRow: function(row, data, dataIndex) {
                     if (data.rowSpanFlag) {
                        $(row).find('td:eq(0)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(1)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(2)').attr('rowspan', data.rowSpanCount);
                    }else{
                        $(row).find('td:eq(0)', row).css('display', 'none');
                        $(row).find('td:eq(1)', row).css('display', 'none');
                        $(row).find('td:eq(2)', row).css('display', 'none');
                    }
                },
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }

        function renderChart(){
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Reliability Performance Summary',
                    align: 'center'
                },
                xAxis: {
                    categories: ['M/A', 'P/R', 'F/C', 'O/T', 'P/S', 'N/C', 'B/P', 'S/E', 'D/M', 'H/C'],
                    crosshair: true,
                    accessibility: {
                        description: 'Parameter'
                    }
                },
                yAxis: {
                    min: 0,
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                    {
                        name: 'Before',
                        data: [88, 90, 100, 20, 11, 23, 22, 10, 90, 10]
                    },
                    {
                        name: 'Minimum',
                        data: [80, 92, 10, 70, 18, 21, 22, 20, 40, 30]
                    },
                    {
                        name: 'After',
                        data: [88, 30, 50, 21, 14, 23, 42, 16, 92, 50]
                    }
                ]
            });
        }
    </script>

    
    <script>
        var asset = "{{ asset('e-rcm/sample/') }}"; 

        var startRenderLoop = function (engine, scene) {
            engine.runRenderLoop(function () {
                if (scene && scene.activeCamera) {
                    scene.render();
                }
            });
        };

        var createDefaultEngine = function (canvas) {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };

        var createScene = async function (canvasId) {
            const canvas = document.getElementById(canvasId);
            const engine = createDefaultEngine(canvas);
            const scene = new BABYLON.Scene(engine);

            await BABYLON.SceneLoader.AppendAsync(`${asset}/`, "hull-construction.glb", scene);

            scene.createDefaultCameraOrLight(true, true, true);
            scene.activeCamera.alpha += Math.PI;
            scene.clearColor = BABYLON.Color3.White();

            startRenderLoop(engine, scene);

            window.addEventListener("resize", function () {
                engine.resize();
            });

            return { engine, scene };
        };

        createScene("modelCanvas1");
        createScene("modelCanvas2");
        createScene("modelCanvas3");
        createScene("modelCanvas4");
    </script>

@endsection