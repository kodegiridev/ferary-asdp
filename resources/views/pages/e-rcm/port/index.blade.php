@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Port Electronic Record Condition Monitoring') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item">
                    <a href="/" class="text-muted text-hover-primary">{{ __('e-RCM') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Port') }}</li>
            </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Filter') }}</h3>
                </div>
                <div class="card-body">
                <form method="GET">
                    @csrf
                    <div class="form-group row">
                    <div class="col-md-3">
                        <label for="port_name" class="form-label font-bold">{{ __('Nama Pelabuhan') }}</label>
                        <input id="port_name" name="port_name" value="{{ isset($urlParams['port_name']) ? $urlParams['port_name'] : '' }}" type="text" class="form-control mb-2 mb-md-0" placeholder="Nama Pelabuhan" />
                    </div>
                    <div class="col-md-3">
                        <label for="status" class="required form-label">
                            {{ __('Status') }}
                        </label>
                        <select
                            id="status"
                            name="status"
                            class="form-select form-select-solid"
                        >
                        <option value="">Pilih data</option>
                        @foreach ($statuses as $status)
                            <option value="{{ $status['id'] }}"  {{ (isset($urlParams['status_id']) && $urlParams['status_id'] == $status['id']->value) ? "selected": "" }}>{{ $status['name'] }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="regional" class="required form-label">
                            {{ __('Regional') }}
                        </label>
                        <select
                        id="regional"
                        name="region_id"
                        class="form-select form-select-solid"
                        >
                        <option value="">Pilih data</option>
                        @foreach ($regions as $region)
                            <option value="{{ $region->id }}"  {{ (isset($urlParams['region_id']) && $urlParams['region_id'] == $region->id) ? "selected": "" }}>{{ $region->nama }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="cabang" class="required form-label">
                            {{ __('Cabang') }}
                        </label>
                        <select
                        id="cabang"
                        name="branch_id"
                        class="form-select form-select-solid"
                        >
                        <option value="">Pilih data</option>
                        @foreach ($branchs as $branch)
                            <option value="{{ $branch->id }}"  {{ (isset($urlParams['branch_id']) && $urlParams['branch_id'] == $branch->id) ? "selected": "" }}>{{ $branch->nama }}</option>
                        @endforeach
                        </select>
                    </div>
                    </div>

                    <div class="mt-10">
                    <button type="button" id="btn-filter-reset" class="btn btn-warning text-black" data-bs-dismiss="modal">{{ __('Reset') }}</button>
                    <button type="submit" id="btn-filter" class="btn btn-primary">{{ __('Filter') }}</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div id="kt_app_table_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title font-extrabold">{{ __('Data Pelabuhan') }}</h3>
                </div>
                <div class="card-body">
                    <div class="flex mb-10">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#create_modal">{{ __('+ Tambah') }}</button>
                        <a href="#" class="btn btn-icon btn-dark">
                            <i class="fa fa-arrows-rotate"></i>
                        </a>
                        <a href="#" class="btn btn-icon btn-success">
                            <i class="fa fa-print"></i>
                        </a>
                    </div>

                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="ercm_table">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                        <th>{{ __('No') }}</th>
                        <th>{{ __('Regional') }}</th>
                        <th>{{ __('Cabang') }}</th>
                        <th>{{ __('Pelabuhan') }}</th>
                        <th>{{ __('Kelas Cabang') }}</th>
                        <th>{{ __('PIC') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th class="text-center">{{ __('Action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>

  @include('pages.e-rcm.port.modals.create')
@endsection

@section('scripts')
  <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
  <script>
        $(document).ready(function() {
            checkSession();

            const queryUrl = "{{ request()->getQueryString() }}";

            var table = $('#ercm_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.index') }}" + '?' + queryUrl.replaceAll('amp;', '')
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'w-20px'
                    },
                    {
                        data: 'port.cabang.regional.nama',
                        name: 'port.cabang.regional.nama'
                    },
                    {
                        data: 'port.cabang.nama',
                        name: 'port.cabang.nama'
                    },
                    {
                        data: 'port.nama',
                        name: 'port.nama'
                    },
                    {
                        data: 'port.cabang.cabang_kelas',
                        name: 'port.cabang.cabang_kelas'
                    },
                    {
                        data: 'port.pic_user.name',
                        name: 'port.pic_user.name'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'w-100px'
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        });

        const url = "{{ url()->current() }}";

        $('#btn-filter').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            const queryParams = {
                port_name: $('input[name=port_name]').val() || null,
                status_id: $('select[name=status]').val() || null,
                region_id: $('select[name=region_id]').val() || null,
                branch_id: $('select[name=branch_id]').val() || null
            }

            let urlParams = url
            Object.keys(queryParams).forEach(key=>{
                if (queryParams[key]) {
                    urlParams += urlParams.includes('?') ? `&${key}=${queryParams[key]}` : `?${key}=${queryParams[key]}`
                }
            })

            location.href = urlParams;
        });

        $('#btn-filter-reset').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            location.href = url;
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('e-rcm.port.delete') }}",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
  </script>
@endsection
