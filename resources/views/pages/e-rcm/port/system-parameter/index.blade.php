@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('System Parameter Port Electronic Record Condition Monitoring') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item">
                    <a href="/" class="text-muted text-hover-primary">{{ __('e-RCM') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <a href="{{ route('e-rcm.port.index') }}" class="breadcrumb-item text-muted">{{ __('Port') }}</a>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <a href="{{ route('e-rcm.fleet.show', 1) }}" class="breadcrumb-item text-muted text-hover-primary">{{ __('KMP Goare') }}</a>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <a href="{{ route('e-rcm.fleet.show', 1) }}" class="breadcrumb-item text-muted text-hover-primary">{{ __('10-11-2023') }}</a>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('System Paremeter') }}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Sub Group') }}</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Keel') }}</li>
            </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-body">
                    <ul class="nav nav-tabs mt-2" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link inspection-menu active" data-id="initial" data-target="#initialContent" data-label="initial">{{ __('Initial') }}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link inspection-menu" data-id="before" data-target="#beforeContent" data-label="before">{{ __('Before') }}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link inspection-menu" data-id="repair" data-target="#repairContent" data-label="repair">{{ __('Repair') }}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link inspection-menu" data-id="after" data-target="#afterContent" data-label="after">{{ __('After') }}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link inspection-menu" data-id="postpone" data-target="#postponeContent" data-label="postpone">{{ __('Postpone') }}</button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="initialContent" class="tab-pane fade show active" role="tabpanel">
                            @include('pages.e-rcm.port.tables_parameter.initial')
                        </div>
                        <div id="beforeContent" class="tab-pane fade" role="tabpanel">
                            @include('pages.e-rcm.port.tables_parameter.before')
                        </div>
                        <div id="repairContent" class="tab-pane fade" role="tabpanel">
                            @include('pages.e-rcm.port.tables_parameter.repair')
                        </div>
                         <div id="afterContent" class="tab-pane fade" role="tabpanel">
                            @include('pages.e-rcm.port.tables_parameter.after')
                        </div>
                         <div id="postponeContent" class="tab-pane fade" role="tabpanel">
                            @include('pages.e-rcm.port.tables_parameter.postpone')
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('e-rcm.port.show', 1) }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('pages.e-rcm.port.modals.riwayat_pengajuan')
    @include('pages.e-rcm.port.modals.upload_evidence')
    @include('pages.e-rcm.port.modals.riwayat_pengajuan')
    @include('pages.e-rcm.port.modals.submit_system')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <!-- script initial before after table -->
    <script>
        var selectedTable = 'initial'
        const listTable = ["initial", "before", "after"]
        $(document).ready(function() {
            for(let i=0; i< listTable.length; i++){
                displayTable(listTable[i])
                onHandleSubmit(listTable[i]);
            }
            checkInputsAndToggleSubmitButton();
        })

        $(document).on('click', '.inspection-menu', function() {
            const targetDiv = $(this).data('target');
            const idDiv = $(this).data('id');

            $('.inspection-menu').removeClass('active');
            $('.tab-pane').removeClass('show active');
            selectedTable= idDiv

            $(this).addClass('active');
            $(targetDiv).addClass('show active');
        });

        function displayTable(idTable){
            var table = $(`#${idTable}_table`).DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.system-parameter', 1) }}",
                    data: { tableType: 'process' },
                    dataSrc: function(json) {
                        var return_data = new Array();
                        for (var i = 0; i < json.data.length; i++) {
                            var item = json.data[i];
                            if (item.parameters && item.parameters.length > 0) {
                                for (var j = 0; j < item.parameters.length; j++) {
                                    var task = item.parameters[j];
                                    return_data.push({
                                        'no':  item.DT_RowIndex ,
                                        'group': item.group ,
                                        'subGroup': item.subGroup ,
                                        'component':  item.component ,
                                        'parameter':  task.name,
                                        'standard':  concatStandard(task.standard),
                                        'score': item.score,
                                        'status': item.status,
                                        'rowSpanFlag': j === 0, 
                                        'approval': item.approval,
                                        'rowSpanCount': item.parameters.length
                                    });
                                }
                            }
                        }
                        return return_data;
                    }
                },
                columns: [
                    {   data: 'no', name: 'no' },
                    {   data: 'group', name: 'group' },
                    {   data: 'subGroup', name: 'subGroup' },
                    {   data: 'component', name: 'component' },
                    {   data: 'parameter', name: 'parameter' },
                    {   data: 'standard', name: 'standard' },
                    {
                        data: 'parameter',
                        render: function(data, type, row, meta) {
                            return renderMeasurement(data, row.no, meta.row, idTable);
                        }
                    },
                    {   data: 'remarks', 
                        render: function(data, type, row, meta) {
                            return (
                                `<input type="textarea" name="remarks-${idTable}-${row.no}" id="remarks-${idTable}-${row.no}" class="form-control mb-2 mb-md-0"/>`
                            )
                        }
                    },
                    {   data: 'classification', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                <select 
                                    name="classification" 
                                    id="select-classification-${idTable}-${row.no}" 
                                    class="form-select form-select-solid" 
                                >
                                    <option value="">Pilih Data</option>
                                    <option value="class">Class Meter</option>
                                    <option value="safety">Safety Meter</option>
                                    <option value="owner">Owner Meter</option>
                                </select>
                                `
                            )
                        }
                        
                    },
                    {   data: 'score', 
                        render: function(data, type, row, meta) {
                            return (
                                `<p class="fw-bold">${row.score}</p>`
                            )
                        }
                    },
                    {   data: 'status', 
                        render: function(data, type, row, meta) {
                            let color = "";

                            if(row.status === "Monitoring"){
                                color = "#f5aa42"
                            }else if(row.status === "Accepted"){
                                color = "#42f5b3"
                            }else {
                                color = "red"
                            }

                            return (
                                `<p class="fw-bold"; style="color:${color};">${row.status}</p>`
                            )
                        }
                    },
                    {   data: 'evidence', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                    <button type="button"
                                        class="btn btn-sm btn-success"
                                        data-id="${idTable}-${row.no}"
                                        data-bs-toggle="modal" 
                                        data-bs-target="#upload_evidence_modal"
                                    >
                                        Upload
                                    </button>
                                `
                            )
                        }
                    },
                    {   data: 'approval', name: 'approval' },
                ],
                createdRow: function(row, data, dataIndex) {
                     if (data.rowSpanFlag) {
                        $(row).find('td:eq(0)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(1)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(2)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(3)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(9)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(10)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(11)').attr('rowspan', data.rowSpanCount);
                        $(row).find('td:eq(12)').attr('rowspan', data.rowSpanCount);
                    }else{
                        $(row).find('td:eq(0)', row).css('display', 'none');
                        $(row).find('td:eq(1)', row).css('display', 'none');
                        $(row).find('td:eq(2)', row).css('display', 'none');
                        $(row).find('td:eq(3)', row).css('display', 'none');
                        $(row).find('td:eq(9)', row).css('display', 'none');
                        $(row).find('td:eq(10)', row).css('display', 'none');
                        $(row).find('td:eq(11)', row).css('display', 'none');
                        $(row).find('td:eq(12)', row).css('display', 'none');
                    }
                },
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }

        function concatStandard(data){
            if (Array.isArray(data)) {
                return data.map(item =>  { 
                    if(Object.keys(item)[0] === "name") return Object.values(item)[0]
                    return Object.keys(item)[0] + ': ' + Object.values(item)[0] 
                }).join('<br/>');
            } else {
                return data;
            }
        }

        function updateSelect(rowIndex, tableNumber) {  
            var selectValue = document.getElementById(`select-${selectedTable}-${rowIndex}`).value;
            var textElement = document.getElementById(`text-select-${selectedTable}-${rowIndex}`);
            var additionalInput = document.getElementById(`additional-input-${selectedTable}-${rowIndex}`);

            if (selectValue === "yes") {
                textElement.innerText = "Good";
                additionalInput.style.display = 'none';

                var remarksInputs = document.querySelectorAll(`input[name^="remarks-${selectedTable}-${tableNumber}"]`);
                remarksInputs.forEach(function(input) {
                    input.value = '';
                });

                var inputVerificators = document.querySelectorAll('.input-remarks');
                inputVerificators.forEach(function(verificator) {
                    verificator.remove();
                });
                
            } else if (selectValue === "no") {
                textElement.innerText = "";
                additionalInput.style.display = 'block';
            } else {
                textElement.innerText = "";
                additionalInput.style.display = 'none';
            }
        }

        function onHandleSubmit(table){
            $(`#form-${table}`).on('submit', function(e) {
                e.preventDefault();

                var table = $(`#${selectedTable}_table`).DataTable();
                var collectedData = [];
                var currentGroupData = {};

                table.rows().every(function(rowIdx) {
                    var rowNode = this.node();
                    var rowData = this.data();

                    if (rowData.rowSpanFlag) {
                        if (Object.keys(currentGroupData).length > 0) {
                            collectedData.push(currentGroupData);
                        }
                        currentGroupData = {
                            id: rowData.no, 
                            rangeInput: '',
                            rangeValue: '',
                            selectValue: '',
                            deformation: '',
                            remarks: [],
                            thickness: ''
                        };
                    }

                    var rangeInput = rowNode.querySelector('input[type="number"][name^="range-"]');
                    var thickness = rowNode.querySelector('input[type="text"][name^="thickness"]');
                    var deformation = rowNode.querySelector('input[type="text"][name^="deformation"]');
                    var select = rowNode.querySelector('select[name="final"]');
                    var remarks = rowNode.querySelector('input[type="text"][name^="remarks-"]');


                    if (rangeInput) {
                        currentGroupData.rangeValues = rangeInput.value;
                    }
                    if (thickness) {
                        currentGroupData.thickness = thickness.value;
                    }
                    if (select) {
                        currentGroupData.selectValue = select.value;
                    }
                    if (deformation) {
                        currentGroupData.deformation = deformation.value;
                    }

                    var allRemarks = [];

                    // Find each input field that has a name starting with 'remarks-'
                    $(`input[name^="remarks-${rowData.no}-"]`).each(function() {
                        var remarkValue = $(this).val();
                        if (remarkValue) { 
                            allRemarks.push(remarkValue);
                        }
                    });

                    if(remarks){
                        currentGroupData.remarks = allRemarks;
                    }
                });

                if (Object.keys(currentGroupData).length > 0) {
                    collectedData.push(currentGroupData);
                }

                console.log(collectedData);
            })
        }

        function renderMeasurement(parameter, tableNumber, rowIndex, idTable) {
            if (parameter.includes("(number)") || parameter.includes("Area")) {
                let name = parameter === "Thickness (number)" ? "thickness" : "deformation"
                return '<input type="text" name="' + parameter + '" class="form-control mb-2 mb-md-0"/>';
            } else if (parameter.includes("(Ranges)")) {
                return `
                    <div class="row px-3 pb-0">
                        <input type="number" name="range-${idTable}-${rowIndex}" id="range-${idTable}-${rowIndex}" class="form-control mb-2 mb-md-0" oninput="updateTextMeasurement(${rowIndex})"/>
                        <p id="text-${idTable}-${rowIndex}"></p>
                    </div>
                `;
            }else{
                return `
                <div class="row px-3 pb-0 select-repeat">
                    <select 
                        name="final" 
                        id="select-${idTable}-${rowIndex}" 
                        class="form-select form-select-solid" 
                        onchange="updateSelect(${rowIndex}, ${tableNumber})"
                    >
                        <option value="">Pilih data</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                    <p id="text-select-${idTable}-${rowIndex}"></p>
                    <div class="box-input-remarks p-0" id="additional-input-${idTable}-${rowIndex}" style="display: none;">
                        <div class="d-flex align-items-center gap-1 p-0">
                            <input type="text" name="remarks-${idTable}-${tableNumber}-${rowIndex}[]" id="remarks-${tableNumber}-${rowIndex}" class="form-control mt-2" />
                            <button type="button"
                                class="btn btn-sm btn-icon btn-success"
                                id="btn_add_remarks"
                                data-id="${idTable}-${tableNumber}-${rowIndex}"
                            >
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                `;
            }
        }

        $(document).on('click', '#btn_add_remarks', function() {
            var modal = $(this).closest('.select-repeat');
            var boxInputRemarks = modal.find('.box-input-remarks');
            var dataId = $(this).data('id');
            const dataSplit = dataId.split("-")
            var newIndex = parseInt(dataSplit[1]) + 1;

            boxInputRemarks.append(`
                <div class="d-flex align-items-center gap-1 p-0 input-remarks">
                    <input type="text" name="remarks-${selectedTable}-${dataSplit[0]}-${newIndex}[]" id="remarks-${dataSplit[1]}-${newIndex}" class="form-control mt-2" />
                    <button type="button"
                        class="btn btn-sm btn-icon btn-danger"
                        id="btn-delete-remarks"
                    >
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            `);
        });

        $(document).on('click', '#btn-delete-remarks', function() {
            var inputVerificator = $(this).closest('.input-remarks');
            inputVerificator.remove();
        });

        function updateTextMeasurement(rowIndex) {
            var inputValue = document.getElementById(`range-${selectedTable}-${rowIndex}`).value;
            var textElement = document.getElementById(`text-${selectedTable}-${rowIndex}`);

            if(inputValue === "") {
                textElement.innerText = "";
            }else if (inputValue == 1) {
                textElement.innerText = "Dev === 1 mm";
            } else if(inputValue > 2){
                textElement.innerText = "Dev > 1 mm";
            }else {
                textElement.innerText = "Dev < 1 mm";
            }
        }

        // still need enhancement
        function checkInputsAndToggleSubmitButton() {
            var allFieldsFilled = true;
            var inputs = document.querySelectorAll('#initial_table input, #initial_table select');

            inputs.forEach(function(field) {
                if ((field.tagName === 'INPUT' && !field.value) || (field.tagName === 'SELECT' && !field.value)) {
                    allFieldsFilled = false;
                }
            });

            
            var submitBtn = document.getElementById('openModalSubmitButton');
            submitBtn.disabled = allFieldsFilled;
        }

        document.addEventListener('DOMContentLoaded', function() {
            $('#initial_table').on('input change', 'input, select', function() {
                checkInputsAndToggleSubmitButton();
            });
        });

        $(document).on('click', '#btn_add_verificator', function() {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');
            var options = ''

            $('select[name="user_verifikator_id[]"]').each(function(index, element) {
                if (index == 0) {
                    options = $(this).html();
                }
            });

            boxInputVerificator.append(`
                <div class="row input-verificator mt-2">
                    <div class="col-10">
                        <select name="user_verifikator_id[]" class="form-select form-select-solid verificator">
                            ${options}
                        </select>
                    </div>
                    <div class="col-1 d-flex align-items-center">
                        <button type="button" class="btn btn-sm btn-danger btn-delete-verificator">
                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr090.svg', 'svg-icon-3') !!}
                        </button>
                    </div>
                </div>
            `);
        });

        $(document).on('click', '.btn-delete-verificator', function() {
            var inputVerificator = $(this).closest('.input-verificator');
            var selectElement = inputVerificator.find('select').val();

            if (selectElement != '') {
                var approver = $('select[name="penyetuju_id"]');
                var verificator = $('select[name="user_verifikator_id[]"]');

                $.each(approver.find('option'), function(key, value) {
                    if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                        $(this).prop('hidden', false);
                    }
                });

                verificator.each(function(index, element) {
                    var selectVerificator = $(this);
                    $.each(selectVerificator.find('option'), function(key, value) {
                        if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                            $(this).prop('hidden', false);
                        }
                    });
                });
            }
            inputVerificator.remove();
        });
    </script>
    
    <!-- script upload evidence -->
    <script>
        $(document).ready(function() {  
            onHandleSubmitEvidence();
        })

        $(document).on('click', '#btn_add_document', function() {
            var modal = $(this).closest('.upload-repeat');
            var boxInputRemarks = modal.find('.box-input-document');
            var dataId = $(this).data('id');

            boxInputRemarks.append(`
                <div class="row items-align-center mb-4 box-upload">
                    <div class="col-10">
                        <input name="document_effectiveness[]" type="file" class="form-control" accept=".pdf, .png, .jpg, .jpeg" required/>
                    </div>
                    <div class="col-2">
                        <button type="button"
                            class="btn btn-sm btn-icon btn-danger"
                            id="btn-delete-documents"
                        >
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                </div>
            `);
        });

        $(document).on('click', '#btn-delete-documents', function() {
            var uploadDocument = $(this).closest('.box-upload');
            uploadDocument.remove();
        });

        function onHandleSubmitEvidence(){
            $('#form-evidence').on('submit', function(e) {
                e.preventDefault(); 

                var files = [];

                $('input[type="file"]').each(function() {
                    $.each(this.files, function() {
                        files.push(this);
                    });
                });
               
                console.log("files", files)
            })
        }
    </script>
  
    <!-- script repair and postpone table -->
    <script>
        var selectedTableApproval = 'repair'
        const listTableApproval = ["repair", "postpone"]
        $(document).ready(function() {
            for(let i=0; i< listTableApproval.length; i++){
                onHandleSubmit(listTableApproval[i]);
            }
            checkInputsAndToggleSubmitButton();
            displayTableApproval();
            displayTableTasklist();
            displayTablePostpone();
        })

        function displayTableApproval(){
            var table = $(`#repair_table`).DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.system-parameter', 1) }}",
                    data: { tableType: 'repair' }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'w-20px'
                    },
                    {   data: 'group', name: 'group' },
                    {   data: 'subGroup', name: 'subGroup' },
                    {   data: 'component', name: 'component' },
                    {   data: 'score', name: 'score' },
                    {   data: 'status', 
                        render: function(data, type, row, meta) {
                            let bgColor = "";
                            let textColor = "black"

                            if(row.status === "Repair"){
                                bgColor = "red"
                                textColor = "white"
                            }else {
                                bgColor = "yellow"
                            }

                            return (
                                `
                                <div style="background-color:${bgColor};">
                                    <p class="fw-bold w-100 p-4 m-0" style="color:${textColor};">${row.status}</p>
                                </div>
                                `
                            )
                        }
                    },
                    {   data: 'taskList', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                    <button type="button"
                                        class="btn btn-sm btn-primary"
                                        data-id=repair-${row.id}
                                        data-bs-toggle="modal" 
                                        data-bs-target="#create_tasklist_modal"
                                    >
                                        View
                                    </button>
                                `
                            )
                        }
                    },
                    {   data: 'classification', name: 'classification' },
                    {   data: 'status', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                <select 
                                    name="status-approval" 
                                    id="select-repair-${row.id}" 
                                    class="form-select form-select-solid" 
                                >
                                    <option value="">Pilih data</option>
                                    <option value="execute">Execute</option>
                                    <option value="postpone">Pospotne</option>
                                </select>
                                `
                            )
                        }
                    },
                    {   data: 'approval', 
                        render: function(data, type, row, meta) {
                            let color = "";

                            if(row.approval === "Approved"){
                                color = "green"
                            }else {
                                color = "red"
                            }

                            return (
                                `
                                <div>
                                    <p class="fw-bold w-100 p-0" style="color:${color}; text-align:center;">${row.approval}</p>
                                    <p class="p-0">${row.approvalNote}</p>
                                </div>
                                `
                            )
                        }
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle p-0'
                }, {
                    className: "p-0", "targets": [5]
                }]
            });
        }

        function displayTableTasklist(){
            var table = $(`#tasklist_table`).DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.system-parameter', 1) }}",
                    data: { tableType: 'tasklist' }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'w-20px'
                    },
                    {   data: 'taskList', name: 'taskList' },
                    {   data: 'action', name: 'action' },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle p-0'
                }]
            });
        }

        function displayTablePostpone(){
            var table = $(`#postpone_table`).DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('e-rcm.port.system-parameter', 1) }}",
                    data: { tableType: 'postpone' }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'w-20px'
                    },
                    {   data: 'group', name: 'group' },
                    {   data: 'subGroup', name: 'subGroup' },
                    {   data: 'component', name: 'component' },
                    {   data: 'taskList', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                    <button type="button"
                                        class="btn btn-sm btn-primary"
                                        data-id=repair-${row.id}
                                        data-bs-toggle="modal" 
                                        data-bs-target="#create_tasklist_modal"
                                    >
                                        View
                                    </button>
                                `
                            )
                        }
                    },
                     {   data: 'remarks', 
                        render: function(data, type, row, meta) {
                            return (
                                `<input type="textarea" name="remarks-repair-${row.id}" id="remarks-repair-${row.id}" class="form-control mb-2 mb-md-0"/>`
                            )
                        }
                    },
                    {   data: 'classification', name: 'classification' },
                    {   data: 'status', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                <select 
                                    name="status-approval" 
                                    id="select-repair-${row.id}" 
                                    class="form-select form-select-solid" 
                                >
                                    <option value="">Pilih data</option>
                                    <option value="open">Open</option>
                                    <option value="close">Close</option>
                                </select>
                                `
                            )
                        }
                    },
                    {   data: 'evidence', 
                        render: function(data, type, row, meta) {
                            return (
                                `
                                    <button type="button"
                                        class="btn btn-sm btn-success"
                                        data-id="postpone-${row.no}"
                                        data-bs-toggle="modal" 
                                        data-bs-target="#upload_evidence_modal"
                                    >
                                        Upload
                                    </button>
                                `
                            )
                        }
                    },
                    {   data: 'approval', 
                        render: function(data, type, row, meta) {
                            let color = "";

                            if(row.approval === "Approved"){
                                color = "green"
                            }else {
                                color = "red"
                            }

                            return (
                                `
                                <div>
                                    <p class="fw-bold w-100 p-0" style="color:${color}; text-align:center;">${row.approval}</p>
                                </div>
                                `
                            )
                        }
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }
    </script>
@endsection