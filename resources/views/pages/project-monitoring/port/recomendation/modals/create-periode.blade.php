<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_periode_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.port.recomendation.store', $data['project']['id']) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Periode Selanjutanya') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <p>
                        Anda yakin ingin melanjutkan ke periode selanjutnya ? <br>
                        Data yang ada di periode ini akan terduplikasi ke periode selanjutnya
                    </p>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
