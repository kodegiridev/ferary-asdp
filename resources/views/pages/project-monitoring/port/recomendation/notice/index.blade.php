@extends('pages.project-monitoring.port.detail')

@section('recomendation-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush


@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.recomendation.index', $data['project']['id']) }}" class="text-muted text-hover-primary">{{ __('Recomendation') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Notice') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-start">
        <div class="col-md-6 col-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="25%">Nama Project</td>
                        <td>: {{ $data['project']['nama_project'] }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice Open</td>
                        <td>: {{ $data['notice']['total_open'] }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice Close</td>
                        <td>: {{ $data['notice']['total_close'] }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice</td>
                        <td>: {{ $data['notice']['total'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex justify-content-start">
        <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_notice_modal">
            <i class="fa fa-plus"></i>
            {{ __('Tambah') }}
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id="notice_table">
            <thead>
                <tr>
                    <th class="">{{ __('No') }}</th>
                    <th class="">{{ __('Notice') }}</th>
                    <th class="">{{ __('Pending Matters') }}</th>
                    <th class="">{{ __('Execute By') }}</th>
                    <th class="">{{ __('Due Date') }}</th>
                    <th class="">{{ __('Status') }}</th>
                    <th class="">{{ __('Evidence') }}</th>
                    <th class="">{{ __('Aksi') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    @include('pages.project-monitoring.port.recomendation.notice.modals.create')
    @include('pages.project-monitoring.port.recomendation.notice.modals.edit')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end">
        <a class="btn btn-secondary btn-sm" href="{{ route('ipm.port.recomendation.index', $data['project']['id']) }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            var table = $('#notice_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.recomendation.notice.index', [$data['project']['id'], $data['period']['id']]) }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'notice',
                        name: 'notice'
                    },
                    {
                        data: 'to_do_list',
                        name: 'to_do_list'
                    },
                    {
                        data: 'pic',
                        name: 'pic'
                    },
                    {
                        data: 'due_date',
                        name: 'due_date'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'text-center'
                    },
                    {
                        data: 'file',
                        name: 'file'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ],
                columnDefs: [{
                    target: "_all",
                    className: 'align-middle'
                }]
            });
        });

        $(document).on('click', '.btn-edit', function() {
            var url = "{{ route('ipm.port.recomendation.notice.show', [$data['project']['id'], $data['period']['id']]) }}";
            var id = $(this).data('id');
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function(data) {
                    $('#edit_notice_modal input[name="id"]').val(id);
                    $('#edit_notice_modal input[name="notice"]').val(data?.notice);
                    $('#edit_to_do_list').text(data?.to_do_list);
                    $('#edit_notice_modal input[name="due_date"]').val(data?.due_date);
                    $('#edit_notice_modal input[name="nomor_dokumen"]').val(data?.nomor_dokumen);

                    const types = ['open', 'close'];
                    for (let i = 0; i < types.length; i++) {
                        const isSelected = types[i] === data?.status ? 'selected' : '';
                        $('#edit_status').append(`<option value="${types[i]}" ${isSelected}>${types[i].toUpperCase()}</option>`);
                    }

                    data?.users?.forEach((user) => {
                        const isSelected = user?.id === data?.pic ? 'selected' : '';
                        $('#edit_pic').append(`<option value="${user?.id}" ${isSelected}>${user?.name?.toUpperCase()}</option>`);
                    })

                    $('#edit_notice_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        text: err?.responseText,
                        icon: "error"
                    });
                }
            });
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('ipm.port.recomendation.notice.delete', [$data['project']['id'], $data['period']['id']]) }}",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endpush
