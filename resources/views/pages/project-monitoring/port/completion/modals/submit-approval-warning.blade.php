<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="submit_completion_warning">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form>
                @csrf
                <div class="modal-body">
                    <div class="d-flex align-items-center justify-content-center h-125px">
                        <h3 class="fw-bold text-center">
                            Apakah anda yakin ingin melakukan <br>
                            closing project ini ?
                        </h3>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="button" class="btn btn-sm btn-primary" id="btn_confirm_completion">{{ __('Ya') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
