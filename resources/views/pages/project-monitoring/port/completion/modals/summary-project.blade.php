<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="completion_summary_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Project Completion') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>

            <div class="modal-body">
                <div class="form-group mb-3">
                    <label class="form-label">{{ __('Presentase Progress Project') }}</label>
                    <input type="text" name="progress" class="form-control" value="{{ $data['completion']['progress'] }}" readonly />
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">{{ __('Presentase Quality Project') }}</label>
                    <input type="text" name="quality" class="form-control" value="{{ $data['completion']['quality'] }}" readonly />
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">{{ __('Presentase Dokumen Project') }}</label>
                    <input type="text" name="document" class="form-control" value="{{ $data['completion']['document'] }}" readonly />
                </div>
                <div class="form-group mb-3">
                    <label class="form-label">{{ __('Presentase Pemenuhan Rekomendasi') }}</label>
                    <input type="text" name="recomendation" class="form-control" value="{{ $data['completion']['recomendation'] }}" readonly />
                </div>
                <div class="form-group">
                    <label class="form-label">{{ __('Catatan Khusus Project') }}</label>
                    <textarea name="catatan_project" class="form-control" rows="3" id="catatan_project"></textarea>
                </div>
            </div>

            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                <button type="button" class="btn btn-sm btn-primary" id="submit_completion">{{ __('Project Completion') }}</button>
            </div>
        </div>
    </div>
</div>
@push('modal-scripts')
    <script>
        $(document).on('click', '#completion', function() {
            $.ajax({
                url: "{{ route('ipm.port.completion.index', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    $('#completion_summary_modal input[name="progress"]').val(response?.progress);
                    $('#completion_summary_modal input[name="quality"]').val(response?.quality);
                    $('#completion_summary_modal input[name="document"]').val(response?.document);
                    $('#completion_summary_modal input[name="recomendation"]').val(response?.recomendation);

                    var approvers = '<option value="">Pilih penyetuju</option>';
                    $.each(response?.approvers, function(key, val) {
                        approvers += `<option value="${val?.id}">${val?.name}</option>`
                    })

                    var verificators = '<option value="">Pilih verifikator</option>';
                    $.each(response?.verificators, function(key, val) {
                        verificators += `<option value="${val?.id}">${val?.name}</option>`
                    })

                    $('#submit_completion_modal .approver').html(approvers)
                    $('#submit_completion_modal .verifikator').html(verificators)
                    $('#completion_summary_modal').modal('show');
                },
                error: function(error) {
                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        $(document).on('click', '#submit_completion', function() {
            $('#submit_completion_modal input[name="catatan_project"]').val($('#catatan_project').val());
            $('#completion_summary_modal').modal('hide');
            $('#submit_completion_modal').modal('show');
        });
    </script>
@endpush
