<div class="modal fade" tabindex="-1" id="doc_remarks_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Document Remarks') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800 text-center">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Last Update') }}</th>
                                <th>{{ __('Progress') }}</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle" id="document-table"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
            </div>
        </div>
    </div>
</div>
