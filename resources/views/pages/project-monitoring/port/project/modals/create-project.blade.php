<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_project_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.port.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="project" name="action">
                <input type="hidden" value="2" name="jenis_project">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Project') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Kontraktor') }}</label>
                        <input type="text" class="form-control form-control-solid @error('kontraktor') is-invalid @enderror" name="kontraktor" value="{{ old('kontraktor') }}" placeholder="Nama kontraktor" />
                        @error('kontraktor')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Project') }}</label>
                        <input type="text" class="form-control form-control-solid @error('nama_project') is-invalid @enderror" name="nama_project" value="{{ old('nama_project') }}" placeholder="Nama project" />
                        @error('nama_project')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="orm-group mb-3">
                        <label class="required form-label">{{ __('Nilai Project') }}</label>
                        <input type="text" class="form-control form-control-solid input-currency @error('nilai_project') is-invalid @enderror" id="nilai_project" name="nilai_project" value="{{ old('nilai_project') }}" placeholder="Nilai project" />
                        @error('nilai_project')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Nomor Kontrak') }}</label>
                        <input type="text" class="form-control form-control-solid @error('nomor_kontrak') is-invalid @enderror" name="nomor_kontrak" value="{{ old('nomor_kontrak') }}" placeholder="Nomor kontrak" />
                        @error('nomor_kontrak')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Supervisor') }}</label>
                        <input type="text" class="form-control form-control-solid @error('supervisor') is-invalid @enderror" name="supervisor" value="{{ old('supervisor') }}" placeholder="Nama supervisor" />
                        @error('supervisor')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Jenis Periode') }}</label>
                        <select class="form-control form-control-solid @error('jenis_periode') is-invalid @enderror" name="jenis_periode" data-control="select2" data-placeholder="Pilih jenis period">
                            @foreach ($data['periods'] as $period)
                                <option value="{{ $period }}" @if (old('jenis_periode') == $period) selected @endif>
                                    {{ strtoupper($period) }}
                                </option>
                            @endforeach
                        </select>
                        @error('jenis_periode')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('PIC') }}</label>
                        <select class="form-control form-control-solid @error('pic') is-invalid @enderror" name="pic" data-control="select2" data-placeholder="Pilih PIC">
                            @foreach ($data['users'] as $pic)
                                <option value="{{ $pic->id }}" @if (old('pic') == $pic->id) selected @endif>
                                    {{ strtoupper($pic->name) }}
                                </option>
                            @endforeach
                        </select>
                        @error('pic')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
