@extends('pages.project-monitoring.fleet.index')

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Port - Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Port (IPM)') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="card shadow-sm">
        <div class="card-header">
            <h3 class="card-title">{{ __('Data Main Project') }}</h3>
            <div class="card-toolbar">
                @if (isAdmin())
                    <button type="button" class="btn btn-sm btn-light btn_create_main_project" data-bs-toggle="modal" data-bs-target="#create_main_project_modal">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                        {{ __('Tambah Main Project') }}
                    </button>
                @endif
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="main_project_table">
                    <thead>
                        <tr>
                            <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                            <th class="text-center align-middle min-w-250px" rowspan="2">{{ __('Project') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('PIC') }}</th>
                            <th class="text-center align-middle min-w-75px" rowspan="2">{{ __('Project Status') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('Contractor') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('Project Value') }}</th>
                            <th class="text-center align-middle" colspan="2">{{ __('Project Period (Days)') }}</th>
                            <th class="text-center align-middle" colspan="2">{{ __('Actual Project') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Cost Performances Index(Rp)') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Estimate Time Completion') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Document Remarks') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Non Conformity Record (NCR)') }}</th>
                            <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th>{{ __('Contractual') }}</th>
                            <th>{{ __('Remaining') }}</th>
                            <th>{{ __('Progress') }}</th>
                            <th>{{ __('Quality') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card shadow-sm mt-10">
        <div class="card-header">
            <h3 class="card-title">{{ __('Data Project') }}</h3>
            <div class="card-toolbar">
                @if (isAdmin())
                    <button type="button" class="btn btn-sm btn-light btn_create_project" data-bs-toggle="modal" data-bs-target="#create_project_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                @endif
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="project_table">
                    <thead>
                        <tr>
                            <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                            <th class="text-center align-middle min-w-250px" rowspan="2">{{ __('Project') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('PIC') }}</th>
                            <th class="text-center align-middle min-w-75px" rowspan="2">{{ __('Status') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('Contractor') }}</th>
                            <th class="text-center align-middle min-w-125px" rowspan="2">{{ __('Value') }}</th>
                            <th class="text-center align-middle" colspan="2">{{ __('Project Period (Days)') }}</th>
                            <th class="text-center align-middle" colspan="2">{{ __('Actual Project') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Document Remarks') }}</th>
                            <th class="text-center align-middle" rowspan="2">{{ __('Non Conformity Record (NCR)') }}</th>
                            <th class="text-center align-middle min-w-100px" rowspan="2">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th>{{ __('Contractual') }}</th>
                            <th>{{ __('Remaining') }}</th>
                            <th>{{ __('Progress') }}</th>
                            <th>{{ __('Quality') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('pages.project-monitoring.port.project.modals.create-project')
    @include('pages.project-monitoring.port.project.modals.create-main-project')
    @include('pages.project-monitoring.port.project.modals.doc-remarks')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();

            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                prefix: "Rp. "
            }).mask(".input-currency");

            var table = $('#main_project_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.index') }}",
                    data: function(d) {
                        d.jenis_project = 1
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'nama_project',
                        name: 'nama_project'
                    },
                    {
                        data: 'pic',
                        name: 'pic'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'kontraktor',
                        name: 'kontraktor'
                    },
                    {
                        data: 'nilai_project',
                        name: 'nilai_project'
                    },
                    {
                        data: 'contractual',
                        name: 'contractual'
                    },
                    {
                        data: 'remaining',
                        name: 'remaining'
                    },
                    {
                        data: 'progres',
                        name: 'progres'
                    },
                    {
                        data: 'quality',
                        name: 'quality'
                    },
                    {
                        data: 'cpi',
                        name: 'cpi'
                    },
                    {
                        data: 'etc',
                        name: 'etc'
                    },
                    {
                        data: 'document',
                        name: 'document',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'ncr',
                        name: 'ncr'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 6, 7, 8, 9, 11, 12, 13, 14],
                        className: 'text-center'
                    }
                ]
            });

            var table = $('#project_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.index') }}",
                    data: function(d) {
                        d.jenis_project = 2
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'nama_project',
                        name: 'nama_project'
                    },
                    {
                        data: 'pic',
                        name: 'pic'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'kontraktor',
                        name: 'kontraktor'
                    },
                    {
                        data: 'nilai_project',
                        name: 'nilai_project'
                    },
                    {
                        data: 'contractual',
                        name: 'contractual'
                    },
                    {
                        data: 'remaining',
                        name: 'remaining'
                    },
                    {
                        data: 'progres',
                        name: 'progres'
                    },
                    {
                        data: 'quality',
                        name: 'quality'
                    },
                    {
                        data: 'document',
                        name: 'document',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'ncr',
                        name: 'ncr'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 6, 7, 8, 9, 10, 11, 12],
                        className: 'text-center'
                    }
                ]
            });
        });

        $(document).on('click', '.btn-delete', function() {
            var url = `{{ route('ipm.port.delete', ':project') }}`;
            url = url.replace(':project', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-document', function() {
            var url = `{{ route('ipm.port.document.index', ':project') }}`;
            url = url.replace(':project', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "is_project_index": true
                },
                success: function(data) {
                    var html = '';
                    $('#document-table').empty();

                    data?.documents?.forEach((doc, idx) => {
                        html += `<tr>`;
                        html += `<td class="text-center">${idx + 1}</td>`;
                        html += `<td class="text-center fw-bold">${doc?.jenis_dokumen}</td>`;
                        html += `<td></td>`;
                        html += `<td></td>`;
                        html += `</tr>`;

                        doc?.sub_dokumen_ports?.forEach((sub, key) => {
                            html += `<tr>`;
                            html += `<td class="text-center">${idx + 1}.${key + 1}</td>`;
                            html += `<td class="text-center">${sub?.sub_dokumen}</td>`;
                            html += `<td class="text-center">${data?.['calculated']?.['sub_dokumen']?.[doc?.id]?.[sub?.id]?.['updated_at'] ?? ''}</td>`;
                            html += `<td class="text-center">${data?.['calculated']?.['sub_dokumen']?.[doc?.id]?.[sub?.id]?.['percentage'] ?? "{{ toPercent(0, 2) }}"}</td>`;
                            html += `</tr>`;
                        })
                    });

                    html += `<tr>`;
                    html += `<td class="text-center fw-bold" colspan="3">Total</td>`;
                    html += `<td class="text-center fw-bold">${data?.['calculated']?.['total']}</td>`;
                    html += `</tr>`;

                    $('#document-table').append(html)
                    $('#doc_remarks_modal').modal('show');
                }
            });
        });

        $(document).on('click', '.btn-reopen', function() {
            Swal.fire({
                title: "{{ __('Yakin membuka kembali project ?') }}",
                text: "{{ __('Project akan dibuka kembali!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.port.reopen') }}`,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.href = location.href;
                        }
                    });
                }
            });
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_create_${action}`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }
    </script>
@endpush
