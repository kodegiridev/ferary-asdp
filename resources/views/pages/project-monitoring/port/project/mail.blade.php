<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ __('ASDP Ferary') }}</title>
        <style type="text/css">
            body {
                width: 100%;
            }

            table {
                width: 560px;
                text-align: left;
            }

            span {
                color: black;
            }

            li {
                color: black;
            }

            .d-block {
                display: block;
            }

            .mb-1 {
                margin-bottom: 0.75rem;
            }

            .mt-1 {
                margin-top: 0.75rem;
            }

            .my-1 {
                margin-bottom: 0.75rem;
                margin-top: 0.75rem;
            }
        </style>
    </head>
    <body>
        <table border="0">
            <tbody>
                <tr>
                    <td>
                        <span>
                            {{
                                greetings().', '.$param['receiver']->name
                            }}
                        </span>
                    </td>
                </tr>
                <tr class="spacer"><td><br/></td></tr>
                <tr>
                    <td>
                        <span>
                            {{
                                __('Telah diajukan Project ')
                                .$param['projectMonitoring']->nama_project
                                .__(' oleh ')
                                .$param['sender']->name
                            }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="d-block">
                            {{
                                __('Mohon segera diproses pada Menu Project Monitoring Port di Sistem Ferary.')
                            }}
                        </span>
                        <span class="d-block">
                            {{
                                __('Untuk melihat detail pengajuan tersebut silahkan klik tautan berikut.')
                            }}
                        </span>
                        <span class="d-block">
                            <a href="{{ $param['action'] }}">{{ __('Tautan detail pengajuan') }}</a>
                        </span>
                        <span class="d-block">{{ __('Terima kasih atas perhatian dan kerja samanya.') }}</span>
                        <span class="d-block my-1">{{ __('Salam,') }}</span>
                        <span class="d-block">{{ __('FERARY SYSTEM') }}</span>
                        <span class="d-block">{{ __('PT ASDP Indonesia Ferry') }}</span>
                    </td>
                </tr>
                <tr class="spacer"><td><br/></td></tr>
                <tr>
                    <td>
                        <span class="d-block">{{ __('******************************************') }}</span>
                        <span class="d-block my-1">
                            {{ __('Note: Mohon untuk tidak membalas email ini karena dikirim dari sistem aplikasi') }}
                        </span>
                        <span class="d-block">{{ __('******************************************') }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
