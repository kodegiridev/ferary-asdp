<div class="modal fade" tabindex="-1" id="add_basic_data_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Basic Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form action="{{ route('ipm.port.basic_data.store') }}" method="post">
                @csrf
                <input type="hidden" name="project_id" value="{{ $port->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Mulai Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="mulai_kontrak" type="date" class="form-control form-control-solid" placeholder="Pilih tanggal"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Selesai Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="selesai_kontrak" type="date" class="form-control form-control-solid" placeholder="Pilih tanggal"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Status Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="status_kontrak" id="" class="form-control form-control-solid">
                                    <option value="" disabled>Pilih status kontrak</option>
                                    @if ($basic_data["is_induk"] == 0)
                                    <option value="Induk">Induk</option>
                                    @elseif ($basic_data["is_induk"] == 1)
                                        @if ($basic_data["adendum_seq"] == 0) <option value="Addendum 1">Addendum 1</option> @endif
                                        @if ($basic_data["adendum_seq"] == 1) <option value="Addendum 2">Addendum 2</option> @endif
                                        @if ($basic_data["adendum_seq"] == 2) <option value="Addendum 3">Addendum 3</option> @endif
                                        @if ($basic_data["adendum_seq"] == 3) <option value="Addendum 4">Addendum 4</option> @endif
                                        @if ($basic_data["adendum_seq"] == 4) <option value="Addendum 5">Addendum 5</option> @endif
                                        @if ($basic_data["adendum_seq"] == 5) <option value="Addendum 6">Addendum 6</option> @endif
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#basic_data_modal">{{ __('Kembali') }}</button>
                    <button id="btn-commit-add-basic-data" type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
