<div class="modal fade" tabindex="-1" id="edit_basic_data_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Ubah Information Project') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.port.progress.update_project', $port) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama Project') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input {{ auth()->user()->id != $port->pic ? "readonly" : "" }} name="nama_project" type="text" class="form-control" value="{{ $port->nama_project }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nilai Project') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input {{ auth()->user()->id != $port->pic ? "readonly" : "" }} name="nilai_project" type="text" class="form-control nilai_project" value="{{ $port->nilai_project }}"/>
                            </div>
                        </div>
                    </div>
                    @if (isAdmin())
                    <div class="form-group">
                        <label class="required form-label">{{ __('PIC') }}</label>
                        <select class="form-control form-control-solid" name="pic" id="select-pic">
                            @foreach ($user_pic as $pic)
                                <option value="{{ $pic->id }}" @if ($port->pic == $pic->id) selected @endif>
                                    {{ strtoupper($pic->name) }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nomor Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input {{ auth()->user()->id != $port->pic ? "readonly" : "" }} name="nomor_kontrak" type="text" class="form-control" value="{{ $port->nomor_kontrak }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Kontraktor') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input {{ auth()->user()->id != $port->pic ? "readonly" : "" }} name="kontraktor" type="text" class="form-control" value="{{ $port->kontraktor }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Supervisor') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input {{ auth()->user()->id != $port->pic ? "readonly" : "" }} name="supervisor" type="text" class="form-control" value="{{ $port->supervisor }}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#basic_data_modal">{{ __('Kembali') }}</button>
                    <button id="btn-commit-edit-basic-data" type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
