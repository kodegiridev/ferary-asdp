<div class="modal fade" tabindex="-1" id="add_progress_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Progres') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.port.progress.store') }}" method="post">
                @csrf
                <input type="hidden" name="project_id" value="{{ $port->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Status Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="status_kontrak" type="text" class="form-control" value="{{ $status_kontrak }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tanggal Update') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="tgl_update" type="date" class="form-control" placeholder="Pilih tanggal"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="actual_cost" class="required form-label">
                                {{ __('Actual Cost') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="actual_cost" type="text" class="form-control nilai_project" placeholder="Actual Cost"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="remarks" class="required form-label">
                                {{ __('Remarks') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="remarks" type="text" class="form-control" placeholder="Remarks"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-add-progress" type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
