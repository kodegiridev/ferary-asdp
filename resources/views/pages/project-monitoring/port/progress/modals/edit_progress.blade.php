<div class="modal fade" tabindex="-1" id="edit_progress_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Ubah Progres') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form id="form-update-progress" method="post" action="">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Status Kontrak') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="ep_status_kontrak" name="status_kontrak" type="text" class="form-control"readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tanggal Update') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="ep_tgl_update" name="tgl_update" type="date" class="form-control" placeholder="Pilih tanggal"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="actual_cost" class="required form-label">
                                {{ __('Actual Cost') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="ep_actual_cost" name="actual_cost" type="text" class="form-control nilai_project" placeholder="Actual Cost"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="remarks" class="required form-label">
                                {{ __('Remarks') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="ep_remarks" name="remarks" type="text" class="form-control" placeholder="Remarks"/>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-edit-progress" type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
