@extends('pages.project-monitoring.port.detail', ['title' => $port->nama_project])

@section('progress-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title me-3" style="width:100%">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3  my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1" style="float:right">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Porject Monitoring') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Progress') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <form action="{{ route('ipm.port.index') }}" method="get" enctype="multipart/form-data">
        @csrf
        <?php 
            $status_kontrak_data = "";
            $mulai_kontrak      = "";
            $selesai_kontrak    = "";
            if (!empty($data['project']->latest_basic_data->status_kontrak)) { 
                $status_kontrak_data = $data['project']->latest_basic_data->status_kontrak;  
                $mulai_kontrak = $data['project']->latest_basic_data->mulai_kontrak;  
                $selesai_kontrak = $data['project']->latest_basic_data->selesai_kontrak;  
            } 
        ?>
        <div class="card-body">
            <div class="d-flex justify-content-start">
                <div class="table-responsive" style="width: 100%">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td width="25%">Nama Project</td>
                                <td>: {{ $port->nama_project }}</td>
                            </tr>
                            <tr>
                                <td>Nilai Project</td>
                                <td>: {{ toRupiah($port->nilai_project) }}</td>
                            </tr>
                            <tr>
                                <td>Status Kontrak</td>
                                <td>: {{ $status_kontrak_data }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Mulai</td>
                                <td>: {{ $mulai_kontrak }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Selesai</td>
                                <td>: {{ $selesai_kontrak }}</td>
                            </tr>
                            <tr>
                                <td>Total Plan</td>
                                <td>: {{ toDecimal($data['datas']['total_plan']) }}%</td>
                            </tr>
                            <tr>
                                <td>Total Progres</td>
                                <td>: {{ toDecimal($data['datas']['total_actual']) }}%</td>
                            </tr>
                            <tr>
                                <td>Total Deviasi</td>
                                <td>: <span class="text-success">{{ toDecimal($data['datas']['total_deviasi'])}}%</span></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left">
                                    <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#basic_data_modal" data-id="">{{ __('Basic Data') }}</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="width: 100%">
                    {{-- <canvas id="myChart" style="height: 350px"></canvas> --}}
                    <div id="container" style="height: 350px"></div>
                </div>
            </div>

            <div class="d-flex justify-content-start mt-5 mb-2">
                @if (auth()->user()->id == $port->pic && $status == 'close')
                <button type="button" class="btn btn-sm btn-primary m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#add_progress_modal" data-id="">
                    {{ __('+ Tambah Progres') }}
                </button>
                @endif
                <button type="button" class="btn btn-sm btn-secondary m-1 btn-icon">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr029.svg', 'svg-icon-1') !!}
                </button>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered" id="progress_table">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Bulan') }}</th>
                            <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Tanggal Update') }}</th>
                            <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Actual Cost (Rp)') }}</th>
                            <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Status Kontrak') }}</th>
                            <th class="text-center align-middle" colspan="3" style="color:white !important">{{ __('Progress (%)') }}</th>
                            <th class="text-center align-middle" colspan="3" style="color:white !important">{{ __('Total Progress (%)') }}</th>
                            <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('CPI') }}</th>
                            <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('PPC') }}</th>
                            <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Estimate Time Completion') }}</th>
                            <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Documentation') }}</th>
                            <th class="text-center align-middle w-75px" rowspan="2" style="color:white !important">{{ __('Status') }}</th>
                            <th class="text-center align-middle min-w-150px" rowspan="2" style="color:white !important">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th>{{ __('Plan') }}</th>
                            <th>{{ __('Actual') }}</th>
                            <th>{{ __('Deviasi') }}</th>
                            <th>{{ __('Plan') }}</th>
                            <th>{{ __('Actual') }}</th>
                            <th>{{ __('Deviasi') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-end mt-5 mb-2">
                <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.index') }}">{{ __('Kembali') }}</a>
            </div>
        </div>
    </form>

    <form id="form-delete-documentation" action="" method="post">
        @csrf
    </form>
    <form id="form-delete-progress" action="" method="post">
        @csrf
        @method('delete')
    </form>
    <form id="form-print-documentation" action="" method="post">
        @csrf
        <input type="hidden" name="chart_base64" id="chart_base64">
    </form>

    <?php 
        $basic_data_status_arr = [
            "is_induk"      => 0,
            "adendum_seq"   => 0,
        ]; 
        foreach ($data['basic_data'] as $bs) { 
            if(!in_array($bs->status_kontrak, $basic_data_status_arr)) {
                if ($bs->status_kontrak == 'Induk') {
                    $basic_data_status_arr["is_induk"] = 1;
                } else {
                    $pecah = explode(" ", $bs->status_kontrak);
                    $basic_data_status_arr["adendum_seq"] = $pecah[1];
                }
            } 
        } 
    ?>
    @include('pages.project-monitoring.port.progress.modals.basic_data', ['port' => $port, 'basic_data' => $data['basic_data']])
    @include('pages.project-monitoring.port.progress.modals.add_basic_data', ['port' => $port, 'basic_data' => $basic_data_status_arr])
    @include('pages.project-monitoring.port.progress.modals.edit_basic_data', ['port' => $port, 'user_pic' => $data['user_pic']])

    @include('pages.project-monitoring.port.progress.modals.add_progress', ['port'  => $port, 'status_kontrak' => $status_kontrak_data])
    @include('pages.project-monitoring.port.progress.modals.add_progress_documentation')
    @include('pages.project-monitoring.port.progress.modals.edit_progress')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        let chart_data_raw = {!! $chart !!};
        let chart_data = [
            {
                name: 'Aktual',
                data: [],
            },
            {
                name: 'Induk',
                data: [],
            },
            {
                name: 'Addendum 1',
                data: [],
            },
            {
                name: 'Addendum 2',
                data: [],
            },
            {
                name: 'Addendum 3',
                data: [],
            },
            {
                name: 'Addendum 4',
                data: [],
            },
            {
                name: 'Addendum 5',
                data: [],
            },
            {
                name: 'Addendum 6',
                data: [],
            },
        ];
        let labels = [];
        let is_first_time = [];
        let show_to_chart = [];
        if (chart_data_raw) {
            let is_first_time_ad1 = 0;
            let is_first_time_ad2 = 0;
            let is_first_time_ad3 = 0;
            let is_first_time_ad4 = 0;
            let is_first_time_ad5 = 0;
            let is_first_time_ad6 = 0;
            chart_data[0].data.push(null);
            chart_data[1].data.push(null);
            $.each(chart_data_raw, function(i, item) {
                let status_kontrak = item.status_kontrak;
                labels.push(i);
                chart_data[0].data.push(item.total_actual);
                if (status_kontrak == "Induk") {
                    chart_data[1].data.push(item.total_plan); // Induk
                } else if (status_kontrak == "Addendum 1") {
                    if (is_first_time_ad1 == 0) {
                        total_data_previous = chart_data[1].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[2].data.push(null);
                        }
                        chart_data[1].data.push(item.total_plan);
                        is_first_time_ad1 = 1;
                    }
                    chart_data[2].data.push(item.total_plan); // Addendum 1
                } else if (status_kontrak == "Addendum 2") {
                    if (is_first_time_ad2 == 0) {
                        total_data_previous = chart_data[2].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[3].data.push(null);
                        }
                        chart_data[2].data.push(item.total_plan);
                        is_first_time_ad2 = 1;
                    }
                    chart_data[3].data.push(item.total_plan); // Addendum 2
                } else if (status_kontrak == "Addendum 3") {
                    if (is_first_time_ad3 == 0) {
                        total_data_previous = chart_data[3].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[4].data.push(null);
                        }
                        chart_data[3].data.push(item.total_plan);
                        is_first_time_ad3 = 1;
                    }
                    chart_data[4].data.push(item.total_plan); // Addendum 3
                } else if (status_kontrak == "Addendum 4") {
                    if (is_first_time_ad4 == 0) {
                        total_data_previous = chart_data[4].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[5].data.push(null);
                        }
                        chart_data[4].data.push(item.total_plan);
                        is_first_time_ad4 = 1;
                    }
                    chart_data[5].data.push(item.total_plan); // Addendum 4
                } else if (status_kontrak == "Addendum 5") {
                    if (is_first_time_ad5 == 0) {
                        total_data_previous = chart_data[5].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[6].data.push(null);
                        }
                        chart_data[5].data.push(item.total_plan);
                        is_first_time_ad5 = 1;
                    }
                    chart_data[6].data.push(item.total_plan); // Addendum 5
                } else if (status_kontrak == "Addendum 6") {
                    if (is_first_time_ad6 == 0) {
                        total_data_previous = chart_data[6].data.length;
                        for (let id = 0; id < total_data_previous; id++) {
                            chart_data[7].data.push(null);
                        }
                        chart_data[6].data.push(item.total_plan);
                        is_first_time_ad6 = 1;
                    }
                    chart_data[7].data.push(item.total_plan); // Addendum 6
                }
            });

            let indx = 0;
            $.each(chart_data, function(i, item) {
                if (item.data.length > 0) {
                    show_to_chart[indx] = item;
                    indx++;
                }
            });
        }

        $(document).ready(function() {
            checkSession();

            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 1,
                prefix: "Rp. "
            }).mask(".nilai_project");
        })

        $('#select-pic').select2({
            placeholder: 'Pilih PIC',
            dropdownParent: $('#edit_basic_data_modal')
        });

        function open_document(progress_id) {
            $("#document-list").empty();
            $("#progres_id").val(progress_id);

            $("#btn-commit-add-progress-documentation").show();
                                $("#add_documentation").show();
            var is_pic = `{{ auth()->user()->id == $port->pic ? 1 : 0 }}`;
            if (is_pic == 0) {
                $("#btn-commit-add-progress-documentation").hide();
                                $("#add_documentation").hide(); 
            }

            var url = `{{ route('ipm.port.progress.dokumentasi_get') }}`;
                url = url.replace(':id', progress_id);
            $.ajax({
                url: url,
                type: "POST",
                data:{
                    "progress_id": progress_id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    let html = '';
                    if (data.length > 0) {
                        $.each(data, function(i, item) {
                            var action = `{{ asset('upload/project-monitoring-port/:filename') }}`;
                                action = action.replace(':filename', item.file);
                            var button = `<a onclick="delete_document('${item.id}', '${item.nama}')" style="float: right" href="javascript:void(0)"><i class="fa fa-trash text-danger"></i></a>`;
                            if (is_pic == 0) { 
                                button = '';
                            }
                            html += `
                                <li>
                                    <a href="${action}" target="_blank">${item.nama}</a>
                                    ${button}
                                </li>
                            `;
                        });
                    }
                    $("#document-list").html(html);
                }
            });
        }

        async function edit_progress(progress_id) {
            var can_edit = await check_approval_status(progress_id, 'edit');
            $('#btn-commit-edit-progress').removeAttr("disabled");
            if (!can_edit) {
                $("#btn-commit-edit-progress").attr("disabled", true);
            }

            $("#progress_id").val(progress_id);

            var url = `{{ route('ipm.port.progress.progress_get') }}`;
                url = url.replace(':id', progress_id);

            $.ajax({
                url: url,
                type: "POST",
                data:{
                    "progress_id": progress_id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    var url_post = `{{ route('ipm.port.progress.update', ':id') }}`;
                        url_post = url_post.replace(':id', progress_id);
                    $("#ep_status_kontrak").val(data.status_kontrak);
                    $("#ep_tgl_update").val(data.tgl_update);
                    $("#ep_actual_cost").val(data.actual_cost);
                    $("#ep_remarks").val(data.remarks);
                    $("#form-update-progress").attr('action', url_post);
                }
            });
        }

        function delete_document(id, nama) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Dokumen ${nama}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                    var url = `{{ route('ipm.port.progress.dokumentasi_delete', ':id') }}`;
                        url = url.replace(':id', id);
                    $("#form-delete-documentation").attr('action', url);
                    $("#form-delete-documentation").submit();
                }
            })
        }

        async function duplicate_progress(id, tgl_periode) {
            var can_duplicate = await check_approval_status(id, 'duplikasi');
            if (can_duplicate) {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-warning',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Duplikasi Data',
                    text: `Anda yakin ingin menduplikasi Data Progres Induk Tanggal ${tgl_periode}?`,
                    showCancelButton: true,
                    confirmButtonText: 'Duplikasi!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('ipm.port.progress.duplicate', ':id') }}`;
                            url = url.replace(':id', id);
                        $("#form-delete-documentation").attr('action', url);
                        $("#form-delete-documentation").submit();
                    }
                })
            }
        }

        async function delete_progress(id, tgl_periode) {
            var can_delete = await check_approval_status(id, 'hapus');
            if (can_delete) {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-danger',
                        cancelButton: 'btn btn-secondary'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Hapus Data',
                    text: `Anda yakin ingin menghapus Data Progres Induk Tanggal ${tgl_periode}?`,
                    showCancelButton: true,
                    showConfirmButton: can_delete,
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        // DO DUPLICATE HERE
                        var url = `{{ route('ipm.port.progress.delete', ':id') }}`;
                            url = url.replace(':id', id);
                        $("#form-delete-progress").attr('action', url);
                        $("#form-delete-progress").submit();
                    }
                })
            }
        }

        var table = $('#progress_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ipm.port.progress.index', $data['project']) }}"
            },
            columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false,
            },
            {
                data: 'tgl_update',
                name: 'tgl_update'
            },
            {
                data: 'actual_cost',
                name: 'actual_cost'
            },
            {
                data: 'status_kontrak',
                name: 'status_kontrak'
            },
            {
                data: 'progress_plan',
                name: 'progress_plan',
                orderable: false,
                searchable: false,
            },
            {
                data: 'progress_actual',
                name: 'progress_actual',
                orderable: false,
                searchable: false,
            },
            {
                data: 'progress_deviasi',
                name: 'progress_deviasi',
                orderable: false,
                searchable: false,
            },
            {
                data: 'total_progress_plan',
                name: 'total_progress_plan',
                orderable: false,
                searchable: false,
            },
            {
                data: 'total_progress_actual',
                name: 'total_progress_actual',
                orderable: false,
                searchable: false,
            },
            {
                data: 'total_progress_deviasi',
                name: 'total_progress_deviasi',
                orderable: false,
                searchable: false,
            },
            {
                data: 'cpi',
                name: 'cpi'
            },
            {
                data: 'ppc',
                name: 'ppc'
            },
            {
                data: 'etc',
                name: 'etc'
            },
            {
                data: 'document',
                name: 'document',
                orderable: false,
                searchable: false,
                className: 'text-center'
            },
            {
                data: 'status',
                name: 'status',
                className: 'text-center'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                className: 'text-center'
                },
            ],
        });

        // const ctx = document.getElementById('myChart');

        // new Chart(ctx, {
        //     type: 'line',
        //     data: {
        //         labels: labels,
        //         datasets: [
        //             {
        //                 label: 'Aktual',
        //                 data: chart_data[0].data,
        //                 borderColor: 'rgb(54, 162, 235)',
        //                 // backgroundColor: Utils.transparentize(Utils.CHART_COLORS.blue, 0.5),
        //             },
        //             {
        //                 label: 'Induk',
        //                 data: chart_data[1].data,
        //                 borderColor: 'rgb(255, 99, 132)',
        //                 // backgroundColor: Utils.transparentize(Utils.CHART_COLORS.red, 0.5),
        //             }
        //         ]
        //     },
        //     options: {
        //         responsive: true,
        //         plugins: {
        //             legend: {
        //                 position: 'bottom',
        //             },
        //             title: {
        //                 display: true,
        //                 text: '{{ date('d M Y') }}'
        //             }
        //         }
        //     },
        // });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}_project`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }

        var highChart = Highcharts.chart('container', {
            title: {
                text: `{{ date('d M Y') }}`,
                align: 'center'
            },
            yAxis: {
                title: {
                    text: 'Persentase (%)'
                }
            },
            xAxis: {
                title: {
                    text: 'Bulan Ke - ',
                    style: {
                        fontSize: '1.2em'
                    }
                },
                categories: labels,
                min: 0,
                // max: labels.length - 1,
                tickmarkPlacement: 'on',
                labels:{
                    connectorAllowed: false,
                    // formatter:function(){
                    //     return (this.value + 1);
                    // }
                },
                // step: 1
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
            },
            plotOptions: {
                line: {
                    connectNulls: true
                },
                series: {
                    connectNulls: false,
                    pointPlacement: 'on'
                }
            },
            tooltip: {
                crosshairs: false,
                shared: true,
                formatter: function(){
                    let data_point = this.points;
                    // if (this.x == 0) { return false; }
                    let tooltip_data = `
                        Periode ke ${this.x}
                    `;
                    $.each(data_point, function(i, point) {
                        if (point.series.name !== 'Aktual') {
                            if(typeof data_point[i + 1] !== 'undefined') {
                                return;
                            }
                        }
                        tooltip_data += `
                            <br/>
                            <div style="width:20px; height:20px; border: 1px solid ${point.color}"></div>
                            ${point.series.name} : ${point.y}
                        `;
                    });
                    return tooltip_data;
                    // return this.points.reduce(function (s, point) {
                    //     if (point.series.name !== 'Aktual') {
                    //         next_key = s + 1;
                    //         console.log(s, next_key);
                    //         if(typeof this.points[next_key] === 'undefined') {
                                
                    //         }
                    //         else {
                    //             return;
                    //         }
                    //     }
                    //     return s + '<br/>' + point.series.name + ': ' + point.y + 'm';
                    // }, this.x);
                }
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    }
                }
            },
            colors: ['#027bff', '#db3747', '#f9a12d', '#2df5f9', '#bc2df9', '#f9a72d', '#50f92d', '#f92df3'],
            series: show_to_chart,
        });
        var highChartSVG = highChart.getSVG()
            .replace('/</g', '\n&lt;') // make it slightly more readable
            .replace('/>/g', '&gt;');
        var base64Image = getImageDataURL(highChartSVG);

        function getImageDataURL(svgXml) {
            return "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(svgXml)));
        }

        function print_progress(id) {
            $("#chart_base64").val(base64Image);
            var url = `{{ route('ipm.port.progress_periode.print_periode', ':id') }}`;
                url = url.replace(':id', id);
            $("#form-print-documentation").attr('action', url);
            $("#form-print-documentation").submit();
        }

        async function check_approval_status(progress_id, action) {
            var result = await $.ajax({
                url: `{{ route('ipm.port.progress.approval_status') }}`,
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "progress_id": progress_id
                }
            });

            if (!result?.can_edit) {
                var message = result?.type != 2
                    ? `Progres telah disetujui. Jika ingin melakukan ${action}, klik status!`
                    : `Project telah diakhiri. Silakan buat project baru!`;

                Swal.fire({
                    title: `Progres tidak dapat di${action}!`,
                    text: message,
                    icon: "error",
                });
            }

            return result?.can_edit;
        }

        $(document).on('click', '.btn-reopen', function() {
            Swal.fire({
                title: "{{ __('Yakin membuka kembali progres ?') }}",
                text: "{{ __('Anda harus mengajukan approval kembali!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.port.progress.reopen_progress') }}`,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endpush
