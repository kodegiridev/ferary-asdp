<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')

<head>
    <title>IPM Port | Progres per Periode</title>
    <style>
        @page {
            size: A4 landscape;
        }

        .title {
            text-align: left;
            margin-bottom: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            text-align: center;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .date-place {
            text-align: left;
            font-style: bold;
            margin-top: 10px;
            padding: 0;
        }

        .text-success {
            color: #50cd89 !important;
        }

        /* Signature box styling */
        .signature-box {
            border-top: 1px solid black;
            padding: 8px;
            width: 200px;
            text-align: center;
        }

        .signature-role {
            font-style: bold;
        }

        .signature-name {
            font-weight: bold;
            text-decoration: underline;
        }

        .signature-container {
            width: 100%;
        }

        .signature-content {
            float: right;
        }

        .borderless {
            border: 0;
        }

        .logo {
            height: 150px;
        }
    </style>
</head>

<body>
    
    {{-- <img src="data:image/png;base64,{{ base64_encode($imageLogo) }}" alt="Logo Ferary"  style="width:150px; height:auto"> --}}
    <img class="logo" src="{{ $ctrl->base64Logo() }}" />
    <div class="title">
        <h3>Integrated Project Monitoring Progress Port | Periode ke - {{ $data['sequence'] }}</h3>
    </div>
    
    <?php 
        $status_kontrak_data = "";
        $mulai_kontrak      = "";
        $selesai_kontrak    = "";
        if (!empty($data['project']->project->latest_basic_data->status_kontrak)) { 
            $status_kontrak_data = $data['project']->project->latest_basic_data->status_kontrak;  
            $mulai_kontrak = $data['project']->project->latest_basic_data->mulai_kontrak;  
            $selesai_kontrak = $data['project']->project->latest_basic_data->selesai_kontrak;  
        }
    ?>
    <table style="width: 100%">
        <tr>
            <td class="borderless" width="50%">
                <table style="width: 100%">
                    <tr>
                        <td width="25%" class="borderless">Nama Project</td>
                        <td class="borderless">: {{ $data['project']->project->nama_project }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Nilai Project</td>
                        <td class="borderless">: {{ toRupiah($data['project']->project->nilai_project) }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Actual Cost</td>
                        <td class="borderless">: {{ toRupiah($data['project']->actual_cost) }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Status Kontrak</td>
                        <td class="borderless">: {{ $status_kontrak_data }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Total Plan</td>
                        <td class="borderless">: {{ toPercent($data['datas']['total_plan']) }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Total Progress</td>
                        <td class="borderless">: {{ toPercent($data['datas']['total_progres']) }}</td>
                    </tr>
                    <tr>
                        <td width="25%" class="borderless">Total Deviasi</td>
                        <td class="borderless">: <span class="text-success">{{ toPercent($data['datas']['total_deviasi']) }}</td>
                    </tr>
                </table>
            </td>
            <td class="borderless" width="50%">
                <img src="{{ $data['chart_base64'] }}" alt="" style="width: 100%; height: auto">
            </td>
        </tr>
    </table>
    <table>
        <thead>
            <tr>
                <th rowspan="2">{{ __('No') }}</th>
                <th rowspan="2">{{ __('Item Pekerjaan') }}</th>
                <th rowspan="2">{{ __('Bobot') }}</th>
                <th colspan="4">{{ __('Per Periode (%)') }}</th>
                <th colspan="3">{{ __('Total (%)') }}</th>
            </tr>
            <tr>
                <th>{{ __('Plan') }}</th>
                <th>{{ __('Progress Lalu') }}</th>
                <th>{{ __('Progress Ini') }}</th>
                <th>{{ __('Deviasi') }}</th>
                <th>{{ __('Plan') }}</th>
                <th>{{ __('Progress') }}</th>
                <th>{{ __('Deviasi') }}</th>
            </tr>
        </thead>
        <tbody>
            @if (count($data['main_item']) > 0)
                @foreach ($data['main_item'] as $item)
                    <tr>
                        <td class="middle fw-bold">#</td>
                        <td class="middle fw-bold">{{ $item->main_item }}</td>
                        <td class="middle fw-bold">{{ $item->batas_bobot }}</td>
                        <td class="middle fw-bold">{{ $item->plan }}</td>
                        <td class="middle fw-bold">{{ $item->progres_lalu }}</td>
                        <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                        <td class="middle fw-bold">{{ $item->deviasi }}</td>
                        <td class="middle fw-bold">{{ $item->plan }}</td>
                        <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                        <td class="middle fw-bold">{{ $item->deviasi }}</td>
                    </tr> 
                    @if (!empty($item->sub_item))
                    @foreach ($item->sub_item as $subitem)
                    <tr>
                        <td class="middle fw-bold">{{ $loop->iteration }}</td>
                        <td class="middle fw-bold">{{ $subitem->sub_item }}</td>
                        <td class="middle fw-bold">{{ $subitem->bobot }}</td>
                        <td class="middle fw-bold">{{ $subitem->plan }}</td>
                        <td class="middle fw-bold">{{ $subitem->progres_lalu }}</td>
                        <td class="middle fw-bold">{{ $subitem->progres_ini }}</td>
                        <td class="middle fw-bold">{{ $subitem->deviasi }}</td>
                        <td class="middle fw-bold">{{ $subitem->plan }}</td>
                        <td class="middle fw-bold">{{ $subitem->progres_ini }}</td>
                        <td class="middle fw-bold">{{ $subitem->deviasi }}</td>
                    </tr> 
                    @endforeach
                    @endif 
                @endforeach
            @else
            <tr>
                <td colspan="10" style="text-align: center" class="middle fw-bold">No Data</td>
            </tr>
            @endif

        </tbody>
    </table>

    {{-- <div class="signature-container">
        <div class="signature-content">
            <div class="date-place">
                <p>
                    Dikeluarkan: Jakarta <br>
                    Pada Tanggal: {{ now()->locale('id')->translatedFormat('d F Y') }}
                </p>
            </div>
            <div class="signature-box">
            </div>
        </div>
    </div> --}}
</body>

</html>
