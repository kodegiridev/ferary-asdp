@extends('pages.project-monitoring.port.detail', ['title' => $data['project']->project->nama_project, 'project_id' => $data['project']->project_id])

@section('progress-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.progress.index', $data['project']->project_id) }}" class="text-muted text-hover-primary">{{ __('Progress') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">Periode</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-start">
        <div class="table-responsive" style="width: 100%">
            <table class="table">
                <?php 
                    $status_kontrak_data = "";
                    $mulai_kontrak      = "";
                    $selesai_kontrak    = "";
                    if (!empty($data['project']->project->latest_basic_data->status_kontrak)) { 
                        $status_kontrak_data = $data['project']->project->latest_basic_data->status_kontrak;  
                        $mulai_kontrak = $data['project']->project->latest_basic_data->mulai_kontrak;  
                        $selesai_kontrak = $data['project']->project->latest_basic_data->selesai_kontrak;  
                    }
                ?>
                <tbody>
                    <tr>
                        <td width="25%">Nama Project</td>
                        <td>: {{ $data['project']->project->nama_project }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']->project->nilai_project) }}</td>
                    </tr>
                    <tr>
                        <td>Status Kontrak</td>
                        <td>: {{ $status_kontrak_data }}</td>
                    </tr>
                    <tr>
                        <td>Total Plan</td>
                        <td>: {{ toDecimal($data['datas']['total_plan']) }}</td>
                    </tr>
                    <tr>
                        <td>Total Progres</td>
                        <td>: {{ toDecimal($data['datas']['total_progres']) }}%</td>
                    </tr>
                    <tr>
                        <td>Total Deviasi</td>
                        <td>: <span class="text-success">{{ toDecimal($data['datas']['total_deviasi']) }}%</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @if ($data['project']->project->pic == auth()->user()->id && $data['can_edit'])
    <div class="d-flex justify-content-start mt-5 mb-2">
        <button type="button" class="btn btn-sm btn-primary m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#add_main_item_modal" data-id="">
            {{ __('+ Tambah Main Item') }}
        </button>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-bordered" id="project_table">
            <thead class="bg-primary text-white">
                <tr>
                    <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('No') }}</th>
                    <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Item Pekerjaan') }}</th>
                    <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Bobot') }}</th>
                    <th class="text-center align-middle w-200px" colspan="4" style="color:white !important">{{ __('Per Periode (%)') }}</th>
                    <th class="text-center align-middle w-200px" colspan="3" style="color:white !important">{{ __('Total (%)') }}</th>
                    <th class="text-center align-middle min-w-150px" rowspan="2" style="color:white !important">{{ __('Aksi') }}</th>
                </tr>
                <tr>
                    <th>{{ __('Plan') }}</th>
                    <th>{{ __('Progress Lalu') }}</th>
                    <th>{{ __('Progress Ini') }}</th>
                    <th>{{ __('Deviasi') }}</th>
                    <th>{{ __('Plan') }}</th>
                    <th>{{ __('Progress') }}</th>
                    <th>{{ __('Deviasi') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php $sisa_bobot_toadd = 100; ?>
                @if (!empty($data['main_item']))
                    @foreach ($data['main_item'] as $item)
                        <?php $sisa_bobot_toadd -= $item->batas_bobot; ?>
                        <tr>
                        <td class="middle fw-bold">#</td>
                            <td class="middle fw-bold">{{ $item->main_item }}</td>
                            <td class="middle fw-bold">{{ $item->batas_bobot }}</td>
                            <td class="middle fw-bold">{{ $item->plan }}</td>
                            <td class="middle fw-bold">{{ $item->progres_lalu }}</td>
                            <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $item->deviasi }}</td>
                            <td class="middle fw-bold">{{ $item->plan }}</td>
                            <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $item->deviasi }}</td>
                            <td>
                                @if ($data['project']->project->pic == auth()->user()->id)
                                <a href="{{ route('ipm.port.progress_periode.edit_periode_item', ['project' => $item->progres_id, 'main_item' => $item]) }}" class="btn btn-success btn-sm m-1">Ubah</a>
                                <a onclick="delete_item('{{ $item->id }}', '{{ $item->main_item }}')" href="javascript:void(0)" class="btn btn-danger btn-sm m-1">Hapus</a>
                                @endif
                            </td>
                        </tr> 
                        @if (!empty($item->sub_item))
                            @foreach ($item->sub_item as $subitem)
                                <?php $task_item = $subitem->task_item->first(); ?>
                                <tr>
                                    <td class="middle fw-bold">{{ $loop->iteration }}</td>
                                    <td class="middle fw-bold">{{ $subitem->sub_item }}</td>
                                    <td class="middle fw-bold">{{ $task_item->bobot }}</td>
                                    <td class="middle fw-bold">{{ $task_item->plan }}</td>
                                    <td class="middle fw-bold">{{ $task_item->progres_lalu }}</td>
                                    <td class="middle fw-bold">{{ $task_item->progres }}</td>
                                    <td class="middle fw-bold">{{ $task_item->deviasi }}</td>
                                    <td class="middle fw-bold">{{ $task_item->plan }}</td>
                                    <td class="middle fw-bold">{{ $task_item->progres }}</td>
                                    <td class="middle fw-bold">{{ $task_item->deviasi }}</td>
                                    <td></td>
                                </tr> 
                            @endforeach
                        @endif 
                    @endforeach
                @endif
            </tbody>
        </table>
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.progress.index', $data['project']->project_id) }}">{{ __('Kembali') }}</a>
    </div>

    <form id="form-delete-main-item" action="" method="post">
        @csrf
    </form>

    @include('pages.project-monitoring.port.progress.periode.modals.add_main_item', ['progres' => $data['project']])
@endsection
@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $("#add_main_item_batas_bobot").attr('max', {{ $sisa_bobot_toadd }})
        })

        function delete_item(id, item_name) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus ${item_name}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                    var url = `{{ route('ipm.port.progress_periode.delete_main_item', ':id') }}`;
                        url = url.replace(':id', id);
                    $("#form-delete-main-item").attr('action', url);
                    $("#form-delete-main-item").submit();
                }
            })
        }

        

        var table = $('#project_table').DataTable({
            order: [],
        });
    </script>
@endpush
