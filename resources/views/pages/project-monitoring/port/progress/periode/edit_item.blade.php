@extends('pages.project-monitoring.port.detail', ['title' => $data['project']->project->nama_project, 'project_id' => $data['project']->project_id])

@section('progress-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
<div class="page-title me-3" style="width:100%">
    <h1 class="page-heading d-flex text-dark fw-bold fs-3  my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1" style="float:right">
        <li class="breadcrumb-item text-muted">
            <a href="/" class="text-muted text-hover-primary">{{ __('Project Monitoring') }}</a>
        </li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-400 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
        </li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-400 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">{{ __($data['project']->project->nama_project) }}</li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-400 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">{{ __('Progress') }}</li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-400 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">{{ __($data['project']->tgl_update) }}</li>
    </ul>
</div>
@endsection

@section('page-content')

    {{-- <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="card shadow-sm">
                        <div class="card-header" style="background-color: #00a5bb">
                            <h3 class="card-title text-light">{{ __('+ Edit Detail Item Progress') }}</h3>
                            <div class="card-toolbar"></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <form id="form-input" method="post" action="{{ route('ipm.port.progress_periode.update_main_item') }}">
        @csrf
        <input type="hidden" name="main_item_id" value="{{ $data['main_item']->id }}">
        <input type="hidden" name="progress_id" value="{{ $data['main_item']->progres_id }}">
        <div class="card-body">
            <div style="margin-right: 150px; margin-left: 150px">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Main Item</th>
                                <th colspan="2">Batas Bobot</th>
                                <th>Sisa Bobot</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th>
                                    <input name="main_item" type="text" class="form-control" value="{{ $data['main_item']->main_item }}" placeholder="Main Item"/>
                                </th>
                                <th colspan="2"><input name="batas_bobot" type="text" class="form-control" value="{{ $data['main_item']->batas_bobot }}"/></th>
                                <th> <input id="sisa_bobot" type="number" class="form-control" value="{{ $data['sisa_bobot'] }}" readonly/></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="sub-item">
                            @php $sisa_bobot_tanpa_task = $data['sisa_bobot']; @endphp
                            @if (count($data['main_item']->sub_item) > 0)
                            @foreach ($data['main_item']->sub_item as $i => $subitem)
                            <?php  $task_item = $subitem->task_item->first(); ?>
                            <tr class="head subitem-{{ $i }}">
                                <td>SubItem <span class="ordering-data" id="ordering-{{ $i }}">{{ $i + 1 }}</span></td>
                                <td>Bobot (%)</td>
                                <td>Plan (%)</td>
                                <td>Progress (%)</td>
                                <td></td>
                            </tr>
                            <tr data-id="subitem-{{ $i }}" class="subitem-{{ $i }} subitem">
                                <td><input required name="sub_item[]" value="{{ $subitem->sub_item }}" type="text" class="form-control" placeholder="Sub Item"/></td>
                                <td><input id="subitem-{{ $i }}-bobot" onkeyup="calculate_bobot()" name="bobot[]" value="{{ $task_item->bobot }}" type="text" class="form-control" placeholder="Bobot"/></td>
                                <td><input name="plan[]" value="{{ $task_item->plan }}" type="text" class="form-control" placeholder="Plan"/></td>
                                <td><input name="progres[]" value="{{ $task_item->progres }}" type="text" class="form-control" placeholder="Progress"/></td>
                                <td>
                                    <button onclick="delete_subitem('subitem-{{ $i }}')" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            @php $sisa_bobot_tanpa_task += $task_item->bobot; @endphp
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-between mt-5 mb-2 mr-5 ml-5">
                    <button onclick="add_row()" type="button" class="btn btn-sm btn-success m-1">
                        {{ __('+ Tambah Sub Item') }}
                    </button>
                    <button id="submit-button" type="button" class="btn btn-sm btn-primary m-1">
                        {{ __('Simpan') }}
                    </button>
                </div>
            </div>
            <div class="d-flex justify-content-end mt-5 mb-2">
                <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.progress_periode.index_periode', $data['project']) }}">{{ __('Kembali') }}</a>
            </div>
        </div>
    </form>
@endsection
@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        add_row();
        checkSession();

        $("#submit-button").on('click', function() {
            let sisa_bobot = Number($("#sisa_bobot").val());
            if (sisa_bobot < 0) {
                return Swal.fire({
                    text: `Sisa bobot tidak boleh minus!`,
                    icon: "error",
                });
            }
            $("#form-input").submit();
        })
    });
    function delete_subitem(classname) {
        $("." + classname).remove();
        reorder_number();
        calculate_bobot();
    }

    function add_row() {
        let sisa_bobot = Number($("#sisa_bobot").val());
        if (sisa_bobot <= 0) {
            return Swal.fire({
                text: `Sisa bobot sudah habis!`,
                icon: "error",
            });
        }
        let randomNumber = Math.floor(Math.random() * 100000);

        let html = `
            <tr class="head subitem-${randomNumber}">
                <td>SubItem <span class="ordering-data" id="ordering-${randomNumber}"></span></td>
                <td>Bobot (%)</td>
                <td>Plan (%)</td>
                <td>Progress (%)</td>
                <td></td>
            </tr>
            <tr data-id="subitem-${randomNumber}" class="subitem-${randomNumber} subitem">
                <td><input required name="sub_item[]" type="text" class="form-control" placeholder="Sub Item"/></td>
                <td><input id="subitem-${randomNumber}-bobot" onkeyup="calculate_bobot()" name="bobot[]" type="number" class="form-control" placeholder="Bobot"/></td>
                <td><input name="plan[]" type="number" class="form-control" placeholder="Plan"/></td>
                <td><input name="progres[]" type="number" class="form-control" placeholder="Progress"/></td>
                <td>
                    <button onclick="delete_subitem('subitem-${randomNumber}')" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        `;

        $("#sub-item").append(html);
        reorder_number();
    }

    function calculate_bobot() {
        let sisa_bobot_data = Number({{ $sisa_bobot_tanpa_task }});
        let total_bobot = 0;
        $('.subitem').each(function(i, obj) {
            let dataid = $(this).data('id');
            let bobot = Number($(`#${dataid}-bobot`).val());
            console.log("BOBOT " + dataid, bobot);
            total_bobot += bobot;
        });
        let sisa_bobot = sisa_bobot_data - total_bobot;
        console.log(sisa_bobot_data, total_bobot);
        $("#sisa_bobot").val(sisa_bobot);
    }
    
    function reorder_number() {
        $('.ordering-data').each(function(i, obj) {
            let nomor = i + 1;
            $(this).text(nomor);
        });
    }

    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: `{{ Session::get('success') }}`,
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: `{{ Session::get('failed') }}`,
                icon: "error"
            });
        }
    }
</script>
@endsection
