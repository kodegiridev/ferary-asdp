@extends('pages.project-monitoring.port.detail')

@section('document-tab')
    active
@endsection

@section('styles')
    <style>
        .form-check-input {
            border: 1.75px solid var(--bs-gray-700) !important;
        }
    </style>
@endsection

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Document') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-start">
        <div class="col-md-6 col-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="25%">Nama Project</td>
                        <td>: {{ $data['project']['nama_project'] }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex justify-content-start mb-10">
        <button type="button" class="btn btn-sm btn-primary btn_create_document_modal" data-bs-toggle="modal" data-bs-target="#create_document_modal">
            {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
            {{ __('Tambah Document') }}
        </button>
        &nbsp
        &nbsp
        <a href="{{ route('ipm.port.document.print', $data['project']['id']) }}" class="btn btn-sm btn-warning btn-icon">
            {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
        </a>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="text-center">
                    <th class="w-20px">{{ __('No') }}</th>
                    <th>{{ __('Document Title') }}</th>
                    <th>{{ __('Last Update') }}</th>
                    <th class="w-50px">{{ __('Progress') }}</th>
                    <th class="w-150px">{{ __('Aksi') }}</th>
                </tr>
            </thead>
            <tbody class="align-middle">
                @foreach ($data['documents'] as $i => $doc)
                    <tr>
                        <td>{{ $i + 1 }}</td>
                        <td class="text-center fw-bold">{{ $doc['jenis_dokumen'] }}</td>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                            @if ($i == 0)
                                <a type="button" class="btn btn-sm btn-icon btn-success btn_edit_document_modal" data-id="{{ $doc['jenis_dokumen'] }}">
                                    {!! theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-1') !!}
                                </a>
                            @else
                                <a class="btn btn-sm btn-icon btn-success btn_edit_document_modal" data-id="{{ $doc['jenis_dokumen'] }}">
                                    {!! theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-1') !!}
                                </a>
                                <a class="btn btn-sm btn-icon btn-danger btn_delete_document" type="button" data-id="{{ $doc['id'] }}">
                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                </a>
                            @endif
                        </td>
                    </tr>
                    @foreach ($doc['sub_dokumen_ports'] as $j => $sub)
                        <tr>
                            <td>{{ $i + 1 . '.' . $j + 1 }}</td>
                            <td class="text-center">{{ $sub['sub_dokumen'] }}</td>
                            <td class="text-center">{{ $data['calculated']['sub_dokumen'][$doc->id][$sub->id]['updated_at'] ?? '' }}</td>
                            <td class="text-center">{{ $data['calculated']['sub_dokumen'][$doc->id][$sub->id]['percentage'] ?? toPercent(0, 2) }}</td>
                            <td class="text-center">
                                @if ($i == 0 && $j == 0)
                                    <a href="{{ route('ipm.port.document.sub.index', [$data['project']['id'], $sub['id']]) }}" class="btn btn-sm btn-icon btn-primary">
                                        {!! theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') !!}
                                    </a>
                                    <a href="{{ route('ipm.port.document.sub.print', [$data['project']['id'], $sub['id']]) }}" class="btn btn-sm btn-warning btn-icon">
                                        {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
                                    </a>
                                @else
                                    <a href="{{ route('ipm.port.document.sub.index', [$data['project']['id'], $sub['id']]) }}" class="btn btn-sm btn-icon btn-primary">
                                        {!! theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') !!}
                                    </a>
                                    <a class="btn btn-sm btn-icon btn-danger btn_delete_sub_document" type="button" data-id="{{ $sub['id'] }}">
                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                    </a>
                                    <a href="{{ route('ipm.port.document.sub.print', [$data['project']['id'], $sub['id']]) }}" class="btn btn-sm btn-warning btn-icon">
                                        {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                <tr>
                    <td class="text-center" colspan="3">Total</td>
                    <td>{{ $data['calculated']['total'] }}</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>

    @include('pages.project-monitoring.port.document.modals.create')
    @include('pages.project-monitoring.port.document.modals.edit')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();
        });

        $(document).on('click', '.btn_delete_document', function() {
            var url = `{{ route('ipm.port.document.delete', $data['project']['id']) }}`;
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "type": "JenisDokumenPort",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        },
                        error: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn_delete_sub_document', function() {
            var url = `{{ route('ipm.port.document.delete', $data['project']['id']) }}`;

            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "type": "SubDokumenPort",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        },
                        error: function() {
                            location.reload();
                        }
                    });
                }
            });
        });
    </script>

    @stack('modal-scripts')
@endpush
