<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_detail_document_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.port.document.sub.index', [$data['project']['id'], $data['sub_dokumen']['id']]) }}" method="POST" enctype="multipart/form-data" id="form-add-dokumen">
                @csrf
                <input type="hidden" value="create_detail_document_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Dokumen') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="form-label required">{{ __('Judul Dokumen') }}</label>
                        <input type="text" class="form-control form-control-solid @error('judul_dokumen') is-invalid @enderror" name="judul_dokumen" required />
                        @error('judul_dokumen')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Status Pemenuhan') }}</label>
                        <select class="form-control form-control-solid status_pemenuhan @error('status_pemenuhan') is-invalid @enderror" name="status_pemenuhan" data-control="select2" data-placeholder="Pilih status pemenuhan" required>
                            @foreach ($data['status'] as $key => $status)
                                <option></option>
                                <option value="{{ $key }}">
                                    {{ strtoupper($status) }}
                                </option>
                            @endforeach
                        </select>
                        @error('status_pemenuhan')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Remarks') }}</label>
                        <input type="text" class="form-control form-control-solid @error('remarks') is-invalid @enderror" name="remarks" />
                        @error('remarks')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 no_dokumen">
                        <label class="form-label required">{{ __('Nomor Dokumen') }}</label>
                        <input type="text" class="form-control form-control-solid @error('nomor_dokumen') is-invalid @enderror" name="nomor_dokumen" />
                        @error('nomor_dokumen')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 file_dokumen">
                        <label class="form-label required">{{ __('Dokumen') }}</label>
                        <input type="file" class="form-control form-control-solid @error('file') is-invalid @enderror" name="file" accept=".pdf" />
                        @error('file')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="button" class="btn btn-sm btn-primary" id="submit-add-dokumen">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
