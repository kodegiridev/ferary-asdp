@extends('pages.project-monitoring.port.detail')

@section('document-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.document.index', $data['project']['id']) }}" class="text-muted text-hover-primary">{{ __('Document') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-between align-items-center p-0">
        <h3>{{ $data['jenis_dokumen']['jenis_dokumen'] . ': ' . $data['sub_dokumen']['sub_dokumen'] }}</h3>
        <button type="button" class="btn btn-sm btn-primary btn_create_detail_document_modal" data-bs-toggle="modal" data-bs-target="#create_detail_document_modal">
            <i class="fa fa-plus"></i>
            {{ __('Tambah Dokumen') }}
        </button>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id="document_project_table">
            <thead>
                @if ($data['sub_dokumen']['sub_dokumen'] != 'Executive Summary')
                    <tr>
                        <th class="text-center align-middle" rowspan="2">{{ __('No') }}</th>
                        <th class="text-center align-middle" rowspan="2">{{ __('Judul Dokumen') }}</th>
                        <th class="text-center align-middle" rowspan="2">{{ __('Nomor Dokumen') }}</th>
                        <th class="text-center align-middle" colspan="2">{{ __('Status Pemenuhan') }}</th>
                        <th class="text-center align-middle" rowspan="2">{{ __('File') }}</th>
                        <th class="text-center align-middle" rowspan="2">{{ __('Remarks') }}</th>
                        @if ($data['is_pic'])
                            <th class="text-center align-middle" rowspan="2">{{ __('Aksi') }}</th>
                        @endif
                    </tr>
                    <tr>
                        <th class="text-center">{{ __('Ya') }}</th>
                        <th class="text-center">{{ __('Tidak') }}</th>
                    </tr>
                @else
                    <tr>
                        <th class="text-center align-middle">{{ __('No') }}</th>
                        <th class="text-center align-middle">{{ __('Judul Dokumen') }}</th>
                        <th class="text-center align-middle">{{ __('File') }}</th>
                        @if ($data['is_pic'])
                            <th class="text-center align-middle">{{ __('Aksi') }}</th>
                        @endif
                    </tr>
                @endif
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    @include('pages.project-monitoring.port.document.detail.modals.create')
    @include('pages.project-monitoring.port.document.detail.modals.edit')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end">
        <a class="btn btn-secondary btn-sm" href="{{ route('ipm.port.document.index', $data['project']['id']) }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('.no_dokumen').hide();
            $('.file_dokumen').hide();
            checkSession();
        });

        var columns = [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false,
            },
            {
                data: 'judul_dokumen',
                name: 'judul_dokumen',
            },
            {
                data: 'file',
                name: 'file',
            }
        ];
        if ("{{ $data['is_pic'] }}") {
            columns.push({
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                className: 'text-center'
            });
        }
        if ("{{ $data['sub_dokumen']['sub_dokumen'] != 'Executive Summary' }}") {
            columns.push({
                data: 'nomor_dokumen',
                name: 'nomor_dokumen',
            }, {
                data: 'yes',
                name: 'yes',
                className: 'text-center'
            }, {
                data: 'no',
                name: 'no',
                className: 'text-center'
            }, {
                data: 'remarks',
                name: 'remarks',
            });
        }

        var table = $('#document_project_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ipm.port.document.sub.index', [$data['project']['id'], $data['sub_dokumen']['id']]) }}"
            },
            columns,
            columnDefs: [{
                target: "_all",
                className: 'align-middle'
            }]
        });

        $(document).on('click', '.btn-edit', function() {
            var url = "{{ route('ipm.port.document.sub.show', [$data['project']['id'], $data['sub_dokumen']['id']]) }}";

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id')
                },
                success: function(data) {
                    $('#edit_sub_document_modal input[name="id"]').val(data?.id);
                    $('#edit_sub_document_modal input[name="judul_dokumen"]').val(data?.judul_dokumen);
                    $('#edit_sub_document_modal input[name="remarks"]').val(data?.remarks);
                    $('#edit_sub_document_modal input[name="nomor_dokumen"]').val(data?.nomor_dokumen);
                    $('#edit_status_pemenuhan').empty();

                    const types = ['TIDAK ADA', 'ADA'];
                    for (let i = types.length - 1; i >= 0; i--) {
                        const isSelected = i === data?.status_pemenuhan ? 'selected' : '';
                        $('#edit_status_pemenuhan').append(`<option value="${i}" ${isSelected}>${types[i]}</option>`);
                    }


                    $('#edit_sub_document_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        text: err?.responseText,
                        icon: "error"
                    });
                }
            });
        });

        $(document).on('click', '.btn-delete', function() {
            var url = "{{ route('ipm.port.document.sub.index', [$data['project']['id'], $data['sub_dokumen']['id']]) }}";

            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $('.status_pemenuhan').on('select2:select', (e) => {
            const parent = e.currentTarget.parentElement.parentElement
            const {
                text
            } = e.params.data

            if ($.trim(text) == 'ADA') {
                $('.no_dokumen').show();
                $('.file_dokumen').show();
            } else {
                $('.no_dokumen').hide();
                $('.file_dokumen').hide();

                $('.no_dokumen input[name="nomor_dokumen"]').val('');
                $('.file_dokumen input[name="file"]').val('');
            }
        })

        $(document).on('click', '#submit-add-dokumen', function() {
            var dokumenNo = $('#create_detail_document_modal input[name="nomor_dokumen"]').val();
            var status = $('#create_detail_document_modal .status_pemenuhan').val();
            if (status == 1) {
                if (!dokumenNo) {
                    return Swal.fire({
                        title: "Gagal, Tambah Dokumen!",
                        text: "Isi Nomor Dokumen.",
                        icon: "error"
                    });
                }

                var dokumenFile = $('#create_detail_document_modal input[name="file"]').val();
                if (!dokumenFile) {
                    return Swal.fire({
                        title: "Gagal, Tambah Dokumen!",
                        text: "Pilih Dokumen Yang Akan Diunggah.",
                        icon: "error"
                    });
                }
            }

            $('#form-add-dokumen').submit();
        })
    </script>
    @stack('modal-scripts')
@endpush
