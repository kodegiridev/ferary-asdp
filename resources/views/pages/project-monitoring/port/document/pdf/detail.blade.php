<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')

<head>
    <title> {{ $data['file_name'] }} </title>
    <style type="text/css">
        html,
        body {
            margin: 0px;
            padding: 0px;
        }

        @page {
            size: A4 landscape;
            margin-top: 100px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }

        .logo {
            height: 150px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .content {
            padding: 24px;
        }

        .table {
            padding: 10px;
        }

        .table-data th,
        .table-data td {
            border: 1px solid black;
            padding: 8px
        }

        .table-data th,
        .center {
            text-align: center;
        }

        .checkmark {
            font-style: bold;
        }
    </style>

</head>

<body>
    <div class="content">
        <div class="mb-10">
            <img class="logo" src="{{ $ctrl->base64Logo() }}" />
            <h2>Visual Document Port | {{ $data['project']['nama_project'] }}</h2>
        </div>
        <div class="table">
            <h3>{{ $data['document']['jenis_dokumen']['jenis_dokumen'] }}: {{ $data['document']['sub_dokumen'] }}</h3>
            <table class="table-data">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Judul Dokumen</th>
                        <th rowspan="2">Nomor Dokumen</th>
                        <th colspan="2">Status Pemenuhan</th>
                        <th rowspan="2">File</th>
                        <th rowspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <th>Ada</th>
                        <th>Tidak</th>
                    </tr>
                </thead>
                @if (count($data['files']) > 0)
                    <tbody>
                        @foreach ($data['files'] as $key => $value)
                            <tr>
                                <td class="center">{{ $key + 1 }}</td>
                                <td>{{ $value->judul_dokumen }}</td>
                                <td>{{ $value->nomor_dokumen }}</td>
                                @if ($value->status_pemenuhan)
                                    <td class="center"><span class="checkmark">V</span></td>
                                @else
                                    <td></td>
                                @endif
                                @if (!$value->status_pemenuhan)
                                    <td class="center"><span class="checkmark">V</span></td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ $ctrl->getFileName($value->file) }}</td>
                                <td>{{ $value->remarks }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</body>

</html>
