<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="add_item_facility_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="url" value="{{ route('ipm.port.quality.item.store', [$data['project']['id'], $data['period']['id']]) }}">
                <input type="hidden" name="jenis_data">
                <input type="hidden" name="action" value="add_item_facility_modal">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Code') }}</label>
                        <input type="text" class="form-control @error('kode') is-invalid @enderror" name="kode" required>
                        @error('kode')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Sub Facility') }}</label>
                        <input type="text" class="form-control @error('sub_fasilitas') is-invalid @enderror" name="sub_fasilitas" required>
                        @error('sub_fasilitas')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Area') }}</label>
                        <input type="text" class="form-control @error('area') is-invalid @enderror" name="area" required>
                        @error('area')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Evidence') }}</label>
                        <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf">
                        @error('evidence')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="required form-label">{{ __('Remarks') }}</label>

                        <!--begin::Repeater-->
                        <div id="add_item_facility_remark">
                            <div data-repeater-list="remarks">
                                <div data-repeater-item>
                                    @forelse(old('remarks') ?? [] as $remark)
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="remark" value="{{ $remark['remark'] }}" placeholder="Enter the remakrs" required />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="remark" placeholder="Enter the remakrs" required />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>

                            <div class="form-group mt-3 col-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                    Add
                                </a>
                            </div>
                        </div>
                        <!--end::Repeater-->
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Simpan') }}</button>
                </div>
        </div>
        </form>
    </div>
</div>
@push('modal-scripts')
    <script>
        $(document).ready(function() {
            $('#add_item_facility_remark').repeater({
                initEmpty: false,
                show: function() {
                    $(this).slideDown();
                    checkAndHideFacilityButtonOnCreate(8);
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                    checkAndHideFacilityButtonOnCreate(9);
                }
            });
        });

        function checkAndHideFacilityButtonOnCreate(max) {
            var itemElements = $('#add_item_facility_remark [data-repeater-item]');
            var addButton = $('#add_item_facility_remark [data-repeater-create]');

            if (itemElements.length >= max) {
                addButton.hide();
            } else {
                addButton.show();
            }
        }
    </script>
@endpush
