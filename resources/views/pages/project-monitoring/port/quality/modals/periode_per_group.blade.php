<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="periode_per_group_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary btn-sm mb-5" data-bs-stacked-modal="#create_periode_modal">{{ __('Tambah Periode') }}</button>
                <div class="table-responsive">
                    <table class="table table-bordered" id="period_table">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th style="color:white !important">{{ strtoupper($data['project']['jenis_periode']) }} {{ __(' KE') }}</th>
                                <th style="color:white !important">{{ __('LAST UPDATED') }}</th>
                                <th style="color:white !important">{{ __('DETAIL') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer flex justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
            </div>
        </div>
    </div>
</div>

@push('modal-scripts')
    <script>
        $(document).on('submit', '#add_period', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    Swal.fire({
                        text: response?.message,
                        icon: "success"
                    });

                    $('#create_periode_modal').modal('hide');
                    dataPeriod($('#create_periode_modal input[name="id"]').val());
                },
                error: function(error) {
                    $('#create_periode_modal').modal('hide');
                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        $(document).on('click', '.btn-detail-period', function() {
            var url = `{{ route('ipm.port.quality.detail', [$data['project']['id'], ':period']) }}`;
            url = url.replace(':period', $(this).data('id'));
            window.location.href = url
        });

        function dataPeriod(subId) {
            $('#period_table').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                lengthChange: false,
                pageLength: 5,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.quality.period.index', $data['project']['id']) }}",
                    type: "GET",
                    data: function(d) {
                        d.is_datatable = true;
                        d.id = subId;
                    }
                },
                columns: [{
                        data: 'periode',
                        name: 'periode'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }
    </script>
@endpush
