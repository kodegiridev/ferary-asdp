<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_group_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form_port_quality_create_group" class="form" action="{{ route('ipm.port.quality.group.store', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create_group_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Add Group') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Nama Group') }}</label>
                        <select class="form-control group_type @error('fasilitas') is-invalid @enderror" name="fasilitas" data-control="select2" data-placeholder="Pilih data" required>
                            <option></option>
                            @foreach ($data['types'] as $type)
                                <option value="{{ $type }}" @if ($type == old('fasilitas')) selected @endif>
                                    {{ $type }}
                                </option>
                            @endforeach
                        </select>
                        @error('fasilitas')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-hidden" id="group_selected">
                        <div class="form-group my-3">
                            <label class="form-label">{{ __('Sisa Bobot: ') }}<span class="max-bobot"></span></label>
                        </div>
                        <div class="form-group mb-3">
                            <label class="form-label">{{ __('Bobot') }}</label>
                            <input type="text" class="form-control @error('bobot') is-invalid @enderror" name="bobot">
                            <input type="hidden" name="temp_bobot">
                            @error('bobot')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mb-3" id="content_sub_group_type">
                            <label class="required form-label">{{ __('Sub Facility') }}</label>
                            <div id="sub_group_type"></div>
                        </div>
                        <div class="form-group mb-3 form-hidden" id="sub_group_selected">
                            <label class="form-label">{{ __('Sub Facility') }}</label>
                            <!--begin::Repeater-->
                            <div id="create_sub_facility">
                                <div data-repeater-list="sub_fasilitas_tambahan">
                                    <div data-repeater-item>
                                        @forelse(old('sub_fasilitas_tambahan') ?? [] as $facility)
                                            <div class="form-group row mb-3">
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="facility" value="{{ $facility['name'] }}" placeholder="Enter the additional sub facility" />
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                    </a>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="form-group row mb-3">
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter the additional sub facility" />
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>

                                <div class="form-group mt-3 col-12">
                                    <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                        Add
                                    </a>
                                </div>
                            </div>
                            <!--end::Repeater-->
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-save-quality-port-group-create" type="button" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('modal-scripts')
    <script>
        var maxBobot = 0;
        $(document).ready(function() {
            var selected = `{{ old('fasilitas') }}`;
            if (selected != '') {
                optionGroupType(selected);
            }

            maxBobot = parseInt("{{ $data['sisa_bobot'] }}")
            $('#create_group_modal .max-bobot').text(`${maxBobot} %`);
            $('#create_sub_facility').repeater({
                initEmpty: false,
                show: function() {
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });
        });

        $(document).on('click', '#btn-save-quality-port-group-create', function() {
            let total = $(`#create_group_modal input:checkbox[name="sub_fasilitas[]"]:checked`).length;
            if (total < 1) {
                return Swal.fire({
                    title: "IPM Port Quality",
                    text: "Sub Facility is required",
                    icon: "error"
                });
            }
            $("#form_port_quality_create_group").submit();
        })

        $(document).on('keyup', '#create_group_modal input[name="bobot"]', function() {
            const value = parseInt($(this).val().split(' ')?.[0] || 0);
            var lastBobot = $('#create_group_modal input[name="temp_bobot"]').val();

            maxBobot = maxBobot - (value - lastBobot);
            lastBobot = value;

            $('#create_group_modal .max-bobot').text(`${maxBobot} %`);
            $('#create_group_modal input[name="temp_bobot"]').val(value);
        });

        $('.group_type').change(function() {
            var selected = $(".group_type option:selected").val();
            optionGroupType(selected);

            $('#group_selected').show();
            if (selected == 'Facility') {
                $('#sub_group_selected').show();
            } else {
                $('#sub_group_selected').hide();
            }
        });

        function optionGroupType(groupType) {
            var url = `{{ route('ipm.port.quality.group.type', $data['project']['id']) }}?type=${encodeURIComponent(groupType)}`;

            $.ajax({
                type: 'GET',
                url: url,
                success: function(data) {
                    const options = data?.options;
                    if (options?.length > 0) {
                        var types = '';

                        $.each(options, function(key, value) {
                            if (key % 2 === 0) {
                                types += '<div class="row p-3">';
                            }

                            types += `
                                <div class="col-md-6 col-12 form-check">
                                    <input class="form-check-input" name="sub_fasilitas[]" type="checkbox" value="${value}" />
                                    <label>
                                        ${value}
                                    </label>
                                </div>
                            `;

                            if (key % 2 !== 0 || key === options.length - 1) {
                                types += '</div>';
                            }
                        });
                        $('#sub_group_type').html(types);
                        $('#content_sub_group_type').show();
                    } else {
                        $('#sub_group_type').empty();
                        $('#content_sub_group_type').hide();
                    }

                    $('#create_group_modal .max-bobot').text(`${data?.max_bobot - data?.bobot} %`);
                    $('#create_group_modal input[name="bobot"]').val(`${data?.bobot}`);
                    $('#create_group_modal input[name="temp_bobot"]').val(`${data?.bobot}`);
                }
            });
        }
    </script>
@endpush
