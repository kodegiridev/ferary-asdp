<div class="modal fade" tabindex="-1" id="duplicate_per_code_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Duplikasi Data') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>
            <form class="form">
                <input type="hidden" name="url" value="{{ route('ipm.port.quality.item.duplicate.code', [$data['project']['id'], $data['period']['id']]) }}">
                <input type="hidden" name="type" value="{{ $data['data']['visual_quality']['fasilitas'] }}">
                <input type="hidden" name="jenis_data">
                <input type="hidden" name="id">
                <input type="hidden" name="action" value="duplicate_per_code_modal">
                <div class="modal-body">
                    <span>Yakin ingin menduplikasi data <span class="duplicate_item"></span> ?</span>
                    <br>
                    <br>
                    <span>Seluruh data <span class="duplicate_item"></span> akan terduplikasi pada form ini sebanyak jumlah yang diinputkan.</span>
                    <br>
                    <br>
                    <p><strong class="text-danger">PASTIKAN KEMBALI DATA YANG DIPILIH DAN JUMLAH YANG DIMASUKKAN UNTUK MENGHINDARI DUPLIKASI DATA YANG TIDAK DIINGINKAN !</strong></p>
                    <div class="form-group mt-3">
                        <label class="required form-label">{{ __('Jumlah Duplikasi Data') }}</label>
                        <input type="number" name="qty" class="form-control" placeholder="Masukkan angka" required />
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-warning">{{ __('Duplikasi') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
