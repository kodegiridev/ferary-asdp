<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_group_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="form_port_quality_edit_group" class="form" action="{{ route('ipm.port.quality.group.update', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit_group_modal" name="action">
                <input type="hidden" name="type">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title"></h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group my-3">
                        <label class="form-label">{{ __('Sisa Bobot: ') }}<span class="max-bobot"></span></label>
                        <input type="hidden" name="max_bobot">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Bobot') }}</label>
                        <input type="text" class="form-control @error('bobot') is-invalid @enderror" name="bobot">
                        <input type="hidden" name="temp_bobot">
                        @error('bobot')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 form-hidden" id="edit_sub_group_type">
                        <label class="required form-label">{{ __('Sub Facility') }}</label>
                        <div id="edit_group_type"></div>
                    </div>
                    <div class="form-group mb-3 form-hidden" id="edit_sub_group_selected">
                        <label class="form-label">{{ __('Sub Facility') }}</label>
                        <!--begin::Repeater-->
                        <div id="edit_sub_facility">
                            <div data-repeater-list="sub_fasilitas_tambahan">
                                <div data-repeater-item>
                                    @forelse(old('sub_fasilitas_tambahan') ?? [] as $facility)
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="facility" value="{{ $facility['name'] }}" placeholder="Enter the additional sub facility" />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="name" placeholder="Enter the additional sub facility" />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>

                            <div class="form-group mt-3 col-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                    Add
                                </a>
                            </div>
                        </div>
                        <!--end::Repeater-->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-save-quality-port-group-edit" type="button" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('modal-scripts')
    <script>
        $('#edit_sub_facility').repeater({
            initEmpty: false,
            show: function() {
                $(this).slideDown();
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });

        $(document).on('click', '#btn-save-quality-port-group-edit', function() {
            if ($('#edit_group_modal input[name="type"]').val() != 'SubVisualQuality') {
                let total = $(`#edit_group_modal input:checkbox[name="sub_fasilitas[]"]:checked`).length;
                if (total < 1) {
                    return Swal.fire({
                        title: "IPM Port Quality",
                        text: "Sub Facility is required",
                        icon: "error"
                    });
                }
            }

            $("#form_port_quality_edit_group").submit();
        })

        $(document).on('keyup', '#edit_group_modal input[name="bobot"]', function() {
            const value = parseInt($(this).val().split(' ')?.[0] || 0);
            var maxBobot = $('#edit_group_modal input[name="max_bobot"]').val();
            var lastBobot = $('#edit_group_modal input[name="temp_bobot"]').val();

            maxBobot = maxBobot - (value - lastBobot);
            lastBobot = value;

            $('#edit_group_modal .max-bobot').text(`${maxBobot} %`);
            $('#edit_group_modal input[name="max_bobot"]').val(maxBobot);
            $('#edit_group_modal input[name="temp_bobot"]').val(value);
        });
    </script>
@endpush
