<div class="modal fade drawing" tabindex="-1" data-bs-backdrop="static" id="drawing_period_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="drawing" action="{{ route('ipm.port.quality.drawing.period', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="period_id" value="{{ $data['period']['id'] }}">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Upload Drawing') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label required">{{ __('Document Drawing') }}</label>
                        <input type="file" name="drawing" class="form-control" accept=".jpg,.jpeg,.png,.pdf" required />
                        <span class="text-muted">Accepted file: .pdf, .jpg</span>
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
