<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_periode_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form id="add_period" action="{{ route('ipm.port.quality.period.store', $data['project']['id']) }}" method="POST">
                @csrf
                <input type="hidden" name="id">
                <input type="hidden" name="group">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ganti Periode Selanjutanya') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <p>
                        Yakin ingin melanjutkan ke periode selanjutnya ? <br>
                        Data yang ada di periode ini akan terduplikasi ke periode selanjutnya
                    </p>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
