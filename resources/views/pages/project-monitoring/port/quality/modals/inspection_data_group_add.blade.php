<div class="modal fade" tabindex="-1" backdrop="static" id="inspection_data_group_add_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.port.quality.inspection.store', [$data['project']['id'], $data['sub_visual_quality']['id']]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="sub_visual_quality_port_id" value="{{ $data['sub_visual_quality']['id'] }}">
                <input type="hidden" name="visual_quality_port_id" value="{{ $data['sub_visual_quality']['visual_quality']['id'] }}">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Periode Ke') }}</label>
                        <input type="number" name="periode" class="form-control @error('periode') is-invalid @enderror" value="{{ old('periode') }}" placeholder="Periode Ke" required />
                        @error('periode')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Invitation') }}</label>
                        <input type="number" name="jml_undangan" class="form-control @error('jml_undangan') is-invalid @enderror" value="{{ old('jml_undangan') }}" placeholder="Jumlah Invitation" required />
                        @error('jml_undangan')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Re-Inspect') }}</label>
                                <input type="number" name="reinspect" class="form-control @error('reinspect') is-invalid @enderror" value="{{ old('reinspect') }}" placeholder="Jumlah Re-Inspect" required />
                                @error('reinspect')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc by Class') }}</label>
                                <input type="number" name="acc_konsultan" class="form-control @error('acc_konsultan') is-invalid @enderror" value="{{ old('acc_konsultan') }}" placeholder="Jumlah Acc by Class" required />
                                @error('acc_konsultan')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc by Owner') }}</label>
                                <input type="number" name="acc_owner" class="form-control @error('acc_owner') is-invalid @enderror" value="{{ old('acc_owner') }}" placeholder="Jumlah Re-Inspect" required />
                                @error('acc_owner')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc by Both') }}</label>
                                <input type="number" name="acc_both" class="form-control @error('acc_both') is-invalid @enderror" value="{{ old('acc_both') }}" placeholder="Jumlah Acc by Class" required />
                                @error('acc_both')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="form-label">{{ __('Evidence') }}</label>
                        <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf" />
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
