<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_item_facility_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="url" value="{{ route('ipm.port.quality.item.update', [$data['project']['id'], $data['period']['id']]) }}">
                <input type="hidden" name="id">
                <input type="hidden" name="type">
                <input type="hidden" name="jenis_data">
                <input type="hidden" name="action" value="edit_item_facility_modal">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Code') }}</label>
                        <input type="text" class="form-control @error('kode') is-invalid @enderror" name="kode" required>
                        @error('kode')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Sub Facility') }}</label>
                        <input type="text" class="form-control @error('sub_fasilitas') is-invalid @enderror" name="sub_fasilitas" required>
                        @error('sub_fasilitas')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Area') }}</label>
                        <input type="text" class="form-control @error('area') is-invalid @enderror" name="area" required>
                        @error('area')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Evidence') }}</label>
                        <div class="row">
                            <div class="col-md-10 col-10">
                                <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf">
                            </div>
                            <div class="col-md-2 col-2">
                                <a class="btn btn-sm btn-icon btn-light-primary mt-2" target="_blank" id="evidence_item_facility" style="float: right;">
                                    {!! theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') !!}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required form-label">{{ __('Remarks') }}</label>

                        <!--begin::Repeater-->
                        <div id="edit_item_facility_remark">
                            <div data-repeater-list="remarks">
                                <div data-repeater-item>
                                    <div class="form-group my-5">
                                        <input type="hidden" name="remark_id" value="">
                                        <div class="row mb-3">
                                            <div class="col-10">
                                                <span class="form-label">Code Component: NEW CODE COMPONENT</span>
                                            </div>
                                            <div class="col-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger" style="float: right;">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6 col-12">
                                                <input name="remarks" type="text" class="form-control" placeholder="Ketikan remark" value="">
                                            </div>
                                            <div class="col-md-6 col-12 text-right">
                                                <input name="tanggal" type="date" class="form-control" placeholder="Pilih tanggal inspeksi" value="">
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-12">
                                                <textarea rows="3" name="catatan_inspeksi" class="form-control" placeholder="Ketikan catatan inspeksi"></textarea>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6 col-12">
                                                <select name="status_plan" class="form-select form-select-solid">
                                                    @foreach ($data['status'] as $key => $status)
                                                        <option value="{{ $key }}">{{ $status }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <select name="status_inspeksi" class="form-select form-select-solid">
                                                    @foreach ($data['status'] as $key => $status)
                                                        <option value="{{ $key }}">{{ $status }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mt-3 col-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                    Tambah New Code Component
                                </a>
                            </div>
                        </div>
                        <!--end::Repeater-->
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
