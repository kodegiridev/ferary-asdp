@extends('pages.project-monitoring.port.detail_quality')

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .form-check-input {
            border: 1.75px solid var(--bs-gray-700) !important;
        }

        .form-hidden {
            display: none;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
    <div class="row d-flex justify-content-start mb-5">
        <div class="col-md-6 col-12">
            <div class="table-responsive" style="width: 100%">
                <table class="table">
                    <tbody>
                        <tr>
                            <td width="25%">Nama Project</td>
                            <td>: {{ $data['project']['nama_project'] }}</td>
                        </tr>
                        <tr>
                            <td>Nilai Project</td>
                            <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai</td>
                            <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
                        </tr>
                        <tr>
                            <td>Total Quality Plan</td>
                            <td>: {{ toPercent($data['cumulative']['total_plan'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>Total Quality Rate</td>
                            <td>: {{ toPercent($data['cumulative']['total_actual'], 2) }}</td>
                        </tr>
                        <tr>
                            <td>Total Deviasi</td>
                            <td>: {{ toPercent($data['cumulative']['total_deviasi'], 2) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div style="width: 100%">
                <button type="button" class="btn btn-sm btn-primary m-1" id="drawing">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                    {{ __('Tambah Drawing') }}
                </button>
                <br>
                @if (!empty($data['drawing']['drawing']))
                    <img src="{{ $data['drawing']['drawing'] }}" class="lozad rounded w-100 mh-400px" alt="{{ $data['drawing']['deskripsi'] }}" />
                @else
                    <canvas id="canvas" style="width:100%; max-height:400px; border: 2px solid black"></canvas>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <ul class="nav nav-tabs mt-2" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="visual-quality-tab" data-bs-toggle="tab" data-bs-target="#visual-quality" type="button" role="tab" aria-controls="visual-quality" aria-selected="true">Visual Quality</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="inspection-acceptance-tab" data-bs-toggle="tab" data-bs-target="#inspection-acceptance" type="button" role="tab" aria-controls="inspection-acceptance" aria-selected="false">Inspection Acceptance Rate</button>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="visual-quality" role="tabpanel" aria-labelledby="visual-quality-tab">
            <div class="mt-3 mb-3">
                <span class="text-primary"><strong>VISUAL QUALITY PROGRESS (PROGRESS: 0)</strong></span>
            </div>
            <div class="row">
                <div class="col-md-6 col-7">
                    <button type="button" class="btn btn-sm btn-primary btn_create_group_modal" data-bs-toggle="modal" data-bs-target="#create_group_modal">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                        {{ __('Tambah Group') }}
                    </button>
                    <a href="{{ route('ipm.port.quality.print', $data['project']['id']) }}" class="btn btn-sm btn-success btn-icon btn-print">
                        {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
                    </a>
                </div>
                <div class="col-md-6 col-5">
                    <div class="cursor-pointer" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" style="float: right;">
                        <button class="btn btn-sm btn-primary font-weight-bold">
                            Periode
                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr072.svg', 'svg-icon-1') !!}
                        </button>
                    </div>
                    @if (!empty($data['periods']))
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-125px" data-kt-menu="true">
                            @foreach ($data['periods'] as $period)
                                <div class="menu-item px-5">
                                    <a class="menu-link px-5 btn-period @if ($period == $data['last_period']) active @endif" data-id="{{ $period }}">
                                        {{ $period }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="table-responsive my-5">
                <table class="table table-hover table-bordered">
                    <thead class="text-center align-middle bg-primary text-white">
                        <tr>
                            <th rowspan="2">{{ __('No') }}</th>
                            <th rowspan="2">{{ __('Group') }}</th>
                            <th rowspan="2">{{ __('Last Update') }}</th>
                            <th rowspan="2">{{ __('Bobot') }}</th>
                            <th colspan="3">{{ __('Quality Progress') }}</th>
                            <th rowspan="2">{{ __('Status Progress') }}</th>
                            <th rowspan="2">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th>{{ __('Plan') }}</th>
                            <th>{{ __('Actual') }}</th>
                            <th>{{ __('Deviasi') }}</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle" id="group_table"></tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="inspection-acceptance" role="tabpanel" aria-labelledby="inspection-acceptance-tab">
            <div class="table-responsive my-5">
                <table class="table table-bordered table-hover">
                    <thead class="text-center align-middle bg-primary text-white">
                        <tr>
                            <th rowspan="2">{{ __('No') }}</th>
                            <th rowspan="2">{{ __('Group') }}</th>
                            <th colspan="3">{{ __('Accepted By') }}</th>
                            <th rowspan="2">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th>{{ __('Consultant') }}</th>
                            <th>{{ __('Owner') }}</th>
                            <th>{{ __('Both') }}</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle" id="inspection_table"></tbody>
                </table>
            </div>
        </div>
    </div>

    @include('pages.project-monitoring.port.quality.modals.data_group_add')
    @include('pages.project-monitoring.port.quality.modals.data_group_edit')
    @include('pages.project-monitoring.port.quality.modals.periode_per_group')
    @include('pages.project-monitoring.port.quality.modals.periode_warning')
    @include('pages.project-monitoring.port.quality.modals.drawing_add')
    @include('pages.project-monitoring.port.quality.modals.drawing_edit')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.index') }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            Inputmask({
                rightAlign: false,
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                max: 100,
                suffix: " %"
            }).mask('input[name="bobot"]');

            dataVisualQuality(`{{ $data['last_period'] }}`);
            dataInspection(`{{ $data['last_period'] }}`);

            var printUrl = `{{ route('ipm.port.quality.print', $data['project']['id']) }}?periode=:periode`;
            printUrl = printUrl.replace(':periode', "{{ $data['last_period'] }}");
            $('.btn-print').prop('href', printUrl)
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.port.quality.group.delete', $data['project']['id']) }}`,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "type": $(this).data('type'),
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-detail', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "{{ route('ipm.port.quality.period.index', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function(response) {
                    dataPeriod(id);
                    $('#periode_per_group_modal').modal('show');
                    $('#create_periode_modal input[name="id"]').val(id);
                    $('#create_periode_modal input[name="group"]').val(response?.data?.visual_quality?.fasilitas);
                    $('#periode_per_group_modal .modal-title').text(`Periode Data: ${response?.data?.sub_fasilitas}`);
                }
            });
        });

        $(document).on('click', '#drawing', function() {
            dataDrawing();
            $('#add_document_drawing_modal').modal('show');
        });

        $(document).on('click', '.btn-period', function() {
            const period = $(this).data('id');
            $('.btn-period').removeClass('active');
            $(this).addClass('active');

            var printUrl = `{{ route('ipm.port.quality.print', $data['project']['id']) }}?periode=:periode`;
            printUrl = printUrl.replace(':periode', period);
            $('.btn-print').prop('href', printUrl)

            dataVisualQuality(period);
            dataInspection(period);
        });

        $(document).on('click', '.btn-edit', function() {
            var type = $(this).data('type');
            $.ajax({
                url: `{{ route('ipm.port.quality.group.detail', $data['project']['id']) }}`,
                type: 'GET',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "type": type,
                    "id": $(this).data('id')
                },
                success: function(result) {
                    $('#edit_group_modal input[name="max_bobot"]').val(result?.sisa_bobot);
                    $('#edit_group_modal input[name="id"]').val(result?.data?.id);
                    $('#edit_group_modal .max-bobot').text(`${result?.sisa_bobot} %`);
                    $('#edit_group_modal input[name="bobot"]').val(result?.data?.bobot);

                    if (type == 'VisualQuality') {
                        var types = '';
                        $.each(result?.options, function(key, value) {
                            if (key % 2 === 0) {
                                types += '<div class="row p-3">';
                            }

                            types += `
                                <div class="col-md-6 col-12 form-check">
                                    <input class="form-check-input" name="sub_fasilitas[]" type="checkbox" value="${value}" />
                                    <label>
                                        ${value}
                                    </label>
                                </div>
                            `;

                            if (key % 2 !== 0 || key === result?.options?.length - 1) {
                                types += '</div>';
                            }
                        });

                        $('#edit_group_type').html(types);
                        $('#edit_sub_group_type').show();
                        $('#edit_group_modal input[name="type"]').val('VisualQuality');
                        $('#edit_group_modal input[name="temp_bobot"]').val(result?.data?.bobot);
                        $('#edit_group_modal .modal-title').text(`Ubah Group: ${result?.data?.fasilitas}`);

                        if (result?.data?.fasilitas == 'Facility') {
                            $('#edit_sub_group_selected').show();
                        } else {
                            $('#edit_sub_group_selected').hide();
                        }
                    } else {
                        $('#edit_sub_group_type').hide();
                        $('#edit_sub_group_selected').hide();
                        $('#edit_group_modal input[name="type"]').val('SubVisualQuality');
                        $('#edit_group_modal .modal-title').text(`Ubah Data ${result?.data?.visual_quality?.fasilitas}: ${result?.data?.sub_fasilitas}`);
                    }

                    $('#edit_group_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        title: "Something went wrong!",
                        text: err?.responseJSON?.message,
                        icon: "error"
                    })
                }
            });
        });

        function dataVisualQuality(period) {
            $.ajax({
                url: `{{ route('ipm.port.quality.index', $data['project']['id']) }}?type=${encodeURIComponent('VisualQuality')}&periode=${encodeURIComponent(period)}`,
                type: `GET`,
                success: function(result) {
                    $('#group_table').empty();

                    result?.data?.forEach((facility, idx) => {
                        var html = '';
                        html += `
                            <tr class="fw-bold">
                                <td class="text-center">${idx + 1}</td>
                                <td>${facility?.fasilitas}</td>
                                <td></td>
                                <td class="text-center">${facility?.bobot}%</td>
                                <td class="text-center">${Math.round(facility?.total_plan, 2)}%</td>
                                <td class="text-center">${Math.round(facility?.total_actual, 2)}%</td>
                                <td class="text-center">${Math.round(facility?.total_deviasi, 2)}%</td>
                                <td></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-icon btn-success btn-edit" type="button" data-type="VisualQuality" data-id="${facility?.id}">
                                        {!! theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') !!}
                                    </a>
                                    <a class="btn btn-sm btn-icon btn-danger btn-delete" type="button" data-type="VisualQuality" data-id="${facility?.id}">
                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                    </a>
                                </td>
                            </tr>
                        `;

                        facility?.sub_visual_qualities?.forEach((sub, key) => {
                            html += `
                                <tr class="text-center">
                                    <td>${idx + 1}.${key + 1}</td>
                                    <td >${sub?.sub_fasilitas}</td>
                                    <td>${sub?.updated_at ? moment(sub?.updated_at).format("YYYY-MM-DD HH:mm:ss") : ''}</td>
                                    <td>${sub?.bobot}%</td>
                                    <td>${Math.round(sub?.plan, 2)}%</td>
                                    <td>${Math.round(sub?.actual, 2)}%</td>
                                    <td>${Math.round(sub?.deviasi, 2)}%</td>
                                    <td>${sub?.status_progress}</td>
                                    <td>
                                        <a class="btn btn-sm btn-icon btn-primary btn-detail" type="button" data-id="${sub?.id}">
                                            {!! theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') !!}
                                        </a>
                                        <a class="btn btn-sm btn-icon btn-success btn-edit" type="button" data-type="SubVisualQuality" data-id="${sub?.id}">
                                            {!! theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') !!}
                                        </a>
                                        <a class="btn btn-sm btn-icon btn-danger btn-delete" type="button" data-type="SubVisualQuality" data-id="${sub?.id}">
                                            {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                        </a>
                                    </td>
                                </tr>
                            `;
                        });

                        $('#group_table').append(html);
                    });
                }
            });
        }

        function dataInspection(period) {
            $.ajax({
                url: `{{ route('ipm.port.quality.index', $data['project']['id']) }}?type=${encodeURIComponent('Inspection')}&periode=${encodeURIComponent(period)}`,
                type: `GET`,
                success: function(result) {
                    $('#inspection_table').empty();

                    result?.data?.forEach((facility, idx) => {
                        var html = '';
                        html += `
                            <tr class="fw-bold">
                                <td class="text-center">${idx + 1}</td>
                                <td>${facility?.fasilitas}</td>
                                <td colspan="3"></td>
                                <td></td>
                            </tr>
                        `;

                        facility?.sub_visual_qualities?.forEach((sub, key) => {
                            var inspection = sub?.inspection_acceptance_rates?.[0];
                            var url = "{{ route('ipm.port.quality.inspection.index', [$data['project']['id'], ':sub_id']) }}";
                            url = url.replace(":sub_id", sub?.id);

                            html += `
                                <tr class="text-center">
                                    <td>${idx + 1}.${key + 1}</td>
                                    <td >${sub?.sub_fasilitas}</td>
                                    <td>${Math.round(inspection?.consultant_rate || 0)}%</td>
                                    <td>${Math.round(inspection?.owner_rate || 0)}%</td>
                                    <td>${Math.round(inspection?.both_rate || 0)}%</td>
                                    <td>
                                        <a href="${url}" class="btn btn-sm btn-icon btn-primary">
                                            {!! theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') !!}
                                        </a>
                                    </td>
                                </tr>
                            `;
                        });

                        $('#inspection_table').append(html);
                    });
                }
            });
        }
    </script>

    @stack('modal-scripts')
@endpush
