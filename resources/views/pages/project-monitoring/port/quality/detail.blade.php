@extends('pages.project-monitoring.port.detail_quality')

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .form-check-input {
            border: 1.75px solid var(--bs-gray-700) !important;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
    <div class="col-md-6 col-12 d-flex justify-content-star">
        <div class="table-responsive">
            <table class="table">
                <tbody class="align-middle">
                    <tr>
                        <td>Nama Project</td>
                        <td>: {{ $data['project']['nama_project'] }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                    </tr>
                    <tr>
                        <td>Last Update</td>
                        <td>: {{ $data['basic_data']['updated_at'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Outstanding</td>
                        <td>
                            <select class="form-select select2-outstanding" name="outstanding">
                                @foreach ($data['outstandings'] as $label => $value)
                                    <option value="{{ $value }}" @if ($data['period']['outstanding'] == $value)  @endif>{{ ucfirst($label) }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Quality Plan</td>
                        <td>: {{ toPercent($data['quality_inspection']['summary']['status']['plan']['percentage'], 2) }}</td>
                    </tr>
                    <tr>
                        <td>Quality Actual</td>
                        <td>: {{ toPercent($data['quality_inspection']['summary']['status']['actual']['percentage'], 2) }}</td>
                    </tr>
                    <tr>
                        <td>Deviasi</td>
                        <td>: {{ toPercent($data['quality_inspection']['summary']['status']['deviation']['percentage'], 2) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-8 col-12">
            @if (!empty($data['period']['document_drawing']))
                <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#drawing_period_modal">
                    {!! theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-1') !!}
                    {{ __('Edit Drawing') }}
                </button>
                @if (str_ends_with($data['period']['document_drawing'], '.pdf'))
                    <embed src="{{ asset('upload/ipm/port/quality/drawing/period/' . $data['period']['document_drawing']) }}" type="application/pdf" width="100%" height="600px" />
                @else
                    <div class="h-400px">
                        <img class="rounded mw-100 mh-400px" src="{{ asset('upload/ipm/port/quality/drawing/period/' . $data['period']['document_drawing']) }}" alt="{{ $data['project']['nama_project'] }}">
                    </div>
                @endif
            @else
                <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#drawing_period_modal">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                    {{ __('Tambah Drawing') }}
                </button>
                <br>
                <canvas style="width:100%; max-height:400px; border: 2px solid black"></canvas>
            @endif

        </div>
        <div class="col-md-4 col-12">
            <span class="text-primary"><strong>QUALITY INSPECTION PROGRESS</strong></span>
            <div class="table-responsive mt-7">
                <table class="table table-bordered">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                            <th colspan="3" class="text-center align-middle" style="color:white !important">{{ __('Inspection') }}</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Status') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Qty') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('%') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['quality_inspection']['data'] as $item)
                            <tr class="text-center align-middle">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ strtoupper($item['status']) }}</td>
                                <td class="{{ $item['qty_color'] }}">{{ $item['qty'] }}</td>
                                <td>{{ $item['percentage'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr class="text-center align-middle">
                            <td colspan="2" class="fw-bold"><strong>TOTAL</strong></td>
                            <td>{{ $data['quality_inspection']['summary']['qty'] }}</td>
                            <td>{{ $data['quality_inspection']['summary']['percentage'] }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-content')
    <span class="text-primary"><strong>VISUAL QUALITY PROGRESS {{ strtoupper($data['data']['visual_quality']['fasilitas']) }}: {{ strtoupper($data['data']['sub_fasilitas']) }}</strong></span>
    <div class="buttons">
        @if ($data['data']['visual_quality']['fasilitas'] != 'Facility')
            <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#add_item_modal">
                {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                {{ __('Tambah Item') }}
            </button>

            <button type="button" class="btn btn-sm btn-warning m-1 btn-icon" data-bs-toggle="modal" data-bs-target="#duplicate_per_section_modal">
                {!! theme()->getSvgIcon('icons/duotune/general/gen054.svg', 'svg-icon-1') !!}
            </button>
        @else
            <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#add_item_facility_modal">
                {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                {{ __('Tambah Item') }}
            </button>
        @endif
        <a href="{{ route('ipm.port.quality.item.print', [$data['project']['id'], $data['period']['id']]) }}" class="btn btn-sm btn-success m-1 btn-icon">
            {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
        </a>
    </div>

    @if ($data['data']['visual_quality']['fasilitas'] != 'Facility')
        <ul class="nav nav-tabs mt-2" role="tablist">
            @foreach ($data['inspection_types'] as $key => $type)
                <li class="nav-item" role="presentation">
                    <button class="nav-link inspection-menu @if ($loop->iteration == 1) active @endif" data-id="{{ $key }}" data-label="{{ $type }}">{{ __($type) }}</button>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" role="tabpanel">
                <div class="table-responsive mt-2 mb-5">
                    <table class="table table-bordered" id="item_table">
                        <thead class="bg-primary text-white">
                            <tr class="text-center align-middle">
                                <th style="color:white !important">{{ __('No') }}</th>
                                <th style="color:white !important">{{ __('Code') }}</th>
                                <th style="color:white !important">{{ __('Item') }}</th>
                                <th style="color:white !important" class="w-75px">{{ __('Remarks') }}</th>
                                <th style="color:white !important">{{ __('Evidence') }}</th>
                                <th style="color:white !important" class="w-150px">{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="tab-content">
            <div class="tab-pane fade show active" role="tabpanel">
                <div class="table-responsive mt-2 mb-5">
                    <table class="table table-bordered" id="item_table">
                        <thead class="bg-primary text-white">
                            <tr class="text-center align-middle">
                                <th style="color:white !important">{{ __('No') }}</th>
                                <th style="color:white !important">{{ __('Code') }}</th>
                                <th style="color:white !important">{{ __('Sub Facility') }}</th>
                                <th style="color:white !important">{{ __('Area') }}</th>
                                <th style="color:white !important" class="w-75px">{{ __('Remarks') }}</th>
                                <th style="color:white !important">{{ __('Evidence') }}</th>
                                <th style="color:white !important" class="w-150px">{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    @include('pages.project-monitoring.port.quality.modals.drawing_period')
    @include('pages.project-monitoring.port.quality.modals.item_add')
    @include('pages.project-monitoring.port.quality.modals.item_edit')
    @include('pages.project-monitoring.port.quality.modals.item_facility_add')
    @include('pages.project-monitoring.port.quality.modals.item_facility_edit')
    @include('pages.project-monitoring.port.quality.modals.remarks_detail')
    @include('pages.project-monitoring.port.quality.modals.duplicate_data_per_section')
    @include('pages.project-monitoring.port.quality.modals.duplicate_data_per_code')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.quality.index', $data['project']['id']) }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            datatable(`{{ $data['first_inspection'] }}`);
            optionMilestone(`{{ $data['first_inspection'] }}`);
            $('.select2-outstanding').select2();

            $('input[name="jenis_data"]').val(`{{ $data['first_inspection'] }}`)
            $('.duplicate_item').text(`{{ $data['first_label'] }}`);

            $('#edit_item_remark').repeater({
                initEmpty: true,
                show: function() {
                    $(this).slideDown();
                    checkAndHideButtonOnEdit(8)
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                    checkAndHideButtonOnEdit(9)
                }
            });

            $('#edit_item_facility_remark').repeater({
                initEmpty: true,
                show: function() {
                    $(this).slideDown()
                    checkAndHideFacilityButtonOnEdit(8)
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement)
                    checkAndHideFacilityButtonOnEdit(9)
                },
            });
        });

        $(document).on('click', '.inspection-menu', function() {
            const selectedData = $(this).data('id');

            $('.inspection-menu').removeClass('active');
            $('input[name="jenis_data"]').val(selectedData)
            $('.duplicate_item').text($(this).data('label'));
            $(this).addClass('active');
            datatable(selectedData)
            optionMilestone(selectedData);
        });

        $(document).on('click', '.btn-remark', function() {
            $.ajax({
                url: "{{ route('ipm.port.quality.remark.index', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id'),
                    "type": $(this).data('type')
                },
                success: function(response) {
                    var remarks = '';
                    var status = response?.status;
                    $.each(response?.data?.remarks, function(key, value) {
                        remarks += `
                            <div class="row mb-5">
                                <span class="fw-bold">Code Component: ${value?.remarks ?? ''}</span>
                            </div>
                            <div class="table-responsive mb-5">
                                <table class="table table-bordered">
                                    <thead class="bg-primary text-white">
                                        <tr class="text-center align-middle">
                                            <th>{{ __('Plan Status') }}</th>
                                            <th>{{ __('Inspection Status') }}</th>
                                            <th>{{ __('Inspection Result') }}</th>
                                            <th>{{ __('Date') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center align-middle">
                                            <td>${status?.[value?.status_inspeksi] ?? status?.[0]}</td>
                                            <td>${status?.[value?.status_plan] ?? status?.[0]}</td>
                                            <td>${value?.catatan_inspeksi ?? ''}</td>
                                            <td>${value?.tanggal ? moment(value?.tanggal).format("YYYY-MM-DD") : ''}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        `;
                    });

                    $('#list-remarks').html(remarks);
                    $('#remarks_detail_modal .modal-title').text(`Remarks ${response?.data?.item}`);
                    $('#remarks_detail_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    })
                }
            });
        });

        $(document).on('click', '.btn-remark-delete', function() {
            var jenisData = $(this).data('jenis');
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.port.quality.remark.delete', $data['project']['id']) }}`,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "type": $(this).data('type'),
                            "id": $(this).data('id')
                        },
                        success: function(response) {
                            Swal.fire({
                                text: response?.message,
                                icon: "success"
                            });

                            datatable(jenisData);
                        },
                        error: function(error) {
                            Swal.fire({
                                title: error?.responseJSON?.message,
                                text: error?.responseJSON?.error,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-remark-edit', function() {
            $.ajax({
                url: "{{ route('ipm.port.quality.item.detail', [$data['project']['id'], $data['period']['id']]) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id'),
                    "type": $(this).data('type')
                },
                success: function(response) {
                    $('#edit_item_remark [data-repeater-list="remarks"]').empty();

                    $.each(response?.data?.remarks, function(key, val) {
                        var remarks = '';
                        var statusInspeksi = '';
                        response?.status?.forEach((status, idx) => {
                            statusInspeksi += `<option value="${idx}" ${idx == val?.status_inspeksi ? 'selected' : ''}>${status}</option>`;
                        });

                        var statusPlan = '';
                        response?.status?.forEach((status, idx) => {
                            statusPlan += `<option value="${idx}" ${idx == val?.status_plan ? 'selected' : ''}>${status}</option>`;
                        });

                        remarks += `
                            <div class="form-group my-5">
                                <input type="hidden" name="remarks[${key}][remark_id]" value="${val?.id}">
                                <div class="row mb-3">
                                    <div class="col-10">
                                        <span class="form-label">Code Component: ${val?.remarks ?? ''}</span>
                                    </div>
                                    <div class="col-2">
                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger" style="float: right;">
                                            {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                        </a>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12">
                                        <input name="remarks[${key}][remarks]" type="text" class="form-control form-control-solid" placeholder="Ketikan remark" value="${val?.remarks ?? ''}">
                                    </div>
                                    <div class="col-md-6 col-12 text-right">
                                        <input name="remarks[${key}][tanggal]" type="date" class="form-control form-control-solid" placeholder="Pilih tanggal inspeksi" value="${val?.tanggal}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <textarea rows="3" name="remarks[${key}][catatan_inspeksi]" class="form-control" placeholder="Ketikan catatan inspeksi">${val?.catatan_inspeksi ?? ''}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12">
                                        <select name="remarks[${key}][status_plan]" class="form-select">
                                            ${statusPlan}
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <select name="remarks[${key}][status_inspeksi]" class="form-select">
                                            ${statusInspeksi}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        `;

                        if (key == 0) {
                            var repeaterItem = $('<div data-repeater-item></div>').append(remarks);
                        } else {
                            var repeaterItem = $('<div data-repeater-item style></div>').append(remarks);
                        }

                        $('#edit_item_remark [data-repeater-list="remarks"]').append(repeaterItem);
                    });

                    $('#edit_item_modal .modal-title').text(`Edit Detail ${response?.data?.period?.sub_visual_quality?.sub_fasilitas}`);
                    $('#edit_item_modal input[name="id"]').val(response?.data?.id);
                    $('#edit_item_modal input[name="type"]').val(response?.data?.sub_grup);
                    $('#edit_item_modal input[name="kode"]').val(response?.data?.kode);
                    $('#edit_item_modal input[name="item"]').val(response?.data?.item);
                    $('#edit_item_modal input[name="jenis_data"]').val(response?.data?.jenis_data);
                    $('#evidence_item').attr('href', response?.data?.evidence);
                    $('#edit_item_modal').modal('show');
                }
            })
        });

        $(document).on('click', '.btn-facility-edit', function() {
            $.ajax({
                url: "{{ route('ipm.port.quality.item.detail', [$data['project']['id'], $data['period']['id']]) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id'),
                    "type": $(this).data('type')
                },
                success: function(response) {
                    $('#edit_item_facility_remark [data-repeater-list="remarks"]').empty();

                    $.each(response?.data?.remarks, function(key, val) {
                        var remarks = '';
                        var statusInspeksi = '';
                        response?.status?.forEach((status, idx) => {
                            statusInspeksi += `<option value="${idx}" ${idx == val?.status_inspeksi ? 'selected' : ''}>${status}</option>`;
                        });

                        var statusPlan = '';
                        response?.status?.forEach((status, idx) => {
                            statusPlan += `<option value="${idx}" ${idx == val?.status_plan ? 'selected' : ''}>${status}</option>`;
                        });

                        remarks += `
                            <div class="form-group my-5">
                                <input type="hidden" name="remarks[${key}][remark_id]" value="${val?.id}">
                                <div class="row mb-3">
                                    <div class="col-10">
                                        <span class="form-label">Code Component: ${val?.remarks ?? ''}</span>
                                    </div>
                                    <div class="col-2">
                                        <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger" style="float: right;">
                                            {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                        </a>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12">
                                        <input name="remarks[${key}][remarks]" type="text" class="form-control" placeholder="Ketikan remark" value="${val?.remarks ?? ''}">
                                    </div>
                                    <div class="col-md-6 col-12 text-right">
                                        <input name="remarks[${key}][tanggal]" type="date" class="form-control" placeholder="Pilih tanggal inspeksi" value="${val?.tanggal ?? ''}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <textarea rows="3" name="remarks[${key}][catatan_inspeksi]" class="form-control" placeholder="Ketikan catatan inspeksi">${val?.catatan_inspeksi ?? ''}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12">
                                        <select name="remarks[${key}][status_plan]" class="form-select">
                                            ${statusPlan}
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <select name="remarks[${key}][status_inspeksi]" class="form-select">
                                            ${statusInspeksi}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        `;

                        if (key == 0) {
                            var repeaterItem = $('<div data-repeater-item></div>').append(remarks);
                        } else {
                            var repeaterItem = $('<div data-repeater-item style></div>').append(remarks);
                        }

                        $('#edit_item_facility_remark [data-repeater-list="remarks"]').append(repeaterItem);
                    });

                    $('#edit_item_facility_modal .modal-title').text(`Edit Detail ${response?.data?.period?.sub_visual_quality?.sub_fasilitas}`);
                    $('#edit_item_facility_modal input[name="id"]').val(response?.data?.id);
                    $('#edit_item_facility_modal input[name="type"]').val(response?.data?.sub_grup);
                    $('#edit_item_facility_modal input[name="kode"]').val(response?.data?.kode);
                    $('#edit_item_facility_modal input[name="sub_fasilitas"]').val(response?.data?.sub_fasilitas);
                    $('#edit_item_facility_modal input[name="area"]').val(response?.data?.area);
                    $('#edit_item_facility_modal input[name="jenis_data"]').val(response?.data?.jenis_data);
                    $('#evidence_item_facility').attr('href', response?.data?.evidence);
                    $('#edit_item_facility_modal').modal('show');
                }
            })
        });

        $(document).on('click', '.btn-remark-duplicate', function() {
            $('#duplicate_per_code_modal input[name="id"]').val($(this).data('id'));
            $('#duplicate_per_code_modal').modal('show');
        });

        $(document).on('submit', '.form', function(e) {
            e.preventDefault();

            var modal = $(this).find('input[name="action"]').val()
            var jenisData = $(this).find('input[name="jenis_data"]').val()

            $.ajax({
                url: $(this).find('input[name="url"]').val(),
                type: 'POST',
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    $(`#${modal}`).find('form')[0].reset();
                    $(`#${modal}`).modal('hide');

                    Swal.fire({
                        text: response?.message,
                        icon: "success"
                    });

                    datatable(jenisData);
                },
                error: function(error) {
                    $(`#${modal}`).find('form')[0].reset();
                    $(`#${modal}`).modal('hide');

                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        $(document).on('change', '.select2-outstanding', function() {
            $.ajax({
                url: "{{ route('ipm.port.quality.period.update', $data['project']['id']) }}",
                type: 'POST',
                data: {
                    period_id: "{{ $data['period']['id'] }}",
                    outstanding: $(this).val(),
                },
                success: function(response) {
                    Swal.fire({
                        text: response?.message,
                        icon: "success"
                    });

                },
                error: function(error) {
                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        function datatable(selectedData) {
            var columns = {};
            if (selectedData != 0) {
                columns = [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'kode',
                        name: 'kode',
                    },
                    {
                        data: 'item',
                        name: 'item'
                    },
                    {
                        data: 'remarks',
                        name: 'remarks',
                        searchable: false
                    },
                    {
                        data: 'evidence',
                        name: 'evidence',
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            } else {
                columns = [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'kode',
                        name: 'kode'
                    },
                    {
                        data: 'sub_fasilitas',
                        name: 'sub_fasilitas'
                    },
                    {
                        data: 'area',
                        name: 'area'
                    },
                    {
                        data: 'remarks',
                        name: 'remarks'
                    },
                    {
                        data: 'evidence',
                        name: 'evidence'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            }

            $('#item_table').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.quality.item.index', [$data['project']['id'], $data['period']['id']]) }}",
                    type: "GET",
                    data: function(d) {
                        d.jenis_data = selectedData
                    }
                },
                columns,
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }

        function optionMilestone(selectedData) {
            var options = JSON.parse(`{!! json_encode($data['inspection_types']) !!}`);
            var milestones = '';
            var idx = 0;
            $('#milestone_tujuan').empty();
            for (const [key, value] of Object.entries(options)) {
                if (key != selectedData) {
                    if (idx % 2 === 0) {
                        milestones += '<div class="row p-3">';
                    }

                    milestones += `
                        <div class="col-md-6 col-12 form-check">
                            <input class="form-check-input" name="milestones[]" type="checkbox" value="${key}" />
                            <label>
                                ${value}
                            </label>
                        </div>
                    `;

                    if (idx % 2 !== 0 || idx === options.length - 1) {
                        milestones += '</div>';
                    }

                    idx++;
                }
            }

            $('#milestone_tujuan').append(milestones);
        }

        function checkAndHideButtonOnEdit(max) {
            var itemElements = $('#edit_item_remark [data-repeater-item]');
            var addButton = $('#edit_item_remark [data-repeater-create]');

            if (itemElements.length >= max) {
                addButton.hide();
            } else {
                addButton.show();
            }
        }

        function checkAndHideFacilityButtonOnEdit(max) {
            var itemElements = $('#edit_item_facility_remark [data-repeater-item]');
            var addButton = $('#edit_item_facility_remark [data-repeater-create]');

            if (itemElements.length >= max) {
                addButton.hide();
            } else {
                addButton.show();
            }
        }
    </script>

    @stack('modal-scripts')
@endpush
