@extends('pages.project-monitoring.port.detail_quality')

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.port.index') }}" class="text-muted text-hover-primary">{{ __('Port (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
    <div class="col-md-6 col-12 d-flex justify-content-star">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Nama Project</td>
                        <td>: {{ $data['project']['nama_project'] }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                    </tr>
                    <tr>
                        <td>Quality Plan</td>
                        <td><b>: {{ toPercent($data['cumulative']['total_actual'], 2) }}</b></td>
                    </tr>
                    <tr>
                        <td>Quality Rate</td>
                        <td><b>: {{ toPercent($data['cumulative']['total_plan'], 2) }}</b></td>
                    </tr>
                    <tr>
                        <td>Deviasi</td>
                        <td><b>: {{ toPercent($data['cumulative']['total_deviasi'], 2) }}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('page-content')
    <span class="text-primary"><strong>ACCEPTANCE RATE {{ strtoupper($data['sub_visual_quality']['visual_quality']['fasilitas']) }}: {{ strtoupper($data['sub_visual_quality']['sub_fasilitas']) }}</strong></span>
    <div class="buttons">
        <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#inspection_data_group_add_modal">
            {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
            {{ __('Tambah IAR') }}
        </button>
    </div>

    <div class="table-responsive mt-2 mb-5">
        <table class="table table-bordered nowrap" id="inspection_table">
            <thead class="header-white">
                <tr>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" rowspan="2">{{ $data['project']['jenis_periode'] }} Ke</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" rowspan="2">{{ __('Invitation') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" colspan="4">{{ __('Status') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" rowspan="2">{{ __('Inspection Acceptance Rate') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" rowspan="2">{{ __('Evidence') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white" rowspan="2">{{ __('Aksi') }}</th>
                </tr>
                <tr>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white">{{ __('Re Inspect') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white">{{ __('Accepted by Consultant / MK') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white">{{ __('Accepted by Owner') }}</th>
                    <th style="color:white !important" class="text-center align-middle bg-primary text-white">{{ __('Accepted by Both') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    @include('pages.project-monitoring.port.quality.modals.inspection_data_group_add')
    @include('pages.project-monitoring.port.quality.modals.inspection_data_group_edit')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.port.quality.index', $data['project']['id']) }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            datatable();
        });

        $(document).on('click', '.btn-inspection-edit', function() {
            $.ajax({
                url: "{{ route('ipm.port.quality.inspection.detail', [$data['project']['id'], $data['sub_visual_quality']['id']]) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id')
                },
                success: function(response) {
                    $('#inspection_data_group_edit_modal input[name="id"]').val(response?.id);
                    $('#inspection_data_group_edit_modal input[name="periode"]').val(response?.periode);
                    $('#inspection_data_group_edit_modal input[name="jml_undangan"]').val(response?.jml_undangan);
                    $('#inspection_data_group_edit_modal input[name="reinspect"]').val(response?.reinspect);
                    $('#inspection_data_group_edit_modal input[name="acc_konsultan"]').val(response?.acc_konsultan);
                    $('#inspection_data_group_edit_modal input[name="acc_owner"]').val(response?.acc_owner);
                    $('#inspection_data_group_edit_modal input[name="acc_both"]').val(response?.acc_both);
                    $('#evidence_item').attr('href', response?.evidence);

                    $('#inspection_data_group_edit_modal').modal('show');
                },
                error: function(error) {
                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        $(document).on('click', '.btn-inspection-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.port.quality.inspection.delete', [$data['project']['id'], $data['sub_visual_quality']['id']]) }}`,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function(response) {
                            Swal.fire({
                                text: response?.message,
                                icon: "success"
                            });

                            datatable();
                        },
                        error: function(error) {
                            Swal.fire({
                                title: error?.responseJSON?.message,
                                text: error?.responseJSON?.error,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });

        function datatable() {
            $('#inspection_table').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                order: [],
                ajax: {
                    url: "{{ route('ipm.port.quality.inspection.index', [$data['project']['id'], $data['sub_visual_quality']['id']]) }}"
                },
                columns: [{
                        data: 'periode',
                        name: 'periode',
                    },
                    {
                        data: 'jml_undangan',
                        name: 'jml_undangan',
                    },
                    {
                        data: 'reinspect',
                        name: 'reinspect',
                    },
                    {
                        data: 'acc_konsultan',
                        name: 'acc_konsultan',
                    },
                    {
                        data: 'acc_owner',
                        name: 'acc_owner',
                    },
                    {
                        data: 'acc_both',
                        name: 'acc_both',
                    },
                    {
                        data: 'rate',
                        name: 'rate',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'evidence',
                        name: 'evidence',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                    }
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }
    </script>
@endpush
