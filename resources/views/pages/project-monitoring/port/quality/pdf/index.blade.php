<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')

<head>
    <title> {{ $data['file_name'] }} </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        @page {
            size: A4 landscape;
        }

        .content {
            padding: 24px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }

        .logo {
            height: 150px;
        }

        .table {
            padding: 10px;
        }

        .table-data th,
        .table-data td {
            border: 1px solid black;
            padding: 8px
        }

        .table-data th,
        .center {
            text-align: center;
        }

        .table-title {
            margin: 15px 0;
            font-size: 15px;
        }

        .fw-bold {
            font-weight: bold;
        }
    </style>

</head>

<body>
    <img alt="Logo" src="{{ $ctrl->base64Logo() }}" style="width:150px; height:auto" />
    <h2 style="font-size: 1.25em; font-weight: bold; margin: 12px 0 8 0;">Integrated Project Monitoring Quality Port</h2>
    <table>
        <tr>
            <td colspan="2"><br>
                <table cellpadding="3" style="width:100%;">
                    <tbody>
                        <tr>
                            <td><b>Nama Project</b></td>
                            <td style=" width: auto;"><b>: {{ $data['project']['nama_project'] }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Nilai Project</b></td>
                            <td><b>: {{ toRupiah($data['project']['nilai_project']) }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Tanggal Mulai</b></td>
                            <td><b>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Tanggal Selesai</b></td>
                            <td><b>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Total Quality Plan</b></td>
                            <td><b>: {{ toPercent($data['cumulative']['total_actual'], 2) }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Total Quality Rate</b></td>
                            <td><b>: {{ toPercent($data['cumulative']['total_plan'], 2) }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Total Deviasi</b></td>
                            <td><b>: {{ toPercent($data['cumulative']['total_deviasi'], 2) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <div style="height: 300px;">
                    @if (!empty($data['drawing']['drawing']))
                        <img src="{{ $data['drawing']['drawing'] }}" alt="{{ $data['drawing']['deskripsi'] }}" />
                    @else
                        <div style="width: 500px; height: 100%; border: 1px solid black; float: right;"></div>
                    @endif
                </div>
            </td>
        </tr>
    </table>

    <h2 class="table-title"><b>VISUAL QUALITY PROGRESS ( PERIODE : {{ $data['period'] }})</b></h2>
    <table class="table-data center">
        <thead>
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Group</th>
                <th rowspan="2">Last Update</th>
                <th rowspan="2">Bobot</th>
                <th colspan="3">Quality Progress</th>
                <th rowspan="2">Status Progress</th>
            </tr>
            <tr>
                <th>Plan</th>
                <th>Actual</th>
                <th>Deviasi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['visual_qualities'] as $key => $group)
                <tr class="fw-bold">
                    <td class="center">{{ $key + 1 }}</td>
                    <td>{{ $group['fasilitas'] }}</td>
                    <td></td>
                    <td class="center">{{ toPercent($group['bobot'] ?? 0) }}</td>
                    <td class="center">{{ toPercent($group['total_plan'] ?? 0) }}</td>
                    <td class="center">{{ toPercent($group['total_actual'] ?? 0) }}</td>
                    <td class="center">{{ toPercent($group['total_deviasi'] ?? 0) }}</td>
                    <td></td>
                </tr>

                @foreach ($group['sub_visual_qualities'] as $idx => $sub)
                    <tr class="text-center">
                        <td>{{ $key + 1 }}.{{ $idx + 1 }}</td>
                        <td>{{ $sub['sub_fasilitas'] }}</td>
                        <td>{{ date('Y-m-d h:i:s', strtotime($sub['updated_at'])) }}</td>
                        <td>{{ toPercent($sub['bobot'] ?? 0) }}</td>
                        <td>{{ toPercent($sub['plan'] ?? 0) }}</td>
                        <td>{{ toPercent($sub['actual'] ?? 0) }}</td>
                        <td>{{ toPercent($sub['deviasi'] ?? 0) }}</td>
                        <td>{{ $sub['status_progress'] }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>

    <h2 class="table-title"><b>INSPECTION ACCEPTANCE RATE</b></h2>
    <table class="table-data center">
        <thead>
            <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Group</th>
                <th colspan="3">Accepted By</th>
            </tr>
            <tr>
                <th>Consultant</th>
                <th>Owner</th>
                <th>Both</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['inspection_acceptances'] as $key => $iar)
                <tr class="fw-bold">
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $iar['fasilitas'] }}</td>
                    <td colspan="3"></td>
                </tr>

                @foreach ($iar['sub_visual_qualities'] as $idx => $sub)
                    <tr>
                        <td>{{ $key + 1 }}.{{ $idx + 1 }}</td>
                        <td>{{ $sub['sub_fasilitas'] }}</td>
                        <td>{{ toPercent($sub['consultant_rate'] ?? 0) }}</td>
                        <td>{{ toPercent($sub['owner_rate'] ?? 0) }}</td>
                        <td>{{ toPercent($sub['both_rate'] ?? 0) }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>

</html>
