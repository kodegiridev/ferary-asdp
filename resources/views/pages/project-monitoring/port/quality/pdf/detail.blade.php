<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')


<head>
    <title> {{ $data['file_name'] }} </title>
    <style>
        @page {
            size: A4 landscape;
        }

        .title {
            text-align: left;
            margin-bottom: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            text-align: center;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .text-success {
            color: #50cd89 !important;
        }

        .borderless {
            border: 0;
        }
    </style>
</head>

<body>

    {{-- <img src="data:image/png;base64,{{ base64_encode($imageLogo) }}" alt="Logo Ferary"  style="width:150px; height:auto"> --}}
    <img alt="Logo" src="{{ $ctrl->base64Logo() }}" style="width:150px; height:auto" />
    <div class="title">
        <h3>Visual Quality Port | {{ $data['project']['nama_project'] }}</h3>
    </div>

    @foreach ($data['inspections'] as $value)
        <h5>{{ $value['visual_quality_name'] }}: {{ $value['sub_visual_quality_name'] }} ({{ $value['inspection_name'] }})</h5>

        <table style="width: 100%; outline: 1px solid;">
            <tr>
                <td class="borderless">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <td class="borderless" rowspan="2" style="text-align: center">NO</td>
                                <td class="borderless" colspan="3" style="text-align: center">INSPECTION</td>
                            </tr>
                            <tr>
                                <td class="borderless" style="text-align: center">STATUS</td>
                                <td class="borderless" style="text-align: center">QTY</td>
                                <td class="borderless" style="text-align: center">%</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($value['summaries']['data'] as $summary)
                                <tr>
                                    <td class="borderless" style="text-align: center">{{ $loop->iteration }}</td>
                                    <td class="borderless" style="text-align: center">{{ strtoupper($summary['status']) }}</td>
                                    <td class="borderless" style="{{ $summary['pdf_style'] ?? 'text-align: center' }}">{{ $summary['qty'] }}</td>
                                    <td class="borderless" style="text-align: center">{{ $summary['percentage'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="borderless"></td>
                                <td class="borderless" style="text-align: center;">TOTAL</td>
                                <td class="borderless" style="text-align: center;">{{ $value['summaries']['summary']['qty'] }}</td>
                                <td class="borderless" style="text-align: center;">{{ $value['summaries']['summary']['percentage'] }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>

        @foreach ($value['inspections'] as $item)
            <table style="margin-top: 20px;">
                <thead>
                    <tr>
                        <th style="border-right:0" colspan="2">Code: {{ $item['kode'] }}</th>
                        <th style="border-left:0; border-right:0" colspan="2">Item: {{ $item['item'] ?? ($item['sub_fasilitas'] ?? []) }}</th>
                        <th style="border-left:0" colspan="2">Evidence : <a href="{{ $item['evidence'] }}">{{ $ctrl->getFileName($item['evidence']) }}</a></th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Code Compartment</th>
                        <th>Plan Status</th>
                        <th>Inspection Status</th>
                        <th>Inspection Result</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($item['remarks'] as $key => $remark)
                        <tr>
                            <td class="center">{{ $key + 1 }}</td>
                            <td>{{ $remark['remarks'] }}</td>
                            <td>{{ $data['remark_status'][$remark['status_plan'] ?? 0] }}</td>
                            <td>{{ $data['remark_status'][$remark['status_inspeksi'] ?? 0] }}</td>
                            <td>{{ $remark['catatan_inspeksi'] }}</td>
                            <td class="center">{{ $remark['tanggal'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endforeach
    @endforeach
</body>

</html>
