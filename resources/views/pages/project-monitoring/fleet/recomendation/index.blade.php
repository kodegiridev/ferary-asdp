@extends('pages.project-monitoring.fleet.detail')

@section('recomendation-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Recomendation') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-start">
        <div class="col-md-6 col-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="25%">Nama Project</td>
                        <td>: {{ $data['project']['nama_project'] }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Selesai</td>
                        <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice Open</td>
                        <td>: {{ $data['notice']['total_open'] }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice Close</td>
                        <td>: {{ $data['notice']['total_close'] }}</td>
                    </tr>
                    <tr>
                        <td>Total Notice</td>
                        <td>: {{ $data['notice']['total'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="d-flex justify-content-start">
        <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_periode_modal">
            <i class="fa fa-plus"></i>
            {{ __('Tambah') }}
        </button>
        &nbsp
        &nbsp
        <a href="{{ route('ipm.fleet.recomendation.print', $data['project']['id']) }}" class="btn btn-sm btn-warning btn-icon">
            {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
        </a>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id="recomendation_table">
            <thead>
                <tr>
                    <th class="">{{ ucfirst($data['project']['jenis_periode']) }}</th>
                    <th class="">{{ __('Jumlah Notice') }}</th>
                    <th class="">{{ __('Notice Closed') }}</th>
                    <th class="">{{ __('Persentase Closed') }}</th>
                    <th class="">{{ __('Aksi') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    @include('pages.project-monitoring.fleet.recomendation.modals.create-periode')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        var table = $('#recomendation_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ipm.fleet.recomendation.index', $data['project']['id']) }}"
            },
            columns: [{
                    data: 'periode',
                    name: 'periode'
                },
                {
                    data: 'total_notice',
                    name: 'total_notice'
                },
                {
                    data: 'closed_notice',
                    name: 'closed_notice'
                },
                {
                    data: 'closed_percentage',
                    name: 'closed_percentage'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [{
                target: '_all',
                className: 'text-center align-middle'
            }]
        });
    </script>
@endpush
