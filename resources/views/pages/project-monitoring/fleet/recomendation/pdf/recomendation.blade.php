<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')

<head>
    <title>{{ $data['file_name'] }}</title>
    <style type="text/css">
        @page {
            size: A4 landscape;
            margin-top: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }

        .logo {
            height: 150px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .project-info th,
        .project-info td {
            padding: 4px 0px;
        }

        .table-data th,
        .table-data td {
            border: 1px solid black;
            padding: 8px 0px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="mb-10">
        <img class="logo" src="{{ $ctrl->base64Logo() }}" />
    </div>
    <div class="mb-10">
        <h3>Integrated Project Monitoring Recomendation Fleet</h3>
    </div>
    <table class="project-info mb-10">
        <tbody>
            <tr>
                <td width="20%">Nama Project</td>
                <td>: {{ $data['project']['nama_project'] }}</td>
            </tr>
            <tr>
                <td>Nilai Project</td>
                <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td>
                <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Total Notice Open</td>
                <td>: {{ $data['notice']['total_open'] }}</td>
            </tr>
            <tr>
                <td>Total Notice Close</td>
                <td>: {{ $data['notice']['total_close'] }}</td>
            </tr>
            <tr>
                <td>Total Notice</td>
                <td>: {{ $data['notice']['total'] }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table-data">
        <thead>
            <tr>
                <th>{{ ucfirst($data['project']['jenis_periode']) }}</th>
                <th>Jumlah Notice</th>
                <th>Notice Closed</th>
                <th>Persentase Closed</th>
            </tr>
        </thead>
        @if (count($data['periods']) > 0)
            <tbody>
                @foreach ($data['periods'] as $key => $period)
                    <tr>
                        <td>{{ $period->periode }}</td>
                        <td>{{ $period->total }}</td>
                        <td>{{ $period->total_close }}</td>
                        @if ($period->total > 0)
                            <td>{{ toPercent(($period->total_close / $period->total) * 100, 2) }}</td>
                        @else
                            <td>0</td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        @endif
    </table>
</body>

</html>
