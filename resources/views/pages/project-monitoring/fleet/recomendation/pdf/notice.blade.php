<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')
@inject('carbon', 'Carbon\Carbon')

<head>
    <title>{{ $data['file_name'] }}</title>
    <style type="text/css">
        @page {
            size: A4 landscape;
            margin-top: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }

        .logo {
            height: 150px;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .project-info th,
        .project-info td {
            padding: 4px 0px;
        }

        .table-data th,
        .table-data td {
            border: 1px solid black;
        }

        .table-data th {
            padding: 8px 0px;
        }

        .table-data td {
            padding: 8px 4px;
        }

        .table-data th,
        .center {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="mb-10">
        <img class="logo" src="{{ $ctrl->base64Logo() }}" />
    </div>
    <div class="mb-10">
        <h3>Detail Integrated Project Monitoring Recomendation Fleet</h3>
    </div>
    <table class="project-info mb-10">
        <tbody>
            <tr>
                <td width="20%">Nama Project</td>
                <td>: {{ $data['project']['nama_project'] }}</td>
            </tr>
            <tr>
                <td>Nilai Project</td>
                <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td>
                <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Total Notice Open</td>
                <td>: {{ $data['notice']['total_open'] }}</td>
            </tr>
            <tr>
                <td>Total Notice Close</td>
                <td>: {{ $data['notice']['total_close'] }}</td>
            </tr>
            <tr>
                <td>Total Notice</td>
                <td>: {{ $data['notice']['total'] }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table-data">
        <thead>
            <tr>
                <th>No</th>
                <th>Notice</th>
                <th>Pending Matters</th>
                <th>Execute By</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Evidence</th>
            </tr>
        </thead>
        @if (count($data['notices']) > 0)
            <tbody>
                @foreach ($data['notices'] as $key => $row)
                    <tr>
                        <td class="center">{{ $key + 1 }}</td>
                        <td>{{ $row->notice }}</td>
                        <td>{{ $row->to_do_list }}</td>
                        <td>{{ $row->user->name }}</td>
                        <td class="center">{{ $carbon::parse($row->due_date)->format('Y-m-d') }}</td>
                        <td class="center">{{ strtoupper($row->status) }}</td>
                        <td>{{ $ctrl->getFileName($row->file) }}</td>
                    </tr>
                @endforeach
            </tbody>
        @endif
    </table>
</body>

</html>
