<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_notice_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.recomendation.notice.update', [$data['project']['id'], $data['period']['id']]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit_notice_modal" name="action">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Notice') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Notice') }}</label>
                        <input type="text" class="form-control form-control-solid @error('notice') is-invalid @enderror" name="notice" value="{{ old('notice') }}" />
                        @error('notice')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('To Do List') }}</label>
                        <textarea class="form-control form-control-solid @error('to_do_list') is-invalid @enderror" rows="3" id="edit_to_do_list" name="to_do_list">{!! old('to_do_list') !!}</textarea>
                        @error('to_do_list')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Due Date') }}</label>
                        <input type="date" class="form-control form-control-solid @error('due_date') is-invalid @enderror" id="due_date" name="due_date" value="{{ old('due_date') }}" />
                        @error('due_date')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required">{{ __('Status') }}</label>
                        <select class="form-control form-control-solid @error('status') is-invalid @enderror" name="status" id="edit_status" data-control="select2" data-placeholder="Pilih Status"></select>
                        @error('status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('PIC') }}</label>
                        <select class="form-control form-control-solid @error('pic') is-invalid @enderror" name="pic" id="edit_pic" data-control="select2" data-placeholder="Pilih PIC"></select>
                        @error('pic')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Evidence') }}</label>
                        <input type="file" class="form-control form-control-solid @error('file') is-invalid @enderror" name="file" accept=".pdf" />
                        @error('file')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
