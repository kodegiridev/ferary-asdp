<div class="modal fade" tabindex="-1" id="basic_data_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Basic Data') }} <span id="basic_data_title">{{ $fleet->nama_project }}</span></h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

                <div class="modal-body">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-information-project">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#elm-information-project" aria-expanded="false" aria-controls="elm-information-project">
                                Information Project
                                </button>
                            </h2>
                            <div id="elm-information-project" class="accordion-collapse collapse" aria-labelledby="heading-information-project" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="name" class="required form-label">
                                                {{ __('Nama Project') }}
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control" value="{{ $fleet->nama_project }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="name" class="required form-label">
                                                {{ __('Nilai Project') }}
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control nilai_project" value="{{ $fleet->nilai_project }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="name" class="required form-label">
                                                {{ __('PIC') }}
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control" value="{{ $fleet->user->name }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="name" class="required form-label">
                                                {{ __('Nomor Kontrak') }}
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control" value="{{ $fleet->nomor_kontrak }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="name" class="required form-label">
                                                {{ __('Supervisor') }}
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control" value="{{ $fleet->supervisor }}" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <button id="btn-edit-basic-data" type="button" class="btn btn-success btn-sm mt-2"  data-bs-toggle="modal" data-bs-target="#edit_basic_data_modal">{{ __('Edit') }}</button>
                                </div>
                            </div>
                        </div>
                        @if (!empty($basic_data))
                            @foreach ($basic_data as $key => $item)
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-induk-{{ $key }}">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#elm-induk-{{ $key }}" aria-expanded="false" aria-controls="elm-induk-{{ $key }}">
                                    {{ $item->status_kontrak }}
                                    </button>
                                </h2>
                                <div id="elm-induk-{{ $key }}" class="accordion-collapse collapse" aria-labelledby="heading-induk-{{ $key }}" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="name" class="required form-label">
                                                    {{ __('Tanggal Kontrak') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($item->mulai_kontrak))." s/d ".date('d-m-Y', strtotime($item->selesai_kontrak)) }}" disabled/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="name" class="required form-label">
                                                    {{ __('Jangka Waktu Pelaksanaan') }}
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="text" class="form-control" value="{{ $item->jangka_waktu }} Minggu" disabled/>
                                                </div>
                                            </div>
                                        </div>
                                        @if (auth()->user()->id == $fleet->pic)
                                        <form action="{{ route('ipm.fleet.basic_data.delete', $item) }}" method="post">
                                            <button id="btn-delete-basic-data" type="submit" class="btn btn-danger btn-sm mt-2">{{ __('Hapus') }}</button>
                                            @method('delete')
                                            @csrf
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        
                    </div>
                    
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
                    @if (auth()->user()->id == $fleet->pic)
                    <button id="btn-add-basic-data" type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#add_basic_data_modal">{{ __('Tambah') }}</button>
                    @endif
                </div>
        </div>
    </div>
</div>
