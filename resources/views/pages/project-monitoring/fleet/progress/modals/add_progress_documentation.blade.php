<div class="modal fade" tabindex="-1" id="add_progress_documentation_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Dokumentasi') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.fleet.progress.dokumentasi_store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="progress_id" id="progress_id">
                <div class="modal-body">
                    <ol id="document-list">
                        
                    </ol>
                    <hr>
                    <div id="add_documentation">
                        <h3 class="mb-2">Tambah Dokumentasi</h3>
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="required form-label">
                                    {{ __('Upload Dokumentasi') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input name="file" type="file" class="form-control" accept="image/*" required/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label">
                                    {{ __('Nama Dokumentasi') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input name="nama" type="text" class="form-control" placeholder="Nama Dokumentasi" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-add-progress-documentation" type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
