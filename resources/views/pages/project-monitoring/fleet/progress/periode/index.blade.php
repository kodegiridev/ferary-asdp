@extends('pages.project-monitoring.fleet.detail', ['title' => $data['project']->project->nama_project, 'project_id' => $data['project']->project_id])

@section('progress-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('fleet (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.progress.index', $data['project']->project->id) }}" class="text-muted text-hover-primary">{{ __('Progress') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Periode') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="d-flex justify-content-start">
        <div class="table-responsive" style="width: 100%">
            <table class="table">
                <?php 
                    $status_kontrak_data = "";
                    $mulai_kontrak      = "";
                    $selesai_kontrak    = "";
                    if (!empty($data['project']->project->latest_basic_data->status_kontrak)) { 
                        $status_kontrak_data = $data['project']->project->latest_basic_data->status_kontrak;  
                        $mulai_kontrak = $data['project']->project->latest_basic_data->mulai_kontrak;  
                        $selesai_kontrak = $data['project']->project->latest_basic_data->selesai_kontrak;  
                    }
                ?>
                <tbody>
                    <tr>
                        <td width="25%">Nama Project</td>
                        <td>: {{ $data['project']->project->nama_project }}</td>
                    </tr>
                    <tr>
                        <td>Nilai Project</td>
                        <td>: {{ toRupiah($data['project']->project->nilai_project) }}</td>
                    </tr>
                    <tr>
                        <td>Actual Cost</td>
                        <td>: {{ toRupiah($data['project']->actual_cost) }}</td>
                    </tr>
                    <tr>
                        <td>Status Kontrak</td>
                        <td>: {{ $status_kontrak_data }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Mulai</td>
                        <td>: {{ $mulai_kontrak }}</td>
                    </tr>
                    <tr>
                        <td>Total Plan</td>
                        <td>: {{ toDecimal($data['datas']['total_plan']) }}%</td>
                    </tr>
                    <tr>
                        <td>Total Progres</td>
                        <td>: {{ toDecimal($data['datas']['total_progres']) }}%</td>
                    </tr>
                    <tr>
                        <td>Total Deviasi</td>
                        <td>: <span class="text-success">{{ toDecimal($data['datas']['total_deviasi']) }}%</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @if ($data['project']->project->pic == auth()->user()->id && $data['can_edit'])
    <div class="d-flex justify-content-start mt-5 mb-2">
        <button type="button" class="btn btn-sm btn-primary m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#add_main_item_modal" data-id="">
            {{ __('+ Tambah Main Item') }}
        </button>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-bordered" id="project_table">
            <thead class="bg-primary text-white">
                <tr>
                    <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('No') }}</th>
                    <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Item Pekerjaan') }}</th>
                    <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Bobot') }}</th>
                    <th class="text-center align-middle w-200px" colspan="4" style="color:white !important">{{ __('Per Periode (%)') }}</th>
                    <th class="text-center align-middle w-200px" colspan="3" style="color:white !important">{{ __('Total (%)') }}</th>
                    <th class="text-center align-middle min-w-150px" rowspan="2" style="color:white !important">{{ __('Aksi') }}</th>
                </tr>
                <tr>
                    <th>{{ __('Plan') }}</th>
                    <th>{{ __('Progress Lalu') }}</th>
                    <th>{{ __('Progress Ini') }}</th>
                    <th>{{ __('Deviasi') }}</th>
                    <th>{{ __('Plan') }}</th>
                    <th>{{ __('Progress') }}</th>
                    <th>{{ __('Deviasi') }}</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($data['main_item']))
                    @foreach ($data['main_item'] as $item)
                        <tr>
                            <td class="middle fw-bold">#</td>
                            <td class="middle fw-bold">{{ $item->main_item }}</td>
                            <td class="middle fw-bold">{{ $item->batas_bobot }}</td>
                            <td class="middle fw-bold">{{ $item->plan }}</td>
                            <td class="middle fw-bold">{{ $item->progres_lalu }}</td>
                            <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $item->deviasi }}</td>
                            <td class="middle fw-bold">{{ $item->plan }}</td>
                            <td class="middle fw-bold">{{ $item->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $item->deviasi }}</td>
                            <td>
                                @if ($data['project']->project->pic == auth()->user()->id)
                                <a onclick="add_sub_item('{{ $item->id }}')" href="javascript:void(0)" class="btn btn-primary btn-sm m-1 btn-block" data-bs-toggle="modal" data-bs-target="#add_sub_item_modal">Tambah Sub</a>
                                <br>
                                <a onclick="edit_main_item('{{ $item->id }}', '{{ $item->main_item }}', '{{ $item->batas_bobot }}')" href="javascript:void(0)" class="btn btn-success btn-sm m-1" data-bs-toggle="modal" data-bs-target="#ubah_main_item_modal">Ubah</a>
                                <a onclick="delete_item('{{ $item->id }}', '{{ $item->main_item }}')" href="javascript:void(0)" class="btn btn-danger btn-sm m-1">Hapus</a>
                                @endif
                            </td>
                        </tr> 
                        @if (!empty($item->sub_item))
                        @foreach ($item->sub_item as $subitem)
                        <tr>
                            <td class="middle fw-bold">{{ $loop->iteration }}</td>
                            <td class="middle fw-bold">{{ $subitem->sub_item }}</td>
                            <td class="middle fw-bold">{{ $subitem->bobot }}</td>
                            <td class="middle fw-bold">{{ $subitem->plan }}</td>
                            <td class="middle fw-bold">{{ $subitem->progres_lalu }}</td>
                            <td class="middle fw-bold">{{ $subitem->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $subitem->deviasi }}</td>
                            <td class="middle fw-bold">{{ $subitem->plan }}</td>
                            <td class="middle fw-bold">{{ $subitem->progres_ini }}</td>
                            <td class="middle fw-bold">{{ $subitem->deviasi }}</td>
                            <td>
                                @if ($data['project']->project->pic == auth()->user()->id)
                                <a href="{{ route('ipm.fleet.progress_periode.edit_periode_item', ['project' => $item->progres_id, 'sub_item' => $subitem, 'sisa_bobot' => $item['sisa_bobot']]) }}" class="btn btn-success btn-sm m-1">Ubah</a>
                                <a onclick="delete_sub_item('{{ $subitem->id }}', '{{ $subitem->sub_item }}')" href="javascript:void(0)" class="btn btn-danger btn-sm m-1">Hapus</a>
                                @endif
                            </td>
                        </tr> 
                        @endforeach
                        @endif 
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.progress.index', $data['project']->project->id) }}">{{ __('Kembali') }}</a>
    </div>

    <form id="form-delete-main-item" action="" method="post">
        @csrf
    </form>

    @include('pages.project-monitoring.fleet.progress.periode.modals.add_main_item', ['progres' => $data['project']])
    @include('pages.project-monitoring.fleet.progress.periode.modals.add_sub_item')
    @include('pages.project-monitoring.fleet.progress.periode.modals.ubah_main_item')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        $(document).ready(function() {
            checkSession();
            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                prefix: "Rp. "
            }).mask(".nilai_project");
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}_project`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }

        function edit_main_item(id, main_item, batas_bobot) {
            var url = `{{ route('ipm.fleet.progress_periode.update_main_item', ':id') }}`;
                url = url.replace(':id', id);
            $("#form-update-main-item").attr('action', url);
            $("#em_main_item").val(main_item);
            $("#em_batas_bobot").val(batas_bobot);
        }

        function add_sub_item(main_item_id) {
            $("#main_item_id").val(main_item_id);
        }

        

        function delete_item(id, item_name) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus ${item_name}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                    var url = `{{ route('ipm.fleet.progress_periode.delete_main_item', ':id') }}`;
                        url = url.replace(':id', id);
                    $("#form-delete-main-item").attr('action', url);
                    $("#form-delete-main-item").submit();
                }
            })
        }

        function delete_sub_item(id, item_name) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus ${item_name}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                    var url = `{{ route('ipm.fleet.progress_periode.delete_sub_item', ':id') }}`;
                        url = url.replace(':id', id);
                    $("#form-delete-main-item").attr('action', url);
                    $("#form-delete-main-item").submit();
                }
            })
        }

        var table = $('#project_table').DataTable({
            // proccesing: true,
            // serverSide: true,
            order: [],
            // ajax: {
            //     url: "{{ route('ipm.port.index') }}"
            // },
            // columns: [{
            //         data: 'DT_RowIndex',
            //         name: 'DT_RowIndex',
            //         orderable: false,
            //         searchable: false,
            //     },
            //     {
            //         data: 'nama_project',
            //         name: 'nama_project'
            //     },
            //     {
            //         data: 'pic',
            //         name: 'pic'
            //     },
            //     {
            //         data: 'status_approval',
            //         name: 'status_approval'
            //     },
            //     {
            //         data: 'kontraktor',
            //         name: 'kontraktor'
            //     },
            //     {
            //         data: 'nilai_project',
            //         name: 'nilai_project'
            //     },
            //     {
            //         data: 'contractual',
            //         name: 'contractual'
            //     },
            //     {
            //         data: 'remaining',
            //         name: 'remaining'
            //     },
            //     {
            //         data: 'progres',
            //         name: 'progres'
            //     },
            //     {
            //         data: 'quality',
            //         name: 'quality'
            //     },
            //     {
            //         data: 'cpi',
            //         name: 'cpi'
            //     },
            //     {
            //         data: 'etc',
            //         name: 'etc'
            //     },
            //     {
            //         data: 'document',
            //         name: 'document',
            //         orderable: false,
            //         searchable: false,
            //         className: 'text-center'
            //     },
            //     {
            //         data: 'ncr',
            //         name: 'ncr'
            //     },
            //     {
            //         data: 'action',
            //         name: 'action',
            //         orderable: false,
            //         searchable: false,
            //         className: 'text-center'
            //     },
            // ],
        });

        const ctx = document.getElementById('myChart');

        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endpush
