@extends('pages.project-monitoring.fleet.detail', ['title' => $data['project']->project->nama_project, 'project_id' => $data['project']->project_id])

@section('progress-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('fleet (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.progress.index', $data['project']->project->id) }}" class="text-muted text-hover-primary">{{ __('Progress') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Periode') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
<form method="post" action="{{ route('ipm.fleet.progress_periode.store_task_item') }}">
    @csrf
    <input type="hidden" name="sub_item_id" value="{{ $data['sub_item']->id }}">
    <input type="hidden" name="progress_id" value="{{ $data['sub_item']->main_item->progres_id }}">
    <div style="margin-right: 150px; margin-left: 150px">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Main Item</th>
                        <th colspan="2">Sub Item</th>
                        <th>Sisa Bobot</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>
                            <input type="text" class="form-control" value="{{ $data['sub_item']->main_item->main_item }}" placeholder="Main Item" disabled/>
                        </th>
                        <th colspan="2"><input name="sub_item" type="text" class="form-control" placeholder="Sub Item" value="{{ $data['sub_item']->sub_item }}"/></th>
                        <th><input type="text" id="sisa_bobot" class="form-control" value="{{ $data['sisa_bobot'] }}" disabled/></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="sub-item">
                    @php $sisa_bobot_tanpa_task = $data['sisa_bobot']; @endphp
                    @if (!empty($data['sub_item']->task_item))
                    @foreach ($data['sub_item']->task_item as $i => $task)
                    <tr class="head subitem-{{ $i }}">
                        <td>Task <span class="ordering-data" id="ordering-{{ $i }}">{{ $i + 1 }}</span></td>
                        <td>Bobot (%)</td>
                        <td>Plan (%)</td>
                        <td>Progress (%)</td>
                        <td></td>
                    </tr>
                    <tr data-id="subitem-{{ $i }}" class="subitem-{{ $i }} subitem">
                        <td><input value="{{ $task->task }}" name="task[]" type="text" class="form-control" placeholder="Task"/></td>
                        <td><input id="subitem-{{ $i }}-bobot" onkeyup="calculate_bobot()" value="{{ $task->bobot }}" name="bobot[]" type="number" class="form-control data_bobot" placeholder="Bobot"/></td>
                        <td><input value="{{ $task->plan }}" name="plan[]" type="number" class="form-control" placeholder="Plan"/></td>
                        <td><input value="{{ $task->progres }}" name="progres[]" type="number" class="form-control" placeholder="Progress"/></td>
                        <td>
                            <button onclick="delete_subitem('subitem-{{ $i }}')" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    @php $sisa_bobot_tanpa_task += $task->bobot; @endphp
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between mt-5 mb-2 mr-5 ml-5">
            <button onclick="add_row()" type="button" class="btn btn-sm btn-success m-1">
                {{ __('+ Tambah Task') }}
            </button>
            <button type="submit" class="btn btn-sm btn-primary m-1">
                {{ __('Simpan') }}
            </button>
        </div>
    </div>
    
</form>
<div class="d-flex justify-content-end mt-5 mb-2">
    <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.progress_periode.index_periode', $data['project']->id) }}">{{ __('Kembali') }}</a>
</div>
@endsection
@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        add_row();
        checkSession();
    });
    function delete_subitem(classname) {
        $("." + classname).remove();
        reorder_number();
        calculate_bobot();
    }

    function add_row() {
        let randomNumber = Math.floor(Math.random() * 100000);

        let html = `
            <tr class="head subitem-${randomNumber}">
                <td>Task <span class="ordering-data" id="ordering-${randomNumber}"></span></td>
                <td>Bobot (%)</td>
                <td>Plan (%)</td>
                <td>Progress (%)</td>
                <td></td>
            </tr>
            <tr data-id="subitem-${randomNumber}" class="subitem-${randomNumber} subitem">
                <td><input name="task[]" type="text" class="form-control" placeholder="Task"/></td>
                <td><input id="subitem-${randomNumber}-bobot" onkeyup="calculate_bobot()" name="bobot[]" type="number" class="form-control" placeholder="Bobot"/></td>
                <td><input name="plan[]" type="number" class="form-control" placeholder="Plan"/></td>
                <td><input name="progres[]" type="number" class="form-control" placeholder="Progress"/></td>
                <td>
                    <button onclick="delete_subitem('subitem-${randomNumber}')" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        `;

        $("#sub-item").append(html);
        reorder_number();
    }

    function calculate_bobot() {
        let sisa_bobot_data = Number({{ $sisa_bobot_tanpa_task }});
        let total_bobot = 0;
        $('.subitem').each(function(i, obj) {
            let dataid = $(this).data('id');
            let bobot = Number($(`#${dataid}-bobot`).val());
            console.log("BOBOT " + dataid, bobot);
            total_bobot += bobot;
        });
        let sisa_bobot = sisa_bobot_data - total_bobot;
        console.log(sisa_bobot_data, total_bobot);
        $("#sisa_bobot").val(sisa_bobot);
    }

    function reorder_number() {
        $('.ordering-data').each(function(i, obj) {
            let nomor = i + 1;
            $(this).text(nomor);
        });
    }

    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: `{{ Session::get('success') }}`,
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: `{{ Session::get('failed') }}`,
                icon: "error"
            });
        }
    }
</script>
@endsection
