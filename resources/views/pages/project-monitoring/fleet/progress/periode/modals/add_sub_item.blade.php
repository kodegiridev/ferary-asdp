<div class="modal fade" tabindex="-1" id="add_sub_item_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Sub Item') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.fleet.progress_periode.store_sub_item') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="main_item" id="main_item_id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Sub Item') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="sub_item" type="text" class="form-control" placeholder="Sub Item"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-add-sub-item" type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
