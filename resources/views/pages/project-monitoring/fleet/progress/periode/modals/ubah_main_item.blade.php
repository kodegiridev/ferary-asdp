<div class="modal fade" tabindex="-1" id="ubah_main_item_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Ubah Main Item') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form id="form-update-main-item" action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Main Item') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="em_main_item" name="main_item" type="text" class="form-control @error('main_item') is-invalid @enderror" placeholder="Main Item"/>

                                @error('main_item')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Batas Bobot (%)') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="em_batas_bobot" name="batas_bobot" type="text" class="form-control @error('batas_bobot') is-invalid @enderror" placeholder="Batas Bobot"/>

                                @error('batas_bobot')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-add-main-item" type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
