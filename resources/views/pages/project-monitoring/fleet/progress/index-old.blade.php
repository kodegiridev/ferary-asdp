@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Progress') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <!--start::Code-->

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">PROGRESS</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">QUALITY</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">DOCUMENT</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">RECOMENDATION</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">EXECUTIVE SUMMARY</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="card shadow-sm">
                        <div class="card-header" style="background-color: #00a5bb">
                            <h3 class="card-title text-light">{{ __('Pelabuhan Aceh 2') }}</h3>
                            <div class="card-toolbar"></div>
                        </div>
                        <form action="{{ route('ipm.port.index') }}" method="get" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="d-flex justify-content-start">
                                    <div class="table-responsive" style="width: 100%">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td width="25%">Nama Project</td>
                                                    <td>: PELABUHAN ACEH 2</td>
                                                </tr>
                                                <tr>
                                                    <td>Nilai Project</td>
                                                    <td>: Rp. 5.000.000.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Status Kontrak</td>
                                                    <td>: Induk</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Mulai</td>
                                                    <td>: 2023-10-15</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai</td>
                                                    <td>: 2023-12-31</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Plan</td>
                                                    <td>: 0%</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Progres</td>
                                                    <td>: 0%</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Deviasi</td>
                                                    <td>: <span class="text-success">0</span>%</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: left">
                                                        <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#basic_data_modal" data-id="">{{ __('Basic Data') }}</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="width: 50%">
                                        <canvas id="myChart" style="height: 350px"></canvas>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-start mt-5 mb-2">
                                    <button type="button" class="btn btn-sm btn-primary m-1 btn-tolak" data-bs-toggle="modal" data-bs-target="#add_progress_modal" data-id="">
                                        {{ __('+ Tambah Progres') }}
                                    </button>
                                    <button type="button" class="btn btn-sm btn-secondary m-1 btn-icon">
                                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr029.svg', 'svg-icon-1') !!}
                                    </button>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="project_table">
                                        <thead class="bg-primary text-white">
                                            <tr>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Bulan') }}</th>
                                                <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Tanggal Update') }}</th>
                                                <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Actual Cost (Rp)') }}</th>
                                                <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Status Kontrak') }}</th>
                                                <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Contractor') }}</th>
                                                <th class="text-center align-middle w-200px" rowspan="2" style="color:white !important">{{ __('Value') }}</th>
                                                <th class="text-center align-middle" colspan="3" style="color:white !important">{{ __('Progress (%)') }}</th>
                                                <th class="text-center align-middle" colspan="3" style="color:white !important">{{ __('Total Progress (%)') }}</th>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('CPI') }}</th>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('PPC') }}</th>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Estimate Time Completion') }}</th>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Documentation)') }}</th>
                                                <th class="text-center align-middle" rowspan="2" style="color:white !important">{{ __('Aksi') }}</th>
                                            </tr>
                                            <tr>
                                                <th>{{ __('Plan') }}</th>
                                                <th>{{ __('Actual') }}</th>
                                                <th>{{ __('Deviasi') }}</th>
                                                <th>{{ __('Plan') }}</th>
                                                <th>{{ __('Actual') }}</th>
                                                <th>{{ __('Deviasi') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>2023-10-15</td>
                                                <td>200.000.000</td>
                                                <td>Induk</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td><a href="javascript:void(0)" class="text-primary" data-bs-toggle="modal" data-bs-target="#add_progress_documentation_modal">View...</a></td>
                                                <td>
                                                    <a href="{{ route('ipm.fleet.progress_periode.index_periode', 1) }}" class="btn btn-icon btn-primary btn-xs">
                                                        {!! theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-1') !!}
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-icon btn-success btn-xs" data-bs-toggle="modal" data-bs-target="#edit_progress_modal">
                                                        {!! theme()->getSvgIcon('icons/bi/pencil-square.svg', 'svg-icon-1') !!}
                                                    </a>
                                                    <a onclick="duplicate_progress()" href="javascript:void(0)" class="btn btn-icon btn-warning btn-xs">
                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen028.svg', 'svg-icon-1') !!}
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-icon btn-secondary btn-xs">
                                                        {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
                                                    </a>
                                                    <a onclick="delete_progress()" href="javascript:void(0)" class="btn btn-icon btn-danger btn-xs">
                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') !!}
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-end mt-5 mb-2">
                                    <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.index') }}">{{ __('Kembali') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.project-monitoring.fleet.progress.modals.basic_data')
    @include('pages.project-monitoring.fleet.progress.modals.add_basic_data')
    @include('pages.project-monitoring.fleet.progress.modals.edit_basic_data')

    @include('pages.project-monitoring.fleet.progress.modals.add_progress')
    @include('pages.project-monitoring.fleet.progress.modals.add_progress_documentation')
    @include('pages.project-monitoring.fleet.progress.modals.edit_progress')
@endsection
@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        function duplicate_progress() {
            let tgl_periode = "2023-10-26";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-warning',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Duplikasi Data',
                text: `Anda yakin ingin menduplikasi Data Progres Induk Tanggal ${tgl_periode}?`,
                showCancelButton: true,
                confirmButtonText: 'Duplikasi!',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                }
            })
        }

        function delete_progress() {
            let tgl_periode = "2023-10-26";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Data Progres Induk Tanggal ${tgl_periode}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                }
            })
        }

        var table = $('#project_table').DataTable({
            // proccesing: true,
            // serverSide: true,
            // order: [],
            // ajax: {
            //     url: "{{ route('ipm.port.index') }}"
            // },
            // columns: [{
            //         data: 'DT_RowIndex',
            //         name: 'DT_RowIndex',
            //         orderable: false,
            //         searchable: false,
            //     },
            //     {
            //         data: 'nama_project',
            //         name: 'nama_project'
            //     },
            //     {
            //         data: 'pic',
            //         name: 'pic'
            //     },
            //     {
            //         data: 'status_approval',
            //         name: 'status_approval'
            //     },
            //     {
            //         data: 'kontraktor',
            //         name: 'kontraktor'
            //     },
            //     {
            //         data: 'nilai_project',
            //         name: 'nilai_project'
            //     },
            //     {
            //         data: 'contractual',
            //         name: 'contractual'
            //     },
            //     {
            //         data: 'remaining',
            //         name: 'remaining'
            //     },
            //     {
            //         data: 'progres',
            //         name: 'progres'
            //     },
            //     {
            //         data: 'quality',
            //         name: 'quality'
            //     },
            //     {
            //         data: 'cpi',
            //         name: 'cpi'
            //     },
            //     {
            //         data: 'etc',
            //         name: 'etc'
            //     },
            //     {
            //         data: 'document',
            //         name: 'document',
            //         orderable: false,
            //         searchable: false,
            //         className: 'text-center'
            //     },
            //     {
            //         data: 'ncr',
            //         name: 'ncr'
            //     },
            //     {
            //         data: 'action',
            //         name: 'action',
            //         orderable: false,
            //         searchable: false,
            //         className: 'text-center'
            //     },
            // ],
        });

        const ctx = document.getElementById('myChart');

        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endsection
