<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="submit_completion_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Project Completion') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>

            <form class="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="send_approval" name="action">
                <input type="hidden" name="catatan_project">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="penyetuju_id" class="required form-label">
                                {{ __('Approver') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="penyetuju_id" class="form-select approver @error('penyetuju_id') is-invalid @enderror"></select>

                                @error('penyetuju_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center justify-content-between my-3">
                        <div class="col-6">
                            <label for="user_verifikator_id" class="required form-label">
                                {{ __('Verifikator') }}
                            </label>
                        </div>
                        <div class="col-6">
                            <button type="button" class="btn btn-sm btn-dark float-end" id="btn_add_verificator_completion">
                                <i class="fa fa-plus"></i>
                                <i class="fa fa-user"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="box-input-verificator-completion @error('user_verifikator_id.*') is-invalid @enderror">
                            <div class="row input-verificator">
                                <div class="col-10">
                                    <select name="user_verifikator_id[]" class="form-select verifikator"></select>
                                </div>
                            </div>
                        </div>
                        @error('user_verifikator_id.*')
                            <span class="invalid-feedback mb-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <label for="keterangan_pengaju" class="form-label">{{ __('Remarks') }}</label>
                        <input type="text" class="form-control form-control-solid @error('keterangan_pengaju') is-invalid @enderror" placeholder="Ketik disini" name="keterangan_pengaju" value="{{ old('keterangan_pengaju') }}" />

                        @error('keterangan_pengaju')
                            <span class="invalid-feedback mb-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="button" class="btn btn-sm btn-primary" id="btn_submit_completion">{{ __('Kirim') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('modal-scripts')
    <script>
        $(document).on('click', '#btn_add_verificator_completion', function() {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator-completion');
            var options = ''

            $('select[name="user_verifikator_id[]"]').each(function(index, element) {
                if (index == 0) {
                    options = $(this).html();
                }
            });

            boxInputVerificator.append(`
                <div class="row input-verificator mt-2">
                    <div class="col-10">
                        <select name="user_verifikator_id[]" class="form-select form-select-solid">
                            ${options}
                        </select>
                    </div>
                    <div class="col-1 d-flex align-items-center">
                        <button type="button" class="btn btn-sm btn-danger btn-delete-verificator-completion">
                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr090.svg', 'svg-icon-3') !!}
                        </button>
                    </div>
                </div>
            `);
        });

        $(document).on('click', '.btn-delete-verificator-completion', function() {
            var inputVerificator = $(this).closest('.input-verificator');
            var selectElement = inputVerificator.find('select').val();

            if (selectElement != '') {
                var approver = $('select[name="penyetuju_id"]');
                var verificator = $('select[name="user_verifikator_id[]"]');

                $.each(approver.find('option'), function(key, value) {
                    if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                        $(this).prop('hidden', false);
                    }
                });

                verificator.each(function(index, element) {
                    var selectVerificator = $(this);
                    $.each(selectVerificator.find('option'), function(key, value) {
                        if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                            $(this).prop('hidden', false);
                        }
                    });
                });
            }
            inputVerificator.remove();
        });

        $(document).on('change', 'select[name="penyetuju_id"]', function() {
            var hiddenOption = [];
            var approver = $('select[name="penyetuju_id"]');
            var verificator = $('select[name="user_verifikator_id[]"]');

            if (approver.val() != '') {
                hiddenOption.push(parseInt(approver.val()));
            }

            verificator.each(function(index, element) {
                if ($(this).val() != '') {
                    hiddenOption.push(parseInt($(this).val()));
                }
            });

            $.each(approver.find('option'), function(key, value) {
                if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', true);
                }
                if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', false);
                }
            });

            verificator.each(function(index, element) {
                var selectVerificator = $(this);
                $.each(selectVerificator.find('option'), function(key, value) {
                    if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', true);
                    }
                    if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', false);
                    }
                });
            });
        });

        $(document).on('change', 'select[name="user_verifikator_id[]"]', function() {
            var hiddenOption = [];
            var approver = $('select[name="penyetuju_id"]');
            var verificator = $('select[name="user_verifikator_id[]"]');

            if (approver.val() != '') {
                hiddenOption.push(parseInt(approver.val()));
            }

            verificator.each(function(index, element) {
                if ($(this).val() != '') {
                    hiddenOption.push(parseInt($(this).val()));
                }
            });

            $.each(approver.find('option'), function(key, value) {
                if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', true);
                }
                if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', false);
                }
            });

            verificator.each(function(index, element) {
                var selectVerificator = $(this);
                $.each(selectVerificator.find('option'), function(key, value) {
                    if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', true);
                    }
                    if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', false);
                    }
                });
            });
        });

        $(document).on('click', '#btn_submit_completion', function() {
            $('#submit_completion_warning').modal('show');
            $('#submit_completion_modal').modal('hide');
        });

        $(document).on('click', '#btn_confirm_completion', function() {
            $.ajax({
                url: "{{ route('ipm.fleet.completion.send-approval', $data['project']['id']) }}",
                type: 'POST',
                data: new FormData($('#submit_completion_modal .form')[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    location.reload();
                },
                error: function(err) {
                    $(`#submit_completion_modal`).find('.form')[0].reset();
                    $('#submit_completion_warning').modal('hide')
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    })
                }
            });
        });
    </script>
@endpush
