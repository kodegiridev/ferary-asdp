<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title edit_detail_title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input id="edit_detail_id" type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_kode" name="main[kode]" type="text" class="form-control" placeholder="Code Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('System') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_sistem" name="main[sistem]" type="text" class="form-control" placeholder="System" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Component') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_komponen" name="main[komponen]" type="text" class="form-control" placeholder="Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Evidence') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a id="edit_detail_evidence" href="javascript:void(0)" target="_blank">Lihat Dokumen</a>
                            </div>
                        </div>
                    </div>
                    <div id="list_code_component"></div>
                    <button type="button" class="btn btn-sm btn-primary m-1" onclick="add_row()">
                        {{ __('+ Tambah New Code Component') }}
                    </button>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
