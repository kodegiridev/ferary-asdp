<div class="modal fade" tabindex="-1" id="add_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form class="form-milestone" action="" method="POST"  enctype="multipart/form-data">
                @csrf
                
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input type="hidden" name="main[visual_quality_fleet_id]" value="{{ $visual_quality_fleet_id }}">
                <input type="hidden" name="main[periode_visual_quality_id]" value="{{ $periode_quality_fleet_id }}">
                <input type="hidden" name="main[jenis_data]" value="1">
                <input type="hidden" name="main[jenis_proyek]" value="{{ $flag->jenis_project }}">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="main[kode]" type="text" class="form-control" placeholder="Code Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Equipment') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="main[peralatan]" type="text" class="form-control" placeholder="Compartment" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('State Status') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select 
                                    name="main[status_state]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                    <option value="0">Replace</option>
                                    <option value="1">Repair</option>
                                    <option value="2">Accepted</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="name" class="required form-label">
                                    {{ __('Plan Status') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <label for="name" class="required form-label">
                                    {{ __('Inspection Status') }}
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <select 
                                    name="main[status_plan]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                    <option value="1">Accepted</option>
                                    <option value="2">Inspection Invitation</option>
                                    <option value="3">Internal Progress Yard</option>
                                    <option value="0">No Progress</option>
                                </select>
                            </div>
                             <div class="col-6">
                                <select 
                                    name="main[status_inspeksi]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                    <option value="1">Accepted</option>
                                    <option value="2">Inspection Invitation</option>
                                    <option value="3">Internal Progress Yard</option>
                                    <option value="0">No Progress</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Remarks') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <input name="detail[nama_staging][]" type="text" class="form-control" placeholder="Remarks" required>
                            </div>
                            <div class="col-2">
                                <button onclick="add_remarks()" type="button" class="btn btn-primary">+</button>
                            </div>
                        </div>
                        <div class="new_remarks"></div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="form-label">
                                {{ __('Evidence') }} 
                            </label> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
