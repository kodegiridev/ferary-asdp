<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title edit_detail_title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input id="edit_detail_id" type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_kode" name="main[kode]" type="text" class="form-control" placeholder="Code Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Equipment') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_peralatan" name="main[peralatan]" type="text" class="form-control" placeholder="Compartment" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('State Status') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select 
                                    id="edit_detail_status_state"
                                    name="main[status_state]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                    <option value="0">Replace</option>
                                    <option value="1">Repair</option>
                                    <option value="2">Accepted</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="name" class="required form-label">
                                    {{ __('Plan Status') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <label for="name" class="required form-label">
                                    {{ __('Inspection Status') }}
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <select 
                                    id="edit_detail_status_plan"
                                    name="main[status_plan]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                <option value="1">Accepted</option>
                                <option value="2">Inspection Invitation</option>
                                <option value="3">Internal Progress Yard</option>
                                <option value="0">No Progress</option>
                                </select>
                            </div>
                             <div class="col-6">
                                <select 
                                id="edit_detail_status_inspeksi"
                                    name="main[status_inspeksi]"
                                    class="form-select form-select-solid"
                                    required
                                >
                                <option value="1">Accepted</option>
                                <option value="2">Inspection Invitation</option>
                                <option value="3">Internal Progress Yard</option>
                                <option value="0">No Progress</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="form-label">
                                {{ __('Evidence') }} 
                            </label> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Remarks Staging') }}
                            </label>
                        </div>
                        <div id="list_remarks_staging"></div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
