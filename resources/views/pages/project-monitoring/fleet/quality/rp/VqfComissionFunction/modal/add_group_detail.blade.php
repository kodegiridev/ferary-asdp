<div class="modal fade" tabindex="-1" id="add_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form class="form-milestone" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input type="hidden" name="visual_quality_fleet_id" value="{{ $visual_quality_fleet_id }}">
                <input type="hidden" name="periode_visual_quality_id" value="{{ $periode_quality_fleet_id }}">
                <input type="hidden" name="jenis_proyek" value="{{ $flag->jenis_project }}">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="kode" type="text" class="form-control" placeholder="Code" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Commissioning & Function Test') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="cnft" type="text" class="form-control" placeholder="Commissioning & Function Test" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tanggal') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="tanggal" type="date" class="form-control" placeholder="Pilih Tanggal" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Plan Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="status_plan" id="" class="form-control">
                                            @foreach ($mapping_status['plan'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Inspection Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="status_inspeksi" id="" class="form-control">
                                            @foreach ($mapping_status['inspection'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Remarks Staging') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <input name="remarks[]" type="text" class="form-control" placeholder="Remarks" required>
                            </div>
                            <div class="col-2">
                                <button onclick="add_remarks()" type="button" class="btn btn-primary">+</button>
                            </div>
                        </div>
                        <div class="new_remarks"></div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="form-label">
                                {{ __('Evidence') }} 
                            </label> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Notice') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="notice" type="text" class="form-control" placeholder="Notice" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
