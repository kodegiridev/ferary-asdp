<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title edit_detail_title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input type="hidden" name="visual_quality_fleet_id" value="{{ $visual_quality_fleet_id }}">
                <input type="hidden" name="periode_visual_quality_id" value="{{ $periode_quality_fleet_id }}">
                <input id="edit_detail_id" type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_kode" name="kode" type="text" class="form-control" placeholder="Code Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tank/Compartment') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="tank" type="text" class="form-control" placeholder="Tank/Compartment" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Leak Test Method') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="metode_leak_test" class="form-control select2">
                                    <option value="Air Test">Air Test</option>
                                    <option value="Vacuum Test">Vacuum Test</option>
                                    <option value="Chalk Test">Chalk Test</option>
                                    <option value="Hose Test">Hose Test</option>
                                    <option value="Hydro Test">Hydro Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="name" class="form-label">
                                    {{ __('Plan Status') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <label for="name" class="form-label">
                                    {{ __('Inspection Status') }}
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <select name="status_plan" class="form-control select2">
                                    <option value="1">Accepted</option>
                                    <option value="2">Inspection Invitation</option>
                                    <option value="3">Internal Progress Yard</option>
                                    <option value="0">No Progress</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <select name="status_inspeksi" class="form-control select2">
                                    <option value="1">Accepted</option>
                                    <option value="2">Re-Inspect / Partial</option>
                                    <option value="3">Yard Progress</option>
                                    <option value="0">No Progress</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="required form-label">
                                    {{ __('Remarks') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input name="remarks" type="text" class="form-control" placeholder="Remarks" required>
                                </div>
                            </div>
                            <div class="new_remarks"></div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label">
                                    {{ __('Evidence') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
