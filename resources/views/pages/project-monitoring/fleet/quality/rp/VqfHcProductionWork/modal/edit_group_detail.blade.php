<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title edit_detail_title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input id="edit_detail_id" type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Block Number') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_blok" name="main[blok]" type="text" class="form-control" placeholder="Code Component" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Plan Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_plan]" id="edit_status_plan" class="form-control">
                                            @foreach ($mapping_status['plan'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Inspection Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_inspeksi]" id="edit_status_inspeksi" class="form-control">
                                            @foreach ($mapping_status['inspection'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Ident Material Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_ident_material]" id="edit_status_ident_material" class="form-control">
                                            <option value="1">&times;</option>
                                            <option value="2">&check;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Ident Material Note *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <textarea name="main[catatan_ident_material]" id="edit_catatan_ident_material" class="form-control" rows="3" placeholder="Ident Material Note *"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Scantling Check Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_scantiling_check]" id="edit_status_scantiling_check" class="form-control">
                                            <option value="1">&times;</option>
                                            <option value="2">&check;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Scantling Check Note *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <textarea name="main[catatan_scantiling_check]" id="edit_catatan_scantiling_check" class="form-control" rows="3" placeholder="Scantling Check Note *"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Deformation Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_deformation]" id="edit_status_deformation" class="form-control">
                                            <option value="1">&times;</option>
                                            <option value="2">&check;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Deformation Note *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <textarea name="main[catatan_deformation]" id="edit_catatan_deformation" class="form-control" rows="3" placeholder="Deformation Note *"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Alignment Check Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_alignment_check]" id="edit_status_alignment_check" class="form-control">
                                            <option value="1">&times;</option>
                                            <option value="2">&check;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Alignment Check Note *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <textarea name="main[catatan_alignment_check]" id="edit_catatan_alignment_check" class="form-control" rows="3" placeholder="Alignment Check Note *"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Evidence') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a id="edit_detail_evidence" href="javascript:void(0)" target="_blank">Lihat Dokumen</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
