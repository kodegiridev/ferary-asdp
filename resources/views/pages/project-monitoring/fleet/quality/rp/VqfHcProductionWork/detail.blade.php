@extends('pages.project-monitoring.fleet.detail_quality', ['title' =>  $data['project']->nama_project, 'project_id' => $data['project']->id])

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
<div class="table-responsive" style="width: 100%">
    <table class="table">
        <tbody>
            <tr>
                <td width="25%">Nama Project</td>
                <td>: {{ $data['project']->nama_project }}</td>
            </tr>
            <tr>
                <td>Nilai Project</td>
                <td>: {{ toRupiah($data['project']->nilai_project) }}</td>
            </tr>
            <tr>
                <td>Last Update</td>
                <td>: {{ date('Y-m-d', strtotime($data['project']->updated_at)) }}</td>
            </tr>
            <tr>
                <td>Outstanding</td>
                <td>: </td>
            </tr>
            <tr>
                <td>Quality Plan</td>
                <td>: {{ toDecimal(0) }}%</td>
            </tr>
            <tr>
                <td>Quality Actual</td>
                <td>: {{ toDecimal(0) }}%</td>
            </tr>
            <tr>
                <td>Deviasi</td>
                <td>: {{ toDecimal(0)}}%</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="d-flex justify-content-start">
    <div style="width: 100%">
        <button type="button" class="btn btn-sm btn-primary m-1" onclick="add_drawing()">
            {{ __('Tambah Drawing') }}
        </button>
        <br>
        @if (!empty($data['periode']->document_drawing))
            <img src="{{ url('upload/'.$data['periode']->document_drawing) }}" class="lozad rounded w-100 mh-400px" alt="{{ url('upload/'.$data['periode']->document_drawing) }}" />
        @else
            <canvas id="canvas" style="width:100%; max-height:400px; border: 2px solid black"></canvas>
        @endif
    </div>
    <div class="p-5" style="width: 100%">
        <span class="text-primary"><strong>QUALITY INSPECTION PROGRESS</strong></span>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="bg-primary text-white">
                    <tr>
                        <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                        <th colspan="3" class="text-center align-middle" style="color:white !important">{{ __('Inspection') }}</th>
                    </tr>
                    <tr>
                        <th class="text-center align-middle" style="color:white !important">{{ __('Status') }}</th>
                        <th class="text-center align-middle" style="color:white !important">{{ __('Qty') }}</th>
                        <th class="text-center align-middle" style="color:white !important">{{ __('%') }}</th>
                    </tr>
                </thead>
                @php $total_qty = 0; $total_percentage = 0; @endphp
                <tbody>
                    @foreach ($data['quality_inspection'] as $item)
                        <tr>
                            <td class="text-center align-middle">{{ $loop->iteration }}</td>
                            <td class="text-center align-middle">{{ $item['status'] }}</td>
                            <td class="text-center align-middle {{ $item['qty_color'] }}">{{ $item['qty'] }}%</td>
                            <td class="text-center align-middle">{{ $item['percentage'] }}%</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" class="text-center align-middle"><strong>TOTAL</strong></td>
                        <td class="text-center align-middle">{{ $total_qty }}</td>
                        <td class="text-center align-middle">{{ $total_percentage }}%</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-content')
    <span class="text-primary"><strong>{{ strtoupper($data['detail']['flag']->name) }}</strong></span>
    <div class="buttons">
        <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#add_group_detail_modal">
            {{ __('+ Tambah Group') }}
        </button>
        <button type="button" class="btn btn-sm btn-warning m-1 btn-icon" onclick="duplicate_section()">
            <span class="svg-icon fs-1">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect opacity="0.5" x="7" y="2" width="14" height="16" rx="3" fill="currentColor"/>
                    <rect x="3" y="6" width="14" height="16" rx="3" fill="currentColor"/>
                </svg>                                                 
            </span>
        </button>
        <a href="{{ route('ipm.fleet.quality.detail_print', ['project' => $data['project']->id, 'periode' => $data['periode']->id]) }}" class="btn btn-sm btn-warning m-1 btn-icon">
            <span class="svg-icon fs-1">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer-fill" viewBox="0 0 16 16">
                    <path d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5zm6 8H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
                    <path d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                </svg>                                   
            </span>
        </a>
    </div>

    <?php $arr_jenis_data = [1 => "ASSEMBLY", 2 => "ERECTION"]; ?>

    <ul class="nav nav-tabs mt-2" id="myTab" role="tablist">
        @foreach ($arr_jenis_data as $jenis_data => $item)
        <li class="nav-item" role="presentation">
            <button class="nav-link tabdata {{ $jenis_data == 1 ? 'active' : '' }}" id="{{ $jenis_data }}-tab" data-jenisdata="{{ $jenis_data }}" data-bs-toggle="tab" data-bs-target="#{{ $jenis_data }}-tabcontent" type="button" role="tab" aria-controls="{{ $jenis_data }}-tabcontent" aria-selected="true">{{ $item }}</button>
        </li>
        @endforeach
    </ul>
    <div class="tab-content" id="myTabContent">
        @foreach ($arr_jenis_data as $jenis_data => $item)
        <div class="tab-pane fade {{ $jenis_data == 1 ? 'show active' : '' }}" id="{{ $jenis_data }}-tabcontent" role="tabpanel" aria-labelledby="{{ $jenis_data }}-tabcontent-tab">
            <div class="table-responsive mt-2 mb-5">
                <table class="table table-bordered datatable-basic">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Block Number') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Plan Status') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Status') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Remarks') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Evidence') }}</th>
                            <th class="text-center align-middle min-w-150px" style="color:white !important">{{ __('Aksi') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($data['detail']['datas']))
                            @foreach ($data['detail']['datas']->where('jenis_data', $jenis_data) as $item)
                                <?php 
                                    $remarks = [
                                        "ident_material"    => [],
                                        "scantling_check"   => [],
                                        "deformation"       => [],
                                        "alignment_check"    => []
                                    ];
                                    $remarks["ident_material"] = [
                                        "name"      => "Ident Material",
                                        "status"    => $item->status_ident_material,
                                        "catatan"   => $item->catatan_ident_material
                                    ];
                                    $remarks["scantling_check"] = [
                                        "name"      => "Scantling Check",
                                        "status"    => $item->status_scantiling_check,
                                        "catatan"   => $item->catatan_scantiling_check
                                    ];
                                    $remarks["deformation"] = [
                                        "name"      => "Deformation",
                                        "status"    => $item->status_deformation,
                                        "catatan"   => $item->catatan_deformation
                                    ];
                                    $remarks["alignment_check"] = [
                                        "name"      => "Alignment Check",
                                        "status"    => $item->status_alignment_check,
                                        "catatan"   => $item->catatan_alignment_check
                                    ];
                                    $json_remarks = json_encode($remarks); 
                                ?>
                                <tr>
                                    <th class="text-center align-middle">{{ $loop->iteration }}</th>
                                    <th class="text-center align-middle">{{ $item->blok }}</th>
                                    <th class="text-center align-middle">{{ $data["mapping_status"]["plan"][$item->status_plan] }}</th>
                                    <th class="text-center align-middle">{{ $data["mapping_status"]["inspection"][$item->status_inspeksi] }}</th>
                                    <th class="text-center align-middle">
                                        <button data-remarks="{{ $json_remarks }}" data-title="{{ $item->blok }}" type="button" class="btn btn-sm btn-primary m-1 btn-icon detail-remarks" data-bs-toggle="modal" data-bs-target="#remarks_detail_modal">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                            </svg>
                                        </button>
                                    </th>
                                    <th class="text-center align-middle">
                                        @if (!empty($item->evidence) && $item->evidence != '-')
                                        <a href="{{ asset('upload/project-monitoring-fleet/quality-detail/'.$item->evidence) }}" target="_blank">{{ $item->evidence }}</a>        
                                        @endif
                                    </th>
                                    <th class="text-center align-middle">
                                        <button data-item="{{ json_encode($item) }}" type="button" class="btn btn-sm btn-success m-1 btn-icon edit_detail" data-bs-toggle="modal" data-bs-target="#edit_group_detail_modal">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                            </svg>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-danger m-1 btn-icon" onclick="delete_group_code('{{ $item->id }}')">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                                <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                                <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                                            </svg>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-warning m-1 btn-icon" onclick="duplicate_code('{{ $item->id }}', '{{ $item->komponen }}')">
                                            <span class="svg-icon fs-1">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect opacity="0.5" x="7" y="2" width="14" height="16" rx="3" fill="currentColor"/>
                                                    <rect x="3" y="6" width="14" height="16" rx="3" fill="currentColor"/>
                                                </svg>                                                 
                                            </span>
                                        </button>
                                    </th>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.quality.index', $data['project']) }}">{{ __('Kembali') }}</a>
    </div>
    @include('pages.project-monitoring.fleet.quality.rp.VqfHcProductionWork.modal.add_group_detail', ['flag' => $data['detail']['flag'], 'periode_quality_fleet_id' => $data['periode']->id, 'visual_quality_fleet_id' => $data['visual']->id, 'mapping_status' => $data['mapping_status'] ])
    @include('pages.project-monitoring.fleet.quality.rp.VqfHcProductionWork.modal.edit_group_detail', ['flag' => $data['detail']['flag'], 'periode_quality_fleet_id' => $data['periode']->id, 'visual_quality_fleet_id' => $data['visual']->id, 'mapping_status' => $data['mapping_status'] ])

    @include('pages.project-monitoring.fleet.quality.modals.add_document_drawing')
    @include('pages.project-monitoring.fleet.quality.modals.remarks_detail')
    
    @include('pages.project-monitoring.fleet.quality.modals.duplicate_data_per_code', ['project_id' => $data['project']->id, 'flag' => $data['detail']['flag'], 'periode_id' => $data['periode']->id, 'vqf_id' => $data['visual']->id ])
    @include('pages.project-monitoring.fleet.quality.modals.duplicate_data_per_section', ['project_id' => $data['project']->id, 'flag' => $data['detail']['flag'], 'periode_id' => $data['periode']->id, 'vqf_id' => $data['visual']->id ])

    <form id="form-delete-visual-quality-fleet" action="{{ route('ipm.fleet.quality.delete_group_remarks', ['project' => $data['project']->id, 'periode' => $data['periode']->id]) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="hidden" name="flag" value="{{ json_encode($data['detail']['flag']) }}">
        <input type="hidden" name="id" id="delete_visual_quality_fleet_id">
    </form>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        $(document).ready(function() {
            checkSession();
            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                prefix: "Rp. "
            }).mask(".nilai_project");

            $(".tabdata").on('click', function() {
                let jenis_data = $(this).data('jenisdata');
                $("#add_jenis_data").val(jenis_data);
                $("#jenis_data_from").val(jenis_data);
            })

            $('.datatable-basic').DataTable({
                proccesing: true,
                serverSide: false,
                order: [],
                'columnDefs'        : [
                    { 
                        'searchable'    : false, 
                        'targets'       : [0,4,6] 
                    },
                ]
            });
        });

        function add_drawing() {
            $("#periode_id").val({{ $data['periode']->id }});
            $("#add_document_drawing_modal").modal('show');
        }

        let jenis_project   = `{{ $data['detail']['flag']->jenis_project }}`;
        let model           = `{{ $data['detail']['flag']->model }}`;

        $(".edit_detail").on('click', function() {
            let item = $(this).data('item');

            $(".edit_detail_title").text(`Edit Detail ${item.blok}`);
            $("#edit_detail_id").val(item.id);
            $("#edit_detail_blok").val(item.blok);
            $("#edit_status_inspeksi").val(item.status_inspeksi);
            $("#edit_status_plan").val(item.status_plan);
            
            let url_evidence = `{{ asset('upload/project-monitoring-fleet/quality-detail/:id') }}`;
                url_evidence = url_evidence.replace(":id", item.evidence);
            if (item.evidence != "-") {
                $("#edit_detail_evidence").attr("href", url_evidence);
            }
            
            let status_ident_material = item.status_ident_material;
            let catatan_ident_material = item.catatan_ident_material;
            $("#edit_status_ident_material").val(status_ident_material);
            $("#edit_catatan_ident_material").val(catatan_ident_material);

            let status_scantiling_check = item.status_scantiling_check;
            if (status_scantiling_check == 0) { status_scantiling_check = 1; }
            let catatan_scantiling_check = item.catatan_scantiling_check;
            $("#edit_status_scantiling_check").val(status_scantiling_check);
            $("#edit_catatan_scantiling_check").val(catatan_scantiling_check);

            let status_deformation = item.status_deformation;
            if (status_deformation == 0) { status_deformation = 1; }
            let catatan_deformation = item.catatan_deformation;
            $("#edit_status_deformation").val(status_deformation);
            $("#edit_catatan_deformation").val(catatan_deformation);

            let status_alignment_check = item.status_alignment_check;
            if (status_alignment_check == 0) { status_alignment_check = 1; }
            let catatan_alignment_check = item.catatan_alignment_check;
            $("#edit_status_alignment_check").val(status_alignment_check);
            $("#edit_catatan_alignment_check").val(catatan_alignment_check);
        });

        let mapping_status = {!! json_encode($data['mapping_status']) !!}
       
        $(".detail-remarks").on('click', function() {
            let remarks_detail = $(this).data('remarks');
            let title = $(this).data('title');
            
            $("#remarks_detail_title").text('Remarks ' + title)
            let html_detail = ``;
            $.each(remarks_detail, function(i, item) {
                let status = '-';
                if (item.status == 1) {
                    status = '&times;';
                } else if (item.status == 2) {
                    status = '&check;';
                }
                html_detail += `
                    <tr>
                        <td>${item.name}</td>
                        <td>${status}</td>
                        <td>${item.catatan}</td>
                    </tr>
                `;
            })

            let html = `
                <div class="remarks-data mt-3">
                    <table class="table table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Item Check') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Status') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Note') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${html_detail}
                        </tbody>
                    </table>
                </div>
            `;
            $("#list-remarks").html(html)
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}_project`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }

        function delete_group_code(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Data Group Code ini?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#delete_visual_quality_fleet_id").val(id);
                    $("#form-delete-visual-quality-fleet").submit();
                }
            })
        }

        let mapping_section = {!! json_encode($arr_jenis_data) !!}
        function duplicate_section() {
            let section_source = Number($("#jenis_data_from").val());
            console.log(mapping_section, section_source);

            let option_html = "";
            let jenis_data_from_section_name = "";
            $.each(mapping_section, function(jenis_data, section_name) {
                if (jenis_data == section_source) {
                    jenis_data_from_section_name = section_name.toUpperCase();
                } else {
                    option_html += `
                        <div class="col-6 mt-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="jenis_data_to[${jenis_data}]" id="${section_name}">
                                <label class="form-check-label" for="${section_name}">
                                    <p style="color:black">${section_name}</p>
                                </label>
                            </div>
                        </div>
                    `;
                }
            });

            $("#jenis_data_from_section_name").text(jenis_data_from_section_name);
            $("#list_tab_tujuan").html(option_html);
            $("#duplicate_per_section_modal").modal('show');
        }

        function duplicate_code(id, code_name) {
            $("#vqf_code_id").val(id);
            $(".duplicate-per-code-name").text(code_name);

            $("#duplicate_per_code_modal").modal('show');
        }
    </script>
@endpush
