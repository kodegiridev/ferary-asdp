@extends('pages.project-monitoring.fleet.detail_quality', ['title' =>  $data['project']->nama_project, 'project_id' => $data['project']->id])

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
<div class="table-responsive" style="width: 100%">
    <table class="table">
        <tbody>
            <tr>
                <td width="25%">Nama Project</td>
                <td>: {{ $data['project']->nama_project }}</td>
            </tr>
            <tr>
                <td>Nilai Project</td>
                <td>: {{ toRupiah($data['project']->nilai_project) }}</td>
            </tr>
            <tr>
                <td>Last Update</td>
                <td>: {{ date('Y-m-d', strtotime($data['project']->updated_at)) }}</td>
            </tr>
            <tr>
                <td>Outstanding</td>
                <td>: </td>
            </tr>
            <tr>
                <td>Quality Plan</td>
                <td>: {{ toDecimal($data['detail']['total_plan']) }}%</td>
            </tr>
            <tr>
                <td>Quality Actual</td>
                <td>: {{ toDecimal($data['detail']['total_inspeksi']) }}%</td>
            </tr>
            <tr>
                <td>Deviasi</td>
                <td>: {{ toDecimal($data['detail']['total_deviasi'])}}%</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="d-flex justify-content-start">
    <div style="width: 100%">
        <button type="button" class="btn btn-sm btn-primary m-1" onclick="add_drawing()">
            {{ __('Tambah Drawing') }}
        </button>
        <br>
        @if (!empty($data['periode']->document_drawing))
            <img src="{{ url('upload/'.$data['periode']->document_drawing) }}" class="lozad rounded w-100 mh-400px" alt="{{ url('upload/'.$data['periode']->document_drawing) }}" />
        @else
            <canvas id="canvas" style="width:100%; max-height:400px; border: 2px solid black"></canvas>
        @endif
    </div>
    <div class="p-5" style="width: 100%">
        <span class="text-primary"><strong>QUALITY INSPECTION PROGRESS</strong></span>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="bg-primary text-white">
                    <tr>
                        <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                        <th colspan="3" class="text-center align-middle" style="color:white !important">{{ __('Inspection') }}</th>
                    </tr>
                    <tr>
                        <th class="text-center align-middle" style="color:white !important">{{ __('Status') }}</th>
                        <th class="text-center align-middle" style="color:white !important">{{ __('Qty') }}</th>
                        <th class="text-center align-middle" style="color:white !important">{{ __('%') }}</th>
                    </tr>
                </thead>
                @php $total_qty = 0; $total_percentage = 0; @endphp
                <tbody>
                    @foreach ($data['detail']['inspeksi']['inspection'] as $item)
                        <tr>
                            <td class="text-center align-middle">{{ $loop->iteration }}</td>
                            <td class="text-center align-middle">{{ $item['name'] }}</td>
                            <td class="text-center align-middle">{{ $item['quantity'] }}</td>
                            <td class="text-center align-middle">{{ 0 }}%</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" class="text-center align-middle"><strong>TOTAL</strong></td>
                        <td class="text-center align-middle">{{ $data['detail']['inspeksi']['total_quantity'] }}</td>
                        <td class="text-center align-middle">{{ $total_percentage }}%</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-content')
    <span class="text-primary"><strong>{{ strtoupper($data['detail']['flag']->name) }}</strong></span>
    <div class="buttons">
        <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#add_group_detail_modal">
            {{ __('+ Tambah Group') }}
        </button>
        <a href="{{ route('ipm.fleet.quality.detail_print', ['project' => $data['project']->id, 'periode' => $data['periode']->id]) }}" class="btn btn-sm btn-warning m-1 btn-icon">
            <span class="svg-icon fs-1">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer-fill" viewBox="0 0 16 16">
                    <path d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5zm6 8H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
                    <path d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                </svg>
            </span>
        </a>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="visual-quality" role="tabpanel" aria-labelledby="visual-quality-tab">
            <div class="table-responsive mt-2 mb-5">
                <table class="table table-bordered datatable-basic">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Code') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Component') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Remark') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Evidence') }}</th>
                            <th class="text-center align-middle min-w-150px" style="color:white !important">{{ __('Aksi') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($data['detail']['datas']))
                            @foreach ($data['detail']['datas'] as $item)
                                <?php $json_remarks = json_encode($item->remarks); ?>
                                <tr>
                                    <th class="text-center align-middle">{{ $loop->iteration }}</th>
                                    <th class="text-center align-middle">{{ $item->kode }}</th>
                                    <th class="text-center align-middle">{{ $item->komponen }}</th>
                                    <th class="text-center align-middle">
                                        <button data-remarks="{{ $json_remarks }}" data-title="{{ $item->komponen }}" type="button" class="btn btn-sm btn-primary m-1 btn-icon detail-remarks" data-bs-toggle="modal" data-bs-target="#remarks_detail_modal"">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                            </svg>
                                        </button>
                                    </th>
                                    <th class="text-center align-middle">
                                        @if (!empty($item->evidence) && $item->evidence != '-')
                                        <a href="{{ asset('upload/project-monitoring-fleet/quality-detail/'.$item->evidence) }}" target="_blank">{{ $item->evidence }}</a>
                                        @endif
                                    </th>
                                    <th class="text-center align-middle">
                                        <button data-item="{{ json_encode($item) }}" type="button" class="btn btn-sm btn-success m-1 btn-icon edit_detail" data-bs-toggle="modal" data-bs-target="#edit_group_detail_modal">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                            </svg>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-danger m-1 btn-icon" onclick="delete_group_code('{{ $item->id }}')">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                                <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                                <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                                            </svg>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-warning m-1 btn-icon" onclick="duplicate_code('{{ $item->id }}', '{{ $item->komponen }}')">
                                            <span class="svg-icon fs-1">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect opacity="0.5" x="7" y="2" width="14" height="16" rx="3" fill="currentColor"/>
                                                    <rect x="3" y="6" width="14" height="16" rx="3" fill="currentColor"/>
                                                </svg>
                                            </span>
                                        </button>
                                    </th>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.quality.index', $data['project']) }}">{{ __('Kembali') }}</a>
    </div>
    @include('pages.project-monitoring.fleet.quality.nb.VqfMachineryOutfiting.modal.add_group_detail', ['flag' => $data['detail']['flag'], 'periode_quality_fleet_id' => $data['periode']->id, 'visual_quality_fleet_id' => $data['visual']->id ])
    @include('pages.project-monitoring.fleet.quality.nb.VqfMachineryOutfiting.modal.edit_group_detail', ['flag' => $data['detail']['flag'], 'periode_quality_fleet_id' => $data['periode']->id, 'visual_quality_fleet_id' => $data['visual']->id ])

    @include('pages.project-monitoring.fleet.quality.modals.add_document_drawing')
    @include('pages.project-monitoring.fleet.quality.modals.remarks_detail')

    @include('pages.project-monitoring.fleet.quality.modals.duplicate_data_per_code', ['project_id' => $data['project']->id, 'flag' => $data['detail']['flag'], 'periode_id' => $data['periode']->id, 'vqf_id' => $data['visual']->id ])
    <form id="form-delete-visual-quality-fleet" action="{{ route('ipm.fleet.quality.delete_group_remarks', ['project' => $data['project']->id, 'periode' => $data['periode']->id]) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="hidden" name="flag" value="{{ json_encode($data['detail']['flag']) }}">
        <input type="hidden" name="id" id="delete_visual_quality_fleet_id">
    </form>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        $(document).ready(function() {
            checkSession();
            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                prefix: "Rp. "
            }).mask(".nilai_project");

            $('.datatable-basic').DataTable({
                proccesing: true,
                serverSide: false,
                order: [],
                'columnDefs'        : [
                    {
                        'searchable'    : false,
                        'targets'       : [0,3,5]
                    },
                ]
            });
        });

        function add_drawing() {
            $("#periode_id").val({{ $data['periode']->id }});
            $("#add_document_drawing_modal").modal('show');
        }

        let jenis_project   = `{{ $data['detail']['flag']->jenis_project }}`;
        let model           = `{{ $data['detail']['flag']->model }}`;
        function add_remarks() {
            let item_id = jenis_project + "-" + model + "-" + Math.round(Math.random() * 9999999);
            let html = `
                <div id="item-${item_id}" class="row mt-2">
                    <div class="col-10">
                        <input name="detail[remarks][]" type="text" class="form-control" placeholder="Remarks" required>
                    </div>
                    <div class="col-2">
                        <button onclick="delete_row('${item_id}')" type="button" class="btn btn-danger btn-icon">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                            </svg>
                        </button>
                    </div>
                </div>
            `;
            $(".new_remarks").append(html);
        }

        $(".edit_detail").on('click', function() {
            let item = $(this).data('item');

            $(".edit_detail_title").text(`Edit Detail ${item.komponen}`);
            $("#edit_detail_id").val(item.id);
            $("#edit_detail_kode").val(item.kode);
            $("#edit_detail_komponen").val(item.komponen);

            let url_evidence = `{{ asset('upload/project-monitoring-fleet/quality-detail/:id') }}`;
                url_evidence = url_evidence.replace(":id", item.evidence);
            if (item.evidence != "-") {
                $("#edit_detail_evidence").attr("href", url_evidence);
            }

            $("#list_code_component").empty();
            $.each(item.remarks, function(i, rmk) {

                console.log(rmk);
                existing_row(item.komponen, rmk.remarks, rmk.tanggal, rmk.catatan_inspeksi, rmk.status_inspeksi, rmk.status_plan);
            });


        });

        let mapping_status = {!! json_encode($data['mapping_status']) !!}

        $(".detail-remarks").on('click', function() {
            let remarks_detail = $(this).data('remarks');
            let title = $(this).data('title');
            console.log(mapping_status);

            $("#remarks_detail_title").text('Remarks ' + title)
            let html = ``;
            if (remarks_detail.length > 0) {
                $.each(remarks_detail, function(i, item) {
                    console.log(item);
                    let plan_status = mapping_status.plan[item.status_plan]
                    let inspection_status = mapping_status.inspection[item.status_inspeksi]
                    let tanggal = item.tanggal == '0000-00-00' ? '' : item.tanggal
                    let catatan_inspeksi = item.catatan_inspeksi == '-' ? '' : item.catatan_inspeksi

                    html += `
                        <div class="remarks-data mt-3">
                            <span class="text-primary"><strong>Code Component :  ${item.remarks}</strong></span>
                            <table class="table table-bordered">
                                <thead class="bg-primary text-white">
                                    <tr>
                                        <th class="text-center align-middle" style="color:white !important">{{ __('Plan Status') }}</th>
                                        <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Status') }}</th>
                                        <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Result') }}</th>
                                        <th class="text-center align-middle" style="color:white !important">{{ __('Date') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center align-middle">${plan_status}</td>
                                        <td class="text-center align-middle">${inspection_status}</td>
                                        <td class="text-center align-middle">${catatan_inspeksi}</td>
                                        <td class="text-center align-middle">${tanggal}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    `;
                })
            }
            $("#list-remarks").html(html)
        });

        function existing_row(component, remarks, tanggal, catatan_inspeksi, status_inspeksi, status_plan) {
            let item_id = Math.round(Math.random() * 9999999);
            let input_option_inspection = ``;
            let input_option_plan       = ``;
            $.each(mapping_status.plan, function(i, v) {
                let selected = status_plan == i ? "selected" : "";
                input_option_plan += `
                    <option ${selected} value="${i}">${v}</option>
                `;
            })
            $.each(mapping_status.inspection, function(i, v) {
                let selected = status_inspeksi == i ? "selected" : "";
                input_option_inspection += `
                    <option ${selected} value="${i}">${v}</option>
                `;
            })
            let html = `
                <div id="item-${item_id}" class="row mt-5">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Code Component : ${component}') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input required name="detail[remarks][]" type="text" class="form-control" placeholder="code" value="${remarks}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm btn-danger m-1 btn-icon mb-2" style="float:right" onclick="delete_row('${item_id}')">
                                <span class="svg-icon fs-1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                        <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                        <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                                    </svg>
                                </span>
                            </button>
                            <textarea required name="detail[catatan_inspeksi][]" id="" class="form-control" rows="3" placeholder="Inspection Status *">${catatan_inspeksi}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Date') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input required name="detail[tanggal][]" type="date" class="form-control" placeholder="code" value="${tanggal}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Plan Status') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="detail[status_plan][]" id="" class="form-control">
                                        ${input_option_plan}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Inspection Status') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="detail[status_inspeksi][]" id="" class="form-control">
                                        ${input_option_inspection}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            $("#list_code_component").append(html);
        }

        function add_row() {
            let item_id = Math.round(Math.random() * 9999999);
            let input_option_inspection = ``;
            let input_option_plan       = ``;
            $.each(mapping_status.plan, function(i, v) {
                input_option_plan += `
                    <option value="${i}">${v}</option>
                `;
            })
            $.each(mapping_status.inspection, function(i, v) {
                input_option_inspection += `
                    <option value="${i}">${v}</option>
                `;
            })
            let html = `
                <div id="item-${item_id}" class="row mt-5">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input required name="detail[remarks][]" type="text" class="form-control" placeholder="code">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-sm btn-danger m-1 btn-icon mb-2" style="float:right" onclick="delete_row('${item_id}')">
                                <span class="svg-icon fs-1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                        <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                        <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                                    </svg>
                                </span>
                            </button>
                            <textarea required name="detail[catatan_inspeksi][]" id="" class="form-control" rows="3" placeholder="Inspection Status *"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Date') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input required name="detail[tanggal][]" type="date" class="form-control" placeholder="code" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Plan Status') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="detail[status_plan][]" id="" class="form-control">
                                        ${input_option_plan}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label mb-2">
                                    {{ __('Inspection Status') }}
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="detail[status_inspeksi][]" id="" class="form-control">
                                        ${input_option_inspection}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            $("#list_code_component").append(html);
        }

        function delete_row(id) {
            $("#item-" + id).remove();
        }

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}_project`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }

        function delete_group_code(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Data Group Code ini?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#delete_visual_quality_fleet_id").val(id);
                    $("#form-delete-visual-quality-fleet").submit();
                }
            })
        }

        function duplicate_code(id, code_name) {
            $("#vqf_code_id").val(id);
            $(".duplicate-per-code-name").text(code_name);

            $("#duplicate_per_code_modal").modal('show');
        }
    </script>
@endpush
