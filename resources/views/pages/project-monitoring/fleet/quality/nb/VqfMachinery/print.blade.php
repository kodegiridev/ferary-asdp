<!DOCTYPE html>
<html>

<head>
    <title>Visual Quality Fleet | {{ $data['project']->nama_project }}</title>
    <style>
        @page {
            size: A4 landscape;
        }

        .title {
            text-align: left;
            margin-bottom: 10px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            text-align: center;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left;
        }

        .date-place {
            text-align: left;
            font-style: bold;
            margin-top: 10px;
            padding: 0;
        }

        .text-success {
            color: #50cd89 !important;
        }

        /* Signature box styling */
        .signature-box {
            border-top: 1px solid black;
            padding: 8px;
            width: 200px;
            text-align: center;
        }

        .signature-role {
            font-style: bold;
        }

        .signature-name {
            font-weight: bold;
            text-decoration: underline;
        }

        .signature-container {
            width: 100%;
        }

        .signature-content {
            float: right;
        }

        .borderless {
            border: 0;
        }
    </style>
</head>

<body>
    
    {{-- <img src="data:image/png;base64,{{ base64_encode($imageLogo) }}" alt="Logo Ferary"  style="width:150px; height:auto"> --}}
    <img alt="Logo" src="{{ asset('logo.png') }}" style="width:150px; height:auto" />
    <div class="title">
        <h3>Visual Quality Fleet | {{ $data['project']->nama_project }}</h3>
    </div>
    
    <h5><strong>{{ strtoupper($data['detail']['flag']->name) }}</strong><</h5>
    <table style="width: 100%">
        <tr>
            <td class="borderless">
                <table style="width: 100%">
                    <thead>
                        <tr>
                            <td class="borderless" rowspan="2" style="text-align: center">NO</td>
                            <td class="borderless" colspan="3" style="text-align: center">INSPECTION</td>
                        </tr>
                        <tr>
                            <td class="borderless" style="text-align: center">STATUS</td>
                            <td class="borderless" style="text-align: center">QTY</td>
                            <td class="borderless" style="text-align: center">%</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="borderless" style="text-align: center">1</td>
                            <td class="borderless" style="text-align: center">ACCEPTED</td>
                            <td class="borderless" style="text-align: center; background-color:#008200;color:white">3</td>
                            <td class="borderless" style="text-align: center">37.50%</td>
                        </tr>
                        <tr>
                            <td class="borderless" style="text-align: center">2</td>
                            <td class="borderless" style="text-align: center">RE-INSPECT / PARTIAL</td>
                            <td class="borderless" style="text-align: center; background-color:#ffff00;color:black">4</td>
                            <td class="borderless" style="text-align: center">25.00%</td>
                        </tr>
                        <tr>
                            <td class="borderless" style="text-align: center">3</td>
                            <td class="borderless" style="text-align: center">YARD PROGRESS</td>
                            <td class="borderless" style="text-align: center; background-color:#ff0000;color:white">1</td>
                            <td class="borderless" style="text-align: center">3.13%</td>
                        </tr>
                        <tr>
                            <td class="borderless" style="text-align: center">4</td>
                            <td class="borderless" style="text-align: center">NO PROGRESS</td>
                            <td class="borderless" style="text-align: center;">0</td>
                            <td class="borderless" style="text-align: center">0.00%</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="borderless"></td>
                            <td class="borderless" style="text-align: center;">TOTAL</td>
                            <td class="borderless" style="text-align: center;">8</td>
                            <td class="borderless" style="text-align: center;">65.63%</td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>

        @if (!empty($data['detail']['datas']))
            @foreach ($data['detail']['datas'] as $item)
                <table style="margin-top: 20px;margin-bottom: 10px">
                    <thead>
                        <tr>
                            <th style="border-right:0">Group : {{ $item->kode }}</th>
                            <th style="border-left:0; border-right:0">Equipment : {{ $item->peralatan }}</th>
                            <th style="border-left:0">
                                Evidence : 
                                @if (!empty($item->evidence) && $item->evidence != '-')
                                    <a href="{{ asset('upload/project-monitoring-fleet/quality-detail/'.$item->evidence) }}" target="_blank">{{ $item->evidence }}</a>        
                                @endif
                            </th>
                        </tr>
                        <tr>
                            <th style="border-right:0" colspan="2">Plan Status : {{ $data['mapping_status']['plan'][$item->status_plan] }}</th>
                            <th style="border-left:0;">Inspection Status : {{ $data['mapping_status']['inspection'][$item->status_inspeksi] }}</th>
                        </tr>
                        <tr>
                            <th>Item Check</th>
                            <th>Status</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($item->remarks))
                            @foreach ($item->remarks as $rmk)
                                <?php $status = "-"; 
                                    if ($rmk->status == 1) {
                                        $status = 'x';
                                    } else if ($rmk->status == 2) {
                                        $status = 'v';
                                    }
                                ?>
                                <tr>
                                    <td>{{ $rmk->nama_staging }}</td>
                                    <td>{!! $status !!}</td>
                                    <td>{{ $rmk->catatan }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            @endforeach
        @endif
</body>

</html>
