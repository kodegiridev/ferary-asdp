<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title edit_detail_title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                <input id="edit_detail_id" type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Code') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_kode" name="main[kode]" type="text" class="form-control" placeholder="Code" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Commissioning & Function Test') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_cnft" name="main[cnft]" type="text" class="form-control" placeholder="Commissioning & Function Test" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tanggal') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_tanggal" name="main[tanggal]" type="date" class="form-control" placeholder="Pilih Tanggal" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Plan Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_plan]" id="edit_status_plan" class="form-control">
                                            @foreach ($mapping_status['plan'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="row">
                                    <label for="name" class="form-label">
                                        {{ __('Inspection Status *') }} 
                                    </label> 
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="main[status_inspeksi]" id="edit_status_inspeksi" class="form-control">
                                            @foreach ($mapping_status['inspection'] as $key => $item)
                                            <option value="{{ $key }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="form-label">
                                {{ __('Evidence') }} 
                            </label> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a id="edit_detail_evidence" href="javascript:void(0)" target="_blank">Lihat Dokumen</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Notice') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="edit_detail_notice" name="main[notice]" type="text" class="form-control" placeholder="Notice" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Remarks Staging') }}
                            </label>
                        </div>
                        <div id="list_remarks_staging"></div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
