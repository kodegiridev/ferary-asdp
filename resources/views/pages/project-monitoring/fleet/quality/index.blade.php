@extends('pages.project-monitoring.fleet.detail_quality')

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .btn-xs {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }

        /* Dropdown Button */
        /* .dropbtn {
            background-color: #3498DB;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        } */

        /* Dropdown button on hover & focus */
        .dropbtn:hover, .dropbtn:focus {
            background-color: #2980B9;
        }

        /* The container <div> - needed to position the dropdown content */
        .dropdown {
            position: relative;
            display: inline-block;
        }

        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 80px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        /* Change color of dropdown links on hover */
        .dropdown-content a:hover {background-color: #ddd;}

        /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
        .show {display:block;}
    </style>
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-project')
<div class="d-flex justify-content-start">
    <div class="table-responsive" style="width: 100%">
        <table class="table">
            <tbody>
                <tr>
                    <td width="25%">Nama Project</td>
                    <td>: {{$data['project']['nama_project']}}</td>
                </tr>
                <tr>
                    <td>Nilai Project</td>
                    <td>: {{ toRupiah($data['project']['nilai_project']) }}</td>
                </tr>
                <tr>
                    <td>Tanggal Mulai</td>
                    <td>: {{ $data['basic_data']['mulai_kontrak'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Tanggal Selesai</td>
                    <td>: {{ $data['basic_data']['selesai_kontrak'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Total Quality Plan</td>
                    <td>: {{ toDecimal($data['summary_plan']) }}%</td>
                </tr>
                <tr>
                    <td>Total Quality Rate</td>
                    <td>: {{ toDecimal($data['summary_actual']) }}%</td>
                </tr>
                <tr>
                    <td>Total Deviasi</td>
                    <td>: {{ toDecimal($data['summary_deviasi'])}}%</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="width: 100%">
        <button type="button" class="btn btn-sm btn-primary m-1" id="drawing">
            {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
            {{ __('Tambah Drawing') }}
        </button>
        <br>
        @if (!empty($data['drawing']['drawing']))
            <img src="{{ $data['drawing']['drawing'] }}" class="lozad rounded w-100 mh-400px" alt="{{ $data['drawing']['deskripsi'] }}" />
        @else
            <canvas id="canvas" style="width:100%; max-height:400px; border: 2px solid black"></canvas>
        @endif
    </div>
</div>
@endsection

@section('page-content')
    <span class="text-primary"><strong>MILESTONE QUALITY PROGRESS</strong></span>
    <div class="table-responsive mt-2 mb-5">
        <table class="table table-bordered">
            <thead class="bg-primary text-white">
                <tr>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Kick Off Meeting') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('First Steel Culting') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Keel Laying') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Engine Loading') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Launching') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Sea Trial') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Delivery') }}</th>
                    <th class="text-center align-middle" style="color:white !important">{{ __('Aksi') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['kick_off_meeting'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['first_steel_culting'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['keel_laying'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['engine_loading'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['launching'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['sea_trial'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">{{ toPercent($data['milestone_progress']['delivery'] ?? 0, 0) }}</th>
                    <th class="text-center align-middle">
                        <a href="{{ route('ipm.fleet.quality.milestone.index', $data['project']['id']) }}" class="btn btn-icon btn-success btn-sm m-1">
                            <span class="svg-icon fs-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                            </span>
                        </a>
                    </th>
                </tr>
            </tbody>
        </table>
    </div>

    <ul class="nav nav-tabs mt-2" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="visual-quality-tab" data-bs-toggle="tab" data-bs-target="#visual-quality" type="button" role="tab" aria-controls="visual-quality" aria-selected="true">Visual Quality</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="iar-tab" data-bs-toggle="tab" data-bs-target="#iar" type="button" role="tab" aria-controls="visual-quality" aria-selected="false">Inspection Acceptance Rate</button>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="visual-quality" role="tabpanel" aria-labelledby="visual-quality-tab">
            <div class="mt-3 mb-3">
                <span class="text-primary"><strong>VISUAL QUALITY PROGRESS (PROGRESS: {{ $data['current_periode'] }})</strong></span>
            </div>
            <div class="buttons">
                <button type="button" class="btn btn-sm btn-primary m-1" data-bs-toggle="modal" data-bs-target="#data_group_add_modal">
                    {{ __('+ Tambah Group') }}
                </button>
                <button type="button" class="btn btn-sm btn-success m-1 btn-icon">
                    <span class="svg-icon fs-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer-fill" viewBox="0 0 16 16">
                            <path d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5zm6 8H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
                            <path d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                        </svg>                                   
                    </span>
                </button>
                <div class="dropdown" style="float: right;">
                    <button onclick="myFunction()" class="btn btn-sm btn-primary m-1 dropbtn">Periode</button>
                    <div id="myDropdown" class="dropdown-content">
                        @for($i=1; $i <= $data['max_periode']; $i++)
                            <a href="{{ route('ipm.fleet.quality.index', $data['project']->id, ['periode_id' => $i]) }}">{{$i}}</a>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="table-responsive mt-2 mb-5">
                <table class="table table-bordered">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('Group') }}</th>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('Last Updated') }}</th>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('Bobot') }}</th>
                            <th colspan="3" class="text-center align-middle" style="color:white !important">{{ __('Quality Progress (%)') }}</th>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('Status Progress') }}</th>
                            <th rowspan="2" class="text-center align-middle min-w-150px" style="color:white !important">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Plan') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Actual') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Deviasi') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_bobot = 100; ?>
                        @if (!empty($data['data_group']))
                            @foreach ($data['data_group'] as $visual_quality)
                            <?php $total_bobot = $total_bobot - $visual_quality->bobot; ?>
                            <tr>
                                <th class="text-center align-middle">{{ $loop->iteration }}</th>
                                <th class="text-center align-middle"><strong>{{ $visual_quality->nama_grup }}</strong></th>
                                <th class="text-center align-middle">{{ $visual_quality->updated_at }}</th>
                                <th class="text-center align-middle">{{ !empty($visual_quality->bobot) ? $visual_quality->bobot : "0" }}%</th>
                                <th class="text-center align-middle">{{toDecimal($visual_quality->plan)}}%</th>
                                <th class="text-center align-middle">{{toDecimal($visual_quality->actual)}}%</th>
                                <th class="text-center align-middle">{{toDecimal($visual_quality->deviasi)}}%</th>
                                <th class="text-center align-middle">{{$visual_quality->progres_status}}</th>
                                <th class="text-center align-middle">
                                    <button type="button" class="btn btn-sm btn-primary m-1 btn-icon" onclick="groupPeriodeModal(`{{ $visual_quality->id }}`, `{{ json_encode($visual_quality->periode) }}`)" data-bs-toggle="modal" data-bs-target="#periode_per_group_modal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                            <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                        </svg>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-success m-1 btn-icon" onclick="groupEditModal('{{ $visual_quality->nama_grup }}', '{{ $visual_quality->id }}', '{{ $visual_quality->bobot }}')" data-bs-toggle="modal" data-bs-target="#data_group_edit_modal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger m-1 btn-icon" onclick="groupDelete('{{ $visual_quality->id }}')">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                                            <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                                            <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                                        </svg>
                                    </button>
                                </th>
                            </tr>
                            @endforeach
                        @endif
    
                        {{-- @if (count($data['project_data_group']) > 0)
                            @foreach ($data['project_data_group'] as $main_project)
                                @foreach ($main_project["sub_project"] as $item)
                                <?php //$visual_quality = []; if (!empty($data['data_group'])) { $visual_quality = $data['data_group']->where('jenis_form', $item->id)->first(); } ?>
                                
                                @endforeach
                            @endforeach
                        @endif --}}
                        <tr>
                            <th class="text-center align-middle"></th>
                            <th class="text-center align-middle"><strong>Progress Summary</strong></th>
                            <th class="text-center align-middle"></th>
                            <th class="text-center align-middle">{{ toPercent($data['summary_total']) }}</th>
                            <th class="text-center align-middle">{{ toDecimal($data['summary_plan']) }}%</th>
                            <th class="text-center align-middle">{{ toDecimal($data['summary_actual']) }}%</th>
                            <th class="text-center align-middle">{{ toDecimal($data['summary_deviasi']) }}%</th>
                            <th class="text-center align-middle"></th>
                            <th class="text-center align-middle"></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane fade" id="iar" role="tabpanel" aria-labelledby="iar-tab">
            <div class="table-responsive mt-2 mb-5">
                <table class="table table-bordered">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('No') }}</th>
                            <th rowspan="2" class="text-center align-middle" style="color:white !important">{{ __('Group') }}</th>
                            <th colspan="3" class="text-center align-middle" style="color:white !important">{{ __('Accepted By') }}</th>
                            <th rowspan="2" class="text-center align-middle min-w-150px" style="color:white !important">{{ __('Aksi') }}</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Class') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Owner') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Both') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($data['data_group']))
                            @foreach ($data['data_group'] as $visual_quality)
                            <tr>
                                <th class="text-center align-middle">{{ $loop->iteration }}</th>
                                <th class="text-center align-middle"><strong>{{ $visual_quality->nama_grup }}</strong></th>
                                <th class="text-center align-middle">{{ $visual_quality->class }}</th>
                                <th class="text-center align-middle">{{ $visual_quality->owner }}</th>
                                <th class="text-center align-middle">{{ $visual_quality->both }}</th>
                                <th class="text-center align-middle">
                                    <a href="{{ route('ipm.fleet.quality.inspection.index', ['project' => $data['project'], 'periode' => $visual_quality->periode->first()]) }}" class="btn btn-sm btn-primary m-1 btn-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                            <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                        </svg>
                                    </a>
                                </th>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end mt-5 mb-2">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.index') }}">{{ __('Kembali') }}</a>
    </div>

    <form id="form-add-periode-visual-quality" action="" method="post">
        @csrf
        <input type="hidden" name="visual_quality_id" id="add_periode_vsq">
    </form>

    <form id="form-delete-visual-quality" action="{{ route('ipm.fleet.quality.delete', $data['project']->id) }}" method="post">
        @csrf
        @method('DELETE')
        <input type="hidden" name="visual_quality_id" id="delete_visual_quality_id">
    </form>
    @include('pages.project-monitoring.fleet.quality.modals.data_group_add', ['project' => $data['project'], 'jenis_project' =>$data['project_data_group']])
    @include('pages.project-monitoring.fleet.quality.modals.data_group_edit')
    @include('pages.project-monitoring.fleet.quality.modals.periode_per_group', ['project' => $data['project']])
    @include('pages.project-monitoring.fleet.quality.modals.drawing_add')
    @include('pages.project-monitoring.fleet.quality.modals.drawing_edit')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        let project_type = <?php echo json_encode($data['project_data_group']) ?>;
        $(document).ready(function() {
            checkSession();
            Inputmask({
                rightAlign: false,
                groupSeparator: ",",
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                prefix: "Rp. "
            }).mask(".nilai_project");

            
        });

        function groupEditModal(name, visual_id, bobot) {
            $("#group_name").val(name);
            var url = `{{ route('ipm.fleet.quality.store_bobot', ':id') }}`;
                url = url.replace(':id', visual_id);
            let bobot_existing = {{ $total_bobot }}
            bobot = bobot > 0 ? bobot : 0;
            $("#sisa_bobot").val(bobot);
            $("#sisa_bobot_old").val(bobot);
            $("#sisa_bobot_data").text(bobot_existing);
            $("#group_edit").attr('action', url);
        }

        function updateSisaBobot() {
            let bobot_existing = {{ $total_bobot }}
            let sisa_bobot = Number($("#sisa_bobot").val());
            let sisa_bobot_old = Number($("#sisa_bobot_old").val());
            let sisa =  (bobot_existing + sisa_bobot_old) - sisa_bobot;
            $("#sisa_bobot_data").text(sisa);
        }

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}_project`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }

        function groupDelete(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Data Group ini?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#delete_visual_quality_id").val(id);
                    $("#form-delete-visual-quality").submit();
                }
            })
        }

        function delete_document(id, nama) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Hapus Data',
                text: `Anda yakin ingin menghapus Dokumen ${nama}?`,
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // DO DUPLICATE HERE
                    var url = `{{ route('ipm.fleet.progress.dokumentasi_delete', ':id') }}`;
                        url = url.replace(':id', id);
                    $("#form-delete-documentation").attr('action', url);
                    $("#form-delete-documentation").submit();
                }
            })
        }

        function groupPeriodeModal(visual_quality_id, json_periode) {
            $("#periode_per_group_list").empty();
            $("#add_periode_vsq").val(visual_quality_id);
            $("#btn-new-periode").hide();
            json_periode = JSON.parse(json_periode);
            let max_periode = {{ $data['max_periode'] }};
            if (max_periode > json_periode.length) {
                $("#btn-new-periode").show();
            }
            let html = '';
            $.each(json_periode, function(i, item) {
                var url = `{{ route('ipm.fleet.quality.detail', ['project' => $data['project'], 'periode' => ':id']) }}`;
                        url = url.replace(':id', item.id);
                html += `
                <tr>
                    <td class="text-center align-middle">${item.periode}</td>
                    <td class="text-center align-middle">${item.created_format}</td>
                    <td class="text-center align-middle">
                        <a href="${url}" class="btn btn-sm btn-primary m-1 btn-icon" >
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
                `;
            });

            $("#periode_per_group_list").html(html);
        }

        function add_new_periode() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Ganti Periode Selanjutnya',
                html: `Yakin ingin melanjutkan ke periode selanjutnya ? <br/> Data yang ada pada periode ini akan terduplikasi ke periode selanjutnya`,
                // text: ``,
                // customClass: {
                //     htmlContainer: 'text-left'
                // },
                showCancelButton: true,
                confirmButtonText: 'Tambah',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    var url = `{{ route('ipm.fleet.quality.store_periode', $data['project']->id) }}`;
                    $("#form-add-periode-visual-quality").attr('action', url);
                    $("#form-add-periode-visual-quality").submit();
                }
            })
        }

        function change_jenisform() {
            let jenis_project = $("#jenis_project :selected").val();
            $("#jenis_form").empty();
            let html = `<option value="">Pilih jenis form</option>`;
            if (jenis_project != '') {
                let data_toload = project_type[jenis_project].sub_project;
                console.log(data_toload);
                $.each(data_toload, function(i, item) {
                    html += `<option value="${item.id}">${item.name}</option>`;
                })
            }
            $("#jenis_form").html(html);
            // console.log(project_type, jenis_project);
        }

        var table = $('#progress_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: []
        });

        $(document).on('click', '#drawing', function() {
            dataDrawing();
            $('#add_document_drawing_modal').modal('show');
        });

        function dataDrawing() {
            $('#drawing_table').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                lengthChange: false,
                pageLength: 3,
                order: [],
                ajax: {
                    url: "{{ route('ipm.fleet.quality.drawing.index', [$data['project']->id]) }}"
                },
                columns: [{
                        data: 'drawing',
                        name: 'drawing'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }

        
        function detailDrawing(id) {
            $.ajax({
                url: "{{ route('ipm.fleet.quality.drawing.detail', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function(response) {
                    $('#drawing_edit_modal input[name="id"]').val(response?.id);
                    $('#drawing_edit_modal input[name="deskripsi"]').val(response?.deskripsi);
                    $('#drawing_edit_modal input[name="tanggal"]').val(response?.tanggal);
                    $('#drawing_edit_modal').modal('show');

                    $('#add_document_drawing_modal').modal('hide');
                },
                error: function(error) {
                    $('#drawing_edit_modal').modal('hide');
                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            });
        };

        function deleteDrawing(id) {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('ipm.fleet.quality.drawing.delete', $data['project']['id']) }}`,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id
                        },
                        success: function(response) {
                            Swal.fire({
                                text: response?.message,
                                icon: "success"
                            });

                            dataDrawing();
                        },
                        error: function(error) {
                            Swal.fire({
                                title: error?.responseJSON?.message,
                                text: error?.responseJSON?.error,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        };

        /* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
    </script>
@endpush
