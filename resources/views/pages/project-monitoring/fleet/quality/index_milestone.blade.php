@extends('pages.project-monitoring.fleet.detail')

@section('quality-tab')
    active
@endsection

@push('page-styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endpush

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.quality.index', $data['project']['id']) }}" class="text-muted text-hover-primary">{{ __('Quality') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a class="text-muted">{{ __('Milestone') }}</a>
            </li>
        </ul>
    </div>
@endsection

@section('page-content')
    <span class="text-primary fw-bold">MILESTONE QUALITY PROGRESS</span>
    <div class="row mt-5 mb-5">
        <div class="col-12">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu active" data-id="1">Kick Off Meeting</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="2">First Steel Culting</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="3">Keel Laying</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="4">Engine Loading</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="5">Launching</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="6">Sea Trial</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link milestone-menu" data-id="7">Delivery</button>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-sm btn-primary m-1 btn-add-milestone">
                {!! theme()->getSvgIcon('icons/duotune/arrows/arr075.svg', 'svg-icon-1') !!}
                <span id="text-add-milestone"></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered" id="milestone_table">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('No') }}</th>
                        <th class="text-center">{{ __('Item Inspeksi') }}</th>
                        <th class="text-center">{{ __('Task List') }}</th>
                        <th class="text-center">{{ __('Progress') }}</th>
                        <th class="text-center">{{ __('Evidence') }}</th>
                        <th class="text-center">{{ __('Remarks') }}</th>
                        <th class="text-center">{{ __('Last Update') }}</th>
                        <th class="text-center">{{ __('Aksi') }}</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    @include('pages.project-monitoring.fleet.quality.modals.milestone_add')
    @include('pages.project-monitoring.fleet.quality.modals.milestone_edit_progress')
@endsection

@section('page-footer')
    <div class="d-flex justify-content-end">
        <a class="btn btn-secondary btn-sm m-1" href="{{ route('ipm.fleet.quality.index', $data['project']['id']) }}">{{ __('Kembali') }}</a>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        const types = {
            1: 'Kick Off Meeting',
            2: 'First Steel Culting',
            3: 'Keel Laying',
            4: 'Engine Loading',
            5: 'Launching',
            6: 'Sea Trial',
            7: 'Delivery',
        }

        $(document).ready(function() {
            datatable();
            $('#text-add-milestone').text(`Tambah ${types?.[1]}`)
            $('.btn-add-milestone').attr('data-id', 1);

            Inputmask({
                rightAlign: false,
                alias: "numeric",
                autoGroup: true,
                digits: 0,
                min: 0,
                max: 100,
                suffix: " %"
            }).mask("#progres");
        })

        $(document).on('click', '.milestone-menu', function() {
            const selectedMilestone = $(this).data('id');

            $('.milestone-menu').removeClass('active');
            $(this).addClass('active');

            datatable(selectedMilestone)
            $('#text-add-milestone').text(`Tambah ${types?.[selectedMilestone]}`)
            $('.btn-add-milestone').data('id', selectedMilestone);
        });

        $(document).on('click', '.btn-add-milestone', function() {
            const selectedMilestone = $(this).data('id');

            $('#add_milestone_modal input[name="tipe_milestone"]').val(selectedMilestone);
            $('#add_milestone_modal').modal('show');
        });

        $(document).on('submit', '.form-milestone', function(e) {
            e.preventDefault();

            var actionURL = $(this).find('input[name="action"]').val()
            var typeMilestone = $(this).find('input[name="tipe_milestone"]').val()
            var url = $(this).attr('action');

            $.ajax({
                url: url,
                type: 'POST',
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    $(`#${actionURL}_milestone_modal`).find('form')[0].reset();
                    $(`#${actionURL}_milestone_modal`).modal('hide');

                    Swal.fire({
                        text: response?.message,
                        icon: "success"
                    });

                    datatable(typeMilestone);
                },
                error: function(error) {
                    $(`#${actionURL}_milestone_modal`).find('form')[0].reset();
                    $(`#${actionURL}_milestone_modal`).modal('hide');

                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });

        $(document).on('click', '.btn-delete', function() {
            var typeMilestone = $(this).data('type');
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('ipm.fleet.quality.milestone.delete', $data['project']['id']) }}",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id'),
                            "tipe_milestone": typeMilestone
                        },
                        success: function(response) {
                            Swal.fire({
                                text: response?.message,
                                icon: "success"
                            });

                            datatable(typeMilestone);
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-edit', function() {
            var typeMilestone = $(this).data('type');
            $.ajax({
                url: "{{ route('ipm.fleet.quality.milestone.show', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": $(this).data('id'),
                    "tipe_milestone": typeMilestone
                },
                success: function(data) {
                    $('#edit_milestone_modal input[name="id"]').val(data?.id);
                    $('#edit_milestone_modal input[name="tipe_milestone"]').val(typeMilestone);
                    $('#edit_milestone_modal input[name="item_inspeksi"]').val(data?.item_inspeksi);
                    $('#edit_milestone_modal input[name="task_list"]').val(data?.task_list);
                    $('#edit_milestone_modal input[name="progres"]').val(data?.progres);
                    $('#edit_milestone_modal input[name="remarks"]').val(data?.remarks);

                    $('#edit_milestone_modal').modal('show');
                },
                error: function(response) {
                    Swal.fire({
                        text: response?.responseJSON?.message,
                        icon: "error"
                    });
                }
            })
        });

        function datatable(tipeMilestone = 1) {
            var table = $('#milestone_table').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: {
                    url: "{{ route('ipm.fleet.quality.milestone.index', $data['project']['id']) }}",
                    type: "GET",
                    data: function(d) {
                        d.tipe_milestone = tipeMilestone
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'id_item',
                        searchable: false,
                        orderable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'item_inspeksi',
                        name: 'item_inspeksi'
                    },
                    {
                        data: 'task_list',
                        name: 'task_list'
                    },
                    {
                        data: 'progres',
                        name: 'progres',
                        className: 'text-center'
                    },
                    {
                        data: 'evidence',
                        name: 'evidence'
                    },
                    {
                        data: 'remarks',
                        name: 'remarks'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: false,
                        orderable: false,
                        className: 'text-center'
                    }
                ],
                columnDefs: [{
                    targets: '_all',
                    className: 'align-middle'
                }]
            })
        }
    </script>
@endpush
