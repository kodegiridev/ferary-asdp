<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_milestone_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form-milestone" action="{{ route('ipm.fleet.quality.milestone.update', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ubah Progres') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="action" value="edit">
                    <input type="hidden" name="id">
                    <input type="hidden" name="tipe_milestone">
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Item Inspeksi') }}</label>
                        <input name="item_inspeksi" type="text" class="form-control" required />
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Task List') }} </label>
                        <input name="task_list" type="text" class="form-control" required />
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Progress') }} </label>
                        <input name="progres" id="progres" type="text" class="form-control" required />
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="form-label">{{ __('Evidence') }} </label>
                        <input name="evidence" type="file" class="form-control" accept=".pdf" />
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="form-label">{{ __('Remarks') }} </label>
                        <input name="remarks" type="text" class="form-control" />
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
