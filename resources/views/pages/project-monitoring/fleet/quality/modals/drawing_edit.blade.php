<div class="modal fade drawing" tabindex="-1" data-bs-backdrop="static" id="drawing_edit_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.quality.drawing.update', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="modal" value="drawing_edit_modal">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ubah Drawing') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">{{ __('Unggah Drawing') }}</label>
                        <input type="file" name="drawing" class="form-control" accept="image/*" />
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6 col-12">
                            <label class="form-label">{{ __('Deskripsi') }}</label>
                            <input type="text" name="deskripsi" class="form-control" placeholder="Masukan deskripsi" />
                        </div>
                        <div class="col-md-6 col-12">
                            <label class="form-label">{{ __('Tanggal') }}</label>
                            <input type="date" name="tanggal" class="form-control" placeholder="Pilih tanggal" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
