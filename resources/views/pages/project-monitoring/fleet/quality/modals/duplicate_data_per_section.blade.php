<div class="modal fade" tabindex="-1" id="duplicate_per_section_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Duplikasi Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form id="form-duplicate-per-section" action="{{ route('ipm.fleet.quality.duplicate_section', ['project' => $project_id, 'periode' => $periode_id]) }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="jenis_data_from" id="jenis_data_from" value="1">
                    <input type="hidden" name="vqf_id" value="{{ $vqf_id }}">
                    <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                    <span>Yakin ingin menduplikasi data GROUP dari TAB <span id="jenis_data_from_section_name"></span> ?</span>
                    <br>
                    <span>Seluruh data yang ada pada tab ini akan diduplikasi ke tab yang dipilih.</span>
                    <p><strong class="text-danger">PASTIKAN KEMBALI PILIHAN TAB TUJUAN DAN DATA YANG ADA DI DALAMNYA UNTUK MENGHINDARI DUPLIKASI DATA YANG TIDAK DIINGINKAN !</strong></p>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Tab Tujuan') }}
                            </label>
                        </div>
                        <div class="row" id="list_tab_tujuan">
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-warning btn-sm">{{ __('Duplikasi') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
