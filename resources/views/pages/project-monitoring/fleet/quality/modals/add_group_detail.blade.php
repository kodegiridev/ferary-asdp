<div class="modal fade" tabindex="-1" id="add_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="required form-label">
                            {{ __('Block Erection') }}
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input name="block_name" type="text" class="form-control" placeholder="Block Name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label">
                                    {{ __('Plan Status *') }} 
                                </label> 
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="" id="" class="form-control">
                                        <option value="accepted">Accepted</option>
                                        <option value="inspection-invitation">Inspection Invitation</option>
                                        <option value="internal-progress-yard">Internal Progress Yard</option>
                                        <option value="no-progress" selected>No Progress</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="name" class="form-label">
                                    {{ __('Inspection Status *') }} 
                                </label> 
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="" id="" class="form-control">
                                        <option value="accepted">Accepted</option>
                                        <option value="reinspect-partial">Re-Inspect / Partial</option>
                                        <option value="yard-progress">Yard Progress</option>
                                        <option value="no-progress" selected>No Progress</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="form-label">
                            {{ __('Evidence *') }} 
                        </label> 
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input name="file" type="file" class="form-control" accept="image/*, application/pdf" required/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer flex justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                <button id="btn-commit-add-progress" type="button" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
            </div>
        </div>
    </div>
</div>
