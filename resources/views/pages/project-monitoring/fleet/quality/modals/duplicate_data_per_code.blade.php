<div class="modal fade" tabindex="-1" id="duplicate_per_code_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Duplikasi Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form id="form-duplicate-per-code" action="{{ route('ipm.fleet.quality.duplicate_code', ['project' => $project_id, 'periode' => $periode_id]) }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="vqf_id" value="{{ $vqf_id }}">
                    <input type="hidden" name="flag" value="{{ json_encode($flag) }}">
                    <input type="hidden" name="id" id="vqf_code_id">
                    <span>Yakin ingin menduplikasi data <span class="duplicate-per-code-name"></span> ?</span>
                    <br>
                    <span>Seluruh data <span class="duplicate-per-code-name"></span> akan terduplikasi pada form ini sebanyak jumlah yang diinputkan.</span>
                    <p><strong class="text-danger">PASTIKAN KEMBALI DATA YANG DIPILIH DAN JUMLAH YANG DIMASUKKAN UNTUK MENGHINDARI DUPLIKASI DATA YANG TIDAK DIINGINKAN !</strong></p>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Jumlah Duplikasi Data') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input required min="1" name="jumlah_data" type="number" class="form-control" placeholder="Masukkan angka"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-add-basic-data" type="submit" class="btn btn-warning btn-sm">{{ __('Duplikasi') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
