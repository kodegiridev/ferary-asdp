<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="add_milestone_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form-milestone" action="{{ route('ipm.fleet.quality.milestone.store', $data['project']['id']) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="action" value="add">
                    <input type="hidden" name="tipe_milestone" value="{{ old('tipe_milestone') }}">
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Item Inspeksi') }}</label>
                        <input name="item_inspeksi" type="text" class="form-control" value="{{ old('item_inspeksi') }}" placeholder="Item Inspeksi" required />
                    </div>
                    <div class="form-group">
                        <label for="name" class="required form-label">{{ __('Task List') }} </label>
                        <input name="task_list" type="text" class="form-control" value="{{ old('task_list') }}" placeholder="Task List" required />
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
