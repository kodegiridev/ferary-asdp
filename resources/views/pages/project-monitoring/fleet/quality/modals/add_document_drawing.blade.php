<div class="modal fade" tabindex="-1" id="add_document_drawing_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Masukkan Dokumen Drawing') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.fleet.quality.drawing.store_group', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="periode" id="periode_id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="form-label">
                                {{ __('Document Drawing * (jpg)') }} 
                            </label> 
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="file" type="file" class="form-control" accept="image/*" required/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary btn-sm">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
