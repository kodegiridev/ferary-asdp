<div class="modal fade drawing" tabindex="-1" data-bs-backdrop="static" id="add_document_drawing_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Drawing List') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>
            <div class="modal-body mb-0">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered nowrap" id="drawing_table">
                        <thead class="text-center align-middle bg-primary">
                            <tr>
                                <th style="color: white !important;">{{ __('Drawing') }}</th>
                                <th style="color: white !important;">{{ __('Date') }}</th>
                                <th style="color: white !important;">{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <form class="save-form form" action="{{ route('ipm.fleet.quality.drawing.store', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="modal" value="add_document_drawing_modal">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label required">{{ __('Unggah Drawing') }}</label>
                        <input type="file" name="drawing" class="form-control" accept="image/*" required />
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6 col-12">
                            <label class="form-label">{{ __('Deskripsi') }}</label>
                            <input type="text" name="deskripsi" class="form-control" placeholder="Masukan deskripsi" />
                        </div>
                        <div class="col-md-6 col-12">
                            <label class="form-label">{{ __('Tanggal') }}</label>
                            <input type="date" name="tanggal" class="form-control" placeholder="Pilih tanggal" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary save">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('modal-scripts')
    <script>
        // $(document).on('submit', '.save-form', function(e) {
        //     e.preventDefault();
        //     var modal = $(this).find('input[name="modal"]').val()

        //     $.ajax({
        //         url: $(this).attr('action'),
        //         type: 'POST',
        //         data: new FormData($(this)[0]),
        //         contentType: false,
        //         cache: false,
        //         processData: false,
        //         success: function(response) {
        //             $(`#${modal}`).find('form')[0].reset();
        //             location.reload();
        //         },
        //         error: function(error) {
        //             $(`#${modal}`).find('form')[0].reset();
        //             Swal.fire({
        //                 title: error?.responseJSON?.message,
        //                 text: error?.responseJSON?.error,
        //                 icon: "error"
        //             });
        //         }
        //     });
        // });



        function dataDrawing() {
            $('#drawing_table').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                lengthChange: false,
                pageLength: 3,
                order: [],
                ajax: {
                    url: "{{ route('ipm.fleet.quality.drawing.index', [$data['project']->id]) }}"
                },
                columns: [{
                        data: 'drawing',
                        name: 'drawing'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'text-center align-middle'
                }]
            });
        }
    </script>
@endpush
