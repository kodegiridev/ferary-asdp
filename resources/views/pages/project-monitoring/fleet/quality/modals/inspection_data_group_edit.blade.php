<div class="modal fade" tabindex="-1" backdrop="static" id="inspection_data_group_edit_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.quality.inspection.update', [$data['project']['id'], $data['periode']->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Edit Data') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Periode Ke') }}</label>
                        <input type="number" name="periode" class="form-control @error('periode') is-invalid @enderror" value="{{ old('periode') }}" placeholder="Periode Ke" required />
                        @error('periode')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Invitation') }}</label>
                        <input type="number" name="jml_undangan" class="form-control @error('jml_undangan') is-invalid @enderror" value="{{ old('jml_undangan') }}" placeholder="Jumlah Invitation" required />
                        @error('jml_undangan')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Re-Inspect') }}</label>
                                <input type="number" name="resinpect" class="form-control @error('resinpect') is-invalid @enderror" value="{{ old('resinpect') }}" placeholder="Jumlah Re-Inspect" required />
                                @error('resinpect')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc by Class') }}</label>
                                <input type="number" name="acc_class" class="form-control @error('acc_class') is-invalid @enderror" value="{{ old('acc_class') }}" placeholder="Jumlah Acc by Class" required />
                                @error('acc_class')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc By Owner') }}</label>
                                <input type="number" name="acc_owner" class="form-control @error('acc_owner') is-invalid @enderror" value="{{ old('acc_owner') }}" placeholder="Acc By Owner" required />
                                @error('acc_owner')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label for="name" class="required form-label">{{ __('Acc by Both') }}</label>
                                <input type="number" name="acc_both" class="form-control @error('acc_both') is-invalid @enderror" value="{{ old('acc_both') }}" placeholder="Acc By Both" required />
                                @error('acc_both')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="required form-label">{{ __('Evidence') }}</label>
                        <div class="row">
                            <div class="col-md-10 col-10">
                                <input name="evidence" type="file" class="form-control" accept="image/*, application/pdf">
                            </div>
                            <div class="col-md-2 col-2">
                                <a class="btn btn-sm btn-icon btn-light-primary mt-2" target="_blank" id="evidence_item" style="float: right;">
                                    {!! theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') !!}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success btn-sm">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
