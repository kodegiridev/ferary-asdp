<div class="modal fade" tabindex="-1" id="periode_per_group_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Pilih Periode Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <div class="modal-body">
                
                <button id="btn-new-periode" onclick="add_new_periode()" type="button" class="btn btn-primary btn-sm">{{ __('Tambah Periode') }}</button>
                <table class="table table-bordered">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Bulan Ke') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Last Updated') }}</th>
                            <th class="text-center align-middle" style="color:white !important">{{ __('Detail') }}</th>
                        </tr>
                    </thead>
                    <tbody id="periode_per_group_list">
                        
                    </tbody>
                </table>
            </div>

            <div class="modal-footer flex justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
            </div>
        </div>
    </div>
</div>
