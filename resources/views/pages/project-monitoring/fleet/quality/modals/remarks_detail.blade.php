<div class="modal fade" tabindex="-1" id="remarks_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="remarks_detail_title">{{ __('Remark Testing Comparment 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <div id="list-remarks" class="modal-body">
                <div class="remarks-data mt-3">
                    <span class="text-primary"><strong>Code Component :  Testing Remarks 1</strong></span>
                    <table class="table table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Plan Status') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Status') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Result') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Date') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center align-middle">Inspection Invitation</td>
                                <td class="text-center align-middle">Re - Inspect / Partial</td>
                                <td class="text-center align-middle">Not Ok</td>
                                <td class="text-center align-middle">2023-11-17</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="remarks-data mt-3">
                    <span class="text-primary"><strong>Code Component :  Testing Remarks 2</strong></span>
                    <table class="table table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Plan Status') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Status') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Inspection Result') }}</th>
                                <th class="text-center align-middle" style="color:white !important">{{ __('Date') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center align-middle">Accepted</td>
                                <td class="text-center align-middle">Accepted</td>
                                <td class="text-center align-middle">Ok</td>
                                <td class="text-center align-middle">2023-11-24</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="modal-footer flex justify-content-center">
                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
            </div>
        </div>
    </div>
</div>
