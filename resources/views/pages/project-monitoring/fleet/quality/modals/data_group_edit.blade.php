<div class="modal fade" tabindex="-1" id="data_group_edit_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Edit Group') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form id="group_edit" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama Group') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="group_name" type="text" class="form-control" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-12 mt-5 mb-2">
                                <span><strong >Sisa Bobot : <span id="sisa_bobot_data">{{ $total_bobot }}</span>%</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Bobot %') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input id="sisa_bobot_old" type="hidden"/>
                                <input id="sisa_bobot" name="bobot" placeholder="Bobot" type="number" class="form-control" onkeyup="updateSisaBobot()" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-edit-data-group" type="submit" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
