<div class="modal fade" tabindex="-1" id="data_group_add_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Data') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <form action="{{ route('ipm.fleet.quality.store', $project['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="project_id" value="{{ $project['id'] }}">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama Parameter') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input name="nama_grup" placeholder="Item Inspeksi" type="text" class="form-control" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Jenis Project') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="jenis_project" id="jenis_project" class="form-control" onchange="change_jenisform()" required>
                                    <option value="">Pilih jenis project</option>
                                    @if (!empty($jenis_project))
                                    @foreach ($jenis_project as $item)
                                        <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Jenis Form') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="jenis_form" id="jenis_form" class="form-control" required>
                                    <option value="">Pilih jenis form</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer flex justify-content-center">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="btn-commit-add-data-group" type="submit" class="btn btn-primary btn-sm">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
