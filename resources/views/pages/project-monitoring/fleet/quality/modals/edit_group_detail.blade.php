<div class="modal fade" tabindex="-1" id="edit_group_detail_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Edit Detail Testing Component 1') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="required form-label">
                            {{ __('Code') }}
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input name="block_name" type="text" class="form-control" placeholder="code" value="Testing code 1">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="required form-label">
                            {{ __('Component') }}
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input name="block_name" type="text" class="form-control" placeholder="code" value="Testing Component 1">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="name" class="required form-label">
                            {{ __('Evidence') }}
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <a href="javascript:void(0)">Lihat Dokumen</a>
                        </div>
                    </div>
                </div>
                <div id="list_code_component"></div>
                <button type="button" class="btn btn-sm btn-primary m-1" onclick="add_row()">
                    {{ __('+ Tambah New Code Component') }}
                </button>
            </div>

            <div class="modal-footer flex justify-content-center">
                <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                <button id="btn-commit-add-progress" type="button" class="btn btn-success btn-sm">{{ __('Ubah') }}</button>
            </div>
        </div>
    </div>
</div>
