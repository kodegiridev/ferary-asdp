@extends('layout.demo1.master')

@section('styles')
    @stack('page-styles')
@endsection

@section('content-module')
    <!--begin::Breadcrumb-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            @yield('title-breadcrumb')
        </div>
    </div>
    <!--end::Breadcrumb-->

    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            @yield('page-content')
        </div>
    </div>
    <!--end::Content-->
@endsection

@section('scripts')
    @stack('page-scripts')
@endsection
