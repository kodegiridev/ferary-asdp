<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="submit_summary_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Submit Summary') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>

            <form class="form" method="POST">
                @csrf
                <input type="hidden" name="judul_dokumen">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="penyetuju_id" class="required form-label">
                                {{ __('Approver') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="penyetuju_id" class="form-select form-select-solid @error('penyetuju_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($data['approvers'] as $approver)
                                        <option value="{{ $approver->id }}" {{ old('penyetuju_id') == $approver->id ? 'selected' : '' }}>
                                            {{ $approver->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('penyetuju_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center justify-content-between my-3">
                        <div class="col-6">
                            <label for="user_verifikator_id" class="required form-label">
                                {{ __('Verifikator') }}
                            </label>
                        </div>
                        <div class="col-6">
                            <button type="button" class="btn btn-sm btn-dark float-end" id="btn_add_verificator">
                                <i class="fa fa-plus"></i>
                                <i class="fa fa-user"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="box-input-verificator @error('user_verifikator_id.*') is-invalid @enderror">
                            <div class="row input-verificator">
                                <div class="col-10">
                                    <select name="user_verifikator_id[]" class="form-select form-select-solid verificator" required>
                                        <option value="">Pilih data</option>
                                        @foreach ($data['verificators'] as $verificator)
                                            <option value="{{ $verificator->id }}">
                                                {{ $verificator->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @error('user_verifikator_id.*')
                            <span class="invalid-feedback mb-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mt-3">
                        <label for="keterangan_pengaju" class="form-label">{{ __('Remarks') }}</label>
                        <input type="text" class="form-control form-control-solid @error('keterangan_pengaju') is-invalid @enderror" placeholder="Ketik disini" name="keterangan_pengaju" value="{{ old('keterangan_pengaju') }}" />

                        @error('keterangan_pengaju')
                            <span class="invalid-feedback mb-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('modal-scripts')
    <script>
        $(document).on('submit', '#submit_summary_modal .form', function(e) {
            e.preventDefault();
            $('input[name="judul_dokumen"]').val($('#summary_judul_dokumen').val());
            $('#submit_summary_warning').modal('show');
            $('#submit_summary_modal').modal('hide');
        });

        $('#btn_submit_summary').on('click', function() {
            $.ajax({
                url: "{{ route('ipm.fleet.summary.send-approval', $data['project']['id']) }}",
                type: 'POST',
                data: new FormData($('#submit_summary_modal .form')[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function() {
                    location.reload();
                },
                error: function() {
                    location.reload();
                }
            });
        });

        $(document).on('click', '#btn_add_verificator', function() {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');
            var options = ''

            $('select[name="user_verifikator_id[]"]').each(function(index, element) {
                if (index == 0) {
                    options = $(this).html();
                }
            });

            boxInputVerificator.append(`
                <div class="row input-verificator mt-2">
                    <div class="col-10">
                        <select name="user_verifikator_id[]" class="form-select form-select-solid verificator">
                            ${options}
                        </select>
                    </div>
                    <div class="col-1 d-flex align-items-center">
                        <button type="button" class="btn btn-sm btn-danger btn-delete-verificator">
                            {!! theme()->getSvgIcon('icons/duotune/arrows/arr090.svg', 'svg-icon-3') !!}
                        </button>
                    </div>
                </div>
            `);
        });

        $(document).on('click', '.btn-delete-verificator', function() {
            var inputVerificator = $(this).closest('.input-verificator');
            var selectElement = inputVerificator.find('select').val();

            if (selectElement != '') {
                var approver = $('select[name="penyetuju_id"]');
                var verificator = $('select[name="user_verifikator_id[]"]');

                $.each(approver.find('option'), function(key, value) {
                    if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                        $(this).prop('hidden', false);
                    }
                });

                verificator.each(function(index, element) {
                    var selectVerificator = $(this);
                    $.each(selectVerificator.find('option'), function(key, value) {
                        if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                            $(this).prop('hidden', false);
                        }
                    });
                });
            }
            inputVerificator.remove();
        });
    </script>
@endpush
