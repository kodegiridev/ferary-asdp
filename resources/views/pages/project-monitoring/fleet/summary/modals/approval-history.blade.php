<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="approval_history">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Riwayat Pengajuan') }}</h3>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover nowrap" id="approval_table">
                        <thead>
                            <tr>
                                <td class="text-center align-middle">{{ __('No') }}</td>
                                <td class="text-center align-middle">{{ __('Nama') }}</td>
                                <td class="text-center align-middle">{{ __('Status') }}</td>
                                <td class="text-center align-middle">{{ __('Tanggal') }}</td>
                                <td class="text-center align-middle">{{ __('Keterangan') }}</td>
                            </tr>
                        </thead>
                        <tbody id="approval_data">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Tutup') }}</button>
            </div>
        </div>
    </div>
</div>
