@extends('pages.project-monitoring.fleet.detail')

@section('summary-tab')
    active
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@php
    $isPic = $data['project']['pic'] == $data['user']['id'];
@endphp

@section('title-breadcrumb')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Integrated Project Monitoring (IPM)') }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            <li class="breadcrumb-item text-muted">
                <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('ipm.fleet.index') }}" class="text-muted text-hover-primary">{{ __('Fleet (IPM)') }}</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-400 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">{{ __('Summary') }}</li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <!--begin::Project Info-->
            <div class=row>
                <div class="col-md-6 col-12 mt-5">
                    <div class="card card-flush h-100">
                        <div class="card-header">
                            <h3 class="card-title text-gray-800 fw-bold">Project Data</h3>
                            <div class="card-toolbar">
                                @if ($isPic)
                                    @if (isset($data['approval']['status']) && !in_array($data['approval']['status'] ?? false, [3, 4]))
                                        <a type="button" class="btn btn-sm btn-light btn-approval-history">
                                            {{ __('Lihat Persetujuan') }}
                                        </a>
                                    @else
                                        <a type="button" class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#submit_summary_modal">
                                            {!! theme()->getSvgIcon('icons/duotune/general/gen016.svg', 'svg-icon-1') !!}
                                            {{ __('Submit Approval') }}
                                        </a>
                                    @endif
                                @endif
                                &nbsp
                                &nbsp
                                <a href="{{ route('ipm.fleet.summary.print', $data['project']['id']) }}" class="btn btn-sm btn-icon btn-light-warning">
                                    {!! theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') !!}
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive">
                                @if ($isPic)
                                    <tr class="align-middle">
                                        <td>Judul Document</td>
                                        <td><input type="text" class="form-control" id="summary_judul_dokumen" placeholder="Masukan judul dokumen" value="{{ $data['approval']['judul_dokumen'] ?? '' }}"></td>
                                    </tr>
                                @endif
                                <tr>
                                    <td class="w-150px">Project Name</td>
                                    <td> : {{ $data['project']['nama_project'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Contractor</td>
                                    <td> : {{ $data['project']['kontraktor'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Contract No</td>
                                    <td> : {{ $data['project']['nomor_kontrak'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Contract Status</td>
                                    <td> : {{ $data['project']['status'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Supervisor</td>
                                    <td> : {{ $data['project']['supervisor'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Project Value</td>
                                    <td> : {{ toRupiah($data['project']['nilai_project'] ?? 0) }}</td>
                                </tr>
                                <tr>
                                    <td>Project Periode</td>
                                    <td> : 0 {{ strtoupper($data['project']['jenis_periode']) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">Start</td>
                                    <td> : {{ $data['first_basic_data']['mulai_kontrak'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">End</td>
                                    <td> : {{ $data['first_basic_data']['selesai_kontrak'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <td>Update Up To</td>
                                    <td> : {{ $data['latest_basic_data']['mulai_kontrak'] ?? '' }} s/d {{ $data['latest_basic_data']['selesai_kontrak'] ?? '' }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 h-100">
                    @if ($data['is_approver_verificator'])
                        <div class="card card-flush mt-5">
                            <div class="card-header">
                                <h3 class="card-title text-gray-800 fw-bold">Approval</h3>
                                <div class="card-toolbar">
                                    <a type="button" class="btn btn-sm btn-light btn-approval-history">
                                        {{ __('Lihat Persetujuan') }}
                                    </a>
                                    @if ($data['can_approve_reject'])
                                        &nbsp
                                        &nbsp
                                        <button type="button" id="btn_reject_status_modal" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#reject_status_modal">
                                            {{ __('Tolak') }}
                                        </button>
                                        &nbsp;&nbsp;
                                        @if ($data['approval']['status'] != 4)
                                            <button type="button" id="btn_approve_status_modal" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#approve_status_modal">
                                                {{ __('Terima') }}
                                            </button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @if ($data['approval']['type'] == 1)
                                    <div class="form-group mb-3">
                                        <label class="form-label">{{ __('Judul Dokumen') }}</label>
                                        <input type="text" class="form-control" value="{{ $data['approval']['judul_dokumen'] ?? '' }}" readonly>
                                    </div>
                                @else
                                    <div class="form-group mb-3">
                                        <label class="form-label">{{ __('Catatan Project') }}</label>
                                        <input type="text" class="form-control" value="{{ $data['approval']['catatan_project'] ?? '' }}" readonly>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="form-label">Status</label>
                                    <input type="text" class="form-control" value="{{ $data['approval']['approval_status'] ?? '' }}" readonly>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card card-flush mt-5">
                        <div class="card-header">
                            <h3 class="card-title text-gray-800 fw-bold">Cost Performances Index</h3>
                            <div class="card-toolbar"></div>
                        </div>
                        <div class="card-body">
                            <table class="table table-repsonsive">
                                <tr>
                                    <td class="w-175px">Cost Performances Index</td>
                                    <td> : {{ $data['calculate']['cpi'] }}</td>
                                </tr>
                                <tr class="text-right">
                                    <td>Actual Cost</td>
                                    <td> : {{ $data['calculate']['actual_cost'] }}</td>
                                </tr>
                                <tr class="text-right">
                                    <td>Contruction Value</td>
                                    <td> : {{ $data['calculate']['construction_value'] }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card card-flush mt-5">
                        <div class="card-header">
                            <h3 class="card-title text-gray-800 fw-bold">Estimate Time Completion ( {{ strtoupper($data['project']['jenis_periode'] ?? '') }} )</h3>
                            <div class="card-toolbar"></div>
                        </div>
                        <div class="card-body pt-5">
                            <table class="table table-borderless">
                                <tbody class="text-center">
                                    <tr>
                                        <td>
                                            <h1 style="font-size: 60px;">{{ $data['calculate']['etc'] }}</h1>
                                            <h3>( {{ \Carbon\Carbon::parse($data['latest_progress']['tgl_update'] ?? $data['project']['updated_at'])->format('F Y') }} )</h3>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Project Info-->

            <!--begin::Time Progress-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Time Schedule Progress (%)') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @foreach ($data['schedule_progress'] as $idx => $progress)
                                    <table class="table table-bordered table-hover mb-5">
                                        <thead>
                                            <tr class="fw-bold text-center">
                                                <td class="w-150px">Periode</td>
                                                @for ($i = 0; $i < 15; $i++)
                                                    <td>{{ $i + 1 + 15 * $idx }}</td>
                                                @endfor
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($progress as $key => $progres)
                                                <tr class="text-center">
                                                    <td>{{ $key }}</td>
                                                    @for ($j = 0; $j < 15; $j++)
                                                        <td>{{ $progres[$j] ?? '-' }}</td>
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Time Progress-->

            <!--begin::Overview Progress-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Overview Detail Progress (%)') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead class="fw-bold text-center align-middle">
                                        <tr>
                                            <td rowspan="2">{{ __('No') }}</td>
                                            <td rowspan="2">{{ __('Task Description') }}</td>
                                            <td rowspan="2">{{ __('Bobot (%)') }}</td>
                                            <td colspan="4">{{ __('New Periode (%)') }}</td>
                                            <td colspan="3">{{ __('Total Progress (%)') }}</td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('Plan') }}</td>
                                            <td>{{ __('Last Actual') }}</td>
                                            <td>{{ __('This Actual') }}</td>
                                            <td>{{ __('Deviasi') }}</td>
                                            <td>{{ __('Plan') }}</td>
                                            <td>{{ __('Actual') }}</td>
                                            <td>{{ __('Deviasi') }}</td>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @if (count($data['item_progress']) > 0)
                                            @php
                                                $totalPlan = 0;
                                                $totalActual = 0;
                                                $totalDeviasi = 0;
                                            @endphp
                                            @foreach ($data['item_progress'] as $item)
                                                <tr class="fw-bold">
                                                    <td>#</td>
                                                    <td>{{ $item['main_item'] }}</td>
                                                    <td>{{ $item['batas_bobot'] }}</td>
                                                    <td>{{ $item['plan'] }}</td>
                                                    <td>{{ $item['progres_lalu'] }}</td>
                                                    <td>{{ $item['progres_ini'] }}</td>
                                                    <td>{{ $item['deviasi'] }}</td>
                                                    <td>{{ toDecimal($item['plan'], 1) }}</td>
                                                    <td>{{ toDecimal($item['progres_ini'], 1) }}</td>
                                                    <td>{{ toDecimal($item['deviasi'], 1) }}</td>
                                                </tr>
                                                @php
                                                    $totalPlan += $item['plan'];
                                                    $totalActual += $item['progres_ini'];
                                                    $totalDeviasi += $item['deviasi'];
                                                @endphp
                                                @if (!empty($item['sub_item']))
                                                    @foreach ($item['sub_item'] as $subitem)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $subitem['sub_item'] }}</td>
                                                            <td>{{ $subitem['bobot'] }}</td>
                                                            <td>{{ $subitem['plan'] }}</td>
                                                            <td>{{ $subitem['progres_lalu'] }}</td>
                                                            <td>{{ $subitem['progres_ini'] }}</td>
                                                            <td>{{ $subitem['deviasi'] }}</td>
                                                            <td>{{ toDecimal($subitem['plan'], 1) }}</td>
                                                            <td>{{ toDecimal($subitem['progres_ini'], 1) }}</td>
                                                            <td>{{ toDecimal($subitem['deviasi'], 1) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="7">
                                                    {{ __('Total Progress Main Item (%)') }}
                                                </td>
                                                <td>{{ toDecimal($totalPlan, 1) }}</td>
                                                <td>{{ toDecimal($totalActual, 1) }}</td>
                                                <td>{{ toDecimal($totalDeviasi, 1) }}</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Overview Progress-->

            <!--begin::Time Quality-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Time Schedule Quality (%)') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                @foreach ($data['schedule_quality'] as $idx => $qualities)
                                    <table class="table table-bordered table-hover mb-5">
                                        <thead>
                                            <tr class="fw-bold text-center">
                                                <td class="w-150px">Periode</td>
                                                @for ($i = 0; $i < 15; $i++)
                                                    <td>{{ $i + 1 + 15 * $idx }}</td>
                                                @endfor
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($qualities as $key => $quality)
                                                <tr class="text-center">
                                                    <td>{{ $key }}</td>
                                                    @for ($j = 0; $j < 15; $j++)
                                                        <td>{{ $quality[$j] ?? '-' }}</td>
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Time Quality-->

            <!--begin::Visual Quality-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Visual Quality Monitoring (%)') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <td>{{ __('QUALITY COMPLETION') }}</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="font-size: 60px;">{{ __('0%') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Visual Quality-->

            <!--begin::Document-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Documentation') }}</h3>
                        </div>
                        <div class="card-body">
                            @foreach ($data['documentations'] as $documents)
                                <div class="row mb-5">
                                    @foreach ($documents as $document)
                                        <div class="col-4">
                                            <img src="{{ asset('upload/project-monitoring-port/' . $document['file']) }}" class="rounded h-300px" alt="{{ $document['nama'] }}" />
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Document-->

            <!--begin::Recomendation-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Recomendation') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap">
                                    <thead class="fw-bold text-center align-middle">
                                        <tr>
                                            <th class="w-50px">{{ __('No') }}</th>
                                            <th>{{ __('Notice') }}</th>
                                            <th>{{ __('Pending Matters') }}</th>
                                            <th>{{ __('Execute By') }}</th>
                                            <th class="w-150px">{{ __('Due Date') }}</th>
                                            <th class="w-100px">{{ __('Status') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['recomendations'] as $key => $recomendation)
                                            <tr>
                                                <td class="text-center">{{ $key + 1 }}</td>
                                                <td>{{ $recomendation->notice }}</td>
                                                <td>{{ $recomendation->to_do_list }}</td>
                                                <td>{{ $recomendation->pic }}</td>
                                                <td class="text-center">{{ $recomendation->due_date }}</td>
                                                <td class="text-center">{{ strtoupper($recomendation->status) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Recomendation-->

            <!--begin::Remarks-->
            <div class="row">
                <div class="col-12">
                    <div class="card card-flush shadow-sm mt-5">
                        <div class="card-header d-flex justify-content-center">
                            <h3 class="card-title text-gray-800 fw-bold">{{ __('Remarks') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tbody class="text-center">
                                        <tr>
                                            <td>{{ $data['latest_progress']['remarks'] ?? '' }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Remarks-->

        </div>
    </div>

    @include('pages.project-monitoring.fleet.summary.modals.submit-approval')
    @include('pages.project-monitoring.fleet.summary.modals.submit-approval-warning')
    @include('pages.project-monitoring.fleet.summary.modals.approval-history')
    @include('pages.approval.status.modals.approve')
    @include('pages.approval.status.modals.reject')
    @include('pages.project-monitoring.fleet.completion.modals.summary-project')
    @include('pages.project-monitoring.fleet.completion.modals.submit-approval')
    @include('pages.project-monitoring.fleet.completion.modals.submit-approval-warning')
@endsection

@push('page-scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();
            $('#approval_table').DataTable({
                responsive: true,
                bFilter: false,
                paging: false,
                bInfo: false,
            });
        });

        $(document).on('click', '.btn-approval-history', function() {
            $.ajax({
                url: "{{ route('ipm.fleet.summary.history-approval', $data['project']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    $('#approval_data').empty();
                    var data = '';
                    $.each(response?.data, function(key, value) {
                        data += `
                            <tr>
                                <td class="text-center">${key + 1}</td>
                                <td>${value?.name}</td>
                                <td>${value?.status}</td>
                                <td class="text-center">${value?.tanggal}</td>
                                <td>${value?.keterangan ?? '-'}</td>
                            </tr>
                        `;
                    });

                    $('#approval_data').html(data);
                    $('#approval_history').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    });
                }
            });
        });

        function checkSession() {
            if ("{{ Session::has('success') }}") {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if ("{{ Session::has('failed') }}") {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>
    @stack('modal-scripts')
@endpush
