<!DOCTYPE html>
<html>

@inject('ctrl', 'App\Http\Controllers\Controller')

<head>
    <title> {{ $data['file_name'] }} </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        @page {
            size: A4 landscape;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 15;
        }

        .table-data td,
        .table-data th {
            border: 1px solid black;
            padding: 8px;
            font-size: 15;
        }

        .row {
            display: flex;
            flex: wrap;
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        .col-6 {
            width: 50%
        }

        .bordered {
            border: solid;
        }

        .float-right {
            float: right;
        }

        .fw-bold {
            font-weight: bold;
        }

        .borderless {
            border: 0px;
        }

        .text-center {
            text-align: center;
        }

        .w-100 {
            width: 100%;
        }

        .h-300px {
            height: 300px;
        }
    </style>

</head>

<body>
    <table class="mb-10">
        <tr>
            <td>
                <img alt="Logo" src="{{ $ctrl->base64Logo() }}" style="width:150px; height:auto" />
            </td>
            <td style="text-align: center; margin: auto; font-style: italic;">
                <h1 class="fw-bold">Executive Summary Project Report</h1>
                <h2 class="fw-bold">{{ $data['project']['nama_project'] }}</h2>
                <h4 class="fw-bold">{{ date('d F Y', strtotime($data['latest_progress']['tgl_update'] ?? $data['project']['updated_at'])) }}</h4>
            </td>
        </tr>
    </table>

    <div class="row mb-10" style="padding: 12px;">
        <div class="col-6">
            <table class="table-data">
                <tr>
                    <td style="width: 150px">Project Name</td>
                    <td> : {{ $data['project']['nama_project'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Contractor</td>
                    <td> : {{ $data['project']['kontraktor'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Contract No</td>
                    <td> : {{ $data['project']['nomor_kontrak'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Contract Status</td>
                    <td> : {{ $data['project']['status'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Supervisor</td>
                    <td> : {{ $data['project']['supervisor'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Project Value</td>
                    <td> : {{ toRupiah($data['project']['nilai_project'] ?? 0) }}</td>
                </tr>
                <tr>
                    <td>Project Periode</td>
                    <td> : {{ $data['periode_progress'] . ' ' . strtoupper($data['project']['jenis_periode']) }}</td>
                </tr>
                <tr>
                    <td class="text-right">Start</td>
                    <td> : {{ $data['first_basic_data']['mulai_kontrak'] ?? '' }}</td>
                </tr>
                <tr>
                    <td class="text-right">End</td>
                    <td> : {{ $data['latest_basic_data']['selesai_kontrak'] ?? '' }}</td>
                </tr>
                <tr>
                    <td>Update Up To</td>
                    <td> : {{ $data['latest_basic_data']['mulai_kontrak'] ?? '' }} s/d {{ $data['latest_basic_data']['selesai_kontrak'] ?? '' }}</td>
                </tr>
            </table>
        </div>
        <div class="col-6">
            <table class="table-data">
                <thead style=" text-align: center;">
                    <tr>
                        <th colspan="2"><b>Cost Performances Index</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align: center; color: #28C6C2;"><b>Cost Performances Index</b></td>
                        <td> : {{ $data['cost_calculate']['cpi'] }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: center; color: #28C6C2;"><b>Actual Cost</b></td>
                        <td> : {{ $data['cost_calculate']['actual_cost'] }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: center; color: #28C6C2;"><b>Contruction Value</b></td>
                        <td> : {{ $data['cost_calculate']['construction_value'] }}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table-data" style="margin-top: 80px;">
                <thead style="text-align: center;">
                    <tr>
                        <th><b>Estimate Time Completion {{ $data['project']['jenis_periode'] }}</b></th>
                    </tr>
                </thead>
                <tbody style="text-align: center; color: #28C6C2; font-size: 85;">
                    <tr>
                        <td> {{ $data['cost_calculate']['etc'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @foreach ($data['schedule_progress'] as $idx => $progress)
        <table class="table-data text-center mb-10">
            <thead class="fw-bold">
                <tr>
                    <th colspan="16">{{ __('Time Schedule Progress (%)') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr class="fw-bold">
                    <td>Periode</td>
                    @for ($i = 0; $i < 15; $i++)
                        <td>{{ $i + 1 + 15 * $idx }}</td>
                    @endfor
                </tr>
                @foreach ($progress as $key => $progres)
                    <tr>
                        <td>{{ $key }}</td>
                        @for ($j = 0; $j < 15; $j++)
                            <td>{{ $progres[$j] ?? '-' }}</td>
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
    <table class="table-data text-center mb-10">
        <thead class="fw-bold">
            <tr>
                <th colspan="10">{{ __('Overview Detail Progress (%)') }}</th>
            </tr>
            <tr>
                <th rowspan="2">{{ __('No') }}</th>
                <th rowspan="2">{{ __('Task Description') }}</th>
                <th rowspan="2">{{ __('Bobot (%)') }}</th>
                <th colspan="4">{{ __('New Periode (%)') }}</th>
                <th colspan="3">{{ __('Total Progress (%)') }}</th>
            </tr>
            <tr>
                <th>{{ __('Plan') }}</th>
                <th>{{ __('Last Actual') }}</th>
                <th>{{ __('This Actual') }}</th>
                <th>{{ __('Deviasi') }}</th>
                <th>{{ __('Plan') }}</th>
                <th>{{ __('Actual') }}</th>
                <th>{{ __('Deviasi') }}</th>
            </tr>
        </thead>
        <tbody>
            @if (count($data['item_progress']) > 0)
                @php
                    $totalPlan = 0;
                    $totalActual = 0;
                    $totalDeviasi = 0;
                @endphp
                @foreach ($data['item_progress'] as $item)
                    <tr class="fw-bold">
                        <td>#</td>
                        <td>{{ $item['main_item'] }}</td>
                        <td>{{ $item['batas_bobot'] }}</td>
                        <td>{{ $item['plan'] }}</td>
                        <td>{{ $item['progres_lalu'] }}</td>
                        <td>{{ $item['progres_ini'] }}</td>
                        <td>{{ $item['deviasi'] }}</td>
                        <td>{{ toDecimal($item['plan'], 1) }}</td>
                        <td>{{ toDecimal($item['progres_ini'], 1) }}</td>
                        <td>{{ toDecimal($item['deviasi'], 1) }}</td>
                    </tr>
                    @php
                        $totalPlan += $item['plan'];
                        $totalActual += $item['progres_ini'];
                        $totalDeviasi += $item['deviasi'];
                    @endphp
                    @if (!empty($item['sub_item']))
                        @foreach ($item['sub_item'] as $subitem)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $subitem['sub_item'] }}</td>
                                <td>{{ $subitem['bobot'] }}</td>
                                <td>{{ $subitem['plan'] }}</td>
                                <td>{{ $subitem['progres_lalu'] }}</td>
                                <td>{{ $subitem['progres_ini'] }}</td>
                                <td>{{ $subitem['deviasi'] }}</td>
                                <td>{{ toDecimal($subitem['plan'], 1) }}</td>
                                <td>{{ toDecimal($subitem['progres_ini'], 1) }}</td>
                                <td>{{ toDecimal($subitem['deviasi'], 1) }}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
                <tr>
                    <td colspan="7">
                        {{ __('Total Progress Main Item (%)') }}
                    </td>
                    <td>{{ toDecimal($totalPlan, 1) }}</td>
                    <td>{{ toDecimal($totalActual, 1) }}</td>
                    <td>{{ toDecimal($totalDeviasi, 1) }}</td>
                </tr>
            @endif
        </tbody>
    </table>
    @foreach ($data['schedule_quality'] as $idx => $qualities)
        <table class="table-data text-center mb-10">
            <thead class="fw-bold">
                <tr>
                    <th colspan="16">{{ __('Time Schedule Quality (%)') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr class="fw-bold">
                    <td>Periode</td>
                    @for ($i = 0; $i < 15; $i++)
                        <td>{{ $i + 1 + 15 * $idx }}</td>
                    @endfor
                </tr>
                @foreach ($qualities as $key => $quality)
                    <tr>
                        <td>{{ $key }}</td>
                        @for ($j = 0; $j < 15; $j++)
                            <td>{{ $quality[$j] ?? '-' }}</td>
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
    <table class="table-data text-center mb-10 w-100">
        <thead class="fw-bold">
            <tr>
                <th>{{ __('Visual Quality Monitoring (%)') }}</th>
            </tr>
            <tr>
                <th>{{ __('QUALITY COMPLETION') }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size: 60px;">{{ __('0%') }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table-data text-center mb-10">
        <thead class="fw-bold">
            <tr>
                @if (count($data['documentations']) > 0)
                    <th colspan="3">{{ __('Documentation') }}</th>
                @else
                    <th>{{ __('Documentation') }}</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @forelse($data['documentations'] as $documents)
                <tr>
                    @foreach ($documents as $document)
                        <td style="text-align: center; vertical-align: middle; width: 33.3333333333%;">
                            <img src="{{ $document['file_base64'] }}" class="rounded h-300px" alt="{{ $document['nama'] }}" />
                            <p style="margin: 5px 0 0 0;">{{ $document['nama'] }}</p>
                        </td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
    <table class="table-data mb-10">
        <thead class="text-center fw-bold">
            <tr>
                <th colspan="6">{{ __('Recomendation') }}</th>
            </tr>
            <tr>
                <th style="width: 50px;">{{ __('No') }}</th>
                <th>{{ __('Notice') }}</th>
                <th>{{ __('Pending Matters') }}</th>
                <th>{{ __('Execute By') }}</th>
                <th style="width: 150px;">{{ __('Due Date') }}</th>
                <th style="width: 100px;">{{ __('Status') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['recomendations'] as $key => $recomendation)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</td>
                    <td>{{ $recomendation->notice }}</td>
                    <td>{{ $recomendation->to_do_list }}</td>
                    <td>{{ $recomendation->pic }}</td>
                    <td class="text-center">{{ $recomendation->due_date }}</td>
                    <td class="text-center">{{ strtoupper($recomendation->status) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <table class="table-data text-center mb-10">
        <thead class="fw-bold">
            <tr>
                <th class="w-100">{{ __('Remarks') }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $data['latest_progress']['remarks'] ?? '' }}</td>
            </tr>
        </tbody>
    </table>
</body>

</html>
