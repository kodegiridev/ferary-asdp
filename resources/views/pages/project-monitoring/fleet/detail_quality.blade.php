@extends('layout.demo1.master')

@section('styles')
    @stack('page-styles')
@endsection

@section('content-module')
    <!--begin::Breadcrumb-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            @yield('title-breadcrumb')
        </div>
    </div>
    <!--end::Breadcrumb-->

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <!--begin::Tab-->
            @yield('page-project')
            <?php if (!empty($project_id)) {
                $data['project']['id'] = $project_id;
            } ?>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a href="{{ route('ipm.fleet.progress.index', $data['project']['id']) }}" class="nav-link @yield('progress-tab')">
                        PROGRESS
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ipm.fleet.quality.index', $data['project']['id']) }}" class="nav-link @yield('quality-tab')">
                        QUALITY
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ipm.fleet.document.index', $data['project']['id']) }}" class="nav-link @yield('document-tab')">
                        DOCUMENT
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ipm.fleet.recomendation.index', $data['project']['id']) }}" class="nav-link @yield('recomendation-tab')">
                        RECOMENDATION
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ipm.fleet.summary.index', $data['project']['id']) }}" class="nav-link @yield('summary-tab')">
                        EXECUTIVE SUMMARY
                    </a>
                </li>
            </ul>
            <!--end::Tab-->

            <!--begin::Tab Content-->
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="card shadow-sm">
                        <div class="card-header" style="background-color: #00a5bb">
                            <h3 class="card-title">{{ !empty($title) ? $title : $data['project']['nama_project'] ?? 'Docking KMP Aceh' }}</h3>
                            <div class="card-toolbar">
                                <span class="badge badge-light-primary">{{ __('Fleet') }}</span>
                            </div>
                        </div>

                        <!--begin::Page Content-->
                        <div class="card-body">
                            @yield('page-content')
                        </div>
                        <!--end::Page Content-->
                        <div class="card-footer">
                            @yield('page-footer')
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Tab Content-->
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            checkSession();
        });

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }

            if ($('.modal .is-invalid').length > 0) {
                var action = "{{ old('action') }}";
                $(`.btn_${action}`).trigger('click');

                Swal.fire({
                    text: `Data yang diinputkan tidak valid`,
                    icon: "error",
                });
            }
        }
    </script>

    @stack('page-scripts')
@endsection
