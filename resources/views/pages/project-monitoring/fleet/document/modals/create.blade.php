<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_document_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.document.store', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create_document_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Add Document') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Main Subject') }}</label>
                        <select class="form-control form-control-solid document_type @error('jenis_dokumen') is-invalid @enderror" name="jenis_dokumen" data-control="select2" data-placeholder="Pilih jenis dokumen">
                            @foreach ($data['types'] as $type)
                                <option value="{{ $type }}" @if ($type == old('jenis_dokumen')) selected @endif>
                                    {{ $type }}
                                </option>
                            @endforeach
                        </select>
                        @error('jenis_dokumen')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3" id="content_sub_document_type">
                        <label class="form-label">{{ __('Sub Subject') }}</label>
                        <div id="sub_document_type"></div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Sub Subject (Max. 8)') }}</label>
                        <!--begin::Repeater-->
                        <div id="create_sub_dokumen">
                            <div data-repeater-list="dokumen_tambahan">
                                <div data-repeater-item>
                                    @forelse(old('dokumen_tambahan') ?? [] as $dokumen)
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="nama_dokumen" value="{{ $dokumen['nama_dokumen'] }}" placeholder="Enter the additional sub subject" />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="form-group row mb-3">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="nama_dokumen" placeholder="Enter the additional sub subject" />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                    {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                </a>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>

                            <div class="form-group mt-3 col-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                    Add
                                </a>
                            </div>
                        </div>
                        <!--end::Repeater-->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Tambah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('modal-scripts')
    <script>
        $(document).ready(function() {
            var selected = 'Project Basic';
            if ("{{ old('jenis_dokumen') }}" != '') {
                selected = "{{ old('jenis_dokumen') }}";
            }

            optionDocumentType(selected);
        });

        $('.document_type').change(function() {
            var selected = $(".document_type option:selected").val();
            optionDocumentType(selected);
        });

        $('#create_sub_dokumen').repeater({
            initEmpty: false,
            show: function() {
                $(this).slideDown();
                checkAndHideButtonOnCreate(8);
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
                checkAndHideButtonOnCreate(9);
            }
        });

        function checkAndHideButtonOnCreate(max) {
            var itemElements = $('#create_sub_dokumen [data-repeater-item]');
            var addButton = $('#create_sub_dokumen [data-repeater-create]');

            if (itemElements.length >= max) {
                addButton.hide();
            } else {
                addButton.show();
            }
        }

        function optionDocumentType(documentType) {
            var url = `{{ route('ipm.fleet.document.select-type', $data['project']['id']) }}?type=${encodeURIComponent(documentType)}`;

            $.ajax({
                type: 'GET',
                url: url,
                success: function(data) {
                    if (data.length > 0) {
                        var types = '';

                        $.each(data, function(key, value) {
                            if (key % 2 === 0) {
                                types += '<div class="row p-3">';
                            }

                            types += `
                                <div class="col-md-6 col-12 form-check">
                                    <input class="form-check-input" name="sub_dokumen[]" type="checkbox" value="${value}" />
                                    <label>
                                        ${value}
                                    </label>
                                </div>
                            `;

                            if (key % 2 !== 0 || key === data.length - 1) {
                                types += '</div>';
                            }
                        });
                        $('#sub_document_type').html(types);
                        $('#content_sub_document_type').show();
                    } else {
                        $('#sub_document_type').empty();
                        $('#content_sub_document_type').hide();
                    }
                }
            });
        }
    </script>
@endpush
