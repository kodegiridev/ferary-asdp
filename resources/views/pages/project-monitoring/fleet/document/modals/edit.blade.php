<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_document_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.document.update', $data['project']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit_document_modal" name="action">
                <input type="hidden" name="jenis_dokumen">
                <div class="modal-header">
                    <h3 class="modal-title">
                        {{ __('Ubah ') }}
                        <span id="modal_title"></span>
                    </h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3" id="content_edit_sub_document">
                        <label class="form-label">{{ __('Sub Subject') }}</label>
                        <div id="edit_sub_document"></div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Sub Subject') }} <span id="max_limit"></span></label>
                        <!--begin::Repeater-->
                        <div id="edit_sub_dokumen">
                            <div data-repeater-list="dokumen_tambahan">
                                <div data-repeater-item>
                                    <div class="form-group row mb-3">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="nama_dokumen" placeholder="Enter the additional sub subject" />
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-2">
                                                {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mt-3 col-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                    {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                    Add
                                </a>
                            </div>
                        </div>
                        <!--end::Repeater-->
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('modal-scripts')
    <script>
        var maxSubDocument = 8;

        $(document).on('click', '.btn_edit_document_modal', function() {
            var url = `{{ route('ipm.fleet.document.show', [':project_id', ':jenis_dokumen']) }}`;
            url = url.replace(':project_id', "{{ $data['project']['id'] }}");
            url = url.replace(':jenis_dokumen', encodeURIComponent($(this).data('id')));

            $.ajax({
                url: url,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    $('#edit_document_modal input[name="jenis_dokumen"]').val(data?.dokumen?.jenis_dokumen);
                    $('#modal_title').text(data?.dokumen?.jenis_dokumen);

                    var types = '';
                    var i = 0;
                    $.each(data?.sub_dokumen, function(key, value) {
                        if (i % 2 === 0) {
                            types += '<div class="row p-3">';
                        }

                        types += `
                            <div class="col-md-6 col-12 form-check">
                                <input class="form-check-input" name="sub_dokumen[]" type="checkbox" value="${value}" />
                                <label>
                                    ${value}
                                </label>
                            </div>
                        `;

                        if (i % 2 !== 0 || i === data.length - 1) {
                            types += '</div>';
                        }

                        i++;
                    });

                    if (data?.dokumen_tambahan?.length) {
                        maxSubDocument = 8 - data?.dokumen_tambahan?.length;
                    }

                    $('#max_limit').text(`(max. ${maxSubDocument})`);
                    $('#edit_sub_document').html(types);
                    $('#content_edit_sub_document').show();
                    $('#edit_document_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        text: err?.responseText,
                        icon: "error"
                    });
                }
            });
        });

        $('#edit_sub_dokumen').repeater({
            initEmpty: false,
            show: function() {
                $(this).slideDown();
                checkAndHideButtonOnEdit(maxSubDocument);
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
                checkAndHideButtonOnEdit(maxSubDocument + 1);
            }
        });

        function checkAndHideButtonOnEdit(max) {
            var itemElements = $('#edit_sub_dokumen [data-repeater-item]');
            var addButton = $('#edit_sub_dokumen [data-repeater-create]');

            if (itemElements.length >= max) {
                addButton.hide();
            } else {
                addButton.show();
            }
        }
    </script>
@endpush
