<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_sub_document_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('ipm.fleet.document.sub.update', [$data['project']['id'], $data['sub_dokumen']['id']]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit_sub_document_modal" name="action">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ubah Dokumen') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="form-label required">{{ __('Judul Dokumen') }}</label>
                        <input type="text" class="form-control form-control-solid @error('judul_dokumen') is-invalid @enderror" name="judul_dokumen" required/>
                        @error('judul_dokumen')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Status Pemenuhan') }}</label>
                        <select class="form-control form-control-solid status_pemenuhan @error('status_pemenuhan') is-invalid @enderror" name="status_pemenuhan" id="edit_status_pemenuhan" data-control="select2" data-placeholder="Pilih jenis dokumen" required>
                        </select>
                        @error('status_pemenuhan')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">{{ __('Remarks') }}</label>
                        <input type="text" class="form-control form-control-solid @error('remarks') is-invalid @enderror" name="remarks" />
                        @error('remarks')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 no_dokumen">
                        <label class="form-label required">{{ __('Nomor Dokumen') }}</label>
                        <input type="text" class="form-control form-control-solid @error('nomor_dokumen') is-invalid @enderror" name="nomor_dokumen" />
                        @error('nomor_dokumen')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 file_dokumen">
                        <label class="form-label required">{{ __('Dokumen') }}</label>
                        <input type="file" class="form-control form-control-solid @error('file') is-invalid @enderror" name="file" accept=".pdf" />
                        @error('file')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
