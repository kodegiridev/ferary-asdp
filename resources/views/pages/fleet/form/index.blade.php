@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <style>
        .form-radio-button {
            border: 1.75px solid var(--bs-gray-800) !important;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Fleet Standard Parameter') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.fleet.index') }}" class="text-muted text-hover-primary">
                            {{ __('Fleet') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Form') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data Fleet Form') }}</h3>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-sm btn-primary btn_create_document_modal" data-bs-toggle="modal" data-bs-target="#create_form_modal">
                            <i class="ki-duotone ki-plus fs-1"></i>
                            {{ __('Tambah') }}
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="form_table">
                            <thead>
                                <tr>
                                    <th class="w-35px">{{ __('No') }}</th>
                                    <th class="text-center">{{ __('Fleet Form') }}</th>
                                    <th class="w-200px">{{ __('Proportional Value') }}</th>
                                    <th class="w-150px">{{ __('Status') }}</th>
                                    <th class="w-200px">{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('fleet.fleet.index') }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    @include('pages.fleet.form.modals.create')
    @include('pages.fleet.form.modals.edit')
    @include('pages.fleet.form.modals.sub-group.index')
    @include('pages.fleet.form.modals.sub-group.edit')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        function initInputMaskValue() {
            Inputmask({
                rightAlign: false,
                alias: "numeric",
                min: 1,
                max: 100,
            }).mask(".input-value");
        }

        function valueValidationEvent() {
            var totalValue = 0;
            var fields = document.querySelectorAll(`#create_form_modal .subgroup-value`);
            fields?.forEach(function(field) {
                totalValue += parseInt(field?.value) || 0;
            });

            $(`#create_form_modal .invalid-remaining-value`).remove();
            $(`#create_form_modal .create_value_sub`).text(totalValue);
            if (totalValue > 100) {
                $(`#create_form_modal :submit`).prop('disabled', true);
                $(`#create_form_modal .subgroup-value`).addClass('is-invalid');
                $(`#create_form_modal .subgroup-remaining`).addClass('text-danger');
                $(`#create_form_modal .subgroup-value`).after(`
                    <div class="row invalid-remaining-value">
                        <span class="text-danger mt-1">
                            <strong>Total value melebihi batas maksimum</strong>
                        </span>
                    </div>
                `);
            } else {
                $(`#create_form_modal :submit`).prop('disabled', false);
                $(`#create_form_modal .subgroup-value`).removeClass('is-invalid');
                $(`#create_form_modal .subgroup-remaining`).removeClass('text-danger');
            }
        }

        function formDatatable() {
            $('#form_table').DataTable({
                proccesing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('fleet.form.index', $data['fleet']['id']) }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'value',
                        name: 'value'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 2, 3, 4],
                        className: 'text-center'
                    },
                ]
            });
        }

        function initRepeater() {
            $('#add_sub_group').repeater({
                initEmpty: false,
                show: function() {
                    $(this).slideDown();
                    $(this).find(`.subgroup-value`).on('keyup', valueValidationEvent);

                    initInputMaskValue();
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                },
                ready: function() {
                    $(`.subgroup-value`).on('keyup', valueValidationEvent);

                    initInputMaskValue();
                }
            });
        }

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }

        var subGroupTable;

        function subGroupDatatable(systemId = null) {
            subGroupTable = $('#datatable_sub_group').DataTable({
                proccesing: true,
                serverSide: true,
                destroy: true,
                lengthChange: false,
                pageLength: 5,
                ajax: {
                    url: "{{ route('fleet.form.sub-group.index', $data['fleet']['id']) }}",
                    data: function(d) {
                        d.mst_ercm_vessel_system_id = systemId;
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 2],
                        className: 'text-center'
                    },
                ]
            });
        }

        $(document).ready(function() {
            checkSession();
            $('#create_value').text(parseInt("{{ $data['used_value'] }}"));
        });

        formDatatable();
        initRepeater();

        $(document).on('click', '.btn-edit', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "{{ route('fleet.form.show', $data['fleet']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function(response) {
                    $('#edit_form_modal input[name="id"]').val(response?.data?.id);
                    $('#edit_form_modal input[name="name"]').val(response?.data?.name);
                    $('#edit_form_modal input[name="value"]').val(response?.data?.value);
                    $('#edit_form_modal input[name="temp_value"]').val(response?.used_value);

                    var total = parseInt(response?.data?.value) + parseInt(response?.used_value);
                    $('#edit_value').text(total);
                    $('#edit_form_modal').modal('show');
                }
            })
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('fleet.form.delete', $data['fleet']['id']) }}",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('keyup', '#create_form_modal input[name="value"]', function() {
            var usedValue = parseInt("{{ $data['used_value'] }}");
            var inputValue = parseInt($(this).val() || 0);
            var totalValue = inputValue + usedValue;

            if (totalValue > 100) {
                $(this).addClass('is-invalid');
                var error = `<span class="text-danger">Melebihi batas maksimum!</span>`;
                $('#create_form_modal .invalid_value').html(error);
            } else {
                $(this).removeClass('is-invalid');
                $('#create_form_modal .invalid_value').empty();
            }

            $('#create_value').text(totalValue);
        });

        $(document).on('keyup', '#edit_form_modal input[name="value"]', function() {
            var usedValue = parseInt($('#edit_form_modal input[name="temp_value"]').val());
            var inputValue = parseInt($(this).val() || 0);
            var totalValue = inputValue + usedValue;

            if (totalValue > 100) {
                $(this).addClass('is-invalid');
                var error = `<span class="text-danger">Melebihi batas maksimum!</span>`;
                $('#edit_form_modal .invalid_value').html(error);
            } else {
                $(this).removeClass('is-invalid');
                $('#edit_form_modal .invalid_value').empty();
            }

            $('#edit_value').text(totalValue);
        });

        $(document).on('keyup', '#sub_group_edit_modal input[name="value"]', function() {
            var usedValue = parseInt($('#sub_group_edit_modal input[name="sub_used_value"]').val());
            var inputValue = parseInt($(this).val() || 0);
            var totalValue = inputValue + usedValue;

            $(`#sub_group_edit_modal .create_value_sub`).text(totalValue);
            if (totalValue > 100) {
                $(this).addClass('is-invalid');
                var error = `<span class="text-danger invalid-remaining-value">Total value melebihi batas maksimum</span>`;
                $('#sub_group_edit_modal .subgroup-value').after(error);
                $(`#sub_group_edit_modal .subgroup-remaining`).addClass('text-danger');
            } else {
                $(this).removeClass('is-invalid');
                $('#sub_group_edit_modal .invalid-remaining-value').empty();
                $(`#sub_group_edit_modal .subgroup-remaining`).removeClass('text-danger');
            }
        });

        $(document).on('click', '.btn_sub_group', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "{{ route('fleet.form.show', $data['fleet']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                },
                success: function(response) {
                    subGroupDatatable(id);

                    $('#sub_group_modal input[name="mst_ercm_vessel_system_id"]').val(id);
                    $('#sub_group_modal .modal-title').text(`Sub Group - ${response?.data?.name}`);
                    $('#sub_group_modal').modal('show');
                },
                error: function(err) {
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    })
                }
            })
        });

        $(document).on('click', '.btn_edit_sub_group', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "{{ route('fleet.form.sub-group.show', $data['fleet']['id']) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                },
                success: function(response) {
                    $('#sub_group_edit_modal input[name="id"]').val(response?.data?.id);
                    $('#sub_group_edit_modal input[name="name"]').val(response?.data?.name);
                    $('#sub_group_edit_modal input[name="value"]').val(response?.data?.value);
                    $('#sub_group_edit_modal input[name="sub_used_value"]').val(response?.used_value);
                    $('#sub_group_edit_modal input[name="mst_ercm_vessel_system_id"]').val(response?.data?.mst_ercm_vessel_system_id);

                    var total = parseInt(response?.data?.value) + parseInt(response?.used_value);
                    $('#sub_group_edit_modal .create_value_sub').text(total);
                    $('#sub_group_edit_modal').modal('show');
                    $('#sub_group_modal').modal('hide');
                },
                error: function(err) {
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    })
                }
            })
        });

        $(document).on('submit', '.form-sub-group', function(e) {
            e.preventDefault();

            var modal = $(this).find('input[name="modal"]').val();
            var systemId = $(this).find('input[name="mst_ercm_vessel_system_id"]');
            var id = $(this).find('input[name="id');

            $.ajax({
                url: $(this).find('input[name="url"]').val(),
                type: 'POST',
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    Swal.fire({
                        text: response?.message,
                        icon: "success",
                    });

                    $(`#${modal}`).find('.form-sub-group')[0].reset();
                    $(`#${modal}`).modal('hide');
                    $('#sub_group_modal').modal('show');

                    if (subGroupTable) {
                        subGroupTable.ajax.reload();
                    }
                },
                error: function(error) {
                    $(`#${modal}`).find('.form-sub-group')[0].reset();
                    $(`#${modal}`).modal('hide');
                    $('#sub_group_modal').modal('show');

                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            })
        });
    </script>

    @stack('modal-scripts')
@endsection
