<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('fleet.form.store', $data['fleet']['id']) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Form Tambah ') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card border shadow-sm mb-8">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('System Parameter') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group mb-3">
                                <label class="required form-label">{{ __('Fleet Form') }}</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Form" required />
                            </div>
                            <div class="form-group mb-3">
                                <label class="required form-label">{{ __('Proportional Value') }}</label>
                                <label class="form-label">({{ __('Remaining') }} <span id="create_value"></span>/100)</label>
                                <input type="text" class="form-control input-value" name="value" value="{{ old('value') }}" placeholder="Masukan proportional value" required />
                                <div class="invalid_value"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card border shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Sub Group') }}</h3>
                        </div>
                        <div class="card-body">
                            <div id="add_sub_group">
                                <div data-repeater-list="mst_ercm_vessel_subgroups">
                                    <div data-repeater-item>
                                        <div class="border rounded p-3 mb-5">
                                            <div class="form-group row mb-3">
                                                <div class="col-md-10 col-10">
                                                    <label class="form-label">{{ __('Sub Group') }}</label>
                                                    <input type="text" class="form-control" name="name" placeholder="Masukan nama sub group" />
                                                </div>
                                                <div class="col-md-2 col-2">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-icon btn-light-danger mt-3 mt-md-9" style="float: right;">
                                                        {!! theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') !!}
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="form-group col-10">
                                                <label class="form-label">{{ __('Proportional Value') }}</label>
                                                <label class="form-label subgroup-remaining">({{ __('Remaining') }} <span class="create_value_sub">0</span>/100)</label>
                                                <input type="text" class="form-control input-value subgroup-value" name="value" placeholder="Masukan proportional value" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group mt-3 col-12">
                                    <a href="javascript:;" data-repeater-create class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">
                                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr087.svg', 'svg-icon-3') !!}
                                        Add
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-danger" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
