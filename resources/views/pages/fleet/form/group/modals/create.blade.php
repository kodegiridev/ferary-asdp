<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="create_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="{{ route('fleet.form.group.store', [$data['fleet']['id'], $data['form']['id']]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Tambah Group') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Group') }}</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Masukan nama group" />
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Proportional Value') }}</label>
                        <label class="form-label">({{ __('Remaining') }} <span id="create_value"></span>/100)</label>
                        <input type="number" class="form-control @error('value') is-invalid @enderror" name="value" value="{{ old('value') }}" placeholder="Masukan proportional value" required />
                        <div class="invalid_value"></div>
                        @error('value')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12 form-group">
                            <label class="form-label">{{ __('3D File (.gtlf, .obj)') }}</label>
                            <input type="file" class="form-control" accept=".glb, .gltf, .obj" name="object_3d">
                        </div>
                        <div class="col-md-6 col-12 form-group">
                            <label class="form-label">{{ __('Import Data (.xlx)') }}</label>
                            <input type="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" name="excel_file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
