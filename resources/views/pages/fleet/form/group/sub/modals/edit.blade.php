<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="edit_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="/" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit_form_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Ubah Sub Component') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Sub Component') }}</label>
                        <input type="text" class="form-control form-control-solid @error('form_name') is-invalid @enderror" name="form_name" value="{{ old('form_name') ?? 'Konstruksi Lambung' }}" placeholder="Nama Form" />
                        @error('form_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Proportional Value') }}</label>
                        <input type="text" class="form-control form-control-solid @error('proportional_value') is-invalid @enderror" name="proportional_value" value="{{ old('proportional_value') ?? '15' }}" placeholder="Nilai proportional value" />
                        @error('proportional_value')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
