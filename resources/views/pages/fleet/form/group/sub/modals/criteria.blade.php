<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="criteria_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" action="/" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="criteria_form_modal" name="action">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Criteria') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-3') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card card-custom shadow-sm mb-10">
                        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#initial_form">
                            <h3 class="card-title">{{ __('Initial Condition') }}</h3>
                            <div class="card-toolbar rotate-180">
                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr073.svg', 'svg-icon-1') !!}
                            </div>
                        </div>
                        <div id="initial_form" class="collapse">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Success Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Warning Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Danger Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <input class="form-check-input" type="checkbox" />
                                            <label>Input</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <input class="form-check-input" type="checkbox" />
                                            <label>Reverse Scoring</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-custom shadow-sm mb-10">
                        <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse" data-bs-target="#defect_form">
                            <h3 class="card-title">{{ __('Defect: Thickness') }}</h3>
                            <div class="card-toolbar rotate-180">
                                {!! theme()->getSvgIcon('icons/duotune/arrows/arr073.svg', 'svg-icon-1') !!}
                            </div>
                        </div>
                        <div id="defect_form" class="collapse">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Success Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Warning Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="required form-label">{{ __('Danger Value') }}</label>
                                            <div class="row p-0">
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                                <div class="col-md-2 col-12 d-flex justify-content-center align-items-center">
                                                    <span class="fw-bold">-</span>
                                                </div>
                                                <div class="col-md-5 col-12">
                                                    <input type="text" class="form-control form-control-solid" placeholder="Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <label class="form-label">{{ __('Remarks') }}</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Free text" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <input class="form-check-input" type="checkbox" />
                                            <label>Input</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 mb-3">
                                        <div class="form-group">
                                            <input class="form-check-input" type="checkbox" />
                                            <label>Reverse Scoring</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
