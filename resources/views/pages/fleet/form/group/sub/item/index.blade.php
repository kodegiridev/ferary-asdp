@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <style>
        .form-check-input {
            border: 1.75px solid var(--bs-gray-700) !important;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Kontruksi Lambung: Komponen 1') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.fleet.index') }}" class="text-muted text-hover-primary">
                            {{ __('Fleet') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.index', $data['fleet']['id']) }}" class="text-muted text-hover-primary">
                            {{ __('Form') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.component.index', [$data['fleet']['id'], 1]) }}" class="text-muted text-hover-primary">
                            {{ __('Konstruksi Lambung') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Komponen 1') }}
                        <a href="{{ route('fleet.form.component.sub.index', [$data['fleet']['id'], 1, 1]) }}" class="text-muted text-hover-primary">
                            {{ __('Komponen 1') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="component_table">
                            <thead>
                                <tr class="text-center">
                                    <th class="w-35px">{{ __('No') }}</th>
                                    <th>{{ __('Item') }}</th>
                                    <th class="w-200px">{{ __('Proportional Value') }}</th>
                                    <th class="w-175px">{{ __('Parameter') }}</th>
                                    <th class="w-150px">{{ __('Criteria') }}</th>
                                    <th class="w-100px">{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('fleet.form.component.sub.index', [$data['fleet']['id'], 1, 1, 1]) }}" class="btn btn-sm btn-info">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr022.svg', 'svg-icon-2') !!}
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    @include('pages.fleet.form.component.sub.item.modals.edit')
    @include('pages.fleet.form.component.sub.item.modals.parameter')
    @include('pages.fleet.form.component.sub.item.modals.criteria')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        var table = $('#component_table').DataTable({
            proccesing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('fleet.form.component.sub.item.index', [$data['fleet']['id'], 1, 1, 1]) }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'component_name',
                    name: 'component_name'
                },
                {
                    data: 'proportional_value',
                    name: 'proportional_value'
                },
                {
                    data: 'parameter',
                    name: 'parameter',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'criteria',
                    name: 'criteria',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [{
                    target: '_all',
                    className: 'align-middle'
                },
                {
                    target: [0, 2, 3, 4, 5],
                    className: 'text-center'
                },
            ]
        });

        $(document).on('click', '.btn-parameter', function() {
            $('#parameter_form_modal').modal('show');
        });

        $(document).on('click', '.btn-criteria', function() {
            $('#criteria_form_modal').modal('show');
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endsection
