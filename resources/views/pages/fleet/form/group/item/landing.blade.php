@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <!-- Babylon.js -->
    @include('/layout/partials/babylon')

    <style>
        .form-checkbox-button {
            border: 1.75px solid var(--bs-gray-800) !important;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Group') }}: {{ $data['group']['name'] }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.fleet.index') }}" class="text-muted text-hover-primary">
                            {{ __('Fleet') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.index', $data['fleet']['id']) }}" class="text-muted text-hover-primary">
                            {{ __('Form') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.group.index', [$data['fleet']['id'], $data['form']['id']]) }}" class="text-muted text-hover-primary">
                            {{ __('Group') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ $data['group']['name'] }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Component Data') }}</h3>
                            <div class="card-toolbar"></div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="component_table">
                                    <thead>
                                        <tr>
                                            <td class="text-center w-50px">No</td>
                                            <td class="text-center">Component</td>
                                            <td class="text-center w-50px">Select</td>
                                        </tr>
                                    </thead>
                                    <tbody id="3d_component"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('3D Image') }}</h3>
                            <div class="card-toolbar">
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="canvasZone"><canvas id="modelCanvas" style="height: 60vh;width: 100%;touch-action: none;"></canvas></div>
                        </div>
                    </div>
                    <div class="card shadow-sm mt-7">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Grouped Component') }}</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap" id="grouped_table">
                                    <thead>
                                        <tr class="text-center">
                                            <th>{{ __('No') }}</th>
                                            <th>{{ __('Sub Group') }}</th>
                                            <th>{{ __('Component Qty') }}</th>
                                            <th class="w-200px">{{ __('Standard') }}</th>
                                            <th class="w-100px">{{ __('Aksi') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(async function() {
            $('#grouped_table').DataTable({
                bInfo: false
            });
        });

        const canvas = document.getElementById("modelCanvas");
        var path = "{{ asset('upload/master-ercm/fleet') }}";
        var file = "{{ $data['group']['attachment'] }}";
        var engine = null;
        var scene = null;
        var sceneToRender = null;
        var startRenderLoop = function(engine, canvas) {
            engine.runRenderLoop(function() {
                if (sceneToRender && sceneToRender.activeCamera) {
                    sceneToRender.render();
                }
            });
        };
        var createDefaultEngine = function() {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };
        var delayCreateScene = async function() {
            return new Promise((resolve, reject) => {
                // Create a scene
                scene = new BABYLON.Scene(engine);

                // Append glTF model to scene.
                const sceneLoad = BABYLON.SceneLoader.Append(
                    `${path}/`,
                    file,
                    scene,
                    function(scene) {
                        // Create a default arc rotate camera and light.
                        scene.createDefaultCameraOrLight(true, true, true);

                        // The default camera looks at the back of the asset.
                        // Rotate the camera by 180 degrees to the front of the asset.
                        scene.activeCamera.alpha += Math.PI;

                        scene.clearColor = BABYLON.Color3.White();

                        resolve(scene);
                    }
                );
            });
        };

        window.initFunction = async function() {
            var asyncEngineCreation = async function() {
                try {
                    return createDefaultEngine();
                } catch (e) {
                    console.log(
                        "the available createEngine function failed. Creating the default engine instead"
                    );
                    return createDefaultEngine();
                }
            };

            window.engine = await asyncEngineCreation();
            if (!engine) throw "engine should not be null.";
            startRenderLoop(engine, canvas);
            window.scene = await delayCreateScene();

            return new Promise((resolve, reject) => {
                resolve(scene);
            });
        };

        ///////////////////////////// MAIN FUNC /////////////////////////////
        initFunction().then((sceneData) => {
            sceneToRender = scene;
            const {
                materials
            } = sceneData;
            const tbody = $('#3d_component');
            let idx = 1;
            let components = [];

            for (const key in materials) {
                const newRow = $('<tr>');

                newRow.append(`<td>${idx}</td>`);
                newRow.append(`<td>${materials[key].name}</td>`);
                newRow.append(`
                    <td>
                        <div class="form-check">
                            <input class="form-check-input form-checkbox-button selected-component" type="checkbox" name="selected[]" value=""  />
                        </div>
                    </td>
                `);

                tbody.append(newRow);
                components.push({
                    component: materials[key].name,
                    component3d_id: materials[key].id,
                })
                idx++;
            }

            $('#component_table').DataTable({
                bInfo: false
            });

            $.ajax({
                url: "{{ route('fleet.form.group.item.upload', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "components": components,
                    "is_confirmation": true,
                    "not_excel": true,
                },
                success: function() {
                    window.location = "{{ route('fleet.form.group.item.index', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}";
                }
            })
        });

        // Resize
        window.addEventListener("resize", function() {
            engine.resize();
        });
    </script>
@endsection
