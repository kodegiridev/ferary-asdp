@extends('layout.demo1.master')

@inject('ctrl', 'App\Http\Controllers\Controller')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <!-- Babylon.js -->
    @include('/layout/partials/babylon')
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <form class="form" action="{{ route('fleet.form.group.item.upload', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Data Comparison') }}</h3>
                        <div class="card-toolbar"></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="card shadow-sm">
                                    <div class="card-header">
                                        <h3 class="card-title">{{ __('Excel') }}</h3>
                                        <div class="card-toolbar"></div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover" id="component_table">
                                                <thead>
                                                    <tr>
                                                        <td class="text-center w-50px">No</td>
                                                        <td class="text-center">Component</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="card shadow-sm">
                                    <input type="hidden" name="is_confirmation" value="1">
                                    <input type="hidden" name="exist_components" value="">
                                    <div class="card-header">
                                        <h3 class="card-title">{{ __('File 3D') }}</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover" id="3d_table">
                                                <thead>
                                                    <tr>
                                                        <td class="text-center w-50px">No</td>
                                                        <td class="text-center">Component</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="3d_component"></tbody>
                                            </table>
                                        </div>
                                        <div id="canvasZone" hidden>
                                            <canvas id="modelCanvas" style="height: 60vh;width: 100%;touch-action: none;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ route('fleet.form.group.item.index', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}" class="btn btn-sm btn-secondary">
                            <i class="ki-duotone ki-arrow-left fs-1">
                                <span class="path1"></span>
                                <span class="path2"></span>
                            </i>
                            {{ __('Kembali') }}
                        </a>
                        &nbsp; &nbsp;
                        <button type="submit" class="btn btn-sm btn-primary">
                            {{ __('Simpan') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        var mode = KTThemeMode.getMode();

        $(document).ready(function() {
            checkSession();
        });

        $('#component_table').DataTable({
            proccesing: true,
            serverSide: true,
            bInfo: false,
            paging: false,
            searching: false,
            ordering: false,
            ajax: {
                url: "{{ route('fleet.form.group.item.index', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                type: "GET",
                data: function(d) {
                    d.type = 'component'
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: 'text-center'
                },
                {
                    data: 'component',
                    name: 'component'
                }
            ],
            columnDefs: [{
                target: '_all',
                className: 'align-middle'
            }]
        });

        function highlightColor(type = 'green') {
            if (type == 'green') {
                return mode == 'dark' ? '#52c41a' : '#95de64';
            } else {
                return mode == 'dark' ? '#a8071a' : '#ff7875';
            }
        }

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }

        function comparingComponent(component3D, compared = []) {
            var table = document.getElementById('component_table');
            for (var i = 1; i < table.rows.length; i++) {
                var row = table.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    if (j == 1) {
                        var component = row.cells[j].innerText;
                        if (!compared.includes(component)) {
                            if (component == component3D) {
                                row.style.backgroundColor = highlightColor('green');
                                return component;
                            } else {
                                row.style.backgroundColor = highlightColor('red');
                            }
                        }
                    }
                }
            }
        }

        var path = "{{ asset('upload/master-ercm/fleet/temp') }}";
        var file = "{{ $data['group']['attachment_temp'] }}";
        var canvas = document.getElementById("modelCanvas");
        var engine = null;
        var scene = null;
        var sceneToRender = null;

        var startRenderLoop = function(engine, canvas) {
            engine.runRenderLoop(function() {
                if (sceneToRender && sceneToRender.activeCamera) {
                    sceneToRender.render();
                }
            });
        };

        var createDefaultEngine = function() {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };

        var delayCreateScene = async function() {
            return new Promise((resolve, reject) => {
                // Create a scene
                scene = new BABYLON.Scene(engine);

                // Append glTF model to scene.
                const sceneLoad = BABYLON.SceneLoader.Append(
                    `${path}/`,
                    file,
                    scene,
                    function(scene) {
                        // Create a default arc rotate camera and light.
                        scene.createDefaultCameraOrLight(true, true, true);

                        // The default camera looks at the back of the asset.
                        // Rotate the camera by 180 degrees to the front of the asset.
                        scene.activeCamera.alpha += Math.PI;

                        scene.clearColor = BABYLON.Color3.White();

                        resolve(scene);
                    }
                );
            });
        };

        window.initFunction = async function() {
            var asyncEngineCreation = async function() {
                try {
                    return createDefaultEngine();
                } catch (e) {
                    console.log(
                        "the available createEngine function failed. Creating the default engine instead"
                    );
                    return createDefaultEngine();
                }
            };

            window.engine = await asyncEngineCreation();
            if (!engine) throw "engine should not be null.";
            startRenderLoop(engine, canvas);
            window.scene = await delayCreateScene();

            return new Promise((resolve, reject) => {
                resolve(scene);
            });
        };

        initFunction().then((sceneData) => {
            sceneToRender = scene;
            const {
                materials
            } = sceneData;

            let compared = [];
            let idx = 1;
            const tbody = $('#3d_component');

            for (const key in materials) {
                const newRow = $('<tr>');

                newRow.append(`<td class="text-center">${idx}</td>`);
                newRow.append(`<td>${materials[key].name}</td>`);

                $('#canvasZone').append(`
                    <input type="hidden" name="components[${key}][component]" value="${materials[key].name}">
                    <input type="hidden" name="components[${key}][component3d_id]" value="${materials[key].id}">
                `)

                const compare = comparingComponent(materials[key].name, compared);
                if (compare) {
                    compared.push(compare);
                    newRow.css('background-color', highlightColor('green'));
                } else {
                    newRow.css('background-color', highlightColor('red'));
                }

                tbody.append(newRow);
                idx++;
            }

            $('input[name="exist_components"]').val(JSON.stringify(compared));
            $('#3d_table').DataTable({
                bInfo: false,
                paging: false,
                searching: false,
                ordering: false,
            });
        });
    </script>
@endsection
