<div class="modal fade" tabindex="-1" data-bs-backdrop="static" id="group_form_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="form" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="modal" value="group_form_modal">
                <input type="hidden" name="url" value="{{ route('fleet.form.group.item.grouped', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}">
                <input type="hidden" name="mst_ercm_vessel_components">
                <div class="modal-header">
                    <h3 class="modal-title">{{ __('Component Selected Item') }}</h3>
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        {!! theme()->getSvgIcon('icons/duotune/arrows/arr061.svg', 'svg-icon-1') !!}
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label class="required form-label">{{ __('Sub Group') }}</label>
                        <select class="form-select" name="mst_ercm_vessel_subgroup_id" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">{{ __('Selected Component') }}</label>
                        <input type="text" class="form-control" name="component_qty" readonly />
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-sm btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
