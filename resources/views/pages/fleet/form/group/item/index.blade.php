@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <!-- Babylon.js -->
    @include('/layout/partials/babylon')

    <style>
        .form-checkbox-button {
            border: 1.75px solid var(--bs-gray-800) !important;
        }
    </style>
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Group') }}: {{ $data['group']['name'] }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.fleet.index') }}" class="text-muted text-hover-primary">
                            {{ __('Fleet') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.index', $data['fleet']['id']) }}" class="text-muted text-hover-primary">
                            {{ __('Form') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.group.index', [$data['fleet']['id'], $data['form']['id']]) }}" class="text-muted text-hover-primary">
                            {{ __('Group') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ $data['group']['name'] }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Component Data') }}</h3>
                            <div class="card-toolbar">
                                <button type="button" class="btn btn-icon btn-secondary btn-refresh" data-type="Component">
                                    <i class="ki-duotone ki-arrows-loop fs-1">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                    </i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="component_table">
                                    <thead>
                                        <tr>
                                            <td class="text-center w-50px">No</td>
                                            <td class="text-center">Component</td>
                                            <td class="text-center w-50px">Select</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <a href="{{ route('fleet.form.group.index', [$data['fleet']['id'], $data['form']['id']]) }}" class="btn btn-sm btn-secondary">
                                <i class="ki-duotone ki-arrow-left fs-1">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                                {{ __('Kembali') }}
                            </a>
                            &nbsp; &nbsp;
                            <button type="button" class="btn btn-sm btn-primary btn-grouped">
                                {{ __('Group') }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('3D Image') }}</h3>
                            <div class="card-toolbar">
                                @if (!empty($data['is_excel']))
                                    <button type="button" class="btn btn-sm btn-primary btn_create_document_modal" data-bs-toggle="modal" data-bs-target="#update_form_modal">
                                        {!! theme()->getSvgIcon('icons/duotune/files/fil022.svg', 'svg-icon-1') !!}
                                        {{ __('Upload 3D') }}
                                    </button>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="canvasZone"><canvas id="modelCanvas" style="height: 60vh;width: 100%;touch-action: none;"></canvas></div>
                        </div>
                    </div>
                    <div class="card shadow-sm mt-7">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Grouped Component') }}</h3>
                            <div class="card-toolbar">
                                <button type="button" class="btn btn-icon btn-secondary btn-refresh" data-type="Grouped">
                                    <i class="ki-duotone ki-arrows-loop fs-1">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                    </i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap" id="grouped_table">
                                    <thead>
                                        <tr class="text-center">
                                            <th>{{ __('No') }}</th>
                                            <th>{{ __('Sub Group') }}</th>
                                            <th>{{ __('Component Qty') }}</th>
                                            <th class="w-200px">{{ __('Standard') }}</th>
                                            <th class="w-100px">{{ __('Aksi') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.fleet.form.group.item.modals.upload')
    @include('pages.fleet.form.group.item.modals.group')
    @include('pages.fleet.form.group.item.modals.edit-selected')
    @include('pages.fleet.form.group.item.modals.detail-selected')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('demo1/plugins/custom/formrepeater/formrepeater.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(async function() {
            dataComponent();
            dataGrouped();
            checkSession();
        });

        $(document).on('click', '.btn-refresh', function() {
            switch ($(this).data('type')) {
                case 'Grouped':
                    dataGrouped();
                    break;
                case 'Component':
                    dataComponent()
                    break;
            }
        })

        $(document).on('click', '.btn-grouped', function() {
            var selected = $("#component_table input:checkbox:checked").map(function() {
                return $(this).val();
            }).get();

            if (selected?.length > 0) {
                $.ajax({
                    url: "{{ route('fleet.form.sub-group.ungroup', $data['fleet']['id']) }}",
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "mst_ercm_vessel_system_id": "{{ $data['form']['id'] }}"
                    },
                    success: function(response) {
                        var options = `<option></option>`;
                        $('#group_form_modal .form-select').empty();
                        $.each(response?.data, function(key, value) {
                            options += `<option value="${value?.id}">${value?.name}</option>`;
                        })

                        $('#group_form_modal .form-select').append(options);
                        $('#group_form_modal .form-select').select2({
                            placeholder: "Pilih sub group"
                        });
                        $('#group_form_modal input[name="mst_ercm_vessel_components"]').val(selected);
                        $('#group_form_modal input[name="component_qty"]').val(selected?.length);
                        $('#group_form_modal').modal('show');

                    },
                    error: function(err) {
                        Swal.fire({
                            title: err?.responseJSON?.message,
                            text: err?.responseJSON?.error,
                            icon: "error"
                        })
                    }
                })
            }
        });

        $(document).on('click', '.btn-edit-sub', function() {
            var subId = $(this).data('id');
            var type = $(this).data('type');
            $.ajax({
                url: "{{ route('fleet.form.group.item.grouped-detail', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "mst_ercm_vessel_subgroup_id": subId
                },
                success: function(response) {
                    dataSubGroupComponent(subId, type);

                    if (type == 'edit') {
                        $('#edit_selected_modal .modal-title').text(`Sub Group: ${response?.sub_group?.name || ''}`);
                        $('#edit_selected_modal').modal('show');
                    } else {
                        $('#detail_selected_modal .modal-title').text(`Sub Group: ${response?.sub_group?.name || ''}`);
                        $('#detail_selected_modal').modal('show');
                    }
                },
                error: function(err) {
                    Swal.fire({
                        title: err?.responseJSON?.message,
                        text: err?.responseJSON?.error,
                        icon: "error"
                    })
                }
            })
        });

        $(document).on('click', '.btn-delete-sub', function() {
            var id = $(this).data('id');
            var subId = $(this).data('sub');
            var type = $(this).data('type');
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: `{{ route('fleet.form.group.item.grouped-delete', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}`,
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "type": type,
                            "mst_ercm_vessel_subgroup_id": subId ?? id,
                            "mst_ercm_vessel_component_id": id
                        },
                        success: function(response) {
                            Swal.fire({
                                text: response?.message,
                                icon: "success"
                            });

                            dataGrouped();
                            dataComponent();
                            if (type == 'Component') {
                                dataSubGroupComponent(subId, 'edit');
                            }
                        },
                        error: function(error) {
                            Swal.fire({
                                title: error?.responseJSON?.message,
                                text: error?.responseJSON?.error,
                                icon: "error"
                            });
                        }
                    });
                }
            });
        });

        $(document).on('submit', '.form', function(e) {
            e.preventDefault();

            var modal = $(this).find('input[name="modal"]').val();

            $.ajax({
                url: $(this).find('input[name="url"]').val(),
                type: 'POST',
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    $(`#${modal}`).find('form')[0].reset();
                    $(`#${modal}`).modal('hide');

                    Swal.fire({
                        text: response?.message,
                        icon: "success"
                    });

                    dataComponent();
                    dataGrouped();
                },
                error: function(error) {
                    $(`#${modal}`).find('form')[0].reset();
                    $(`#${modal}`).modal('hide');

                    Swal.fire({
                        title: error?.responseJSON?.message,
                        text: error?.responseJSON?.error,
                        icon: "error"
                    });
                }
            });
        });

        function dataComponent() {
            $('#component_table').DataTable({
                proccesing: true,
                serverSide: true,
                bInfo: false,
                destroy: true,
                ajax: {
                    url: "{{ route('fleet.form.group.item.index', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                    type: "GET",
                    data: function(d) {
                        d.type = 'component'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                    {
                        data: 'component',
                        name: 'component'
                    },
                    {
                        data: 'select',
                        name: 'select',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'align-middle'
                }]
            });
        }

        function dataGrouped() {
            $('#grouped_table').DataTable({
                proccesing: true,
                serverSide: true,
                bInfo: false,
                destroy: true,
                ajax: {
                    url: "{{ route('fleet.form.group.item.index', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                    type: "GET",
                    data: function(d) {
                        d.type = 'grouped'
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'qty',
                        name: 'qty',
                        searchable: false
                    },
                    {
                        data: 'standard',
                        name: 'standard',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [{
                        target: '_all',
                        className: 'align-middle'
                    },
                    {
                        target: [0, 2, 3, 4],
                        className: 'text-center'
                    }
                ]
            });
        }

        function dataSubGroupComponent(id, type) {
            var tables = {
                'edit': '#edit_selected_table',
                'detail': '#detail_selected_table',
            };

            $(tables?.[type]).DataTable({
                proccesing: true,
                serverSide: true,
                bInfo: false,
                destroy: true,
                ajax: {
                    url: "{{ route('fleet.form.group.item.grouped-detail', [$data['fleet']['id'], $data['form']['id'], $data['group']['id']]) }}",
                    type: "GET",
                    data: function(d) {
                        d.type = type;
                        d.mst_ercm_vessel_subgroup_id = id;
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center',
                        searchable: false
                    },
                    {
                        data: 'component',
                        name: 'component'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'text-center',
                        orderable: false,
                        searchable: false
                    }
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'align-middle'
                }]
            });
        }

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }

        const canvas = document.getElementById("modelCanvas");
        var path = "{{ asset('upload/master-ercm/fleet') }}";
        var file = "{{ $data['group']['attachment'] }}";
        var engine = null;
        var scene = null;
        var sceneToRender = null;
        var startRenderLoop = function(engine, canvas) {
            engine.runRenderLoop(function() {
                if (sceneToRender && sceneToRender.activeCamera) {
                    sceneToRender.render();
                }
            });
        };
        var createDefaultEngine = function() {
            return new BABYLON.Engine(canvas, true, {
                preserveDrawingBuffer: true,
                stencil: true,
                disableWebGL2Support: false,
            });
        };
        var delayCreateScene = async function() {
            return new Promise((resolve, reject) => {
                // Create a scene
                scene = new BABYLON.Scene(engine);

                // Append glTF model to scene.
                const sceneLoad = BABYLON.SceneLoader.Append(
                    `${path}/`,
                    file,
                    scene,
                    function(scene) {
                        // Create a default arc rotate camera and light.
                        scene.createDefaultCameraOrLight(true, true, true);

                        // The default camera looks at the back of the asset.
                        // Rotate the camera by 180 degrees to the front of the asset.
                        scene.activeCamera.alpha += Math.PI;

                        scene.clearColor = BABYLON.Color3.White();

                        resolve(scene);
                    }
                );
            });
        };

        window.initFunction = async function() {
            var asyncEngineCreation = async function() {
                try {
                    return createDefaultEngine();
                } catch (e) {
                    console.log(
                        "the available createEngine function failed. Creating the default engine instead"
                    );
                    return createDefaultEngine();
                }
            };

            window.engine = await asyncEngineCreation();
            if (!engine) throw "engine should not be null.";
            startRenderLoop(engine, canvas);
            window.scene = await delayCreateScene();

            return new Promise((resolve, reject) => {
                resolve(scene);
            });
        };

        ///////////////////////////// MAIN FUNC /////////////////////////////
        initFunction().then((sceneData) => {
            sceneToRender = scene;
            const {
                materials
            } = sceneData;
        });

        // Resize
        window.addEventListener("resize", function() {
            engine.resize();
        });
    </script>

    @stack('modal-scripts')
@endsection
