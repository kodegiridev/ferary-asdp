@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/keenicons/duotone/style.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Fleet Form:') }} {{ $data['form']['name'] }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.fleet.index') }}" class="text-muted text-hover-primary">
                            {{ __('Fleet') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('fleet.form.index', $data['fleet']['id']) }}" class="text-muted text-hover-primary">
                            {{ __('Form') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ $data['form']['name'] }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid mb-5 mb-lg-10">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data') }} {{ $data['form']['name'] }}</h3>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-sm btn-primary btn_create_form_modal" data-bs-toggle="modal" data-bs-target="#create_form_modal">
                            <i class="ki-duotone ki-plus fs-1"></i>
                            {{ __('Tambah') }}
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="component_table">
                            <thead>
                                <tr class="text-center">
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Group') }}</th>
                                    <th>{{ __('Qty Sub Group') }}</th>
                                    <th>{{ __('Qty Component') }}</th>
                                    <th class="w-200px">{{ __('Proportional Value') }}</th>
                                    <th class="w-100px">{{ __('Detail') }}</th>
                                    <th class="w-100px">{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('fleet.form.index', $data['fleet']['id']) }}" class="btn btn-sm btn-info">
                        <i class="ki-duotone ki-arrow-left fs-1">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        {{ __('Kembali') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    @include('pages.fleet.form.group.modals.create')
    @include('pages.fleet.form.group.modals.edit')
@endsection

@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            checkSession();
            $('#create_value').text(parseInt("{{ $data['used_value'] }}"));
        });

        var table = $('#component_table').DataTable({
            proccesing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('fleet.form.group.index', [$data['fleet']['id'], $data['form']['id']]) }}"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'group_qty',
                    name: 'group_qty'
                },
                {
                    data: 'component_qty',
                    name: 'component_qty'
                },
                {
                    data: 'value',
                    name: 'value'
                },
                {
                    data: 'detail',
                    name: 'detail',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [{
                    target: '_all',
                    className: 'align-middle'
                },
                {
                    target: [0, 2, 3, 4, 5, 6],
                    className: 'text-center'
                },
            ]
        });

        $(document).on('click', '.btn-edit', function() {
            var id = $(this).data('id');
            $.ajax({
                url: "{{ route('fleet.form.group.show', [$data['fleet']['id'], $data['form']['id']]) }}",
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function(response) {
                    $('#edit_form_modal input[name="id"]').val(response?.data?.id);
                    $('#edit_form_modal input[name="name"]').val(response?.data?.name);
                    $('#edit_form_modal input[name="value"]').val(response?.data?.value);
                    $('#edit_form_modal input[name="temp_value"]').val(response?.used_value);

                    $('#edit_value').text(response?.data?.value + response?.used_value);
                    $('#edit_form_modal').modal('show');
                }
            })
        });

        $(document).on('click', '.btn-delete', function() {
            Swal.fire({
                title: "{{ __('Apakah anda yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('fleet.form.group.delete', [$data['fleet']['id'], $data['form']['id']]) }}",
                        type: "DELETE",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": $(this).data('id')
                        },
                        success: function() {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('keyup', '#create_form_modal input[name="value"]', function() {
            var usedValue = parseInt("{{ $data['used_value'] }}");
            var inputValue = parseInt($(this).val() || 0);
            var totalValue = inputValue + usedValue;

            if (totalValue > 100) {
                $(this).addClass('is-invalid');
                var error = `<span class="text-danger">Melebihi batas maksimum!</span>`;
                $('#create_form_modal .invalid_value').html(error);
            } else {
                $(this).removeClass('is-invalid');
                $('#create_form_modal .invalid_value').empty();
            }

            $('#create_value').text(totalValue);
        });

        $(document).on('keyup', '#edit_form_modal input[name="value"]', function() {
            var usedValue = parseInt($('#edit_form_modal input[name="temp_value"]').val());
            var inputValue = parseInt($(this).val() || 0);
            var totalValue = inputValue + usedValue;

            if (totalValue > 100) {
                $(this).addClass('is-invalid');
                var error = `<span class="text-danger">Melebihi batas maksimum!</span>`;
                $('#edit_form_modal .invalid_value').html(error);
            } else {
                $(this).removeClass('is-invalid');
                $('#edit_form_modal .invalid_value').empty();
            }

            $('#edit_value').text(totalValue);
        });

        function checkSession() {
            if (`{{ Session::has('success') }}`) {
                Swal.fire({
                    text: `{{ Session::get('success') }}`,
                    icon: "success"
                });
            }

            if (`{{ Session::has('failed') }}`) {
                Swal.fire({
                    text: `{{ Session::get('failed') }}`,
                    icon: "error"
                });
            }
        }
    </script>
@endsection
