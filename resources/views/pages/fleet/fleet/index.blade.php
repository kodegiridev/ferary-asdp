@extends('layout.demo1.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">{{ __('Data Fleet') }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Fleet') }}</li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">{{ __('Fleet') }}</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="kt_app_content" class="app-content flex-column-fluid">
        <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Data Fleet') }}</h3>
                    <div class="card-toolbar">
                        <button type="button" id="btn_create_fleet_modal" class="btn btn-sm btn-light" data-bs-toggle="modal" data-bs-target="#create_fleet_modal">
                            <i class="fa fa-plus"></i>
                            {{ __('Tambah') }}
                        </button>
                        <button type="button" id="btn_edit_fleet_modal" data-bs-toggle="modal" data-bs-target="#edit_fleet_modal" hidden>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered nowrap" id="fleet_table">
                            <thead>
                                <tr class="text-center fw-bold fs-6 text-gray-800">
                                    <th>{{ __('No') }}</th>
                                    <th>{{ __('Regional') }}</th>
                                    <th>{{ __('Cabang') }}</th>
                                    <th>{{ __('Kapal') }}</th>
                                    <th>{{ __('Lintasan') }}</th>
                                    <th>{{ __('Tonnase') }}</th>
                                    <th>{{ __('Panjang(m)') }}</th>
                                    <th>{{ __('Daya(KW/HP)') }}</th>
                                    <th>{{ __('Usia Teknis') }}</th>
                                    <th>{{ __('Status Survey') }}</th>
                                    <th>{{ __('PIC') }}</th>
                                    <th>{{ __('OS') }}</th>
                                    <th>{{ __('Aksi') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>

    @include('pages.fleet.fleet.modals.edit')
    @include('pages.fleet.fleet.modals.create')
@endsection


@section('scripts')
    <script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $("#create_regional_id").on('change', function() {
            let regional_id = $(this).val();
            $.ajax({
                url: "{{ route('port.ajax_get_branch') }}",
                type: 'GET',
                data: {
                    id: regional_id
                },
                beforeSend: function() {
                    $("#create_cabang_id").empty();
                },
                success: function(data) {
                    let html = '<option value="">Pilih data</option>';
                    if (data) {
                        $.each(data, function(index, item) {
                            html += `<option value="${item.id}">${item.nama}</option>`;
                        });
                    }
                    $("#create_cabang_id").html(html);
                }
            });
        });

        $("#update_regional_id").on('change', function() {
            let regional_id = $(this).val();
            $.ajax({
                url: "{{ route('port.ajax_get_branch') }}",
                type: 'GET',
                data: {
                    id: regional_id
                },
                beforeSend: function() {
                    $("#update_cabang_id").empty();
                },
                success: function(data) {
                    let html = '<option value="">Pilih data</option>';
                    let cabang_id = $('#edit_port_modal input[name="cabang_id_source"]').val();
                    if (data) {
                        $.each(data, function(index, item) {
                            let selected = '';
                            if (item.id == cabang_id) {
                                selected = 'selected';
                            }
                            html += `<option ${selected} value="${item.id}">${item.nama}</option>`;
                        });
                    }
                    $("#update_cabang_id").html(html);
                }
            });
        });

        function checkSession() {
            if ('{{ Session::has('success') }}') {
                Swal.fire({
                    text: '{{ Session::get('success') }}',
                    icon: "success"
                });
            }

            if ('{{ Session::has('failed') }}') {
                Swal.fire({
                    text: '{{ Session::get('failed') }}',
                    icon: "error"
                });
            }
        }

        if ($('.modal .is-invalid').length > 0) {
            var action = "{{ old('action') }}";
            $('#btn_' + action + '_fleet_modal').trigger('click');
        }

        $(document).ready(function() {
            checkSession();

            var table = $('#fleet_table').DataTable({
                proccesing: true,
                serverSide: true,
                order: [],
                ajax: {
                    url: "{{ route('fleet.fleet.index') }}"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'regional_id',
                        name: 'regional_id'
                    },
                    {
                        data: 'cabang_id',
                        name: 'cabang_id'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'lintasan_id',
                        name: 'lintasan_id'
                    },
                    {
                        data: 'grt',
                        name: 'grt'
                    },
                    {
                        data: 'panjang',
                        name: 'panjang'
                    },
                    {
                        data: 'daya',
                        name: 'daya'
                    },
                    {
                        data: 'usia_teknis',
                        name: 'usia_teknis'
                    },
                    {
                        data: 'status_survey',
                        name: 'status_survey'
                    },
                    {
                        data: 'pic',
                        name: 'pic'
                    },
                    {
                        data: 'os',
                        name: 'os'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    },
                ],
                columnDefs: [{
                    target: '_all',
                    className: 'align-middle'
                }]
            });

            $(document).on('click', '.btn-edit-fleet', function() {
                var url = `{{ route('fleet.fleet.show', ':fleet') }}`;
                url = url.replace(':fleet', $(this).data('id'));

                $.ajax({
                    url: url,
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(data) {

                        var action = `{{ route('fleet.fleet.update', ':fleet') }}`;
                        action = action.replace(':fleet', data.id);

                        $('#edit_fleet_modal form').prop('action', action);
                        $('#edit_fleet_modal input[name="nama"]').val(data.nama);
                        $('#edit_fleet_modal select[name="regional_id"]').val(data.regional_id);
                        $('#edit_fleet_modal select[name="cabang_id"]').val(data.cabang_id);
                        $('#edit_fleet_modal select[name="lintasan_id"]').val(data.lintasan_id);
                        $('#edit_fleet_modal input[name="grt"]').val(data.grt);
                        $('#edit_fleet_modal input[name="panjang"]').val(data.panjang);
                        $('#edit_fleet_modal input[name="daya"]').val(data.daya);
                        $('#edit_fleet_modal select[name="satuan"]').val(data.satuan);
                        $('#edit_fleet_modal input[name="usia_teknis"]').val(data.usia_teknis);
                        $('#edit_fleet_modal .status-survey').val(data.status_survey);
                        $('#edit_fleet_modal select[name="pic"]').val(data.pic);
                        $('#edit_fleet_modal select[name="os"]').val(data.os);
                        $('#update_fleet_id').trigger('change');

                    }
                });
            });

            $(document).on('click', '.btn-delete-fleet', function() {
                var url = `{{ route('fleet.fleet.destroy', ':fleet') }}`;
                url = url.replace(':fleet', $(this).data('id'));

                Swal.fire({
                    title: "{{ __('Apakah kamu yakin?') }}",
                    text: "{{ __('Data akan terhapus secara permanen!') }}",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Ya') }}",
                    cancelButtonText: "{{ __('Batal') }}",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}",
                            },
                            success: function() {
                                location.reload();
                            }
                        });
                    }
                });
            });

            $(document).on('hidden.bs.modal', '.modal', function() {
                $('.is-invalid').each(function(index, element) {
                    $(this).removeClass('is-invalid');
                });
            });

            $(document).on('click', '#btn_create_fleet_modal', function() {
                $('input').not('input[type="hidden"]').each(function(index, element) {
                    $(this).val('');
                });

                $('select').each(function(index, element) {
                    $(this).val('').trigger('change');
                });
            });
        })

        $(document).on('change', 'input[name="usia_teknis"]', function() {
            const selected = moment($(this).val());
            const today = moment();
            const inMonth = today.diff(selected, 'months') % 12;
            const inYear = today.diff(selected, 'years') + (inMonth > 3 ? 1 : 0);
            const rules = [5, 3];
            let status = '';

            rules?.forEach((rule) => {
                if (inYear % rule === 0) {
                    if (rule === 5) {
                        status = `SS ${inYear/5}`;
                    } else if (rule === 3 && inMonth <= 11) {
                        status = `IS ${Math.ceil((inYear / 5))}`;
                    }
                }
            })

            if (status === '') {
                status = `AS ${inYear}`;
            }

            $('.status-survey').val(status);
        });

        $(document).on('keypress', 'input[type="number"]', function (e) {
            if (["e", "E", "+", "-"].includes(e.key)) {
                e.preventDefault();
            }
        });

        $(document).on('paste', 'input[type="number"]', function (e) {
            e.preventDefault();
        });
    </script>
@endsection
