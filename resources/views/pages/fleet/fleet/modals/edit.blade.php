<div class="modal fade" tabindex="-1" id="edit_fleet_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Ubah Fleet') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit" name="action">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" value="create" name="action">
                    <div class="form-group">
                        <div class="row">
                            <label for="name" class="required form-label">
                                {{ __('Nama Kapal') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text" class="form-control form-control-solid @error('nama') is-invalid @enderror" placeholder="Ketik disini" name="nama" value="{{ old('nama') }}" />

                                @error('nama')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="regional_id" class="required form-label">
                                {{ __('Regional') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="update_regional_id" name="regional_id" class="form-select form-select-solid @error('regional_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($region as $r)
                                    <option value="{{ $r->id }}" {{ old('regional_id') == $r->id ? 'selected' : '' }}>
                                        {{ $r->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('regional_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="cabang_id" class="required form-label">
                                {{ __('Cabang') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="update_cabang_id" name="cabang_id" class="form-select form-select-solid @error('cabang_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($cabang as $c)
                                    <option value="{{ $c->id }}" {{ old('cabang_id') == $c->id ? 'selected' : '' }}>
                                        {{ $c->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('cabang_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="lintasan_id" class="required form-label">
                                {{ __('Tipe Lintasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="lintasan_id" class="form-select form-select-solid @error('lintasan_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($track as $t)
                                    <option value="{{ $t->id }}" {{ old('lintasan_id') == $t->id ? 'selected' : '' }}>
                                        {{ $t->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('lintasan_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="grt" class="required form-label">
                                {{ __('Tonnase(GT)') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="number" class="form-control form-control-solid @error('grt') is-invalid @enderror" placeholder="Ketik disini" name="grt" value="{{ old('grt') }}" />

                                @error('grt')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="panjang" class="required form-label">
                                {{ __('Panjang(M)') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="number" class="form-control form-control-solid @error('panjang') is-invalid @enderror" placeholder="Ketik disini" name="panjang" value="{{ old('panjang') }}" />

                                @error('panjang')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <label for="daya" class="required form-label">
                                        {{ __('Daya') }}
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="number" class="form-control form-control-solid @error('daya') is-invalid @enderror" placeholder="Ketik disini" name="daya" value="{{ old('daya') }}" />

                                        @error('daya')
                                        <span class="invalid-feedback mb-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <label for="satuan" class="required form-label">
                                        {{ __('Satuan') }}
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <select name="satuan" class="form-select form-select-solid @error('satuan') is-invalid @enderror">
                                            <option value="">Pilih data</option>
                                            <option value="KW" {{ old('satuan') == 'KW' ? 'selected' : '' }}>KW</option>
                                            <option value="HP" {{ old('satuan') == 'HP' ? 'selected' : '' }}>HP</option>
                                        </select>

                                        @error('satuan')
                                        <span class="invalid-feedback mb-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="usia_teknis" class="required form-label">
                                {{ __('Usia Teknis') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="date" class="form-control form-control-solid @error('usia_teknis') is-invalid @enderror" placeholder="Ketik disini" name="usia_teknis" value="{{ old('usia_teknis') }}" />

                                @error('usia_teknis')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="status_survey" class="required form-label">
                                {{ __('Status Survey') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                {{-- <select name="status_survey" class="form-select form-select-solid @error('status_survey') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    <option value="AS1" {{ old('pic') == 'AS1' ? 'selected' : '' }}>AS I</option>
                                    <option value="AS2" {{ old('pic') == 'AS2' ? 'selected' : '' }}>AS II</option>
                                    <option value="AS3" {{ old('pic') == 'AS3' ? 'selected' : '' }}>AS III</option>
                                    <option value="AS4" {{ old('pic') == 'AS4' ? 'selected' : '' }}>AS IV</option>
                                    <option value="SS" {{ old('pic') == 'SS' ? 'selected' : '' }}>SS</option>
                                    <option value="IS" {{ old('pic') == 'IS' ? 'selected' : '' }}>IS</option>
                                </select> --}}

                                <input type="text" class="form-control form-control-solid status-survey @error('status_survey') is-invalid @enderror" value="{{ old('status_survey') }}" disabled>
                                <input type="hidden" name="status_survey" class="form-control form-control-solid status-survey" value="{{ old('status_survey') }}">

                                @error('status_survey')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="pic" class="required form-label">
                                {{ __('PIC') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="pic" name="pic" class="form-select form-select-solid @error('pic') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($user as $u)
                                    <option value="{{ $u->id }}" {{ old('pic') == $u->id ? 'selected' : '' }}>
                                        {{ $u->name }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('pic')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="os" class="required form-label">{{ __('Owner Surveyor') }}</label>
                        <select id="os" name="os" class="form-select form-select-solid @error('os') is-invalid @enderror" data-control="select2" data-placeholder="Pilih owner surveyor">
                            <option></option>
                            @foreach ($user as $u)
                            <option value="{{ $u->id }}" {{ old('os') == $u->id ? 'selected' : '' }}>
                                {{ $u->name }}
                            </option>
                            @endforeach
                        </select>

                        @error('os')
                        <span class="invalid-feedback mb-2" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>