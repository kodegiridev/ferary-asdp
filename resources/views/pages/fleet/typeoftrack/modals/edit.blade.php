<div class="modal fade" tabindex="-1" id="edit_typeoftrack_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Ubah Tipe Lintasan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="edit" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama" class="required form-label">
                                {{ __('Tipe Lintasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama" value="{{ old('nama') }}"/>

                                @error('nama')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
