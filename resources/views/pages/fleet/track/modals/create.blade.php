<div class="modal fade" tabindex="-1" id="create_track_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Track') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('fleet.track.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama" class="required form-label">
                                {{ __('Nama Lintasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text" class="form-control form-control-solid @error('nama') is-invalid @enderror" placeholder="Ketik disini" name="nama" value="{{ old('nama') }}" />

                                @error('nama')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="rute_id" class="required form-label">
                                {{ __('Rute') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="rute_id" class="form-select form-select-solid @error('rute_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($rute as $r)
                                    <option value="{{ $r->id }}" {{ old('rute_id') == $r->id ? 'selected' : '' }}>
                                        {{ $r->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('rute_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe_lintasan_id" class="required form-label">
                                {{ __('Tipe Lintasan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select id="create_tipe_lintasan_id" name="tipe_lintasan_id" class="form-select form-select-solid @error('tipe_lintasan_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($type as $t)
                                    <option value="{{ $t->id }}" {{ old('tipe_lintasan_id') == $t->id ? 'selected' : '' }}>
                                        {{ $t->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('tipe_lintasan_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="pelabuhan_id" class="required form-label">
                                {{ __('Pelabuhan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="pelabuhan_id" class="form-select form-select-solid @error('pelabuhan_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($dock as $d)
                                    <option value="{{ $d->id }}" {{ old('pelabuhan_id') == $d->id ? 'selected' : '' }}>
                                        {{ $d->nama }}
                                    </option>
                                    @endforeach
                                </select>

                                @error('pelabuhan_id')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>