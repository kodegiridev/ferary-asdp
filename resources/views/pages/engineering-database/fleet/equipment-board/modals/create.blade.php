<div class="modal fade" tabindex="-1" id="create_equipment_board_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Tambah Peralatan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('ed.equipment-board.store', $vessel_id) }}"
            enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="create" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama_peralatan" class="required form-label">
                                {{ __('Nama Peralatan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama_peralatan') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama_peralatan" value="{{ old('nama_peralatan') }}"/>

                                @error('nama_peralatan')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="merek" class="required form-label">
                                {{ __('Merek') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('merek') is-invalid @enderror"
                                placeholder="Ketik disini" name="merek" value="{{ old('merek') }}"/>

                                @error('merek')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="quantity" class="required form-label">
                                {{ __('Kuantitas') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="number"
                                class="form-control form-control-solid @error('quantity') is-invalid @enderror"
                                placeholder="Ketik disini" name="quantity" value="{{ old('quantity') }}"
                                min="0"/>

                                @error('quantity')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe_sertifikat" class="required form-label">
                                {{ __('Tipe Sertifikat') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="tipe_sertifikat"
                                class="form-select form-select-solid @error('tipe_sertifikat') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($listTypeCertificate as $typeCertificate)
                                        <option value="{{ $typeCertificate }}"
                                        {{ old('tipe_sertifikat') == $typeCertificate ? 'selected' : '' }}>
                                            {{ ucwords($typeCertificate) }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('tipe_sertifikat')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="diterbitkan_oleh" class="form-label">
                                {{ __('Diterbitkan Oleh') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('diterbitkan_oleh') is-invalid @enderror"
                                placeholder="Ketik disini" name="diterbitkan_oleh"
                                value="{{ old('diterbitkan_oleh') }}"/>

                                @error('diterbitkan_oleh')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="nomor_sertifikat" class="form-label">
                                {{ __('Nomor Sertifikat') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nomor_sertifikat') is-invalid @enderror"
                                placeholder="Ketik disini" name="nomor_sertifikat"
                                value="{{ old('nomor_sertifikat') }}"/>

                                @error('nomor_sertifikat')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tgl_sertifikat" class="form-label">
                                {{ __('Tanggal Sertifikat') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="date"
                                class="form-control form-control-solid @error('tgl_sertifikat') is-invalid @enderror"
                                placeholder="Ketik disini" name="tgl_sertifikat"
                                value="{{ old('tgl_sertifikat') }}"/>

                                @error('tgl_sertifikat')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tgl_habis" class="form-label">
                                {{ __('Tanggal Habis') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="date"
                                class="form-control form-control-solid @error('tgl_habis') is-invalid @enderror"
                                placeholder="Ketik disini" name="tgl_habis"
                                value="{{ old('tgl_habis') }}"/>

                                @error('tgl_habis')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="status" class="form-label">
                                {{ __('Status') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('status') is-invalid @enderror"
                                placeholder="Ketik disini" name="status"
                                value="{{ old('status') }}"/>

                                @error('status')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan" class="form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('keterangan') is-invalid @enderror"
                                placeholder="Ketik disini" name="keterangan"
                                value="{{ old('keterangan') }}"/>

                                @error('keterangan')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row justify-content-between mb-2">
                            <div class="col-6">
                                <label for="dokumen" class="required form-label">
                                    {{ __('Dokumen') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <button type="button"
                                class="btn btn-sm btn-dark float-end"
                                id="btn_add_document">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-file"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-input-file 
                            @error('dokumen') is-invalid @enderror
                            @error('dokumen.*') is-invalid @enderror">
                                <div class="row input-file">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                </div>
                            </div>
                            @error('dokumen')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @error('dokumen.*')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
