@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Detail Equipment Board') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                    <a href="{{ route('ed.equipment-board.indexFleet') }}" class="text-muted text-hover-primary">{{ __('Equipment Board') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Detail Equipment Board') }}</h3>
                <div class="card-toolbar">
                    @if ($countEquipmentBoard)
                        <a class="btn btn-sm btn-light"
                        href="{{ route('ed.equipment-board.print', $vessel_id) }}">
                            <i class="fa fa-print"></i>
                            {{ __('Cetak') }}
                        </a>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="detail_fleet_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Peralatan') }}</th>
                                <th>{{ __('Merek') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Tipe Sertifikat') }}</th>
                                <th>{{ __('Diterbitkan Oleh') }}</th>
                                <th>{{ __('Nomor Sertifikat') }}</th>
                                <th>{{ __('Tanggal Sertifikat') }}</th>
                                <th>{{ __('Tanggal Habis') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                                <th>{{ __('Tanggal Disetujui') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.fleet.equipment-board.modals.document')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#detail_fleet_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.equipment-board.detailFleet', $vessel_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_peralatan',
                    name: 'nama_peralatan'
                },
                {
                    data: 'merek',
                    name: 'merek'
                },
                {
                    data: 'quantity',
                    name: 'quantity'
                },
                {
                    data: 'tipe_sertifikat',
                    name: 'tipe_sertifikat'
                },
                {
                    data: 'diterbitkan_oleh',
                    name: 'diterbitkan_oleh'
                },
                {
                    data: 'nomor_sertifikat',
                    name: 'nomor_sertifikat'
                },
                {
                    data: 'tgl_sertifikat',
                    name: 'tgl_sertifikat'
                },
                {
                    data: 'tgl_habis',
                    name: 'tgl_habis'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'tgl_penyetujuan',
                    name: 'tgl_penyetujuan'
                },
            ],
        });

        $(document).on('click', '.btn-delete-equipment-board', function () {
            var url = `{{ route('ed.equipment-board.destroy', [':vessel_id', ':equipmentBoard']) }}`;
            url = url.replace(':vessel_id', "{{ $vessel_id }}");
            url = url.replace(':equipmentBoard', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-document-equipment-board', function () {
            var url = `{{ route('ed.equipment-board.getDocument', ':equipmentBoard') }}`;
            url = url.replace(':equipmentBoard', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_equipment_board_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.equipment-board.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });
    })
</script>
@endsection