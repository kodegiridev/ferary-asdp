<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ __('Cetak Equipment Board') }}</title>
    </head>
    <style>
        body {
            font-family: sans-serif;
        }
        .title {
            margin-bottom: 2rem;
        }
        table {
            font-size: 0.750rem;
            border-collapse: collapse;
            width: 100%;
        }
        table, th, td {
            border: 1px solid;
            padding: 0.250rem;
        }
        .footer {
            width: 100%;
            margin-top: 2rem;
        }
        .approver {
            float: right;
            width: 25%;
        }
        .approver p {
            margin: 0.250rem;
            padding: 0;
        }
        .qrcode {
            margin-top: 1rem;
            width: 100%;
            text-align: center;
        }
    </style>
    <body>
        <div class="title">
            <h2>{{ __('Equipment On Board | ') . $fleet->nama }}</h2>
        </div>
        <table>
            <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('Peralatan') }}</th>
                    <th>{{ __('Merek') }}</th>
                    <th>{{ __('Quantity') }}</th>
                    <th>{{ __('Tipe Sertifikat') }}</th>
                    <th>{{ __('Diterbitkan Oleh') }}</th>
                    <th>{{ __('Nomor Sertifikat') }}</th>
                    <th>{{ __('Tanggal Sertifikat') }}</th>
                    <th>{{ __('Tanggal Habis') }}</th>
                    <th>{{ __('Status') }}</th>
                    <th>{{ __('Keterangan') }}</th>
                    <th>{{ __('Dokumen') }}</th>
                    <th>{{ __('Tanggal Disetujui') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($equipmentBoards as $equipmentBoard)
                    @foreach ($equipmentBoard->dokumen as $document)
                        @if ($loop->first)
                            <tr>
                                <td style="text-align: center">{{ $loop->parent->iteration }}</td>
                                <td>{{ $equipmentBoard->nama_peralatan }}</td>
                                <td>{{ $equipmentBoard->merek }}</td>
                                <td style="text-align: center">{{ $equipmentBoard->quantity }}</td>
                                <td>{{ $equipmentBoard->tipe_sertifikat }}</td>
                                <td>{{ $equipmentBoard->diterbitkan_oleh }}</td>
                                <td>{{ $equipmentBoard->nomor_sertifikat }}</td>
                                <td style="white-space: nowrap">{{ $equipmentBoard->tgl_sertifikat }}</td>
                                <td style="white-space: nowrap">{{ $equipmentBoard->tgl_habis }}</td>
                                <td style="text-align: center">{{ $equipmentBoard->status }}</td>
                                <td style="text-align: center">{{ $equipmentBoard->keterangan }}</td>
                                <td>{{ explode('/', $document->file)[3] }}</td>
                                <td style="white-space: nowrap">{{ $equipmentBoard->approval->tgl_penyetujuan }}</td>
                            </tr>
                        @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ explode('/', $document->file)[3] }}</td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>

        <div class="footer">
            <div class="approver">
                <p>{{ __('Dikeluarkan : Jakarta') }}</p>
                <p>{{ __('Pada Tanggal : ') . $equipmentBoards[0]->approval->tgl_penyetujuan }}</p>
                <div class="qrcode">
                    <img src="data:image/svg;base64, {{  $qrcode  }}">
                    <p>{{ $equipmentBoards[0]->approval->penyetuju->name }}</p>
                </div>
            </div>
        </div>
    </body>
</html>