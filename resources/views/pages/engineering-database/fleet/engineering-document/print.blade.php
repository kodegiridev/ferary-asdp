<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ __('Cetak Engineering Document') }}</title>
    </head>
    <style>
        body {
            font-family: sans-serif;
        }
        .title {
            margin-bottom: 2rem;
        }
        table {
            font-size: 0.750rem;
            border-collapse: collapse;
            width: 100%;
        }
        table, th, td {
            border: 1px solid;
            padding: 0.250rem;
        }
        .footer {
            width: 100%;
            margin-top: 2rem;
        }
        .approver {
            float: right;
            width: 25%;
        }
        .approver p {
            margin: 0.250rem;
            padding: 0;
        }
        .qrcode {
            margin-top: 1rem;
            width: 100%;
            text-align: center;
        }
    </style>
    <body>
        <div class="title">
            <h2>{{ __('Engineering Document | ') . $fleet->nama }}</h2>
        </div>
        <table>
            <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('Tipe Gambar') }}</th>
                    <th>{{ __('Nama Gambar') }}</th>
                    <th>{{ __('Nomor Gambar') }}</th>
                    <th>{{ __('Status Gambar') }}</th>
                    <th>{{ __('Keterangan') }}</th>
                    <th>{{ __('Dokumen') }}</th>
                    <th>{{ __('Tanggal Disetujui') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($engineeringDocuments as $engineeringDocument)
                    @foreach ($engineeringDocument->dokumen as $document)
                        @if ($loop->first)
                            <tr>
                                <td style="text-align: center">{{ $loop->parent->iteration }}</td>
                                <td>{{ $engineeringDocument->tipe_gambar }}</td>
                                <td>{{ $engineeringDocument->nama_gambar }}</td>
                                <td>{{ $engineeringDocument->nomor_gambar }}</td>
                                <td>{{ $engineeringDocument->status_gambar }}</td>
                                <td style="text-align: center">{{ $engineeringDocument->keterangan }}</td>
                                <td>{{ explode('/', $document->file)[3] }}</td>
                                <td style="white-space: nowrap">{{ $engineeringDocument->approval->tgl_penyetujuan }}</td>
                            </tr>
                        @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ explode('/', $document->file)[3] }}</td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>

        <div class="footer">
            <div class="approver">
                <p>{{ __('Dikeluarkan : Jakarta') }}</p>
                <p>{{ __('Pada Tanggal : ') . $engineeringDocuments[0]->approval->tgl_penyetujuan }}</p>
                <div class="qrcode">
                    <img src="data:image/svg;base64, {{  $qrcode  }}">
                    <p>{{ $engineeringDocuments[0]->approval->penyetuju->name }}</p>
                </div>
            </div>
        </div>
    </body>
</html>