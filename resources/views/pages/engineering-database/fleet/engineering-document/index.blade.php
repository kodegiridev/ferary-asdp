@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Engineering Document') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Engineering Document') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Engineering Document') }}</h3>
                <div class="card-toolbar">
                    <button type="button"
                    id="btn_create_engineering_document_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_engineering_document_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    @if ($countEngineeringDocument)
                        <button type="button"
                        id="btn_send_approval_engineering_document_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_engineering_document_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>
                    @endif
                    <button type="button"
                    id="btn_edit_engineering_document_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_engineering_document_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="engineering_document_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Tipe Gambar') }}</th>
                                <th>{{ __('Nama Gambar') }}</th>
                                <th>{{ __('Nomor Gambar') }}</th>
                                <th>{{ __('Status Gambar') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.fleet.engineering-document.modals.create')
@include('pages.engineering-database.fleet.engineering-document.modals.edit')
@include('pages.engineering-database.fleet.engineering-document.modals.send-approval')
@include('pages.engineering-database.fleet.engineering-document.modals.document')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_engineering_document_modal').trigger('click');
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#engineering_document_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.engineering-document.index', $vessel_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'tipe_gambar',
                    name: 'tipe_gambar'
                },
                {
                    data: 'nama_gambar',
                    name: 'nama_gambar'
                },
                {
                    data: 'nomor_gambar',
                    name: 'nomor_gambar'
                },
                {
                    data: 'status_gambar',
                    name: 'status_gambar'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $(document).on('click', '.btn-edit-engineering-document', function () {
            var url = `{{ route('ed.engineering-document.show', [':vessel_id', ':engineeringDocument']) }}`;
            url = url.replace(':vessel_id', "{{ $vessel_id }}");
            url = url.replace(':engineeringDocument', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var action = `{{ route('ed.engineering-document.update', [':vessel_id', ':engineeringDocument']) }}`;
                    action = action.replace(':vessel_id', "{{ $vessel_id }}");
                    action = action.replace(':engineeringDocument', data.id);

                    $('#edit_engineering_document_modal form').prop('action', action);

                    $('#edit_engineering_document_modal input[name="nama_gambar"]').val(data.nama_gambar);
                    $('#edit_engineering_document_modal input[name="nomor_gambar"]').val(data.nomor_gambar);
                    $('#edit_engineering_document_modal input[name="keterangan"]').val(data.keterangan);
                    $('#edit_engineering_document_modal select[name="tipe_gambar"]')
                        .val(data.tipe_gambar)
                        .trigger('change');
                    $('#edit_engineering_document_modal select[name="status_gambar"]')
                        .val(data.status_gambar)
                        .trigger('change');
                }
            });
        });

        $(document).on('click', '.btn-delete-engineering-document', function () {
            var url = `{{ route('ed.engineering-document.destroy', [':vessel_id', ':engineeringDocument']) }}`;
            url = url.replace(':vessel_id', "{{ $vessel_id }}");
            url = url.replace(':engineeringDocument', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_engineering_document_modal', function () {
            $('#create_engineering_document_modal input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('#create_engineering_document_modal select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });

        $(document).on('click', '#btn_add_document', function () {
            var modal = $(this).closest('.modal');
            var boxInputFile = modal.find('.box-input-file');

            boxInputFile.append(`<div class="row input-file mt-2">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                    <div class="col-1 d-flex align-items-center">
                                        <button type="button"
                                        class="btn btn-sm btn-danger btn-delete-file">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>`);
        });

        $(document).on('click', '.btn-delete-file', function () {
            var inputFile = $(this).closest('.input-file');
            inputFile.remove();
        });

        $(document).on('click', '.btn-document-engineering-document', function () {
            var url = `{{ route('ed.engineering-document.getDocument', ':engineeringDocument') }}`;
            url = url.replace(':engineeringDocument', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_engineering_document_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.engineering-document.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $(document).on('click', '.btn-delete-verificator', function () {
            var inputVerificator = $(this).closest('.input-verificator');
            var selectElement = inputVerificator.find('select').val();

            if (selectElement != '') {
                var approver = $('select[name="penyetuju_id"]');
                var verificator = $('select[name="user_verifikator_id[]"]');

                $.each(approver.find('option'), function (key, value) {
                    if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                        $(this).prop('hidden', false);
                    }
                });

                verificator.each(function (index, element) {
                    var selectVerificator = $(this);
                    $.each(selectVerificator.find('option'), function (key, value) {
                        if (parseInt($(this).prop('value')) == parseInt(selectElement)) {
                            $(this).prop('hidden', false);
                        }
                    });
                });
            }
            inputVerificator.remove();
        });

        $(document).on('keypress', 'input[type="number"]', function (e) {
            if (["e", "E", "+", "-"].includes(e.key)) {
                e.preventDefault();
            }
        });

        $(document).on('paste', 'input[type="number"]', function (e) {
            e.preventDefault();
        });

        $(document).on('click', '#btn_add_verificator', function () {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');
            var options = ''

            $('select[name="user_verifikator_id[]"]').each(function (index, element) {
                if (index == 0) {
                    options = $(this).html();
                }
            });

            boxInputVerificator.append(`<div class="row input-verificator mt-2">
                                            <div class="col-10">
                                                <select name="user_verifikator_id[]"
                                                class="form-select form-select-solid">
                                                    ${options}
                                                </select>
                                            </div>
                                            <div class="col-1 d-flex align-items-center">
                                                <button type="button"
                                                class="btn btn-sm btn-danger btn-delete-verificator">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>`);
        });

        $(document).on('change', 'select[name="penyetuju_id"]', function () {
            var hiddenOption = [];
            var approver = $('select[name="penyetuju_id"]');
            var verificator = $('select[name="user_verifikator_id[]"]');

            if (approver.val() != '') {
                hiddenOption.push(parseInt(approver.val()));
            }

            verificator.each(function (index, element) {
                if ($(this).val() != '') {
                    hiddenOption.push(parseInt($(this).val()));
                }
            });

            $.each(approver.find('option'), function (key, value) {
                if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', true);
                }
                if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', false);
                }
            });

            verificator.each(function (index, element) {
                var selectVerificator = $(this);
                $.each(selectVerificator.find('option'), function (key, value) {
                    if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', true);
                    }
                    if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', false);
                    }
                });
            });
        });

        $(document).on('change', 'select[name="user_verifikator_id[]"]', function () {
            var hiddenOption = [];
            var approver = $('select[name="penyetuju_id"]');
            var verificator = $('select[name="user_verifikator_id[]"]');

            if (approver.val() != '') {
                hiddenOption.push(parseInt(approver.val()));
            }

            verificator.each(function (index, element) {
                if ($(this).val() != '') {
                    hiddenOption.push(parseInt($(this).val()));
                }
            });

            $.each(approver.find('option'), function (key, value) {
                if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', true);
                }
                if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                    $(this).prop('hidden', false);
                }
            });

            verificator.each(function (index, element) {
                var selectVerificator = $(this);
                $.each(selectVerificator.find('option'), function (key, value) {
                    if (hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', true);
                    }
                    if (!hiddenOption.includes(parseInt($(this).prop('value')))) {
                        $(this).prop('hidden', false);
                    }
                });
            });
        });
    })
</script>
@endsection