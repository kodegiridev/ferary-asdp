<div class="modal fade" tabindex="-1" id="send_approval_ship_document_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Kirim Persetujuan') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" action="{{ route('ed.ship-document.sendApproval', $vessel_id) }}"
            enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="send_approval" name="action">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="penyetuju_id" class="required form-label">
                                {{ __('Approver') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="penyetuju_id"
                                class="form-select form-select-solid @error('penyetuju_id') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($approvers as $approver)
                                        <option value="{{ $approver->id }}"
                                        {{ old('penyetuju_id') == $approver->id ? 'selected' : '' }}>
                                            {{ $approver->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('penyetuju_id')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row justify-content-between mb-2">
                            <div class="col-6">
                                <label for="user_verifikator_id" class="required form-label">
                                    {{ __('Verifikator') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <button type="button"
                                class="btn btn-sm btn-dark float-end"
                                id="btn_add_verificator">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-user"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-input-verificator @error('user_verifikator_id.*') is-invalid @enderror">
                                <div class="row input-verificator">
                                    <div class="col-10">
                                        <select name="user_verifikator_id[]"
                                        class="form-select form-select-solid">
                                            <option value="">Pilih data</option>
                                            @foreach ($verificators as $verificator)
                                                <option value="{{ $verificator->id }}">
                                                    {{ $verificator->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @error('user_verifikator_id.*')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan_pengaju" class="form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid
                                @error('keterangan_pengaju') is-invalid @enderror"
                                placeholder="Ketik disini" name="keterangan_pengaju"
                                value="{{ old('keterangan_pengaju') }}"/>

                                @error('keterangan_pengaju')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
