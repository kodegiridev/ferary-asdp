@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Detail Port Drawing') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                    <a href="{{ route('ed.port-drawing.indexPort') }}" class="text-muted text-hover-primary">{{ __('Port Drawing') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Detail') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Detail Port Drawing') }}</h3>
                <div class="card-toolbar">
                    @if ($countPortDrawing)
                        <a class="btn btn-sm btn-light"
                        href="{{ route('ed.port-drawing.print', $dock_id) }}" target="_blank">
                            <i class="fa fa-print"></i>
                            {{ __('Cetak') }}
                        </a>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="detail_port_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Komponen') }}</th>
                                <th>{{ __('Nama Gambar') }}</th>
                                <th>{{ __('Nomor Gambar') }}</th>
                                <th>{{ __('Prioritas') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                                <th>{{ __('Tanggal Disetujui') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.port-drawing.modals.document')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    $(document).ready(function () {
        checkSession();

        var table = $('#detail_port_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.port-drawing.detailPort', $dock_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_komponen',
                    name: 'nama_komponen'
                },
                {
                    data: 'nama_gambar',
                    name: 'nama_gambar'
                },
                {
                    data: 'nomor_gambar',
                    name: 'nomor_gambar'
                },
                {
                    data: 'tipe_gambar',
                    name: 'tipe_gambar'
                },
                {
                    data: 'status_gambar',
                    name: 'status_gambar'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'tanggal_disetujui',
                    name: 'tanggal_disetujui'
                },
            ],
        });

        $(document).on('click', '.btn-delete-port-drawing', function () {
            let id = $(this).data('id')
            var url = `{{ route('ed.port-drawing.destroy', [':port_id']) }}`;
            url = url.replace(':port_id', id);

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.btn-document-port-drawing', function () {
            var url = `{{ route('ed.port-drawing.getDocument', ':portDrawing') }}`;
            url = url.replace(':portDrawing', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_port_drawing_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.port-drawing.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);
                        let filename = value.file.split('/')
                        filename = filename[filename.length - 1]

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${filename}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_port_drawing_modal').on('hidden.bs.modal', function() {
            $(this).find('tbody').empty()
        })
    })
</script>
@endsection