<div class="modal fade" tabindex="-1" id="edit_port_drawing_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Form Ubah Komponen') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" enctype="multipart/form-data" id="form-update-port-drawing">
                @csrf
                <input type="hidden" value="edit" name="action">
                <input type="hidden" name="port_drawing">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama_komponen" class="required form-label">
                                {{ __('Nama Komponen') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama_komponen') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama_komponen" value="{{ old('nama_komponen') }}"/>

                                @error('nama_komponen')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="nama_gambar" class="required form-label">
                                {{ __('Nama Gambar') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama_gambar') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama_gambar" value="{{ old('nama_gambar') }}"/>

                                @error('nama_gambar')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="nomor_gambar" class="required form-label">
                                {{ __('Nomor Gambar') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid input-number @error('nomor_gambar') is-invalid @enderror"
                                placeholder="Ketik disini" name="nomor_gambar" value="{{ old('nomor_gambar') }}"
                                min="0"/>

                                @error('nomor_gambar')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="tipe_gambar" class="required form-label">
                                {{ __('Tipe Gambar') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="tipe_gambar"
                                class="form-select form-select-solid @error('tipe_gambar') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($imageType as $type)
                                        <option value="{{ $type }}"
                                        {{ old('tipe_gambar') == $type ? 'selected' : '' }}>
                                            {{ ucwords($type) }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('tipe_gambar')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="status_gambar" class="required form-label">
                                {{ __('Status Gambar') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <select name="status_gambar"
                                class="form-select form-select-solid @error('status_gambar') is-invalid @enderror">
                                    <option value="">Pilih data</option>
                                    @foreach ($imageStatus as $status)
                                        <option value="{{ $status }}"
                                        {{ old('status_gambar') == $status ? 'selected' : '' }}>
                                            {{ ucwords($status) }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('status_gambar')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan" class="form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea class="form-control form-control-solid @error('keterangan') is-invalid @enderror" placeholder="Ketik disini" name="keterangan" rows="3">{{ old('keterangan') }}</textarea>

                                @error('keterangan')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row justify-content-between mb-2">
                            <div class="col-6">
                                <label for="dokumen" class="form-label">
                                    {{ __('Dokumen') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <button type="button"
                                class="btn btn-sm btn-dark float-end"
                                id="btn_add_document">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-file"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-input-file @error('dokumen.*') is-invalid @enderror">
                                <div class="row input-file">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                </div>
                            </div>
                            @error('dokumen.*')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('Ubah') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
