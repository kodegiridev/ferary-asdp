@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Port Drawing') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                    <a href="{{ route('ed.port-drawing.indexPort') }}" class="text-muted text-hover-primary">{{ __('Port Drawing') }}</a>
                </li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Port Drawing') }}</h3>
                <div class="card-toolbar" id="toolbar-action">
                    <button type="button"
                    id="btn_create_port_drawing_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_port_drawing_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                    @if ($countPortDrawing)
                        <button type="button"
                        id="btn_send_approval_port_drawing_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_port_drawing_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>
                    @endif
                    <button type="button"
                    id="btn_edit_port_drawing_modal"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_port_drawing_modal"
                    hidden>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="port_drawing_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Komponen') }}</th>
                                <th>{{ __('Nama Gambar') }}</th>
                                <th>{{ __('Nomor Gambar') }}</th>
                                <th>{{ __('Tipe Gambar') }}</th>
                                <th>{{ __('Status Gambar') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.port-drawing.modals.create')
@include('pages.engineering-database.port.port-drawing.modals.edit')
@include('pages.engineering-database.port.port-drawing.modals.send-approval')
@include('pages.engineering-database.port.port-drawing.modals.document')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

    function getCountPortDrawing()
    {
        const url = '{{ route('ed.port-drawing.countPortDrawing', $dock_id) }}'
        $.get(url, function(response) {
            if (response.data > 0) {
                if ($('#btn_send_approval_port_drawing_modal').length > 0) {
                    $('#btn_send_approval_port_drawing_modal').remove()
                }

                $("#toolbar-action").append(`<button type="button"
                        id="btn_send_approval_port_drawing_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_port_drawing_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>`)
            } else {
                $('#btn_send_approval_port_drawing_modal').remove()
            }
        })
    }

    if ($('.modal .is-invalid').length > 0) {
        var action = "{{ old('action') }}";
        $('#btn_' + action + '_port_drawing_modal').trigger('click');
    }
    
    var verificators = '{!! json_encode($verificators) !!}'
    verificators = JSON.parse(verificators)

    $(document).ready(function () {
        checkSession();

        var table = $('#port_drawing_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.port-drawing.index', $dock_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_komponen',
                    name: 'nama_komponen'
                },
                {
                    data: 'nama_gambar',
                    name: 'nama_gambar'
                },
                {
                    data: 'nomor_gambar',
                    name: 'nomor_gambar'
                },
                {
                    data: 'tipe_gambar',
                    name: 'tipe_gambar'
                },
                {
                    data: 'status_gambar',
                    name: 'status_gambar'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $('#form-create-port-drawing').submit(function(e) {
            e.preventDefault()
            const params = new FormData($('#form-create-port-drawing')[0])
            let url = '{{ route('ed.port-drawing.store', ':port_id') }}'
            url = url.replace(':port_id', '{{ $dock_id }}')

            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#create_port_drawing_modal').modal('hide')
                    Swal.fire({
                        text: response.message,
                        icon: "success"
                    });
                    table.ajax.reload()
                    getCountPortDrawing()
                },
                error: function(err){
                    Swal.fire({
                        text: err.responseJSON.message,
                        icon: 'error'
                    }) 
                }
            })
        })

        $('#form-update-port-drawing').submit(function(e) {
            e.preventDefault()
            const port_drawing_id = $('#edit_port_drawing_modal input[name="port_drawing"]').val()
            const params = new FormData($('#form-update-port-drawing')[0])
            let url = '{{ route('ed.port-drawing.update', ':port_drawing_id') }}'
            url = url.replace(':port_drawing_id', port_drawing_id)

            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#edit_port_drawing_modal').modal('hide')
                    Swal.fire({
                        text: response.message,
                        icon: "success"
                    });
                    table.ajax.reload()
                },
                error: function(err){
                    Swal.fire({
                        text: err.responseJSON.message,
                        icon: 'error'
                    }) 
                }
            })
        })

        $(document).on('click', '.btn-edit-port-drawing', function () {
            var url = `{{ route('ed.port-drawing.show', [':portDrawing']) }}`;
            url = url.replace(':portDrawing', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    var action = `{{ route('ed.port-drawing.update', [':port_id', ':portDrawing']) }}`;
                    action = action.replace(':port_id', data.id);

                    $('#edit_port_drawing_modal form').prop('action', action);
                    $('#edit_port_drawing_modal input[name="port_drawing"]').val(data.id);
                    $('#edit_port_drawing_modal input[name="nama_komponen"]').val(data.nama_komponen);
                    $('#edit_port_drawing_modal input[name="nama_gambar"]').val(data.nama_gambar);
                    $('#edit_port_drawing_modal input[name="nomor_gambar"]').val(data.nomor_gambar);
                    $('#edit_port_drawing_modal select[name="tipe_gambar"]').val(data.tipe_gambar).trigger('change');
                    $('#edit_port_drawing_modal select[name="status_gambar"]').val(data.status_gambar).trigger('change');
                    $('#edit_port_drawing_modal textarea[name="keterangan"]').val(data.keterangan);
                }
            });
        });

        $(document).on('click', '.btn-delete-port-drawing', function () {
            var url = `{{ route('ed.port-drawing.destroy', [':portDrawing']) }}`;
            url = url.replace(':portDrawing', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            Swal.fire({
                                text: response.message,
                                icon: "success"
                            });
                            table.ajax.reload()
                            getCountPortDrawing()
                        },
                        error: function(err){
                            Swal.fire({
                                text: err.responseJSON.message,
                                icon: 'error'
                            }) 
                        }
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function () {
            $('.is-invalid').each(function (index, element) {
                $(this).removeClass('is-invalid');
            });
        });

        $(document).on('click', '#btn_create_port_drawing_modal', function () {
            $('#create_port_drawing_modal input').not('input[type="hidden"]').each(function (index, element) {
                $(this).val('');
            });

            $('#create_port_drawing_modal textarea').each(function (index, element) {
                $(this).val('');
            });

            $('#create_port_drawing_modal select').each(function (index, element) {
                $(this).val('').trigger('change');
            });
        });

        $(document).on('click', '#btn_add_document', function () {
            var modal = $(this).closest('.modal');
            var boxInputFile = modal.find('.box-input-file');

            boxInputFile.append(`<div class="row input-file mt-2">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                    <div class="col-1 d-flex align-items-center">
                                        <button type="button"
                                        class="btn btn-sm btn-danger btn-delete-file">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>`);
        });

        $(document).on('click', '.btn-delete-file', function () {
            var inputFile = $(this).closest('.input-file');
            inputFile.remove();
        });

        $(document).on('click', '.btn-document-port-drawing', function () {
            var url = `{{ route('ed.port-drawing.getDocument', ':portDrawing') }}`;
            url = url.replace(':portDrawing', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_port_drawing_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.port-drawing.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_port_drawing_modal').on('hidden.bs.modal', function() {
            $(this).find('tbody').empty()
        })

        $(document).on('click', '.btn-delete-verificator', function () {
            var inputVerificator = $(this).closest('.input-verificator');
            inputVerificator.remove();

            if ($('.verificator').length < verificators.length) {
                $('#btn_add_verificator').removeClass('d-none')
            }
            
            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('keypress', '.input-number', function (e) {
            if (["e", "E", "+", "-"].includes(e.key)) {
                e.preventDefault();
            }
        });

        $(document).on('paste', '.input-number', function (e) {
            e.preventDefault();
        });

        $(document).on('click', '#btn_add_verificator', function () {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');

            boxInputVerificator.append(`<div class="row input-verificator mt-2">
                                            <div class="col-10">
                                                <select name="user_verifikator_id[]"
                                                class="form-select form-select-solid verificator" required>
                                                    <option value="">Pilih data</option>
                                                    @foreach ($verificators as $verificator)
                                                        <option value="{{ $verificator->id }}">
                                                            {{ $verificator->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-1 d-flex align-items-center">
                                                <button type="button"
                                                class="btn btn-sm btn-danger btn-delete-verificator">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>`);

            if ($('.verificator').length == verificators.length) {
                $('#btn_add_verificator').addClass('d-none')
            }

            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('change', '.verificator', function() {
            let index = $(this).data('index')
            let value = $(this).val()

            if (value != '') {
                $('.verificator').map(function(elIndex) {
                    if (index != $(this).data('index') && value == $(this).val()) {
                        $(this).val('').trigger('change') 
                    }
                })
            }
        })
    })
</script>
@endsection