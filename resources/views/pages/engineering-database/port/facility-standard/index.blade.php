@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Facility Standard') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">{{ __('Facility Standard') }}</li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Facility Standard') }}</h3>
                <div class="card-toolbar" id="toolbar-action">
                	<button type="button"
                    id="btn_create_facility_standard_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_facility_standard_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="facility_standard_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Dokumen') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>

        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Approved Facility Standard') }}</h3>
                <div class="card-toolbar" id="toolbar-action-approved">
                    
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="facility_standard_approved_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Dokumen') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                                <th>{{ __('Tanggal Disetujui') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.facility-standard.modals.form')
@include('pages.engineering-database.port.facility-standard.modals.document')
@include('pages.engineering-database.port.facility-standard.modals.send-approval')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    var verificators = []

	function getCountDocument()
    {
        const url = '{{ route('ed.facility-standard.countFacilityStandard') }}'
        $.get(url, function(response) {
            $('#btn_send_approval_modal').remove()

            if (response.data > 0) {
                $("#toolbar-action").append(`<button type="button"
                        id="btn_send_approval_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_facility_standard_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>`)
            }
        })
    }

    $(document).ready(function () {
    	getCountDocument()

        verificators = '{!! json_encode($verificators) !!}'
        verificators = JSON.parse(verificators)

        var table = $('#facility_standard_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.facility-standard.index') }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_dokumen',
                    name: 'nama_dokumen'
                },
                {
                    data: 'document',
                    name: 'document'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        var tableApproved = $('#facility_standard_approved_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.facility-standard.documentsApproved') }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_dokumen',
                    name: 'nama_dokumen'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'tanggal_disetujui',
                    name: 'tanggal_disetujui'
                },
            ],
        });

        $('#facility_standard_approved_table').on( 'draw.dt', function(){
            $('#toolbar-action-approved').find('#btn_print_facility_standard').remove()

            if (tableApproved.data().any()) {
                $('#toolbar-action-approved').append(`
                    <button type="button"
                    id="btn_print_facility_standard"
                    class="btn btn-sm btn-primary">
                        <i class="fa fa-print"></i>
                        {{ __('Cetak') }}
                    </button>
                `)
            }
        });

        $(document).on('click', '#btn_print_facility_standard', function() {
            const url = '{{ route('ed.facility-standard.print') }}'

            window.open(url, '_blank')
        })

        $('#form-create-facility-standard').submit(function(e) {
        	e.preventDefault()
        	const params = new FormData($('#form-create-facility-standard')[0])
        	const action = $('#action').val()
        	let url = '{{ route('ed.facility-standard.store') }}'
        	if (action == 'update') {
        		const id = $('#document-id').val()
        		url = '{{ route('ed.facility-standard.update', ':id') }}'
        		url = url.replace(':id', id)
        	}

        	$.ajax({
                url: url,
                type: 'POST',
                data: params,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#create_facility_standard_modal').modal('hide')
                    Swal.fire({
                        text: response.message,
                        icon: "success"
                    });
                    table.ajax.reload()
                    getCountDocument()
                },
                error: function(err){
                    Swal.fire({
                        text: err.responseJSON.message,
                        icon: 'error'
                    }) 
                }
            })
        })

        $(document).on('click', '#btn_add_document', function () {
            var modal = $(this).closest('.modal');
            var boxInputFile = modal.find('.box-input-file');

            boxInputFile.append(`<div class="row input-file mt-2 input-file-temp">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                    <div class="col-1 d-flex align-items-center">
                                        <button type="button"
                                        class="btn btn-sm btn-danger btn-delete-file">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>`);
        });

        $(document).on('click', '.btn-delete-file', function () {
            var inputFile = $(this).closest('.input-file');
            inputFile.remove();
        });

        $(document).on('click', '.btn-document-facility-standard', function () {
            var url = `{{ route('ed.facility-standard.getDocument', ':document') }}`;
            url = url.replace(':document', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_facility_standard_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.facility-standard.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_facility_standard_modal').on('hidden.bs.modal', function() {
            $(this).find('tbody').empty()
        })

        $(document).on('click', '.btn-edit-facility-standard', function () {
            $('#create_facility_standard_modal').find('.modal-title').text("{{ __('Ubah Data Dokumen') }}");

            var url = `{{ route('ed.facility-standard.show', [':facilityStandard']) }}`;
            url = url.replace(':facilityStandard', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    var action = `{{ route('ed.facility-standard.update', [':port_id']) }}`;
                    action = action.replace(':port_id', data.id);

                    $('#create_facility_standard_modal form').append(`<input type="hidden" name="id" id="document-id" value="${data.id}">`);
                    $('#create_facility_standard_modal input[name="action"]').val('update');
                    $('#create_facility_standard_modal input[name="nama_dokumen"]').val(data.nama_dokumen);
                    $('#create_facility_standard_modal textarea[name="keterangan"]').val(data.keterangan);
                    $('#create_facility_standard_modal input[name="dokumen[]"]').prop('required', false);
                    $('#create_facility_standard_modal .document-label').removeClass('required');

                    $('#create_facility_standard_modal').modal('show')
                }
            });
        });

        $('#create_facility_standard_modal').on('hidden.bs.modal', function() {
            $('#create_facility_standard_modal').find('.modal-title').text("{{ __('Tambah Data Dokumen') }}");

        	$('.input-file-temp').remove();
        	$('#document-id').remove();
            $('#create_facility_standard_modal input[name="action"]').val('create');
            $('#create_facility_standard_modal .document-label').addClass('required');
        	$('#create_facility_standard_modal input[name="nama_dokumen"]').val('');
            $('#create_facility_standard_modal textarea[name="keterangan"]').val('');
            $('#create_facility_standard_modal input[name="dokumen[]"]').val(null).prop('required', true);
        })

        $(document).on('click', '.btn-delete-facility-standard', function () {
            var url = `{{ route('ed.facility-standard.destroy', [':facilityStandard']) }}`;
            url = url.replace(':facilityStandard', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            Swal.fire({
                                text: response.message,
                                icon: "success"
                            });
                            table.ajax.reload()
                            tableApproved.ajax.reload()
                            getCountDocument()
                        },
                        error: function(err){
                            Swal.fire({
                                text: err.responseJSON.message,
                                icon: 'error'
                            }) 
                        }
                    });
                }
            });
        });

        $(document).on('click', '#btn_add_verificator', function () {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');

            boxInputVerificator.append(`<div class="row input-verificator mt-2">
                                            <div class="col-10">
                                                <select name="user_verifikator_id[]"
                                                class="form-select form-select-solid verificator" required>
                                                    <option value="">Pilih data</option>
                                                    @foreach ($verificators as $verificator)
                                                        <option value="{{ $verificator->id }}">
                                                            {{ $verificator->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-1 d-flex align-items-center">
                                                <button type="button"
                                                class="btn btn-sm btn-danger btn-delete-verificator">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>`);

            if ($('.verificator').length == verificators.length) {
                $('#btn_add_verificator').addClass('d-none')
            }

            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('click', '.btn-delete-verificator', function () {
            var inputVerificator = $(this).closest('.input-verificator');
            inputVerificator.remove();

            if ($('.verificator').length < verificators.length) {
                $('#btn_add_verificator').removeClass('d-none')
            }
            
            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('change', '.verificator', function() {
            let index = $(this).data('index')
            let value = $(this).val()

            if (value != '') {
                $('.verificator').map(function(elIndex) {
                    if (index != $(this).data('index') && value == $(this).val()) {
                        $(this).val('').trigger('change') 
                    }
                })
            }
        })
    })
</script>
@endsection