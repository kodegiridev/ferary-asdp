@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Port Specification') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                	<a href="{{ route('ed.port-specification.index') }}" class="text-muted text-hover-primary">{{ __('Port Specification') }}</a>
                </li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Port Specification') }}</h3>
                <div class="card-toolbar" id="toolbar-action">
                    <button type="button"
                    id="btn_create_port_specification_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_port_specification_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="detail_port_specification_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Komponen') }}</th>
                                <th>{{ __('Sub Komponen') }}</th>
                                <th>{{ __('Parameter') }}</th>
                                <th>{{ __('Spesifikasi') }}</th>
                                <th>{{ __('Referensi') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.port-specification.modals.form')
@include('pages.engineering-database.port.port-specification.modals.document')
@include('pages.engineering-database.port.port-specification.modals.send-approval')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
	var verificators = []

    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

	function getCountSpecification()
    {
        const url = '{{ route('ed.port-specification.countPortSpecification', $dock_id) }}'
        $.get(url, function(response) {
            $('#btn_send_approval_modal').remove()

            if (response.data > 0) {
                $("#toolbar-action").append(`<button type="button"
                        id="btn_send_approval_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_port_specification_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>`)
            }
        })
    }

    $(document).ready(function () {
        checkSession()
    	getCountSpecification()

        verificators = '{!! json_encode($verificators) !!}'
        verificators = JSON.parse(verificators)

        var table = $('#detail_port_specification_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.port-specification.indexDetail', $dock_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_komponen',
                    name: 'nama_komponen'
                },
                {
                    data: 'sub_komponen',
                    name: 'sub_komponen'
                },
                {
                    data: 'parameter',
                    name: 'parameter'
                },
                {
                    data: 'spesifikasi',
                    name: 'spesifikasi'
                },
                {
                    data: 'referensi',
                    name: 'referensi'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $('#create_port_specification_modal').on('hidden.bs.modal', function() {
        	$('.input-file-temp').remove()
        	$('#specification-id').remove()
        	$('#nama-komponen').val('')
        	$('#sub-komponen').val('')
        	$('#parameter').val(null).trigger('change')
        	$('#spesifikasi').val('')
        	$('#referensi').val('')
        	$('#keterangan').val('')
            $('#create_port_specification_modal input[name="dokumen[]"]').val('').prop('required', true);
            $('#create_port_specification_modal input[name="action"]').val('create')
            $('#create_port_specification_modal .document-label').addClass('required')
            $('#create_port_specification_modal .modal-title').html('{{ __('Tambah Data Spesifikasi') }}')
        })

        $('#form-create-port-specification').submit(function(e) {
        	e.preventDefault()
        	const action = $('#create_port_specification_modal input[name="action"]').val()
        	const params = new FormData($('#form-create-port-specification')[0])
        	let url = '{{ route('ed.port-specification.store', $dock_id) }}'
        	if (action == 'update') {
        		const id = $('#specification-id').val()
        		url = '{{ route('ed.port-specification.update', ':specification_id') }}'
        		url = url.replace(':specification_id', id)
        	}

        	$.ajax({
                url: url,
                type: 'POST',
                data: params,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#create_port_specification_modal').modal('hide')
                    Swal.fire({
                        text: response.message,
                        icon: "success"
                    });
                    table.ajax.reload()
                    getCountSpecification()
                },
                error: function(err){
                    Swal.fire({
                        text: err.responseJSON.message,
                        icon: 'error'
                    }) 
                }
            })
        })

        $(document).on('click', '.btn-edit-port-specification', function() {
        	const id = $(this).data('id')
        	let url = '{{ route('ed.port-specification.show', ':specification_id') }}'
        	url = url.replace(':specification_id', id)

        	$.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                	$('#create_port_specification_modal .modal-title').html('{{ __('Edit Data Spesifikasi') }}');
                    $('#create_port_specification_modal form').append(`<input type="hidden" name="id" id="specification-id" value="${data.id}">`);
                    $('#create_port_specification_modal input[name="action"]').val('update');
                    $('#create_port_specification_modal input[name="nama_komponen"]').val(data.nama_komponen);
                    $('#create_port_specification_modal input[name="sub_komponen"]').val(data.sub_komponen);
                    $('#create_port_specification_modal select[name="parameter"]').val(data.parameter).trigger('change');
                    $('#create_port_specification_modal input[name="spesifikasi"]').val(data.spesifikasi);
                    $('#create_port_specification_modal input[name="referensi"]').val(data.referensi);
                    $('#create_port_specification_modal textarea[name="keterangan"]').val(data.keterangan);
                    $('#create_port_specification_modal input[name="dokumen[]"]').prop('required', false);
                    $('#create_port_specification_modal .document-label').removeClass('required');

                    $('#create_port_specification_modal').modal('show');
                }
            });
        })

        $(document).on('click', '.btn-document-port-specification', function () {
            var url = `{{ route('ed.port-specification.getDocument', ':document') }}`;
            url = url.replace(':document', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_port_specification_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.port-specification.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_port_specification_modal').on('hidden.bs.modal', function() {
            $(this).find('tbody').empty()
        })

        $(document).on('click', '#btn_add_verificator', function () {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');

            boxInputVerificator.append(`<div class="row input-verificator mt-2">
                                            <div class="col-10">
                                                <select name="user_verifikator_id[]"
                                                class="form-select form-select-solid verificator" required>
                                                    <option value="">Pilih data</option>
                                                    @foreach ($verificators as $verificator)
                                                        <option value="{{ $verificator->id }}">
                                                            {{ $verificator->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-1 d-flex align-items-center">
                                                <button type="button"
                                                class="btn btn-sm btn-danger btn-delete-verificator">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>`);

            if ($('.verificator').length == verificators.length) {
                $('#btn_add_verificator').addClass('d-none')
            }

            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('click', '.btn-delete-verificator', function () {
            var inputVerificator = $(this).closest('.input-verificator');
            inputVerificator.remove();

            if ($('.verificator').length < verificators.length) {
                $('#btn_add_verificator').removeClass('d-none')
            }
            
            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('change', '.verificator', function() {
            let index = $(this).data('index')
            let value = $(this).val()

            if (value != '') {
                $('.verificator').map(function(elIndex) {
                    if (index != $(this).data('index') && value == $(this).val()) {
                        $(this).val('').trigger('change') 
                    }
                })
            }
        })

        $('#send_approval_port_specification_modal').on('hidden.bs.modal', function() {
        	$('#send_approval_port_specification_modal select[name="penyetuju_id"]').val(null).trigger('change')
        	$('.btn-delete-verificator').trigger('click')
        	$('#send_approval_port_specification_modal select[name="user_verifikator_id[]"]').val(null).trigger('change')
        })

        $(document).on('click', '.btn-delete-port-specification', function () {
            var url = `{{ route('ed.port-specification.destroy', [':portSpecification']) }}`;
            url = url.replace(':portSpecification', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            Swal.fire({
                                text: response.message,
                                icon: "success"
                            });
                            table.ajax.reload()
                            getCountSpecification()
                        },
                        error: function(err){
                            Swal.fire({
                                text: err.responseJSON.message,
                                icon: 'error'
                            }) 
                        }
                    });
                }
            });
        });

        $(document).on('click', '#btn_add_document', function () {
            var modal = $(this).closest('.modal');
            var boxInputFile = modal.find('.box-input-file');

            boxInputFile.append(`<div class="row input-file mt-2 input-file-temp">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                    <div class="col-1 d-flex align-items-center">
                                        <button type="button"
                                        class="btn btn-sm btn-danger btn-delete-file">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>`);
        });

        $(document).on('click', '.btn-delete-file', function () {
            var inputFile = $(this).closest('.input-file');
            inputFile.remove();
        });
    });
</script>
@endsection