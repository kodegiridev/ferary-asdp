@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Port Specification') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                	<a href="{{ route('ed.port-specification.index') }}" class="text-muted text-hover-primary">{{ __('Port Specification') }}</a>
                </li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Port Specification') }}</h3>
                <div class="card-toolbar" id="toolbar-action">
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="detail_port_specification_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Komponen') }}</th>
                                <th>{{ __('Sub Komponen') }}</th>
                                <th>{{ __('Parameter') }}</th>
                                <th>{{ __('Spesifikasi') }}</th>
                                <th>{{ __('Referensi') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                                <th>{{ __('Tanggal Disetujui') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.port-specification.modals.document')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
        var table = $('#detail_port_specification_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.port-specification.detailApproved', $dock_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_komponen',
                    name: 'nama_komponen'
                },
                {
                    data: 'sub_komponen',
                    name: 'sub_komponen'
                },
                {
                    data: 'parameter',
                    name: 'parameter'
                },
                {
                    data: 'spesifikasi',
                    name: 'spesifikasi'
                },
                {
                    data: 'referensi',
                    name: 'referensi'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'tanggal_disetujui',
                    name: 'tanggal_disetujui'
                },
            ],
        });

        $('#detail_port_specification_table').on( 'draw.dt', function(){
            $('#toolbar-action').find('#btn_print_port_specification').remove()

            if (table.data().any()) {
                $('#toolbar-action').append(`
                    <button type="button"
                    id="btn_print_port_specification"
                    class="btn btn-sm btn-primary">
                        <i class="fa fa-print"></i>
                        {{ __('Cetak') }}
                    </button>
                `)
            }
        });

        $(document).on('click', '#btn_print_port_specification', function() {
            const url = '{{ route('ed.port-specification.print', $dock_id) }}'

            window.open(url, '_blank')
        })

        $(document).on('click', '.btn-document-port-specification', function () {
            var url = `{{ route('ed.port-specification.getDocument', ':document') }}`;
            url = url.replace(':document', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_port_specification_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.port-specification.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_port_specification_modal').on('hidden.bs.modal', function() {
            $(this).find('tbody').empty()
        })

        $(document).on('click', '.btn-delete-port-specification', function () {
            var url = `{{ route('ed.port-specification.destroy', [':portSpecification']) }}`;
            url = url.replace(':portSpecification', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            Swal.fire({
                                text: response.message,
                                icon: "success"
                            });
                            table.ajax.reload()
                        },
                        error: function(err){
                            Swal.fire({
                                text: err.responseJSON.message,
                                icon: 'error'
                            }) 
                        }
                    });
                }
            });
        });
    });
</script>
@endsection