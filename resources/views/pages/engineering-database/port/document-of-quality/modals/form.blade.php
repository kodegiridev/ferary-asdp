<div class="modal fade" tabindex="-1" id="create_document_of_quality_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ __('Tambah Data Dokumen') }}</h3>

                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-x"></i>
                </div>
            </div>

            <form method="POST" enctype="multipart/form-data" id="form-create-document-of-quality">
                @csrf
                <input type="hidden" value="create" name="action" id="action">
                <div class="modal-body">
                	<div class="form-group">
                        <div class="row">
                            <label for="nama_dokumen" class="required form-label">
                                {{ __('Nama Dokumen') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                            	<select class="form-control form-control-solid" id="nama-dokumen" name="nama_dokumen" required>
                            		<option value="">Pilih data</option>
                            		<option value="Approval Material">Approval Material</option>
                            		<option value="Material Certificate">Material Certificate</option>
                            	</select>

                                @error('nama_dokumen')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="nama_komponen" class="required form-label">
                                {{ __('Nama Komponen') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="text"
                                class="form-control form-control-solid @error('nama_komponen') is-invalid @enderror"
                                placeholder="Ketik disini" name="nama_komponen" id="nama-komponen" required value="{{ old('nama_komponen') }}"/>

                                @error('nama_komponen')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="keterangan" class="form-label">
                                {{ __('Keterangan') }}
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <textarea class="form-control form-control-solid @error('keterangan') is-invalid @enderror" placeholder="Ketik disini" name="keterangan" id="keterangan" rows="3">{{ old('keterangan') }}</textarea>

                                @error('keterangan')
                                    <span class="invalid-feedback mb-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <div class="row justify-content-between mb-2">
                            <div class="col-6">
                                <label for="dokumen" class="required form-label document-label">
                                    {{ __('Dokumen') }}
                                </label>
                            </div>
                            <div class="col-6">
                                <button type="button"
                                class="btn btn-sm btn-dark float-end"
                                id="btn_add_document">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-file"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-input-file 
                            @error('dokumen') is-invalid @enderror
                            @error('dokumen.*') is-invalid @enderror">
                                <div class="row input-file">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/
                                        required>
                                    </div>
                                </div>
                            </div>
                            @error('dokumen')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @error('dokumen.*')
                                <span class="invalid-feedback mb-2" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">{{ __('Batal') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
