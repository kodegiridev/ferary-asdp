@extends('layout.demo1.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.css') }}" type="text/css">
@endsection

@section('content-module')
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
	<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {{ __('Data Document Of Quality') }}
            </h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="/" class="text-muted text-hover-primary">{{ __('Beranda') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-400 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                	<a href="{{ route('ed.document-of-quality.index') }}" class="text-muted text-hover-primary">{{ __('Document Of Quality') }}</a>
                </li>
            </ul>
        </div>
	</div>
</div>

<div id="kt_app_content" class="app-content flex-column-fluid">
	<div id="kt_app_content_container" class="app-container container-fluid">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">{{ __('Data Document Of Quality') }}</h3>
                <div class="card-toolbar" id="toolbar-action">
                    <button type="button"
                    id="btn_create_document_of_quality_modal"
                    class="btn btn-sm btn-light"
                    data-bs-toggle="modal"
                    data-bs-target="#create_document_of_quality_modal">
                        <i class="fa fa-plus"></i>
                        {{ __('Tambah') }}
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered nowrap" id="detail_document_of_quality_table">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800">
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Nama Dokumen') }}</th>
                                <th>{{ __('Nama Komponen') }}</th>
                                <th>{{ __('Keterangan') }}</th>
                                <th>{{ __('Dokumen') }}</th>
                                <th>{{ __('Aksi') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
@endsection

@include('pages.engineering-database.port.document-of-quality.modals.form')
@include('pages.engineering-database.port.document-of-quality.modals.document')
@include('pages.engineering-database.port.document-of-quality.modals.send-approval')

@section('scripts')
<script src="{{ asset('demo1/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script>
    var verificators = []

    function checkSession() {
        if ('{{ Session::has('success') }}') {
            Swal.fire({
                text: '{{ Session::get('success') }}',
                icon: "success"
            });
        }

        if ('{{ Session::has('failed') }}') {
            Swal.fire({
                text: '{{ Session::get('failed') }}',
                icon: "error"
            });
        }
    }

	function getCountDocument()
    {
        const url = '{{ route('ed.document-of-quality.countDocumentOfQuality', $dock_id) }}'
        $.get(url, function(response) {
            $('#btn_send_approval_modal').remove()
            
            if (response.data > 0) {
                $("#toolbar-action").append(`<button type="button"
                        id="btn_send_approval_modal"
                        class="btn btn-sm btn-dark ms-3"
                        data-bs-toggle="modal"
                        data-bs-target="#send_approval_document_of_quality_modal">
                            <i class="fa fa-paper-plane"></i>
                            {{ __('Kirim') }}
                        </button>`)
            }
        })
    }

    $(document).ready(function () {
    	checkSession()
    	getCountDocument()

    	verificators = '{!! json_encode($verificators) !!}'
    	verificators = JSON.parse(verificators)

        var table = $('#detail_document_of_quality_table').DataTable({
            proccesing: true,
            serverSide: true,
            order: [],
            ajax: {
                url: "{{ route('ed.document-of-quality.indexDetail', $dock_id) }}"
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                },
                {
                    data: 'nama_dokumen',
                    name: 'nama_dokumen'
                },
                {
                    data: 'nama_komponen',
                    name: 'nama_komponen'
                },
                {
                    data: 'keterangan',
                    name: 'keterangan'
                },
                {
                    data: 'document',
                    name: 'document'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });

        $('#create_document_of_quality_modal').on('hidden.bs.modal', function() {
        	$('.input-file-temp').remove()
        	$('#document-id').remove()

        	$('#create_document_of_quality_modal select[name="nama_dokumen"]').val('').trigger('change')
        	$('#create_document_of_quality_modal input[name="nama_komponen"]').val('')
        	$('#create_document_of_quality_modal textarea[name="keterangan"]').val('')
            $('#create_document_of_quality_modal input[name="dokumen[]"]').val('').prop('required', true);
            $('#create_document_of_quality_modal input[name="action"]').val('create')
            $('#create_document_of_quality_modal .document-label').addClass('required')
            $('#create_document_of_quality_modal .modal-title').html('{{ __('Tambah Data Dokumen') }}')
        })

        $('#form-create-document-of-quality').submit(function(e) {
        	e.preventDefault()
        	const params = new FormData($('#form-create-document-of-quality')[0])
        	const action = $('#form-create-document-of-quality input[name="action"]').val()
        	let url = '{{ route('ed.document-of-quality.store', $dock_id) }}'

        	if (action == 'update') {
        		let id = $('#document-id').val()
        		url = '{{ route('ed.document-of-quality.update', ':documentOfQuality') }}'
        		url = url.replace(':documentOfQuality', id)
        	}

        	$.ajax({
                url: url,
                type: 'POST',
                data: params,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#create_document_of_quality_modal').modal('hide')
                    Swal.fire({
                        text: response.message,
                        icon: "success"
                    });
                    table.ajax.reload()
                    getCountDocument()
                },
                error: function(err){
                    Swal.fire({
                        text: err.responseJSON.message,
                        icon: 'error'
                    }) 
                }
            })
        })

        $(document).on('click', '#btn_add_document', function () {
            var modal = $(this).closest('.modal');
            var boxInputFile = modal.find('.box-input-file');

            boxInputFile.append(`<div class="row input-file mt-2 input-file-temp">
                                    <div class="col-10">
                                        <input type="file"
                                        class="form-control form-control-solid"
                                        name="dokumen[]"
                                        accept="application/pdf"/>
                                    </div>
                                    <div class="col-1 d-flex align-items-center">
                                        <button type="button"
                                        class="btn btn-sm btn-danger btn-delete-file">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>`);
        });

        $(document).on('click', '.btn-delete-file', function () {
            var inputFile = $(this).closest('.input-file');
            inputFile.remove();
        });

        $(document).on('click', '.btn-document-document-of-quality', function () {
            var url = `{{ route('ed.document-of-quality.getDocument', ':document') }}`;
            url = url.replace(':document', $(this).data('id'));

            $.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {

                    var tableBody = $('#document_document_of_quality_modal').find('tbody');
                    tableBody.empty();

                    $.each(data, function (key, value) {
                        var href = `{{ route('ed.document-of-quality.viewDocument', ':document_id') }}`;
                        href = href.replace(':document_id', value.id);

                        tableBody.append(`<tr>
                                            <td>
                                                ${key + 1}
                                            </td>
                                            <td>
                                                ${value.file.split('/')[3]}
                                            </td>
                                            <td>
                                                <a class="btn btn-sm btn-dark"
                                                href="${href}" target="_blank">
                                                {{ __('Lihat') }}</a>
                                            </td>
                                        </tr>`)
                    });
                }
            });
        });

        $('#document_document_of_quality_modal').on('hidden.bs.modal', function() {
        	$(this).find('tbody').empty()
        })

        $(document).on('click', '#btn_add_verificator', function () {
            var modal = $(this).closest('.modal');
            var boxInputVerificator = modal.find('.box-input-verificator');

            boxInputVerificator.append(`<div class="row input-verificator mt-2">
                                            <div class="col-10">
                                                <select name="user_verifikator_id[]"
                                                class="form-select form-select-solid verificator" required>
                                                    <option value="">Pilih data</option>
                                                    @foreach ($verificators as $verificator)
                                                        <option value="{{ $verificator->id }}">
                                                            {{ $verificator->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-1 d-flex align-items-center">
                                                <button type="button"
                                                class="btn btn-sm btn-danger btn-delete-verificator">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>`);

            if ($('.verificator').length == verificators.length) {
                $('#btn_add_verificator').addClass('d-none')
            }

            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('click', '.btn-delete-verificator', function () {
            var inputVerificator = $(this).closest('.input-verificator');
            inputVerificator.remove();

            if ($('.verificator').length < verificators.length) {
                $('#btn_add_verificator').removeClass('d-none')
            }
            
            $('.verificator').map(function(index) {
                $(this).attr('data-index', index)
            })
        });

        $(document).on('change', '.verificator', function() {
            let index = $(this).data('index')
            let value = $(this).val()

            if (value != '') {
                $('.verificator').map(function(elIndex) {
                    if (index != $(this).data('index') && value == $(this).val()) {
                        $(this).val('').trigger('change') 
                    }
                })
            }
        })

        $('#send_approval_document_of_quality_modal').on('hidden.bs.modal', function() {
        	$('#send_approval_document_of_quality_modal select[name="penyetuju_id"]').val(null).trigger('change')
        	$('.btn-delete-verificator').trigger('click')
        	$('#send_approval_document_of_quality_modal select[name="user_verifikator_id[]"]').val(null).trigger('change')
        })

        $(document).on('click', '.btn-edit-document-of-quality', function() {
        	const id = $(this).data('id')
        	let url = '{{ route('ed.document-of-quality.show', ':document_id') }}'
        	url = url.replace(':document_id', id)

        	$.ajax({
                url: url,
                type: "GET",
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                	$('#create_document_of_quality_modal .modal-title').html('{{ __('Edit Data Dokumen') }}')
                    $('#create_document_of_quality_modal form').append(`<input type="hidden" name="id" id="document-id" value="${data.id}">`);
                    $('#create_document_of_quality_modal input[name="action"]').val('update')
                    $('#create_document_of_quality_modal select[name="nama_dokumen"]').val(data.nama_dokumen);
                    $('#create_document_of_quality_modal input[name="nama_komponen"]').val(data.nama_komponen);
                    $('#create_document_of_quality_modal textarea[name="keterangan"]').val(data.keterangan);
                    $('#create_document_of_quality_modal input[name="dokumen[]"]').prop('required', false);
                    $('#create_document_of_quality_modal .document-label').removeClass('required')

                    $('#create_document_of_quality_modal').modal('show')
                }
            });
        })

        $(document).on('click', '.btn-delete-document-of-quality', function () {
            var url = `{{ route('ed.document-of-quality.destroy', [':documentOfQuality']) }}`;
            url = url.replace(':documentOfQuality', $(this).data('id'));

            Swal.fire({
                title: "{{ __('Apakah kamu yakin?') }}",
                text: "{{ __('Data akan terhapus secara permanen!') }}",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "{{ __('Ya') }}",
                cancelButtonText: "{{ __('Batal') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            Swal.fire({
                                text: response.message,
                                icon: "success"
                            });
                            table.ajax.reload()
                            getCountDocument()
                        },
                        error: function(err){
                            Swal.fire({
                                text: err.responseJSON.message,
                                icon: 'error'
                            }) 
                        }
                    });
                }
            });
        });
    });
</script>
@endsection