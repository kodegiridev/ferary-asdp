<!--begin::sidebar menu-->
<div class="app-sidebar-menu overflow-hidden flex-column-fluid">
    <!--begin::Menu wrapper-->
    <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
        <!--begin::Menu-->
        <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="true">
            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-icon">
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                                <path opacity="0.3" d="M16.5 9C16.5 13.125 13.125 16.5 9 16.5C4.875 16.5 1.5 13.125 1.5 9C1.5 4.875 4.875 1.5 9 1.5C13.125 1.5 16.5 4.875 16.5 9Z" fill="currentColor"></path>
                                <path d="M9 16.5C10.95 16.5 12.75 15.75 14.025 14.55C13.425 12.675 11.4 11.25 9 11.25C6.6 11.25 4.57499 12.675 3.97499 14.55C5.24999 15.75 7.05 16.5 9 16.5Z" fill="currentColor"></path>
                                <rect x="7" y="6" width="4" height="4" rx="2" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </span>
                    <span class="menu-title">
                        User Management
                    </span>
                    <span class="menu-arrow">
                    </span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('users.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                User
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('roles.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Role
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('permissions.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Permission
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="menu-item menu-sub-indention menu-accordion" data-kt-menu-trigger="click">
                <!--begin::Menu link-->
                <a href="#" class="menu-link">
                    <span class="menu-icon">
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                                <path opacity="0.3" d="M16.5 9C16.5 13.125 13.125 16.5 9 16.5C4.875 16.5 1.5 13.125 1.5 9C1.5 4.875 4.875 1.5 9 1.5C13.125 1.5 16.5 4.875 16.5 9Z" fill="currentColor"></path>
                                <path d="M9 16.5C10.95 16.5 12.75 15.75 14.025 14.55C13.425 12.675 11.4 11.25 9 11.25C6.6 11.25 4.57499 12.675 3.97499 14.55C5.24999 15.75 7.05 16.5 9 16.5Z" fill="currentColor"></path>
                                <rect x="7" y="6" width="4" height="4" rx="2" fill="currentColor"></rect>
                            </svg>
                        </span>
                    </span>
                    <span class="menu-title">Master Data</span>
                    <span class="menu-arrow"></span>
                </a>
                <!--end::Menu link-->

                <!--begin::Menu sub-->
                <div class="menu-sub menu-sub-accordion">
                    <!--begin::Menu item-->
                    <div class="menu-item menu-accordion" data-kt-menu-trigger="click">
                        <!--begin::Menu link-->
                        <a href="#" class="menu-link">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Fleet</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <!--end::Menu link-->

                        <!--begin::Menu sub-->
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.region.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Region</span>
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.branch.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Branch</span>
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.sub_branch.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Sub Branch</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.route.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Rute</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.typeoftrack.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Type Of Track</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.track.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Track</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('fleet.fleet.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Fleet</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu sub-->
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item menu-accordion" data-kt-menu-trigger="click">
                        <!--begin::Menu link-->
                        <a href="#" class="menu-link">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Port</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <!--end::Menu link-->

                        <!--begin::Menu sub-->
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('port.region.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Region</span>
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('port.branch.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Branch</span>
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('port.sub_branch.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Sub Branch</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('port.port.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Port</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item">
                                <a href="{{ route('port.dock.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Dock</span>
                                </a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu sub-->
                    </div>
                    <!--end::Menu item-->
                </div>
                <!--end::Menu sub-->
            </div>

            <div class="menu-item menu-sub-indention menu-accordion" data-kt-menu-trigger="click">
                <a href="#" class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/general/gen005.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">Engineering Database</span>
                    <span class="menu-arrow"></span>
                </a>

                <div class="menu-sub menu-sub-accordion">

                    <div class="menu-item menu-accordion" data-kt-menu-trigger="click">
                        <a href="#" class="menu-link">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Fleet</span>
                            <span class="menu-arrow"></span>
                        </a>

                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item">
                                <a href="{{ route('ed.equipment-board.indexFleet') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Equipment Board</span>
                                </a>
                            </div>

                            <div class="menu-item">
                                <a href="{{ route('ed.engineering-document.indexFleet') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Engineering Document</span>
                                </a>
                            </div>

                            <div class="menu-item">
                                <a href="{{ route('ed.manual-book.indexFleet') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Manual Book</span>
                                </a>
                            </div>

                            <div class="menu-item">
                                <a href="{{ route('ed.ship-document.indexFleet') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Ship Document</span>
                                </a>
                            </div>

                            <div class="menu-item">
                                <a href="{{ route('ed.fleet-technical-standard.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Fleet Technical Standard</span>
                                </a>
                            </div>

                            <div class="menu-item">
                                <a href="{{ route('ed.fleet-facility-standard.index') }}" class="menu-link">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Fleet Facility Standard</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                        <span class="menu-link">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Port
                            </span>
                            <span class="menu-arrow">
                            </span>
                        </span>
                        <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                            <div class="menu-item">
                                <a class="menu-link" href="{{ route('ed.port-drawing.indexPort') }}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">
                                        Port Drawing
                                    </span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="{{ route('ed.technical-quality-standard.index') }}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">
                                        Technical Quality Standard
                                    </span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="{{ route('ed.facility-standard.index') }}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">
                                        Facility Standard
                                    </span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="{{ route('ed.port-specification.index') }}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">
                                        Port Specification
                                    </span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="{{ route('ed.document-of-quality.index') }}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">
                                        Document Of Quality
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--begin::E-Report-->
            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/general/gen005.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">
                        Electronic Report
                    </span>
                    <span class="menu-arrow">
                    </span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('e-report.penugasan.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Penugasan
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('e-report.progress.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Progress
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('e-report.finish.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Selesai
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end::E-Report-->

            <!--begin::Approval-->
            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/files/fil008.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">
                        Approval
                    </span>
                    <span class="menu-arrow">
                    </span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('approval.status.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Status
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('approval.persetujuan.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Persetujuan
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end::Approval-->

            <!--begin::Project Monitoring-->
            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/general/gen062.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">
                        Project Monitoring
                    </span>
                    <span class="menu-arrow">
                    </span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('ipm.fleet.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Fleet
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('ipm.port.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Port
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end::Project Monitoring-->

            <!--begin::E-RCM-->
            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/coding/cod006.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">
                        E-RCM
                    </span>
                    <span class="menu-arrow">
                    </span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-active-bg" style="display: none; overflow: hidden;">
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('e-rcm.fleet.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Fleet
                            </span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('e-rcm.port.index') }}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">
                                Port
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!--end::E-RCM-->

            <div class="menu-item">
                <a href="{{ route('activity-log.index') }}" class="menu-link">
                    <span class="menu-icon">
                        {!! theme()->getSvgIcon('icons/duotune/general/gen013.svg', 'svg-icon-1') !!}
                    </span>
                    <span class="menu-title">Activity Log</span>
                </a>
            </div>
        </div>
        <!--end::Menu-->
    </div>
    <!--end::Menu wrapper-->
</div>
<!--end::sidebar menu-->
