<!--begin::Navbar-->
<div class="app-navbar flex-shrink-0">
    <!--begin::Search-->
    <div class="app-navbar-item align-items-stretch ms-1 ms-lg-3"></div>
    <!--end::Search-->
    <!--begin::Activities-->
    <div class="app-navbar-item ms-1 ms-lg-3"></div>
    <!--end::Activities-->
    <!--begin::Notifications-->
    <div class="app-navbar-item ms-1 ms-lg-3">
        <!--begin::Menu- wrapper-->
        <div class="btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
            <!--begin::Svg Icon | path: icons/duotune/general/gen007.svg-->
            <span class="svg-icon svg-icon-1 position-relative">
                {!! theme()->getSvgIcon('icons/duotune/general/gen007.svg', 'svg-icon-1') !!}

                <span class="position-absolute top-0 start-100 translate-middle badge badge-light-primary"
                id="unread_notifications">
                </span>
            </span>
            <!--end::Svg Icon-->
        </div>
        @include('partials/menus/_notifications-menu')
        <!--end::Menu wrapper-->
    </div>
    <!--end::Notifications-->
    <!--begin::Chat-->
    {{-- <div class="app-navbar-item ms-1 ms-lg-3"></div> --}}
    <!--end::Chat-->
    <!--begin::Quick links-->
    {{-- <div class="app-navbar-item ms-1 ms-lg-3"></div> --}}
    <!--end::Quick links-->
    <!--begin::Theme mode-->
    <div class="app-navbar-item ms-1 ms-lg-3">
        @include('partials/theme-mode/_main')
    </div>
    <!--end::Theme mode-->
    <!--begin::User menu-->
    <div class="app-navbar-item ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
        <!--begin::Menu wrapper-->
        <div class="cursor-pointer symbol symbol-35px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
            @if (auth()->user()->avatarUrl)
                <img src="{{ auth()->user()->avatarUrl }}" alt="user" />
            @else
                <span class="symbol-label symbol-label-name font-size-h5 font-weight-bold">{{ ucfirst(substr(auth()->user()->name ?? 'Admin', 0, 1)) }}</span>
            @endif
        </div>
        @include('partials/menus/_user-account-menu')
        <!--end::Menu wrapper-->
    </div>
    <!--end::User menu-->
    <!--begin::Header menu toggle-->
    <div class="app-navbar-item d-lg-none ms-2 me-n3" title="Show header menu"></div>
    <!--end::Header menu toggle-->
</div>
<!--end::Navbar-->
