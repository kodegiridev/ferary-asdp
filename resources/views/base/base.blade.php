<!DOCTYPE html>
{{--
Product Name: {{ theme()->getOption('product', 'description') }}
Author: KeenThemes
Purchase: {{ theme()->getOption('product', 'purchase') }}
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: {{ theme()->getOption('product', 'license') }}
--}}
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"{!! theme()->printHtmlAttributes('html') !!} {{ theme()->printHtmlClasses('html') }}>
{{-- begin::Head --}}
<head>
    <meta charset="utf-8"/>
    <title>{{ ucfirst(theme()->getOption('meta', 'title')) }}</title>
    <meta name="description" content="{{ ucfirst(theme()->getOption('meta', 'description')) }}"/>
    <meta name="keywords" content="{{ theme()->getOption('meta', 'keywords') }}"/>
    <link rel="canonical" href="{{ ucfirst(theme()->getOption('meta', 'canonical')) }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="{{ getAsset(theme()->getOption('assets', 'favicon')) }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- begin::Fonts --}}
    {{ theme()->includeFonts() }}
    {{-- end::Fonts --}}

    @if (theme()->hasVendorFiles('css'))
        {{-- begin::Page Vendor Stylesheets(used by this page) --}}
        @foreach (array_unique(theme()->getVendorFiles('css')) as $file)
            @if(util()->isExternalURL($file))
                <link rel="stylesheet" href="{{ $file }}"/>
            @else
                {!! preloadCss(getAsset($file)) !!}
            @endif
        @endforeach
        {{-- end::Page Vendor Stylesheets --}}
    @endif

    @if (theme()->hasOption('page', 'assets/custom/css'))
        {{-- begin::Page Custom Stylesheets(used by this page) --}}
        @foreach (array_unique(theme()->getOption('page', 'assets/custom/css')) as $file)
            {!! preloadCss(getAsset($file)) !!}
        @endforeach
        {{-- end::Page Custom Stylesheets --}}
    @endif

    @if (theme()->hasOption('assets', 'css'))
        {{-- begin::Global Stylesheets Bundle(used by all pages) --}}
        @foreach (array_unique(theme()->getOption('assets', 'css')) as $file)
            @if (strpos($file, 'plugins') !== false)
                {!! preloadCss(getAsset($file)) !!}
            @else
                <link href="{{ getAsset($file) }}" rel="stylesheet" type="text/css"/>
            @endif
        @endforeach
        {{-- end::Global Stylesheets Bundle --}}
    @endif

    @if (theme()->getViewMode() === 'preview')
        {{ theme()->getView('partials/trackers/_ga-general') }}
        {{ theme()->getView('partials/trackers/_ga-tag-manager-for-head') }}
    @endif

    @yield('styles')
</head>
{{-- end::Head --}}

{{-- begin::Body --}}
<body {!! theme()->printHtmlAttributes('body') !!} {!! theme()->printHtmlClasses('body') !!} {!! theme()->printCssVariables('body') !!} data-kt-name="metronic">

@yield('content')

{{-- begin::Javascript --}}
@if (theme()->hasOption('assets', 'js'))
    {{-- begin::Global Javascript Bundle(used by all pages) --}}
    @foreach (array_unique(theme()->getOption('assets', 'js')) as $file)
        <script src="{{ getAsset($file) }}"></script>
    @endforeach
    {{-- end::Global Javascript Bundle --}}
@endif

@if (theme()->hasVendorFiles('js'))
    {{-- begin::Page Vendors Javascript(used by this page) --}}
    @foreach (array_unique(theme()->getVendorFiles('js')) as $file)
        @if(util()->isExternalURL($file))
            <script src="{{ $file }}"></script>
        @else
            <script src="{{ getAsset($file) }}"></script>
        @endif
    @endforeach
    {{-- end::Page Vendors Javascript --}}
@endif

@if (theme()->hasOption('page', 'assets/custom/js'))
    {{-- begin::Page Custom Javascript(used by this page) --}}
    @foreach (array_unique(theme()->getOption('page', 'assets/custom/js')) as $file)
        <script src="{{ getAsset($file) }}"></script>
    @endforeach
    {{-- end::Page Custom Javascript --}}
@endif
{{-- end::Javascript --}}

@if (theme()->getViewMode() === 'preview')
    {{ theme()->getView('partials/trackers/_ga-tag-manager-for-body') }}
@endif

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'X-Requested-With': 'XMLHttpRequest'
        },
        xhrFields: {
        withCredentials: true
        }
    });

    function readNotification(id, element) {
        var url = "{{ route('notifications.update', ':id') }}";
        url = url.replace(':id', id);

        $.ajax({
            type: "PUT",
            url: url,
            success: function (response) {
                if (response.status == 'success') {
                    element.removeClass('bg-light-primary');

                    href = element.find('a').prop('href')
                    location.href = href;
                }
            }
        });
    }

    function getNotification() {
        $.ajax({
            type: "GET",
            url: "{{ route('notifications.index') }}",
            success: function (response) {
                var notificationBox = $('#notification_box');
                notificationBox.empty()
                $('#unread_notifications').text(response.unread_notification);

                $.each(response.data, function (key, value) {
                    notificationBox.append(`<div class="w-100 ${!value.read_at ? 'bg-light-primary' : ''}
                                            px-8 notification-bar mb-3">
                                                <input type="hidden" name="id" value="${value.id}">
                                                <div class="d-flex flex-stack">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mb-0 me-2">
                                                            <a href="${value.action}"
                                                            class="fs-6 text-gray-800 text-hover-primary fw-bold">
                                                                ${value.title}
                                                            </a>
                                                            <div class="text-gray-400 fs-7">${value.data}</div>
                                                        </div>
                                                    </div>
                                                    <span class="badge badge-light fs-8">${value.created_at}</span>
                                                </div>
                                            </div>`)
                });
            }
        });
    }

    $(document).ready(function () {
        if (location.href.indexOf('login') < 0) {
            getNotification();
            setInterval(getNotification, 10000);
        }

        $(document).on('click', '.notification-bar', function () {
            var id = $(this).find('input[name="id"]').val();

            readNotification(id, $(this));
        });
    })
</script>
@yield('scripts')
</body>
{{-- end::Body --}}
</html>
