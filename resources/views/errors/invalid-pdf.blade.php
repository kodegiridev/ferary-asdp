<x-error-layout>

    <!--begin::Wrapper-->
    <div class="card card-flush w-lg-650px py-5">
        <div class="card-body py-15 py-lg-20">
            <!--begin::Title-->
            <h1 class="fw-bolder fs-2hx text-gray-900 mb-4">Maaf! Dokumen Anda Tidak Valid</h1>
            <!--end::Title-->
            <!--begin::Illustration-->
            <div class="mb-3">
                <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" class="mw-100 mh-150px theme-light-show" alt="">
                <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" class="mw-100 mh-150px theme-dark-show" alt="">
            </div>
            <!--end::Illustration-->
            <!--begin::Text-->
            <div class="fw-semibold fs-6 mt-7">
                <a href="/" class="text-muted text-hover-primary">
                    {{ ucfirst(theme()->getOption('meta', 'title')) }}
                </a>
            </div>
            <!--end::Text-->
            <!--begin::Link-->
            <div class="mb-0">
            </div>
            <!--end::Link-->
        </div>
    </div>
    <!--end::Wrapper-->

</x-error-layout>
