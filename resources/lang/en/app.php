<?php
return [
    'notification' => [
        'engineering_database' => 'Dokumen Telah disubmit ke Verifikator',
        'approval' => [
            'approve' => 'Dokumen telah disetujui',
            'reject' => 'Dokumen telah ditolak'
        ],
        'e_report' => [
            'assignment' => [
                'submit' => 'Usulan Penugasan [TITLE] telah disubmit ke [NAME]',
                'disposition' => 'Disposisi Penugasan [TITLE] telah disubmit ke [NAME]',
                'target' => [
                    'submit' => 'Usulan Target dan Capaian Penugasan [TITLE] telah disubmit ke [NAME]',
                    'approve' => 'Usulan Target dan Capaian Penugasan [TITLE] telah disetujui',
                    'reject' => 'Usulan Target dan Capaian Penugasan [TITLE] telah ditolak'
                ]
            ],
            'progress' => [
                'submit' => 'Penugasan [TITLE] telah disubmit ke [NAME]',
                'approve' => 'Penugasan [TITLE] telah disetujui',
                'reject' => 'Penugasan [TITLE] telah ditolak',
                'revision' => [
                    'submit' => 'Perbaikan Penugasan [TITLE] telah disubmit ke [NAME]',
                    'approve' => 'Perbaikan Penugasan [TITLE] telah disetujui',
                    'reject' => 'Perbaikan Penugasan [TITLE] telah ditolak',
                ]
            ]
        ],
        'ipm' => [
            'project' => [
                'Project [TITLE] telah disubmit'
            ],
            'executive_summary' => [
                'Dokumen Executive Summary telah disubmit ke Verifikator'
            ],
            'project_completion' => [
                'Dokumen Project Completion telah disubmit ke Verifikator'
            ]
        ]
    ],
    'mail' => [
        'engineering_database' => [
            'subject' => 'ASDP Ferary - Engineering Database',
        ],
        'approval' => [
            'subject' => 'ASDP Ferary - Approval',
        ],
        'e_report' => [
            'assignment' => [
                'subject' => 'ASDP Ferary - Electronic Report Penugasan'
            ],
            'progress' => [
                'subject' => 'ASDP Ferary - Electronic Report Progres'
            ]
        ],
        'ipm' => [
            'fleet' => [
                'subject' => 'ASDP Ferary - Project Monitoring Fleet'
            ],
            'port' => [
                'subject' => 'ASDP Ferary - Project Monitoring Port'
            ]
        ]
    ]
];