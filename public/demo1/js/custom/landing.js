/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/assets/demo1/js/custom/landing.js":
/*!*****************************************************!*\
  !*** ./resources/assets/demo1/js/custom/landing.js ***!
  \*****************************************************/
/***/ ((module) => {

eval("\n\n// Class definition\nvar KTLandingPage = function () {\n  // Private methods\n  var initTyped = function initTyped() {\n    var typed = new Typed(\"#kt_landing_hero_text\", {\n      strings: [\"The Best Theme Ever\", \"The Most Trusted Theme\", \"#1 Selling Theme\"],\n      typeSpeed: 50\n    });\n  };\n\n  // Public methods\n  return {\n    init: function init() {\n      //initTyped();\n    }\n  };\n}();\n\n// Webpack support\nif (true) {\n  module.exports = KTLandingPage;\n}\n\n// On document ready\nKTUtil.onDOMContentLoaded(function () {\n  KTLandingPage.init();\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvYXNzZXRzL2RlbW8xL2pzL2N1c3RvbS9sYW5kaW5nLmpzIiwibWFwcGluZ3MiOiJBQUFhOztBQUViO0FBQ0EsSUFBSUEsYUFBYSxHQUFHLFlBQVk7RUFDNUI7RUFDQSxJQUFJQyxTQUFTLEdBQUcsU0FBWkEsU0FBU0EsQ0FBQSxFQUFjO0lBQ3ZCLElBQUlDLEtBQUssR0FBRyxJQUFJQyxLQUFLLENBQUMsdUJBQXVCLEVBQUU7TUFDM0NDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixFQUFFLHdCQUF3QixFQUFFLGtCQUFrQixDQUFDO01BQzlFQyxTQUFTLEVBQUU7SUFDZixDQUFDLENBQUM7RUFDTixDQUFDOztFQUVEO0VBQ0EsT0FBTztJQUNIQyxJQUFJLEVBQUUsU0FBQUEsS0FBQSxFQUFZO01BQ2Q7SUFBQTtFQUVSLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQzs7QUFFSDtBQUNBLElBQUksSUFBNkIsRUFBRTtFQUMvQkMsTUFBTSxDQUFDQyxPQUFPLEdBQUdSLGFBQWE7QUFDbEM7O0FBRUE7QUFDQVMsTUFBTSxDQUFDQyxrQkFBa0IsQ0FBQyxZQUFXO0VBQ2pDVixhQUFhLENBQUNNLElBQUksQ0FBQyxDQUFDO0FBQ3hCLENBQUMsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9hc3NldHMvZGVtbzEvanMvY3VzdG9tL2xhbmRpbmcuanM/MzMwZSJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcclxuXHJcbi8vIENsYXNzIGRlZmluaXRpb25cclxudmFyIEtUTGFuZGluZ1BhZ2UgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAvLyBQcml2YXRlIG1ldGhvZHNcclxuICAgIHZhciBpbml0VHlwZWQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgdHlwZWQgPSBuZXcgVHlwZWQoXCIja3RfbGFuZGluZ19oZXJvX3RleHRcIiwge1xyXG4gICAgICAgICAgICBzdHJpbmdzOiBbXCJUaGUgQmVzdCBUaGVtZSBFdmVyXCIsIFwiVGhlIE1vc3QgVHJ1c3RlZCBUaGVtZVwiLCBcIiMxIFNlbGxpbmcgVGhlbWVcIl0sXHJcbiAgICAgICAgICAgIHR5cGVTcGVlZDogNTBcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQdWJsaWMgbWV0aG9kc1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIC8vaW5pdFR5cGVkKCk7XHJcbiAgICAgICAgfSAgIFxyXG4gICAgfVxyXG59KCk7XHJcblxyXG4vLyBXZWJwYWNrIHN1cHBvcnRcclxuaWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICBtb2R1bGUuZXhwb3J0cyA9IEtUTGFuZGluZ1BhZ2U7XHJcbn1cclxuXHJcbi8vIE9uIGRvY3VtZW50IHJlYWR5XHJcbktUVXRpbC5vbkRPTUNvbnRlbnRMb2FkZWQoZnVuY3Rpb24oKSB7XHJcbiAgICBLVExhbmRpbmdQYWdlLmluaXQoKTtcclxufSk7XHJcbiJdLCJuYW1lcyI6WyJLVExhbmRpbmdQYWdlIiwiaW5pdFR5cGVkIiwidHlwZWQiLCJUeXBlZCIsInN0cmluZ3MiLCJ0eXBlU3BlZWQiLCJpbml0IiwibW9kdWxlIiwiZXhwb3J0cyIsIktUVXRpbCIsIm9uRE9NQ29udGVudExvYWRlZCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./resources/assets/demo1/js/custom/landing.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./resources/assets/demo1/js/custom/landing.js");
/******/ 	
/******/ })()
;