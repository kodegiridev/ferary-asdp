<?php

namespace App\Http\Controllers\ERCM\Port;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterRegion\Region;
use App\Models\MasterRegion\Branch;
use App\Models\Port\Port;
use App\Models\ERCM\Ercm;
use App\Models\ERCM\ErcmPeriod;
use App\Models\ERCM\ErcmSystem;
use App\Models\ERCM\ErcmGroup;
use App\Models\ERCM\ErcmSubGroup;
use App\Models\ERCM\ErcmComponent;
use App\Enums\ERCMStatusEnum;
use App\Enums\ERCMPeriodStatusEnum;
use App\Http\Controllers\ERCM\Fleet\FleetERCMController;
use Illuminate\Support\Facades\DB;

use App\Models\Port\ERCM\MstErcmPelabuhanSubGroup;
use App\Models\Port\ERCM\MstErcmPelabuhanSubGroupConditionCriteria;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameter;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameterApprovalParam;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameterAreaParam;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameterNumberParam;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameterRangeParam;
use App\Models\Port\ERCM\MstErcmPelabuhanSystem;
use App\Models\User;

class PortERCMController extends Controller
{
    private $path = 'ercm';
    private $filePath = 'ercm/port';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatable($request);
        }
        // $ports = Ercm::query()->with(['port' => ['cabang' => ['regional'], 'pic_user']])->get();
        // dd($ports);

        // get list kapal
        $ports = Port::select("id", "nama")->get();

        // get select filter
        $regions = Region::select("id", "nama")->get();
        $branchs = Branch::select("id", "nama")->get();
        $statuses = ERCMStatusEnum::toArray();

        // get query string filter
        $urlParams = $request->query();

        return view('pages.e-rcm.port.index', compact('ports', 'regions', 'branchs', 'statuses', 'urlParams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $request->validate([
                'port_id' => 'required|exists:App\Models\Port\Port,id|unique:App\Models\ERCM\Ercm,port_id'
            ]);

            Ercm::create(['port_id' => $request->port_id, 'status' => ERCMStatusEnum::ON_PROGRESS, 'updated_by' => auth()->user()->id]);
            return redirect()->route('e-rcm.port.index')->with(['success' => 'Berhasil Tambah ERCM port!']);
        } catch (\Throwable $th) {
            return redirect()->route('e-rcm.port.index')->with(['failed' => $th->getMessage()]);
        }
    }

    /**
     * Store a newly created resource ercm port period.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function upsertPortPeriod(Request $request)
    {

        DB::beginTransaction();

        try {
            $request->validate([
                'ercm_id' => 'required|exists:App\Models\ERCM\Ercm,id',
                'contractor' => 'required|string',
                'effectiveness' => 'required|numeric|min:1|max:100',
                'cost' => 'required',
            ]);

            if ($request->hasFile('effectiveness_doc')) {
                $effectivenessDoc = $this->uploadFileName($request->file('effectiveness_doc'), $this->filePath.'/effectiveness-doc');
            }

            if ($request->hasFile('cost_doc')) {
                $costDoc = $this->uploadFileName($request->file('cost_doc'), $this->filePath.'/cost-doc');
            }

            $message = '';

            if ($request->post_type === 'update') {

                $portPeriod = ErcmPeriod::find($request->port_period_id);

                if (isset($effectivenessDoc) && $effectivenessDoc && $portPeriod->effectiveness_doc) {
                    $this->removeFile($portPeriod->effectiveness_doc, $this->filePath.'/effectiveness-doc');
                }

                if (isset($costDoc) && $costDoc && $portPeriod->cost_doc) {
                    $this->removeFile($portPeriod->cost_doc, $this->filePath.'/cost-doc');
                }

                $effectivenessDoc ??= $portPeriod->effectiveness_doc;
                $costDoc ??= $portPeriod->cost_doc;

                $portPeriod->update([
                    'contractor' => $request->contractor,
                    'effectiveness' => $request->effectiveness,
                    'effectiveness_doc' => $effectivenessDoc,
                    'cost' => (int) str_replace(['Rp. ', ','], '', $request->cost),
                    'cost_doc' => $costDoc,
                    'period_status' => ERCMPeriodStatusEnum::ON_PROGRESS,
                    'updated_by' => auth()->user()->id
                ]);
                $message = 'Berhasil Update ERCM Port Period!';
            } else {

                $ercmPort = Ercm::with('port:id')->find($request->ercm_id);
                $portSystem = MstErcmPelabuhanSystem::with(['mst_ercm_pelabuhan_groups' => ['mst_ercm_pelabuhan_subgroups' => ['mst_ercm_pelabuhan_components']]])->where('pelabuhan_id', $ercmPort->port->id)->get();
                // dd($ercmPort);
                // $portSystem = MstErcmPelabuhanSystem::with(['mst_ercm_pelabuhan_groups' => ['mst_ercm_pelabuhan_subgroups' => ['mst_ercm_pelabuhan_components']]])->where('pelabuhan_id', $ercmPort->port->id)->get()->toJson();
                // dd(json_decode($portSystem, true));

                $effectivenessDoc ??= null;
                $costDoc ??= null;

                $portPeriod = ErcmPeriod::create([
                    'ercm_id' => $request->ercm_id,
                    'period' => date("Y/m/d"),
                    'contractor' => $request->contractor,
                    'effectiveness' => $request->effectiveness,
                    'effectiveness_doc' => $effectivenessDoc,
                    'cost' => (int) str_replace(['Rp. ', ','], '', $request->cost),
                    'cost_doc' => $costDoc,
                    'period_status' => ERCMPeriodStatusEnum::NO_PROGRESS,
                    'created_by' => auth()->user()->id
                ]);

                foreach ($portSystem as $index => $value) {
                    $ercmSystem = ErcmSystem::create([
                        'periode_ercm_id' => $portPeriod->id,
                        'nama' => $value->name,
                        'value' => $value->value,
                        'status' => 1,
                        'created_by' => $value->created_by,
                        'updated_by' => $value->updated_by
                    ]);

                    foreach ($value->mst_ercm_pelabuhan_groups as $indexGroup => $groupValue) {
                        $ercmGroup = ErcmGroup::create([
                            'ercm_system_id' => $ercmSystem->id,
                            'nama' => $groupValue->name,
                            'value' => $groupValue->value,
                            'attachment' => $groupValue->attachment,
                            'status' => 1,
                            'created_by' => $groupValue->created_by,
                            'updated_by' => $groupValue->updated_by
                        ]);

                        foreach ($groupValue->mst_ercm_pelabuhan_subgroups as $indexSubGroup => $subGroupValue) {
                            $ercmSubGroup = ErcmSubGroup::create([
                                'ercm_group_id' => $ercmGroup->id,
                                'nama' => $subGroupValue->name,
                                'status' => 1,
                                'created_by' => $subGroupValue->created_by,
                                'updated_by' => $subGroupValue->updated_by
                            ]);

                            $components = [];
                            foreach ($subGroupValue->mst_ercm_pelabuhan_components as $indexComponent => $componentValue) {
                                $components[] = [
                                    'ercm_subgroup_id' => $ercmSubGroup->id,
                                    'component3d_id' => $componentValue->component3d_id,
                                    'component' => $componentValue->component,
                                    'color' => $componentValue->color,
                                    'created_by' => $componentValue->created_by,
                                    'updated_by' => $componentValue->updated_by
                                ];
                            }

                            ErcmComponent::insert($components);
                        }
                    }
                }

                $message = 'Berhasil Tambah ERCM Port Period!';
            }

            DB::commit();
            return redirect()->back()->with(['success' => $message]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => $th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $port = Ercm::find($request->id);
            if (!$port) {
                abort(404, 'Not found data');
            }

            $port->delete();

            $response = [
                'status' => 'success',
                'message' => 'Berhasil Hapus ERCM Port!'
            ];
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ], 404);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from ercm port period.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroyPortPeriod(Request $request)
    {
        try {
            $portPeriod = ErcmPeriod::find($request->id);
            if (!$portPeriod) {
                abort(404, 'Not found data');
            }

            if ($portPeriod->effectiveness_doc) {
                $this->removeFile($portPeriod->effectiveness_doc, $this->filePath.'/effectiveness-doc');
            }

            if ($portPeriod->cost_doc) {
                $this->removeFile($portPeriod->cost_doc, $this->filePath.'/cost-doc');
            }

            $portPeriod->delete();

            $response = [
                'status' => 'success',
                'message' => 'Berhasil Hapus ERCM Fleet Period!'
            ];
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ], 404);
        }

        return response()->json($response);
    }

    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $tableType = $request->get('tableType');

            $portPeriods = ErcmPeriod::where('ercm_id', $id);

            if($tableType == 'ongoing'){
                $onGoingItems = $portPeriods->where('period_status', '!=', ERCMPeriodStatusEnum::COMPLETION)->get();
                return $this->datatableOnGoing($onGoingItems);
            }else{
                $closeItems = $portPeriods->where('period_status', ERCMPeriodStatusEnum::COMPLETION)->get();
                return $this->datatableRecord($closeItems);
            }
        }
        $data = Ercm::with('port')->find($id);
        $user = auth()->user();

        return view('pages.e-rcm.port.detail', [
            'port' => $data->port,
            'approvers' => User::asApprover($user->id)->orderBy('name')->get(),
            'verificators' => User::asVerifikator($user->id)->orderBy('name')->get(),
        ]);
    }

    /**
     * Store a label in storage.
     *
     * @param MappingData $data
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function label(Request $request, MappingData $data)
    {

    }

    public function summaryIndex(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablePostponeItems();
        }

        return view('pages.e-rcm.port.executive-summary.index');
    }

    public function systemParameterIndex($id, Request $request)
    {
        $ercmPeriod     = ErcmPeriod::with('ercm')->find($id);
        $ercmSystem     = ErcmSystem::with('ercm_groups')->where('periode_ercm_id', $id)->get();
        if ($request->ajax()) {
            $tableType = $request->get('tableType');

            if($tableType == 'process'){
                return $this->datatableProcess($ercmPeriod->ercm->port_id);
            }else if($tableType == 'repair'){
                return $this->datatableRepair();
            }else if($tableType == 'tasklist'){
                return $this->datatableTasklist();
            }else{
                return $this->datatablePostpone();
            }
        }

        return view('pages.e-rcm.port.system-parameter.index');
    }

    public function detailSystemParameterIndex(Request $request){

        if ($request->ajax()) {
            return $this->datatableDetailSystemParameter($request->id);
        }

        $ercmPeriod = ErcmPeriod::with(['ercm:id,port_id' => ['port:nama,id']])->find($request->id);
        $meta = [
            'ercm_id' => $ercmPeriod->ercm_id,
            'port_name' => $ercmPeriod->ercm->port->nama,
            'periode_ercm_id' => $ercmPeriod->id,
            'periode' => $ercmPeriod->period,
            'system_paramater_name' => $ercmPeriod->contractor
        ];

        return view('pages.e-rcm.port.detail-system-parameter.index', compact('meta'));
    }

    public function subDetailSystemParameterIndex(Request $request){
        if ($request->ajax()) {
            return $this->datatableSubDetailSystemParameter($request->id);
        }

        $ercmSystem = ErcmSystem::with(['ercm_period:id,ercm_id,period' => ['ercm:id,port_id' => ['port:nama,id']]])->find($request->id);
        $meta = [
            'ercm_id' => $ercmSystem->ercm_period->ercm_id,
            'port_name' => $ercmSystem->ercm_period->ercm->port->nama,
            'periode_ercm_id' => $ercmSystem->periode_ercm_id,
            'periode' => $ercmSystem->ercm_period->period,
            'system_paramater_name' => $ercmSystem->nama
        ];

        return view('pages.e-rcm.port.sub-detail-system-parameter.index', compact('meta'));
    }

    public function submitApproval(Request $request)
    {
        $request->merge([
            'jenis_modul' => 'ERCM Port'
        ]);

        return (new FleetERCMController)->submitApproval($request);
    }

    /**
     * Display data in datatables
     *
     * @return \Yajra\DataTables\DataTables
     */
    private function datatable($request)
    {
        $ports = Ercm::query()->whereNotNull('port_id')->with(['port' => ['cabang' => ['regional'], 'pic_user']]);

        if ($request->has('status_id')) {
            $ports->where('status','=', $request->status_id);
        }

        if ($request->has('port_name')) {
            $ports->whereHas('port', function ($query) use ($request){
                $query->where('nama', 'LIKE', "%$request->port_name%");
            });
        }

        if ($request->has('region_id') || $request->has('branch_id')) {
            $ports->whereHas('port', function ($query) use ($request){
                if ($request->has('branch_id')) {
                    $query->where('cabang_id', $request->branch_id);
                }

                if ($request->has('region_id')) {
                    $query->whereHas('cabang', function ($cabang) use ($request){
                        $cabang->where('regional_id', $request->region_id);
                    });
                }
            });
        }

        $items = $ports->get();

        return datatables()
            ->of($items)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="'. route('e-rcm.port.show', $row->id) .'" class="btn btn-sm btn-warning text-black btn-detail">
                        Detail
                    </a>

                    <a href="'. route('e-rcm.port.summary', $row->id) .'" class="btn btn-sm btn-success">
                        Executive
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->addColumn('status', function ($row) {
                return $row->status->getLabelText();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function datatableOnGoing($data)
    {
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('effectiveness', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">'.$row->effectiveness_percent.'</h1>
                        '.( $row->file_url['effectiveness'] ? '<a href="'.$row->file_url['effectiveness'].'" class="btn bg-body p-0">Lihat</a>' : '').'
                    </div>';
                return $div;
            })
            ->editColumn('cost', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">'.$row->cost_format.'</h1>
                        '.( $row->file_url['cost'] ? '<a href="'.$row->file_url['cost'].'" class="btn bg-body p-0">Lihat</a>' : '').'
                    </div>';
                return $div;
            })
            ->editColumn('status', function ($row) {
                return $row->period_status->getLabelText();
            })
            ->addColumn('action', function ($row) {
                $row->file_url = $this->filePath;
                $button = '
                    <a href="'. route('e-rcm.port.system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="'. route('e-rcm.port.detail-system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-success edit-data-period"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#edit_periode_modal"
                        data-contractor="'.$row->contractor.'"
                        data-effectiveness="'.$row->effectiveness.'"
                        data-cost="'.$row->cost.'"
                        data-effectiveness_doc="'.$row->file_url['effectiveness'].'"
                        data-cost_doc="'.$row->file_url['cost'].'"
                    >
                        <i class="fa fa-pencil"></i>
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->addColumn('checkbox', function ($row) {
                $div = '<input class="form-check-input" type="radio" value="'.$row->id.'" id="flexRadioChecked" name="flexRadioChecked" />';
                // $div = '<input class="form-check-input" type="radio" value="" id="flexRadioChecked" checked="checked" />';
                return $div;
            })
            ->rawColumns(['action', 'effectiveness', 'cost', 'checkbox'])
            ->make(true);
    }

    private function datatableRecord($data)
    {
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('effectiveness', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">'.$row->effectiveness_percent.'</h1>
                        '.( $row->file_url['effectiveness'] ? '<a href="'.$row->file_url['effectiveness'].'" class="btn bg-body p-0">Lihat</a>' : '').'
                    </div>';
                return $div;
            })
            ->editColumn('cost', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">'.$row->cost_format.'</h1>
                        '.( $row->file_url['cost'] ? '<a href="'.$row->file_url['cost'].'" class="btn bg-body p-0">Lihat</a>' : '').'
                    </div>';
                return $div;
            })
            ->editColumn('status', function ($row) {
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px; color:green">'.$row->period_status->getLabelText().'</h1>
                    </div>';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="'. route('e-rcm.port.show', $row->id) .'" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                    >
                        <i class="fa fa-pencil"></i>
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['action', 'effectiveness', 'cost', 'status'])
            ->make(true);
    }

    private function datatablePostponeItems(){
        $sampleData = [
            [
                'no' => 1,
                'system' => 'Hull',
                'component' => 'Closet',
                'tasks' => [
                    [
                        'task' => 'Install Recover',
                        'dueDate' => '10/10/23',
                        'status' => 'Open'
                    ],
                    [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ],
                     [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ]
                ]
            ],
            [
                'no' => 2,
                'system' => 'Hull 2',
                'component' => 'Closet',
                'tasks' => [
                    [
                        'task' => 'Install Recover',
                        'dueDate' => '10/10/23',
                        'status' => 'Open'
                    ],
                    [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ]
                ]
            ]
        ];

        $data = collect(array_map(function($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableProcess($id){
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'parameters' => [
                    [
                        'name' => 'Thickness (number)',
                        'standard' => [
                            [
                                'STD' => 50
                            ],
                            [
                                'MIN' => 30
                            ]
                        ],
                    ],
                    [
                        'name' => 'Deformation (Area)',
                        'standard' => [
                            [
                                'STD 1' => 40
                            ],
                            [
                                'STD 2' => 60
                            ]
                        ]
                    ],
                    [
                        'name' => 'Pitted (Ranges)',
                        'standard' => [
                            [
                                'name' => 'Dev > 1 mm'
                            ],
                            [
                                'name' => 'Dev === 1 mm'
                            ],
                            [
                                'name' => 'Dev < 1 mm'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Final Thickness (Approval)',
                        'standard' => 'good'
                    ]
                ],
                'score' => 100,
                'status' => 'Accepted',
                 'approval' => ''
            ],
            [
                'id' => 2,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'parameters' => [
                    [
                        'name' => 'Thickness (number)',
                        'standard' => [
                            [
                                'STD' => 50
                            ],
                            [
                                'MIN' => 30
                            ]
                        ],
                    ],
                    [
                        'name' => 'Deformation (Area)',
                        'standard' => [
                            [
                                'STD 1' => 40
                            ],
                            [
                                'STD 2' => 60
                            ]
                        ]
                    ],
                    [
                        'name' => 'Final Thickness (Approval)',
                        'standard' => 'good'
                    ]
                ],
                'score' => 100,
                'status' => 'Monitoring',
                'approval' => ''
            ],

        ];

        $mstErcmPelabuhanSystem = MstErcmPelabuhanSystem::where('pelabuhan_id', $id)->get();
        $realData = [];
        foreach ($mstErcmPelabuhanSystem as $system) {
            $MstErcmPelabuhanSubGroup  = MstErcmPelabuhanSubGroup::with('mst_ercm_pelabuhan_components', 'mst_ercm_pelabuhan_system', 'mst_ercm_pelabuhan_subgroup_standard_parameter', 'mst_ercm_pelabuhan_subgroup_condition_criteria', 'mst_ercm_pelabuhan_group')->where('mst_ercm_pelabuhan_system_id', $system->id)->get();

            if (!empty($MstErcmPelabuhanSubGroup)) {
                $stdParam = new MstErcmPelabuhanSubGroupStandardParameter;
    
                $orderTypes = [
                    1 => 'subgroup_standard_parameter_areas',
                    2 => 'subgroup_standard_parameter_number',
                    3 => 'subgroup_standard_parameter_ranges',
                    4 => 'subgroup_standard_parameter_approvals',
                ];
                foreach ($MstErcmPelabuhanSubGroup as $subGroup) {
                    // $groupName  = $subGroup->mst_ercm_vessel_group->name;
                    $groupName  = $subGroup->name;
                    $initial    = $stdParam::$assestmentTypes['initial'];
    
                    $parameters = $stdParam::where('mst_ercm_pelabuhan_subgroup_id', $subGroup->id)->where('assestment_type', $initial)
                        ->with([
                            'subgroup_standard_parameter_number',
                            'subgroup_standard_parameter_areas',
                            'subgroup_standard_parameter_ranges',
                            'subgroup_standard_parameter_approvals'
                        ])
                        ->orderBy('id')
                        ->get();
                    $assestmentParams = $parameters->where('assestment_type', $initial)->toArray();
                    $data = [];
    
                    $parameters_data = []; 
                    foreach ($assestmentParams as $param) {
                        $relation = $orderTypes[$param['order_type']];
                        $param['order_parameters'] = $param[$relation];
                        $param['range_parameters'] = [];
    
                        if ($param['order_type'] == 4) {
                            // APPROVAL
                            $parameters_data[] = [
                                "name"      => $param['name']." (Approval)",
                                "standard"  => $param['order_parameters'][0]['param']
                            ];
                        } elseif ($param['order_type'] == 1) {
                            // AREA
                            $standards = $param['order_parameters'];
    
                            $standard = [];
                            foreach ($standards as $i => $v) {
                                $no = $i + 1;
                                $standard[] = [
                                    "STD ".$no => $v['std']
                                ];
                            }
                            $parameters_data[] = [
                                "name"      => $param['name']." (Area)",
                                "standard"  => $standard
                            ];
                        } elseif ($param['order_type'] == 2) {
                            // NUMBER
                            $standards = $param['order_parameters'];
    
                            $standard = [];
                            $standard[] = ["STD" => $standards['std']];
                            $standard[] = ["MIN" => $standards['min']];
                            $parameters_data[] = [
                                "name"      => $param['name']." (number)",
                                "standard"  => $standard
                            ];
                            // dd($param);
                        } elseif ($param['order_type'] == 3) {
                            $standards = $param['order_parameters'];
    
                            $standard = [];
                            foreach ($standards as $i => $v) {
                                $no = $i + 1;
                                $standard[] = [
                                    "name" => $v['criteria']
                                ];
                            }
                            $parameters_data[] = [
                                "name"      => $param['name']." (Area)",
                                "standard"  => $standard
                            ];
                        }
                    }
                    $components = $subGroup->mst_ercm_pelabuhan_components;
                    foreach ($components as $component) {
                        $realData[] = [
                            "id"            => $component->id,
                            "group"         => $groupName,
                            "subGroup"      => $subGroup->name,
                            "component"     => $component->component,
                            "parameters"    => $parameters_data,
                            "score"         => 0,
                            "status"        => "",
                            "approval"      => ""
                        ];
                    }
                }
            }
        }

        $data = collect(array_map(function($item) {
            return (object)$item;
        }, $realData));

        return datatables()
            ->of($data)
            ->addColumn('evidence', function ($row) {
                $button = '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                    >
                        Upload
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['evidence'])
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableRepair(){
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'score' => 20,
                'status' => 'Repair',
                'classification' => 'Safety Meter',
                'approval' => 'Approved',
                'approvalNote' => ''
            ]

        ];

        $data = collect(array_map(function($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableTasklist(){
        $sampleData = [
            [
                'id' => 1,
                'taskList' => 'Repairing K1',
            ]

        ];

        $data = collect(array_map(function($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $button = '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function datatablePostpone(){
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'classification' => 'Safety Meter',
                'approval' => 'Approved',
            ]

        ];

        $data = collect(array_map(function($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableDetailSystemParameter($periode_ercm_id){

        $data = ErcmSystem::where('periode_ercm_id', $periode_ercm_id)->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('checkbox', function ($row) {
                $div = '<input class="form-check-input" type="radio" value="' . $row->parameter . '" id="flexRadioChecked" name="flexRadioChecked" />';
                // $div = '<input class="form-check-input" type="radio" value="' . $row->parameter . '" id="flexRadioChecked-' . $row->parameter . '" />';
                return $div;
            })
            ->editColumn('parameter', function ($row) {
                return $row->nama;
            })
            ->addColumn('status', function ($row) {
                $div = '
                     <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#status_modal"
                    >
                        <i class="fa fa-info"></i>
                    </button>
                ';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="'. route('e-rcm.port.system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="'. route('e-rcm.port.sub-detail-system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>
                ';

                return $button;
            })
            ->rawColumns(['action','checkbox', 'status'])
            ->make(true);
    }

    private function datatableSubDetailSystemParameter($ercm_system_id){
        $data = ErcmGroup::with('ercm_subgroups')->where('ercm_system_id', $ercm_system_id)->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('group', function ($row) {
                return $row->nama;
            })
            ->addColumn('status', function ($row) {
                $div = '
                     <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#status_modal"
                    >
                        <i class="fa fa-info"></i>
                    </button>
                ';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="'. route('e-rcm.port.system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="'. route('e-rcm.port.sub-detail-system-parameter', $row->id) .'" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>
                ';

                return $button;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }
}
