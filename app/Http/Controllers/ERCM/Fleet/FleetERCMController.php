<?php

namespace App\Http\Controllers\ERCM\Fleet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterRegion\Region;
use App\Models\MasterRegion\Branch;
use App\Models\ERCM\Ercm;
use App\Models\ERCM\ErcmPeriod;
use App\Models\ERCM\ErcmSystem;
use App\Models\ERCM\ErcmGroup;
use App\Models\ERCM\ErcmSubGroup;
use App\Models\ERCM\ErcmComponent;
use App\Models\ERCM\ErcmComponentConditionCriteria;
use App\Models\ERCM\ErcmComponentStandardParameter;
use App\Models\Fleet\Fleet;
use App\Enums\ERCMStatusEnum;
use App\Enums\ERCMPeriodStatusEnum;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\ERCM\ErcmApproval;
use Illuminate\Support\Facades\DB;

use App\Models\Fleet\ERCM\MstErcmVesselSubGroup;
use App\Models\Fleet\ERCM\MstErcmVesselSubGroupConditionCriteria;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameter;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterApprovalParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterAreaParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterNumberParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterRangeParam;
use App\Models\Fleet\ERCM\MstErcmVesselSystem;
use App\Models\User;
use stdClass;

class FleetERCMController extends Controller
{
    private $path = 'ercm';
    private $filePath = 'ercm/fleet';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatable($request);
        }

        // get list kapal
        $fleets = Fleet::select("id", "nama")->get();

        // get select filter
        $regions = Region::select("id", "nama")->get();
        $branchs = Branch::select("id", "nama")->get();
        $statuses = ERCMStatusEnum::toArray();

        // get query string filter
        $urlParams = $request->query();

        return view('pages.e-rcm.fleet.index', compact('fleets', 'regions', 'branchs', 'statuses', 'urlParams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $request->validate([
                'vessel_id' => 'required|exists:App\Models\Fleet\Fleet,id|unique:App\Models\ERCM\Ercm,vessel_id'
            ]);

            Ercm::create(['vessel_id' => $request->vessel_id, 'status' => ERCMStatusEnum::ON_PROGRESS, 'updated_by' => auth()->user()->id]);
            return redirect()->route('e-rcm.fleet.index')->with(['success' => 'Berhasil Tambah ERCM Fleet!']);
        } catch (\Throwable $th) {
            return redirect()->route('e-rcm.fleet.index')->with(['failed' => $th->getMessage()]);
        }
    }

    /**
     * Store a newly created resource ercm vessel period.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function upsertFleetPeriod(Request $request)
    {

        DB::beginTransaction();

        try {
            $request->validate([
                // 'ercm_vessel_id' => 'required|exists:App\Models\ERCM\Ercm,id',
                'ercm_id' => 'required|exists:App\Models\ERCM\Ercm,id',
                'contractor' => 'required|string',
                'effectiveness' => 'required|numeric|min:1|max:100',
                'cost' => 'required',
            ]);

            if ($request->hasFile('effectiveness_doc')) {
                $effectivenessDoc = $this->uploadFileName($request->file('effectiveness_doc'), $this->filePath . '/effectiveness-doc');
            }

            if ($request->hasFile('cost_doc')) {
                $costDoc = $this->uploadFileName($request->file('cost_doc'), $this->filePath . '/cost-doc');
            }

            $message = '';

            if ($request->post_type === 'update') {

                $vesselPeriod = ErcmPeriod::find($request->vessel_period_id);

                if (isset($effectivenessDoc) && $effectivenessDoc && $vesselPeriod->effectiveness_doc) {
                    $this->removeFile($vesselPeriod->effectiveness_doc, $this->filePath . '/effectiveness-doc');
                }

                if (isset($costDoc) && $costDoc && $vesselPeriod->cost_doc) {
                    $this->removeFile($vesselPeriod->cost_doc, $this->filePath . '/cost-doc');
                }

                $effectivenessDoc ??= $vesselPeriod->effectiveness_doc;
                $costDoc ??= $vesselPeriod->cost_doc;

                $vesselPeriod->update([
                    'contractor' => $request->contractor,
                    'effectiveness' => $request->effectiveness,
                    'effectiveness_doc' => $effectivenessDoc,
                    'cost' => (int) str_replace(['Rp. ', ','], '', $request->cost),
                    'cost_doc' => $costDoc,
                    'period_status' => ERCMPeriodStatusEnum::ON_PROGRESS,
                    'updated_by' => auth()->user()->id
                ]);
                $message = 'Berhasil Update ERCM Fleet Period!';
            } else {

                $ercmVessel = Ercm::with('fleet:id')->find($request->ercm_id);
                $vesselSystem = MstErcmVesselSystem::with(['mst_ercm_vessel_groups' => ['mst_ercm_vessel_subgroups' => ['mst_ercm_vessel_components', 'mst_ercm_vessel_subgroup_standard_parameter', 'mst_ercm_vessel_subgroup_condition_criteria']]])->where('vessel_id', $ercmVessel->fleet->id)->get();
                // $vesselSystem = MstErcmVesselSystem::with(['mst_ercm_vessel_groups' => ['mst_ercm_vessel_subgroups' => ['mst_ercm_vessel_components']]])->where('vessel_id', $ercmVessel->fleet->id)->get()->toJson();
                // dd(json_decode($vesselSystem, true));

                $effectivenessDoc ??= null;
                $costDoc ??= null;

                $vesselPeriod = ErcmPeriod::create([
                    'ercm_id' => $request->ercm_id,
                    'period' => date("Y/m/d"),
                    'contractor' => $request->contractor,
                    'effectiveness' => $request->effectiveness,
                    'effectiveness_doc' => $effectivenessDoc,
                    'cost' => (int) str_replace(['Rp. ', ','], '', $request->cost),
                    'cost_doc' => $costDoc,
                    'period_status' => ERCMPeriodStatusEnum::NO_PROGRESS,
                    'created_by' => auth()->user()->id
                ]);
                foreach ($vesselSystem as $index => $value) {
                    $ercmSystem = ErcmSystem::create([
                        'periode_ercm_id' => $vesselPeriod->id,
                        'name' => $value->name,
                        'value' => $value->value,
                        'status' => 1,
                        'created_by' => $value->created_by,
                        'updated_by' => $value->updated_by
                    ]);

                    
                    foreach ($value->mst_ercm_vessel_groups as $indexGroup => $groupValue) {
                        $ercmGroup = ErcmGroup::create([
                            'ercm_systems_id' => $ercmSystem->id,
                            'name' => $groupValue->name,
                            'value' => $groupValue->value,
                            'attachment' => $groupValue->attachment,
                            'status' => 1,
                            'created_by' => $groupValue->created_by,
                            'updated_by' => $groupValue->updated_by
                        ]);

                        foreach ($groupValue->mst_ercm_vessel_subgroups as $indexSubGroup => $subGroupValue) {
                            $ercmSubGroup = ErcmSubGroup::create([
                                'ercm_groups_id' => $ercmGroup->id,
                                'name' => $subGroupValue->name,
                                'status' => 1,
                                'created_by' => $subGroupValue->created_by,
                                'updated_by' => $subGroupValue->updated_by
                            ]);

                            $conditionCriterias = $subGroupValue->mst_ercm_vessel_subgroup_condition_criteria;
                            $conditionCriteriasData = [
                                "ercm_components_id"    => 0,
                                "up1"                   => $conditionCriterias->up1,
                                "down1"                 => $conditionCriterias->down1,
                                "status1"               => $conditionCriterias->status1,
                                "up2"                   => $conditionCriterias->up2,
                                "down2"                 => $conditionCriterias->down2,
                                "status2"               => $conditionCriterias->status2,
                                "up3"                   => $conditionCriterias->up3,
                                "down3"                 => $conditionCriterias->down3,
                                "status3"               => $conditionCriterias->status3,
                            ];


                            $stdParam = new MstErcmVesselSubgroupStandardParameter;
                            $orderTypes = [
                                1 => 'subgroup_standard_parameter_areas',
                                2 => 'subgroup_standard_parameter_number',
                                3 => 'subgroup_standard_parameter_ranges',
                                4 => 'subgroup_standard_parameter_approvals',
                            ];
                            $tableTypes = [
                                1 => 'ercm_component_standard_parameter_area_params',
                                2 => 'ercm_component_standard_parameter_number_params',
                                3 => 'ercm_component_standard_parameter_range_params',
                                4 => 'ercm_component_standard_parameter_approval_params',
                            ];
                            $stage = $stdParam::$assestmentTypes;

                            $parameters = $stdParam::where('mst_ercm_vessel_subgroup_id', $subGroupValue->id)
                                ->with([
                                    'subgroup_standard_parameter_number',
                                    'subgroup_standard_parameter_areas',
                                    'subgroup_standard_parameter_ranges',
                                    'subgroup_standard_parameter_approvals'
                                ])
                                ->orderBy('id')
                                ->get();
                            $parametersData = [];
                            $standardsParametersData = [];
                            foreach ($parameters as $param) {
                                $param_id = $param->id;
                                $relation = $orderTypes[$param->order_type];
                                $standard_parameter = $param->$relation;
                                $parameter = [
                                    "ercm_components_id"    => 0,
                                    "name"                  => $param->name,
                                    "value"                 => $param->value,
                                    "assestment_type"       => $param->assestment_type,
                                    "order_type"            => $param->order_type,
                                    "type_stage"            => $param->type_stage,
                                    "parameter_type"        => $param->parameter_type
                                ];

                                $std_parameter = [];
                                if ($param->order_type == 2) {
                                    $sp = $standard_parameter->toArray();
                                    unset($sp['id']);
                                    unset($sp['mst_ercm_vessel_subgroup_standard_parameter_id']);
                                    unset($sp['created_at']);
                                    unset($sp['updated_at']);
                                    $sp['ercm_component_standard_parameters_id'] = 0;

                                    $std_parameter[] = $sp;
                                } else {
                                    foreach ($standard_parameter as $sParameter) {
                                        $sp = $sParameter->toArray();
                                        unset($sp['id']);
                                        unset($sp['mst_ercm_vessel_subgroup_standard_parameter_id']);
                                        unset($sp['created_at']);
                                        unset($sp['updated_at']);
                                        $sp['ercm_component_standard_parameters_id'] = 0;

                                        $std_parameter[] = $sp;
                                    }
                                }
                                $parametersData[$param_id] = $parameter;
                                $standardsParametersData[$param_id] = $std_parameter;
                            }

                            // $components = [];
                            foreach ($subGroupValue->mst_ercm_vessel_components as $indexComponent => $componentValue) {
                                $component = ErcmComponent::create([
                                    'ercm_subgroups_id' => $ercmSubGroup->id,
                                    'component3d_id' => $componentValue->component3d_id,
                                    'component' => $componentValue->component,
                                    'color' => $componentValue->color,
                                    'created_by' => $componentValue->created_by,
                                    'updated_by' => $componentValue->updated_by
                                ]);

                                // Save ERCM Condition Criteria
                                $conditionCriteria = $conditionCriteriasData;
                                $conditionCriteria['ercm_components_id'] = $component->id;
                                $condition_criteria = ErcmComponentConditionCriteria::create($conditionCriteria);
                                

                                foreach ($parametersData as $param_id => $params) {
                                    $standard_parameter = $standardsParametersData[$param_id];
                                    $standard_parameter_table = $tableTypes[$params['order_type']];
                                    
                                    $params['ercm_components_id'] = $component->id;
                                    $componentStandardParameter = ErcmComponentStandardParameter::create($params);

                                    foreach ($standard_parameter as $sp) {
                                        $sp['ercm_component_standard_parameters_id'] = $componentStandardParameter->id;
                                        $insert = DB::table($standard_parameter_table)->insert($sp);
                                    }
                                }
                                
                            }

                            // ErcmComponent::insert($components);
                        }
                    }
                    
                }

                $message = 'Berhasil Tambah ERCM Fleet Period!';
            }

            DB::commit();
            return redirect()->back()->with(['success' => $message]);
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage(), $parameter, $standard_parameter);
            return redirect()->back()->with(['failed' => $th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $vessel = Ercm::find($request->id);
            if (!$vessel) {
                abort(404, 'Not found data');
            }

            $vessel->delete();

            $response = [
                'status' => 'success',
                'message' => 'Berhasil Hapus ERCM Fleet!'
            ];
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ], 404);
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from ercm vessel period.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroyFleetPeriod(Request $request)
    {
        try {
            $vesselPeriod = ErcmPeriod::find($request->id);
            if (!$vesselPeriod) {
                abort(404, 'Not found data');
            }

            if ($vesselPeriod->effectiveness_doc) {
                $this->removeFile($vesselPeriod->effectiveness_doc, $this->filePath . '/effectiveness-doc');
            }

            if ($vesselPeriod->cost_doc) {
                $this->removeFile($vesselPeriod->cost_doc, $this->filePath . '/cost-doc');
            }

            $vesselPeriod->delete();

            $response = [
                'status' => 'success',
                'message' => 'Berhasil Hapus ERCM Fleet Period!'
            ];
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ], 404);
        }

        return response()->json($response);
    }

    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $tableType = $request->get('tableType');

            $vesselPeriods = ErcmPeriod::where('ercm_id', $id);

            if ($tableType == 'ongoing') {
                $onGoingItems = $vesselPeriods->where('period_status', '!=', ERCMPeriodStatusEnum::COMPLETION)->get();
                return $this->datatableOnGoing($onGoingItems);
            } else {
                $closeItems = $vesselPeriods->where('period_status', ERCMPeriodStatusEnum::COMPLETION)->get();
                return $this->datatableRecord($closeItems);
            }
        }
        $data = Ercm::with('fleet')->find($id);
        $user = auth()->user();
        // dd($data);

        return view('pages.e-rcm.fleet.detail', [
            'fleet' => $data->fleet,
            'approvers' => User::asApprover($user->id)->orderBy('name')->get(),
            'verificators' => User::asVerifikator($user->id)->orderBy('name')->get(),
        ]);
    }

    /**
     * Store a label in storage.
     *
     * @param MappingData $data
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function label(Request $request, MappingData $data)
    {
    }

    public function summaryIndex(Request $request, $id)
    {
        if ($request->ajax()) {
            return $this->datatablePostponeItems();
        }

        $fleet = Fleet::select("id", "nama", "regional_id")->where("id", $id)->first();
        $region = Region::select("id", "nama")->where("id", $fleet->regional_id ?? null)->first();

        $ercm = Ercm::select("id")->where("vessel_id", $id)->first();


        $ercmPeriods = ErcmPeriod::select("contractor","before_score", "after_score", "delta_score")->where("ercm_id", $ercm->id ?? null)->first();

        $dataSummary = new stdClass();
        $dataSummary->nameAsset = $fleet->nama;
        $dataSummary->region = $region->nama;
        $dataSummary->contractor = $ercmPeriods->contractor ?? "-";
        $dataSummary->before = $ercmPeriods->before_score ?? "0";
        $dataSummary->after = $ercmPeriods->after_score ?? "0";
        $dataSummary->delta = $ercmPeriods->delta_score?? "0";

        $paramsVessel = MstErcmVesselSystem::get();
        $listPeriods = ErcmPeriod::get();

        return view('pages.e-rcm.fleet.executive-summary.index', compact('dataSummary', 'paramsVessel','listPeriods'));
    }

    public function systemParameterIndex($id, Request $request)
    {
        $ercmPeriod     = ErcmPeriod::with('ercm')->find($id);

        $ercm_id = $ercmPeriod->ercm->id;
        if ($request->ajax()) {
            $tableType = $request->get('tableType');
            if ($tableType == 'process') {
                $tableId = $request->get('tableId');
                // if($ercmPeriod){
                //     $id = $ercmPeriod->ercm->vessel_id;
                // }else{
                //     $id = new Object_();
                // }
                return $this->datatableProcess($ercmPeriod->id, $tableId);
            } else if ($tableType == 'repair') {
                return $this->datatableRepair();
            } else if ($tableType == 'tasklist') {
                return $this->datatableTasklist();
            } else {
                return $this->datatablePostpone();
            }
        }


        // dd($data);
        return view('pages.e-rcm.fleet.system-parameter.index', compact('id', 'ercm_id'));
    }

    public function detailSystemParameterIndex(Request $request)
    {

        if ($request->ajax()) {
            return $this->datatableDetailSystemParameter($request->id);
        }

        $ercmPeriod = ErcmPeriod::with(['ercm:id,vessel_id' => ['fleet:nama,id']])->find($request->id);
        $meta = [
            'ercm_id' => $ercmPeriod->ercm_id,
            'vessel_name' => $ercmPeriod->ercm->fleet->nama,
            'periode_ercm_id' => $ercmPeriod->id,
            'periode' => $ercmPeriod->period,
            'system_paramater_name' => $ercmPeriod->contractor
        ];

        return view('pages.e-rcm.fleet.detail-system-parameter.index', compact('meta'));
    }

    public function subDetailSystemParameterIndex(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatableSubDetailSystemParameter($request->id);
        }

        $ercmSystem = ErcmSystem::with(['ercm_period:id,ercm_id,period' => ['ercm:id,vessel_id' => ['fleet:nama,id']]])->find($request->id);
        $meta = [
            'ercm_id' => $ercmSystem->ercm_period->ercm_id,
            'vessel_name' => $ercmSystem->ercm_period->ercm->fleet->nama,
            'periode_ercm_id' => $ercmSystem->periode_ercm_id,
            'periode' => $ercmSystem->ercm_period->period,
            'system_paramater_name' => $ercmSystem->nama
        ];

        return view('pages.e-rcm.fleet.sub-detail-system-parameter.index', compact('meta'));
    }

    public function submitApproval(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = ErcmApproval::where('ercm_period_id', $request->ercm_period_id);
            if ($request->type == 'component') {
                $data->where('ercm_component_id', $request->ercm_component_id);
            }

            $data = $data->first();
            if ($data && $data->approval) {
                if (!in_array($data->approval->status, [Approval::REJECTED, Approval::APPROVED])) {
                    return redirect()->back()->with('failed', 'Periode yang Anda submit dalam proses persetujuan!');
                }

                if ($data->approval->status == Approval::APPROVED) {
                    return redirect()->back()->with('failed', 'Periode yang Anda submit telah disetujui, pilih periode lain!');
                }
            }

            $approval = Approval::create([
                'pengaju_id'            => auth()->user()->id,
                'penyetuju_id'          => $request->penyetuju_id,
                'jenis_modul'           => $request->jenis_modul ?? 'ERCM Fleet',
                'sub_modul'             => 'Period',
                'tgl_pengajuan'         => date('Y-m-d'),
                'keterangan_pengaju'    => $request->keterangan,
                'status'                => Approval::NEW,
            ]);

            if (!empty($request->user_verifikator_id)) {
                $verifikators = [];
                foreach ($request->user_verifikator_id as $id) {
                    array_push($verifikators, [
                        'user_verifikator_id' => $id,
                        'status' => Verifikator::NEW
                    ]);
                }

                $approval->verifikators()->createMany($verifikators);
            }

            ErcmApproval::create([
                'approval_id' => $approval->id,
                'ercm_period_id' => $request->ercm_period_id
            ]);

            DB::commit();
            return redirect()->back()->with(['success' => 'Submit approval berhasil!']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => $th->getMessage()]);
        }
    }

    /**
     * Display data in datatables
     *
     * @return \Yajra\DataTables\DataTables
     */
    private function datatable($request)
    {
        $vessels = Ercm::query()->whereNotNull('vessel_id')->with(['fleet' => ['regional', 'cabang', 'lintasan', 'user', 'surveyor']]);

        if ($request->has('status_id')) {
            $vessels->where('status', '=', $request->status_id);
        }

        if ($request->has('vessel_name')) {
            $vessels->whereHas('fleet', function ($query) use ($request) {
                $query->where('nama', 'LIKE', "%$request->vessel_name%");
            });
        }

        if ($request->has('region_id')) {
            $vessels->whereHas('fleet', function ($query) use ($request) {
                $query->where('regional_id', '=', $request->region_id);
            });
        }

        if ($request->has('branch_id')) {
            $vessels->whereHas('fleet', function ($query) use ($request) {
                $query->where('cabang_id', '=', $request->branch_id);
            });
        }

        $items = $vessels->get();
        // $recordsTotal = $vessels->count();
        // $items = $vessels->offset($request->start)->limit(1)->get();

        return datatables()
            ->of($items)
            // ->with([
            //     // "draw" => 3,
            //     "recordsTotal"    => 13,
            //     // "recordsFiltered" => $count_filter,
            // ])
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="' . route('e-rcm.fleet.show', $row->id) . '" class="btn btn-sm btn-warning text-black btn-detail">
                        Detail
                    </a>

                    <a href="' . route('e-rcm.fleet.summary', $row->id) . '" class="btn btn-sm btn-success">
                        Executive
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->addColumn('status', function ($row) {
                return $row->status->getLabelText();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function datatableOnGoing($data)
    {
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('effectiveness', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">' . $row->effectiveness_percent . '</h1>
                        ' . ($row->file_url['effectiveness'] ? '<a href="' . $row->file_url['effectiveness'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                    </div>';
                return $div;
            })
            ->editColumn('cost', function ($row) {
                // $row->file_url['cost'] ?: 'javascript:void(0)'
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">' . $row->cost_format . '</h1>
                        ' . ($row->file_url['cost'] ? '<a href="' . $row->file_url['cost'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                    </div>';
                return $div;
            })
            ->editColumn('status', function ($row) {
                return $row->period_status->getLabelText();
            })
            ->addColumn('action', function ($row) {
                $row->file_url = $this->filePath;
                $button = '
                    <a href="' . route('e-rcm.fleet.system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="' . route('e-rcm.fleet.detail-system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-success edit-data-period"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#edit_periode_modal"
                        data-contractor="' . $row->contractor . '"
                        data-effectiveness="' . $row->effectiveness . '"
                        data-cost="' . $row->cost . '"
                        data-effectiveness_doc="' . $row->file_url['effectiveness'] . '"
                        data-cost_doc="' . $row->file_url['cost'] . '"
                    >
                        <i class="fa fa-pencil"></i>
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->addColumn('checkbox', function ($row) {
                $div = '<input class="form-check-input" type="radio" value="' . $row->id . '" name="flexRadioChecked" id="flexRadioChecked" />';
                // $div = '<input class="form-check-input" type="radio" value="" id="flexRadioChecked" checked="checked" />';
                return $div;
            })
            ->rawColumns(['action', 'effectiveness', 'cost', 'checkbox'])
            ->make(true);
    }

    private function datatableRecord($data)
    {
        //    $sampleData = [
        //         [
        //             'id' => 1,
        //             'lastDate' => '10/10/23',
        //             'contractor' => 'PT DRU',
        //             'before' => '70',
        //             'after' => '90',
        //             'alpha' => '20',
        //             'excecute' => '2',
        //             'accept' => '1',
        //             'reject' => '1',
        //             'postpone' => '2',
        //             'effectivenes' => '23%',
        //             'cost' => 'Rp.2000.0000',
        //             'status' => 'Completed',
        //         ],
        //     ];

        //     $data = collect(array_map(function($item) {
        //         return (object)$item;
        //     }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('effectiveness', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">' . $row->effectiveness_percent . '</h1>
                        ' . ($row->file_url['effectiveness'] ? '<a href="' . $row->file_url['effectiveness'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                    </div>';
                return $div;
            })
            ->editColumn('cost', function ($row) {
                $row->file_url = $this->filePath;
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px;">' . $row->cost_format . '</h1>
                        ' . ($row->file_url['cost'] ? '<a href="' . $row->file_url['cost'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                    </div>';
                return $div;
            })
            ->editColumn('status', function ($row) {
                $div = '
                    <div class="d-flex flex-column">
                        <h1 style="font-size:15px; color:green">' . $row->period_status->getLabelText() . '</h1>
                    </div>';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="' . route('e-rcm.fleet.show', $row->id) . '" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                    >
                        <i class="fa fa-pencil"></i>
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['action', 'effectiveness', 'cost', 'status'])
            ->make(true);
    }

    private function datatablePostponeItems()
    {
        $sampleData = [
            [
                'no' => 1,
                'system' => 'Hull',
                'component' => 'Closet',
                'tasks' => [
                    [
                        'task' => 'Install Recover',
                        'dueDate' => '10/10/23',
                        'status' => 'Open'
                    ],
                    [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ],
                    [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ]
                ]
            ],
            [
                'no' => 2,
                'system' => 'Hull 2',
                'component' => 'Closet',
                'tasks' => [
                    [
                        'task' => 'Install Recover',
                        'dueDate' => '10/10/23',
                        'status' => 'Open'
                    ],
                    [
                        'task' => 'Replace Cover',
                        'dueDate' => '12/10/23',
                        'status' => 'Open'
                    ]
                ]
            ]
        ];

        $data = collect(array_map(function ($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableProcess($id, $tableId){
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'parameters' => [
                    [
                        'name' => 'Thickness (number)',
                        'standard' => [
                            [
                                'STD' => 50
                            ],
                            [
                                'MIN' => 30
                            ]
                        ],
                    ],
                    [
                        'name' => 'Deformation (Area)',
                        'standard' => [
                            [
                                'STD 1' => 40
                            ],
                            [
                                'STD 2' => 60
                            ]
                        ]
                    ],
                    [
                        'name' => 'Pitted (Ranges)',
                        'standard' => [
                            [
                                'name' => 'Dev > 1 mm'
                            ],
                            [
                                'name' => 'Dev === 1 mm'
                            ],
                            [
                                'name' => 'Dev < 1 mm'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Final Thickness (Approval)',
                        'standard' => 'good'
                    ]
                ],
                'score' => 100,
                'status' => 'Accepted',
                'approval' => ''
            ],
            [
                'id' => 2,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'parameters' => [
                    [
                        'name' => 'Thickness (number)',
                        'standard' => [
                            [
                                'STD' => 50
                            ],
                            [
                                'MIN' => 30
                            ]
                        ],
                    ],
                    [
                        'name' => 'Deformation (Area)',
                        'standard' => [
                            [
                                'STD 1' => 40
                            ],
                            [
                                'STD 2' => 60
                            ]
                        ]
                    ],
                    [
                        'name' => 'Final Thickness (Approval)',
                        'standard' => 'good'
                    ]
                ],
                'score' => 100,
                'status' => 'Monitoring',
                'approval' => ''
            ],

        ];

        $ercmSystem = ErcmSystem::where('periode_ercm_id', $id)->get();
        $realData = [];
        foreach ($ercmSystem as $system) {
            $ercmGroup     = ErcmGroup::where('ercm_systems_id', $system->id)->get();
            foreach ($ercmGroup as $group) {
                $groupName  = $group->name;
                $ercmSubGroup  = ErcmSubGroup::with(['ercm_components' => ['ercm_standard_parameter' => ['standard_parameter_number', 'standard_parameter_areas', 'standard_parameter_ranges', 'standard_parameter_approvals']]])->where('ercm_groups_id', $group->id)->get();
                if (!empty($ercmSubGroup)) {
        
                    $orderTypes = [
                        1 => 'standard_parameter_areas',
                        2 => 'standard_parameter_number',
                        3 => 'standard_parameter_ranges',
                        4 => 'standard_parameter_approvals',
                    ];
                    foreach ($ercmSubGroup as $subGroup) {
                        $stdParam = new ErcmComponentStandardParameter;
                        $assestment_type    = $stdParam::$assestmentTypes[$tableId];
                        // dd($subGroup, $assestment_type);
                        foreach ($subGroup->ercm_components as $component) {
                            $assestmentParams = $component->ercm_standard_parameter->where('assestment_type', $assestment_type)->toArray();
                            
                            $parameters_data = []; 
                            foreach ($assestmentParams as $param) {
                                $relation = $orderTypes[$param['order_type']];
                                $param['order_parameters'] = $param[$relation];
                                $param['range_parameters'] = [];
            
                                if ($param['order_type'] == 4) {
                                    // APPROVAL
                                    $parameters_data[] = [
                                        "name"      => $param['name']." (Approval)",
                                        "standard"  => $param['order_parameters'][0]['param']
                                    ];
                                } elseif ($param['order_type'] == 1) {
                                    // AREA
                                    $standards = $param['order_parameters'];
            
                                    $standard = [];
                                    foreach ($standards as $i => $v) {
                                        $no = $i + 1;
                                        $standard[] = [
                                            "STD ".$no => $v['std']
                                        ];
                                    }
                                    $parameters_data[] = [
                                        "name"      => $param['name']." (Area)",
                                        "standard"  => $standard
                                    ];
                                } elseif ($param['order_type'] == 2) {
                                    // NUMBER
                                    $standards = $param['order_parameters'];
            
                                    $standard = [];
                                    $standard[] = ["STD" => $standards['std']];
                                    $standard[] = ["MIN" => $standards['min']];
                                    $parameters_data[] = [
                                        "name"      => $param['name']." (number)",
                                        "standard"  => $standard
                                    ];
                                    // dd($param);
                                } elseif ($param['order_type'] == 3) {
                                    // RANGE
                                    $standards = $param['order_parameters'];
            
                                    $standard = [];
                                    foreach ($standards as $i => $v) {
                                        $no = $i + 1;
                                        $standard[] = [
                                            "name" => $v['criteria']
                                        ];
                                    }
                                    $parameters_data[] = [
                                        "name"      => $param['name']." (Ranges)",
                                        "standard"  => $standard
                                    ];
                                }
                            }
                            $realData[] = [
                                "id"            => $component->id,
                                "group"         => $groupName,
                                "subGroup"      => $subGroup->name,
                                "component"     => $component->component,
                                "parameters"    => $parameters_data,
                                "score"         => 0,
                                "status"        => "",
                                "approval"      => ""
                            ];
                            // dd($component, $assestmentParams, $parameters_data, $realData);
                        }
                    }
                }
            }
        }
        

        $data = collect(array_map(function ($item) {
            return (object)$item;
        }, $realData));
        // }else{
        //     $data = null;
        // }

        return datatables()
            ->of($data)
            ->addColumn('evidence', function ($row) {
                $button = '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                    >
                        Upload
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['evidence'])
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableRepair()
    {
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'score' => 20,
                'status' => 'Repair',
                'classification' => 'Safety Meter',
                'approval' => 'Approved',
                'approvalNote' => ''
            ]

        ];

        $data = collect(array_map(function ($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableTasklist()
    {
        $sampleData = [
            [
                'id' => 1,
                'taskList' => 'Repairing K1',
            ]

        ];

        $data = collect(array_map(function ($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $button = '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-light-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-2') . '
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function datatablePostpone()
    {
        $sampleData = [
            [
                'id' => 1,
                'group' => 'Outside Shell',
                'subGroup' => 'Keel',
                'component' => 'K1',
                'classification' => 'Safety Meter',
                'approval' => 'Approved',
            ]

        ];

        $data = collect(array_map(function ($item) {
            return (object)$item;
        }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    private function datatableDetailSystemParameter($periode_ercm_id)
    {
        // $sampleData = [
        //     [
        //         'id' => 1,
        //         'parameter' => 'Hull Construction',
        //         'value' => '10',
        //         'before' => '',
        //         'after' => '',
        //         'alpha' => '',
        //         'excecute' => '',
        //         'accept' => '',
        //         'reject' => '',
        //         'postpone' => '',
        //     ],
        //     [
        //         'id' => 2,
        //         'parameter' => 'Facility',
        //         'value' => '10',
        //         'before' => '',
        //         'after' => '',
        //         'alpha' => '',
        //         'excecute' => '',
        //         'accept' => '',
        //         'reject' => '',
        //         'postpone' => '',
        //     ]
        // ];

        $data = ErcmSystem::where('periode_ercm_id', $periode_ercm_id)->get();

        // $data = collect(array_map(function($item) {
        //     return (object)$item;
        // }, $sampleData));

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('checkbox', function ($row) {
                $div = '<input class="form-check-input" type="radio" value="' . $row->nama . '" id="flexRadioChecked" name="flexRadioChecked" />';
                // $div = '<input class="form-check-input" type="radio" value="' . $row->nama . '" id="flexRadioChecked-' . $row->nama . '" />';
                return $div;
            })
            ->editColumn('parameter', function ($row) {
                return $row->nama;
            })
            ->addColumn('status', function ($row) {
                $div = '
                     <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#status_modal"
                    >
                        <i class="fa fa-info"></i>
                    </button>
                ';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="' . route('e-rcm.fleet.system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="' . route('e-rcm.fleet.sub-detail-system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>
                ';

                return $button;
            })
            ->rawColumns(['action', 'checkbox', 'status'])
            ->make(true);
    }

    private function datatableSubDetailSystemParameter($ercm_system_id)
    {
        $data = ErcmGroup::with('ercm_subgroups')->where('ercm_system_id', $ercm_system_id)->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('group', function ($row) {
                return $row->nama;
            })
            ->addColumn('status', function ($row) {
                $div = '
                     <button type="button"
                        class="btn btn-sm btn-icon btn-success"
                        data-id="' . $row->id . '"
                        data-bs-toggle="modal"
                        data-bs-target="#status_modal"
                    >
                        <i class="fa fa-info"></i>
                    </button>
                ';
                return $div;
            })
            ->addColumn('action', function ($row) {
                $button = '
                    <a href="' . route('e-rcm.fleet.system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-gray-500">
                       <i class="ki-duotone ki-barcode text-white fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                        <span class="path5"></span>
                        <span class="path6"></span>
                        <span class="path7"></span>
                        <span class="path8"></span>
                        </i>
                    </a>

                    <a href="' . route('e-rcm.fleet.sub-detail-system-parameter', $row->id) . '" class="btn btn-sm btn-icon bg-primary">
                        <i class="fa fa-eye text-white"></i>
                    </a>
                ';

                return $button;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }
}
