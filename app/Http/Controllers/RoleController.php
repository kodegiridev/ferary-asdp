<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleHasPermission;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::orderBy('menu_name', 'ASC')
            ->orderBy('action', 'ASC')
            ->get();

        $menus = $permissions->keyBy('menu_name')->pluck('menu_name');
        $actions = $permissions->keyBy('action')->pluck('action');

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view('pages.user-management.role.index', compact('permissions', 'menus', 'actions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;

            $role = Role::create($data);

            $permissions = array_keys($request->permission_id);
            foreach ($permissions as $permission) {
                $data = [];
                $data['role_id'] = $role->id;
                $data['permission_id'] = $permission;
                RoleHasPermission::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('roles.index')->with(['failed' => 'Gagal Tambah Role!']);
        }

        return redirect()->route('roles.index')->with(['success' => 'Berhasil Tambah Role!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Role  $role
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $permissions = RoleHasPermission::where('role_id', $role->id)
            ->pluck('permission_id')->toArray();

        $data['permissions'] = $permissions;
        $data['role'] = $role;

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Role  $role
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;

            $role->update($data);

            RoleHasPermission::where('role_id', $role->id)
                ->delete();

            $permissions = array_keys($request->permission_id);
            foreach ($permissions as $permission) {
                $data = [];
                $data['role_id'] = $role->id;
                $data['permission_id'] = $permission;
                RoleHasPermission::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('roles.index')->with(['failed' => 'Gagal Ubah Role!']);
        }

        return redirect()->route('roles.index')->with(['success' => 'Berhasil Ubah Role!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        DB::beginTransaction();

        try {
            $role->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Role!');
        }

        Session::flash('success', 'Berhasil Hapus Role!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Role::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-role"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_role_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-role ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
