<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringPort;
use App\Models\ProjectMonitoring\ProjectMonitoringPort;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class CompletionController extends Controller
{
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.project-monitoring.port.completion.mail';
    }

    public function index(Request $request, ProjectMonitoringPort $project)
    {
        try {
            $approvers = User::whereNot('id', auth()->user()->id)
                ->whereHas('role', function ($query) {
                    $query->whereIn('name', Role::$asManager)
                        ->orWhereIn('name', Role::$asVP);
                })
                ->orderBy('name')
                ->get();

            $verificators = User::whereNot('id', auth()->user()->id)
                ->whereHas('role', function ($query) {
                    $query->whereIn('name', Role::$asStaff)
                        ->orWhereIn('name', Role::$asManager)
                        ->orWhereIn('name', Role::$asVP);
                })
                ->orderBy('name')
                ->get();
            $summary = (new SummaryController)->projectSummary($project);

            $data = [
                'progress'      => $summary['progress'],
                'quality'       => $summary['quality'],
                'document'      => $summary['document'],
                'recomendation' => $summary['recomendation'],
                'approvers'     => $approvers,
                'verificators'  => $verificators,
            ];

            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Something went wrong',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function sendApproval(Request $request, ProjectMonitoringPort $project)
    {
        \DB::beginTransaction();

        try {
            if ($project->pic != auth()->user()->id) {
                return response()->json([
                    'message' => 'Hanya Project PIC yang dapat mengirim approval!',
                ], 500);
            }

            $progress = $project->latest_progress;
            if (!$progress ?? false) {
                return response()->json([
                    'message' => 'Tidak ada progres yang dapat di kirim ke approval!',
                ], 500);
            }

            $projectApproval = new ApprovalProjectMonitoringPort;
            $onApproval = $projectApproval->onProject($project->id)->latest()->first();
            if ($onApproval) {
                if ($onApproval->status == 1) {
                    return response()->json([
                        'message' => 'Sedang menunggu persetujuan!',
                    ], 500);
                }

                if ($onApproval->status == 2 && $onApproval->type == $projectApproval::APPROVAL_COMPLETION) {
                    return response()->json([
                        'message' => 'Penyelesaian project telah disetujui!'
                    ], 500);
                }
            }

            $approval = Approval::create([
                'pengaju_id'            => auth()->user()->id,
                'penyetuju_id'          => $request->penyetuju_id,
                'jenis_modul'           => 'IPM Port',
                'sub_modul'             => 'Project Completion',
                'tgl_pengajuan'         => date('Y-m-d'),
                'keterangan_pengaju'    => $request->keterangan_pengaju,
                'status'                => Approval::NEW,
            ]);

            if (!empty($request->user_verifikator_id)) {
                $verifikators = [];
                foreach ($request->user_verifikator_id as $id) {
                    array_push($verifikators, [
                        'user_verifikator_id' => $id,
                        'status' => Verifikator::NEW
                    ]);
                }

                $approval->verifikators()->createMany($verifikators);
            }

            $projectApproval::create([
                'approval_id'                   => $approval->id,
                'project_monitoring_port_id'    => $project->id,
                'catatan_project'               => $request->catatan_project,
                'type'                          => $projectApproval::APPROVAL_COMPLETION,
            ]);

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = 'Project Completion';
            $param['data'] = __('app.notification.ipm.project_completion');
            $param['action'] = route('ipm.port.completion.index', $project->id);
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.ipm.port.subject');
            $param['applicant'] = $approval->pengaju;
            $param['projectMonitoring'] = $project;
            $this->notification->send($param);

            \DB::commit();
            \Session::flash('success', 'Berhasil mengirim approval!');
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'message' => 'Gagal mengirim approval!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function projectSummary(ProjectMonitoringPort $project)
    {
        $notice = NoticeRecomendationPort::countStatus()->selectRaw('periode_id')->groupBy('periode_id');
        $recomendation = PeriodeRecomendationPort::selectRaw('data_periode_recomendation_port.*, total_close, total')
            ->leftJoinSub($notice, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_port.id');
            })
            ->whereProject($project->id)
            ->orderBy('periode', 'desc')
            ->first();

        $recomendationSummary = 0;
        if (($row->total ?? 0) > 0) {
            $recomendationSummary = ($row->total_close / $row->total) * 100;
        }

        $document = (new DocumentController)->index(
            new Request(['is_project_index' => true]),
            $project
        );
        $progres = (new ProgressController)->progres_data($project);
        $quality = (new QualityController)->index(
            new Request(['is_data_summary' => true]),
            $project
        );

        return [
            'document'      => $document['calculated']['total'],
            'recomendation' => toPercent($recomendationSummary),
            'quality'       => toPercent($quality['cumulative']['total_actual']),
            'progress'      => toPercent($progres['total_actual']),
        ];
    }
}
