<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringPort;
use App\Models\ProjectMonitoring\EvidenceProgresPort;
use App\Models\ProjectMonitoring\MainItemProjectPort;
use App\Models\ProjectMonitoring\ProgresProjectPort;
use App\Models\ProjectMonitoring\ProjectMonitoringPort;
use App\Models\ProjectMonitoring\SubItemProjectPort;
use App\Models\ProjectMonitoring\TaskItemProjectPort;
use App\Models\ProjectMonitoring\BasicDataProjectPort;
use DB;
use Illuminate\Http\Request;
use Session;
use App\Models\User;
use Carbon\Carbon;

class ProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectMonitoringPort $project)
    {
        if ($request->ajax()) {
            return $this->datatable($project);
        }
        $port = ProjectMonitoringPort::find($project->id);
        $data = [
            'project' => $port,
            'basic_data'    => BasicDataProjectPort::with('project')->where('project_id', $port->id)->get(),
            'user_pic'      => User::asStaff()->selectRaw('id, name')->orderBy('name')->get(),
            'datas'         => $this->progres_data($project)
        ];

        $status = $this->getLatestProgres($project->id);
        $data_chart = $this->index_chart($project->id);
        $chart = json_encode($data_chart);
        return view('pages.project-monitoring.port.progress.index', compact('port', 'data', 'chart', 'status'));
    }

    public function index_periode(Request $request, ProgresProjectPort $project)
    {
        $project_approval = $this->approval_status(new Request(['progress_id' => $project->id]));
        $data = [
            'project'   => ProgresProjectPort::with('project')->where('id', $project->id)->first(),
            'main_item' => $this->main_item($project->id),
            'datas' => $this->index_main_item($project),
            'can_edit' => $project_approval['can_edit']
        ];
        // return print_r($data['main_item']);
        return view('pages.project-monitoring.port.progress.periode.index', compact('data'));
    }

    public function edit_periode_item(ProgresProjectPort $project, MainItemProjectPort $main_item)
    {
        $sub_item = SubItemProjectPort::with('task_item', 'main_item')->where('id', $main_item->id)->first();
        $data = [
            'project'   => $project,
            'main_item' => MainItemProjectPort::with('sub_item')->where('id', $main_item->id)->first(),
            'sub_item'  => $sub_item,
            'sisa_bobot' => $this->count_sisa_bobot($main_item->id)
        ];
        return view('pages.project-monitoring.port.progress.periode.edit_item', compact('data'));
    }

    public function print_periode(Request $request, ProgresProjectPort $project)
    {
        $get_all_progress = DB::table('progres_project_ports')->select('id')->where('project_id', $project->project_id)->orderBy('id', 'asc')->get();
        $sequence = 1;
        foreach ($get_all_progress as $key => $v) {
            if($v->id == $project->id) {
                $sequence = $key + 1;
                break;
            }
        }
        $data = [
            'project'   => $project,
            'main_item' => $this->main_item($project->id),
            'datas' => $this->index_main_item($project),
            'sequence'  => $sequence,
            'chart_base64' => $request->chart_base64
        ];
        $pdf = \PDF::loadView('pages.project-monitoring.port.progress.periode.print', compact('data'));
        return $pdf->stream('IPM_PROGRESS_PORT_' . now()->unix() . '.pdf');
        // return view('pages.project-monitoring.fleet.progress.periode.print', compact('data', 'imageLogo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $approval_validation = $this->approval_validation(null, $request->project_id, 'menambahkan');
        if (!$approval_validation['status']) {
            return redirect()->back()->with('failed', $approval_validation['message']);
        } 

        DB::beginTransaction();
        try {
            $request->actual_cost = (int) str_replace(',', '', str_replace('Rp. ', '', $request->actual_cost));
            $data['project_id'] = $request->project_id;
            $data['status_kontrak'] = $request->status_kontrak;
            $data['tgl_update'] = $request->tgl_update;
            $data['actual_cost'] = $request->actual_cost;
            $data['remarks'] = $request->remarks;
            $data['plan'] = 1;
            $data['aktual'] = 1;


            ProgresProjectPort::create($data);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan Progres Data.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menambahkan Progres Data.');
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ProgresProjectPort $project
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgresProjectPort $project)
    {
        DB::beginTransaction();
        try {
            $request->actual_cost = (int) str_replace(',', '', str_replace('Rp. ', '', $request->actual_cost));
            $data['tgl_update'] = $request->tgl_update;
            $data['actual_cost'] = $request->actual_cost;
            $data['remarks'] = $request->remarks;

            $project->update($data);

            DB::commit();
            Session::flash('success', 'Berhasil ubah Progres Data.');
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal ubahh Progres Data.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProgresProjectPort $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgresProjectPort $project)
    {
        DB::beginTransaction();

        try {
            $project->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus port progres!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus port progres!']);
        }
        return redirect()->back();
    }

    /**
     * Duplicate the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ProgresProjectPort $project
     *
     * @return \Illuminate\Http\Response
     */
    public function duplicate(ProgresProjectPort $project)
    {
        DB::beginTransaction();
        try {
            $new_progres = [
                "project_id" => $project->project_id,
                "status_kontrak" => $project->status_kontrak,
                "tgl_update" => $project->tgl_update,
                "actual_cost" => $project->actual_cost,
                "remarks" => $project->remarks,
                "plan" => $project->plan,
                "aktual" => $project->aktual
            ];
            $progres = ProgresProjectPort::create($new_progres);

            $main_item = MainItemProjectPort::where('progres_id', $project->id)->get();

            foreach ($main_item as $key => $value) {
                if($value){
                    $new_main = [
                        'progres_id' => $progres->id,
                        'main_item' => $value->main_item
                    ];
                    $main = MainItemProjectPort::create($new_main);

                    $sub_item = SubItemProjectPort::where('main_item_id', $value->id)->get();

                    foreach ($sub_item as $k => $v) {
                        if($v){
                            $new_sub = [
                                'main_item_id' => $main->id,
                                'sub_item' => $v->sub_item,
                                'bobot' => $v->bobot,
                                'plan' => $v->plan,
                                'progres' => $v->progres,
                            ];
                            $sub = SubItemProjectPort::create($new_sub);
                        }
                    }
                }
            }

            DB::commit();
            Session::flash('success', 'Berhasil duplikasi Progres Data.');
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal duplikasi Progres Data.');
        }
        return redirect()->back();
    }

    public function evidence_get(Request $request) {
        $data = EvidenceProgresPort::where('progres_id', $request->progress_id)->get();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function evidence_store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data['progres_id'] = $request->progres_id;
            $data['nama'] = $request->nama;
            $data['file'] = $this->uploadFileName($request->file, 'project-monitoring-port');
            
            EvidenceProgresPort::create($data);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan Dokumentasi Progres.');
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
            Session::flash('failed', 'Gagal menambahkan Dokumentasi Progres.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param EvidenceProgresPort $evidence
     * @return \Illuminate\Http\Response
     */
    public function evidence_destroy(EvidenceProgresPort $evidence)
    {
        DB::beginTransaction();

        try {
            $this->removeFile($evidence->file, 'project-monitoring-port');
            $evidence->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus Dokumentasi progres Port!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus Dokumentasi progres Port!']);
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function main_item_store(Request $request)
    {
        $approval_validation = $this->approval_validation($request->progres_id, null, 'menambahkan');
        if (!$approval_validation['status']) {
            return redirect()->back()->with('failed', $approval_validation['message']);
        }

        DB::beginTransaction();
        try {
            $data['progres_id'] = $request->progres_id;
            $data['main_item'] = $request->main_item;
            $data['batas_bobot'] = $request->batas_bobot;
            MainItemProjectPort::create($data);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan Main Item Progres.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menambahkan Main Item Progres.');
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MainItemProjectPort $main_item
     *
     * @return \Illuminate\Http\Response
     */
    public function main_item_update(Request $request, MainItemProjectPort $main_item)
    {
        $approval_validation = $this->approval_validation($request->progress_id, null, 'memperbarui');
        if (!$approval_validation['status']) {
            return redirect()->back()->with('failed', $approval_validation['message']);
        }
        DB::beginTransaction();
        try {
            $project = $request->progress_id;
            $main_item_id = $request->main_item_id;
            
            $sub_item = $request->sub_item;
            $bobot = $request->bobot;
            $plan = $request->plan;
            $progres = $request->progres;

            $data['main_item'] = $request->main_item;
            $data['batas_bobot'] = $request->batas_bobot;
            $update_main     = DB::table('main_item_project_ports')->where('id', $main_item_id)->update($data);
            $get_sub_items  = DB::table('sub_item_project_ports')->where('main_item_id', $main_item_id)->get();
            if (!empty($get_sub_items) && count($get_sub_items) > 0) {
                foreach ($get_sub_items as $sub) {
                    $delete_task     = DB::table('task_item_project_ports')->where('sub_item_id', $sub->id)->delete();
                }
                $delete_sub     = DB::table('sub_item_project_ports')->where('main_item_id', $main_item_id)->delete();
            }
            
            $insert_batch = [];
            foreach ($sub_item as $i => $v) {
                if (!empty($v)) {
                    $no = $i + 1;
                    $subitem = new SubItemProjectPort;
                    $subitem->main_item_id = $main_item_id;
                    $subitem->sub_item = $v;
                    $subitem->bobot = $bobot[$i];
                    $subitem->plan = $plan[$i];
                    $subitem->progres = $progres[$i];
                    $subitem->save();
                    $ins = [
                        "sub_item_id"   => $subitem->id,
                        "task"          => $v,
                        "bobot"         => $bobot[$i],
                        "plan"         => $plan[$i],
                        "progres"         => $progres[$i],
                    ];

                    $insert_batch[] = $ins;
                }
            }
            if (!empty($insert_batch)) {
                $insert     = DB::table('task_item_project_ports')->insert($insert_batch);
            }

            DB::commit();
            Session::flash('success', 'Berhasil ubah Main Item Progres.');
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            Session::flash('failed', 'Gagal ubahh Main Item Progres.');
        }
        return redirect()->route('ipm.port.progress_periode.index_periode', ['project' => $project]);
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param MainItemProjectPort $main_item
     * @return \Illuminate\Http\Response
     */
    public function main_item_destroy(MainItemProjectPort $main_item)
    {
        $approval_validation = $this->approval_validation($main_item->progres_id, null, 'menghapus');
        if (!$approval_validation['status']) {
            return redirect()->back()->with('failed', $approval_validation['message']);
        }
        DB::beginTransaction();

        try {
            $main_item->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus Main Item Progres.');
            return redirect()->back()->with(['success' => 'Berhasil menghapus Main Item Progres.']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus Main Item Progres.']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sub_item_store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data['main_item_id'] = $request->main_item;
            $data['sub_item'] = $request->sub_item;
            $data['bobot'] = $request->bobot;
            $data['plan'] = $request->plan;
            $data['progres'] = $request->progres;

            SubItemProjectPort::create($data);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan Sub Item Progres.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menambahkan Sub Item Progres.');
        }
    }

    public function update_project(Request $request, ProjectMonitoringPort $project)
    {
        // dd($request->all(), $project);
        DB::beginTransaction();
        try {
            $request->nilai_project = (int) str_replace(',', '', str_replace('Rp. ', '', $request->nilai_project));
            $pic = !empty($request->pic) ? (int) $request->pic : $project->pic;
            $kontraktor = !empty($request->kontraktor) ? $request->kontraktor : $project->kontraktor;
            $data['nama_project'] = $request->nama_project;
            $data['nilai_project'] = $request->nilai_project;
            $data['pic'] = $pic;
            $data['kontraktor'] = $kontraktor;
            $data['nomor_kontrak'] = $request->nomor_kontrak;
            $data['supervisor'] = $request->supervisor;
            // dd($data);
            $project->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ipm.port.progress.index', $project->id)->with(['failed' => 'Gagal Ubah Data Project.']);
        }

        return redirect()->route('ipm.port.progress.index', $project->id)->with(['success' => 'Berhasil Ubah Data Project.']);
    }

    public function progress_get(Request $request) {
        $data = ProgresProjectPort::where('id', $request->progress_id)->first();
        return response()->json($data);
    }

    private function datatable($project)
    {
        $isAdmin = isAdmin();
        $data = $this->index_progres($project);
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('actual_cost', function ($row) {
                return toRupiah($row->actual_cost);
            })
            ->addColumn('progress_plan', function ($row) {
                return toDecimal($row->plan);
            })
            ->addColumn('progress_actual', function ($row) {
                return toDecimal($row->actual);
            })
            ->addColumn('progress_deviasi', function ($row) {
                return toDecimal($row->deviasi);
            })
            ->addColumn('total_progress_plan', function ($row) {
                return toDecimal($row->total_plan);
            })
            ->addColumn('total_progress_actual', function ($row) {
                return toDecimal($row->total_actual);
            })
            ->addColumn('total_progress_deviasi', function ($row) {
                return toDecimal($row->total_deviasi);
            })
            ->addColumn('cpi', function ($row) {
                return toDecimal($row->cpi);
            })
            ->addColumn('ppc', function ($row) {
                return toDecimal($row->ppc);
            })
            ->addColumn('etc', function ($row) {
                return toDecimal($row->etc);
            })
            ->addColumn('document', function ($row) {
                return '
                    <a onclick="open_document(`'.$row->id.'`)" href="javascript:void(0)" class="btn btn-secondary btn-sm m-1" data-bs-toggle="modal" data-bs-target="#add_progress_documentation_modal">Lihat</a>
                ';
            })
            ->addColumn('status', function ($row) {
                return '<span type="button" class="badge badge-outline ' . $row->status['class'] . '" data-id="' . $row->status['id'] . '">' . __($row->status['label']) . '</span>';
            })
            ->addColumn('action', function ($row) use ($isAdmin) {
                $button = '
                    <a href="' . route('ipm.port.progress_periode.index_periode', $row) . '" class="btn btn-icon btn-primary btn-sm m-1">
                        ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'fs-1') . '
                    </a>
                    <a onclick="edit_progress(`'.$row->id.'`)" href="javascript:void(0)" class="btn btn-icon btn-success btn-sm m-1" data-bs-toggle="modal" data-bs-target="#edit_progress_modal">
                        ' . theme()->getSvgIcon('icons/bi/pencil-square.svg', 'fs-1') . '
                    </a>
                    <a onclick="duplicate_progress(`'.$row->id.'`, `'.$row->tgl_update.'`)" href="javascript:void(0)" class="btn btn-icon btn-warning btn-sm m-1">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen028.svg', 'fs-1') . ' 
                    </a>
                    <a onclick="print_progress(`'.$row->id.'`)" class="btn btn-icon btn-secondary btn-sm m-1">
                        ' . theme()->getSvgIcon('icons/bi/printer-fill.svg', 'fs-1') . '
                    </a>
                    <a onclick="delete_progress(`'.$row->id.'`, `'.$row->tgl_update.'`)" href="javascript:void(0)" class="btn btn-icon btn-danger btn-sm m-1">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . '
                    </a>
                ';

                if ($row->project->pic != auth()->user()->id) {
                    $button = '
                        <a href="' . route('ipm.port.progress_periode.index_periode', $row) . '" class="btn btn-icon btn-primary btn-sm m-1">
                            ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'fs-1') . '
                        </a>
                    ';
                }

                if ($isAdmin) {
                    // $button .= '
                    //     <button type="button"
                    //         class="btn btn-sm btn-icon btn-light-danger btn-delete"
                    //         data-id=' . $row->id . '>
                    //         ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                    //     </button>
                    // ';
                }

                return $button;
            })
            ->rawColumns(['action', 'document', 'status'])
            ->make(true);
    }

    function index_chart($id)
    {
        $total_progres = 0;
        $total_plan = 0;

        $progres = ProgresProjectPort::where('project_id', $id)->get();

        foreach ($progres as $key => $value) {
            if ($value) {
                $total = $this->index_main_item($value);

                $total_progres += $total['total_progres'];
                $total_plan += $total['total_plan'];

                $progres[$key]->total_actual = $total_progres;
                $progres[$key]->total_plan = $total_plan;
            }
        }

        return $progres;
    }

    function index_progres($project)
    {
        $total_plan = 0;
        $total_aktual = 0;
        $total_deviasi = 0;
        $total_cost = 0;

        $progres = ProgresProjectPort::with('approval_project')->where('project_id', $project->id)->orderBy('created_at')->get();

        $jumlah_periode = count($progres);

        foreach ($progres as $key => $value) {
            if ($value) {
                $progres[$key]->status = ['label' => 'OPEN', 'class' => 'badge-primary', 'id' => null];
                if ($value->approval_project && $value->approval_project->status == ApprovalProjectMonitoringPort::STATUS_APPROVED) {
                    $progres[$key]->status = ['label' => 'CLOSE', 'class' => 'badge-danger btn-reopen', 'id' => $value->approval_project->id];
                }

                $total = $this->index_main_item($value);

                $progres[$key]->actual = $total['total_progres'];
                $progres[$key]->deviasi = $total['total_deviasi'];
                $progres[$key]->plan = $total['total_plan'];
                if ($total['total_plan'] > 0 && $total['total_progres'] > 0) {
                    $progres[$key]->ppc = $total['total_progres'] / ($total['total_plan']);
                } else {
                    $progres[$key]->ppc = 0;
                }

                $total_plan += $total['total_plan'];
                $total_aktual += $total['total_progres'];
                $total_deviasi += $total['total_deviasi'];
                $total_cost += $value->actual_cost;

                $progres[$key]->total_plan = $total_plan;
                $progres[$key]->total_actual = $total_aktual;
                $progres[$key]->total_deviasi = $total_deviasi;
                if ($total_aktual > 0 && $total_cost > 0) {
                    $progres[$key]->cpi = $value->actual_cost / ($total_cost * $total_aktual / 100);
                } else {
                    $progres[$key]->cpi = 0;
                }

                $sp1 = $value->project->nilai_project / $value->actual_cost;
                $etc = (($jumlah_periode - ($key+1))/$sp1) + ($key+1);

                $progres[$key]->etc = $etc;
            }
        }

        return $progres;
    }

    function progres_data($project)
    {
        $total_plan = 0;
        $total_aktual = 0;
        $total_deviasi = 0;
        $progres = ProgresProjectPort::where('project_id', $project->id)->orderBy('created_at')->get();

        foreach ($progres as $key => $value) {
            if ($value) {
                $total = $this->index_main_item($value);

                $total_plan += $total['total_plan'];
                $total_aktual += $total['total_progres'];
                $total_deviasi += $total['total_deviasi'];
            }
        }

        $data = [
            'total_plan' => $total_plan,
            'total_actual' => $total_aktual,
            'total_deviasi' => $total_deviasi
        ];

        return $data;
    }

    function index_main_item($project)
    {
        $total_plan = 0;
        $total_progres = 0;
        $total_deviasi = 0;

        $main_item = $this->main_item($project->id);

        foreach ($main_item as $key => $value) {
            if ($value) {
                $total_plan += $value->plan;
                $total_progres += $value->progres_ini;
                $total_deviasi += $value->deviasi;
            }
        }

        $data = [
            'total_plan' => $total_plan,
            'total_progres' => $total_progres,
            'total_deviasi' => $total_deviasi
        ];

        return $data;
    }

    function main_item($id)
    {
        $main_item = MainItemProjectPort::with('sub_item')->where('progres_id', $id)->get();

        $progres_lalu = 0;

        foreach ($main_item as $key => $value) {
            if ($value) {
                $plan = 0;
                $bobot = 0;
                $progres_ini = 0;
                $deviasi = 0;

                $sub_item = $value->sub_item;

                foreach ($sub_item as $k => $v) {
                    if ($v) {
                        $sub_item[$k]->bobot = $this->bobot($v->id);
                        $sub_item[$k]->plan = $this->count_plan($v->id);
                        $sub_item[$k]->progres_lalu = $progres_ini;
                        $sub_item[$k]->progres_ini = $this->count_progres_ini($v->id);
                        $sub_item[$k]->deviasi = $this->count_deviasi($v->id);

                        $bobot += $this->bobot($v->id);
                        $plan += $this->count_plan($v->id);
                        $progres_ini += $this->count_progres_ini($v->id);
                        $deviasi += $this->count_deviasi($v->id);
                    }
                }

                $main_item[$key]->plan = $plan;
                $main_item[$key]->progres_ini = $progres_ini;
                $main_item[$key]->progres_lalu = $progres_lalu;
                $main_item[$key]->deviasi = $deviasi;

                $progres_lalu += $progres_ini;
            }
        }

        return $main_item;
    }

    function bobot($id)
    {
        return SubItemProjectPort::where('id', $id)->sum('bobot') ?? 0;
    }

    function plan($id)
    {
        return SubItemProjectPort::where('id', $id)->sum('plan') ?? 0;
    }

    function progres($id)
    {
        return SubItemProjectPort::where('id', $id)->sum('progres') ?? 0;
    }

    function count_plan($id)
    {
        if ($this->plan($id) > 0 && $this->bobot($id) > 0) {
            return ($this->bobot($id) * $this->plan($id)) / 100;
        } else {
            return 0;
        }
    }

    function count_progres_ini($id)
    {
        if ($this->progres($id) > 0 && $this->bobot($id) > 0) {
            return ($this->bobot($id) * $this->progres($id)) / 100;
        } else {
            return 0;
        }
    }

    function count_deviasi($id)
    {
        if ($this->count_progres_ini($id) > 0) {
            return $this->count_progres_ini($id) - $this->count_plan($id);
        } else {
            return 0;
        }
    }

    function count_sisa_bobot($id)
    {
        $main_item = MainItemProjectPort::with('sub_item')->where('id', $id)->first();

        $sub_item = $main_item->sub_item;
        $bobot = 0;

        foreach ($sub_item as $key => $value) {
            if ($value) {
                $bobot += $this->bobot($value->id);
            }
        }

        $sisa_bobot = $main_item->batas_bobot - $bobot;

        return $sisa_bobot;
    }

    public function reopen_progress(Request $request)
    {
        DB::beginTransaction();

        $project_approval = ApprovalProjectMonitoringPort::find($request->id);
        if (!$project_approval) {
            Session::flash('failed', 'Gagal membuka kembali progres. Approval tidak ditemukan!');
            return;
        }

        try {
            $project_approval->update(['status' => 0]);

            DB::commit();
            Session::flash('success', 'Berhasil membuka kembali progres!');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('failed', 'Gagal membuka kembali progres!');
        }

        return;
    }

    public function approval_status(Request $request)
    {
        $project_approval = ApprovalProjectMonitoringPort::onProgress($request->progress_id)->first();
        return [
            'can_edit' => !($project_approval && $project_approval->status == ApprovalProjectMonitoringPort::STATUS_APPROVED),
            'type' => $project_approval->type ?? null
        ];
    }

    public function approval_validation($progess_id = null, $project_id = null, $action = null)
    {
        $data = ['status' => true];
        $project_approval = new ApprovalProjectMonitoringPort;
        if ($progess_id) {
            $project_approval = $project_approval->onProgress($progess_id);
        }

        if ($project_id) {
            $project_approval = $project_approval->onProject($project_id)->where('type', ApprovalProjectMonitoringPort::APPROVAL_COMPLETION);
        }

        $project_approval = $project_approval->orderBy('created_at', 'desc')->first();
        if ($project_approval) {
            if ($project_approval->status == ApprovalProjectMonitoringPort::STATUS_APPROVED) {
                $data['status'] = false;
                $data['message'] = $project_approval->type == ApprovalProjectMonitoringPort::APPROVAL_SUMMARY
                    ? 'Progress telah disetujui. Silakan buat progres baru atau buka kembali!'
                    : 'Tidak dapat ' . $action . ' data. Project telah diakhiri!';
            }
        }

        return $data;
    }

    public function getLatestProgres($project_id)
    {
        $status = 'close';
        $progres = ProgresProjectPort::with('approval_project')->where('project_id', $project_id)->latest()->first();
        if ($progres) {
            if ($progres->approval_project ?? false) {
                if ($progres->approval_project->status != ApprovalProjectMonitoringPort::STATUS_APPROVED) {
                    $status = 'open';
                }
            } else {
                $status = 'open';
            }
        }

        return $status;
    }
}
