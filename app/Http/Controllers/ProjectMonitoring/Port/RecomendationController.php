<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectMonitoring\Port\Recomendation\StoreNoticeRequest;
use App\Http\Requests\ProjectMonitoring\Port\Recomendation\UpdateNoticeRequest;
use App\Models\ProjectMonitoring\NoticeRecomendationPort;
use App\Models\ProjectMonitoring\PeriodeRecomendationPort;
use App\Models\ProjectMonitoring\ProjectMonitoringPort;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecomendationController extends Controller
{
    private $path = 'ipm/port/recomendation';

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringPort $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectMonitoringPort $project)
    {
        if ($request->ajax()) {
            return $this->datatable($project);
        }

        $notice = NoticeRecomendationPort::whereHas('periode_recomendation', function ($query) use ($project) {
            return $query->where('project_id', $project->id);
        });

        $data = [
            'project' => $project,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $notice)->openStatus()->count(),
                'total_close' => (clone $notice)->closeStatus()->count(),
                'total' => $notice->count()
            ]
        ];

        return view('pages.project-monitoring.port.recomendation.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param ProjectMonitoringPort $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ProjectMonitoringPort $project)
    {
        DB::beginTransaction();

        try {
            $data = PeriodeRecomendationPort::whereProject($project->id)
                ->with([
                    'notices' => fn ($q) => $q->selectRaw('periode_id, notice, to_do_list, due_date, status, pic, file')
                ])
                ->orderBy('periode', 'desc')
                ->first();

            $store = PeriodeRecomendationPort::create([
                'project_id' => $project->id,
                'periode' => ($data->periode ?? 0) + 1
            ]);
            if (count($data->notices ?? []) > 0) {
                $store->notices()->createMany($data->notices->toArray());
            }

            DB::commit();
            return redirect()->route('ipm.port.recomendation.index', $project->id)->with(['success' => 'Berhasil menambah period!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah period!']);
        }
    }

    /**
     * Data for own statistics
     * 
     * @param ProjectMonitoringPort $project
     * @return \PDF
     */
    public function print(ProjectMonitoringPort $project)
    {
        $fileName = 'IPM_PORT_' . $project->nama_project . '_' . time();
        $noticeQuery = NoticeRecomendationPort::countStatus()->selectRaw('periode_id')->groupBy('periode_id');
        $noticeCount = NoticeRecomendationPort::whereHas('periode_recomendation', function ($query) use ($project) {
            return $query->where('project_id', $project->id);
        });

        $periods = PeriodeRecomendationPort::selectRaw('data_periode_recomendation_port.*, total_close, total')
            ->whereProject($project->id)
            ->joinSub($noticeQuery, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_port.id');
            })
            ->get();

        $data = [
            'project' => $project,
            'periods' => $periods,
            'file_name' => $fileName,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $noticeCount)->openStatus()->count(),
                'total_close' => (clone $noticeCount)->closeStatus()->count(),
                'total' => $noticeCount->count()
            ]
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.port.recomendation.pdf.recomendation', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \Illuminate\Http\Response
     */
    public function indexNotice(Request $request, ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        if ($request->ajax()) {
            return $this->datatableNotice($period);
        }

        $query = NoticeRecomendationPort::where('periode_id', $period->id);

        $data = [
            'project' => $project,
            'period' => $period,
            'users' => User::asStaff()->selectRaw('id, name')->orderBy('name')->get(),
            'status' => ['open', 'close'],
            'notice' => [
                'total_open' => (clone $query)->openStatus()->count(),
                'total_close' => (clone $query)->closeStatus()->count(),
                'total' => $query->count()
            ],
            'basic_data' => $project->latest_basic_data
        ];

        return view('pages.project-monitoring.port.recomendation.notice.index', compact('data'));
    }

    /**
     * Display a listing of the resource.
     * 
     * @param StoreNoticeRequest $request
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \Illuminate\Http\Response
     */
    public function storeNotice(StoreNoticeRequest $request, ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        DB::beginTransaction();

        try {
            NoticeRecomendationPort::create(array_merge($request->validated(), [
                'periode_id' => $period->id,
                'file' => $this->uploadFile($request->file('file'), $this->path)
            ]));

            DB::commit();
            return redirect()->route('ipm.port.recomendation.notice.index', [$project->id, $period->id])->with(['success' => 'Berhasil menambah notice']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah notice']);
        }
    }

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \Illuminate\Http\Response
     */
    public function showNotice(Request $request, ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        $data = NoticeRecomendationPort::selectRaw('*, DATE_FORMAT(due_date, "%Y-%m-%d") AS due_date')->where('id', $request->id)->first();
        if (!$data) {
            return response('Notice tidak ditemukan!', 400);
        }

        $data->users = User::asStaff()->selectRaw('id, name')->orderBy('name')->get();

        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     * 
     * @param UpdateNoticeRequest $request
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \Illuminate\Http\Response
     */
    public function updateNotice(UpdateNoticeRequest $request, ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        DB::beginTransaction();

        $data = NoticeRecomendationPort::find($request->id);
        if (!$data) {
            return redirect()->back()->withInput()->with(['failed' => 'Notice tidak ditemukan!']);
        }

        try {
            $_request = $request->validated();
            if ($request->file) {
                $_request['file'] = $this->uploadFile($request->file('file'), $this->path);
            } else {
                unset($_request['file']);
            }

            $data->update($_request);

            DB::commit();
            return redirect()->route('ipm.port.recomendation.notice.index', [$project->id, $period->id])->with(['success' => 'Berhasil memperbarui notice']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal memperbarui notice']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \Illuminate\Http\Response
     */
    public function destroyNotice(Request $request, ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        DB::beginTransaction();

        try {
            $data = NoticeRecomendationPort::find($request->id);
            if (!$data) {
                \Session::flash('failed', 'Notice terpilih tidak ditemukan!');
                return;
            }

            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus notice!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus notice!');
        }
    }

    /**
     * Data for own statistics
     * 
     * @param ProjectMonitoringPort $project
     * @param PeriodeRecomendationPort $period
     * @return \PDF
     */
    public function printNotice(ProjectMonitoringPort $project, PeriodeRecomendationPort $period)
    {
        $fileName = 'IPM_PORT_' . $project->nama_project . '-' . $period->periode . '_' . time();
        $noticeQuery = NoticeRecomendationPort::where('periode_id', $period->id)->orderBy('id');

        $data = [
            'project' => $project,
            'notices' => (clone $noticeQuery)->get(),
            'file_name' => $fileName,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $noticeQuery)->openStatus()->count(),
                'total_close' => (clone $noticeQuery)->closeStatus()->count(),
                'total' => $noticeQuery->count()
            ]
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.port.recomendation.pdf.notice', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * Display data recomendation in datatables
     *
     * @param ProjectMonitoringPort $project
     * @return \Yajra\DataTables\DataTables
     */
    public function datatable(ProjectMonitoringPort $project)
    {
        $notice = NoticeRecomendationPort::countStatus()->selectRaw('periode_id')->groupBy('periode_id');

        $data = PeriodeRecomendationPort::selectRaw('data_periode_recomendation_port.*, total_close, total')
            ->whereProject($project->id)
            ->leftJoinSub($notice, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_port.id');
            })
            ->get();

        return datatables()
            ->of($data)
            ->addColumn('total_notice', function ($row) {
                return $row->total ?? 0;
            })
            ->addColumn('closed_notice', function ($row) {
                return $row->total_close ?? 0;
            })
            ->addColumn('closed_percentage', function ($row) {
                $amount = 0;
                if (($row->total ?? 0) > 0) {
                    $amount = ($row->total_close / $row->total) * 100;
                }

                return toPercent($amount, 2);
            })
            ->addColumn('action', function ($row) use ($project) {
                return '
                    <a href="' . route('ipm.port.recomendation.notice.index', [$project->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                        ' . theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') . '
                    </a>

                    <a href="' . route('ipm.port.recomendation.notice.print', [$project->id, $row->id]) . '" class="btn btn-sm btn-success btn-icon">
                        ' . theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') . '
                    </a>
                ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data notice in datatables.
     * 
     * @param PeriodeRecomendationPort $period
     * @return \Yajra\DataTables\DataTables
     */
    public function datatableNotice(PeriodeRecomendationPort $period)
    {
        $data = NoticeRecomendationPort::where('periode_id', $period->id)
            ->orderBy('id')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('pic', function ($row) {
                return $row->user->name;
            })
            ->addColumn('status', function ($row) {
                return strtoupper($row->status);
            })
            ->addColumn('due_date', function ($row) {
                return \Carbon\Carbon::parse($row->due_date)->format('Y-m-d');
            })
            ->addColumn('file', function ($row) {
                return '
                    <a href="' . $row->file . '" class="text text-hover-primary">
                        ' . $this->getFileName($row->file) . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) {
                return '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-primary btn-edit"
                        data-id=' . $row['id'] . '>
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'fs-1') . ' 
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-danger btn-delete"
                        data-id=' . $row['id'] . '>
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                    </button>
                ';
            })
            ->rawColumns(['action', 'file'])
            ->make(true);
    }
}
