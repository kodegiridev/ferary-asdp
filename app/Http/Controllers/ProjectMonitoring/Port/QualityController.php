<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Models\ProjectMonitoring\DrawingVisualQuality;
use App\Models\ProjectMonitoring\InspectionAcceptanceRatePort;
use App\Models\ProjectMonitoring\PeriodeRecomendationPort;
use App\Models\ProjectMonitoring\PeriodeVisualQualityPort;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringPort;
use App\Models\ProjectMonitoring\SubVisualQualityPort;
use App\Models\ProjectMonitoring\VisualQualityPort;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QualityController extends Controller
{
    public function index(Request $request, ProjectMonitoringPort $project)
    {
        if ($request->ajax()) {
            return $this->indexVisualQuality($request, $project);
        }

        $visualQualities = VisualQualityPort::with('sub_visual_qualities')
            ->where('project_id', $project->id)
            ->get();

        $idSubVisualQualities = $visualQualities->pluck('sub_visual_qualities')->flatten()->pluck('id');
        $periods = PeriodeVisualQualityPort::selectRaw('periode')
            ->whereIn('sub_visual_quality_port_id', $idSubVisualQualities)
            ->groupBy('periode')
            ->get();

        $cumulative = [
            'total_actual' => 0,
            'total_plan' => 0,
            'total_deviasi' => 0,
        ];
        $_request = new Request(['type' => 'VisualQuality']);
        foreach ($periods as $period) {
            $_request->merge([
                'periode' => $period->periode
            ]);

            $calculate = $this->indexVisualQuality($_request, $project);
            $cumulative['actual'][] = collect($calculate['data'])->sum('total_actual');
            $cumulative['plan'][] = collect($calculate['data'])->sum('total_plan');
            $cumulative['deviasi'][] = collect($calculate['data'])->sum('total_deviasi');
            $cumulative['total_actual'] += collect($calculate['data'])->sum('total_actual');
            $cumulative['total_plan'] += collect($calculate['data'])->sum('total_plan');
            $cumulative['total_deviasi'] += collect($calculate['data'])->sum('total_deviasi');
        }

        $data = [
            'sisa_bobot'            => 100 - $visualQualities->sum('bobot'),
            'data'                  => $visualQualities->toArray(),
            'types'                 => array_keys($this->facilityGroup()),
            'project'               => $project,
            'periods'               => $periods->pluck('periode')->all(),
            'last_period'           => $periods->pluck('periode')->max(),
            'basic_data'            => $project->latest_basic_data,
            'cumulative'            => $cumulative,
            'drawing'               => DrawingVisualQuality::isPort()->lastDate()->onProject($project->id)->first()
        ];

        if ($request->is_data_summary ?? false) {
            return $data;
        }

        return view('pages.project-monitoring.port.quality.index', compact('data'));
    }

    public function detail(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        $subVQ  = SubVisualQualityPort::with('visual_quality')
            ->where('id', $period->sub_visual_quality_port_id)
            ->first();

        $inspections = $subVQ->visual_quality->fasilitas != 'Facility'
            ? $this->facilityGroup()[$subVQ->visual_quality->fasilitas][$subVQ->sub_fasilitas]
            : $this->facilityGroup()[$subVQ->visual_quality->fasilitas];

        $data = [
            'project'               => $project,
            'basic_data'            => $project->latest_basic_data,
            'data'                  => $subVQ,
            'period'                => $period,
            'inspection_types'      => $inspections,
            'outstandings'          => PeriodeVisualQualityPort::$outstandings,
            'first_inspection'      => array_key_first($inspections),
            'first_label'           => array_values($inspections)[0] ?? '',
            'quality_inspection'    => $this->qualityInspectionProgress($period, $subVQ->visual_quality->fasilitas),
            'drawing'               => DrawingVisualQuality::isPort()->lastDate()->onProject($project->id)->first(),
            'status'                => $this->remarkStatus()
        ];

        return view('pages.project-monitoring.port.quality.detail', compact('data'));
    }

    public function print(Request $request, ProjectMonitoringPort $project)
    {
        $fileName = 'IPM_QUALITY_PORT_' . trim($project->nama_project) . time();
        $visualQualities = $this->indexVisualQuality(new Request(['type' => 'VisualQuality', 'periode' => $request->periode]), $project)['data'];
        $cumulative = [
            'total_actual' => collect($visualQualities)->sum('total_actual'),
            'total_plan' => collect($visualQualities)->sum('total_plan'),
        ];
        $cumulative['total_deviasi'] = $cumulative['total_plan'] - $cumulative['total_actual'];

        $data = [
            'file_name'                 => $fileName,
            'project'                   => $project,
            'basic_data'                => $project->latest_basic_data,
            'period'                    => $request->periode,
            'cumulative'                => $cumulative,
            'visual_qualities'          => $visualQualities,
            'inspection_acceptances'    => $this->indexVisualQuality(new Request(['type' => 'Inspection', 'periode' => $request->periode]), $project)['data']
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.port.quality.pdf.index', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    public function indexInspection(Request $request, ProjectMonitoringPort $project, SubVisualQualityPort $sub)
    {
        if ($request->ajax()) {
            $data = InspectionAcceptanceRatePort::selectRaw('*, CONVERT(jml_undangan,char) AS jml_undangan')
                ->where('sub_visual_quality_port_id', $sub->id)
                ->orderBy('periode')
                ->orderBy('created_at')
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('rate', function ($row) {
                    return '
                        Acc by Consultant / MK : ' . toPercent($row->jml_undangan > 0 ? ($row->acc_konsultan / $row->jml_undangan) * 100 : 0) . '
                        <br>
                        Acc by Owner : ' .  toPercent($row->jml_undangan > 0 ? ($row->acc_owner / $row->jml_undangan) * 100 : 0) . '
                        <br>
                        Acc by Both : ' .  toPercent($row->jml_undangan > 0 ? ($row->acc_both / $row->jml_undangan) * 100 : 0) . '
                    ';
                })
                ->addColumn('evidence', function ($row) {
                    return '
                        <a href="' . $row->evidence . '" class="text-primary">
                            ' . $this->getFileName($row->evidence) . '
                        </a>
                    ';
                })
                ->addColumn('action', function ($row) {
                    return '
                        <a class="btn btn-sm btn-icon btn-success btn-inspection-edit" type="button" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                        </a>
                        <a class="btn btn-sm btn-icon btn-danger btn-inspection-delete" type="button" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                        </a>
                    ';
                })
                ->rawColumns(['action', 'evidence', 'rate'])
                ->make(true);
        }

        $visualQualities = $this->indexVisualQuality(new Request(['type' => 'VisualQuality', 'periode' => $request->periode]), $project)['data'];
        $cumulative = [
            'total_actual' => collect($visualQualities)->sum('total_actual'),
            'total_plan' => collect($visualQualities)->sum('total_plan'),
        ];
        $cumulative['total_deviasi'] = $cumulative['total_plan'] - $cumulative['total_actual'];

        $sub->visual_quality;
        $data = [
            'project'               => $project,
            'sub_visual_quality'    => $sub,
            'cumulative'            => $cumulative,
        ];

        return view('pages.project-monitoring.port.quality.detail_inspection', compact('data'));
    }

    public function storeInspection(Request $request, ProjectMonitoringPort $project, SubVisualQualityPort $sub)
    {
        DB::beginTransaction();

        try {
            $path = 'ipm/port/quality/inspection/' . $sub->visual_quality->fasilitas . '/' . $request->periode;

            InspectionAcceptanceRatePort::create([
                'visual_quality_port_id'        => $request->visual_quality_port_id,
                'sub_visual_quality_port_id'    => $request->sub_visual_quality_port_id,
                'periode'                       => $request->periode,
                'jml_undangan'                  => $request->jml_undangan,
                'reinspect'                     => $request->reinspect,
                'acc_konsultan'                 => $request->acc_konsultan,
                'acc_owner'                     => $request->acc_owner,
                'acc_both'                      => $request->acc_both,
                'evidence'                      => $this->uploadFile($request->file('evidence'), $path)
            ]);

            DB::commit();
            return redirect()->route('ipm.port.quality.inspection.index', [$project->id, $sub->id])->with('success', 'Berhasil menambah inspection rate data!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', 'Gagal menambah inspection rate!');
        }
    }

    public function showInspection(Request $request, ProjectMonitoringPort $project, SubVisualQualityPort $sub)
    {
        $data = InspectionAcceptanceRatePort::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        return response()->json($data);
    }

    public function updateInspection(Request $request, ProjectMonitoringPort $project, SubVisualQualityPort $sub)
    {
        DB::beginTransaction();

        $data = InspectionAcceptanceRatePort::find($request->id);
        if (!$data) {
            return redirect()->back()->with('failed', 'Data tidak ditemukan!');
        }

        try {
            $path = 'ipm/port/quality/inspection/' . $sub->visual_quality->fasilitas . '/' . $request->periode;
            $beUpdate = [
                'periode'       => $request->periode,
                'jml_undangan'  => $request->jml_undangan,
                'reinspect'     => $request->reinspect,
                'acc_konsultan' => $request->acc_konsultan,
                'acc_owner'     => $request->acc_owner,
                'acc_both'      => $request->acc_both,
            ];
            if ($request->evidence) {
                $beUpdate['evidence'] = $this->uploadFile($request->file('evidence'), $path);
            }

            $data->update($beUpdate);

            DB::commit();
            return redirect()->route('ipm.port.quality.inspection.index', [$project->id, $sub->id])->with('success', 'Berhasil memperbarui inspection rate data!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', 'Gagal memperbarui inspection rate!');
        }
    }

    public function destroyInspection(Request $request, ProjectMonitoringPort $project, SubVisualQualityPort $sub)
    {
        DB::beginTransaction();

        $data = InspectionAcceptanceRatePort::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        try {
            $data->delete();
            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal menghapus data!', 'error' => $e->getMessage()], 500);
        }
    }

    public function indexVisualQuality(Request $request, ProjectMonitoringPort $project)
    {
        $data = VisualQualityPort::where('project_id', $project->id)
            ->with([
                'sub_visual_qualities' => function ($query) use ($request) {
                    $query->orderBy('created_at')
                        ->when($request->type == 'Inspection', function ($query) use ($request) {
                            return $query->with([
                                'inspection_acceptance_rates' => function ($query) use ($request) {
                                    $query->where('periode', $request->periode)
                                        ->selectRaw('
                                            sub_visual_quality_port_id
                                            ,(acc_konsultan/jml_undangan) * 100 AS consultant_rate
                                            ,(acc_owner/jml_undangan) * 100 AS owner_rate
                                            ,(acc_both/jml_undangan) * 100 AS both_rate
                                        ')
                                        ->limit(1);
                                }
                            ]);
                        });
                }
            ])
            ->orderBy('created_at')
            ->get();

        if ($request->type == 'VisualQuality') {
            $calculated = [];
            $modals = [
                'Concrete' => '\App\Models\ProjectMonitoring\VQPConcrateGroup',
                'Facility' => '\App\Models\ProjectMonitoring\VQPFacilityGroup',
                'Steel' => '\App\Models\ProjectMonitoring\VQPSteelGroup',
            ];

            foreach ($data as $key => $value) {
                $subQualities = [];
                foreach ($value->sub_visual_qualities as $key => $sub) {
                    $actual = $plan = 0;
                    $status = 'On Progress';
                    $period = PeriodeVisualQualityPort::where('sub_visual_quality_port_id', $sub->id);
                    if ($request->periode ?? false) {
                        $period = $period->where('periode', $request->periode);
                    } else {
                        $period = $period->orderBy('periode', 'desc');
                    }

                    $period = $period->first();
                    if ($period) {
                        $groups = (new $modals[$value->fasilitas])->where('periode_visual_quality_port_id', $period->id)
                            ->with(['remarks' => fn ($q) => $q->defineValue()])
                            ->get();

                        $remarks = $groups
                            ->map(function ($item, $key) {
                                return $item->remarks;
                            })
                            ->flatten(1)
                            ->values();

                        $sheet = count($this->facilityGroup()[$value->fasilitas][$sub->sub_fasilitas] ?? []);
                        if ($sheet > 0) {
                            $actual = ($remarks->avg('status_inspeksi') * (100 / $sheet)) / 100;
                            $plan = ($remarks->avg('status_plan') * (100 / $sheet)) / 100;
                        } else {
                            $count = $remarks->count();
                            if ($count > 0) {
                                $actual = ($remarks->sum('status_inspeksi') / $count) * ($sub->bobot / 100);
                                $plan = ($remarks->sum('status_plan') / $count) * ($sub->bobot / 100);
                            }
                        }

                        if ($actual > 0 || $plan > 0) {
                            if (($actual - $plan) == 0) {
                                $status = 'Accepted';
                            }
                        }
                    }

                    $sub->actual = $actual;
                    $sub->plan = $plan;
                    $sub->deviasi = $actual - $plan;
                    $sub->status_progress = $status;

                    array_push($subQualities, $sub->toArray());
                }

                $value->total_actual = array_sum(array_column($subQualities, 'actual'));
                $value->total_plan = array_sum(array_column($subQualities, 'plan'));
                $value->total_deviasi = array_sum(array_column($subQualities, 'deviasi'));
                $value->sub_visual_qualities = $subQualities;
                array_push($calculated, $value->toArray());
            }

            $data = $calculated;
        }

        return ['data' => $data];
    }

    public function storeVisualQuality(Request $request, ProjectMonitoringPort $project)
    {
        $request->validate([
            'fasilitas' => 'required'
        ]);

        DB::beginTransaction();

        try {
            $availableBobot = 100 - VisualQualityPort::where('fasilitas', '!=', $request->fasilitas)->where('project_id', $project->id)->sum('bobot');
            $request->bobot = (int) str_replace(' %', '', $request->bobot);
            if ($request->bobot > $availableBobot) {
                return redirect()->back()->with(['failed' => 'Maksimum bobot melebihi bobot yang tersedia!']);
            }

            $visualQuality = VisualQualityPort::where('fasilitas', $request->fasilitas)->where('project_id', $project->id)->first();
            if (!$visualQuality) {
                $visualQuality = VisualQualityPort::create([
                    'project_id'    => $project->id,
                    'fasilitas'     => $request->fasilitas,
                    'bobot'         => $request->bobot
                ]);
            }

            $subFacilities = $request->sub_fasilitas ?? [];
            if ($request->fasilitas == 'Facility') {
                $subFacilities = array_merge($subFacilities, array_map(fn ($row) => $row['name'], $request->sub_fasilitas_tambahan));
            }

            if (count($subFacilities) > 0) {
                $subVisualQualities = [];
                $existData  = SubVisualQualityPort::where('visual_quality_port_id', $visualQuality->id)
                    ->whereIn('sub_fasilitas', $subFacilities)
                    ->get()
                    ->pluck('sub_fasilitas')
                    ->toArray();

                foreach ($subFacilities as $sub) {
                    if (!in_array($sub, $existData) && $sub) {
                        $subVisualQuality = SubVisualQualityPort::create([
                            'visual_quality_port_id' => $visualQuality->id,
                            'sub_fasilitas' => $sub
                        ]);

                        $subVisualQuality->periode_visual_qualities()->createMany([['periode' => 1]]);
                    }
                }
            }

            DB::commit();
            return redirect()->route('ipm.port.quality.index', $project->id)->with(['success' => 'Berhasil menambah group ' . $request->fasilitas]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menambah group ' . $request->fasilitas]);
        }
    }

    public function showVisualQuality(Request $request)
    {
        try {
            $data = $request->type == 'SubVisualQuality'
                ? SubVisualQualityPort::find($request->id)
                : VisualQualityPort::find($request->id);

            if (!$data) {
                \Session::flash('failed', 'Data tidak ditemukan!');
                return response('', 500);
            }

            if ($request->type == 'SubVisualQuality') {
                $otherBobot = SubVisualQualityPort::where('visual_quality_port_id', $data->visual_quality_port_id)
                    ->where('id', '!=', $data->id)
                    ->sum('bobot');

                $data = [
                    'sisa_bobot'    => ($data->visual_quality->bobot ?? 0) - $otherBobot,
                    'data'          => $data
                ];
            } else {
                $master = [];
                foreach ($this->facilityGroup()[$data->fasilitas] ?? [] as $key => $value) {
                    if ($value && is_string($value) && !in_array($value, $data->sub_visual_qualities->pluck('sub_fasilitas')->toArray())) {
                        array_push($master, $value);
                    };

                    if ($key && is_string($key) && !in_array($key, $data->sub_visual_qualities->pluck('sub_fasilitas')->toArray())) {
                        array_push($master, $key);
                    };
                }

                $data = [
                    'sisa_bobot'    => 100 - VisualQualityPort::where('project_id', $data->project_id)->sum('bobot'),
                    'data'          => $data,
                    'options'       => $master
                ];
            }

            return response()->json($data);
        } catch (\Exception $e) {
            \Session::flash('failed', 'Gagal menampilkan data!');
            return response('', 500);
        }
    }

    public function destroyVisualQuality(Request $request)
    {
        DB::beginTransaction();

        $data = $request->type == 'SubVisualQuality'
            ? SubVisualQualityPort::find($request->id)
            : VisualQualityPort::find($request->id);

        if (!$data) {
            \Session::flash('failed', 'Data tidak ditemukan!');
            return;
        }

        try {
            $data->delete();
            DB::commit();
            \Session::flash('success', 'Berhasil menghapus ' . $data->fasilitas ?? $data->sub_fasilitas);
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus ' . $data->fasilitas ?? $data->sub_fasilitas);
        }

        return;
    }

    public function updateVisualQuality(Request $request, ProjectMonitoringPort $project)
    {
        DB::beginTransaction();

        $data = $request->type == 'SubVisualQuality'
            ? SubVisualQualityPort::find($request->id)
            : VisualQualityPort::find($request->id);

        if (!$data) {
            \Session::flash('failed', 'Data tidak ditemukan!');
            return;
        }

        try {
            $request->merge([
                'bobot' => (int) str_replace(' %', '', $request->bobot)
            ]);

            if ($request->type == 'SubVisualQuality') {
                $availableBobot = $data->visual_quality->bobot;
                $otherBobot = SubVisualQualityPort::where('visual_quality_port_id', $data->visual_quality_port_id)
                    ->where('id', '!=', $request->id)
                    ->sum('bobot');

                if ($request->bobot > ($availableBobot - $otherBobot)) {
                    return redirect()->back()->with(['failed' => 'Bobot yang diinputkan melebihi bobot yang tersedia!']);
                }
            } else {
                $availableBobot = 100 - VisualQualityPort::where('id', '!=', $request->id)->where('project_id', $project->id)->sum('bobot');
                if ($request->bobot > $availableBobot) {
                    return redirect()->back()->with(['failed' => 'Bobot yang diinputkan melebihi bobot yang tersedia!']);
                }

                $subFacilities = $request->sub_fasilitas ?? [];
                if ($data->fasilitas == 'Facility') {
                    $subFacilities = array_merge($subFacilities, array_map(fn ($row) => $row['name'], $request->sub_fasilitas_tambahan));
                }

                if (count($subFacilities) > 0) {
                    $subVisualQualities = [];
                    $existData  = SubVisualQualityPort::where('visual_quality_port_id', $data->id)
                        ->whereIn('sub_fasilitas', $subFacilities)
                        ->get()
                        ->pluck('sub_fasilitas')
                        ->toArray();

                    foreach ($subFacilities as $sub) {
                        if (!in_array($sub, $existData) && $sub) {
                            $subVisualQuality = SubVisualQualityPort::create([
                                'visual_quality_port_id' => $data->id,
                                'sub_fasilitas' => $sub
                            ]);

                            $subVisualQuality->periode_visual_qualities()->createMany([['periode' => 1]]);
                        }
                    }
                }
            }

            $data->update([
                'bobot' => $request->bobot
            ]);

            DB::commit();
            return redirect()->route('ipm.port.quality.index', $project->id)->with(['success' => 'Berhasil memperbarui ' . $data->fasilitas ?? $data->sub_fasilitas]);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal memperbarui ' . $data->fasilitas ?? $data->sub_fasilitas]);
        }
    }

    public function selectVisualQuality(Request $request, ProjectMonitoringPort $project)
    {
        $stored = VisualQualityPort::with('sub_visual_qualities')->where('fasilitas', $request->type)->where('project_id', $project->id)->first();
        $exists = $stored->sub_visual_qualities ?? collect([]);
        $master = [];
        foreach ($this->facilityGroup()[$request->type] ?? [] as $key => $value) {
            if ($value && is_string($value) && !in_array($value, $exists->pluck('sub_fasilitas')->toArray())) {
                array_push($master, $value);
            };

            if ($key && is_string($key) && !in_array($key, $exists->pluck('sub_fasilitas')->toArray())) {
                array_push($master, $key);
            };
        }

        return response()->json([
            'max_bobot' => 100 - VisualQualityPort::where('fasilitas', '!=', $request->type)->where('project_id', $project->id)->sum('bobot'),
            'bobot' => $stored->bobot ?? 0,
            'options' => $master
        ]);
    }

    public function indexPeriod(Request $request)
    {
        if ($request->is_datatable) {
            $data = PeriodeVisualQualityPort::where('sub_visual_quality_port_id', $request->id)
                ->orderBy('created_at')
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('updated_at', function ($row) {
                    return \Carbon\Carbon::parse($row->updated_at)->format('Y-m-d h:i:s');
                })
                ->addColumn('action', function ($row) {
                    return '
                        <a class="btn btn-icon btn-primary btn-sm btn-detail-period" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                        </a>
                    ';
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        $data = [
            'data' => SubVisualQualityPort::with('visual_quality')->where('id', $request->id)->first()
        ];

        return response()->json($data);
    }

    public function storePeriod(Request $request)
    {
        DB::beginTransaction();

        try {
            $current = PeriodeVisualQualityPort::where('sub_visual_quality_port_id', $request->id)
                ->orderBy('created_at', 'desc')
                ->first();

            $store = PeriodeVisualQualityPort::create([
                'sub_visual_quality_port_id' => $request->id,
                'periode' => ($current->periode ?? 0) + 1
            ]);

            if ($store) {
                $duplicates = ($this->groupModels($request->group))->with('remarks')
                    ->where('periode_visual_quality_port_id', $current->id ?? '')
                    ->get();

                if (!empty($duplicates)) {
                    foreach ($duplicates as $data) {
                        $group = ($this->groupModels($request->group))->create([
                            'visual_quality_port_id'            => $current->sub_visual_quality->visual_quality_port_id,
                            'periode_visual_quality_port_id'    => $store->id,
                            'sub_grup'                          => $data->sub_grup,
                            'kode'                              => $data->kode,
                            'item'                              => $data->item ?? null,
                            'sub_fasilitas'                     => $data->sub_fasilitas ?? null,
                            'area'                              => $data->area ?? null,
                            'evidence'                          => $data->evidence,
                            'jenis_data'                        => $data->jenis_data,
                        ]);

                        if (!empty($data->remarks)) {
                            $group->remarks()->createMany($data->remarks->toArray());
                        }
                    }
                }

                DB::commit();
                return response()->json(['message' => 'Berhasil menambah periode!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal menambah periode!', 'error' => $e->getMessage()], 500);
        }
    }

    public function updatePeriod(Request $request)
    {
        DB::beginTransaction();

        try {
            $period = PeriodeVisualQualityPort::find($request->period_id);
            if (!$period) {
                return response()->json([
                    'message' => 'Data tidak ditemukan'
                ]);
            }

            $period->outstanding = $request->outstanding;
            $period->save();

            DB::commit();
            return response()->json(['message' => 'Berhasil memperbarui outstanding']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal memperbarui outstanding',
                'error' => $e->getMessage()
            ]);
        }
    }

    public function indexItem(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        $type = $period->sub_visual_quality->visual_quality->fasilitas ?? null;
        if (!empty($this->groupModels($type))) {
            $data = $this->groupModels($type)->where('periode_visual_quality_port_id', $period->id)
                ->when($request->jenis_data, function ($query) use ($request) {
                    return $query->where('jenis_data', $request->jenis_data);
                })
                ->orderBy('created_at')
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('evidence', function ($row) {
                    return '
                        <a href="' . $row->evidence . '" class="text text-hover-primary">
                            ' . $this->getFileName($row->evidence) . '
                        </a>
                    ';
                })
                ->addColumn('remarks', function ($row) use ($type) {
                    return '
                        <a class="btn btn-sm btn-icon btn-primary btn-remark" data-type="' . $type . '" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                        </a>
                    ';
                })
                ->addColumn('action', function ($row) use ($type) {
                    $editClass = $type !== 'Facility' ? 'btn-remark-edit' : 'btn-facility-edit';
                    return '
                        <a class="btn btn-sm btn-icon btn-success ' . $editClass . '" type="button" data-type="' . $type . '" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                        </a>
                        <a class="btn btn-sm btn-icon btn-danger btn-remark-delete" type="button" data-type="' . $type . '" data-id="' . $row->id . '" data-jenis="' . $row->jenis_data . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                        </a>
                        <a class="btn btn-sm btn-icon btn-warning btn-remark-duplicate" type="button" data-type="' . $type . '" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen054.svg', 'svg-icon-3') . '
                        </a
                    ';
                })
                ->rawColumns(['action', 'remarks', 'evidence'])
                ->make(true);
        }
    }

    public function storeItem(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        DB::beginTransaction();

        try {
            $type = $period->sub_visual_quality->visual_quality->fasilitas ?? null;
            $path = 'ipm/port/quality/' . $type . '/' . $period->periode;

            $store = $this->groupModels($type)->create([
                'visual_quality_port_id'            => $period->sub_visual_quality->visual_quality_port_id,
                'periode_visual_quality_port_id'    => $period->id,
                'sub_grup'                          => $type,
                'kode'                              => $request->kode,
                'item'                              => $request->item ?? null,
                'sub_fasilitas'                     => $request->sub_fasilitas ?? null,
                'area'                              => $request->area ?? null,
                'evidence'                          => $this->uploadFile($request->file('evidence'), $path),
                'jenis_data'                        => $request->jenis_data,
            ]);

            $remarks = [];
            foreach ($request->remarks as $remark) {
                array_push($remarks, [
                    'remarks' => $remark['remark'],
                    'tanggal' => now()
                ]);
            }
            if (!empty($remarks)) {
                $store->remarks()->createMany($remarks);
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil menambah data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menambah data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function showItem(Request $request)
    {
        try {
            $data = $this->groupModels($request->type)
                ->with([
                    'remarks' => fn ($q) => $q->orderBy('created_at'),
                    'period.sub_visual_quality'
                ])
                ->where('id', $request->id)
                ->orderBy('created_at')
                ->first();

            return response()->json([
                'data' => $data,
                'status' => $this->remarkStatus()
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!', 'error' => $e->getMessage()], 500);
        }
    }

    public function updateItem(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        $data = $this->groupModels($request->type)->find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data yang akan diperbarui tidak ditemukan!'], 400);
        }

        DB::beginTransaction();

        try {
            $beUpdate = [
                'kode'          => $request->kode,
                'item'          => $request->item ?? null,
                'sub_fasilitas' => $request->sub_fasilitas ?? null,
                'area'          => $request->area ?? null,
            ];
            if (!empty($request->evidence)) {
                $path = 'ipm/port/quality/' . $request->type . '/' . $period->periode;
                $beUpdate['evidence'] = $this->uploadFile($request->file('evidence'), $path);
            }

            $data->update($beUpdate);

            if (!empty($request->remarks)) {
                $model = $this->groupModels($request->type . 'Remark');

                $existing_remarks = [];
                foreach ($request->remarks as $rmk) {
                    if ($rmk['remark_id']) {
                        $existing_remarks[] = $rmk['remark_id'];
                    }
                }
                $deletedId = $data->remarks->whereNotIn('id', $existing_remarks)->pluck('id');
                if (!empty($deletedId)) {
                    $model->whereIn('id', $deletedId->toArray())->delete();
                }

                foreach ($request->remarks as $i => $remark) {
                    $fields = [];
                    array_push($fields, [
                        'remarks'           => $remark['remarks'],
                        'tanggal'           => $remark['tanggal'],
                        'catatan_inspeksi'  => $remark['catatan_inspeksi'],
                        'status_plan'       => $remark['status_plan'] ?? null,
                        'status_inspeksi'   => $remark['status_inspeksi'] ?? null,
                    ]);
                    if (!empty($fields[0]['remarks']) && !empty($fields[0]['tanggal']) && !empty($fields[0]['catatan_inspeksi'])) {
                        switch ($request->type) {
                            case 'Concrete':
                                $fields[0]['vqp_concrate_group_id'] = $request->id;
                                array_push($fields, [
                                    'id' => $remark['remark_id'],
                                ]);
                                break;
                            case 'Steel':
                                $fields[0]['vqp_steel_group_id'] = $request->id;
                                array_push($fields, [
                                    'id' => $remark['remark_id'],
                                ]);
                                break;
                            case 'Facility':
                                $fields[0]['vqp_facility_group_id'] = $request->id;
                                array_push($fields, [
                                    'id' => $remark['remark_id'],
                                ]);
                                break;
                            default:
                                throw new \Exception('Undefine type!');
                                break;
                        }

                        $model->updateOrCreate($fields[1], $fields[0]);
                    }
                }
            } else {
                $data->remarks()->delete();
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil memperbarui data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal memperbarui data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function printItem(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        $subVQ  = SubVisualQualityPort::with('visual_quality')
            ->where('id', $period->sub_visual_quality_port_id)
            ->first();

        $inspections = $subVQ->visual_quality->fasilitas != 'Facility'
            ? $this->facilityGroup()[$subVQ->visual_quality->fasilitas][$subVQ->sub_fasilitas]
            : [$subVQ->sub_fasilitas];

        $_inspections = [];
        foreach ($inspections as $key => $inspection) {
            $_inspections[$key]['inspection_name'] = $inspection;
            $_inspections[$key]['visual_quality_name'] = $subVQ->visual_quality->fasilitas;
            $_inspections[$key]['sub_visual_quality_name'] = $subVQ->sub_fasilitas;
            $_inspections[$key]['summaries'] = $this->qualityInspectionProgress($period, $subVQ->visual_quality->fasilitas);
            $_inspections[$key]['inspections'] = $this->groupModels($subVQ->visual_quality->fasilitas)->with('remarks')
                ->where('periode_visual_quality_port_id', $period->id)
                ->where('jenis_data', $key)
                ->orderBy('created_at')
                ->get()
                ->toArray();
        }

        $type = $subVQ->visual_quality->fasilitas ?? null;
        $fileName = 'IPM_QUALITY_PORT_' . trim($project->nama_project) . time();
        $data = [
            'project'       => $project,
            'file_name'     => $fileName,
            'inspections'   => $_inspections,
            'remark_status' => $this->remarkStatus()
        ];
        $pdf = \PDF::loadView('pages.project-monitoring.port.quality.pdf.detail', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    public function duplicateItemToOtherType(Request $request, ProjectMonitoringPort $project, PeriodeVisualQualityPort $period)
    {
        DB::beginTransaction();

        if (empty($request->milestones)) {
            return response()->json([
                'message' => 'Pilih minimal satu milestone tujuan untuk menduplikasi data!',
            ], 400);
        }

        try {
            $sourceData = $this->groupModels($request->type)->with('remarks')
                ->where('periode_visual_quality_port_id', $period->id)
                ->where('jenis_data', $request->jenis_data)
                ->orderBy('created_at')
                ->get();

            foreach ($sourceData as $duplicate) {
                $duplicateRemarks = [];
                $duplicateData = [
                    'visual_quality_port_id'            => $duplicate->visual_quality_port_id,
                    'periode_visual_quality_port_id'    => $duplicate->periode_visual_quality_port_id,
                    'sub_grup'                          => $duplicate->sub_grup,
                    'kode'                              => $duplicate->kode,
                    'item'                              => $duplicate->item,
                    'sub_fasilitas'                     => $duplicate->sub_fasilitas,
                    'area'                              => $duplicate->area,
                    'evidence'                          => $duplicate->evidence,
                ];

                foreach ($duplicate->remarks as $remark) {
                    array_push($duplicateRemarks, [
                        'remarks'           => $remark->remarks,
                        'status_plan'       => $remark->status_plan,
                        'status_inspeksi'   => $remark->status_inspeksi,
                        'catatan_inspeksi'  => $remark->catatan_inspeksi,
                        'tanggal'           => $remark->tanggal,
                    ]);
                }

                foreach ($request->milestones as $milestone) {
                    $duplicateData['jenis_data'] = $milestone;

                    $duplicated = $this->groupModels($request->type)->create($duplicateData);
                    if (!empty($duplicateRemarks)) {
                        $duplicated->remarks()->createMany($duplicateRemarks);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil menduplikasi data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menduplikasi data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function duplicateItemCode(Request $request)
    {
        DB::beginTransaction();

        try {
            $sourceData = $this->groupModels($request->type)->with('remarks')
                ->where('id', $request->id)
                ->first();

            $duplicateRemarks = [];
            $duplicateData = [
                'visual_quality_port_id'            => $sourceData->visual_quality_port_id,
                'periode_visual_quality_port_id'    => $sourceData->periode_visual_quality_port_id,
                'sub_grup'                          => $sourceData->sub_grup,
                'kode'                              => $sourceData->kode,
                'item'                              => $sourceData->item,
                'sub_fasilitas'                     => $sourceData->sub_fasilitas,
                'area'                              => $sourceData->area,
                'evidence'                          => $sourceData->evidence,
                'jenis_data'                        => $sourceData->jenis_data
            ];

            foreach ($sourceData->remarks as $remark) {
                array_push($duplicateRemarks, [
                    'remarks'           => $remark->remarks,
                    'status_plan'       => $remark->status_plan,
                    'status_inspeksi'   => $remark->status_inspeksi,
                    'catatan_inspeksi'  => $remark->catatan_inspeksi,
                    'tanggal'           => $remark->tanggal,
                ]);
            }

            for ($i = 0; $i < $request->qty; $i++) {
                $duplicated = $this->groupModels($request->type)->create($duplicateData);
                if (!empty($duplicateRemarks)) {
                    $duplicated->remarks()->createMany($duplicateRemarks);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil menduplikasi data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menduplikasi data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function indexRemark(Request $request)
    {
        try {
            $data = $this->groupModels($request->type)
                ->with([
                    'remarks' => fn ($q) => $q->orderBy('created_at'),
                    'period.sub_visual_quality'
                ])
                ->where('id', $request->id)
                ->orderBy('created_at')
                ->first();

            return response()->json([
                'data' => $data,
                'status' => $this->remarkStatus()
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong!', 'error' => $e->getMessage()], 500);
        }
    }

    public function destroyRemark(Request $request)
    {
        DB::beginTransaction();

        $data = $this->groupModels($request->type)->find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        try {
            $data->delete();
            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal menghapus data!'], 500);
        }
    }

    public function indexDrawing(Request $request, ProjectMonitoringPort $project)
    {
        $data = DrawingVisualQuality::where('jenis_drawing', 2)
            ->where('project_id', $project->id)
            ->orderBy('tanggal')
            ->get();

        return datatables()
            ->of($data)
            ->addColumn('drawing', function ($row) {
                return '
                    <a href="' . $row->drawing . '" class="text-muted text-hover-primary">
                        ' . $this->getFileName($row->drawing) . '
                    </a>
                ';
            })
            ->addColumn('tanggal', function ($row) {
                return \Carbon\Carbon::parse($row->tanggal)->format('Y-m-d');
            })
            ->addColumn('action', function ($row) {
                return '
                    <a class="btn btn-sm btn-icon btn-success btn-drawing-edit" type="button" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>
                    <a class="btn btn-sm btn-icon btn-danger btn-drawing-delete" type="button" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
            })
            ->rawColumns(['drawing', 'action'])
            ->make(true);
    }

    public function storeDrawing(Request $request, ProjectMonitoringPort $project)
    {
        DB::beginTransaction();

        try {
            if ($request->drawing) {
                $path = 'ipm/port/quality/drawing/' . $project->nama_project;
                DrawingVisualQuality::create([
                    'project_id'    => $project->id,
                    'drawing'       => $this->uploadFile($request->file('drawing'), $path),
                    'deskripsi'     => $request->deskripsi,
                    'tanggal'       => $request->tanggal,
                    'jenis_drawing' => 2 // as port drawing
                ]);

                DB::commit();
                \Session::flash('success', 'Berhasil mengunggah drawing!');
                return;
            }

            return response()->json(['message' => 'Tidak ada data yang diunggah!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal mengunggah file drawing',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function showDrawing(Request $request)
    {
        $data = DrawingVisualQuality::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        return response($data);
    }

    public function updateDrawing(Request $request, ProjectMonitoringPort $project)
    {
        $data = DrawingVisualQuality::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        DB::beginTransaction();

        try {
            $path = 'ipm/port/quality/drawing/' . $project->nama_project;
            $beUpdate = [
                'deskripsi'     => $request->deskripsi,
                'tanggal'       => $request->tanggal,
            ];
            if (!empty($request->drawing)) {
                $beUpdate['drawing'] = $this->uploadFile($request->file('drawing'), $path);
            }

            $data->update($beUpdate);

            DB::commit();
            \Session::flash('success', 'Berhasil mengubah drawing!');
            return;
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal mengubah drawing data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function destroyDrawing(Request $request)
    {
        $data = DrawingVisualQuality::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        DB::beginTransaction();
        try {
            $data->delete();
            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus drawing data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menghapus data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function periodDrawing(Request $request)
    {
        DB::beginTransaction();
        if (empty($request->drawing)) {
            return redirect()->back()->with('failed', 'Tidak ada drawing file untuk diunggah!');
        }

        try {
            $data = PeriodeVisualQualityPort::find($request->period_id);
            if (!$data) {
                throw new \Exception('Data period tidak ditemukan!');
            }

            $path = 'ipm/port/quality/drawing/period';
            $data->update([
                'document_drawing' => $this->uploadFileName($request->file('drawing'), $path)
            ]);

            DB::commit();
            return redirect()->back()->with('success', 'Berhasil mengunggah drawing file.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal menunggah drawing file!');
        }
    }

    private function facilityGroup()
    {
        $insepectionGroup = [
            1 => [
                1 => 'Material Ident.',
                2 => 'Connection Check 2'
            ],
            2 => [
                3 => 'Material Ident.',
                4 => 'Connection Check 1',
                5 => 'Galvanized',
                6 => 'Coating',
                7 => 'Connection Check 2',
            ],
            3 => [
                8 => 'Material Ident.',
                9 => 'Connection Check 1',
                10 => 'PDR',
                11 => 'PDA',
            ],
            4 => [
                12 => 'Material Ident.',
                13 => 'DMF',
                14 => 'Uji Tarik',
                15 => 'Slump Test',
                16 => 'Uji Tekan',
                17 => 'Surface Cond.'
            ],
            5 => [
                18 => 'Material Ident.',
                19 => 'DMF',
                20 => 'Uji Tarik',
                21 => 'Slump Test',
                22 => 'Uji Tekan'
            ],
        ];

        return [
            'Steel' => [
                'Movable Bridge' => $insepectionGroup[2],
                'Catwalk' => $insepectionGroup[2],
                'Tiang Pancang' => $insepectionGroup[3],
                'Frontal Frame' => $insepectionGroup[2],
                'Rubber Fender' => $insepectionGroup[1],
                'Bollard' => $insepectionGroup[1],
                'Railing' => $insepectionGroup[2]
            ],
            'Concrete' => [
                'Abutment' => $insepectionGroup[4],
                'Dudukan Hidrolik' => $insepectionGroup[4],
                'Breasthing Dolphin' => $insepectionGroup[4],
                'Mooring Dolphin' => $insepectionGroup[4],
                'Dolphin Protector' => $insepectionGroup[4],
                'Trestle' => $insepectionGroup[4],
                'Catwalk (Lantai Beton)' => $insepectionGroup[4],
                'Isian Tiang Pancang' => $insepectionGroup[5],
                'Deck on Pile' => $insepectionGroup[4]
            ],
            'Facility' => [
                'Ruang Tunggu',
                'Toilet Ruang Tunggu Pria',
                'Toilet Ruang Tunggu Wanita',
                'Toilet Ruang Tunggu Disabilitas',
                'Toilet Ruang Outdoor Pria',
                'Toilet Ruang Outdoor Wanita',
                'Toilet Ruang Outdoor Disabilitas',
                'Toilet Mushola',
                'Toilet Klinik',
                'Loket Penumpang',
                'Ruang Klinik/Medis',
                'Ruang Laktasi',
                'Mushola',
                'LED Panah Lalu Lintas',
                'Barrirer',
                'Loket Kendaraan dan Kanopi',
                'Tollgate Kendaraan',
                'Alat Pembatas Ketinggian & Lebar Kendaraan',
                'Jembatan Timbang',
                'Access Bridge/Gangway',
                'Marka Jalan',
                'Penerangan Jalan',
                'Gate Gapura',
                'Pagar Sterilisasi',
                'Signage Indoor',
                'Signage Outdoor',
                'Signage Pembatas Area/ Zonasi',
                'Signage Dermaga',
            ]
        ];
    }

    private function remarkStatus()
    {
        return [
            'No Progress',
            'Contractor Progress',
            'Minor/Re-Inspect',
            'Accepted',
        ];
    }

    private function groupModels($type)
    {
        $models = [
            'Steel'             => '\App\Models\ProjectMonitoring\VQPSteelGroup',
            'Concrete'          => '\App\Models\ProjectMonitoring\VQPConcrateGroup',
            'Facility'          => '\App\Models\ProjectMonitoring\VQPFacilityGroup',
            'SteelRemark'       => '\App\Models\ProjectMonitoring\VQPSteelGroupRemark',
            'ConcreteRemark'    => '\App\Models\ProjectMonitoring\VQPConcrateGroupRemark',
            'FacilityRemark'    => '\App\Models\ProjectMonitoring\VQPFacilityGroupRemark',
        ];

        return new $models[$type];
    }

    private function qualityInspectionProgress(PeriodeVisualQualityPort $period, $group)
    {
        $modals = [
            'Concrete' => '\App\Models\ProjectMonitoring\VQPConcrateGroup',
            'Facility' => '\App\Models\ProjectMonitoring\VQPFacilityGroup',
            'Steel' => '\App\Models\ProjectMonitoring\VQPSteelGroup',
        ];

        $groups = (new $modals[$group])->where('periode_visual_quality_port_id', $period->id)
            ->with(['remarks'])
            ->get();

        $remarks = $groups
            ->map(function ($item, $key) {
                return $item->remarks;
            })
            ->flatten(1)
            ->values();

        $count      = @count($remarks);
        $accepted   = $remarks->where('status_inspeksi', 3)->count();
        $minor      = $remarks->where('status_inspeksi', 2)->count();
        $contractor = $remarks->where('status_inspeksi', 1)->count();
        $unprogress = $remarks->where('status_inspeksi', 0)->count();
        $planTotal  = $remarks->where('status_plan', 3)->count() * 100 + $remarks->where('status_plan', 2)->count() * 50;
        $inspTotal  = $remarks->where('status_inspeksi', 3)->count() * 100 + $remarks->where('status_inspeksi', 2)->count() * 50;
        return [
            'data' => [
                [
                    "status" => "accepted",
                    "qty_color" => "bg-success",
                    "qty" => $accepted,
                    "percentage" => $count > 0 ? toPercent(($accepted / $count) * 100, 2) : '0%',
                    "pdf_style" => "text-align: center; background-color:#008200;color:white"
                ],
                [
                    "status" => "minor/re-inspect",
                    "qty_color" => "bg-warning",
                    "qty" => $minor,
                    "percentage" => $count > 0 ? toPercent(($minor / $count) * 100, 2) : '0%',
                    "pdf_style" => "text-align: center; background-color:#ffff00;color:black"
                ],
                [
                    "status" => "contractor progress",
                    "qty_color" => "bg-danger",
                    "qty" => $contractor,
                    "percentage" => $count > 0 ? toPercent(($contractor / $count) * 100, 2) : '0%',
                    "pdf_style" => "text-align: center; background-color:#ff0000;color:white"
                ],
                [
                    "status" => "no progress",
                    "qty_color" => "bg-default",
                    "qty" => $unprogress,
                    "percentage" => $count > 0 ? toPercent(($unprogress / $count) * 100, 2) : '0%'
                ]
            ],
            'summary' => [
                "qty" => $accepted + $minor + $contractor + $unprogress,
                "percentage" => toPercent(100, 2),
                "status" => [
                    "plan" => [
                        "percentage" => $count > 0 ? ($planTotal / ($count * 100)) * 100 : 0
                    ],
                    "actual" => [
                        "percentage" => $count > 0 ? ($inspTotal / ($count * 100)) * 100 : 0
                    ],
                    "deviation" => [
                        "percentage" => $count > 0 ? (($planTotal / ($count * 100)) * 100) - (($inspTotal / ($count * 100)) * 100) : 0
                    ]
                ]
            ]
        ];
    }
}
