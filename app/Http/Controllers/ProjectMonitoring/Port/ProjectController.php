<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Requests\ProjectMonitoring\Port\Project\StoreRequest;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringPort;
use App\Models\ProjectMonitoring\JenisDokumenPort;
use App\Models\ProjectMonitoring\NoticeRecomendationPort;
use App\Models\ProjectMonitoring\PeriodeRecomendationPort;
use App\Models\ProjectMonitoring\ProjectMonitoringPort;
use App\Models\ProjectMonitoring\SubDokumenPort;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.project-monitoring.port.project.mail';
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatable($request);
        }

        $data = [
            'users' => User::asStaff()->selectRaw('id, name')->orderBy('name')->get(),
            'periods' => ['hari', 'minggu', 'bulan']
        ];

        return view('pages.project-monitoring.port.project.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        try {
            $isDuplicated = ProjectMonitoringPort::where('nama_project', $request->nama_project)
                ->where('nomor_kontrak', $request->nomor_kontrak)
                ->exists();
            if ($isDuplicated) {
                DB::rollBack();
                return redirect()->back()->with(['failed' => 'Nama dan nomor project sudah terdaftar!']);
            }

            $storeProject = ProjectMonitoringPort::create($request->validated());

            $this->storeDocument($storeProject);
            $this->storeRecomendation($storeProject);

            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $storeProject->nama_project
                ],
                __('app.notification.ipm.project')
            );

            $param = [];
            $param['users'] = [$request->pic];
            $param['title'] = 'Project Monitoring';
            $param['data'] = $message[0];
            $param['action'] = route('ipm.port.progress.index', $storeProject->id);
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.ipm.port.subject');
            $param['sender'] = Auth::user();
            $param['projectMonitoring'] = $storeProject;
            $this->notification->send($param);

            DB::commit();
            return redirect()->route('ipm.port.index')->with(['success' => 'Berhasil menambah port project!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah port project!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProjectMonitoringPort $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectMonitoringPort $project)
    {
        DB::beginTransaction();

        try {
            $project->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus port project!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus port project!']);
        }
    }

    /**
     * Reopen project status.
     * 
     * @param ProjectMonitoringPort $project
     * @return \Illuminate\Http\Response
     */
    public function reopen(Request $request)
    {
        DB::beginTransaction();

        $project_approval = ApprovalProjectMonitoringPort::find($request->id);
        if (!$project_approval) {
            \Session::flash('failed', 'Gagal membuka kembali project. Approval tidak ditemukan!');
            return;
        }

        try {
            $project_approval->update(['status' => 0]);
            $project_approval->project()->update(['status' => 0]);

            DB::commit();
            \Session::flash('success', 'Berhasil membuka kembali project!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal membuka kembali project!');
        }

        return;
    }

    /**
     * Display data in datatables
     *
     * @param Request $request
     * @return \Yajra\DataTables\DataTables
     */
    private function datatable(Request $request)
    {
        $isAdmin = isAdmin();
        $now = now()->format('Y-m-d h:i:s');
        $progressController = new ProgressController;
        $status = [
            0 => ['class' => 'badge-primary', 'label' => 'On Progress'],
            1 => ['class' => 'badge-success btn-reopen', 'label' => 'Completed'],
        ];
        $data = ProjectMonitoringPort::with('latest_basic_data')->orderBy('created_at')
            ->when($request->jenis_project, function ($query) use ($request) {
                return $query->where('jenis_project', $request->jenis_project);
            })
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('nilai_project', function ($row) {
                return toRupiah($row->nilai_project);
            })
            ->addColumn('contractual', function ($row) use ($now) {
                $duration = strtotime($row->latest_basic_data->selesai_kontrak ?? $now) - strtotime($row->latest_basic_data->mulai_kontrak ?? $now);
                return floor($duration / (60 * 60 * 24));
            })
            ->addColumn('remaining', function ($row) use ($now) {
                $duration = strtotime($row->latest_basic_data->selesai_kontrak ?? $now) - strtotime($now);
                return floor($duration / (60 * 60 * 24));
            })
            ->addColumn('progres', function ($row) {
                return toPercent(0);
            })
            ->addColumn('quality', function ($row) {
                return toPercent(0);
            })
            ->addColumn('cpi', function ($row) use ($progressController) {
                $data = $progressController->index_progres($row)->toArray();
                $last = end($data);
                $cpi = 0;
                if (!empty($last)) {
                    $total = (($last['total_actual'] ?? 0) / 100) * $row->nilai_project;
                    $cpi = $total > 0 ? round($last['actual_cost'] / $total, 2) : 0;
                }

                return $cpi;
            })
            ->addColumn('etc', function ($row) use ($progressController) {
                $data = $progressController->index_progres($row)->toArray();
                $last = end($data);

                return $last['etc'] ?? 0;
            })
            ->addColumn('ncr', function ($row) {
                return NoticeRecomendationPort::whereHas('periode_recomendation', function ($query) use ($row) {
                    $query->where('project_id', $row->id);
                })->count();
            })
            ->addColumn('pic', function ($row) {
                return $row->user->name;
            })
            ->addColumn('status', function ($row) use ($status) {
                $id =  $row->latest_approval ?  $row->latest_approval->id : '';
                return '<span type="button" class="badge badge-outline ' . $status[$row->status]['class'] . '" data-id="' . $id . '">' . __($status[$row->status]['label']) . '</span>';
            })
            ->addColumn('document', function ($row) {
                return '
                <a type="button" class="text-hover-primary btn-document"
                    data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-2x') . '
                </a>
            ';
            })
            ->addColumn('action', function ($row) use ($isAdmin) {
                $button = '
                    <a href="' . route('ipm.port.progress.index', $row->id) . '" class="btn btn-sm btn-light-primary btn-icon">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'fs-1') . '
                    </a>
                ';

                if ($isAdmin) {
                    $button .= '
                        <button type="button"
                            class="btn btn-sm btn-icon btn-light-danger btn-delete"
                            data-id=' . $row->id . '>
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                        </button>
                    ';
                }

                return $button;
            })
            ->rawColumns(['action', 'document', 'status'])
            ->make(true);
    }

    /**
     * Create default document.
     * 
     * @param ProjectMonitoringPort $project
     */
    private function storeDocument(ProjectMonitoringPort $project)
    {
        $storeMainDoc = JenisDokumenPort::create([
            'project_id' => $project->id,
            'jenis_dokumen' => 'Progress Document'
        ]);

        $storeSubDoc = SubDokumenPort::create([
            'jenis_dokumen_id' => $storeMainDoc->id,
            'sub_dokumen' => 'Executive Summary'
        ]);
    }

    /**
     * Create default recomendation.
     * 
     * @param ProjectMonitoringPort $project
     */
    private function storeRecomendation(ProjectMonitoringPort $project)
    {
        PeriodeRecomendationPort::create([
            'project_id' => $project->id,
            'periode' => 1
        ]);
    }
}
