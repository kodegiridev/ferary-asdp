<?php

namespace App\Http\Controllers\ProjectMonitoring\Port;

use App\Http\Controllers\Controller;
use App\Models\ProjectMonitoring\BasicDataProjectPort;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class BasicDataController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $project_id = $request->input('project_id');

        $data = BasicDataProjectPort::query()->with('project.name')->when($project_id, function ($query) use ($project_id) {
            $query->where('project_id', $project_id);
        })->orderByDesc('created_at')->get();

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mulai_kontrak = Carbon::parse($request->mulai_kontrak);
        $selesai_kontrak = Carbon::parse($request->selesai_kontrak);

        DB::beginTransaction();
        try {
            $data['project_id'] = $request->project_id;
            $data['mulai_kontrak'] = $request->mulai_kontrak;
            $data['selesai_kontrak'] = $request->selesai_kontrak;
            $data['jangka_waktu'] = $mulai_kontrak->diffInWeeks($selesai_kontrak);
            $data['status_kontrak'] = $request->status_kontrak;
            BasicDataProjectPort::create($data);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan Basic Data.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menambahkan Basic Data.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BasicDataProjectPort $basic_data
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasicDataProjectPort $basic_data)
    {
        DB::beginTransaction();

        try {
            $basic_data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus Basic Data Project.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus Basic Data Project.']);
        }
        return redirect()->back();
    }
}
