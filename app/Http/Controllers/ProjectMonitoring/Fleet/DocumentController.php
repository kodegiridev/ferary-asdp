<?php

namespace App\Http\Controllers\ProjectMonitoring\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectMonitoring\Fleet\Document\StoreRequest;
use App\Http\Requests\ProjectMonitoring\Fleet\Document\StoreSubRequest;
use App\Http\Requests\ProjectMonitoring\Fleet\Document\UpdateRequest;
use App\Http\Requests\ProjectMonitoring\Fleet\Document\UpdateSubRequest;
use App\Models\ProjectMonitoring\DokumenFleet;
use App\Models\ProjectMonitoring\JenisDokumenFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\ProjectMonitoring\SubDokumenFleet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocumentController extends Controller
{
    private $path = 'ipm/fleet/document';

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectMonitoringFleet $project)
    {
        $jenisDokumen = JenisDokumenFleet::whereProject($project->id)
            ->with(['sub_dokumen_fleets' => fn ($q) => $q->orderBy('id')])
            ->orderBy('id')
            ->get();

        $data = [
            'types' => array_keys($this->projectDocumentType()),
            'documents' => $jenisDokumen,
            'project' => $project,
            'basic_data' => $project->latest_basic_data,
            'calculated' => $this->calculate($jenisDokumen),
        ];

        if ($request->is_project_index ?? false) {
            return $data;
        }

        return view('pages.project-monitoring.fleet.document.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param StoreRequest $request
     * @param ProjectMonitoringFleet $project
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        try {
            $jenisDokumen = JenisDokumenFleet::whereProject($project->id)->whereDoc($request->jenis_dokumen)->first();
            $mergeSubDokumen = array_merge($request->sub_dokumen ?? [], array_map(fn ($row) => $row['nama_dokumen'], $request->dokumen_tambahan));

            if (!$jenisDokumen) {
                $jenisDokumen = JenisDokumenFleet::create([
                    'project_id' => $project->id,
                    'jenis_dokumen' => $request->jenis_dokumen
                ]);
            }

            foreach ($mergeSubDokumen as $dokumen) {
                if ($dokumen != null) {
                    SubDokumenFleet::updateOrCreate(
                        ['jenis_dokumen_id' => $jenisDokumen->id, 'sub_dokumen' => $dokumen],
                        ['sub_dokumen' => $dokumen]
                    );
                }
            }

            DB::commit();
            return redirect()->route('ipm.fleet.document.index', $project->id)->with(['success' => 'Berhasil menambah fleet dokumen!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah fleet dokumen!']);
        }
    }

    /**
     * Show the specified resource.
     * 
     * @param ProjectMonitoringFleet $project
     * @param string $document
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectMonitoringFleet $project, $document)
    {
        $jenisDokumen = JenisDokumenFleet::whereProject($project->id)->whereDoc($document)->first();
        if (!$jenisDokumen) {
            return response($document . ' tidak ditemukan!', 400);
        }

        $availableSubDokumen = $subDokumen = $this->projectDocumentType()[$document];
        $dokumenTambahan = [];
        foreach ($jenisDokumen->sub_dokumen_fleets as $sub) {
            if (in_array($sub->sub_dokumen, $availableSubDokumen)) {
                $key = array_search($sub->sub_dokumen, $subDokumen);
                if ($key !== false) {
                    unset($subDokumen[$key]);
                }
            } else {
                $dokumenTambahan[] = $sub->sub_dokumen;
            }
        }

        $data = [
            'project' => $project,
            'dokumen' => $jenisDokumen,
            'sub_dokumen' => $subDokumen,
            'dokumen_tambahan' => $dokumenTambahan,
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  UpdateRequest $request
     * @param ProjectMonitoringFleet $project
     * @return Response
     */
    public function update(UpdateRequest $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        try {
            $jenisDokumen = JenisDokumenFleet::whereProject($project->id)->whereDoc($request->jenis_dokumen)->first();
            if (!$jenisDokumen) {
                DB::rollBack();
                return redirect()->back()->withInput()->with(['failed' => 'Dokumen tidak ditemukan!']);
            }

            $mergedSubDokumen = array_merge($request->sub_dokumen ?? [], array_column($request->dokumen_tambahan ?? [], 'nama_dokumen'));
            foreach ($mergedSubDokumen as $dokumen) {
                if ($dokumen != null) {
                    SubDokumenFleet::updateOrCreate(
                        ['jenis_dokumen_id' => $jenisDokumen->id, 'sub_dokumen' => $dokumen],
                        ['sub_dokumen' => $dokumen]
                    );
                }
            }

            DB::commit();
            return redirect()->route('ipm.fleet.document.index', $project->id)->with(['success' => 'Berhasil menambah fleet dokumen!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal memperbarui fleet dokumen!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param Request $request
     * @param string $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $project)
    {
        DB::beginTransaction();

        try {
            $model = [
                'JenisDokumenFleet' => new JenisDokumenFleet,
                'SubDokumenFleet' => new SubDokumenFleet
            ];

            $data = $model[$request->type]->where('id', $request->id)->first();
            if (!$data) {
                \Session::flash('failed', 'Fleet dokumen terpilih tidak ditemukan!');
                return;
            }

            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus fleet dokumen!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Fleet dokumen terpilih tidak ditemukan!');
            return response('', 500);
        }
    }

    /**
     * Print index.
     * 
     * @param ProjectMonitoringFleet $project
     * @return \PDF
     */
    public function print(ProjectMonitoringFleet $project)
    {
        $fileName = 'IPM_FLEET_DOCUMENT_' . $project->nama_project . '_' . time();
        $jenisDokumen = JenisDokumenFleet::whereProject($project->id)
            ->with([
                'sub_dokumen_fleets' => function ($query) {
                    $query->with(['dokumen_fleets' => fn ($q) => $q->orderBy('id')])
                        ->orderBy('id');
                }
            ])
            ->orderBy('id')
            ->get();

        $data = [
            'file_name' => $fileName,
            'documents' => $jenisDokumen,
            'project' => $project,
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.fleet.document.pdf.index', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * Display a listing sub of document.
     * 
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return \Illuminate\Http\Response
     */
    public function indexSub(Request $request, ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        if ($request->ajax()) {
            return $this->datatableSub($document);
        }

        $data = [
            'status' => [1 => 'Ada', 0 => 'Tidak Ada'],
            'project' => $project,
            'sub_dokumen' => $document,
            'jenis_dokumen' => $document->jenis_dokumen,
            'is_pic' => $project->pic == auth()->user()->id
        ];

        return view('pages.project-monitoring.fleet.document.detail.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return \Illuminate\Http\Response
     */
    public function storeSub(StoreSubRequest $request, ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        DB::beginTransaction();

        try {
            DokumenFleet::create(array_merge($request->validated(), [
                'sub_dokumen_id' => $document->id,
                'file' => $this->uploadFile($request->file('file'), $this->path)
            ]));

            DB::commit();
            return redirect()->route('ipm.fleet.document.sub.index', [$project->id, $document->id])->with(['success' => 'Berhasil menambah fleet dokumen']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah fleet dokumen']);
        }
    }

    /**
     * Show the specified resource.
     * 
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return \Illuminate\Http\Response
     */
    public function showSub(Request $request, ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        $data = DokumenFleet::find($request->id);
        if (!$data) {
            return response('Dokumen dari ' . $document->sub_dokumen . ' tidak ditemukan!', 400);
        }

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  UpdateSubRequest $request
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return Response
     */
    public function updateSub(UpdateSubRequest $request, ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        DB::beginTransaction();

        $data = DokumenFleet::find($request->id);
        if (!$data) {
            return redirect()->back()->withInput()->with(['failed' => 'Dokumen dari ' . $document->sub_dokumen . ' tidak ditemukan!']);
        }

        try {
            $_request = $request->validated();
            if ($request->file) {
                if ($data->file) {
                    $file = public_path('upload/' . $this->path . '/' . $this->getFileName($data->file));
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }

                $_request['file'] = $this->uploadFile($request->file('file'), $this->path);
            } else {
                unset($_request['file']);
            }

            $data->update($_request);

            DB::commit();
            return redirect()->route('ipm.fleet.document.sub.index', [$project->id, $document->id])->with(['success' => 'Berhasil memperbarui fleet dokumen']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal memperbarui fleet dokumen']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return \Illuminate\Http\Response
     */
    public function destroySub(Request $request, ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        DB::beginTransaction();

        try {
            $data = DokumenFleet::find($request->id);
            if (!$data) {
                \Session::flash('failed', 'Fleet dokumen terpilih tidak ditemukan!');
                return;
            }

            if ($data->file) {
                $file = public_path('upload/' . $this->path . '/' . $this->getFileName($data->file));
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus fleet dokumen!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus fleet dokumen!');
        }
    }

    /**
     * Print detail.
     * 
     * @param ProjectMonitoringFleet $project
     * @param SubDokumenFleet $document
     * @return \PDF
     */
    public function printSub(ProjectMonitoringFleet $project, SubDokumenFleet $document)
    {
        $fileName = 'IPM_FLEET_DETAIL_DOCUMENT_' . $project->nama_project . '_' . time();
        $docFleet = DokumenFleet::where('sub_dokumen_id', $document->id)->orderBy('updated_at')->get();

        $document->jenis_dokumen;
        $data = [
            'file_name' => $fileName,
            'document' => $document,
            'project' => $project,
            'files' => $docFleet,
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.fleet.document.pdf.detail', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * To select sub document.
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectDocumentType(Request $request, ProjectMonitoringFleet $project)
    {
        $data = $this->projectDocumentType()[$request->type] ?? null;

        if (!empty($data)) {
            $stored = (new SubDokumenFleet)
                ->whereHas('jenis_dokumen', function ($query) use ($project, $request) {
                    $query->where('project_id', $project->id)
                        ->where('jenis_dokumen', $request->type);
                })
                ->get()
                ->pluck('sub_dokumen')
                ->all();

            $temp = [];
            foreach ($data as $value) {
                if (!in_array($value, $stored)) {
                    $temp[] = $value;
                }
            }

            $data = $temp;
        }

        return response()->json($data);
    }

    /**
     * Display data detail document in datatables
     *
     * @param SubDokumenFleet $document
     * @return \Yajra\DataTables\DataTables
     */
    private function datatableSub(SubDokumenFleet $document)
    {
        $data = DokumenFleet::where('sub_dokumen_id', $document->id)->orderBy('updated_at')->get();
        $isPic = $document->jenis_dokumen->project->pic == auth()->user()->id;

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('yes', function ($row) {
                return !$row->status_pemenuhan
                    ? null
                    : theme()->getSvgIcon('icons/duotune/arrows/arr085.svg', 'svg-icon-1');
            })
            ->addColumn('no', function ($row) {
                return $row->status_pemenuhan
                    ? null
                    : theme()->getSvgIcon('icons/duotune/arrows/arr085.svg', 'svg-icon-1');
            })
            ->addColumn('file', function ($row) {
                return '
                    <a href="' . $row->file . '" class="text text-hover-primary">
                        ' . $this->getFileName($row->file) . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) use ($isPic) {
                $button = !$isPic ? '' : '
                    <button type="button"
                            class="btn btn-sm btn-icon btn-primary btn-edit"
                            data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'fs-1') . '
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-danger btn-delete"
                        data-id="' . $row->id . '"
                    >
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                    </button>
                ';

                return $button;
            })
            ->rawColumns(['action', 'yes', 'no', 'file'])
            ->make(true);
    }

    /**
     * Define project document & sub-document type.
     * 
     * @param string
     * @return array
     */
    private function projectDocumentType()
    {
        return [
            'Project Basic' => [
                'Master Schedule',
                'Persetujuan',
                'Technical Specification',
                'Project Contract',
                'Consultant/MK Contract',
            ],
            'Protocol Document' => [
                'Protocol',
                'Appendix Protocol'
            ],
            'Engineering Document' => [
                'Detail Engineering Design',
                'As Built Drawing',
                'Approval/Shop Drawing',
            ],
            'Quality Document' => [
                'Material Approval & Certificate',
                'Inspection Testing Plan (ITP)',
                'Testing Procedure',
                'WPS',
            ],
            'Progress Document' => [
                'Executive Summary',
                'Monthly Progress Report',
                'Monthly Quality Report',
            ]
        ];
    }

    /**
     * Calculate progress.
     * 
     * @param array<object>
     * @return array
     */
    private function calculate($documents)
    {
        $calculates = [];
        $total = 0;
        if (count($documents) > 0) {
            $maxDoc = (1 / count($documents)) * 100;
            foreach ($documents as $doc) {
                $countSub = count($doc->sub_dokumen_fleets);
                if ($countSub > 0) {
                    $maxSub = (1 / $countSub) * $maxDoc;
                    foreach ($doc->sub_dokumen_fleets as $sub) {
                        $dokumen = DokumenFleet::where('sub_dokumen_id', $sub->id);
                        $countDoc = $dokumen->count();
                        if ($countDoc > 0) {
                            $current = (clone $dokumen)->latest()->first();
                            $count = ((clone $dokumen)->statusYes()->count() / $countDoc) * $maxSub;

                            $total += $count;
                            $calculates[$doc->id][$sub->id] = [
                                'updated_at' => \Carbon\Carbon::parse($current->updated_at ?? $current->created_at)->format('Y-m-d h:i:s'),
                                'percentage' => toPercent($count, 2)
                            ];
                        }
                    }
                }
            }
        }

        return [
            'total' => toPercent($total, 2),
            'sub_dokumen' => $calculates
        ];
    }
}
