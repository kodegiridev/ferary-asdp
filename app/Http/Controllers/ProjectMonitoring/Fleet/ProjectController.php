<?php

namespace App\Http\Controllers\ProjectMonitoring\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Requests\ProjectMonitoring\Fleet\Project\StoreRequest;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringFleet;
use App\Models\ProjectMonitoring\JenisDokumenFleet;
use App\Models\ProjectMonitoring\NoticeRecomendationFleet;
use App\Models\ProjectMonitoring\PeriodeRecomendationFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\ProjectMonitoring\SubDokumenFleet;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.project-monitoring.fleet.project.mail';
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatable();
        }

        $data = [
            'users' => User::asStaff()->selectRaw('id, name')->orderBy('name')->get(),
            'periods' => ['hari', 'minggu', 'bulan']
        ];

        return view('pages.project-monitoring.fleet.project.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        try {
            $isDuplicated = ProjectMonitoringFleet::where('nama_project', $request->nama_project)
                ->where('nomor_kontrak', $request->nomor_kontrak)
                ->exists();
            if ($isDuplicated) {
                DB::rollBack();
                return redirect()->back()->with(['failed' => 'Nama dan nomor project sudah terdaftar!']);
            }

            $storeProject = ProjectMonitoringFleet::create($request->validated());

            $this->storeDocument($storeProject);
            $this->storeRecomendation($storeProject);

            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $storeProject->nama_project
                ],
                __('app.notification.ipm.project')
            );

            $param = [];
            $param['users'] = [$request->pic];
            $param['title'] = 'Project Monitoring';
            $param['data'] = $message[0];
            $param['action'] = route('ipm.fleet.progress.index', $storeProject->id);
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.ipm.fleet.subject');
            $param['sender'] = Auth::user();
            $param['projectMonitoring'] = $storeProject;
            $this->notification->send($param);

            DB::commit();
            return redirect()->route('ipm.fleet.index')->with(['success' => 'Berhasil menambah fleet project!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah fleet project!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param ProjectMonitoringFleet $project
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectMonitoringFleet $project)
    {
        return $project;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ProjectMonitoringFleet $project
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();
        try {
            $data['nama_project'] = $request->nama_project;
            $data['nilai_project'] = $request->nilai_project;
            $data['pic'] = $request->pic;
            $data['nomor_kontak'] = $request->nomor_kontak;
            $data['supervisor'] = $request->supervisor;

            $project->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ipm.fleet.index')->with(['failed' => 'Gagal Ubah Data Project.']);
        }

        return redirect()->route('ipm.fleet.index')->with(['success' => 'Berhasil Ubah Data Project.']);
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProjectMonitoringFleet $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        try {
            $project->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus fleet project!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus fleet project!']);
        }
    }

    /**
     * Reopen project status.
     * 
     * @param ProjectMonitoringFleet $project
     * @return \Illuminate\Http\Response
     */
    public function reopen(Request $request)
    {
        DB::beginTransaction();

        $project_approval = ApprovalProjectMonitoringFleet::find($request->id);
        if (!$project_approval) {
            \Session::flash('failed', 'Gagal membuka kembali project. Approval tidak ditemukan!');
            return;
        }

        try {
            $project_approval->update(['status' => 0]);
            $project_approval->project()->update(['status' => 0]);

            DB::commit();
            \Session::flash('success', 'Berhasil membuka kembali project!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal membuka kembali project!');
        }

        return;
    }

    /**
     * Display data in datatables
     *
     * @return \Yajra\DataTables\DataTables
     */
    private function datatable()
    {
        $isAdmin = isAdmin();
        $now = now()->format('Y-m-d h:i:s');
        $progressController = new ProgressController;
        $data = ProjectMonitoringFleet::with('latest_basic_data')->orderBy('created_at')->get();
        $status = [
            0 => ['class' => 'badge-primary', 'label' => 'On Progress'],
            1 => ['class' => 'badge-success btn-reopen', 'label' => 'Completed'],
        ];

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('nilai_project', function ($row) {
                return toRupiah($row->nilai_project);
            })
            ->addColumn('contractual', function ($row) use ($now) {
                $duration = strtotime($row->latest_basic_data->selesai_kontrak ?? $now) - strtotime($row->latest_basic_data->mulai_kontrak ?? $now);
                return floor($duration / (60 * 60 * 24));
            })
            ->addColumn('remaining', function ($row) use ($now) {
                $duration = strtotime($row->latest_basic_data->selesai_kontrak ?? $now) - strtotime($now);
                return floor($duration / (60 * 60 * 24));
            })
            ->addColumn('progres', function ($row) use ($progressController) {
                $data = $progressController->progres_data($row);
                return toPercent($data['total_actual'], 2);
            })
            ->addColumn('quality', function ($row) {
                return toPercent(0);
            })
            ->addColumn('cpi', function ($row) use ($progressController) {
                $data = $progressController->index_progres($row)->toArray();
                $last = end($data);
                $cpi = 0;
                if (!empty($last)) {
                    $total = (($last['total_actual'] ?? 0) / 100) * $row->nilai_project;
                    $cpi = $total > 0 ? round($last['actual_cost'] / $total, 2) : 0;
                }

                return $cpi;
            })
            ->addColumn('etc', function ($row) use ($progressController) {
                $data = $progressController->index_progres($row)->toArray();
                $last = end($data);

                return $last['etc'] ?? 0;
            })
            ->addColumn('ncr', function ($row) {
                return NoticeRecomendationFleet::whereHas('periode_recomendation', function ($query) use ($row) {
                    $query->where('project_id', $row->id);
                })->count();
            })
            ->addColumn('pic', function ($row) {
                return $row->user->name;
            })
            ->addColumn('status', function ($row) use ($status) {
                $id =  $row->latest_approval ?  $row->latest_approval->id : '';
                return '<span type="button" class="badge badge-outline ' . $status[$row->status]['class'] . '" data-id="' . $id . '">' . __($status[$row->status]['label']) . '</span>';
            })
            ->addColumn('document', function ($row) {
                return '
                    <a type="button" class="text-hover-primary btn-document"
                        data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-2x') . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) use ($isAdmin) {
                $button = '
                    <a href="' . route('ipm.fleet.progress.index', $row->id) . '" class="btn btn-sm btn-light-primary btn-icon">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'fs-1') . '
                    </a>
                ';

                if ($isAdmin) {
                    $button .= '
                        <button type="button"
                            class="btn btn-sm btn-icon btn-light-danger btn-delete"
                            data-id=' . $row->id . '>
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                        </button>
                    ';
                }

                return $button;
            })
            ->rawColumns(['action', 'document', 'status'])
            ->make(true);
    }

    /**
     * Create default document.
     * 
     * @param ProjectMonitoringFleet $project
     */
    private function storeDocument(ProjectMonitoringFleet $project)
    {
        $storeMainDoc = JenisDokumenFleet::create([
            'project_id' => $project->id,
            'jenis_dokumen' => 'Progress Document'
        ]);

        $storeSubDoc = SubDokumenFleet::create([
            'jenis_dokumen_id' => $storeMainDoc->id,
            'sub_dokumen' => 'Executive Summary'
        ]);
    }

    /**
     * Create default recomendation.
     * 
     * @param ProjectMonitoringFleet $project
     */
    private function storeRecomendation(ProjectMonitoringFleet $project)
    {
        PeriodeRecomendationFleet::create([
            'project_id' => $project->id,
            'periode' => 1
        ]);
    }
}
