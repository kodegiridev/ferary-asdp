<?php

namespace App\Http\Controllers\ProjectMonitoring\Fleet;

use App\Http\Controllers\Controller;
use App\Models\MasterQuality;
use App\Models\ProjectMonitoring\EvidenceProgresFleet;
use App\Models\ProjectMonitoring\MainItemProjectFleet;
use App\Models\ProjectMonitoring\ProgresProjectFleet;
use App\Models\ProjectMonitoring\SubItemProjectFleet;
use App\Models\ProjectMonitoring\TaskItemProjectFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\ProjectMonitoring\BasicDataProjectFleet;
use App\Models\ProjectMonitoring\MilestoneQualityFleet;
use App\Models\ProjectMonitoring\PeriodeVisualQualityFleet;
use App\Models\ProjectMonitoring\VisualQualityFleet;
use App\Models\ProjectMonitoring\InspectionAcceptenceRateFleet;
use DB;
use Illuminate\Http\Request;
use Session;
use App\Models\User;
use Carbon\Carbon;
use Schema;
use App\Models\ProjectMonitoring\DrawingVisualQuality;
use App\Models\ProjectMonitoring\Fleet\DrawingVisualQualityFleet;

class QualityController extends Controller
{
    public $plan;
    public $inspection;
    public $blasting = [1, 2, 3, 4, 5, 6, 7];
    public $propultion = [1, 2, 3, 4, 5, 6, 7];
    public $hcpw = [1, 2];

    function __construct()
    {
        $this->plan = [
            [
                "id" => 0,
                "name" => "No Progress",
                "value" => 0
            ],
            [
                "id" => 1,
                "name" => "Accepted",
                "value" => 100
            ],
            [
                "id" => 2,
                "name" => "Inspection Invitation",
                "value" => 50
            ],
            [
                "id" => 3,
                "name" => "Internal Progress Yard",
                "value" => 25
            ],
        ];

        $this->inspection = [
            [
                "id" => 0,
                "name" => "No Progress",
                "value" => 0
            ],
            [
                "id" => 1,
                "name" => "Accepted",
                "value" => 100
            ],
            [
                "id" => 2,
                "name" => "Re-Inspect / Partial",
                "value" => 50
            ],
            [
                "id" => 3,
                "name" => "Yard Progress",
                "value" => 25
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectMonitoringFleet $project)
    {
        $periode = $request->input('periode_id') ?? 1;
        if ($request->ajax()) {
            // return $this->datatable($project);
        }
        $index_quality = $this->index_quality_by_periode($project->id, $periode);
        $data = [
            'project_data_group'    => $this->dataDummyProjects(),
            'project'               => $project,
            'milestone_progress'    => $this->averageMilestoneProgress($project),
            'basic_data'            => $project->latest_basic_data,
            'data_group'            => $index_quality['data'],
            'summary_total'         => $index_quality['total_bobot'],
            'summary_actual'        => $index_quality['total_actual'],
            'summary_plan'          => $index_quality['total_plan'],
            'summary_deviasi'       => $index_quality['total_deviasi'],
            'max_periode'           => $this->getMaxPeriode($project->id) ?? 0,
            'drawing'               => DrawingVisualQualityFleet::isFleet()->lastDate()->onProject($project->id)->first(),
            'current_periode'       => $periode
        ];
        // dd($data);
        return view('pages.project-monitoring.fleet.quality.index', compact('data'));
    }

    public function indexMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        if ($request->ajax()) {
            return $this->datatableMilestone($request, $project);
        }

        $data = [
            'project' => $project
        ];

        return view('pages.project-monitoring.fleet.quality.index_milestone', compact('data'));
    }

    public function storeMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        $type = MilestoneQualityFleet::$types[$request->tipe_milestone] ?? 'Data';

        try {
            if (in_array($request->tipe_milestone, [1, 2, 3, 4, 5, 6, 7])) {
                MilestoneQualityFleet::create([
                    'project_id'        => $project->id,
                    'tipe_milestone'    => $request->tipe_milestone,
                    'item_inspeksi'     => $request->item_inspeksi,
                    'task_list'         => $request->task_list,
                ]);

                DB::commit();
                return response()->json(['message' => 'Berhasil Menambah ' . $type]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal Menambah ' . $type, 'error' => $e->getMessage()], 500);
        }
    }

    public function showMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        $data = MilestoneQualityFleet::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        return response()->json($data);
    }

    public function updateMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        $milestone = MilestoneQualityFleet::where('id', $request->id)->where('tipe_milestone', $request->tipe_milestone)->first();
        if (!$milestone) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        DB::beginTransaction();

        $type = MilestoneQualityFleet::$types[$request->tipe_milestone] ?? 'Data';

        try {
            $data = [
                'item_inspeksi' => $request->item_inspeksi,
                'task_list'     => $request->task_list,
                'progres'       => (int) str_replace(' %', '', $request->progres),
                'remarks'       => $request->remarks,
            ];
            if (!empty($request->file('evidence'))) {
                $path = 'ipm/fleet/quality/milestone/' . preg_replace("/\s+/", "", $type);
                $data['evidence'] = $this->uploadFile($request->file('evidence'), $path);
            }

            $milestone->update($data);

            DB::commit();
            return response()->json(['message' => 'Berhasil Mengubah ' . $type]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal Mengubah ' . $type, 'error' => $e->getMessage()], 500);
        }
    }

    public function destroyMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        $type = MilestoneQualityFleet::$types[$request->tipe_milestone] ?? 'Data';

        try {
            $data = MilestoneQualityFleet::where('id', $request->id)->where('tipe_milestone', $request->tipe_milestone)->first();;
            if (!$data) {
                return response()->json(['message' => 'Data tidak ditemukan!'], 400);
            }

            $data->delete();

            DB::commit();
            return response()->json(['message' => 'Berhasil Menghapus ' . $type]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal Menghapus ' . $type, 'error' => $e->getMessage()], 500);
        }
    }

    public function averageMilestoneProgress(ProjectMonitoringFleet $project)
    {
        $data = [];
        foreach (MilestoneQualityFleet::$types as $key => $type) {
            $progress = MilestoneQualityFleet::where('project_id', $project->id)
                ->where('tipe_milestone', $key)
                ->get();

            $total = 0;
            if ($progress->count() > 0) {
                $total = $progress->sum('progres') / $progress->count();
            }

            $data[\Illuminate\Support\Str::snake($type)] = $total;
        }

        return $data;
    }

    public function datatableMilestone(Request $request, ProjectMonitoringFleet $project)
    {
        $data = MilestoneQualityFleet::where('project_id', $project->id)
            ->where('tipe_milestone', $request->tipe_milestone)
            ->orderBy('created_at')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('progres', function ($row) {
                return toPercent($row->progres);
            })
            ->addColumn('updated_at', function ($row) {
                return \Carbon\Carbon::parse($row->updated_at)->format('Y-m-d h:i:s');
            })
            ->addColumn('evidence', function ($row) {
                return '
                    <a href="' . $row->evidence . '" class="text text-hover-primary">
                        ' . $this->getFileName($row->evidence) . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) use ($request) {
                $button = '
                    <a class="btn btn-sm btn-success btn-icon btn-edit" data-id="' . $row->id . '" data-type="' . $request->tipe_milestone . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-1') . '
                    </a>

                    <a class="btn btn-sm btn-danger btn-icon btn-delete" data-id="' . $row->id . '" data-type="' . $request->tipe_milestone . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-1') . '
                    </a>
                ';
                return $button;
            })
            ->rawColumns(['action', 'evidence'])
            ->make(true);
    }

    public function detail(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        $visual = VisualQualityFleet::where('id', $periode->visual_quality_fleet_id)->first();
        $data = [
            'project'       => $project,
            'visual'        => $visual,
            'periode'       => $periode,
            'quality_inspection'    => $this->qualityInspectionProgress(),
            'detail' => $this->detail_grup($periode, $visual),
            'mapping_status'    => $this->mappingStatus(),
            'drawing'       => DrawingVisualQualityFleet::isFleet()->lastDate()->onProject($project->id)->first()
        ];
        $path_view = $data['detail']['flag']->jenis_project . "/" . $data['detail']['flag']->model . "/";
        return view('pages.project-monitoring.fleet.quality.' . $path_view . "detail", compact('data', 'path_view'));
    }

    public function detail_print(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        $visual = VisualQualityFleet::where('id', $periode->visual_quality_fleet_id)->first();
        $data = [
            'project'       => $project,
            'visual'        => $visual,
            'periode'       => $periode,
            'quality_inspection'    => $this->qualityInspectionProgress(),
            'detail' => $this->detail_grup($periode, $visual),
            'mapping_status'    => $this->mappingStatus()
        ];
        $path_view = $data['detail']['flag']->jenis_project . "/" . $data['detail']['flag']->model . "/";
        $pdf = \PDF::loadView('pages.project-monitoring.fleet.quality.' . $path_view . 'print', compact('data', 'path_view'));
        return $pdf->stream('IPM_QUALITY_FLEET_' . now()->unix() . '.pdf');
    }

    private function qualityInspectionProgress()
    {

        $quality_inspection_progress = [
            ["status" => "accepted", "qty_color" => "bg-success", "qty" => 0, "percentage" => 0.00],
            ["status" => "re-inspect/partial", "qty_color" => "bg-warning", "qty" => 0, "percentage" => 0.00],
            ["status" => "yard progress", "qty_color" => "bg-danger", "qty" => 0, "percentage" => 0.00],
            ["status" => "accepted", "qty_color" => "bg-default", "qty" => 1, "percentage" => 0.00],
        ];

        return $quality_inspection_progress;
    }

    private function dataDummyProjects()
    {
        $sub_project = $this->dataDummySubProjects();
        $dataNBP = [
            "id"    => "nb",
            "name"  => "New Building Project",
            "sub_project"   => $sub_project["NBP"],
        ];
        $dataRP = [
            "id"    => "rp",
            "name"  => "Rehabilitation Project",
            "sub_project"   => $sub_project["RP"],
        ];
        return ["nb" => $dataNBP, "rp" => $dataRP];
    }

    private function dataDummySubProjects()
    {
        $NBP    = MasterQuality::where('jenis_project', 'nb')->get();
        $RP     = MasterQuality::where('jenis_project', 'rp')->get();

        return ["NBP" => $NBP, "RP" => $RP];
    }

    public function index_quality_by_periode($id, $periode)
    {
        try {
            $data = VisualQualityFleet::where('project_id', $id)->get();
            $total_bobot = 0;
            $summary_plan = 0;
            $summary_actual = 0;
            $summary_deviasi = 0;

            foreach ($data as $key => $value) {
                if ($value) {
                    $progres_status = 'On Progress';
                    $planCount = $this->countPlan($value, $periode);
                    $inspeksiCount = $this->countInspeksi($value, $periode);
                    $total_plan = $planCount['total_plan'];
                    $quantity = $planCount['total_quantity'];

                    if ($total_plan > 0) {
                        $data[$key]->plan = $total_plan / $quantity * ($value->bobot / 100);
                    } else {
                        $data[$key]->plan = 0;
                    }

                    if ($inspeksiCount['total_inspeksi'] > 0) {
                        $data[$key]->actual = $inspeksiCount['total_inspeksi'] / $inspeksiCount['total_quantity'] * ($value->bobot / 100);
                        $data[$key]->deviasi = $value->actual - $value->plan;
                    } else {
                        $data[$key]->actual = 0;
                        $data[$key]->deviasi = 0;
                    }

                    if ($value->plan > 0 && $value->actual > 0) {
                        if ($value->bobot == $value->plan && $value->bobot == $value->actual) {
                            $progres_status = 'Accepted';
                        }
                    }

                    $data[$key]->progres_status = $progres_status;

                    //Periode

                    $dataPeriode = PeriodeVisualQualityFleet::when('periode', function ($query) use ($periode) {
                        $query->where('periode', $periode);
                    })->where('visual_quality_fleet_id', $value->id)
                        ->get();

                    $data[$key]->periode = $dataPeriode;

                    $total_bobot = $total_bobot + $value->bobot;
                    $summary_plan = $summary_plan + $value->plan;
                    $summary_actual = $summary_actual + $value->actual;
                    $summary_deviasi = $summary_deviasi + $value->deviasi;
                }
            }

            $data = $this->inspection_index($data);
            $response['data'] = $data;
            $response['total_bobot'] = $total_bobot;
            $response['total_plan'] = $summary_plan;
            $response['total_actual'] = $summary_actual;
            $response['total_deviasi'] = $summary_deviasi;

            return $response;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function store_quality(Request $request)
    {
        $data['project_id'] = $request->input('project_id');
        $data['status_grup'] = $request->input('jenis_form');
        $data['nama_grup'] = $request->input('nama_grup');
        $data['jenis_project'] = $request->input('jenis_project');
        $data['jenis_form'] = $request->input('jenis_form');
        DB::beginTransaction();
        try {
            $visual = VisualQualityFleet::where('jenis_project', $request->jenis_project)
                ->where('project_id', $request->project_id)
                ->where('jenis_form', $request->jenis_form)->first();

            if ($visual) {
                Session::flash('failed', 'Data grup sudah dibuat.');
                return redirect()->back();
            }
            $visual = VisualQualityFleet::create($data);
            // dd($data, $visual);
            PeriodeVisualQualityFleet::create([
                'periode' => 1,
                'visual_quality_fleet_id' => $visual->id,
                'status_progres' => 1
            ]);

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan grup Visual Quality');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollBack();
            // dd($th->getMessage());
            Session::flash('failed', 'Gagal menambahkan grup Visual Quality.');
            return redirect()->back();
        }
    }

    public function delete_quality(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();
        try {
            $visual = VisualQualityFleet::where('id', $request->visual_quality_id)->first();

            if (!$visual) {
                Session::flash('failed', 'Data tidak ditemukan.');
                return redirect()->back()->with(['failed' => 'Data visual quality group tidak ditemukan']);
            }
            $visual->delete();
            DB::commit();
            Session::flash('success', 'Berhasil menghapus grup Visual Quality');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menghapus grup Visual Quality.');
            return redirect()->back()->with(['failed' => $th->getMessage()]);
        }
    }

    public function store_periode(Request $request, ProjectMonitoringFleet $project)
    {
        $visual_id = $request->input('visual_quality_id');
        $latest_periode = PeriodeVisualQualityFleet::with('visual_quality')->where('visual_quality_fleet_id', $visual_id)->latest()->first();
        if (!$latest_periode) {
            Session::flash('failed', 'Periode sebelumnya tidak ditemukan');
            return redirect()->back();
        }

        try {
            $new_periode = PeriodeVisualQualityFleet::create([
                'visual_quality_fleet_id' => $visual_id,
                'periode' => $latest_periode->periode + 1,
                'status_progres' => 1
            ]);
            Session::flash('success', 'Berhasil menambahkan grup Visual Quality.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Session::flash('failed', 'Gagal menambahkan grup Visual Quality');
            return redirect()->back();
        }
    }

    /**
     * Update bobot group Visual Quality.
     *
     * @param VisualQualityFleet $visual
     * @return \Illuminate\Http\Response
     */
    public function store_bobot(Request $request, VisualQualityFleet $visual)
    {
        $data['bobot'] = $request->input('bobot');
        DB::beginTransaction();
        try {
            $visual->bobot = $data['bobot'];
            $visual->save();

            DB::commit();
            Session::flash('success', 'Berhasil menambahkan bobot Visual Quality');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal menambahkan bobot Visual Quality');
            return redirect()->back();
        }
    }

    public function store_group_remarks(Request $request)
    {
        $flag = json_decode($request->flag);
        $model_main_name = $flag->model;
        $model = $this->getModelByName($flag);
        if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
            $flag_detail        = $flag;
            $flag_detail->model = $flag_detail->model . 'Remarks';
            $model_remarks      = $this->getModelByName($flag_detail);
            $table_name         = $model_remarks->getTable();
            $columns = Schema::getColumnListing($table_name);
            // array_shift($columns);
            // array_pop($columns);
            // array_pop($columns);
            if ($model_main_name == 'VqfElectricalOutfiting') {
                $fk_id = 'vqf_eo_id';
            } else if ($model_main_name == 'VqfMachineryOutfiting') {
                $fk_id = 'vqf_mo_id';
            } else if ($model_main_name == 'VqfPropultionNb') {
                $fk_id = 'vqf_propultion_id';
            } else {
                $fk_id = $model->getForeignKey();
            }
            $columns_data_type = DB::table('INFORMATION_SCHEMA.COLUMNS')
                ->select('DATA_TYPE')
                ->where('TABLE_NAME', $table_name)
                ->get()->toArray();
            // array_shift($columns_data_type);
            // array_pop($columns_data_type);
            // array_pop($columns_data_type);
        }
        DB::beginTransaction();
        try {

            $evidence = "";
            if ($request->has('evidence')) {
                $file = $request->evidence;
                $evidence = "-";
                if (!empty($file)) {
                    $filename = $this->uploadFileName($file, 'project-monitoring-fleet/quality-detail');
                    if ($filename) {
                        $evidence = $filename;
                    }
                }
            }

            if ($request->has('main')) {
                $insert = $request->main;
            } else {
                $insert = $request->all();
            }

            if (!empty($evidence)) {
                $insert["evidence"] = $evidence;
            }


            $model = $model->create($insert);
            if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
                $detail = $request->detail;
                if (!empty($detail)) {
                    $column_arr     = $columns;
                    $total_data     = 0;
                    foreach ($detail as $column => $arr_data) {
                        $total_data = count($arr_data);
                        break;
                    }

                    if ($total_data > 0) {
                        for ($i = 0; $i < $total_data; $i++) {
                            $insert = [];
                            foreach ($column_arr as $index => $column_name) {
                                $data_type = $columns_data_type[$index]->DATA_TYPE;
                                $return_null_data = $this->return_datatype_null($data_type);
                                if ($column_name == $fk_id) {
                                    $insert[$column_name] = $model->id;
                                    continue;
                                }
                                $value = $return_null_data;
                                if (!empty($detail[$column_name][$i])) {
                                    $value = $detail[$column_name][$i];
                                }
                                $insert[$column_name] = $value;
                            }
                            $model_remarks = $this->getModelByName($flag_detail);
                            $model_remarks->create($insert);
                        }
                    }
                }
            }


            DB::commit();
            Session::flash('success', 'Berhasil menambahkan data detail Visual Quality');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            Session::flash('failed', 'Gagal menambahkan data detail Visual Quality');
            return redirect()->back();
        }
    }

    public function return_datatype_null($data_type)
    {
        $integer_list   = ["bigint", "int", "tinyint"];
        $string_list    = ["varchar", "text"];
        $date_list      = ["date"];
        if (in_array($data_type, $integer_list)) {
            return 0;
        } elseif (in_array($data_type, $string_list)) {
            return "";
        } elseif (in_array($data_type, $date_list)) {
            return "0000-00-00";
        }
        return false;
    }

    public function update_group_remarks(Request $request)
    {
        $flag = json_decode($request->flag);
        $model_main_name = $flag->model;
        $model = $this->getModelByName($flag);
        if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
            $flag_detail        = $flag;
            $flag_detail->model = $flag_detail->model . 'Remarks';
            $model_remarks      = $this->getModelByName($flag_detail);
            $table_name         = $model_remarks->getTable();
            $columns = Schema::getColumnListing($table_name);

            // array_shift($columns);
            // array_pop($columns);
            // array_pop($columns);
            if ($model_main_name == 'VqfElectricalOutfiting') {
                $fk_id = 'vqf_eo_id';
            } else if ($model_main_name == 'VqfMachineryOutfiting') {
                $fk_id = 'vqf_mo_id';
            } else if ($model_main_name == 'VqfPropultionNb') {
                $fk_id = 'vqf_propultion_id';
            } else {
                $fk_id = $model->getForeignKey();
            }

            $columns_data_type = DB::table('INFORMATION_SCHEMA.COLUMNS')
                ->select('DATA_TYPE')
                ->where('TABLE_NAME', $table_name)
                ->get()->toArray();
            // array_shift($columns_data_type);
            // array_pop($columns_data_type);
            // array_pop($columns_data_type);
        }

        DB::beginTransaction();
        try {
            $model_id = (int) $request->id;

            $evidence = "";
            if ($request->has('evidence')) {
                $file = $request->evidence;
                $evidence = "-";
                if (!empty($file)) {
                    $filename = $this->uploadFileName($file, 'project-monitoring-fleet/quality-detail');
                    if ($filename) {
                        $evidence = $filename;
                    }
                }
            }

            if ($request->has('main')) {
                $update = $request->main;
            } else {
                $update = $request->all();
            }

            if (!empty($evidence)) {
                $update["evidence"] = $evidence;
            }

            $model = $model->where('id', $model_id)->update($update);

            if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
                $model_remark_deleted = $this->getModelByName($flag_detail);
                $model_remark_deleted->where($fk_id, $model_id)->delete();

                $detail = $request->detail;
                if (!empty($detail)) {
                    $column_arr     = $columns;
                    $total_data     = 0;
                    foreach ($detail as $column => $arr_data) {
                        $total_data = count($arr_data);
                        break;
                    }

                    if ($total_data > 0) {
                        for ($i = 0; $i < $total_data; $i++) {
                            $insert = [];
                            foreach ($column_arr as $index => $column_name) {
                                $data_type = $columns_data_type[$index]->DATA_TYPE;
                                $return_null_data = $this->return_datatype_null($data_type);
                                if ($column_name == $fk_id) {
                                    $insert[$column_name] = $model_id;
                                    continue;
                                }
                                $value = $return_null_data;
                                if (!empty($detail[$column_name][$i])) {
                                    $value = $detail[$column_name][$i];
                                }
                                $insert[$column_name] = $value;
                            }
                            $model_remarks = $this->getModelByName($flag_detail);
                            $model_remarks->create($insert);
                        }
                    }
                }
            }
            DB::commit();
            Session::flash('success', 'Berhasil mengubah data detail Visual Quality');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            Session::flash('failed', 'Gagal mengubah data detail Visual Quality');
            return redirect()->back();
        }
    }

    public function detail_grup($periode, $visual)
    {
        $master = MasterQuality::where('jenis_project', $visual->jenis_project)->where('id', $visual->jenis_form)->first();

        $model = $this->getModelByName($master);

        if ($master->model == 'VqfHcProductionWork' || $master->model == 'VqfTank') {
            $model = $model::where('visual_quality_fleet_id', $visual->id)->where('periode_visual_quality_id', $periode->id)->get();
        } else {
            $model = $model::with('remarks')->where('visual_quality_fleet_id', $visual->id)->where('periode_visual_quality_id', $periode->id)->get();
        }

        $plan = $this->countPlan($visual, $periode->periode);
        $inspection = $this->countInspeksi($visual, $periode->periode);

        $total_plan = 0;
        $total_inspeksi = 0;
        $total_deviasi = 0;
        if($plan['total_plan'] > 0){
            $total_plan = $plan['total_plan'] / $plan['total_quantity'] * ($visual->bobot / 100);
        }

        if($inspection['total_inspeksi'] > 0){
            $total_inspeksi = $inspection['total_inspeksi'] / $inspection['total_quantity'] * ($visual->bobot / 100);
            $total_deviasi = $total_inspeksi - $total_plan;
        }

        $data = [
            'datas' => $model,
            'flag' => $master,
            'plan' => $plan,
            'inspeksi' => $inspection,
            'total_plan' => $total_plan,
            'total_inspeksi' => $total_inspeksi,
            'total_deviasi' => $total_deviasi
        ];

        // dd($data);

        return $data;
    }

    public function duplicate_periode($latest_periode, $new_id)
    {
        DB::beginTransaction();
        try {

            $master = MasterQuality::where('jenis_project', $latest_periode->visual_quality->jenis_project)
                ->where('id', $latest_periode->visual_quality->jenis_form)->first();

            $model = $this->getModelByName($master);

            $model->where('visual_quality_fleet_id', $latest_periode->visual_quality_id)->where('periode_visual_quality_id', $latest_periode->id)->get();

            DB::commit();
            Session::flash('success', 'Berhasil duplikasi periode data');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed', 'Gagal duplikasi periode data');
            return redirect()->back();
        }
    }

    public function duplicate_section(Request $request, $project, $periode)
    {
        $jenis_data_from    = $request->input('jenis_data_from');
        $jenis_data_to      = $request->input('jenis_data_to');
        $vqf_id             = $request->input('vqf_id');
        $flag               = json_decode($request->flag);

        $model = $this->getModelByName($flag);

        if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
            $flag_detail        = $flag;
            $flag_detail->model = $flag_detail->model . 'Remarks';
            $model_remarks      = $this->getModelByName($flag_detail);
            $table_name         = $model_remarks->getTable();
            $columns = Schema::getColumnListing($table_name);

            array_shift($columns);
            array_pop($columns);
            array_pop($columns);
            $fk_id = $columns[0];
        }

        DB::beginTransaction();
        try {
            $sc_data = $model;
            $sc_data = $sc_data->where('visual_quality_fleet_id', $vqf_id)->where('periode_visual_quality_id', $periode)->where('jenis_data', $jenis_data_from)->get();
            foreach ($sc_data as $source_data) {
                $source_data = $source_data->toArray();
                $source_id  = $source_data['id'];
                array_shift($source_data); // column id
                array_pop($source_data); // column created_at
                array_pop($source_data); // column updated_at
                $insert_data = $source_data;

                if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
                    $source_data_remarks = $model_remarks;
                    $source_data_remarks = $source_data_remarks->where($fk_id, $source_id)->get()->toArray();
                }
                if (!empty($jenis_data_to)) {
                    foreach ($jenis_data_to as $jenis_data => $val) {
                        $insert_data['jenis_data'] = $jenis_data;

                        $new_data = $model;
                        $new_data = $new_data::create($insert_data);
                        $new_data_id    = $new_data->id;
                        if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
                            if (!empty($source_data_remarks)) {
                                foreach ($source_data_remarks as $remarks) {
                                    $insert_remarks = $remarks;
                                    $insert_remarks[$fk_id] = $new_data_id;
                                    array_shift($insert_remarks);
                                    array_pop($insert_remarks);
                                    array_pop($insert_remarks);

                                    $new_data_remark = $model_remarks;
                                    $new_data_remark = $new_data_remark::create($insert_remarks);
                                }
                            }
                        }
                    }
                }
            }


            DB::commit();
            Session::flash('success', 'Berhasil duplikasi section data');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollback();
            Session::flash('failed', 'Gagal duplikasi section data');
            dd($th->getMessage());
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    public function duplicate_group(Request $request)
    {
        $id = $request->input('id');
        $jumlah_data = $request->input('jumlah_data');
        $flag = json_decode($request->flag);

        $model = $this->getModelByName($flag);

        if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
            $flag_detail        = $flag;
            $flag_detail->model = $flag_detail->model . 'Remarks';
            $model_remarks      = $this->getModelByName($flag_detail);
            $table_name         = $model_remarks->getTable();
            $columns = Schema::getColumnListing($table_name);

            array_shift($columns);
            array_pop($columns);
            array_pop($columns);
            $fk_id = $columns[0];
        }
        DB::beginTransaction();
        try {
            $source_data = $model;
            $source_data = $source_data->where('id', $id)->first()->toArray();
            $source_id  = $source_data['id'];
            array_shift($source_data);
            array_pop($source_data);
            array_pop($source_data);
            for ($i = 0; $i < $jumlah_data; $i++) {
                $insert_data = $source_data;
                if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {
                    $source_data_remarks = $model_remarks;
                    $source_data_remarks = $source_data_remarks->where($fk_id, $source_id)->get()->toArray();
                }

                $new_data = $model;
                $new_data = $new_data::create($insert_data);
                $new_data_id    = $new_data->id;

                if ($flag->model != 'VqfHcProductionWork' && $flag->model != 'VqfTank') {

                    if (!empty($source_data_remarks)) {
                        foreach ($source_data_remarks as $remarks) {
                            $insert_remarks = $remarks;
                            $insert_remarks[$fk_id] = $new_data_id;
                            array_shift($insert_remarks);
                            array_pop($insert_remarks);
                            array_pop($insert_remarks);

                            $new_data_remark = $model_remarks;
                            $new_data_remark = $new_data_remark::create($insert_remarks);
                        }
                    }
                }
            }

            DB::commit();
            Session::flash('success', 'Berhasil duplikasi grup');
            return redirect()->back()->with('success', 'Duplikasi group berhasil dilakukan.');
        } catch (\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            Session::flash('failed', 'Gagal duplikasi grup');
            return redirect()->back();
        }
    }

    public function destroy_detail(Request $request)
    {
        $id = $request->input('id');
        $flag = json_decode($request->flag);
        DB::beginTransaction();
        try {
            $flag = json_decode($request->flag);
            $model = $this->getModelByName($flag);

            if (!$flag->model == 'VqfHcProductionWork' || !$flag->model == 'VqfTank') {
                $model_remarks  = $flag->model . 'Remarks';
                $model_remarks  = $this->getModelByName($model_remarks);
                $table_name     = $model_remarks->getTable();
                $columns        = Schema::getColumnListing($table_name);

                array_shift($columns);
                array_pop($columns);
                array_pop($columns);
                $fk_id = $columns[0];

                $model_remarks->where($fk_id, $id)->delete();
            }

            $model->where('id', $id)->delete();

            DB::commit();
            Session::flash('success', 'Delete section berhasil dilakukan');
            return redirect()->back();
        } catch (\Throwable $th) {
            DB::rollback();
            // dd($th->getMessage());
            Session::flash('failed', 'Delete section gagal dilakukan');
            return redirect()->back();
        }
    }

    public function mappingStatus()
    {
        $plan_status = [
            0 => "No Progress",
            1 => "Accepted",
            2 => "Inspection Invitation",
            3 => "Internal Progress Yard"
        ];

        $insepction_status = [
            0 => "No Progress",
            1 => "Accepted",
            2 => "Re-Inspect / Partial",
            3 => "Yard Progress"
        ];

        return ["plan" => $plan_status, "inspection" => $insepction_status];
    }

    public function getModelByName($request)
    {
        $model_name = $request->model;
        $model = app("App\Models\ProjectMonitoring\Quality\\" . $model_name);
        return $model;
    }

    public function getModelByRemarks($model)
    {
        $model_name = $model;
        $model = app("App\Models\ProjectMonitoring\Quality\\" . $model_name);
        return $model;
    }


    public function indexInspection(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        $visual = VisualQualityFleet::where('id', $periode->visual_quality_fleet_id)->first();
        if ($request->ajax()) {
            $data = InspectionAcceptenceRateFleet::selectRaw('*, CONVERT(jml_undangan,char) AS jml_undangan')
                ->where('visual_quality_fleet_id', $visual->id)
                ->orderBy('periode')
                ->orderBy('created_at')
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('rate', function ($row) {
                    return '
                        Acc by Class / MK : ' . toPercent(($row->acc_class / $row->jml_undangan) * 100) . '
                        <br>
                        Acc by Owner : ' . toPercent(($row->acc_owner / $row->jml_undangan) * 100) . '
                        <br>
                        Acc by Both : ' . toPercent(($row->acc_both / $row->jml_undangan) * 100) . '
                    ';
                })
                ->addColumn('evidence', function ($row) {
                    return '
                        <a href="' . $row->evidence . '" class="text-primary">
                            ' . $this->getFileName($row->evidence) . '
                        </a>
                    ';
                })
                ->addColumn('action', function ($row) {
                    return '
                        <a class="btn btn-sm btn-icon btn-success btn-inspection-edit" type="button" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                        </a>
                        <a class="btn btn-sm btn-icon btn-danger btn-inspection-delete" type="button" data-id="' . $row->id . '">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                        </a>
                    ';
                })
                ->rawColumns(['action', 'evidence', 'rate'])
                ->make(true);
        }
        $data = [
            'project'       => $project,
            'visual'        => $visual,
            'periode'       => $periode,
            'quality_inspection'    => $this->qualityInspectionProgress(),
            'detail'        => $this->detail_grup($periode, $visual)
        ];
        return view('pages.project-monitoring.fleet.quality.detail_inspection', compact('data'));
    }

    public function storeInspection(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        DB::beginTransaction();
        try {

            $grup_path = preg_replace('/[^a-zA-Z0-9_.]/', '_', $periode->visual_quality->nama_grup);
            $path = 'ipm/fleet/quality/inspection/' . $grup_path . '/' . $request->periode;

            InspectionAcceptenceRateFleet::create([
                'visual_quality_fleet_id'        => $request->visual_quality_fleet_id,
                'periode'                       => $request->periode,
                'jml_undangan'                  => $request->jml_undangan,
                'resinpect'                     => $request->resinpect,
                'acc_class'                     => $request->acc_class,
                'acc_owner'                     => $request->acc_owner,
                'acc_both'                      => $request->acc_both,
                'evidence'                      => $this->uploadFile($request->file('evidence'), $path)
            ]);

            DB::commit();
            // Session::flash('success', 'Berhasil menambah inspection rate data!');
            return redirect()->route('ipm.fleet.quality.inspection.index', [$project->id, $periode->id])->with('success', 'Berhasil menambah inspection rate data!');
        } catch (\Exception $e) {
            DB::rollBack();
            // dd($e->getMessage());
            return redirect()->back()->withInput()->with('failed', 'Gagal menambah inspection rate!');
        }
    }

    public function showInspection(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        $data = InspectionAcceptenceRateFleet::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        return response()->json($data);
    }

    public function updateInspection(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        DB::beginTransaction();

        $data = InspectionAcceptenceRateFleet::find($request->id);
        if (!$data) {
            return redirect()->back()->with('failed', 'Data tidak ditemukan!');
        }

        try {
            $grup_path = preg_replace('/[^a-zA-Z0-9_.]/', '_', $periode->visual_quality->nama_grup);
            $path = 'ipm/fleet/quality/inspection/' . $grup_path . '/' . $request->periode;
            $beUpdate = [
                'periode'       => $request->periode,
                'jml_undangan'  => $request->jml_undangan,
                'resinpect'     => $request->resinpect,
                'acc_class'                 => $request->acc_class,
                'acc_owner'     => $request->acc_owner,
                'acc_both'      => $request->acc_both,
            ];
            if ($request->evidence) {
                $beUpdate['evidence'] = $this->uploadFile($request->file('evidence'), $path);
            }

            $data->update($beUpdate);

            DB::commit();
            return redirect()->route('ipm.fleet.quality.inspection.index', [$project->id, $periode->id])->with('success', 'Berhasil menambah inspection rate data!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', 'Gagal memperbarui inspection rate!');
        }
    }

    public function destroyInspection(Request $request, ProjectMonitoringFleet $project, PeriodeVisualQualityFleet $periode)
    {
        DB::beginTransaction();

        $data = InspectionAcceptenceRateFleet::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        try {
            $data->delete();
            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Gagal menghapus data!', 'error' => $e->getMessage()], 500);
        }
    }

    public function indexDrawing(Request $request, ProjectMonitoringFleet $project)
    {
        $data = DrawingVisualQualityFleet::where('jenis_drawing', 1)
            ->where('project_id', $project->id)
            ->orderBy('tanggal')
            ->get();

        return datatables()
            ->of($data)
            ->addColumn('drawing', function ($row) {
                return '
                    <a href="' . $row->drawing . '" class="text-muted text-hover-primary">
                        ' . $this->getFileName($row->drawing) . '
                    </a>
                ';
            })
            ->addColumn('tanggal', function ($row) {
                return \Carbon\Carbon::parse($row->tanggal)->format('Y-m-d h:i:s');
            })
            ->addColumn('action', function ($row) {
                return '
                    <a class="btn btn-sm btn-icon btn-success btn-drawing-edit" onclick="detailDrawing(`' . $row->id . '`)" type="button" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>
                    <a class="btn btn-sm btn-icon btn-danger btn-drawing-delete" onclick="deleteDrawing(`' . $row->id . '`)" type="button" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
            })
            ->rawColumns(['drawing', 'action'])
            ->make(true);
    }

    public function storeDrawing(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        try {
            if ($request->drawing) {
                $path = 'ipm/fleet/quality/drawing/' . $project->nama_project;
                DrawingVisualQualityFleet::create([
                    'project_id'    => $project->id,
                    'drawing'       => $this->uploadFile($request->file('drawing'), $path),
                    'deskripsi'     => $request->deskripsi,
                    'tanggal'       => $request->tanggal,
                    'jenis_drawing' => 1 // as port drawing
                ]);

                DB::commit();
                return redirect()->route('ipm.fleet.quality.index', $project->id)->with('success', 'Berhasil mengunggah drawing!');
            }

            return redirect()->route('ipm.fleet.quality.index', $project->id)->with('failed', 'Tidak ada file yang diunggah!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ipm.fleet.quality.index', $project->id)->with('failed', 'Gagal mengunggah drawing!');
        }
    }

    public function showDrawing(Request $request)
    {
        $data = DrawingVisualQualityFleet::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        return response($data);
    }

    public function updateDrawing(Request $request, ProjectMonitoringFleet $project)
    {
        $data = DrawingVisualQualityFleet::find($request->id);
        if (!$data) {
            return redirect()->route('ipm.fleet.quality.index', $project->id)->with('failed', 'Data tidak ditemukan!');
        }

        DB::beginTransaction();

        try {
            $path = 'ipm/fleet/quality/drawing/' . $project->nama_project;
            $beUpdate = [
                'deskripsi'     => $request->deskripsi,
                'tanggal'       => $request->tanggal,
            ];
            if (!empty($request->drawing)) {
                $beUpdate['drawing'] = $this->uploadFile($request->file('drawing'), $path);
            }

            $data->update($beUpdate);

            DB::commit();
            return redirect()->route('ipm.fleet.quality.index', $project->id)->with('success', 'Berhasil mengunggah drawing!');

            return;
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ipm.fleet.quality.index', $project->id)->with('failed', 'Gagal mengunggah drawing!');
        }
    }

    public function destroyDrawing(Request $request)
    {
        $data = DrawingVisualQualityFleet::find($request->id);
        if (!$data) {
            return response()->json(['message' => 'Data tidak ditemukan!'], 400);
        }

        DB::beginTransaction();
        try {
            $data->delete();
            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus drawing data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menghapus data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function storeDrawingGroup(Request $request, ProjectMonitoringFleet $project)
    {
        $periode = $request->input('periode');
        try {
            if ($request->hasFile('file')) {
                $path = 'ipm/fleet/quality/drawing/' . $project->nama_project . '/' . $periode;
                PeriodeVisualQualityFleet::where('id', $periode)->update([
                    'document_drawing' => $path . "/" . $this->uploadFileName($request->file, $path)
                ]);
            }

            Session::flash('success', 'Berhasil menambahkan drawing per group');
            return redirect()->back()->with('success', 'Berhasil menambahkan drawing per group');
        } catch (\Throwable $th) {
            Session::flash('failed', 'Gagal menambahkan drawing per group');
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    private function countPlan($quality, $periode)
    {
        $master = MasterQuality::where('id', $quality->jenis_form)->first();
        $total_plan = 0;
        $total_quantity = 0;

        $model = $this->getModelByName($master);
        $periode = PeriodeVisualQualityFleet::where('visual_quality_fleet_id', $quality->id)->where('periode', $periode)->pluck('id')->first();

        if ($master->model != "VqfBlastingPainting" && $master->model != "VqfHcProductionWork" && $master->model != 'VqfPropultionNb') {
            foreach ($this->plan as $key => $value) {
                $value = (object)$value;
                $data = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project);

                if ($master->model == 'VqfTank' || $master->model == 'VqfComissionFunction' || $master->model == 'VqfDeckMachinery' || $master->model == 'VqfMachinery' || $master->model == 'VqfPropultion') {
                    $quantity = $data->where('status_plan', $value->id)->count();
                } else {
                    $fk_id = null;
                    if ($master->model == 'VqfElectricalOutfiting') {
                        $fk_id = 'vqf_eo_id';
                    } else if ($master->model == 'VqfMachineryOutfiting') {
                        $fk_id = 'vqf_mo_id';
                    } else {
                        $fk_id = $model->getForeignKey();
                    }

                    $data = $data->first();
                    if ($data) {
                        $modelRemarks = $this->getModelByRemarks($master->model . 'Remarks');
                        $quantity = $modelRemarks->where($fk_id, $data->id)->where('status_plan', $value->id)->count();
                    } else {
                        $quantity = 0;
                    }
                }

                $total_quantity = $total_quantity + $quantity;
                $total_plan = $total_plan + ($quantity * $value->value);
                $this->plan[$key]['quantity'] = $quantity;
            }
        } else {
            foreach ($this->plan as $key => $value) {
                $jumlah_plan = 0;
                $value = (object)$value;
                if ($master->model == 'VqfBlastingPainting' || $master->model == 'VqfPropultionNb') {
                    $tab = $this->blasting;
                    $blasting = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project)->get();

                    if ($blasting) {
                        foreach ($blasting as $ky => $val) {
                            $fk_id = null;
                            if ($master->model == 'VqfPropultionNb') {
                                $fk_id = 'vqf_propultion_id';
                            } else {
                                $fk_id = $model->getForeignKey();
                            }

                            $modelRemarks = $this->getModelByRemarks($master->model . 'Remarks');
                            $quantity = $modelRemarks->where($fk_id, $val->id)->where('status_plan', $value->id)->count();

                            $total_quantity = $total_quantity + $quantity;
                            $total_plan = $total_plan + ($quantity * $value->value);
                            $jumlah_plan = $jumlah_plan + $quantity;
                        }

                        $this->plan[$key]['quantity'] = $jumlah_plan;
                    }
                }

                if ($master->model == 'VqfHcProductionWork') {
                    $tab = $this->hcpw;
                    $quantity = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project)->where('status_plan', $value->id)->count();

                    $total_quantity = $total_quantity + $quantity;
                    $total_plan = $total_plan + ($quantity * $value->value);
                    $this->plan[$key]['quantity'] = $quantity;
                }
            }
        }

        $datas['total_plan'] = $total_plan;
        $datas['total_quantity'] = $total_quantity;
        $datas['plan'] = $this->plan;

        return $datas;
    }

    private function countInspeksi($quality, $periode)
    {
        $master = MasterQuality::where('id', $quality->jenis_form)->first();
        $total_inspeksi = 0;
        $total_quantity = 0;

        $model = $this->getModelByName($master);
        $periode = PeriodeVisualQualityFleet::where('visual_quality_fleet_id', $quality->id)->where('periode', $periode)->pluck('id')->first();

        if ($master->model != "VqfBlastingPainting" && $master->model != "VqfHcProductionWork" && $master->model != 'VqfPropultionNb') {
            foreach ($this->inspection as $key => $value) {
                $value = (object)$value;
                $data = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project);

                if ($master->model == 'VqfTank' || $master->model == 'VqfComissionFunction' || $master->model == 'VqfDeckMachinery' || $master->model == 'VqfMachinery' || $master->model == 'VqfPropultion') {
                    $quantity = $data->where('status_inspeksi', $value->id)->count();
                } else {
                    $fk_id = null;
                    if ($master->model == 'VqfElectricalOutfiting') {
                        $fk_id = 'vqf_eo_id';
                    } else if ($master->model == 'VqfMachineryOutfiting') {
                        $fk_id = 'vqf_mo_id';
                    } else {
                        $fk_id = $model->getForeignKey();
                    }

                    $data = $data->first();
                    if ($data) {
                        $modelRemarks = $this->getModelByRemarks($master->model . 'Remarks');
                        $quantity = $modelRemarks->where($fk_id, $data->id)->where('status_inspeksi', $value->id)->count();
                    } else {
                        $quantity = 0;
                    }
                }

                $total_quantity = $total_quantity + $quantity;
                $total_inspeksi = $total_inspeksi + ($quantity * $value->value);
                $this->inspection[$key]['quantity'] = $quantity;
            }
        } else {
            foreach ($this->inspection as $key => $value) {
                $jumlah_inspeksi = 0;
                $value = (object)$value;
                if ($master->model == 'VqfBlastingPainting' || $master->model == 'VqfPropultionNb' || $master->model == 'VqfPropultionRp') {
                    $tab = $this->blasting;
                    $blasting = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project)->get();
                    if ($blasting) {
                        foreach ($blasting as $ky => $val) {
                            $fk_id = null;
                            if ($master->model == 'VqfPropultionNb' || $master->model == 'VqfPropultionRp') {
                                $fk_id = 'vqf_propultion_id';
                            } else {
                                $fk_id = $model->getForeignKey();
                            }

                            $modelRemarks = $this->getModelByRemarks($master->model . 'Remarks');
                            $quantity = $modelRemarks->where($fk_id, $val->id)->where('status_inspeksi', $value->id)->count();

                            $total_quantity = $total_quantity + $quantity;
                            $total_inspeksi = $total_inspeksi + ($quantity * $value->value);
                            $jumlah_inspeksi = $jumlah_inspeksi + $quantity;
                        }
                        $this->inspection[$key]['quantity'] = $jumlah_inspeksi;
                    }
                }

                if ($master->model == 'VqfHcProductionWork') {
                    $tab = $this->hcpw;
                    $quantity = $model->where('visual_quality_fleet_id', $quality->id)->where('periode_visual_quality_id', $periode)->where('jenis_proyek', $master->jenis_project)->where('status_inspeksi', $value->id)->count();

                    $total_quantity = $total_quantity + $quantity;
                    $total_inspeksi = $total_inspeksi + ($quantity * $value->value);
                    $this->inspection[$key]['quantity'] = $quantity;
                }
            }
        }

        $datas['total_inspeksi'] = $total_inspeksi;
        $datas['total_quantity'] = $total_quantity;
        $datas['inspection'] = $this->inspection;

        return $datas;
    }

    public function inspection_index($visual)
    {
        foreach ($visual as $k => $v) {
            if ($v) {
                $data = InspectionAcceptenceRateFleet::where('visual_quality_fleet_id', $v->id)
                    ->orderBy('periode')
                    ->orderBy('created_at')
                    ->get();

                $total_class = 0;
                $total_owner = 0;
                $total_both = 0;
                $total_undangan = 0;

                foreach ($data as $key => $value) {
                    if ($value) {
                        $total_class = +$value->acc_class;
                        $total_owner = +$value->acc_owner;
                        $total_both = +$value->acc_both;
                        $total_undangan = +$value->jml_undangan;
                    }
                }

                if ($total_class > 0 && $total_undangan > 0) {
                    $visual[$k]->class = toPercent(($total_class / $total_undangan) * 100);
                } else {
                    $visual[$k]->class = 0;
                }

                if ($total_owner > 0 && $total_undangan > 0) {
                    $visual[$k]->owner = toPercent(($total_owner / $total_undangan) * 100);
                } else {
                    $visual[$k]->owner = 0;
                }

                if ($total_both > 0 && $total_undangan > 0) {
                    $visual[$k]->both = toPercent(($total_both / $total_undangan) * 100);
                } else {
                    $visual[$k]->owner = 0;
                }
            }
        }

        return $visual;
    }

    public function getMaxPeriode($project_id)
    {
        $visual = VisualQualityFleet::where('project_id', $project_id)->get();
        $visual_id = [];

        foreach ($visual as $key => $value) {
            if ($value) {
                array_push($visual_id, $value->id);
            }
        }

        $data = PeriodeVisualQualityFleet::whereIn('visual_quality_fleet_id', $visual_id)->max('periode');
        return $data;
    }
}
