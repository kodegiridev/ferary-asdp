<?php

namespace App\Http\Controllers\ProjectMonitoring\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectMonitoring\Fleet\Recomendation\StoreNoticeRequest;
use App\Http\Requests\ProjectMonitoring\Fleet\Recomendation\UpdateNoticeRequest;
use App\Models\ProjectMonitoring\NoticeRecomendationFleet;
use App\Models\ProjectMonitoring\PeriodeRecomendationFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecomendationController extends Controller
{
    private $path = 'ipm/fleet/recomendation';

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringFleet $project
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectMonitoringFleet $project)
    {
        if ($request->ajax()) {
            return $this->datatable($project);
        }

        $notice = NoticeRecomendationFleet::whereHas('periode_recomendation', function ($query) use ($project) {
            return $query->where('project_id', $project->id);
        });

        $data = [
            'project' => $project,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $notice)->openStatus()->count(),
                'total_close' => (clone $notice)->closeStatus()->count(),
                'total' => $notice->count()
            ]
        ];

        return view('pages.project-monitoring.fleet.recomendation.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param ProjectMonitoringFleet $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ProjectMonitoringFleet $project)
    {
        DB::beginTransaction();

        try {
            $data = PeriodeRecomendationFleet::whereProject($project->id)
                ->with([
                    'notices' => fn ($q) => $q->selectRaw('periode_id, notice, to_do_list, due_date, status, pic, file')
                ])
                ->orderBy('periode', 'desc')
                ->first();

            $store = PeriodeRecomendationFleet::create([
                'project_id' => $project->id,
                'periode' => ($data->periode ?? 0) + 1
            ]);
            if (count($data->notices ?? []) > 0) {
                $store->notices()->createMany($data->notices->toArray());
            }

            DB::commit();
            return redirect()->route('ipm.fleet.recomendation.index', $project->id)->with(['success' => 'Berhasil menambah period!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah period!']);
        }
    }

    /**
     * Data for own statistics
     * 
     * @param ProjectMonitoringFleet $project
     * @return \PDF
     */
    public function print(ProjectMonitoringFleet $project)
    {
        $fileName = 'IPM_FLEET_' . $project->nama_project . '_' . time();
        $noticeQuery = NoticeRecomendationFleet::countStatus()->selectRaw('periode_id')->groupBy('periode_id');
        $noticeCount = NoticeRecomendationFleet::whereHas('periode_recomendation', function ($query) use ($project) {
            return $query->where('project_id', $project->id);
        });

        $periods = PeriodeRecomendationFleet::selectRaw('data_periode_recomendation_fleet.*, total_close, total')
            ->whereProject($project->id)
            ->joinSub($noticeQuery, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_fleet.id');
            })
            ->get();

        $data = [
            'project' => $project,
            'periods' => $periods,
            'file_name' => $fileName,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $noticeCount)->openStatus()->count(),
                'total_close' => (clone $noticeCount)->closeStatus()->count(),
                'total' => $noticeCount->count()
            ]
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.fleet.recomendation.pdf.recomendation', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \Illuminate\Http\Response
     */
    public function indexNotice(Request $request, ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        if ($request->ajax()) {
            return $this->datatableNotice($period);
        }

        $query = NoticeRecomendationFleet::where('periode_id', $period->id);

        $data = [
            'project' => $project,
            'period' => $period,
            'users' => User::asStaff()->selectRaw('id, name')->orderBy('name')->get(),
            'status' => ['open', 'close'],
            'notice' => [
                'total_open' => (clone $query)->openStatus()->count(),
                'total_close' => (clone $query)->closeStatus()->count(),
                'total' => $query->count()
            ],
            'basic_data' => $project->latest_basic_data,
        ];

        return view('pages.project-monitoring.fleet.recomendation.notice.index', compact('data'));
    }

    /**
     * Display a listing of the resource.
     * 
     * @param StoreNoticeRequest $request
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \Illuminate\Http\Response
     */
    public function storeNotice(StoreNoticeRequest $request, ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        DB::beginTransaction();

        try {
            NoticeRecomendationFleet::create(array_merge($request->validated(), [
                'periode_id' => $period->id,
                'file' => $this->uploadFile($request->file('file'), $this->path)
            ]));

            DB::commit();
            return redirect()->route('ipm.fleet.recomendation.notice.index', [$project->id, $period->id])->with(['success' => 'Berhasil menambah notice']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menambah notice']);
        }
    }

    /**
     * Display a listing of the resource.
     * 
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \Illuminate\Http\Response
     */
    public function showNotice(Request $request, ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        $data = NoticeRecomendationFleet::selectRaw('*, DATE_FORMAT(due_date, "%Y-%m-%d") AS due_date')->where('id', $request->id)->first();
        if (!$data) {
            return response('Notice tidak ditemukan!', 400);
        }

        $data->users = User::asStaff()->selectRaw('id, name')->orderBy('name')->get();

        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     * 
     * @param UpdateNoticeRequest $request
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \Illuminate\Http\Response
     */
    public function updateNotice(UpdateNoticeRequest $request, ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        DB::beginTransaction();

        $data = NoticeRecomendationFleet::find($request->id);
        if (!$data) {
            return redirect()->back()->withInput()->with(['failed' => 'Notice tidak ditemukan!']);
        }

        try {
            $_request = $request->validated();
            if ($request->file) {
                $_request['file'] = $this->uploadFile($request->file('file'), $this->path);
            } else {
                unset($_request['file']);
            }

            $data->update($_request);

            DB::commit();
            return redirect()->route('ipm.fleet.recomendation.notice.index', [$project->id, $period->id])->with(['success' => 'Berhasil memperbarui notice']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal memperbarui notice']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \Illuminate\Http\Response
     */
    public function destroyNotice(Request $request, ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        DB::beginTransaction();

        try {
            $data = NoticeRecomendationFleet::find($request->id);
            if (!$data) {
                \Session::flash('failed', 'Notice terpilih tidak ditemukan!');
                return;
            }

            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus notice!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus notice!');
        }
    }

    /**
     * Data for own statistics
     * 
     * @param ProjectMonitoringFleet $project
     * @param PeriodeRecomendationFleet $period
     * @return \PDF
     */
    public function printNotice(ProjectMonitoringFleet $project, PeriodeRecomendationFleet $period)
    {
        $fileName = 'IPM_FLEET_' . $project->nama_project . '-' . $period->periode . '_' . time();
        $noticeQuery = NoticeRecomendationFleet::where('periode_id', $period->id)->orderBy('id');

        $data = [
            'project' => $project,
            'notices' => (clone $noticeQuery)->get(),
            'file_name' => $fileName,
            'basic_data' => $project->latest_basic_data,
            'notice' => [
                'total_open' => (clone $noticeQuery)->openStatus()->count(),
                'total_close' => (clone $noticeQuery)->closeStatus()->count(),
                'total' => $noticeQuery->count()
            ]
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.fleet.recomendation.pdf.notice', compact('data'));
        return $pdf->stream($fileName . '.pdf');
    }

    /**
     * Display data recomendation in datatables
     *
     * @param ProjectMonitoringFleet $project
     * @return \Yajra\DataTables\DataTables
     */
    public function datatable(ProjectMonitoringFleet $project)
    {
        $notice = NoticeRecomendationFleet::countStatus()->selectRaw('periode_id')->groupBy('periode_id');

        $data = PeriodeRecomendationFleet::selectRaw('data_periode_recomendation_fleet.*, total_close, total')
            ->whereProject($project->id)
            ->leftJoinSub($notice, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_fleet.id');
            })
            ->get();

        return datatables()
            ->of($data)
            ->addColumn('total_notice', function ($row) {
                return $row->total ?? 0;
            })
            ->addColumn('closed_notice', function ($row) {
                return $row->total_close ?? 0;
            })
            ->addColumn('closed_percentage', function ($row) {
                $amount = 0;
                if (($row->total ?? 0) > 0) {
                    $amount = ($row->total_close / $row->total) * 100;
                }

                return toPercent($amount, 2);
            })
            ->addColumn('action', function ($row) use ($project) {
                return '
                    <a href="' . route('ipm.fleet.recomendation.notice.index', [$project->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                        ' . theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') . '
                    </a>

                    <a href="' . route('ipm.fleet.recomendation.notice.print', [$project->id, $row->id]) . '" class="btn btn-sm btn-success btn-icon">
                        ' . theme()->getSvgIcon('icons/bi/printer-fill.svg', 'svg-icon-1') . '
                    </a>
                ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data notice in datatables.
     * 
     * @param PeriodeRecomendationFleet $period
     * @return \Yajra\DataTables\DataTables
     */
    public function datatableNotice(PeriodeRecomendationFleet $period)
    {
        $data = NoticeRecomendationFleet::where('periode_id', $period->id)
            ->orderBy('id')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('pic', function ($row) {
                return $row->user->name;
            })
            ->addColumn('status', function ($row) {
                return strtoupper($row->status);
            })
            ->addColumn('due_date', function ($row) {
                return \Carbon\Carbon::parse($row->due_date)->format('Y-m-d');
            })
            ->addColumn('file', function ($row) {
                return '
                    <a href="' . $row->file . '" class="text text-hover-primary">
                        ' . $this->getFileName($row->file) . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) {
                return '
                    <button type="button"
                        class="btn btn-sm btn-icon btn-primary btn-edit"
                        data-id=' . $row['id'] . '>
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'fs-1') . ' 
                    </button>

                    <button type="button"
                        class="btn btn-sm btn-icon btn-danger btn-delete"
                        data-id=' . $row['id'] . '>
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') . ' 
                    </button>
                ';
            })
            ->rawColumns(['action', 'file'])
            ->make(true);
    }
}
