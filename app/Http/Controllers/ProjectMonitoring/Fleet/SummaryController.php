<?php

namespace App\Http\Controllers\ProjectMonitoring\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringFleet;
use App\Models\ProjectMonitoring\EvidenceProgresFleet;
use App\Models\ProjectMonitoring\NoticeRecomendationFleet;
use App\Models\ProjectMonitoring\PeriodeRecomendationFleet;
use App\Models\ProjectMonitoring\ProgresProjectFleet;
use App\Models\ProjectMonitoring\ProjectMonitoringFleet;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.project-monitoring.fleet.summary.mail';
    }

    public function index(ProjectMonitoringFleet $project)
    {
        $user = auth()->user();
        $title = 'Executive Summary Project Refleet';
        $project->status = ProjectMonitoringFleet::$statusLabel[$project->status];
        $approvalProcess = $this->approvalStatus($project);
        $progress = (new ProgressController)->index_progres($project)->toArray();

        $data = [
            'user'              => $user,
            'project'           => $project,
            'is_summary'        => true,
            'calculate'         => $this->costPerformanceIndex($project, $progress),
            'completion'        => $this->projectSummary($project),
            'first_basic_data'  => $project->oldest_basic_data,
            'latest_basic_data' => $project->latest_basic_data,
            'latest_progress'   => $project->latest_progress,
            'periode_progress'  => @count($progress),
            'schedule_progress' => $this->scheduleProgress($progress),
            'schedule_quality'  => $this->scheduleQuality(),
            'item_progress'     => $this->progressProject($project),
            'recomendations'    => $this->recomendation($project),
            'documentations'    => $this->documentation($project),
            'approvers'         => User::asApprover($user->id)->orderBy('name')->get(),
            'verificators'      => User::asVerifikator($user->id)->orderBy('name')->get(),
            'n_period'          => $project->progress()->count(),
            'approval'          => $approvalProcess['approval'],
            'can_approve_reject' => $approvalProcess['can_approve_reject'],
            'is_approver_verificator' => $approvalProcess['is_approver_verificator']
        ];

        return view('pages.project-monitoring.fleet.summary.index', compact('data', 'title'));
    }

    public function print(Request $request, $project, $onlyView = false)
    {
        $project = ProjectMonitoringFleet::find($project);
        if (!$project) {
            return redirect()->back()->with('failed', 'Selected project not found.');
        }

        $progress = (new ProgressController)->index_progres($project)->toArray();
        $fileName = 'EXCECUTIVE_SUMMARY_' . trim($project->nama_project) . '_' . time();
        $data = [
            'file_name'         => $fileName,
            'project'           => $project,
            'first_basic_data'  => $project->oldest_basic_data,
            'latest_progress'   => $project->latest_progress,
            'latest_basic_data' => $project->latest_basic_data,
            'periode_progress'  => @count($progress),
            'cost_calculate'    => $this->costPerformanceIndex($project, $progress),
            'schedule_progress' => $this->scheduleProgress($progress),
            'schedule_quality'  => $this->scheduleQuality($project),
            'item_progress'     => $this->progressProject($project),
            'recomendations'    => $this->recomendation($project),
            'documentations'    => $this->documentation($project),
        ];

        $pdf = \Pdf::loadView('pages.project-monitoring.fleet.summary.pdf', compact('data'));
        if ($onlyView) return $pdf;

        return $pdf->stream($fileName . '.pdf');
    }

    public function sendApproval(Request $request, ProjectMonitoringFleet $project)
    {
        \DB::beginTransaction();

        try {
            if ($project->pic != auth()->user()->id) {
                \Session::flash('failed', 'Hanya Project PIC yang dapat mengirim approval!');
                return;
            }

            $progress = $project->latest_progress;
            if (!$progress ?? false) {
                \Session::flash('failed', 'Tidak ada progres yang dapat di kirim ke approval!');
                return;
            }

            $projectApproval = new ApprovalProjectMonitoringFleet;
            $onApproval = $projectApproval->onProject($project->id)->onProgress($progress->id)->latest()->first();
            if ($onApproval) {
                if ($onApproval->status == 1) {
                    \Session::flash('failed', 'Sedang menunggu persetujuan!');
                    return;
                }

                if ($onApproval->status == 2 && $onApproval->type == $projectApproval::APPROVAL_COMPLETION) {
                    \Session::flash('failed', 'Project telah selesai!');
                    return;
                }

                if ($onApproval->status == 2) {
                    \Session::flash('failed', 'Progress telah disetujui. Silakan buat progres baru atau buka kembali!');
                    return;
                }
            }

            $approval = Approval::create([
                'pengaju_id'            => auth()->user()->id,
                'penyetuju_id'          => $request->penyetuju_id,
                'jenis_modul'           => 'IPM Fleet',
                'sub_modul'             => 'Excecutive Summary',
                'tgl_pengajuan'         => date('Y-m-d'),
                'keterangan_pengaju'    => $request->keterangan_pengaju,
                'status'                => Approval::NEW,
            ]);

            if (!empty($request->user_verifikator_id)) {
                $verifikators = [];
                foreach ($request->user_verifikator_id as $id) {
                    array_push($verifikators, [
                        'user_verifikator_id' => $id,
                        'status' => Verifikator::NEW
                    ]);
                }

                $approval->verifikators()->createMany($verifikators);
            }

            $projectApproval::create([
                'approval_id'                   => $approval->id,
                'project_monitoring_fleet_id'   => $project->id,
                'progres_project_fleet_id'      => $project->latest_progress->id,
                'judul_dokumen'                 => $request->judul_dokumen,
                'type'                          => $projectApproval::APPROVAL_SUMMARY,
            ]);

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = 'Executive Summary';
            $param['data'] = __('app.notification.ipm.executive_summary');
            $param['action'] = route('ipm.fleet.summary.index', $project->id);
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.ipm.fleet.subject');
            $param['applicant'] = $approval->pengaju;
            $param['projectMonitoring'] = $project;
            $this->notification->send($param);

            \DB::commit();
            \Session::flash('success', 'Berhasil mengirim approval!');
        } catch (\Exception $e) {
            \DB::rollBack();
            \Session::flash('failed', 'Gagal mengirim approval!');
        }
    }

    public function historyApproval(Request $request, ProjectMonitoringFleet $project)
    {
        try {
            $projectApproval = ApprovalProjectMonitoringFleet::onProject($project->id)->latest()->first();
            $approval = Approval::with('penyetuju', 'pengaju', 'verifikators')
                ->where('id', $projectApproval->approval_id)
                ->first();
            if (!$approval) {
                throw new \Exception('Data tidak ditemukan!');
            }

            $verifikatorStatus = ['Reviewed', 'Rejected', 'Verified'];
            $approverStatus = ['-', 'Reviewed', 'Reviewed', 'Rejected', 'Approved'];
            $data = [
                [
                    'name'          => $approval->pengaju->name,
                    'status'        => 'Submitted',
                    'tanggal'       => $approval->tgl_pengajuan,
                    'keterangan'    => $approval->keterangan_pengaju
                ]
            ];
            foreach ($approval->verifikators as $verifikator) {
                array_push($data, [
                    'name'          => $verifikator->user->name,
                    'status'        => $verifikatorStatus[$verifikator->status],
                    'tanggal'       => $verifikator->tgl_disetujui ?? $verifikator->tgl_tolak_verifikator ?? '-',
                    'keterangan'    => $verifikator->keterangan_verifikator
                ]);
            }
            array_push($data, [
                'name'          => $approval->penyetuju->name,
                'status'        => $approverStatus[$approval->status],
                'tanggal'       => $approval->tgl_penyetujuan ?? $approval->tgl_tolak_penyetuju ?? '-',
                'keterangan'    => $approval->keterangan_penyetuju
            ]);

            return response()->json(['data' => $data]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Something went wrong!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    private function scheduleProgress($progress)
    {
        $i = 0;
        $data = [];
        foreach ($progress as $key => $progres) {
            $data[$i]['Progress Plan'][] = toDecimal($progres['plan']);
            $data[$i]['Progress Actual'][] = toDecimal($progres['aktual']);
            $data[$i]['Progress Deviasi'][] = toDecimal($progres['deviasi']);

            if ((($key + 1) % 15) == 0) {
                $i++;
            }
        }

        return $data;
    }

    private function scheduleQuality()
    {
        $data = [
            [
                'Progress Plan' => [],
                'Progress Actual' => [],
                'Progress Deviasi' => [],
            ]
        ];

        return $data;
    }

    /**
     * Get recomendation projec.
     * 
     * @param ProjectMonitoringFleet $project
     * @return array<object>
     */
    private function recomendation(ProjectMonitoringFleet $project)
    {
        return NoticeRecomendationFleet::selectRaw("notice, to_do_list, users.name AS pic, DATE_FORMAT(due_date, '%Y-%m-%d') AS due_date, status")
            ->join('data_periode_recomendation_fleet', 'data_periode_recomendation_fleet.id', 'data_notice_recomendation_fleet.periode_id')
            ->join('users', 'users.id', '=', 'data_notice_recomendation_fleet.pic')
            ->where('data_periode_recomendation_fleet.project_id', $project->id)
            ->orderBy('data_notice_recomendation_fleet.id')
            ->get();
    }

    /**
     * Get progress of project.
     * 
     * @param ProjectMonitoringFleet $project
     * @return array<object>
     */
    private function progressProject(ProjectMonitoringFleet $project)
    {
        $data = [];
        $progreses = ProgresProjectFleet::where('project_id', $project->id)->get();
        foreach ($progreses as $progres) {
            $item = (new ProgressController)->main_item($progres->id);
            if (count($item) > 0) {
                $data = array_merge($data, $item->toArray());
            }
        }

        return $data;
    }

    /**
     * Get documentation/evidence of project. And formatting to multidimension array
     * 
     * @param ProjectMonitoringFleet $project
     * @return array<object>
     */
    public function documentation(ProjectMonitoringFleet $project)
    {
        if (!$project->latest_progress) {
            return [];
        }

        $documentations = EvidenceProgresFleet::where('progres_id', $project->latest_progress->id)->get()->toArray();
        $format_1 = $format_2 = [];
        $path = public_path('upload/project-monitoring-fleet');
        foreach ($documentations as $key => $document) {
            $document['file_base64'] = base64Converter($path . '/' . $document['file']);
            array_push($format_2, $document);

            if (($key + 1) % 3 == 0) {
                array_push($format_1, $format_2);
                $format_2 = [];
            }
        }

        return $format_1;
    }

    /**
     * Validate approval process
     * 
     * @param ProjectMonitoringFleet $project
     * @return array<object>
     */
    public function approvalStatus(ProjectMonitoringFleet $project)
    {
        $user = auth()->user();
        $approval = null;
        $canApproveOrReject = false;
        $isApproverOrVerificator = false;
        $projectApproval = ApprovalProjectMonitoringFleet::onProject($project->id)->latest()->first();
        if ($projectApproval) {
            $approval = $projectApproval->approval;
            if ($user->id == $approval->penyetuju_id) {
                $isApproverOrVerificator = true;
            }

            if (in_array($user->id, $approval->verifikators->pluck('user_verifikator_id')->toArray())) {
                $isApproverOrVerificator = true;
            }

            $labels = [
                Approval::NEW => 'Reviewed By Manager',
                Approval::REVIEW_BY_MGR => 'Reviewed By Manager',
                Approval::REVIEW_BY_VP => 'Reviewed By VP',
                Approval::REJECTED => 'Rejected',
                Approval::APPROVED => 'Accepted'
            ];

            $approval->approval_status = $labels[$approval->status] ?? '-';
            $approval->judul_dokumen = $projectApproval->judul_dokumen;
            $approval->catatan_project = $projectApproval->catatan_project;
            $approval->type = $projectApproval->type;

            $verifikator = Verifikator::where('approval_id', $approval->id);
            $hasRejected = (clone $verifikator)->isRejected()->exists();
            $hasReviewed = (clone $verifikator)->isReviewed();

            if ($approval->pengaju_id != $user->id && $approval->status != Approval::REJECTED) {
                if ($approval->penyetuju_id == $user->id) {
                    $canApproveOrReject = $hasReviewed->count() == $verifikator->count() && $approval->status != Approval::APPROVED;
                } else {
                    $canApproveOrReject = !(clone $hasReviewed)->whereUser($user->id)->exists();
                }
            }
        }

        return [
            'approval'                  => $approval,
            'can_approve_reject'        => $canApproveOrReject,
            'is_approver_verificator'   => $isApproverOrVerificator
        ];
    }

    public function costPerformanceIndex($project, $progress)
    {
        $last = end($progress);
        $cpi = 0;
        if (!empty($last)) {
            $total = (($last['total_actual'] ?? 0) / 100) * $project->nilai_project;
            $cpi = $total > 0 ? round($last['actual_cost'] / $total, 2) : 0;
        }

        return [
            'cpi'                   => $cpi,
            'actual_cost'           => toRupiah($project->progress->sum('actual_cost')),
            'construction_value'    => toRupiah(0),
            'etc'                   => $last['etc'] ?? 0,
        ];
    }

    public function projectSummary(ProjectMonitoringFleet $project)
    {
        $notice = NoticeRecomendationFleet::countStatus()->selectRaw('periode_id')->groupBy('periode_id');
        $recomendation = PeriodeRecomendationFleet::selectRaw('data_periode_recomendation_fleet.*, total_close, total')
            ->leftJoinSub($notice, 'notice', function ($join) {
                $join->on('notice.periode_id', '=', 'data_periode_recomendation_fleet.id');
            })
            ->whereProject($project->id)
            ->orderBy('periode', 'desc')
            ->first();

        $recomendationSummary = 0;
        if (($row->total ?? 0) > 0) {
            $recomendationSummary = ($row->total_close / $row->total) * 100;
        }

        $document = (new DocumentController)->index(
            new Request(['is_project_index' => true]),
            $project
        );

        $progres = (new ProgressController)->progres_data($project);

        return [
            'document'      => $document['calculated']['total'],
            'recomendation' => toPercent($recomendationSummary, 2),
            'quality'       => toPercent(0, 2),
            'progress'      => toPercent($progres['total_actual'], 2),
        ];
    }
}
