<?php

namespace App\Http\Controllers\CityProvince;

use App\Http\Controllers\Controller;
use App\Http\Services\CityProvince;
use Illuminate\Http\Request;

class CityProvinceController extends Controller
{
    public function get_province() {
        return CityProvince::get_province();
    }

    public function get_cities(Request $request) {
        return CityProvince::get_cities($request->input('id'));
    }
}
