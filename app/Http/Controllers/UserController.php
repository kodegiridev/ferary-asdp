<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id', 'DESC')->get();

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view('pages.user-management.user.index', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);
            $data['role_id'] = $request->role_id;
            $data['created_by'] = Auth::user()->id;

            User::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('users.index')->with(['failed' => 'Gagal Tambah User!']);
        }

        return redirect()->route('users.index')->with(['success' => 'Berhasil Tambah User!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            if ($request->password) {
                $data['password'] = Hash::make($request->password);
            }
            $data['role_id'] = $request->role_id;
            $data['is_active'] = $request->is_active;
            $data['updated_by'] = Auth::user()->id;

            $user->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('users.index')->with(['failed' => 'Gagal Ubah User!']);
        }

        return redirect()->route('users.index')->with(['success' => 'Berhasil Ubah User!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::beginTransaction();

        try {
            $user->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus User!');
        }

        Session::flash('success', 'Berhasil Hapus User!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, User $user)
    {
        DB::beginTransaction();

        try {
            $data['is_active'] = $request->is_active ? 1 : 0;
            $data['updated_by'] = Auth::user()->id;

            $user->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Ubah Status User!');
        }

        Session::flash('success', 'Berhasil Ubah Status User!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = User::with(['role'])->orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('role_id', function ($data) {
                return $data->role->name;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-user"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_user_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-user ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->editColumn('is_active', function ($data) {
                $checked = $data->is_active ? 'checked' : '' ;
                $toggle = '<div class="form-check form-switch form-check-custom form-check-solid">
                    <input class="form-check-input is_active" type="checkbox"
                    role="switch" data-id="'.$data->id.'"
                    '.$checked.'>
                </div>';
                return $toggle;
            })
            ->rawColumns(['action', 'is_active'])
            ->make(true);
    }
}
