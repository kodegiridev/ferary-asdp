<?php

namespace App\Http\Controllers\ElectronicReport;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Models\ElectronicReport\CatatanRevisi;
use Illuminate\Http\Request;
use Auth;
use File;
use DB;
use Session;

use App\Models\ElectronicReport\EReport;
// use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;
use Karriere\PdfMerge\PdfMerge;
use Barryvdh\DomPDF\Facade\Pdf;

class ProgressReportController extends Controller
{
    private $moduleName;
    private $notification;
    private $viewMail;
    private $viewRevisionMail;

    public function __construct() {
        $this->moduleName = 'E-Report Progres';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.e-report.progress.mail';
        $this->viewRevisionMail = 'pages.e-report.progress.revision.mail';
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::user());
        $user = $this->hirarki_user();
        if ($user['role'] == 'Staf') {
            return view('pages.e-report.progress.index_staf');
        }
        return view('pages.e-report.progress.index', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $user = $this->hirarki_user();
        $data = EReport::where('id', $id)->where('status_progres', '<>', 3)->first();
        // dd($data);
        if (empty($data)) {
            return redirect()->route('e-report.progress.index');
        }

        $data->flow             = $this->flow_data($data);
        $data->status_penugasan = $this->statuse($data);
        $dataCatatan = $this->dataCatatan($data->id);
        // if ($user['role'] == 'Staf') {
        //     return view('pages.e-report.progress.detail_staf', compact('data', 'dataCatatan'));
        // }

        return view('pages.e-report.progress.detail', compact('user', 'data', 'dataCatatan'));
    }

    public function complete_progress(Request $request) {
        $report_id = $request->id;

        DB::beginTransaction();

        try {
            $eReport = EReport::find($report_id);
            $status_progres = $this->status_progres($eReport);

            if ($status_progres == 3) {
                $eReport->tgl_penyetujuan = date('Y-m-d');
            }
            $eReport->status_progres = $status_progres;
            $eReport->save();

            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $eReport->judul_penugasan
                ],
                __('app.notification.e_report.progress.approve')
            );

            $param = [];
            $param['users'] = [$eReport->penerima1_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['approver'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            if ($status_progres == 3) {
                $this->handleSelesai($report_id);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->route('e-report.progress.index')->with(['failed' => 'Gagal Menyelesaikan Progres!']);
        }

        return redirect()->route('e-report.progress.index')->with(['success' => 'Berhasil Menyelesaikan Progres!']);
    }

    public function upload_hasil_penugasan(Request $request) {
        $path = public_path('upload/ereport/progres');
        $file = $request->file('chooseFile');
        $report_id = $request->id;

        DB::beginTransaction();

        try {
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }
            $eReport = EReport::find($report_id);
            $filename = $file->getClientOriginalName();
            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
            $file->move($path, $filename);
            
            $eReport->dok_hasil_penugasan = $filename;
            $eReport->status_progres = 1;
            $eReport->save();

            $message = str_replace(
                [
                    '[TITLE]',
                    '[NAME]'
                ],
                [
                    $eReport->judul_penugasan,
                    $eReport->pembuat->name
                ],
                __('app.notification.e_report.progress.submit')
            );

            $param = [];
            $param['users'] = [$eReport->pembuat_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['sender'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->route('e-report.progress.index')->with(['failed' => 'Gagal Upload Hasil Penugasan!']);
        }

        return redirect()->route('e-report.progress.index')->with(['success' => 'Berhasil Upload Hasil Penugasan!']);
    }

    public function catatan_revisi(Request $request) {
        $path = public_path('upload/ereport/progres/catatan');

        $files      = $request->file('file');
        $report_id  = $request->id;
        $catatan    = $request->catatan;
        $referensi    = $request->referensi;
        $tanggal    = $request->tanggal;
        $to_do      = $request->to_do;
        $pembuat_id = Auth::user()->id;

        DB::beginTransaction();

        try {
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $insert_catatan = [];
            if (!empty($catatan)) {
                foreach ($catatan as $i => $v) {
                    $cat  = $v;
                    $ref  = $referensi[$i];
                    $tgl  = $tanggal[$i];
                    
                    $todo = $to_do[$i];

                    $insert = [
                        "ereport_id"    => $report_id,
                        "pembuat_id"    => $pembuat_id,
                        "catatan"       => $cat,
                        "referensi"     => $ref,
                        "tanggal_pembuatan" => date("Y-m-d", strtotime($tgl)),
                        "arahan"            => $todo,
                        "dokumen_tanggapan" => "",
                        "status_perbaikan"  => 0,
                        "created_at"    => date('Y-m-d H:i:s'),
                        "updated_at"    => date('Y-m-d H:i:s'),
                    ];

                    if (!empty($files)) {
                        
                        if (array_key_exists($i, $files)) {
                            $dok  = $files[$i];
                            $filename = $dok->getClientOriginalName();
                            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
                            if ($dok->move($path, $filename)) {
                                $insert["dokumen_tanggapan"] = $filename;
                            }
                        }

                    }

                    $insert_catatan[] = $insert;
                }
            }

            if (!empty($insert_catatan)) {
                $eReport = EReport::find($report_id);
                $eReport->status_progres = 2;
                $eReport->save();
                DB::table('ereport_catatan_revisi')->insert($insert_catatan);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('e-report.progress.detail', $report_id)->with(['failed' => 'Gagal Menambah Catatan Revisi!']);
        }

        return redirect()->back()->with(['success' => 'Berhasil Menambah Catatan Revisi!']);
    }

    public function catatan_revisi_perbaikan(Request $request) {
        $path = public_path('upload/ereport/progres/catatan_revisi');
        $file = $request->file('file');
        $catatan_id = $request->id;
        $catatan_hasil_perbaikan = $request->catatan_hasil_perbaikan;
        $tgl_hasil_perbaikan = date('Y-m-d', strtotime($request->tgl_hasil_perbaikan));

        DB::beginTransaction();

        try {
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777);
            }

            $update = [
                "catatan_hasil_perbaikan"   => $catatan_hasil_perbaikan,
                "tgl_hasil_perbaikan"   => $tgl_hasil_perbaikan,
                "dok_hasil_perbaikan"   => "",
                "status_perbaikan"   => 0,
                "updated_at"            => date('Y-m-d H:i:s')
                
            ];
            // dd($update);
            $filename = $file->getClientOriginalName();
            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
            if ($file->move($path, $filename)) {
                $update["dok_hasil_perbaikan"] = $filename;
            }
            DB::table('ereport_catatan_revisi')->where('id', $catatan_id)->update($update);

            $eReportCatatanRevisi = CatatanRevisi::find($catatan_id);
            $eReport = EReport::find($eReportCatatanRevisi->ereport_id);

            $message = str_replace(
                [
                    '[TITLE]',
                    '[NAME]'
                ],
                [
                    $eReport->judul_penugasan,
                    $eReport->pembuat->name,
                ],
                __('app.notification.e_report.progress.revision.submit')
            );

            $param = [];
            $param['users'] = [$eReport->pembuat_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewRevisionMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['sender'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->back()->with(['failed' => 'Gagal Upload Hasil Penugasan!']);
        }

        return redirect()->back()->with(['success' => 'Berhasil Upload Hasil Penugasan!']);
    }

    public function approve_revision(Request $request) {
        
        $data = DB::table('ereport_catatan_revisi')->where('id', $request->id)->first();
        $report_id = $data->ereport_id;

        $go_index = 0;
        DB::beginTransaction();

        try {
            $update = DB::table('ereport_catatan_revisi')->where('id', $request->id)->update(['status_perbaikan' => 2]);
            $check_open = DB::table('ereport_catatan_revisi')->where('ereport_id', $report_id)->where('status_perbaikan', '<>', 2)->count();
            $eReport = EReport::find($report_id);

            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $eReport->judul_penugasan
                ],
                __('app.notification.e_report.progress.revision.approve')
            );

            $param = [];
            $param['users'] = [$eReport->penerima1_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewRevisionMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['approver'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            if ($check_open < 1) {
                $status_progres = $this->status_progres($eReport);

                if ($status_progres == 3) {
                    $eReport->tgl_penyetujuan = date('Y-m-d');
                }
                $eReport->status_progres = $status_progres;
                $eReport->save();

                if ($status_progres == 3) {
                    $this->handleSelesai($report_id);

                    $message = str_replace(
                        [
                            '[TITLE]'
                        ],
                        [
                            $eReport->judul_penugasan
                        ],
                        __('app.notification.e_report.progress.approve')
                    );

                    $param = [];
                    $param['users'] = [$eReport->penerima1_id];
                    $param['title'] = $this->moduleName;
                    $param['data'] = $message[0];
                    $param['action'] = route('e-report.progress.index');
                    $param['view'] = $this->viewMail;
                    $param['subject'] = __('app.mail.e_report.progress.subject');
                    $param['approver'] = Auth::user();
                    $param['e_report'] = $eReport;
                    $this->notification->send($param);
                }
                $go_index = 1;
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->back()->with(['failed' => 'Gagal Menyetujui Catatan Revisi!']);
        }

        if ($go_index == 1) {
            return redirect()->route('e-report.progress.index')->with(['success' => 'Berhasil Menyetujui Catatan Revisi!']);
        }
        
        return redirect()->back()->with(['success' => 'Berhasil Menyetujui Catatan Revisi!']);
    }

    public function penolakan_revision(Request $request) {
        $data = DB::table('ereport_catatan_revisi')->where('id', $request->id)->first();
        $report_id = $data->ereport_id;
        DB::beginTransaction();

        try {
            $update = DB::table('ereport_catatan_revisi')->where('id', $request->id)->update(['status_perbaikan' => 1, 'keterangan_penolakan' => $request->keterangan_penolakan]);

            $eReport = EReport::find($data->ereport_id);
            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $eReport->judul_penugasan
                ],
                __('app.notification.e_report.progress.revision.reject')
            );

            $param = [];
            $param['users'] = [$eReport->penerima1_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewRevisionMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['rejecter'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->back()->with(['failed' => 'Gagal Menolak Catatan Revisi!']);
        }

        return redirect()->route('e-report.progress.detail', $report_id)->with(['success' => 'Berhasil Menolak Catatan Revisi!']);
    }

    public function delete_revision(Request $request) {
        
        $data = DB::table('ereport_catatan_revisi')->where('id', $request->id)->first();
        $report_id = $data->ereport_id;
        $go_index = 0;
        DB::beginTransaction();
        try {
            $delete = DB::table('ereport_catatan_revisi')->where('id', $request->id)->delete();
            $check_open = DB::table('ereport_catatan_revisi')->where('ereport_id', $report_id)->where('status_perbaikan', '<>', 2 )->count();
            if ($check_open < 1) {
                $eReport = EReport::find($report_id);
                $eReport->status_progres = 3;
                $eReport->save();
                $go_index = 1;
            }

            DB::commit();
        } catch (\Exception $e) {        
            // dd($e->getMessage());
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal Menghapus Catatan Revisi!']);
        }

        if ($go_index == 1) {
            return redirect()->route('e-report.progress.index')->with(['success' => 'Berhasil Menghapus Catatan Revisi!']);
        }

        return redirect()->back()->with(['success' => 'Berhasil Menghapus Catatan Revisi!']);
    }
    
    public function approve_all_revision(Request $request) {
        $report_id = $request->ereport_id;
        DB::beginTransaction();

        try {
            $update = DB::table('ereport_catatan_revisi')->where('ereport_id', $report_id)->update(['status_perbaikan' => 2]);
            $eReport = EReport::find($report_id);
            $status_progres = $this->status_progres($eReport);

            if ($status_progres == 3) {
                $eReport->tgl_penyetujuan = date('Y-m-d');
            }
            $eReport->status_progres = $status_progres;
            $eReport->save();

            if ($status_progres == 3) {
                $this->handleSelesai($report_id);

                $message = str_replace(
                    [
                        '[TITLE]'
                    ],
                    [
                        $eReport->judul_penugasan
                    ],
                    __('app.notification.e_report.progress.approve')
                );

                $param = [];
                $param['users'] = [$eReport->penerima1_id];
                $param['title'] = $this->moduleName;
                $param['data'] = $message[0];
                $param['action'] = route('e-report.progress.index');
                $param['view'] = $this->viewMail;
                $param['subject'] = __('app.mail.e_report.progress.subject');
                $param['approver'] = Auth::user();
                $param['e_report'] = $eReport;
                $this->notification->send($param);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->back()->with(['failed' => 'Gagal Menyetujui Semua Catatan Revisi!']);
        }

        return redirect()->route('e-report.progress.index')->with(['success' => 'Berhasil Menyetujui Semua Catatan Revisi!']);
    }

    public function penolakan_vp(Request $request) {
        $path = public_path('upload/ereport/progres/');
        $file = $request->file('file');
        $keterangan_penolakan = $request->keterangan_penolakan;
        $ereport_id = $request->ereport_id;
        // dd($file, $keterangan_penolakan, $ereport_id, $request->all());

        DB::beginTransaction();

        try {
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777);
            }

            $update = [
                "keterangan_penolakan_vp"   => $keterangan_penolakan,
                "file_penolakan_vp"     => "",
                "status_progres"        => 2
            ];
            // dd($update);
            $filename = $file->getClientOriginalName();
            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
            if ($file->move($path, $filename)) {
                $update["file_penolakan_vp"] = $filename;
            }
            $eReport = EReport::where('id', $ereport_id)->update($update);
            $eReport = EReport::where('id', $ereport_id)->first();

            $message = str_replace(
                [
                    '[TITLE]'
                ],
                [
                    $eReport->judul_penugasan
                ],
                __('app.notification.e_report.progress.reject')
            );

            $param = [];
            $param['users'] = [$eReport->penerima1_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.progress.index');
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.e_report.progress.subject');
            $param['rejecter'] = Auth::user();
            $param['e_report'] = $eReport;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->back()->with(['failed' => 'Gagal Menolak Hasil Progres!']);
        }

        return redirect()->back()->with(['success' => 'Berhasil Menolak Hasil Progres!']);
    }

    public function detail_catatan($id) {
        $data = DB::table('ereport_catatan_revisi')->where('id', $id)->first();
        return response()->json($data);
    }

    public function datatablesManager()
    {
        $user = $this->hirarki_user();
        $query = EReport::with('pembuat', 'penerima1', 'penerima2', 'revisi')
            ->where('status_penugasan', 3)
            ->whereIn('status_progres', [0, 1, 2, 4])
            ->where(function ($q) use($user) {
                $q->where('penerima1_id', $user['user']->id)
                ->orWhere('pembuat_id', $user['user']->id);
            });
        $data = $query->get();
        $today = strtotime(date('Y-m-d'));
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return  '<a class="btn btn-sm btn-secondary"
                    href="' . route('e-report.progress.detail', $row->id) . '">
                        ' . __('Detail') . '
                    </a>';
            })
            ->addColumn('status', function ($row) use ($today) {
                $deadline_date = strtotime($row->target_waktu);
                if ($deadline_date < $today) {
                    return 'EXPIRED';
                }
                return 'ACTIVE';
            })
            ->addColumn('pengirim', function ($row) use ($today) {
                return $row->pembuat->role->name;
             })
            ->addColumn('status_penugasan', function ($row) use ($today) {
                $status = $this->statuse($row);
                return $status;  
            })
            ->editColumn('target_waktu', function ($row) use ($today) {
                return date('d-m-Y', strtotime($row->target_waktu));
             })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function datatablesVP()
    {
        $user = $this->hirarki_user();
        $query = EReport::with('pembuat', 'penerima1', 'penerima2', 'revisi')
            ->where('status_penugasan', 3)
            ->whereIn('status_progres', [0, 1, 2, 4])
            ->where(function ($q) use($user) {
                $q->where('penerima1_id', $user['user']->id)
                ->orWhere('pembuat_id', $user['user']->id);
            });
        $data = $query->get();
        $today = strtotime(date('Y-m-d'));
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return  '<a class="btn btn-sm btn-secondary"
                    href="' . route('e-report.progress.detail', $row->id) . '">
                        ' . __('Detail') . '
                    </a>';
            })
            ->addColumn('status', function ($row) use ($today) {
                $deadline_date = strtotime($row->target_waktu);
                if ($deadline_date < $today) {
                    return 'EXPIRED';
                }
                return 'ACTIVE';
            })
            ->addColumn('pic', function ($row) use ($today) {
                return $row->penerima1->role->name;
             })
            ->addColumn('status_penugasan', function ($row) use ($today) {
                $status = $this->statuse($row);
                return $status;  
            })
            ->editColumn('target_waktu', function ($row) use ($today) {
                return date('d-m-Y', strtotime($row->target_waktu));
             })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function datatablesStaff()
    {
        $user = $this->hirarki_user();
        $query = EReport::with('pembuat', 'penerima1', 'revisi')
            ->where('status_penugasan', 3)
            ->whereIn('status_progres', [0, 1, 2, 4]);
        if ($user['role'] == 'Staf') {
            $query->where(function ($q) use($user) {
                $q->where('penerima1_id', $user['user']->id)
                ->orWhere('penerima2_id', $user['user']->id);
            });
        } else {
            $query->where('status_progres', 1)->whereNotNull("penerima2_id")->with(["penerima2" => function($q) use ($user) {
                $q->whereIn('users.role_id', $user['staf_role']);
            }]);
        }
        $data = $query->get();
        $today = strtotime(date('Y-m-d'));
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return  '<a class="btn btn-sm btn-secondary"
                    href="' . route('e-report.progress.detail', $row->id) . '">
                        ' . __('Detail') . '
                    </a>';
            })
            ->addColumn('status', function ($row) use ($today) {
                $deadline_date = strtotime($row->target_waktu);
                if ($deadline_date < $today) {
                    return 'EXPIRED';
                }
                return 'ACTIVE';
            })
            ->addColumn('pengirim', function ($row) use ($today) {
                return $row->pembuat->role->name;
             })
            ->addColumn('pic', function ($row) use ($today) {
                return $row->penerima2?->role->name.' ( '.$row->penerima2?->name.' )';
            })
            ->addColumn('status_penugasan', function ($row) use ($today) {
                $status = $this->statuse($row);
                return $status;  
            })
            ->editColumn('target_waktu', function ($row) use ($today) {
                return date('d-m-Y', strtotime($row->target_waktu));
             })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function datatablesAdmin()
    {
        $user = $this->hirarki_user();
        $query = EReport::with('pembuat', 'penerima1', 'penerima2', 'revisi')
            ->where('status_penugasan', 3)
            ->whereIn('status_progres', [0, 1, 2, 4]);
        $data = $query->get();
        $today = strtotime(date('Y-m-d'));
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return  '<a class="btn btn-sm btn-secondary"
                    href="' . route('e-report.progress.detail', $row->id) . '">
                        ' . __('Detail') . '
                    </a>';
            })
            ->addColumn('status', function ($row) use ($today) {
                $deadline_date = strtotime($row->target_waktu);
                if ($deadline_date < $today) {
                    return 'EXPIRED';
                }
                return 'ACTIVE';
            })
            ->addColumn('pic', function ($row) use ($today) {
                return $row->penerima1->role->name;
             })
            ->addColumn('status_penugasan', function ($row) use ($today) {
                $status = $this->statuse($row);
                return $status;  
            })
            ->editColumn('target_waktu', function ($row) use ($today) {
                return date('d-m-Y', strtotime($row->target_waktu));
             })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function dataCatatan($id)
    {
        $data = DB::table('ereport_catatan_revisi')->where('ereport_id', $id)->get();
        return $data;
    }

    private function statuse($data) {
        $role_pembuat = explode(" ", $data->pembuat->role->name)[0];
        $role_penerima1 = explode(" ", $data->penerima1->role->name)[0];
        $role_penerima2 = empty($data->penerima2_id) ? "" : explode(" ", $data->penerima2?->role->name)[0];

        $role_penerima = !empty($role_penerima2) ? $role_penerima2." (Disposisi)" : $role_penerima1;
        $status = 'Ditugaskan kepada '.$role_penerima;
        if ($data->status_progres == 1) {
            $status = "Menunggu Review VP";
            if (!empty($role_penerima2) || str_contains($role_pembuat, 'Manager')) {
                $status = "Menunggu Review Manajer";
            }
        } elseif ($data->status_progres == 2 && !empty($data->revisi)) {
            $status = 'Penugasan Ditolak (Perbaikan)';
        } elseif ($data->status_progres == 2) {
            $status = 'Penugasan Ditolak';
        } elseif ($data->status_progres == 4) {
            $status = 'Menunggu Review VP';
        }

        if ($data->status_progres == 2 && !empty($data->keterangan_penolakan_vp)) {
            $status = 'Penugasan Ditolak (Disposisi)';
        }
        return $status;
    }

    private function status_progres(EReport $eReport) {
        if (!empty($eReport->penerima2_id) && $eReport->status_progres != 4) {
            // Disposition Data
            return 4; 
        }
        return 3;
    }

    private function flow_data($data) {
        $approval_by    = $data->pembuat_id;
        $submit_by      = $data->penerima1_id;
        $pembuat_role   = explode(" ", $data->pembuat->role->name)[0];
        $penerima_role  = explode(" ", $data->penerima1->role->name)[0];
        if (!empty($data->penerima2_id && $data->status_progres != 4)) {
            $approval_by    = $data->penerima1_id;
            $submit_by      = $data->penerima2_id;
            $pembuat_role   = explode(" ", $data->penerima1->role->name)[0];
            $penerima_role  = explode(" ", $data->penerima2->role->name)[0];
        }

        $data = [
            "pembuat_id"    => $approval_by,
            "penerima_id"   => $submit_by,
            "pembuat_role"  => $pembuat_role,
            "penerima_role" => $penerima_role
        ];
        return $data;
    }

    private function hirarki_user() {
        $role_id = Auth::user()->role->id;
        $mrole_id = [3, 4];
        $srole_id = [5, 6, 7, 8, 9, 10, 11, 12, 13];

        $roles_id   = [];
        $roles[3]   = [5, 6, 7, 8, 9, 10];
        $roles[4]   = [11, 12];

        if ($role_id == 1) {
            $data = [
                "role"      => 'Admin',
                "staf_role" => [],
            ];
        } else {
            if ($role_id == 2) {
                $data = [
                    "role" => 'VP',
                    "staf_role" => $mrole_id,
                ];
            } else {
                $data = [
                    "role" => in_array($role_id, $mrole_id) ? 'Manajer' : 'Staf',
                    "staf_role" => in_array($role_id, $mrole_id) ? $roles[$role_id] : [],
                ];
            }
        }
        $data["user"] = Auth::user();

        return $data;
    }

    private function handleSelesai($ereport_id)
    {
        $path = public_path('upload/ereport/progres/');
        $e_report = EReport::find($ereport_id);
        if ($e_report) {
            $status = 2;
            if ($e_report->target_waktu < now()->format('Y-m-d')) {
                $status = 3;
            }
            if ($e_report->target_waktu > now()->format('Y-m-d')) {
                $status = 1;
            }

            if ($e_report->dok_hasil_penugasan && File::exists($path . $e_report->dok_hasil_penugasan)) {
                // path validation page with QR code
                $dok_persetujuan = $path . 'qr_' . time() . '.pdf';

                // generate validation page with QR code
                $validation_url = route('qr-code.e-report.selesai.hasil-penugasan', ['data' => encryptor($ereport_id)]);
                $qr_code = \QrCode::size(150)->generate($validation_url);
                $dok_validation = Pdf::loadView('pages.e-report.finish.pdf.approval-penugasan', compact('e_report', 'qr_code'));
                $dok_validation->save($dok_persetujuan);

                // path uploaded pdf file "hasil penugasan"
                $dok_penugasan = $path . $e_report->dok_hasil_penugasan;

                // merge "hasil penugasan" with QR code
                $pdfMerge = new PdfMerge();
                $pdfMerge->add($dok_penugasan);
                $pdfMerge->add($dok_persetujuan);

                // replace uploaded "hasil penugasan" with merged file
                $pdfMerge->merge($dok_penugasan);

                // delete generated QR code
                if (File::exists($dok_persetujuan)) {
                    File::delete($dok_persetujuan);
                }
            }

            $e_report->status_penyelesaian = $status;
            $e_report->save();
        }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
