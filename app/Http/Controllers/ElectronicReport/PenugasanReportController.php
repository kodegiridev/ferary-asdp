<?php

namespace App\Http\Controllers\ElectronicReport;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Models\ElectronicReport\EReport;
use App\Models\ElectronicReport\TanggapanPenugasan;
use App\Models\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;

class PenugasanReportController extends Controller
{
    public $status_message = [
        '0' => 'Diajukan ke ',
        '1' => 'Menunggu persetujuan perencanaan tugas ke ',
        '2' => 'Permohonan target di Tolak ',
        '4' => 'Diajukan ke Staff(Disposisi)'
    ];

    public $disposisi_message = [
        '0' => null,
        '1' => '(Disposisi)'
    ];

    private $moduleName;
    private $notification;
    private $viewMail;
    private $viewTargetAchievementMail;

    public function __construct()
    {
        $this->moduleName = 'E-Report Penugasan';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.e-report.penugasan.mail';
        $this->viewTargetAchievementMail = 'pages.e-report.penugasan.target-achievement.mail';
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $role_user = $this->getUserRole();
        if ($role_user == 'manager') {
            $staff = User::whereHas('role', function ($query) use ($role_user) {
                $query->where('name', 'LIKE', "%Staff%");
            })->get();
            if ($request->ajax()) {
                return $this->datatbManager();
            }
            return view('pages.e-report.penugasan.manager.index', compact('staff'));
        } else if ($role_user == 'staff') {
            if ($request->ajax()) {
                return $this->datatableStaff();
            }
            return view('pages.e-report.penugasan.staff.index');
        } else {
            $manager = User::whereHas('role', function ($query) use ($role_user) {
                $query->where('name', 'LIKE', "%Manager%");
            })->get();
            if ($request->ajax()) {
                return $this->datatableVP();
            }
            return view('pages.e-report.penugasan.index', compact('manager'));
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data['judul_penugasan'] = $request->title;
            $data['pembuat_id'] = Auth::user()->id;
            $data['penerima1_id'] = $request->manager_id;
            $data['keterangan_penugasan'] = $request->keterangan;
            $data['dokumen_penugasan'] = $this->uploadFile($request->file('dokumen'), 'ereport/penugasan/dokumen_penugasan');

            $e_report = EReport::create($data);

            $message = str_replace(
                [
                    '[TITLE]',
                    '[NAME]'
                ],
                [
                    $e_report->judul_penugasan,
                    $e_report->penerima1->name
                ],
                __('app.notification.e_report.assignment.submit')
            );

            $param = [];
            $param['users'] = [$request->manager_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.penugasan.index');
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.e_report.assignment.subject');
            $param['sender'] = Auth::user();
            $param['e_report'] = $e_report;
            $this->notification->send($param);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('e-report.penugasan.index')->with(['failed' => $th->getMessage()]);
        }

        return redirect()->route('e-report.penugasan.index')->with(['success' => 'Berhasil Tambah Penugasan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Branch $branch
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $detail = EReport::with('pembuat', 'penerima1', 'tanggapan')
            ->where('id', $request->id)->first();
        $detail->status = $detail->status_penugasan;
        if ($detail->status_penugasan == 0 || $detail->status_penugasan == 1) {
            if ($detail->disposisi == 1 && $detail->verify == 0) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->penerima2_id) . $this->disposisi_message[$detail->disposisi];
            } else if ($detail->disposisi == 1 && $detail->verify == 1) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->penerima1_id) . $this->disposisi_message[$detail->disposisi];
            } else if ($detail->disposisi == 1 && $detail->verify == 2) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->pembuat_id) . $this->disposisi_message[$detail->disposisi];
            } else if ($detail->disposisi == 0 && $detail->penerima2_id == null && $detail->verify == 0) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->penerima1_id);
            } else if ($detail->disposisi == 0 && $detail->penerima2_id == null && $detail->verify == 1) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->pembuat_id);
            } else {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->penerima1_id);
            }
        } else {
            if ($detail->disposisi && $detail->status_penugasan == 1 && $detail->verify == 1 || $detail->disposisi && $detail->status_penugasan == 2 && $detail->verify == 1) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->penerima1_id) . $this->disposisi_message[$detail->disposisi];
            } else if ($detail->disposisi && $detail->status_penugasan == 2 && $detail->verify == 2) {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->getUserRoleById($detail->pembuat_id) . $this->disposisi_message[$detail->disposisi];
            } else {
                $detail->status_penugasan = $this->status_message[$detail->status_penugasan] . $this->disposisi_message[$detail->disposisi];
            }
        }


        $role = $this->getuserRoleById(Auth::user()->id);
        if (!str_contains($role, 'Staff')) {
            if ($detail->status == 1) {
                $message = str_replace("ke", "", $this->status_message[$detail->status]);
                $detail->status_penugasan = $message . $this->disposisi_message[$detail->disposisi];
            } elseif ($detail->status == 2) {
                $detail->status_penugasan = $this->status_message[$detail->status] . $this->getUserRoleById($detail->pembuat_id) . $this->disposisi_message[$detail->disposisi];
            }
        }
        return $detail;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request)
    {
        $detail = EReport::with('pembuat')->where('id', $request->id)->first();
        return $detail;
    }

    public function update(Request $request, EReport $id)
    {
        DB::beginTransaction();
        try {
            $ereport = $id;
            $data['judul_penugasan'] = $request->title;
            $data['pembuat_id'] = Auth::user()->id;
            $data['penerima1_id'] = $request->manager_id;
            $data['keterangan_penugasan'] = $request->keterangan;
            if ($request->file('dokumen')) {
                $data['dokumen_penugasan'] = $this->uploadFile($request->file('dokumen'), 'ereport/penugasan/dokumen_penugasan');
            }
            $ereport->update($data);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('e-report.penugasan.index')->with(['failed' => $th->getMessage()]);
        }

        return redirect()->route('e-report.penugasan.index')->with(['success' => 'Berhasil Update Penugasan!']);
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();

        try {
            TanggapanPenugasan::where('ereport_id', $request->id)->delete();
            $data = EReport::find($request->id);
            $data->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Data Penugasan');
        }

        Session::flash('success', 'Berhasil Hapus Data Penugasan!');
    }

    public function disposisi(Request $request)
    {
        DB::beginTransaction();
        try {
            EReport::where('id', $request->id)->update([
                'penerima2_id' => $request->user_id
            ]);

            $e_report = EReport::find($request->id);
            $message = str_replace(
                [
                    '[TITLE]',
                    '[NAME]'
                ],
                [
                    $e_report->judul_penugasan,
                    $e_report->penerima2->name
                ],
                __('app.notification.e_report.assignment.disposition')
            );

            $param = [];
            $param['users'] = [$request->user_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.penugasan.index');
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.e_report.assignment.subject');
            $param['sender'] = Auth::user();
            $param['e_report'] = $e_report;
            $this->notification->send($param);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('e-report.penugasan.index')->with(['failed' => 'Error disposisi tugas']);
        }

        return redirect()->route('e-report.penugasan.index')->with(['success' => 'Disposisi penugasan berhasil.']);
    }

    public function sendPenugasan(Request $request)
    {
        $user = Auth::user()->id;
        DB::beginTransaction();
        try {
            $data = EReport::find($request->id);
            if ($data->status == 2) {
                TanggapanPenugasan::where('ereport_id', $request->id)->delete();
            }

            $data->update([
                'target_waktu' => $request->target_waktu,
                'target_pencapaian' => $request->target_pencapaian,
                'status_penugasan' => $request->status
            ]);

            $message = str_replace(
                [
                    '[TITLE]',
                    '[NAME]'
                ],
                [
                    $data->judul_penugasan,
                    $data->penerima1->name
                ],
                __('app.notification.e_report.assignment.target.submit')
            );

            $param = [];
            $param['users'] = [$data->pembuat_id];
            $param['title'] = $this->moduleName;
            $param['data'] = $message[0];
            $param['action'] = route('e-report.penugasan.index');
            $param['view'] = $this->viewTargetAchievementMail;
            $param['subject'] = __('app.mail.e_report.assignment.subject');
            $param['sender'] = Auth::user();
            $param['e_report'] = $data;
            $this->notification->send($param);

            if ($user == $data->penerima2_id) {
                $tanggapan = TanggapanPenugasan::where('ereport_id', $request->id)
                    ->where('pembuat_id', $data->penerima1_id)->first();
                if ($tanggapan) {
                    TanggapanPenugasan::where('ereport_id', $request->id)
                        ->where('pembuat_id', $data->penerima1_id)->update([
                            'status' => $request->status
                        ]);
                } else {
                    TanggapanPenugasan::create([
                        'ereport_id' => $request->id,
                        'pembuat_id' => $data->penerima1_id,
                        'status' => 0
                    ]);
                }
            } else if ($user == $data->penerima1_id) {
                $tanggapan = TanggapanPenugasan::where('ereport_id', $request->id)->where('pembuat_id', $data->pembuat_id)->first();
                if ($tanggapan) {
                    TanggapanPenugasan::where('ereport_id', $request->id)->where('pembuat_id', $data->pembuat_id)->update([
                        'status' => $request->status
                    ]);
                } else {
                    TanggapanPenugasan::create([
                        'ereport_id' => $request->id,
                        'pembuat_id' => $data->pembuat_id,
                        'status' => 0
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('e-report.penugasan.index')->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('e-report.penugasan.index')->with(['success' => 'Pengajuan penugasan berhasil.']);
    }

    public function statusUpdate(Request $request)
    {
        $tanggapan = [
            'status' => $request->status,
            'keterangan' => $request->keterangan
        ];

        DB::beginTransaction();
        try {
            $data = TanggapanPenugasan::where('ereport_id', $request->id)->where('pembuat_id', Auth::user()->id);
            if ($data) {
                $data->update($tanggapan);
            } else {
                TanggapanPenugasan::create($tanggapan);
            }

            EReport::where('id', $request->id)->update([
                'status_penugasan' => $request->status
            ]);

            $e_report = EReport::find($request->id);
            if ($e_report->status_penugasan == 2) {
                $message = str_replace(
                    [
                        '[TITLE]'
                    ],
                    [
                        $e_report->judul_penugasan
                    ],
                    __('app.notification.e_report.assignment.target.reject')
                );

                $param = [];
                $param['users'] = [$e_report->penerima1_id];
                $param['title'] = $this->moduleName;
                $param['data'] = $message[0];
                $param['action'] = route('e-report.penugasan.index');
                $param['view'] = $this->viewTargetAchievementMail;
                $param['subject'] = __('app.mail.e_report.assignment.subject');
                $param['rejecter'] = Auth::user();
                $param['e_report'] = $e_report;
                $this->notification->send($param);
            }

            if ($e_report->status_penugasan == 3) {
                $message = str_replace(
                    [
                        '[TITLE]'
                    ],
                    [
                        $e_report->judul_penugasan
                    ],
                    __('app.notification.e_report.assignment.target.approve')
                );

                $param = [];
                $param['users'] = [$e_report->penerima1_id];
                $param['title'] = $this->moduleName;
                $param['data'] = $message[0];
                $param['action'] = route('e-report.penugasan.index');
                $param['view'] = $this->viewTargetAchievementMail;
                $param['subject'] = __('app.mail.e_report.assignment.subject');
                $param['approver'] = Auth::user();
                $param['e_report'] = $e_report;
                $this->notification->send($param);
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('failed', $th->getMessage());
        }

        return redirect()->back()->with('success', 'Berhasil update status penugasan');
    }

    public function managerStatusUpdate(Request $request)
    {
        $tanggapan = [
            'status' => $request->status,
            'keterangan' => $request->keterangan ?? null
        ];

        DB::beginTransaction();
        try {

            $data = EReport::find($request->id)->first();
            if ($request->status != 2) {
                TanggapanPenugasan::insert([
                    'pembuat_id' => $data->pembuat_id,
                    'ereport_id' => $request->id,
                    'status' => 0
                ]);
            }

            TanggapanPenugasan::where('ereport_id', $request->id)->where('pembuat_id', Auth::user()->id)->update($tanggapan);
            EReport::where('id', $request->id)->update([
                'status_penugasan' => $request->status
            ]);

            $e_report = EReport::find($request->id);
            if ($e_report->status_penugasan == 2) {
                $message = str_replace(
                    [
                        '[TITLE]'
                    ],
                    [
                        $e_report->judul_penugasan
                    ],
                    __('app.notification.e_report.assignment.target.reject')
                );

                $param = [];
                $param['users'] = [$e_report->penerima1_id];
                $param['title'] = $this->moduleName;
                $param['data'] = $message[0];
                $param['action'] = route('e-report.penugasan.index');
                $param['view'] = $this->viewTargetAchievementMail;
                $param['subject'] = __('app.mail.e_report.assignment.subject');
                $param['rejecter'] = Auth::user();
                $param['e_report'] = $e_report;
                $this->notification->send($param);
            }

            if ($e_report->status_penugasan == 3) {
                $message = str_replace(
                    [
                        '[TITLE]'
                    ],
                    [
                        $e_report->judul_penugasan
                    ],
                    __('app.notification.e_report.assignment.target.approve')
                );

                $param = [];
                $param['users'] = [$e_report->penerima1_id];
                $param['title'] = $this->moduleName;
                $param['data'] = $message[0];
                $param['action'] = route('e-report.penugasan.index');
                $param['view'] = $this->viewTargetAchievementMail;
                $param['subject'] = __('app.mail.e_report.assignment.subject');
                $param['approver'] = Auth::user();
                $param['e_report'] = $e_report;
                $this->notification->send($param);
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('e-report.penugasan.index')->with('failed', $th->getMessage());
        }

        return redirect()->route('e-report.penugasan.index')->with('success', 'Berhasil update status penugasan');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatableVP()
    {
        $data = EReport::with('pembuat')
            ->where('pembuat_id', Auth::user()->id)
            ->where('status_penugasan', '<', 3)
            ->where('status_progres', 0)
            ->where('status_penyelesaian', 0)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('status_penugasan', function ($data) {
                if ($data->penerima1_id == Auth::user()->id && $data->penerima2_id) {
                    return $this->status_message[4];
                } else if ($data->status_penugasan == 0) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id);
                } else if ($data->disposisi && $data->status_penugasan == 1) {
                    return $this->status_message[$data->status_penugasan] . $this->disposisi_message[$data->disposisi];;
                } else if ($data->disposisi && $data->verify == 2) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->pembuat_id) . $this->disposisi_message[$data->disposisi];
                } else {
                    $message = str_replace("ke", "", $this->status_message[$data->status_penugasan]);
                    return $message . $this->disposisi_message[$data->disposisi];
                };
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                class="btn btn-sm btn-secondary btn-detail-penugasan"
                data-bs-toggle="modal"
                data-bs-target="#detail_penugasan_modal"
                data-id=' . $data['id'] . '>
                    ' . __('Detail') . '
                </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-success btn-edit-penugasan ms-2"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_penugasan_modal"
                    data-id=' . $data['id'] . '>
                        ' . __('Ubah') . '
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-penugasan ms-2"
                    data-id=' . $data['id'] . '>
                        ' . __('Hapus') . '
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatbManager()
    {
        $data = EReport::with('pembuat')
            ->where('penerima1_id', Auth::user()->id)
            ->where('status_penugasan', '<', 3)
            ->where('status_progres', 0)
            ->where('status_penyelesaian', 0)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('status_penugasan', function ($data) {
                if ($data->disposisi && $data->status_penugasan == 0) {
                    return $this->status_message[4];
                } else if ($data->status_penugasan == 0) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id);
                } else if ($data->disposisi && $data->verify == 2) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->pembuat_id) . $this->disposisi_message[$data->disposisi];
                } else {
                    $message = str_replace("ke", "", $this->status_message[$data->status_penugasan]);
                    return $message . $this->disposisi_message[$data->disposisi];
                };
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                class="btn btn-sm btn-secondary btn-detail-penugasan"
                data-bs-toggle="modal"
                data-bs-target="#detail_penugasan_modal"
                data-id=' . $data['id'] . '>
                    ' . __('Detail') . '
                </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function datatbStaff()
    {
        $data = EReport::with('pembuat')
            ->where('pembuat_id', Auth::user()->id)
            ->where('penerima2_id', null)
            ->where('status_penugasan', '<', 3)
            ->where('status_progres', 0)
            ->where('status_penyelesaian', 0)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('status_penugasan', function ($data) {
                if ($data->penerima1_id == Auth::user()->id && $data->penerima2_id && $data->status_penugasan == 0) {
                    return $this->status_message[4];
                } else if ($data->status_penugasan == 0) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id);
                } else if ($data->disposisi && $data->status_penugasan == 1) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id) . $this->disposisi_message[$data->disposisi];
                } else if ($data->disposisi && $data->verify == 2) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->pembuat_id) . $this->disposisi_message[$data->disposisi];
                } else {
                    $message = str_replace("ke", "", $this->status_message[$data->status_penugasan]);
                    return $message . $this->disposisi_message[$data->disposisi];
                }
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                class="btn btn-sm btn-secondary btn-detail-penugasan-staff"
                data-bs-toggle="modal"
                data-bs-target="#detail_penugasan_staff_modal"
                data-id=' . $data['id'] . '>
                    ' . __('Detail') . '
                </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-success btn-edit-penugasan ms-2"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_penugasan_modal"
                    data-id=' . $data['id'] . '>
                        ' . __('Ubah') . '
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-penugasan ms-2"
                    data-id=' . $data['id'] . '>
                        ' . __('Hapus') . '
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    protected function datatableStaff()
    {
        $data = EReport::with('pembuat')
            ->where('penerima1_id', Auth::user()->id)
            ->where('status_penugasan', '<', 3)
            ->where('status_progres', 0)
            ->where('status_penyelesaian', 0)
            ->orWhere('penerima2_id', Auth::user()->id)
            ->where('status_penugasan', '<', 3)
            ->where('status_progres', 0)
            ->where('status_penyelesaian', 0)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('status_penugasan', function ($data) {
                if ($data->disposisi && $data->status_penugasan == 0) {
                    return $this->status_message[4];
                } elseif ($data->status_penugasan == 0) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id);
                } elseif ($data->disposisi && $data->status_penugasan == 1) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->penerima1_id) . $this->disposisi_message[$data->disposisi];
                } elseif ($data->disposisi && $data->verify == 2) {
                    return $this->status_message[$data->status_penugasan] . $this->getUserRoleById($data->pembuat_id) . $this->disposisi_message[$data->disposisi];
                } else {
                    $message = str_replace("ke", "", $this->status_message[$data->status_penugasan]);
                    return $message . $this->disposisi_message[$data->disposisi];
                }
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                class="btn btn-sm btn-secondary btn-detail-penugasan"
                data-bs-toggle="modal"
                data-bs-target="#detail_penugasan_modal"
                data-id=' . $data['id'] . '>
                    ' . __('Detail') . '
                </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
