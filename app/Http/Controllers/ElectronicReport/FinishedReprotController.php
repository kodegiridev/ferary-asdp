<?php

namespace App\Http\Controllers\ElectronicReport;

use App\Http\Controllers\Controller;
use App\Models\ElectronicReport\CatatanRevisi;
use App\Models\ElectronicReport\EReport;
use App\Models\Role;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class FinishedReprotController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.e-report.finish.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = EReport::find($id);
        if (!$data) {
            return redirect()->back()->with('failed', 'Data tidak ditemukan.');
        }

        if (isStaff()) {
            $data->pengirim_pic = !$data->penerima2_id
                ? $data->pembuat->name
                : $data->penerima1->name;
        } else {
            $data->pengirim_pic = isVP() ? $data->penerima1->name : $data->pembuat->name;
        }

        $data->dokumen_penugasan_name = $this->getFileName($data->dokumen_penugasan);
        $data->dok_hasil_penugasan_name = $this->getFileName($data->dok_hasil_penugasan);
        $data->status_progres = EReport::$statusPenugasan[$data->status_progres] ?? '-';
        $data->status_penyelesaian = EReport::$statusPenyelesaian[$data->status_penyelesaian] ?? '-';

        return view('pages.e-report.finish.detail', compact('data'));
    }

    /**
     * Display data finish report in datatables
     *
     * @param \Illuminate\Http\Request
     * @return void
     */
    public function datatables(Request $request)
    {
        $user = \Auth::user();
        $data = EReport::orderBy('updated_at', 'desc')
            ->where('status_penyelesaian', '<>', 0)
            ->with([
                'pembuat' => fn ($q) => $q->selectRaw('id, name, role_id')->with('role'),
                'penerima1' => fn ($q) => $q->selectRaw('id, name, role_id')->with('role'),
                'penerima2' => fn ($q) => $q->selectRaw('id, name, role_id')->with('role'),
            ])
            ->selectRaw('
                id
                ,judul_penugasan
                ,target_waktu
                ,target_pencapaian
                ,pembuat_id
                ,penerima1_id
                ,penerima2_id
                ,status_penugasan
                ,status_penyelesaian
            ');

        if (isStaff($user->role->name)) {
            $data->where(function ($query) use ($user) {
                $query
                    ->where('penerima1_id', $user->id)
                    ->orWhere('penerima2_id', $user->id);
            });
        } elseif (isManager($user->role->name)) {
            $data
                ->when($request->data_manager ?? false, function ($query) use ($user) {
                    return $query->where('penerima1_id', $user->id);
                })
                ->when($request->data_staff ?? false, function ($query) use ($user) {
                    return $query->where('pembuat_id', $user->id)
                        ->where(function ($q) use ($user) {
                            $q->where('penerima1_id', $user->id)->whereNotNull('penerima2_id');
                        });
                });
        } elseif (isVP($user->role->name)) {
            $data->where('pembuat_id', $user->id);
        }

        $data = $data->get();
        $datatables = datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('status_penugasan', function ($row) {
                return EReport::$statusPenugasan[$row->status_penugasan] ?? 'Draft';
            })
            ->addColumn('status_penyelesaian', function ($row) {
                return EReport::$statusPenyelesaian[$row->status_penyelesaian] ?? 'On Progress';
            })
            ->addColumn('action', function ($row) {
                return
                    '<a class="btn btn-sm btn-light"
                        href="' . route('e-report.finish.detail', $row['id']) . '">
                        ' . __('Detail') . '
                    </a>';
            });

        if (!isAdmin($user->role->name)) {
            $datatables = $datatables
                ->addColumn('pengirim_pic', function ($row) use ($request) {
                    if ($request->data_staff ?? false) {
                        $user = $row->penerima2->name ?? $row->penerima1->name;
                        $role = $row->penerima2->role->name ?? $row->penerima1->role->name;

                        return $user . ' (' . $role . ')';
                    } else {
                        return isVP() ? $row->penerima1->role->name : $row->pembuat->role->name;
                    }
                });
        } else {
            $datatables = $datatables
                ->addColumn('pengirim', function ($row) {
                    return $row->pembuat->name . ' (' . $row->pembuat->role->name . ')';
                })
                ->addColumn('pic', function ($row) {
                    $user = $row->penerima2->name ?? $row->penerima1->name;
                    $role = $row->penerima2->role->name ?? $row->penerima1->role->name;

                    return $user . ' (' . $role . ')';
                });
        }

        return $datatables
            ->rawColumns(['action', 'status_penugasan', 'status_penyelesaian'])
            ->make(true);
    }



    /**
     * Display data statistic finish report in datatables
     *
     * @return void
     */
    public function datatablesStatistic(Request $request)
    {
        $data = [];
        if ($request->type === 'own') {
            $data = $this->ownStatistic();
        } elseif ($request->type === 'assignee') {
            $data = $this->assigneeStatistic($request);
        }

        return $data;
    }

    /**
     * Data for calendar.
     *
     * @return json
     */
    public function dataCalendar(Request $request)
    {
        $user = \Auth::user();
        $route = route('e-report.finish.detail', ':id');
        $data = EReport::selectRaw("
                target_waktu AS start
                ,target_waktu AS end
                ,'fc-event-primary' AS className
                ,REPLACE('" . $route . "', ':id', id) AS url
            ")
            ->where('status_penyelesaian', '<>', 0)
            ->when(isVP($user->role->name), function ($query) use ($user) {
                return $query->selectRaw("
                        penerima1_id, CONCAT(judul_penugasan, ' (', users.name, ')') AS title
                    ")
                    ->join('users', 'users.id', '=', 'e_report.penerima1_id')
                    ->where('pembuat_id', $user->id);
            })
            ->when(isManager($user->role->name), function ($query) use ($user) {
                return $query->selectRaw("
                        penerima1_id, penerima2_id, CONCAT(judul_penugasan, ' (', users.name, ')') AS title
                    ")
                    ->join('users', function ($join) {
                        $join->on('users.id', '=', 'e_report.penerima1_id')
                            ->orOn('users.id', '=', 'e_report.penerima2_id');
                    })
                    ->where('pembuat_id', $user->id)
                    ->orWhere('penerima1_id', $user->id);
            })
            ->when(isStaff($user->role->name), function ($query) use ($user) {
                return $query->selectRaw("
                        penerima1_id, penerima2_id, CONCAT(judul_penugasan, ' (', users.name, ')') AS title
                    ")
                    ->join('users', function ($join) {
                        $join->on('users.id', '=', 'e_report.penerima1_id')
                            ->orOn('users.id', '=', 'e_report.penerima2_id');
                    })
                    ->where('penerima1_id', $user->id)
                    ->orWhere('penerima2_id', $user->id);
            })
            ->when(isAdmin($user->role->name), function ($query) use ($user) {
                return $query->selectRaw("judul_penugasan AS title");
            })
            ->get();

        return response()->json($data);
    }

    /**
     * Display data finish report in datatables
     *
     * @return void
     */
    public function datatablesHistory($id)
    {
        $data = CatatanRevisi::where('ereport_id', decryptor($id))->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('file', function ($row) {
                return ($row->dokumen_tanggapan ?? false)
                    ? '<a href="' . $row->dokumen_tanggapan . '" class="text-muted text-hover-primary">' . __('Lihat') . '</a>'
                    : '<a class="text-muted">' . __('Tidak ada file') . '</a>';
            })
            ->addColumn('updated_file', function ($row) {
                return ($row->dok_hasil_perbaikan ?? false)
                    ? '<a href="' . $row->dok_hasil_perbaikan . '" class="text-muted text-hover-primary">' . __('Lihat Perbaikan') . '</a>'
                    : '<a class="text-muted">' . __('Tidak ada file') . '</a>';
            })
            ->addColumn('status', function ($row) {
                $labels = [0 => 'Baru', 1 => 'Tolak', 2 => 'Close'];
                return $labels[$row->status_perbaikan] ?? 'Close';
            })
            ->rawColumns(['file', 'updated_file', 'status'])
            ->make(true);
    }

    /**
     * Data for own statistics
     * 
     * @return \PDF
     */
    public function printAssignment()
    {
        $user = \Auth::user();
        $signatureKey = 'Ditandatangani secara digital oleh ' . $user->name . ' sebagai ' . $user->role->name;
        $qrCode = \QrCode::size(150)->generate($signatureKey);

        $userQuery = User::selectRaw('users.id, users.name, roles.name as role_name, role_id')
            ->join('roles', 'roles.id', 'users.role_id')
            ->when(isManager($user->role->name), function ($query) {
                return $query->whereIn('roles.name', Role::$asStaff);
            })
            ->when(isVP($user->role->name), function ($query) {
                return $query->whereIn('roles.name', Role::$asManager);
            });

        $calculate = EReport::selectRaw('
                users.name,
                users.role_name,
                COUNT(IF(status_penyelesaian = 1, 1, null)) AS early,
                COUNT(IF(status_penyelesaian = 2, 1, null)) AS on_time,
                COUNT(IF(status_penyelesaian = 3, 1, null)) AS over_due,
                COUNT(status_penyelesaian) AS total
            ')
            ->where('status_penyelesaian', '<>', 0)
            ->joinSub($userQuery, 'users', function ($join) {
                $join->on('users.id', '=', 'penerima1_id')
                    ->orOn('users.id', '=', 'penerima2_id');
            })
            ->when(!isAdmin(), function ($query) use ($user) {
                return $query->where(function ($q) use ($user) {
                    $q->where('pembuat_id', $user->id)->orWhere('penerima1_id', $user->id);
                });
            })
            ->orderBy('users.role_id')
            ->groupBy('users.id');

        $data = DB::table(DB::raw("({$calculate->toSql()}) as calculate"))
            ->mergeBindings($calculate->toBase())
            ->selectRaw('
                *,
                IF(total > 0, ROUND(((early/total)*100), 0), 0) as early_pct,
                IF(total > 0, ROUND(((on_time/total)*100), 0), 0) as on_time_pct,
                IF(total > 0, ROUND(((over_due/total)*100), 0), 0) as over_due_pct
            ')
            ->get();

        $pdf = \PDF::loadView('pages.e-report.finish.pdf.statistik', compact('data', 'user', 'qrCode'));
        return $pdf->stream('LAPORAN_STATISTIK_SELESAI_' . now()->unix() . '.pdf');
    }

    public function qrCodeValidation(Request $request)
    {
        try {
            $decrypted = decryptor($request->data);

            switch (true) {
                case strpos($request->getUri(), 'hasil-penugasan'):
                    $data = EReport::find($decrypted);
                    if (!$data) {
                        throw new \Exception('invalid', 400);
                    }

                    return view('pages.e-report.finish.qr-code.hasil-penugasan', compact('data'));
                    break;
            }

            throw new \Exception('invalid', 400);
        } catch (\Exception $e) {
            return view('errors.invalid-pdf');
        }
    }

    /**
     * Data for own statistics
     * 
     * @return \Yajra\DataTables\DataTables
     */
    private function ownStatistic()
    {
        $user = \Auth::user();
        $data = EReport::selectRaw('status_penyelesaian, COUNT(status_penyelesaian) AS status')
            ->where('status_penyelesaian', '<>', 0)
            ->when(isStaff($user->role->name), function ($query) use ($user) {
                return $query->where(function ($q) use ($user) {
                    $q->where('penerima1_id', $user->id)
                        ->orWhere('penerima2_id', $user->id);
                });
            })
            ->when(isManager($user->role->name), function ($query) use ($user) {
                return $query->where(function ($q) use ($user) {
                    $q->where('pembuat_id', $user->id)->orWhere('penerima1_id', $user->id);
                });
            })
            ->orderBy('status_penyelesaian')
            ->groupBy('status_penyelesaian')
            ->get();

        $total = $data->sum('status');
        return datatables()->of($data)->addIndexColumn()
            ->addColumn('status', function ($row) {
                return EReport::$statusPenyelesaian[$row->status_penyelesaian];
            })
            ->addColumn('total', function ($row) {
                return $row->status;
            })
            ->addColumn('percentage', function ($row) use ($total) {
                $percentage = $total > 0 ? round(($row->status / $total) * 100, 2) : 0;
                $styles = [1 => 'bg-success', 2 => 'bg-primary', 3 => 'bg-danger'];

                return
                    '<div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated ' . $styles[$row->status_penyelesaian] . '" 
                            style="width: ' . $percentage . '%" 
                        >
                            <div style="width:100%; text-align: center;">' . $percentage . '%</div>
                        </div>
                    </div>';
            })
            ->rawColumns(['status', 'total', 'percentage'])
            ->make(true);
    }

    /**
     * Data for assignee statistics
     * 
     * @param \Illuminate\Http\Request
     * @return \Yajra\DataTables\DataTables
     */
    private function assigneeStatistic(Request $request)
    {
        $data = [];
        $user = \Auth::user();

        $userQuery = User::selectRaw('users.id, users.name, roles.name as role_name')
            ->join('roles', 'roles.id', 'users.role_id')
            ->when(isManager($user->role->name) || $request->role == 'staff', function ($query) {
                return $query->whereIn('roles.name', Role::$asStaff);
            })
            ->when(isVP($user->role->name) || $request->role == 'manager', function ($query) {
                return $query->whereIn('roles.name', Role::$asManager);
            });

        $data = EReport::selectRaw('
                users.name,
                users.role_name,
                COUNT(IF(status_penyelesaian = 1, 1, null)) AS early,
                COUNT(IF(status_penyelesaian = 2, 1, null)) AS on_time,
                COUNT(IF(status_penyelesaian = 3, 1, null)) AS over_due,
                COUNT(status_penyelesaian) AS total
            ')
            ->joinSub($userQuery, 'users', function ($join) {
                $join->on('users.id', '=', 'penerima1_id')
                    ->orOn('users.id', '=', 'penerima2_id');
            })
            ->when(!isAdmin(), function ($query) use ($user) {
                return $query->where(function ($q) use ($user) {
                    $q->where('pembuat_id', $user->id)->orWhere('penerima1_id', $user->id);
                });
            })
            ->where('status_penyelesaian', '<>', 0)
            ->orderBy('users.role_name')
            ->groupBy('users.id')
            ->get();

        return datatables()->of($data)->addIndexColumn()
            ->addColumn('assignee', function ($row) {
                return $row->role_name . ' (' . $row->name . ')';
            })
            ->addColumn('percentage', function ($row) {
                $keys = ['early', 'on_time', 'over_due'];
                $styles = [1 => 'bg-success', 2 => 'bg-primary', 3 => 'bg-danger'];
                $bars = '';
                foreach ($keys as $i => $key) {
                    $percentage = round(($row->$key / $row->total) *  100);
                    $bars .= '
                        <div class="progress-bar progress-bar-striped progress-bar-animated ' . $styles[$i + 1] . '" 
                            style="width: ' . $percentage . '%" 
                        >
                            <div style="width:100%; text-align: center;">' . $percentage . '%</div>
                        </div>
                    ';
                }

                return '<div class="progress">' . $bars . '</div>';
            })
            ->rawColumns(['assignee', 'percentage'])
            ->make(true);
    }
}
