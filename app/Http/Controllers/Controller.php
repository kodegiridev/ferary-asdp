<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use File;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function getUserRole()
    {
        $user = auth()->user()->role->name;
        if (auth()->user()->role_id != 2) {
            $user = explode(" ", $user);

            return strtolower($user[0]);
        } else {
            return $user;
        }
    }

    function getUserRoleById($id)
    {
        $user = User::with('role')->where('id', $id)->first();
        if ($user->role_id != 2) {
            $user = explode(" ", $user->role->name);

            return $user[0];
        } else {
            return $user->role->name;
        }
    }

    static function uploadFile($file, $path)
    {
        if ($file != null) {
            $filename = $file->getClientOriginalName();
            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
            $destinationPath = 'upload/' . $path . '/';

            if (!is_dir(public_path($destinationPath))) {
                File::makeDirectory(public_path($destinationPath), 0777, true);
            }

            if ($file->move(public_path($destinationPath), $filename)) {
                return url($destinationPath) . '/' . $filename;
            } else {
                return null;
            }
        }
    }

    static function uploadFileName($file, $path)
    {
        if ($file != null) {
            $filename = $file->getClientOriginalName();
            $filename = preg_replace('/[^a-zA-Z0-9_.]/', '_', $filename);
            $destinationPath = 'upload/' . $path . '/';

            if (!is_dir(public_path($destinationPath))) {
                File::makeDirectory(public_path($destinationPath), 0777, true);
            }

            if ($file->move(public_path($destinationPath), $filename)) {
                return $filename;
            } else {
                return null;
            }
        }
    }

    static function removeFile($filename, $path)
    {
        if (File::exists(public_path('upload/' . $path . '/' . $filename))) {
            File::delete(public_path('upload/' . $path . '/' . $filename));
        } else {
            dd('File does not exists.', public_path($path . '/' . $filename));
        }
    }

    /**
     * Extract file name from string path.
     * 
     * @param string
     * @return string
     */
    public function getFileName($path)
    {
        $explode = explode('/', $path);
        return end($explode);
    }

    /**
     * Get & convert logo to base64
     * 
     * @return string base64
     */
    public function base64Logo()
    {
        $logoPath = public_path(theme()->getMediaUrlPath() . 'logos/logo.png');

        return "data:image/png;base64," . base64_encode(file_get_contents($logoPath));
    }

    public function getBulan(): array
    {
        $bulan = [
            [
                'id' => '1',
                'name' => 'Januari',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '2',
                'name' => 'Febuari',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '3',
                'name' => 'Maret',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '4',
                'name' => 'April',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '5',
                'name' => 'Mei',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '6',
                'name' => 'Juni',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '7',
                'name' => 'Juli',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '8',
                'name' => 'Augustus',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '9',
                'name' => 'September',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '10',
                'name' => 'Oktober',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '11',
                'name' => 'November',
                'plan' => 0,
                'actual' => 0
            ],
            [
                'id' => '12',
                'name' => 'Desember',
                'plan' => 0,
                'actual' => 0
            ],
        ];
        return $bulan;
    }
}
