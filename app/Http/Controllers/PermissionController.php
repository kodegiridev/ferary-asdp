<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Models\Permission;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listMenu = [
            'user',
            'role',
            'permission'
        ];

        $listAction = [
            'create',
            'read',
            'update',
            'delete',
            'approval',
            'verificator'
        ];

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view('pages.user-management.permission.index', compact('listMenu', 'listAction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;
            $data['menu_name'] = $request->menu_name;
            $data['action'] = $request->action_permission;

            Permission::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('permissions.index')->with(['failed' => 'Gagal Tambah Permission!']);
        }

        return redirect()->route('permissions.index')->with(['success' => 'Berhasil Tambah Permission!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Permission  $permission
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        return $permission;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Permission  $permission
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        DB::beginTransaction();
        try {
            $data['name'] = $request->name;
            $data['menu_name'] = $request->menu_name;
            $data['action'] = $request->action_permission;

            $permission->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('permissions.index')->with(['failed' => 'Gagal Ubah Permission!']);
        }

        return redirect()->route('permissions.index')->with(['success' => 'Berhasil Ubah Permission!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission  $permission
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        DB::beginTransaction();

        try {
            $permission->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Permission!');
        }

        Session::flash('success', 'Berhasil Hapus Permission!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Permission::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action_data', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-permission"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_permission_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-permission ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action_data'])
            ->make(true);
    }
}
