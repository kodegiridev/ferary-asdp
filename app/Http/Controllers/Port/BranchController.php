<?php

namespace App\Http\Controllers\Port;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Port\StoreBranchRequest;
use App\Http\Requests\MasterData\Port\UpdateBranchRequest;
use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\Region;
use DB;
use Illuminate\Http\Request;
use Session;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regions = Region::where('kategori', 2)->orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.port.branch.index', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['regional_id'] = $request->regional_id;
            $data['nama'] = trim($request->nama);
            $data['cabang_kelas'] = $request->cabang_kelas;
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate)) {
                return redirect()->route('port.branch.index')->with(['failed' => 'Branch Sudah Ada!']);
            }
            Branch::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('port.branch.index')->with(['failed' => 'Gagal Tambah Branch!']);
        }

        return redirect()->route('port.branch.index')->with(['success' => 'Berhasil Tambah Branch!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Branch $branch
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return $branch;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Branch $branch
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBranchRequest $request, Branch $branch)
    {
        DB::beginTransaction();
        try {
            $data['regional_id'] = $request->regional_id;
            $data['nama'] = trim($request->nama);
            $data['cabang_kelas'] = $request->cabang_kelas;
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate) && $duplicate->id != $branch->id) {
                return redirect()->route('port.branch.index')->with(['failed' => 'Branch Sudah Ada!']);
            }
            $branch->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('port.branch.index')->with(['failed' => 'Gagal Ubah Branch!']);
        }

        return redirect()->route('port.branch.index')->with(['success' => 'Berhasil Ubah Branch!']);
    }

    protected function check_duplicate($data)
    {
        $duplicate = Branch::where($data)->first();
        return $duplicate;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Branch $branch
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        DB::beginTransaction();

        try {
            $branch->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Branch!');
        }

        Session::flash('success', 'Berhasil Hapus Branch!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Branch::with('regional')->orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('regional_id', function ($data) {
                return $data->regional->nama;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-branch"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_branch_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-branch ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
