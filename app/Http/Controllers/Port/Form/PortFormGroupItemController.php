<?php

namespace App\Http\Controllers\Port\Form;

use App\Http\Controllers\Controller;
use App\Models\Port\ERCM\MstErcmPelabuhanComponent;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentConditionCriteria;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentStandardParameter;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentStandardParameterApprovalParam;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentStandardParameterAreaParam;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentStandardParameterNumberParam;
use App\Models\Port\ERCM\MstErcmPelabuhanComponentStandardParameterRangeParam;
use App\Models\Port\ERCM\MstErcmPelabuhanGroup;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroup;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupConditionCriteria;
use App\Models\Port\ERCM\MstErcmPelabuhanSubgroupStandardParameter;
use App\Models\Port\ERCM\MstErcmPelabuhanSystem;
use App\Models\Port\Port;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PortFormGroupItemController extends Controller
{
    public function index(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        if ($request->ajax()) {
            if ($request->type == 'component') {
                return $this->datatable($group);
            } else {
                return $this->datatableGrouped($port, $form, $group);
            }
        }

        $data = [
            'port'              => $port,
            'form'              => $form,
            'group'             => $group,
            'sub_groups'        => $form->mst_ercm_pelabuhan_subgroups,
            'is_excel'          => str_contains($group->attachment, '.xlsx') || str_contains($group->attachment, '.xls'),
            'assestment_types'  => MstErcmPelabuhanSubgroupStandardParameter::$assestmentTypes,
            'parameters'        => MstErcmPelabuhanSubgroupStandardParameter::$parameterNames,
            'order_types'       => MstErcmPelabuhanSubgroupStandardParameter::$orderTypes,
            'valuation_types'   => MstErcmPelabuhanSubgroupStandardParameter::$valuationTypes,
            'colors'            => MstErcmPelabuhanSubgroupStandardParameter::$colors,
            'criteria_status'   => MstErcmPelabuhanSubgroupConditionCriteria::$status
        ];

        return view('pages.port.form.group.item.index', compact('data'));
    }

    public function landing(Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        $data = [
            'port'          => $port,
            'form'          => $form,
            'group'         => $group,
            'sub_groups'    => $form->mst_ercm_pelabuhan_subgroups
        ];

        return view('pages.port.form.group.item.landing', compact('data'));
    }

    public function upload(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('upload_max_filesize', '1500M');
        ini_set('post_max_size', '1510M');

        DB::beginTransaction();

        try {
            $path = 'master-ercm/port';
            if ($request->is_confirmation) {
                $excepts = null;
                if ($request->exist_components) {
                    $excepts = json_decode($request->exist_components, true);
                }

                $this->storeComponent($request, $port, $form, $group, $excepts);

                if (!$request->not_excel) {
                    $excel = 'upload/' . $path . '/' . $group->attachment;
                    $temp = 'upload/' . $path . '/temp/' . $group->attachment_temp;
                    $group->update([
                        'attachment' => $group->attachment_temp,
                        'attachment_temp' => null
                    ]);

                    // delete temp
                    if (File::exists($temp)) {
                        File::move(public_path($temp), public_path('upload/' . $path . '/' . $group->attachment));
                        File::delete($temp);
                    }

                    // delete excel
                    if (File::exists($excel)) {
                        File::delete($excel);
                    }
                }

                DB::commit();
                return redirect()->route('port.form.group.item.index', [$port->id, $form->id, $group->id])->with('success', 'Berhasil mengunggah 3D file!');
            } else {
                $isExcel = str_contains($group->attachment, '.xlsx') || str_contains($group->attachment, '.xls');
                if ($group->attachment_temp && \File::exists(public_path($path) . '/temp/' . $this->getFileName($group->attachment_temp))) {
                    \File::delete(public_path($path) . '/temp/' . $this->getFileName($group->attachment_temp));
                }

                if ($isExcel) {
                    $group->update([
                        'attachment_temp' => $this->uploadFileName($request->file('3d_file'), $path . '/temp')
                    ]);

                    DB::commit();
                    return redirect()->route('port.form.group.item.confirmation', [$port->id, $form->id, $group->id]);
                } else {
                    $group->update([
                        'attachment' => $this->uploadFileName($request->file('3d_file'), $path)
                    ]);

                    DB::commit();
                    return redirect()->route('port.form.group.item.landing', [$port->id, $form->id, $group->id]);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal mengunggah 3D file!');
        }
    }

    public function confirmation(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        $data = [
            'port'          => $port,
            'form'          => $form,
            'group'         => $group,
            'components'    => $group->mst_ercm_pelabuhan_components
        ];

        return view('pages.port.form.group.item.confirmation', compact('data'));
    }

    public function grouped(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            MstErcmPelabuhanSubgroup::where('id', $request->mst_ercm_pelabuhan_subgroup_id)
                ->update([
                    'mst_ercm_pelabuhan_group_id' => $group->id,
                    'updated_by' => $user->id
                ]);

            MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)
                ->whereIn('id', explode(',', $request->mst_ercm_pelabuhan_components))
                ->update([
                    'mst_ercm_pelabuhan_subgroup_id' => $request->mst_ercm_pelabuhan_subgroup_id,
                    'updated_by' => $user->id
                ]);

            $this->parameterComponentFromParent($request);

            DB::commit();
            return response()->json(['message' => 'Berhasil mengelompokan component!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal mengelompokan component!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function groupedDetail(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        if ($request->ajax() && $request->type) {
            return $this->datatableSubGroup($request, $port, $form, $group);
        }

        $subGroup = MstErcmPelabuhanSubgroup::find($request->mst_ercm_pelabuhan_subgroup_id);
        if (!$subGroup) {
            return response()->json([
                'message' => 'Data tidak ditemukan!',
            ], 500);
        }

        $data = [
            'sub_group' => $subGroup
        ];

        return response()->json($data);
    }

    public function groupedDestroy(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            if ($request->type == 'SubGroup') {
                $components = MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_subgroup_id', $request->mst_ercm_pelabuhan_subgroup_id)->get();
                foreach ($components as $component) {
                    $component->mst_ercm_pelabuhan_component_standard_parameters()->delete();
                }

                MstErcmPelabuhanSubgroup::where('id', $request->mst_ercm_pelabuhan_subgroup_id)
                    ->update([
                        'mst_ercm_pelabuhan_group_id' => null,
                        'updated_by' => $user->id
                    ]);

                MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)
                    ->where('mst_ercm_pelabuhan_subgroup_id', $request->mst_ercm_pelabuhan_subgroup_id)
                    ->update([
                        'mst_ercm_pelabuhan_subgroup_id' => null,
                        'updated_by' => $user->id
                    ]);
            } else {
                $component = MstErcmPelabuhanComponent::find($request->mst_ercm_pelabuhan_component_id);
                if (!$component) {
                    throw new \Exception('Komponen tidak ditemukan');
                }

                $component->mst_ercm_pelabuhan_component_standard_parameters()->delete();

                $hasComponents = MstErcmPelabuhanSubGroup::where('id', $component->mst_ercm_pelabuhan_subgroup_id)
                    ->whereHas('mst_ercm_pelabuhan_components', function ($query) use ($component) {
                        $query->where('id', '!=', $component->id);
                    })
                    ->exists();
                if (!$hasComponents) {
                    MstErcmPelabuhanSubGroup::where('id', $component->mst_ercm_pelabuhan_subgroup_id)
                        ->update([
                            'mst_ercm_pelabuhan_group_id' => null,
                            'updated_by' => $user->id
                        ]);
                }

                $component->mst_ercm_pelabuhan_subgroup_id = null;
                $component->updated_by = $user->id;
                $component->save();
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menghapus data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function showSubGroupParameter(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, MstErcmPelabuhanSubgroup $sub)
    {
        return (new PortFormController)->showSubGroupParameter($request, $port, $sub);
    }

    public function updateSubGRoupParameter(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, MstErcmPelabuhanSubgroup $sub)
    {
        return (new PortFormController)->updateSubGroupParameter($request, $port, $sub);
    }

    public function showComponentParameter(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, MstErcmPelabuhanComponent $component)
    {
        try {
            $component = MstErcmPelabuhanComponent::find($component->id);
            if (!$component) {
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $data = $totalValue = [];
            $stdParam = new MstErcmPelabuhanComponentStandardParameter;
            $orderTypes = [
                1 => 'component_standard_parameter_areas',
                2 => 'component_standard_parameter_number',
                3 => 'component_standard_parameter_ranges',
                4 => 'component_standard_parameter_approvals',
            ];
            $parameters = $stdParam::where('mst_ercm_pelabuhan_component_id', $component->id)
                ->with([
                    'component_standard_parameter_number',
                    'component_standard_parameter_areas',
                    'component_standard_parameter_ranges',
                    'component_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();

            foreach ($stdParam::$assestmentTypes as $key => $value) {
                $i = 0;
                $assestmentParams = $parameters->where('assestment_type', $value)->all();
                $totalValue[$key] = array_sum(array_column($assestmentParams, 'value'));
                foreach ($assestmentParams as $param) {
                    $relation = $orderTypes[$param['order_type']];
                    $param['order_parameters'] = $param[$relation];
                    $param['range_parameters'] = [];

                    if ($param['order_type'] == 3) {
                        $j = 0;
                        $rangeParams = [];
                        $orderParams = collect($param['order_parameters'])->groupBy('color')->all();
                        foreach ($orderParams as $order) {
                            $criterias = collect($order)
                                ->map(function ($val, $idx) {
                                    return ['criteria' => $val['criteria']];
                                })
                                ->all();

                            $rangeParams[$j] = [
                                'color' => $order[0]['color'] ?? null,
                                'range' => $order[0]['range'] ?? null,
                                'criterias' => $criterias
                            ];

                            $j++;
                        }

                        $param['order_parameters'] = [];
                        $param['range_parameters'] = $rangeParams;
                    }

                    $data[$key]['parameters'][$i] = $param;
                    $data[$key]['parameters'][$i]['valuation_type'] = $param['order_parameters']['valuation_type'] ?? $param['order_parameters'][0]['valuation_type'] ?? null;
                    $data[$key]['type_stage'] = $param['type_stage'] ?? null;
                    $data[$key]['assestment_type'] = $value;

                    $i++;
                }
            }

            $data = [
                'title'                 => 'Component: ' . $component->component,
                'port'                  => $port,
                'form'                  => $component->mst_ercm_pelabuhan_group->mst_ercm_pelabuhan_system,
                'group'                 => $component->mst_ercm_pelabuhan_group,
                'component'             => $component,
                'forms'                 => $data,
                'total_value'           => $totalValue,
                'condition_criteria'    => $component->mst_ercm_pelabuhan_component_condition_criteria,
                'is_preview'            => $request->is_preview ?? false,
                'assestment_types'      => MstErcmPelabuhanSubgroupStandardParameter::$assestmentTypes,
                'parameters'            => MstErcmPelabuhanComponentStandardParameter::$parameterNames,
                'type_stages'           => MstErcmPelabuhanComponentStandardParameter::$typeStages,
                'order_types'           => MstErcmPelabuhanComponentStandardParameter::$orderTypes,
                'valuation_types'       => MstErcmPelabuhanComponentStandardParameter::$valuationTypes,
                'colors'                => MstErcmPelabuhanComponentStandardParameter::$colors,
                'criteria_status'       => MstErcmPelabuhanSubgroupConditionCriteria::$status
            ];

            return view('pages.port.form.group.item.form-parameter', compact('data'));
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', 'Gagal mengambil data component parameter');
        }
    }

    public function previewComponentParameter(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, MstErcmPelabuhanComponent $component)
    {
        $request->merge([
            'is_preview' => true
        ]);

        return $this->showComponentParameter($request, $port, $form, $group, $component);
    }

    public function updateComponentParameter(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, MstErcmPelabuhanComponent $component)
    {
        DB::beginTransaction();
        try {
            $data = MstErcmPelabuhanComponent::find($component->id);
            if (!$data) {
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $data->mst_ercm_pelabuhan_component_standard_parameters()->delete();
            $data->mst_ercm_pelabuhan_component_condition_criteria()->delete();

            $this->parameterComponent($request, $data);

            DB::commit();
            return redirect()->back()->with('success', 'Berhasil memperbarui component parameter!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', $e->getMessage());
        }
    }

    private function storeComponent(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group, $exceptComponents = null)
    {
        $components = [];
        $userId = auth()->user()->id;
        if (!$exceptComponents) {
            $group->mst_ercm_pelabuhan_components()->delete();
            foreach ($request->components as $component) {
                $components[] = [
                    'component'         => $component['component'],
                    'component3d_id'    => $component['component3d_id'],
                    'created_by'        => $userId
                ];
            }
        } else {
            MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)
                ->whereNotIn('component', $exceptComponents)
                ->delete();

            foreach ($request->components as $component) {
                if (!in_array($component['component'], $exceptComponents)) {
                    $components[] = [
                        'component'         => $component['component'],
                        'component3d_id'    => $component['component3d_id'],
                        'created_by'        => $userId
                    ];
                } else {
                    MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)
                        ->where('component', $component['component'])
                        ->update([
                            'component3d_id' => $component['component3d_id']
                        ]);
                }
            }
        }

        $group->mst_ercm_pelabuhan_components()->createMany($components);
    }

    private function datatable(MstErcmPelabuhanGroup $group)
    {
        $data = MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)->whereNull('mst_ercm_pelabuhan_subgroup_id')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('select', function ($row) {
                return '
                    <div class="form-check">
                        <input class="form-check-input form-checkbox-button selected-component" type="checkbox" name="selected[]" value="' . $row->id . '"  />
                    </div>
                ';
            })
            ->rawColumns(['select'])
            ->make(true);
    }

    private function datatableGrouped(Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        $data = MstErcmPelabuhanSubgroup::with('mst_ercm_pelabuhan_components')
            ->where('mst_ercm_pelabuhan_group_id', $group->id)
            ->whereHas('mst_ercm_pelabuhan_components')
            ->orderBy('updated_at')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('qty', function ($row) {
                return @count($row->mst_ercm_pelabuhan_components);
            })
            ->addColumn('standard', function ($row) use ($port, $form, $group) {
                return '
                    <a href="' . route('port.form.group.item.sub-param', [$port->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-outline btn-outline-primary fw-bold">
                    ' . __('Choose Std') . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) {
                return '
                    <a type="button" class="btn btn-sm btn-primary btn-icon btn-edit-sub" data-id="' . $row->id . '" data-type="detail">
                        ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                    </a>

                    <a type class="btn btn-sm btn-success btn-icon btn-edit-sub" data-id="' . $row->id . '" data-type="edit">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>

                    <a class="btn btn-sm btn-danger btn-icon btn-delete-sub" data-id="' . $row['id'] . '" data-type="SubGroup">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
            })
            ->rawColumns(['action', 'standard'])
            ->make(true);
    }

    private function datatableSubGroup(Request $request, Port $port, MstErcmPelabuhanSystem $form, MstErcmPelabuhanGroup $group)
    {
        $data = MstErcmPelabuhanComponent::where('mst_ercm_pelabuhan_group_id', $group->id)
            ->where('mst_ercm_pelabuhan_subgroup_id', $request->mst_ercm_pelabuhan_subgroup_id)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($request, $port, $form, $group) {
                $button = '';
                if ($request->type == 'edit') {
                    $button = '
                        <a href="' . route('port.form.group.item.component-param', [$port->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-success btn-icon">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                        </a>

                        <a class="btn btn-sm btn-danger btn-icon btn-delete-sub" data-id="' . $row->id . '" data-sub="' . $request->mst_ercm_pelabuhan_subgroup_id . '" data-type="Component">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                        </a>
                    ';
                }

                if ($request->type == 'detail') {
                    $button = '
                        <a href="' . route('port.form.group.item.preview-component-param', [$port->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                            ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                        </a>
                    ';
                }

                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function parameterComponentFromParent($request)
    {
        $userId = auth()->user()->id;

        $subGroup = MstErcmPelabuhanSubGroup::find($request->mst_ercm_pelabuhan_subgroup_id);
        if (!$subGroup) {
            throw new \Exception('Data sub group tidak ditemukan!');
        }

        foreach (explode(',', $request->mst_ercm_pelabuhan_components) as $id) {
            $parameters = MstErcmPelabuhanSubgroupStandardParameter::where('mst_ercm_pelabuhan_subgroup_id', $request->mst_ercm_pelabuhan_subgroup_id)
                ->with([
                    'subgroup_standard_parameter_number',
                    'subgroup_standard_parameter_areas',
                    'subgroup_standard_parameter_ranges',
                    'subgroup_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();

            $assestmentTypes = $parameters->groupBy('assestment_type')->all();
            if (@count($assestmentTypes) < 3) {
                foreach (MstErcmPelabuhanSubgroupStandardParameter::$assestmentTypes as $key => $value) {
                    if (!isset($assestmentTypes[$value])) {
                        throw new \Exception('Parameter ' . ucfirst($key) . ' stage ' . $subGroup->name . ' tidak ditemukan. Mohon atur parameter dengan benar');
                    }
                }
            }

            foreach ($parameters as $param) {
                $stdParam = MstErcmPelabuhanComponentStandardParameter::create($param->toArray() + [
                    'mst_ercm_pelabuhan_component_id' => $id,
                    'created_by' => $userId
                ]);

                if ($param->subgroup_standard_parameter_number ?? false) {
                    MstErcmPelabuhanComponentStandardParameterNumberParam::create($param->subgroup_standard_parameter_number->toArray() + [
                        'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_areas ?? false as $key => $value) {
                    MstErcmPelabuhanComponentStandardParameterAreaParam::create($value->toArray() + [
                        'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_ranges ?? false as $key => $value) {
                    MstErcmPelabuhanComponentStandardParameterRangeParam::create($value->toArray() + [
                        'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_approvals ?? false as $key => $value) {
                    MstErcmPelabuhanComponentStandardParameterApprovalParam::create($value->toArray() + [
                        'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id
                    ]);
                }
            }

            $condition = MstErcmPelabuhanSubgroupConditionCriteria::where('mst_ercm_pelabuhan_subgroup_id', $request->mst_ercm_pelabuhan_subgroup_id)->first();
            if (!$condition) {
                throw new \Exception('Mohon isi condition criteria dari ' . $subGroup->name);
            }

            MstErcmPelabuhanComponentConditionCriteria::create($condition->toArray() + [
                'mst_ercm_pelabuhan_component_id' => $id,
                'created_by' => $userId
            ]);
        }
    }

    private function parameterComponent(Request $request, MstErcmPelabuhanComponent $component, $isUpdate = false)
    {
        $types = MstErcmPelabuhanComponentStandardParameter::$assestmentTypes;
        $stdParamModel = new MstErcmPelabuhanComponentStandardParameter;
        $userId = auth()->user()->id;
        foreach ($types as $type => $value) {
            if (!isset($request->$type['parameters'])) {
                throw new \Exception('[' . strtoupper($type) . '] Minimal terdapat satu parameter.');
            }

            if ($value == $types['initial']) {
                if (empty($request->$type['type_stage'])) {
                    throw new \Exception('Pilih stage from parameter!');
                }
            }

            foreach ($request->$type['parameters'] as $key => $parameter) {
                if ($parameter['order_type'] ?? false) {
                    if (!($parameter['name_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nama Parameter Ke-' . $key + 1);
                    }

                    if (!($parameter['value_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nilai Parameter Ke-' . $key + 1);
                    } else {
                        if ($parameter['value_' . $parameter['order_type']] == 0) {
                            throw new \Exception('[' . strtoupper($type) . '] Nilai Parameter Ke-' . $key + 1 . ' harus lebih dari 0');
                        }
                    }

                    $stdParam = $stdParamModel::create([
                        'mst_ercm_pelabuhan_component_id'   => $component->id,
                        'name'                          => $parameter['name_' . $parameter['order_type']],
                        'value'                         => $parameter['value_' . $parameter['order_type']],
                        'assestment_type'               => $request->$type['assestment_type'],
                        'order_type'                    => $parameter['order_type'],
                        'type_stage'                    => $request->$type['type_stage'] ?? null,
                        'created_by'                    => $userId,
                    ]);

                    switch ($parameter['order_type']) {
                        case $stdParamModel::$orderTypes['area']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Area Parameter');
                            }

                            foreach ($parameter['order_parameters'] as $param) {
                                MstErcmPelabuhanComponentStandardParameterAreaParam::create([
                                    'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id,
                                    'std'               => $param['std'] ?? null,
                                    'up1'               => $param['up1'] ?? null,
                                    'down1'             => $param['down1'] ?? null,
                                    'color1'            => $param['color1'] ?? null,
                                    'up2'               => $param['up2'] ?? null,
                                    'down2'             => $param['down2'] ?? null,
                                    'color2'            => $param['color2'] ?? null,
                                    'up3'               => $param['up3'] ?? null,
                                    'down3'             => $param['down3'] ?? null,
                                    'color3'            => $param['color3'] ?? null,
                                    'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                                ]);
                            }
                            break;
                        case $stdParamModel::$orderTypes['number']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Number Parameter');
                            }

                            MstErcmPelabuhanComponentStandardParameterNumberParam::create([
                                'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id,
                                'std'               => $parameter['std'] ?? null,
                                'min'               => $parameter['min'] ?? null,
                                'max'               => $parameter['max'] ?? null,
                                'mid1'              => $parameter['mid1'] ?? null,
                                'mid2'              => $parameter['mid2'] ?? null,
                                'less'              => $parameter['less'] ?? null,
                                'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                            ]);
                            break;
                        case $stdParamModel::$orderTypes['ranges']:
                            foreach ($parameter['range_parameters'] as $param) {
                                foreach ($param['criterias'] as $criteria) {
                                    if (!empty($criteria['criteria'])) {
                                        MstErcmPelabuhanComponentStandardParameterRangeParam::create([
                                            'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id,
                                            'range'     => $param['range'],
                                            'color'     => $param['color'],
                                            'criteria'  => $criteria['criteria'],
                                        ]);
                                    }
                                }
                            }
                            break;
                        case $stdParamModel::$orderTypes['approval']:
                            foreach ($parameter['order_parameters'] as $param) {
                                if (!empty($param['param'])) {
                                    MstErcmPelabuhanComponentStandardParameterApprovalParam::create([
                                        'mst_ercm_pelabuhan_component_standard_parameter_id' => $stdParam->id,
                                        'param' => $param['param']
                                    ]);
                                }
                            }
                            break;
                        default:
                            throw new \Exception('Undefined Order Type', 500);
                            break;
                    }
                }
            }
        }

        MstErcmPelabuhanComponentConditionCriteria::create([
            'mst_ercm_pelabuhan_component_id' => $component->id,
            'up1'           => $request->up1,
            'down1'         => $request->down1,
            'status1'       => MstErcmPelabuhanComponentConditionCriteria::$status[strtolower($request->status1)],
            'up2'           => $request->up2,
            'down2'         => $request->down2,
            'status2'       => MstErcmPelabuhanComponentConditionCriteria::$status[strtolower($request->status2)],
            'up3'           => $request->up3,
            'down3'         => $request->down3,
            'status3'       => MstErcmPelabuhanComponentConditionCriteria::$status[strtolower($request->status3)],
            'created_by'    => $userId
        ]);
    }
}
