<?php

namespace App\Http\Controllers\Port;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Port\StorePortRequest;
use App\Http\Requests\MasterData\Port\UpdatePortRequest;
use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\Region;
use DB;
use Illuminate\Http\Request;
use Session;

use App\Models\Port\Port;
use App\Models\User;

class PortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regional   = Region::select('id', 'nama')->orderBy('nama', 'ASC')->get();
        $pic        = User::select('id', 'name')->orderBy('name', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.port.port.index', compact('pic', 'regional'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StorePortRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['cabang_id'] = $request->cabang_id;
            $data['nama'] = trim($request->nama);
            $data['pic'] = $request->pic;
            // dd($data);
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate)) {
                return redirect()->route('port.port.index')->with(['failed' => 'Port Sudah Ada!']);
            }
            Port::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('port.port.index')->with(['failed' => 'Gagal Tambah Port!']);
        }

        return redirect()->route('port.port.index')->with(['success' => 'Berhasil Tambah Port!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Port $port
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Port $port)
    {
        $data = Port::with('cabang', 'pic_user')
            ->where('mst_pelabuhan.id', $port->id)->first();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Port $port
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePortRequest $request, Port $port)
    {
        DB::beginTransaction();
        try {
            $data['cabang_id'] = $request->cabang_id;
            $data['nama'] = trim($request->nama);
            $data['pic'] = $request->pic;
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate) && $duplicate->id != $port->id) {
                return redirect()->route('port.port.index')->with(['failed' => 'Port Sudah Ada!']);
            }
            $port->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('port.port.index')->with(['failed' => 'Gagal Ubah Port!']);
        }

        return redirect()->route('port.port.index')->with(['success' => 'Berhasil Ubah Port!']);
    }

    protected function check_duplicate($data)
    {
        unset($data['pic']);
        $duplicate = Port::where($data)->first();
        return $duplicate;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Port $port
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Port $port)
    {
        DB::beginTransaction();

        try {
            $port->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Port!');
        }

        Session::flash('success', 'Berhasil Hapus Port!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Port::with('cabang', 'pic_user')
            ->orderBy('mst_pelabuhan.id', 'DESC')->get();
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('regional', function($data) {
                return $data->cabang->regional->nama;
            })
            ->addColumn('cabang_kelas', function($data) {
                return $data->cabang->cabang_kelas;
            })
            ->editColumn('cabang_id', function ($data) {
                return $data->cabang->nama;
            })
            ->editColumn('pic', function ($data) {
                return $data->pic_user->name;
            })
            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-sm btn-warning"
                    href="' . route('port.form.index', $data->id) . '">
                    ' . __('Detail') . '
                    </a>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-success btn-edit-port ms-2"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_port_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-port ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function ajax_get_branch(Request $request) {
        $branch = Branch::where('regional_id', $request->id)->orderBy('nama', 'ASC')->get();
        return response()->json($branch);
    }
}
