<?php

namespace App\Http\Controllers\Port;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Port\StoreDockRequest;
use App\Http\Requests\MasterData\Port\UpdateDockRequest;
use DB;
use Illuminate\Http\Request;
use Session;

use App\Models\Port\Port;
use App\Models\Port\Dock;

class DockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $port   = Port::select('id', 'nama')->orderBy('nama', 'ASC')->get();
        $dock_type   = $this->get_dock_type();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.port.dock.index', compact('port', 'dock_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDockRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['pelabuhan_id'] = $request->pelabuhan_id;
            $data['nama'] = trim($request->nama);
            $data['tipe'] = $request->tipe;
            $data['kapasitas'] = $request->kapasitas;
            // dd($data);
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate)) {
                return redirect()->route('port.dock.index')->with(['failed' => 'Dock Sudah Ada!']);
            }
            Dock::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('port.dock.index')->with(['failed' => 'Gagal Tambah Dock!']);
        }

        return redirect()->route('port.dock.index')->with(['success' => 'Berhasil Tambah Dock!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Dock $dock
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Dock $dock)
    {
        return $dock;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Dock $dock
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDockRequest $request, Dock $dock)
    {
        DB::beginTransaction();
        try {
            $data['pelabuhan_id'] = $request->pelabuhan_id;
            $data['nama'] = trim($request->nama);
            $data['tipe'] = $request->tipe;
            $data['kapasitas'] = $request->kapasitas;
            $duplicate = $this->check_duplicate($data);
            if (!empty($duplicate) && $duplicate->id != $dock->id) {
                return redirect()->route('port.dock.index')->with(['failed' => 'Dock Sudah Ada!']);
            }
            $dock->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('port.dock.index')->with(['failed' => 'Gagal Ubah Dock!']);
        }

        return redirect()->route('port.dock.index')->with(['success' => 'Berhasil Ubah Dock!']);
    }

    protected function check_duplicate($data)
    {
        unset($data['kapasitas']);
        $duplicate = Dock::where($data)->first();
        return $duplicate;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Dock $dock
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dock $dock)
    {
        DB::beginTransaction();

        try {
            $dock->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Dock!');
        }

        Session::flash('success', 'Berhasil Hapus Dock!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Dock::with('pelabuhan')->orderBy('id', 'DESC')->get();
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('pelabuhan_id', function ($data) {
                return $data->pelabuhan->nama;
            })->editColumn('kapasitas', function ($data) {
                return number_format($data->kapasitas);
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-dock"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_dock_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-dock ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action', 'kapasitas'])
            ->make(true);
    }

    private function get_dock_type() {
        $dock_type = ['Dermaga kapal ikan', 'Dermaga barang umum', 'Dermaga peti kemas', 'Dermaga marine', 'Dermaga curah', 'Dermaga khusus'];
        return $dock_type;
    }
}
