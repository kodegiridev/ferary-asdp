<?php

namespace App\Http\Controllers\Approval;

use App\Http\Controllers\Controller;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Fleet\EngineeringDocument;
use App\Models\EngineeringDatabase\Fleet\EquipmentBoard;
use App\Models\EngineeringDatabase\Fleet\EquipmentBoardFile;
use App\Models\EngineeringDatabase\Fleet\FleetFacilityStandard;
use App\Models\EngineeringDatabase\Fleet\FleetTechnicalStandard;
use App\Models\EngineeringDatabase\Fleet\ManualBook;
use App\Models\EngineeringDatabase\Fleet\ShipDocument;
use App\Models\EngineeringDatabase\Port\DocumentOfQuality;
use App\Models\EngineeringDatabase\Port\FacilityStandard;
use App\Models\EngineeringDatabase\Port\PortDrawing;
use App\Models\EngineeringDatabase\Port\PortSpecification;
use App\Models\EngineeringDatabase\Port\TechnicalQualityStandard;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\ERCM\ErcmApproval;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringFleet;
use App\Models\ProjectMonitoring\ApprovalProjectMonitoringPort;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables($request);
        }
        return view('pages.approval.approval.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function keterangan(Approval $approval)
    {
        $approval->verifikators;
        return $approval;
    }

    /**
     * Display a listing of the resource.
     * 
     * @param string $approval_id
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $approval_id)
    {
        if ($request->ajax()) {
            return $this->datatablesDetail($approval_id);
        }

        $approval = Approval::find(decryptor($approval_id));
        if (!$approval) {
            return redirect()->back()->with(['failed' => 'Data tidak ditemukan']);
        }

        $user = Auth::user();
        $verifikator = Verifikator::where('approval_id', $approval->id);
        $hasRejected = (clone $verifikator)->isRejected()->exists();
        $hasReviewed = (clone $verifikator)->isReviewed()->count();

        $data = [
            'content_at'    => $this->onModul($approval->sub_modul)['view'],
            'approval'      => $approval,
            'user'          => $user,
            'is_rejected'   => $hasRejected || $approval->status === Approval::REJECTED,
            'is_reviewed'   => $hasReviewed == $verifikator->count(),
            'is_approved'   => $approval->status === Approval::APPROVED
        ];

        return view('pages.approval.approval.detail', compact('data'));
    }

    /**
     * Display data in datatables
     *
     * @param \Illuminate\Http\Request
     * @param boolean $isApprove true for Approval|false for Status
     * @return \Yajra\DataTables\DataTables
     */
    protected function datatables(Request $request, $isApprove = true)
    {
        $user = Auth::user();

        // create query every module
        $equipmentBoard     = EquipmentBoard::selectApproval();
        $engineeringDoc     = EngineeringDocument::selectApproval();
        $manualBook         = ManualBook::selectApproval();
        $shipDoc            = ShipDocument::selectApproval();
        $fleetTechnicalStd  = FleetTechnicalStandard::selectApproval();
        $fleetFacilityStd   = FleetFacilityStandard::selectApproval();
        $portDrawing        = PortDrawing::selectApproval();
        $techQualityStd     = TechnicalQualityStandard::selectApproval();
        $techFacilityPort   = FacilityStandard::selectApproval();
        $portSpesification  = PortSpecification::selectApproval();
        $documentOfQuality  = DocumentOfQuality::selectApproval();
        $projectPort        = ApprovalProjectMonitoringPort::selectApproval();
        $projectFleet       = ApprovalProjectMonitoringFleet::selectApproval();
        $ercm               = ErcmApproval::selectApproval();

        // union every module
        $modulQuery = $equipmentBoard
            ->union($engineeringDoc)
            ->union($manualBook)
            ->union($shipDoc)
            ->union($fleetTechnicalStd)
            ->union($fleetFacilityStd)
            ->union($portDrawing)
            ->union($techQualityStd)
            ->union($techFacilityPort)
            ->union($portSpesification)
            ->union($documentOfQuality)
            ->union($projectPort)
            ->union($projectFleet)
            ->union($ercm);

        // complete the query
        $data = Approval::with('penyetuju', 'pengaju', 'verifikators', 'project_monitoring_port', 'project_monitoring_fleet')
            ->selectRaw("
                approval.id
                ,jenis_modul
                ,sub_modul
                ,penyetuju_id
                ,pengaju_id
                ,tgl_penyetujuan
                ,tgl_pengajuan
                ,tgl_tolak_penyetuju
                ,status
                ,CASE 
                    WHEN sub_modul = 'Fleet Technical Standard' 
                        OR sub_modul = 'Fleet Facility Standard' 
                        OR sub_modul = 'Technical Quality Standard'
                        OR sub_modul = 'Facility Standard'
                        OR sub_modul = 'Period'
                        OR jenis_modul = 'IPM Port'
                        OR jenis_modul = 'IPM Fleet'
                    THEN modul.item
                    WHEN sub_modul = 'Port Drawing' 
                        OR sub_modul = 'Port Spesification'
                        OR sub_modul = 'Document Of Quality'
                    THEN mst_dermaga.nama 
                    ELSE mst_vessel.nama
                END AS item
            ")
            ->leftJoinSub($modulQuery, 'modul', function ($join) {
                $join->on('modul.approval_id', '=', 'approval.id');
            })
            ->leftJoin('mst_vessel', 'mst_vessel.id', '=', 'modul.item')
            ->leftJoin('mst_dermaga', 'mst_dermaga.id', '=', 'modul.item')
            ->when(!isAdmin(), function ($query) use ($user) {
                return $query->where(function ($query) use ($user) {
                    $query->where('pengaju_id', $user->id)
                        ->orWhere('penyetuju_id', $user->id)
                        ->orWhereHas('verifikators', function ($query) use ($user) {
                            $query->where('user_verifikator_id', $user->id);
                        });
                });
            })
            ->when($isApprove, function ($query) {
                return $query->where('approval.status', Approval::APPROVED);
            })
            ->when(!$isApprove, function ($query) {
                return $query->where('approval.status', '!=', Approval::APPROVED);
            })
            ->orderBy('approval.updated_at', 'desc')
            ->groupBy('approval.id')
            ->get();

        $detailRoute = $isApprove ? 'approval.persetujuan.detail' : 'approval.status.detail';
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('pembuat', function ($row) {
                return $row->pengaju->name . ' (' . $row->pengaju->role->name . ')';
            })
            ->addColumn('pemeriksa', function ($row) {
                $verifikators = '';
                foreach ($row->verifikators as $verifikator) {
                    $verifikators .= '<li> Verikator ' . $verifikator->user->role->name . '</li>';
                }

                return '
                    <ul>
                        ' . $verifikators . '
                        <li> Approver ' . $row->penyetuju->role->name . '</li>
                    </ul>
                ';
            })
            ->addColumn('tanggal', function ($row) {
                $dates = '<li>Submit ' . $row->tgl_pengajuan . '</li>';
                if ($row->status != Approval::REJECTED) {
                    foreach ($row->verifikators as $verifikator) {
                        if ($verifikator->tgl_disetujui)
                            $dates .= '<li>Verified ' . $verifikator->tgl_disetujui . '</li>';
                    }
                    if ($row->tgl_penyetujuan)
                        $dates .= '<li>Approved ' . $row->tgl_penyetujuan . '</li>';
                } else {
                    $dates .= '<li>Rejected ' . $row->tgl_tolak_penyetuju . '</li>';
                }

                return '<ul>' . $dates . '</ul>';
            })
            ->addColumn('status', function ($row) {
                $labels = [
                    Approval::NEW => 'Reviewed By Manager',
                    Approval::REVIEW_BY_MGR => 'Reviewed By Manager',
                    Approval::REVIEW_BY_VP => 'Reviewed By VP',
                    Approval::REJECTED => 'Rejected',
                    Approval::APPROVED => 'Accepted'
                ];

                return $labels[$row->status] ?? 'Baru';
            })
            ->addColumn('proses', function ($row) {
                $proses = 0;
                $total = 0;
                if ($row->status != Approval::REJECTED) {
                    $total = count($row->verifikators) + 1;
                    foreach ($row->verifikators as $verifikator) {
                        if ($verifikator->status == Verifikator::APPROVED) $proses++;
                    }

                    if ($row->status == Approval::APPROVED) $proses++;
                }

                return $proses . '/' . $total;
            })
            ->addColumn('action', function ($row) use ($detailRoute, $isApprove, $user) {
                switch ($row->jenis_modul) {
                    case 'IPM Port':
                        $route = route('ipm.port.summary.index', $row->project_monitoring_port->project_monitoring_port_id ?? '');
                        break;
                    case 'IPM Fleet':
                        $route = route('ipm.fleet.summary.index', $row->project_monitoring_fleet->project_monitoring_fleet_id ?? '');
                        break;
                    default:
                        $route = route($detailRoute, encryptor($row->id));
                        break;
                }

                $button = '<a class="btn btn-sm btn-success"
                    href=" ' . $route . '">
                        ' . __('Detail') . '
                    </a> &nbsp;';
                if (!$isApprove && $user->id == $row->pengaju_id) {
                    $button .= '<button type="button"
                            class="btn btn-sm btn-danger btn-delete-approval"
                            data-id=' . $row->id . '>' .
                        theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'fs-1') .
                        __('Delete')
                        . '</button> &nbsp;';
                }
                $button .= '<button type="button"
                    class="btn btn-sm btn-primary btn-keterangan-approval"
                    data-bs-toggle="modal"
                    data-bs-target="#keterangan_approval_modal"
                    data-id=' . $row->id . '>' .
                    theme()->getSvgIcon('icons/duotune/general/gen045.svg', 'fs-1') .
                    __('Keterangan')
                    . '</button>';
                return $button;
            })
            ->rawColumns(['pembuat', 'pemeriksa', 'tanggal', 'action', 'status', 'proses'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @param string of approval id $id
     * @return \Yajra\DataTables\DataTables
     */
    protected function datatablesDetail($id)
    {
        $approval = Approval::find($id);

        if ($approval->sub_modul == 'Period') {
            $data = \App\Models\ERCM\ErcmPeriod::where('id', $approval->ercm_approval->ercm_period_id ?? '')->get();
            $path = $approval->jenis_modul == 'ERCM Fleet'
                ? 'ercm/fleet'
                : 'ercm/port';

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->editColumn('effectiveness', function ($row)  use ($path) {
                    $row->file_url = $path;
                    return '
                        <div class="d-flex flex-column">
                            <h1 style="font-size:15px;">' . $row->effectiveness_percent . '</h1>
                            ' . ($row->file_url['effectiveness'] ? '<a href="' . $row->file_url['effectiveness'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                        </div>';
                })
                ->editColumn('cost', function ($row)  use ($path) {
                    $row->file_url = $path;
                    return '
                        <div class="d-flex flex-column">
                            <h1 style="font-size:15px;">' . $row->cost_format . '</h1>
                            ' . ($row->file_url['cost'] ? '<a href="' . $row->file_url['cost'] . '" class="btn bg-body p-0">Lihat</a>' : '') . '
                        </div>';
                })
                ->rawColumns(['effectiveness', 'cost'])
                ->make(true);
        } else {
            $data = $this->onModul($approval->sub_modul)['modal']
                ->where('approval_id', $approval->id)
                ->get();

            return datatables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('document', function ($row) {
                    return '
                    <a type="button" class="text-hover-primary btn-document"
                        data-bs-toggle="modal"
                        data-bs-target="#document_modal"
                        data-id=' . $row->id . '>
                            ' . theme()->getSvgIcon('icons/duotune/files/fil024.svg', 'svg-icon-1') . '
                    </a>
                ';
                })
                ->rawColumns(['document'])
                ->make(true);
        }
    }

    /**
     * Get the specified resource.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function datatablesDocument(Request $request)
    {
        $data = $this->onModul($request->modul)['modal']->find($request->modul_id);

        return datatables()
            ->of($data->dokumen ?? $data->documents ?? [])
            ->addIndexColumn()
            ->addColumn('file', function ($row) {
                return $this->getFileName($row->file);
            })
            ->addColumn('action', function ($row) {
                return '
                    <a class="text-hover-primary" href="' . route('approval.persetujuan.document-download', ['file' => encryptor($row->file)]) . '">
                        ' . theme()->getSvgIcon('icons/duotune/files/fil021.svg', 'svg-icon-1') . '
                    </a
                ';
            })
            ->rawColumns(['file', 'action'])
            ->make(true);
    }

    /**
     * Get selected document.
     * 
     * @param string
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadDocument(Request $request)
    {
        try {
            $file = decryptor($request->file);
            if (Storage::disk('public')->exists($file)) {
                return response()->file(Storage::disk('public')->path($file));
            }

            return abort(404);
        } catch (\Exception $e) {
            return abort(500);
        }
    }

    /**
     * Define component every modul.
     * 
     * @param string
     * @return array
     */
    protected function onModul($name)
    {
        $data = [
            'Equipment On Board' => [
                'view' => 'pages.approval.approval.details.equipment-board',
                'modal' => new EquipmentBoard,
                'document_name_column' => 'nama_peralatan'
            ],
            'Engineering Document' => [
                'view' => 'pages.approval.approval.details.engineering-document',
                'modal' => new EngineeringDocument,
                'document_name_column' => 'nama_gambar'
            ],
            'Manual Book' => [
                'view' => 'pages.approval.approval.details.manual-book',
                'modal' => new ManualBook,
                'document_name_column' => 'nama_dokumen'
            ],
            'Ship Document' => [
                'view' => 'pages.approval.approval.details.ship-document',
                'modal' => new ShipDocument,
                'document_name_column' => 'nama_dokumen'
            ],
            'Fleet Technical Standard' => [
                'view' => 'pages.approval.approval.details.fleet-tech-std',
                'modal' => new FleetTechnicalStandard,
                'document_name_column' => 'nama_dokumen'
            ],
            'Fleet Facility Standard' => [
                'view' => 'pages.approval.approval.details.fleet-facility-std',
                'modal' => new FleetFacilityStandard,
                'document_name_column' => 'nama_dokumen'
            ],
            'Technical Quality Standard' => [
                'view' => 'pages.approval.approval.details.tech-quality-std',
                'modal' => new TechnicalQualityStandard,
                'document_name_column' => 'nama_dokumen'
            ],
            'Facility Standard' => [
                'view' => 'pages.approval.approval.details.tech-facility-port',
                'modal' => new FacilityStandard,
                'document_name_column' => 'nama_dokumen'
            ],
            'Port Drawing' => [
                'view' => 'pages.approval.approval.details.port-drawing',
                'modal' => new PortDrawing,
                'document_name_column' => 'nama_komponen'
            ],
            'Port Specification' => [
                'view' => 'pages.approval.approval.details.port-spesification',
                'modal' => new PortSpecification,
                'document_name_column' => 'nama_komponen'
            ],
            'Document Of Quality' => [
                'view' => 'pages.approval.approval.details.document-of-quality',
                'modal' => new DocumentOfQuality,
                'document_name_column' => 'nama_komponen'
            ],
            'Project Monitoring Fleet' => [
                'modal' => new ApprovalProjectMonitoringFleet,
                'document_name_column' => 'judul_dokumen'
            ],
            'Project Monitoring Fleet' => [
                'modal' => new ApprovalProjectMonitoringPort,
                'document_name_column' => 'judul_dokumen'
            ],
            'Period' => [
                'view' => 'pages.approval.approval.details.period',
            ]
        ];

        return $data[$name];
    }
}
