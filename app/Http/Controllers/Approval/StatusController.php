<?php

namespace App\Http\Controllers\Approval;

use App\Http\Controllers\NotificationController;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Verifikator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StatusController extends ApprovalController
{
    private $moduleName;
    private $notification;
    private $viewMail;

    public function __construct()
    {
        $this->moduleName = 'Approval';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.approval.mail';
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables($request, false);
        }
        return view('pages.approval.status.index');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param string $approval_id
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $approval_id)
    {
        $approval = Approval::find(decryptor($approval_id));
        if (!$approval) {
            return redirect()->back()->with(['failed' => 'Data tidak ditemukan']);
        }

        $user = Auth::user();
        $verifikator = Verifikator::where('approval_id', $approval->id);
        $hasRejected = (clone $verifikator)->isRejected()->exists();
        $hasReviewed = (clone $verifikator)->isReviewed();

        $canApproveOrReject = false;
        if ($approval->pengaju_id != $user->id && $approval->status != Approval::REJECTED) {
            if ($approval->penyetuju_id == $user->id) {
                $canApproveOrReject = $hasReviewed->count() == $verifikator->count();
            } else {
                $canApproveOrReject = !(clone $hasReviewed)->whereUser($user->id)->exists();
            }
        }

        $data = [
            'content_at'            => $this->onModul($approval->sub_modul)['view'],
            'approval'              => $approval,
            'can_approve_reject'    => $canApproveOrReject,
        ];

        return view('pages.approval.status.detail', compact('data'));
    }

    /**
     * To approve
     * 
     * @return void
     */
    public function approve(Request $request, Approval $approval)
    {
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $documents = [];
            if (!in_array($approval->jenis_modul, ['IPM Fleet', 'IPM Port', 'ERCM Fleet', 'ERCM Port'])) {
                $documents = $this->onModul($approval->sub_modul)['modal']
                    ->where('approval_id', $approval->id)
                    ->pluck($this->onModul($approval->sub_modul)['document_name_column']);
            }

            if ($this->isVerifikator($approval->id)) {
                $verifikator = Verifikator::where('approval_id', $approval->id);
                $otherReviewed = (clone $verifikator)->whereUser($user->id, '!=')->isReviewed()->count();

                $approval->update([
                    'status' => $otherReviewed + 1 == $verifikator->count() ? Approval::REVIEW_BY_VP : Approval::REVIEW_BY_MGR
                ]);

                $unreviewed = (clone $verifikator)->whereUser($user->id)->isNew();
                if ($unreviewed->exists()) {
                    $unreviewed->update([
                        'tgl_disetujui' => now()->format('Y-m-d'),
                        'keterangan_verifikator' => $request->keterangan_verifikator,
                        'status' => Verifikator::APPROVED
                    ]);
                }

                if ($approval->status == Approval::REVIEW_BY_MGR) {
                    $receiver_users = array_merge(
                        [$approval->pengaju_id],
                        $verifikator->pluck('user_verifikator_id')->toArray()
                    );

                    $param = [];
                    $param['users'] = $receiver_users;
                    $param['title'] = $this->moduleName;
                    $param['data'] = __('app.notification.approval.approve');
                    $param['action'] = route('approval.status.detail', encryptor($approval->id));
                    $param['view'] = $this->viewMail;
                    $param['subject'] = __('app.mail.approval.subject');
                    $param['documents'] = $documents;
                    $param['approver'] = $user;
                    $this->notification->send($param);
                }

                if ($approval->status == Approval::REVIEW_BY_VP) {
                    $receiver_users = array_merge(
                        [$approval->pengaju_id],
                        [$approval->penyetuju_id]
                    );

                    $param = [];
                    $param['users'] = $receiver_users;
                    $param['title'] = $this->moduleName;
                    $param['data'] = __('app.notification.approval.approve');
                    $param['action'] = route('approval.status.detail', encryptor($approval->id));
                    $param['view'] = $this->viewMail;
                    $param['subject'] = __('app.mail.approval.subject');
                    $param['documents'] = $documents;
                    $param['approver'] = $user;
                    $this->notification->send($param);
                }
            } elseif ($this->isApprover($approval->id)) {
                $approval->update([
                    'status' => Approval::APPROVED,
                    'keterangan_penyetuju' => $request->keterangan_verifikator,
                    'tgl_penyetujuan' => now()->format('Y-m-d')
                ]);

                if ($approval->jenis_modul == 'IPM Port') {
                    $approval->project_monitoring_port()->update(['status' => 2]);
                    $this->exceSummary($approval->project_monitoring_port, 2);
                }

                if ($approval->jenis_modul == 'IPM Fleet') {
                    $approval->project_monitoring_fleet()->update(['status' => 2]);
                    $this->exceSummary($approval->project_monitoring_fleet, 1);
                }

                if (in_array($approval->jenis_modul, ['ERCM Fleet', 'ERCM Port'])) {
                    \App\Models\ERCM\ErcmPeriod::where('id', $approval->ercm_approval->ercm_period_id ?? null)
                        ->update([
                            'period_status' => \App\Enums\ERCMPeriodStatusEnum::COMPLETION
                        ]);
                }

                $param = [];
                $param['users'] = [$approval->pengaju_id];
                $param['title'] = $this->moduleName;
                $param['data'] = __('app.notification.approval.approve');
                $param['action'] = route('approval.status.detail', encryptor($approval->id));
                $param['view'] = $this->viewMail;
                $param['subject'] = __('app.mail.approval.subject');
                $param['documents'] = $documents;
                $param['approver'] = $user;
                $this->notification->send($param);
            }

            DB::commit();
            return !in_array($approval->jenis_modul, ['IPM Port', 'IPM Fleet'])
                ? redirect()->route('approval.status.index')->with(['success' => 'Berhasil menyetujui!'])
                : redirect()->back()->with(['success' => 'Berhasil menyetujui!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with(['failed' => 'Gagal menyetujui!']);
        }
    }

    /**
     * To reject
     * 
     * @return void
     */
    public function reject(Request $request, Approval $approval)
    {
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $verifikator = Verifikator::where('approval_id', $approval->id);
            $otherReviewed = (clone $verifikator)->whereUser($user->id, '!=')->isReviewed()->count();
            $documents = [];
            if (!$approval->project_monitoring_port && !$approval->project_monitoring_fleet) {
                $documents = $this->onModul($approval->sub_modul)['modal']
                    ->where('approval_id', $approval->id)
                    ->pluck($this->onModul($approval->sub_modul)['document_name_column']);
            }

            $approval->update([
                'status' => Approval::REJECTED,
                'tgl_tolak_penyetuju' => now()->format('Y-m-d')
            ]);

            if ($approval->status != Approval::APPROVED) {
                $unreviewed = (clone $verifikator)->whereUser($user->id)->isNew();
                if ($unreviewed->exists()) {
                    $unreviewed->update([
                        'tgl_tolak_verifikator' => now()->format('Y-m-d'),
                        'keterangan_verifikator' => $request->keterangan_verifikator,
                        'status' => Verifikator::REJECTED
                    ]);

                    $param = [];
                    $param['users'] = $verifikator->pluck('user_verifikator_id')->toArray();
                    $param['title'] = $this->moduleName;
                    $param['data'] = __('app.notification.approval.reject');
                    $param['action'] = route('approval.status.detail', encryptor($approval->id));
                    $param['view'] = $this->viewMail;
                    $param['subject'] = __('app.mail.approval.subject');
                    $param['documents'] = $documents;
                    $param['rejecter'] = $user;
                    $this->notification->send($param);
                }
            }

            if ($approval->jenis_modul == 'IPM Port') {
                $approval->project_monitoring_port()->update(['status' => 0]);
            }

            if ($approval->jenis_modul == 'IPM Fleet') {
                $approval->project_monitoring_fleet()->update(['status' => 0]);
            }

            $param = [];
            $param['users'] = [$approval->pengaju_id];
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.approval.reject');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.approval.subject');
            $param['documents'] = $documents;
            $param['rejecter'] = $user;
            $this->notification->send($param);

            DB::commit();
            return !in_array($approval->jenis_modul, ['IPM Port', 'IPM Fleet'])
                ? redirect()->route('approval.status.index')->with(['success' => 'Berhasil menolak pengajuan!'])
                : redirect()->back()->with(['success' => 'Berhasil menolak pengajuan!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menolak pengajuan!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param string
     */
    public function delete(Approval $approval)
    {
        DB::beginTransaction();

        try {
            $approval->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus approval.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['failed' => 'Gagal menghapus approval.']);
        }
    }

    /**
     * Is auth user is Pengaju.
     * 
     * @param string
     * @return boolean
     */
    private function isVerifikator($approval_id)
    {
        return Verifikator::whereUser(auth()->user()->id)
            ->where('approval_id', $approval_id)
            ->exists();
    }

    /**
     * Is auth user is Penyetuju.
     * 
     * @param string
     * @return boolean
     */
    private function isApprover($approval_id)
    {
        return Approval::where('penyetuju_id', auth()->user()->id)
            ->where('id', $approval_id)
            ->exists();
    }

    /**
     * Generate PDF document approved executive summary.
     * 
     * @param array<object> $projectApproval
     * @param integer $type 1=Fleet 2=Port
     * @return boolean
     */
    private function exceSummary($projectApproval, $type)
    {
        $projectId = $projectApproval->project_monitoring_port_id ?? $projectApproval->project_monitoring_fleet_id;
        if ($projectApproval->type == 1) {
            $data = [
                1 => [
                    'sdok'  => '\App\Models\ProjectMonitoring\SubDokumenFleet',
                    'dok'   => '\App\Models\ProjectMonitoring\DokumenFleet',
                    'ctrl'  => '\App\Http\Controllers\ProjectMonitoring\Fleet\SummaryController',
                    'path'  => 'upload/ipm/fleet/document/'
                ],
                2 => [
                    'sdok'  => '\App\Models\ProjectMonitoring\SubDokumenPort',
                    'dok'   => '\App\Models\ProjectMonitoring\DokumenPort',
                    'ctrl'  => '\App\Http\Controllers\ProjectMonitoring\Port\SummaryController',
                    'path'  => 'upload/ipm/port/document/'
                ]
            ];


            $subDokumen = (new $data[$type]['sdok'])->where('sub_dokumen', 'Executive Summary')
                ->whereHas('jenis_dokumen', function ($query) use ($projectId) {
                    $query->where('project_id', $projectId);
                })
                ->first();

            if ($subDokumen) {
                if (!is_dir(public_path($data[$type]['path']))) {
                    \File::makeDirectory(public_path($data[$type]['path']), 0777, true);
                }

                $path = $data[$type]['path'] . 'EXECUTIVE_SUMMARY_' . time() . '.pdf';
                $pdf = (new $data[$type]['ctrl'])->print(new Request(), $projectId, true);
                $pdf->save(public_path($path));

                (new $data[$type]['dok'])->create([
                    'sub_dokumen_id'    => $subDokumen->id,
                    'judul_dokumen'     => $projectApproval->judul_dokumen,
                    'status_pemenuhan'  => 1,
                    'file'              => url($path),
                ]);
            }
        } else {
            switch ($type) {
                case 1:
                    $project = new \App\Models\ProjectMonitoring\ProjectMonitoringFleet;
                    break;
                case 2:
                    $project = new \App\Models\ProjectMonitoring\ProjectMonitoringPort;
                    break;
            }

            $project->where('id', $projectId)
                ->update([
                    'status' => 1
                ]);
        }
    }
}
