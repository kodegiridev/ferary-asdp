<?php

namespace App\Http\Controllers\Fleet\Form;

use App\Http\Controllers\Controller;
use App\Imports\MstErcmFleetImport;
use App\Models\Fleet\ERCM\MstErcmVesselGroup;
use App\Models\Fleet\ERCM\MstErcmVesselSystem;
use App\Models\Fleet\Fleet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FleetFormGroupController extends Controller
{
    public function index(Request $request, Fleet $fleet, MstErcmVesselSystem $form)
    {
        if ($request->ajax()) {
            return $this->datatable($fleet, $form);
        }

        $data = [
            'fleet'         => $fleet,
            'form'          => $form,
            'used_value'    => MstErcmVesselGroup::where('mst_ercm_vessel_system_id', $form->id)->sum('value')
        ];

        return view('pages.fleet.form.group.index', compact('data'));
    }

    public function store(Request $request, Fleet $fleet, MstErcmVesselSystem $form)
    {
        $request->validate([
            'name'  => 'required',
            'value' => 'required|gt:0'
        ]);

        DB::beginTransaction();

        try {
            $existName = MstErcmVesselGroup::isExistsName($request->name, $fleet->id);
            if ($existName) {
                DB::rollBack();
                return redirect()->back()->with('failed', $request->name . ' group pada armada ' . $fleet->nama . ' sudah ada!');
            }

            $otherValue = MstErcmVesselGroup::where('mst_ercm_vessel_system_id', $form->id)->sum('value');
            if (($otherValue + $request->value) > 100) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Proportional value melebihi nilai maksimum!');
            }

            $data = MstErcmVesselGroup::create([
                'mst_ercm_vessel_system_id' => $form->id,
                'name'                      => $request->name,
                'value'                     => $request->value,
                'created_by'                => auth()->user()->id
            ]);

            if ($request->excel_file ?? false) {
                $this->uploadExcel($request, $data);
            }

            if ($request->object_3d ?? false) {
                $path = 'master-ercm/fleet';
                $data->update([
                    'attachment' => $this->uploadFileName($request->file('object_3d'), $path)
                ]);

                DB::commit();
                return redirect()->route('fleet.form.group.item.landing', [$fleet->id, $form->id, $data->id]);
            }

            DB::commit();
            return redirect()->route('fleet.form.group.index', [$fleet->id, $form->id])->with('success', 'Berhasil menambah group!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal menambah group!');
        }
    }

    public function show(Request $request, Fleet $fleet, MstErcmVesselSystem $form)
    {
        $data = MstErcmVesselGroup::find($request->id);
        if (!$data) {
            return redirect()->back()->with('failed', 'Data tidak ditemukan!');
        }

        $data = [
            'data'          => $data,
            'used_value'    => MstErcmVesselGroup::where('id', '!=', $request->id)->where('mst_ercm_vessel_system_id', $form->id)->sum('value')
        ];

        return response()->json($data);
    }

    public function update(Request $request, Fleet $fleet, MstErcmVesselSystem $form)
    {
        $request->validate([
            'name'  => 'required',
            'value' => 'required|gt:0'
        ]);

        DB::beginTransaction();

        try {
            $data = MstErcmVesselGroup::find($request->id);
            if (!$data) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $existName = MstErcmVesselGroup::isExistsName($request->name, $fleet->id, $request->id);
            if ($existName) {
                DB::rollBack();
                return redirect()->back()->with('failed', $request->name . ' group pada armada ' . $fleet->nama . ' sudah ada!');
            }

            $otherValue = MstErcmVesselGroup::where('id', '!=', $request->id)->where('mst_ercm_vessel_system_id', $form->id)->sum('value');
            if (($otherValue + $request->value) > 100) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Proportional value melebihi nilai maksimum!');
            }

            $data->update([
                'name'          => $request->name,
                'value'         => $request->value,
                'updated_by'    => auth()->user()->id
            ]);

            $isExcel = str_contains($data->attachment, '.xlsx') || str_contains($data->attachment, '.xls');
            if (empty($isExcel) && !empty($data->attachment)) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'File 3D sudah ada!');
            }

            if ($request->excel_file ?? false) {
                $this->uploadExcel($request, $data);
            }

            if ($request->object_3d ?? false) {
                $path = 'master-ercm/fleet';
                $data->update([
                    'attachment' => $this->uploadFileName($request->file('object_3d'), $path)
                ]);

                DB::commit();
                return redirect()->route('fleet.form.group.item.landing', [$fleet->id, $form->id, $data->id]);
            }

            DB::commit();
            return redirect()->route('fleet.form.group.index', [$fleet->id, $form->id])->with('success', 'Berhasil megubah group!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal mengubah group!');
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();

        $data = MstErcmVesselGroup::find($request->id);
        if (!$data) {
            DB::rollBack();
            \Session::flash('failed', 'Data tidak ditemukan!');
            return;
        }

        try {
            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus data!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus data!');
        }
    }

    private function uploadExcel(Request $request, MstErcmVesselGroup $group)
    {
        $user = auth()->user();
        $path = 'master-ercm/fleet';
        $components = [];
        $rows = \Excel::toArray(new MstErcmFleetImport, $request->file('excel_file'))[0] ?? [];
        foreach ($rows as $row) {
            $components[] = [
                'component'     => $row[1],
                'created_by'    => $user->id
            ];
        }

        $group->mst_ercm_vessel_components()->createMany($components);
        $group->update([
            'attachment' => $this->uploadFile($request->file('excel_file'), $path)
        ]);
    }

    private function datatable(Fleet $fleet, MstErcmVesselSystem $form)
    {
        $data = MstErcmVesselGroup::where('mst_ercm_vessel_system_id', $form->id)->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('group_qty', function () {
                return 0;
            })
            ->addColumn('component_qty', function () {
                return 0;
            })
            ->addColumn('detail', function ($row) use ($fleet, $form) {
                return '
                    <a href="' . route('fleet.form.group.item.index', [$fleet->id, $form->id, $row->id]) . '" class="btn btn-sm btn-warning fw-bold">
                    ' . __('Detail') . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) use ($fleet) {
                $button = '
                    <a type="button" class="btn btn-sm btn-success btn-icon btn-edit" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>

                    <a type="button" class="btn btn-sm btn-danger btn-icon btn-delete" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
                return $button;
            })
            ->rawColumns(['action', 'detail'])
            ->make(true);
    }
}
