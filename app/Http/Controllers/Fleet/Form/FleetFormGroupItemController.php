<?php

namespace App\Http\Controllers\Fleet\Form;

use App\Http\Controllers\Controller;
use App\Models\Fleet\ERCM\MstErcmVesselComponent;
use App\Models\Fleet\ERCM\MstErcmVesselComponentConditionCriteria;
use App\Models\Fleet\ERCM\MstErcmVesselComponentStandardParameter;
use App\Models\Fleet\ERCM\MstErcmVesselComponentStandardParameterApprovalParam;
use App\Models\Fleet\ERCM\MstErcmVesselComponentStandardParameterAreaParam;
use App\Models\Fleet\ERCM\MstErcmVesselComponentStandardParameterNumberParam;
use App\Models\Fleet\ERCM\MstErcmVesselComponentStandardParameterRangeParam;
use App\Models\Fleet\ERCM\MstErcmVesselGroup;
use App\Models\Fleet\ERCM\MstErcmVesselSubGroup;
use App\Models\Fleet\ERCM\MstErcmVesselSubGroupConditionCriteria;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameter;
use App\Models\Fleet\ERCM\MstErcmVesselSystem;
use App\Models\Fleet\Fleet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class FleetFormGroupItemController extends Controller
{
    public function index(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        if ($request->ajax()) {
            if ($request->type == 'component') {
                return $this->datatable($group);
            } else {
                return $this->datatableGrouped($fleet, $form, $group);
            }
        }

        $data = [
            'fleet'             => $fleet,
            'form'              => $form,
            'group'             => $group,
            'sub_groups'        => $form->mst_ercm_vessel_subgroups,
            'is_excel'          => str_contains($group->attachment, '.xlsx') || str_contains($group->attachment, '.xls'),
            'assestment_types'  => MstErcmVesselSubgroupStandardParameter::$assestmentTypes,
            'parameters'        => MstErcmVesselSubgroupStandardParameter::$parameterNames,
            'order_types'       => MstErcmVesselSubgroupStandardParameter::$orderTypes,
            'valuation_types'   => MstErcmVesselSubgroupStandardParameter::$valuationTypes,
            'colors'            => MstErcmVesselSubgroupStandardParameter::$colors,
            'criteria_status'   => MstErcmVesselSubGroupConditionCriteria::$status
        ];

        return view('pages.fleet.form.group.item.index', compact('data'));
    }

    public function landing(Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        $data = [
            'fleet'         => $fleet,
            'form'          => $form,
            'group'         => $group,
            'sub_groups'    => $form->mst_ercm_vessel_subgroups
        ];

        return view('pages.fleet.form.group.item.landing', compact('data'));
    }

    public function upload(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        ini_set('upload_max_filesize', '1500M');
        ini_set('post_max_size', '1510M');

        DB::beginTransaction();

        try {
            $path = 'master-ercm/fleet';
            if ($request->is_confirmation) {
                $excepts = null;
                if ($request->exist_components) {
                    $excepts = json_decode($request->exist_components, true);
                }

                $this->storeComponent($request, $fleet, $form, $group, $excepts);

                if (!$request->not_excel) {
                    $excel = 'upload/' . $path . '/' . $group->attachment;
                    $temp = 'upload/' . $path . '/temp/' . $group->attachment_temp;
                    $group->update([
                        'attachment' => $group->attachment_temp,
                        'attachment_temp' => null
                    ]);

                    // delete temp
                    if (File::exists($temp)) {
                        File::move(public_path($temp), public_path('upload/' . $path . '/' . $group->attachment));
                        File::delete($temp);
                    }

                    // delete excel
                    if (File::exists($excel)) {
                        File::delete($excel);
                    }
                }

                DB::commit();
                return redirect()->route('fleet.form.group.item.index', [$fleet->id, $form->id, $group->id])->with('success', 'Berhasil mengunggah 3D file!');
            } else {
                $isExcel = str_contains($group->attachment, '.xlsx') || str_contains($group->attachment, '.xls');
                if ($group->attachment_temp && \File::exists(public_path($path) . '/temp/' . $this->getFileName($group->attachment_temp))) {
                    \File::delete(public_path($path) . '/temp/' . $this->getFileName($group->attachment_temp));
                }

                if ($isExcel) {
                    $group->update([
                        'attachment_temp' => $this->uploadFileName($request->file('3d_file'), $path . '/temp')
                    ]);

                    DB::commit();
                    return redirect()->route('fleet.form.group.item.confirmation', [$fleet->id, $form->id, $group->id]);
                } else {
                    $group->update([
                        'attachment' => $this->uploadFileName($request->file('3d_file'), $path)
                    ]);

                    DB::commit();
                    return redirect()->route('fleet.form.group.item.landing', [$fleet->id, $form->id, $group->id]);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', $e->getMessage());
        }
    }

    public function confirmation(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        $data = [
            'fleet'         => $fleet,
            'form'          => $form,
            'group'         => $group,
            'components'    => $group->mst_ercm_vessel_components
        ];

        return view('pages.fleet.form.group.item.confirmation', compact('data'));
    }

    public function grouped(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            MstErcmVesselSubGroup::where('id', $request->mst_ercm_vessel_subgroup_id)
                ->update([
                    'mst_ercm_vessel_group_id' => $group->id,
                    'updated_by' => $user->id
                ]);

            MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)
                ->whereIn('id', explode(',', $request->mst_ercm_vessel_components))
                ->update([
                    'mst_ercm_vessel_subgroup_id' => $request->mst_ercm_vessel_subgroup_id,
                    'updated_by' => $user->id
                ]);

            $this->parameterComponentFromParent($request);

            DB::commit();
            return response()->json(['message' => 'Berhasil mengelompokan component!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal mengelompokan component!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function groupedDetail(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        if ($request->ajax() && $request->type) {
            return $this->datatableSubGroup($request, $fleet, $form, $group);
        }

        $subGroup = MstErcmVesselSubGroup::find($request->mst_ercm_vessel_subgroup_id);
        if (!$subGroup) {
            return response()->json([
                'message' => 'Data tidak ditemukan!',
            ], 500);
        }

        $data = [
            'sub_group' => $subGroup
        ];

        return response()->json($data);
    }

    public function groupedDestroy(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            if ($request->type == 'SubGroup') {
                $components = MstErcmVesselComponent::where('mst_ercm_vessel_subgroup_id', $request->mst_ercm_vessel_subgroup_id)->get();
                foreach ($components as $component) {
                    $component->mst_ercm_vessel_component_standard_parameters()->delete();
                }

                MstErcmVesselSubGroup::where('id', $request->mst_ercm_vessel_subgroup_id)
                    ->update([
                        'mst_ercm_vessel_group_id' => null,
                        'updated_by' => $user->id
                    ]);

                MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)
                    ->where('mst_ercm_vessel_subgroup_id', $request->mst_ercm_vessel_subgroup_id)
                    ->update([
                        'mst_ercm_vessel_subgroup_id' => null,
                        'updated_by' => $user->id
                    ]);
            } else {
                $component = MstErcmVesselComponent::find($request->mst_ercm_vessel_component_id);
                if (!$component) {
                    throw new \Exception('Komponen tidak ditemukan');
                }

                $component->mst_ercm_vessel_component_standard_parameters()->delete();

                $hasComponents = MstErcmVesselSubGroup::where('id', $component->mst_ercm_vessel_subgroup_id)
                    ->whereHas('mst_ercm_vessel_components', function ($query) use ($component) {
                        $query->where('id', '!=', $component->id);
                    })
                    ->exists();
                if (!$hasComponents) {
                    MstErcmVesselSubGroup::where('id', $component->mst_ercm_vessel_subgroup_id)
                        ->update([
                            'mst_ercm_vessel_group_id' => null,
                            'updated_by' => $user->id
                        ]);
                }

                $component->mst_ercm_vessel_subgroup_id = null;
                $component->updated_by = $user->id;
                $component->save();
            }

            DB::commit();
            return response()->json(['message' => 'Berhasil menghapus data!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal menghapus data!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function showSubGroupParameter(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, MstErcmVesselSubGroup $sub)
    {
        return (new FleetFormController)->showSubGroupParameter($request, $fleet, $sub);
    }

    public function updateSubGRoupParameter(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, MstErcmVesselSubGroup $sub)
    {
        return (new FleetFormController)->updateSubGroupParameter($request, $fleet, $sub);
    }

    public function showComponentParameter(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, MstErcmVesselComponent $component)
    {
        try {
            $component = MstErcmVesselComponent::find($component->id);
            if (!$component) {
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $data = $totalValue = [];
            $stdParam = new MstErcmVesselComponentStandardParameter;
            $orderTypes = [
                1 => 'component_standard_parameter_areas',
                2 => 'component_standard_parameter_number',
                3 => 'component_standard_parameter_ranges',
                4 => 'component_standard_parameter_approvals',
            ];
            $parameters = $stdParam::where('mst_ercm_vessel_component_id', $component->id)
                ->with([
                    'component_standard_parameter_number',
                    'component_standard_parameter_areas',
                    'component_standard_parameter_ranges',
                    'component_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();

            foreach ($stdParam::$assestmentTypes as $key => $value) {
                $i = 0;
                $assestmentParams = $parameters->where('assestment_type', $value)->toArray();
                $totalValue[$key] = array_sum(array_column($assestmentParams, 'value'));
                foreach ($assestmentParams as $param) {
                    $relation = $orderTypes[$param['order_type']];
                    $param['order_parameters'] = $param[$relation];
                    $param['range_parameters'] = [];

                    if ($param['order_type'] == 3) {
                        $j = 0;
                        $rangeParams = [];
                        $orderParams = collect($param['order_parameters'])->groupBy('color')->all();
                        foreach ($orderParams as $order) {
                            $criterias = collect($order)
                                ->map(function ($val, $idx) {
                                    return ['criteria' => $val['criteria']];
                                })
                                ->all();

                            $rangeParams[$j] = [
                                'color' => $order[0]['color'] ?? null,
                                'range' => $order[0]['range'] ?? null,
                                'criterias' => $criterias
                            ];

                            $j++;
                        }

                        $param['order_parameters'] = [];
                        $param['range_parameters'] = $rangeParams;
                    }

                    $data[$key]['parameters'][$i] = $param;
                    $data[$key]['parameters'][$i]['valuation_type'] = $param['order_parameters']['valuation_type'] ?? $param['order_parameters'][0]['valuation_type'] ?? null;
                    $data[$key]['assestment_type'] = $value;
                    $data[$key]['type_stage'] = $param['type_stage'] ?? null;

                    $i++;
                }
            }

            $data = [
                'title'                 => 'Component: ' . $component->component,
                'fleet'                 => $fleet,
                'form'                  => $component->mst_ercm_vessel_group->mst_ercm_vessel_system,
                'group'                 => $component->mst_ercm_vessel_group,
                'component'             => $component,
                'forms'                 => $data,
                'total_value'           => $totalValue,
                'condition_criteria'    => $component->mst_ercm_vessel_component_condition_criteria,
                'is_preview'            => $request->is_preview ?? false,
                'assestment_types'      => MstErcmVesselSubgroupStandardParameter::$assestmentTypes,
                'parameters'            => MstErcmVesselComponentStandardParameter::$parameterNames,
                'type_stages'           => MstErcmVesselComponentStandardParameter::$typeStages,
                'order_types'           => MstErcmVesselComponentStandardParameter::$orderTypes,
                'valuation_types'       => MstErcmVesselComponentStandardParameter::$valuationTypes,
                'colors'                => MstErcmVesselComponentStandardParameter::$colors,
                'criteria_status'       => MstErcmVesselSubGroupConditionCriteria::$status
            ];

            return view('pages.fleet.form.group.item.form-parameter', compact('data'));
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', 'Gagal mengambil data component parameter');
        }
    }

    public function previewComponentParameter(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, MstErcmVesselComponent $component)
    {
        $request->merge([
            'is_preview' => true
        ]);

        return $this->showComponentParameter($request, $fleet, $form, $group, $component);
    }

    public function updateComponentParameter(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, MstErcmVesselComponent $component)
    {
        DB::beginTransaction();
        try {
            $data = MstErcmVesselComponent::find($component->id);
            if (!$data) {
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $data->mst_ercm_vessel_component_standard_parameters()->delete();
            $data->mst_ercm_vessel_component_condition_criteria()->delete();

            $this->parameterComponent($request, $data);

            DB::commit();
            return redirect()->back()->with('success', 'Berhasil memperbarui component parameter!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', $e->getMessage());
        }
    }

    private function storeComponent(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group, $exceptComponents = null)
    {
        $components = [];
        $userId = auth()->user()->id;
        if (!$exceptComponents) {
            $group->mst_ercm_vessel_components()->delete();
            foreach ($request->components as $component) {
                $components[] = [
                    'component'         => $component['component'],
                    'component3d_id'    => $component['component3d_id'],
                    'created_by'        => $userId
                ];
            }
        } else {
            MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)
                ->whereNotIn('component', $exceptComponents)
                ->delete();

            foreach ($request->components as $component) {
                if (!in_array($component['component'], $exceptComponents)) {
                    $components[] = [
                        'component'         => $component['component'],
                        'component3d_id'    => $component['component3d_id'],
                        'created_by'        => $userId
                    ];
                } else {
                    MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)
                        ->where('component', $component['component'])
                        ->update([
                            'component3d_id' => $component['component3d_id']
                        ]);
                }
            }
        }

        $group->mst_ercm_vessel_components()->createMany($components);
    }

    private function datatable(MstErcmVesselGroup $group)
    {
        $data = MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)->whereNull('mst_ercm_vessel_subgroup_id')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('select', function ($row) {
                return '
                    <div class="form-check">
                        <input class="form-check-input form-checkbox-button selected-component" type="checkbox" name="selected[]" value="' . $row->id . '"  />
                    </div>
                ';
            })
            ->rawColumns(['select'])
            ->make(true);
    }

    private function datatableGrouped(Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        $data = MstErcmVesselSubGroup::with('mst_ercm_vessel_components')
            ->where('mst_ercm_vessel_group_id', $group->id)
            ->whereHas('mst_ercm_vessel_components')
            ->orderBy('updated_at')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('qty', function ($row) {
                return @count($row->mst_ercm_vessel_components);
            })
            ->addColumn('standard', function ($row) use ($fleet, $form, $group) {
                return '
                    <a href="' . route('fleet.form.group.item.sub-param', [$fleet->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-outline btn-outline-primary fw-bold">
                    ' . __('Choose Std') . '
                    </a>
                ';
            })
            ->addColumn('action', function ($row) {
                return '
                    <a type="button" class="btn btn-sm btn-primary btn-icon btn-edit-sub" data-id="' . $row->id . '" data-type="detail">
                        ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                    </a>

                    <a type class="btn btn-sm btn-success btn-icon btn-edit-sub" data-id="' . $row->id . '" data-type="edit">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>

                    <a class="btn btn-sm btn-danger btn-icon btn-delete-sub" data-id="' . $row['id'] . '" data-type="SubGroup">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
            })
            ->rawColumns(['action', 'standard'])
            ->make(true);
    }

    private function datatableSubGroup(Request $request, Fleet $fleet, MstErcmVesselSystem $form, MstErcmVesselGroup $group)
    {
        $data = MstErcmVesselComponent::where('mst_ercm_vessel_group_id', $group->id)
            ->where('mst_ercm_vessel_subgroup_id', $request->mst_ercm_vessel_subgroup_id)
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($request, $fleet, $form, $group) {
                $button = '';
                if ($request->type == 'edit') {
                    $button = '
                        <a href="' . route('fleet.form.group.item.component-param', [$fleet->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-success btn-icon">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                        </a>

                        <a class="btn btn-sm btn-danger btn-icon btn-delete-sub" data-id="' . $row->id . '" data-sub="' . $request->mst_ercm_vessel_subgroup_id . '" data-type="Component">
                            ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                        </a>
                    ';
                }

                if ($request->type == 'detail') {
                    $button = '
                        <a href="' . route('fleet.form.group.item.preview-component-param', [$fleet->id, $form->id, $group->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                            ' . theme()->getSvgIcon('icons/bi/eye-fill.svg', 'svg-icon-3') . '
                        </a>
                    ';
                }

                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    private function parameterComponentFromParent($request)
    {
        $userId = auth()->user()->id;

        $subGroup = MstErcmVesselSubGroup::find($request->mst_ercm_vessel_subgroup_id);
        if (!$subGroup) {
            throw new \Exception('Data sub group tidak ditemukan');
        }

        foreach (explode(',', $request->mst_ercm_vessel_components) as $id) {
            $parameters = MstErcmVesselSubgroupStandardParameter::where('mst_ercm_vessel_subgroup_id', $request->mst_ercm_vessel_subgroup_id)
                ->with([
                    'subgroup_standard_parameter_number',
                    'subgroup_standard_parameter_areas',
                    'subgroup_standard_parameter_ranges',
                    'subgroup_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();

            $assestmentTypes = $parameters->groupBy('assestment_type')->all();
            if (@count($assestmentTypes) < 3) {
                foreach (MstErcmVesselSubgroupStandardParameter::$assestmentTypes as $key => $value) {
                    if (!isset($assestmentTypes[$value])) {
                        throw new \Exception('Parameter ' . ucfirst($key) . ' stage ' . $subGroup->name . ' tidak ditemukan. Mohon atur parameter dengan benar');
                    }
                }
            }

            foreach ($parameters as $param) {
                $stdParam = MstErcmVesselComponentStandardParameter::create($param->toArray() + [
                    'mst_ercm_vessel_component_id' => $id,
                    'created_by' => $userId
                ]);

                if ($param->subgroup_standard_parameter_number ?? false) {
                    MstErcmVesselComponentStandardParameterNumberParam::create($param->subgroup_standard_parameter_number->toArray() + [
                        'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_areas ?? false as $key => $value) {
                    MstErcmVesselComponentStandardParameterAreaParam::create($value->toArray() + [
                        'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_ranges ?? false as $key => $value) {
                    MstErcmVesselComponentStandardParameterRangeParam::create($value->toArray() + [
                        'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id
                    ]);
                }

                foreach ($param->subgroup_standard_parameter_approvals ?? false as $key => $value) {
                    MstErcmVesselComponentStandardParameterApprovalParam::create($value->toArray() + [
                        'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id
                    ]);
                }
            }

            $condition = MstErcmVesselSubGroupConditionCriteria::where('mst_ercm_vessel_subgroup_id', $request->mst_ercm_vessel_subgroup_id)->first();
            if (!$condition) {
                throw new \Exception('Mohon isi condition criteria dari ' . $subGroup->name);
            }

            MstErcmVesselComponentConditionCriteria::create($condition->toArray() + [
                'mst_ercm_vessel_component_id' => $id,
                'created_by' => $userId
            ]);
        }
    }

    private function parameterComponent(Request $request, MstErcmVesselComponent $component, $isUpdate = false)
    {
        $types = MstErcmVesselComponentStandardParameter::$assestmentTypes;
        $stdParamModel = new MstErcmVesselComponentStandardParameter;
        $userId = auth()->user()->id;
        foreach ($types as $type => $value) {
            if (!isset($request->$type['parameters'])) {
                throw new \Exception('[' . strtoupper($type) . '] Minimal terdapat satu parameter.');
            }

            if ($value == $types['initial']) {
                if (empty($request->$type['type_stage'])) {
                    throw new \Exception('Pilih stage from parameter!');
                }
            }

            foreach ($request->$type['parameters'] as $key => $parameter) {
                if ($parameter['order_type'] ?? false) {
                    if (!($parameter['name_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nama Parameter Ke-' . $key + 1);
                    }

                    if (!($parameter['value_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nilai Parameter Ke-' . $key + 1);
                    } else {
                        if ($parameter['value_' . $parameter['order_type']] == 0) {
                            throw new \Exception('[' . strtoupper($type) . '] Nilai Parameter Ke-' . $key + 1 . ' harus lebih dari 0');
                        }
                    }

                    $stdParam = $stdParamModel::create([
                        'mst_ercm_vessel_component_id'   => $component->id,
                        'name'                          => $parameter['name_' . $parameter['order_type']],
                        'value'                         => $parameter['value_' . $parameter['order_type']],
                        'assestment_type'               => $request->$type['assestment_type'],
                        'order_type'                    => $parameter['order_type'],
                        'type_stage'                    => $request->$type['type_stage'] ?? null,
                        'created_by'                    => $userId,
                    ]);

                    switch ($parameter['order_type']) {
                        case $stdParamModel::$orderTypes['area']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Area Parameter');
                            }

                            foreach ($parameter['order_parameters'] as $param) {
                                MstErcmVesselComponentStandardParameterAreaParam::create([
                                    'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id,
                                    'std'               => $param['std'] ?? null,
                                    'up1'               => $param['up1'] ?? null,
                                    'down1'             => $param['down1'] ?? null,
                                    'color1'            => $param['color1'] ?? null,
                                    'up2'               => $param['up2'] ?? null,
                                    'down2'             => $param['down2'] ?? null,
                                    'color2'            => $param['color2'] ?? null,
                                    'up3'               => $param['up3'] ?? null,
                                    'down3'             => $param['down3'] ?? null,
                                    'color3'            => $param['color3'] ?? null,
                                    'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                                ]);
                            }
                            break;
                        case $stdParamModel::$orderTypes['number']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Number Parameter');
                            }

                            MstErcmVesselComponentStandardParameterNumberParam::create([
                                'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id,
                                'std'               => $parameter['std'] ?? null,
                                'min'               => $parameter['min'] ?? null,
                                'max'               => $parameter['max'] ?? null,
                                'mid1'              => $parameter['mid1'] ?? null,
                                'mid2'              => $parameter['mid2'] ?? null,
                                'less'              => $parameter['less'] ?? null,
                                'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                            ]);
                            break;
                        case $stdParamModel::$orderTypes['ranges']:
                            foreach ($parameter['range_parameters'] as $param) {
                                foreach ($param['criterias'] as $criteria) {
                                    if (!empty($criteria['criteria'])) {
                                        MstErcmVesselComponentStandardParameterRangeParam::create([
                                            'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id,
                                            'range'     => $param['range'],
                                            'color'     => $param['color'],
                                            'criteria'  => $criteria['criteria'],
                                        ]);
                                    }
                                }
                            }
                            break;
                        case $stdParamModel::$orderTypes['approval']:
                            foreach ($parameter['order_parameters'] as $param) {
                                if (!empty($param['param'])) {
                                    MstErcmVesselComponentStandardParameterApprovalParam::create([
                                        'mst_ercm_vessel_component_standard_parameter_id' => $stdParam->id,
                                        'param' => $param['param']
                                    ]);
                                }
                            }
                            break;
                        default:
                            throw new \Exception('Undefined Order Type', 500);
                            break;
                    }
                }
            }
        }

        MstErcmVesselComponentConditionCriteria::create([
            'mst_ercm_vessel_component_id' => $component->id,
            'up1'           => $request->up1,
            'down1'         => $request->down1,
            'status1'       => MstErcmVesselComponentConditionCriteria::$status[strtolower($request->status1)],
            'up2'           => $request->up2,
            'down2'         => $request->down2,
            'status2'       => MstErcmVesselComponentConditionCriteria::$status[strtolower($request->status2)],
            'up3'           => $request->up3,
            'down3'         => $request->down3,
            'status3'       => MstErcmVesselComponentConditionCriteria::$status[strtolower($request->status3)],
            'created_by'    => $userId
        ]);
    }
}
