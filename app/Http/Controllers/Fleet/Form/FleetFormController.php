<?php

namespace App\Http\Controllers\Fleet\Form;

use App\Http\Controllers\Controller;
use App\Models\Fleet\ERCM\MstErcmVesselSubGroup;
use App\Models\Fleet\ERCM\MstErcmVesselSubGroupConditionCriteria;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameter;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterApprovalParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterAreaParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterNumberParam;
use App\Models\Fleet\ERCM\MstErcmVesselSubgroupStandardParameterRangeParam;
use App\Models\Fleet\ERCM\MstErcmVesselSystem;
use App\Models\Fleet\Fleet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FleetFormController extends Controller
{
    public function index(Request $request, Fleet $fleet)
    {
        if ($request->ajax()) {
            return $this->datatable($fleet);
        }

        $data = [
            'fleet'             => $fleet,
            'used_value'        => MstErcmVesselSystem::where('vessel_id', $fleet->id)->sum('value'),
            'assestment_types'  => MstErcmVesselSubgroupStandardParameter::$assestmentTypes,
            'parameters'        => MstErcmVesselSubgroupStandardParameter::$parameterNames,
            'type_stages'       => MstErcmVesselSubgroupStandardParameter::$typeStages,
            'order_types'       => MstErcmVesselSubgroupStandardParameter::$orderTypes,
            'valuation_types'   => MstErcmVesselSubgroupStandardParameter::$valuationTypes,
            'colors'            => MstErcmVesselSubgroupStandardParameter::$colors,
            'criteria_status'   => MstErcmVesselSubGroupConditionCriteria::$status
        ];

        return view('pages.fleet.form.index', compact('data'));
    }

    public function store(Request $request, Fleet $fleet)
    {
        $request->validate([
            'name'  => 'required',
            'value' => 'required|gt:0'
        ]);

        DB::beginTransaction();

        try {
            $existName = MstErcmVesselSystem::isExistsName($request->name, $fleet->id);
            if ($existName) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Form dengan nama ' . $request->name . ' sudah ada!');
            }

            $otherValue = MstErcmVesselSystem::where('vessel_id', $fleet->id)->sum('value');
            if (($otherValue + $request->value) > 100) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Proportional value melebihi nilai maksimum!');
            }

            $user = auth()->user();
            $data = MstErcmVesselSystem::create([
                'vessel_id'     => $fleet->id,
                'name'          => $request->name,
                'value'         => $request->value,
                'created_by'    => $user->id
            ]);

            $dataGroups = [];
            foreach ($request->mst_ercm_vessel_subgroups as $group) {
                if ($group['name'] ?? false) {
                    $existName = MstErcmVesselSubGroup::isExistsName($group['name'], $fleet->id);
                    if ($existName) {
                        DB::rollBack();
                        return redirect()->back()->with('failed', 'Sub group ' . $group['name'] . ' pada armada ' . $fleet->nama . ' sudah ada!');
                    }

                    $dataGroups[] = [
                        'name'          => $group['name'],
                        'value'         => $group['value'],
                        'created_by'    => $user->id
                    ];
                }
            }

            $data->mst_ercm_vessel_subgroups()->createMany($dataGroups);

            DB::commit();
            return redirect()->route('fleet.form.index', $fleet->id)->with('success', 'Berhasil menambah form!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal menambah form!');
        }
    }

    public function show(Request $request, Fleet $fleet)
    {
        $data = MstErcmVesselSystem::find($request->id);
        if (!$data) {
            return redirect()->back()->with('failed', 'Data tidak ditemukan!');
        }

        $data = [
            'data'          => $data,
            'used_value'    => MstErcmVesselSystem::where('id', '!=', $request->id)->where('vessel_id', $fleet->id)->sum('value')
        ];

        return response()->json($data);
    }

    public function update(Request $request, Fleet $fleet)
    {
        $request->validate([
            'name'  => 'required',
            'value' => 'required|gt:0'
        ]);

        DB::beginTransaction();

        try {
            $data = MstErcmVesselSystem::find($request->id);
            if (!$data) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $existName = MstErcmVesselSystem::isExistsName($request->name, $fleet->id, $request->id);
            if ($existName) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Form dengan nama ' . $request->name . ' sudah ada!');
            }

            $otherValue = MstErcmVesselSystem::where('id', '!=', $request->id)->where('vessel_id', $fleet->id)->sum('value');
            if (($otherValue + $request->value) > 100) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Proportional value melebihi nilai maksimum!');
            }

            $user = auth()->user();
            $data->update([
                'name'          => $request->name,
                'value'         => $request->value,
                'updated_by'    => $user->id
            ]);

            DB::commit();
            return redirect()->route('fleet.form.index', $fleet->id)->with('success', 'Berhasil mengubah form!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal mengubah form!');
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();

        $data = MstErcmVesselSystem::find($request->id);
        if (!$data) {
            DB::rollBack();
            \Session::flash('failed', 'Data tidak ditemukan!');
            return;
        }

        try {
            $data->delete();

            DB::commit();
            \Session::flash('success', 'Berhasil menghapus data!');
        } catch (\Exception $e) {
            DB::rollBack();
            \Session::flash('failed', 'Gagal menghapus data!');
        }
    }

    public function indexSubGroup(Request $request, Fleet $fleet)
    {
        $data = MstErcmVesselSubGroup::where('mst_ercm_vessel_system_id', $request->mst_ercm_vessel_system_id)
            ->orderBy('created_at')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($fleet, $request) {
                $button = '
                    <a href="' . route('fleet.form.sub-group.show-param', [$fleet->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                        <i class="ki-duotone ki-eye fs-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                    </a>

                    <a type="button" class="btn btn-sm btn-success btn-icon btn_edit_sub_group" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>
                ';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeSubGroup(Request $request, Fleet $fleet)
    {
        $request->validate([
            'name' => 'required'
        ]);

        DB::beginTransaction();

        try {
            if (!$request->name) {
                throw new \Exception('Nama Sub Group Diperlukan!');
            }

            $existName = MstErcmVesselSubGroup::isExistsName($request->name, $fleet->id);
            if ($existName) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Sub group ' . $request->name . ' pada armada ' . $fleet->nama . ' sudah ada!');
            }

            $data = MstErcmVesselSubGroup::create([
                'mst_ercm_vessel_system_id' => $request->mst_ercm_vessel_system_id,
                'name'                      => $request->name,
                'created_by'                => auth()->user()->id
            ]);

            DB::commit();
            return redirect()->route('fleet.form.sub-group.show-param', [$fleet->id, $data->id])->with('success', 'Berhasil menambah sub group!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal menambah sub group!');
        }
    }

    public function showSubGroup(Request $request)
    {
        $data = MstErcmVesselSubGroup::find($request->id);
        if (!$data) {
            return response()->json([
                'message' => 'Data tidak ditemukan!'
            ], 500);
        }

        $data = [
            'data' => $data,
            'used_value' => MstErcmVesselSubGroup::where('id', '!=', $request->id)->where('mst_ercm_vessel_system_id', $data->mst_ercm_vessel_system_id)->sum('value')
        ];

        return response()->json($data);
    }

    public function updateSubGroup(Request $request, Fleet $fleet)
    {
        DB::beginTransaction();

        try {
            $data = MstErcmVesselSubGroup::find($request->id);
            if (!$data) {
                return response()->json([
                    'message' => 'Data tidak ditemukan!'
                ], 500);
            }

            $existName = MstErcmVesselSubGroup::isExistsName($request->name, $fleet->id, $request->id);
            if ($existName) {
                DB::rollBack();
                return response()->json([
                    'message' => 'Sub group ' . $request->name . ' pada armada ' . $fleet->nama . ' sudah ada!'
                ], 500);
            }

            $otherValue = MstErcmVesselSubGroup::where('id', '!=', $request->id)->where('mst_ercm_vessel_system_id', $data->mst_ercm_vessel_system_id)->sum('value');
            if (($request->value + $otherValue) > 100) {
                DB::rollBack();
                return response()->json([
                    'message' => 'Total value melebihi batas maksimum!'
                ], 400);
            }

            $data->update([
                'name'          => $request->name,
                'value'         => $request->value,
                'updated_by'    => auth()->user()->id
            ]);

            DB::commit();
            return response()->json(['message' => 'Berhasil memperbarui sub group!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => 'Gagal memperbarui sub group!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function ungroupSubGroup(Request $request)
    {
        try {
            $data = MstErcmVesselSubGroup::selectRaw('id, name')
                ->where('mst_ercm_vessel_system_id', $request->mst_ercm_vessel_system_id)
                ->whereNull('mst_ercm_vessel_group_id')
                ->orderBy('name')
                ->get();

            return response()->json([
                'message' => 'Berhasil mendapat sub group!',
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Gagal mendapat sub group!',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function showSubGroupParameter(Request $request, Fleet $fleet, MstErcmVesselSubGroup $sub)
    {
        try {
            $subGroup = $sub;

            $data = $totalValue = [];
            $stdParam = new MstErcmVesselSubgroupStandardParameter;
            $orderTypes = [
                1 => 'subgroup_standard_parameter_areas',
                2 => 'subgroup_standard_parameter_number',
                3 => 'subgroup_standard_parameter_ranges',
                4 => 'subgroup_standard_parameter_approvals',
            ];
            $subGroups = MstErcmVesselSubGroup::where('mst_ercm_vessel_system_id', $sub->mst_ercm_vessel_system_id)->get();
            $parameters = $stdParam::where('mst_ercm_vessel_subgroup_id', $subGroup->id)
                ->with([
                    'subgroup_standard_parameter_number',
                    'subgroup_standard_parameter_areas',
                    'subgroup_standard_parameter_ranges',
                    'subgroup_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();

            foreach ($stdParam::$assestmentTypes as $key => $value) {
                $i = 0;
                $assestmentParams = $parameters->where('assestment_type', $value)->toArray();
                $totalValue[$key] = array_sum(array_column($assestmentParams, 'value'));
                foreach ($assestmentParams as $param) {
                    $relation = $orderTypes[$param['order_type']];
                    $param['order_parameters'] = $param[$relation];
                    $param['range_parameters'] = [];

                    if ($param['order_type'] == 3) {
                        $j = 0;
                        $rangeParams = [];
                        $orderParams = collect($param['order_parameters'])->groupBy('color')->all();
                        foreach ($orderParams as $order) {
                            $criterias = collect($order)
                                ->map(function ($val, $idx) {
                                    return ['criteria' => $val['criteria']];
                                })
                                ->all();

                            $rangeParams[$j] = [
                                'color' => $order[0]['color'] ?? null,
                                'range' => $order[0]['range'] ?? null,
                                'criterias' => $criterias
                            ];

                            $j++;
                        }

                        $param['order_parameters'] = [];
                        $param['range_parameters'] = $rangeParams;
                    }

                    $data[$key]['parameters'][$i] = $param;
                    $data[$key]['parameters'][$i]['valuation_type'] = $param['order_parameters']['valuation_type'] ?? $param['order_parameters'][0]['valuation_type'] ?? null;
                    $data[$key]['assestment_type'] = $value;
                    $data[$key]['type_stage'] = $param['type_stage'] ?? null;

                    $i++;
                }
            }

            $data = [
                'title'                 => 'Sub Group: ' . $subGroup->name,
                'fleet'                 => $fleet,
                'form'                  => $subGroup->mst_ercm_vessel_system,
                'group'                 => $subGroup->mst_ercm_vessel_group,
                'sub_group'             => $subGroup,
                'sub_groups'            => $subGroups,
                'forms'                 => $data,
                'total_value'           => $totalValue,
                'is_preview'            => false,
                'assestment_types'      => MstErcmVesselSubgroupStandardParameter::$assestmentTypes,
                'condition_criteria'    => $subGroup->mst_ercm_vessel_subgroup_condition_criteria,
                'parameters'            => MstErcmVesselSubgroupStandardParameter::$parameterNames,
                'type_stages'           => MstErcmVesselSubgroupStandardParameter::$typeStages,
                'order_types'           => MstErcmVesselSubgroupStandardParameter::$orderTypes,
                'valuation_types'       => MstErcmVesselSubgroupStandardParameter::$valuationTypes,
                'colors'                => MstErcmVesselSubgroupStandardParameter::$colors,
                'criteria_status'       => MstErcmVesselSubGroupConditionCriteria::$status
            ];

            return !str_contains($request->path(), 'item')
                ? view('pages.fleet.form.form-parameter', compact('data'))
                : view('pages.fleet.form.group.item.form-parameter', compact('data'));
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', 'Gagal mengambil data sub group parameter');
        }
    }

    public function updateSubGroupParameter(Request $request, Fleet $fleet, MstErcmVesselSubGroup $sub)
    {
        DB::beginTransaction();
        try {
            $data = MstErcmVesselSubGroup::find($sub->id);
            if (!$data) {
                return redirect()->back()->with('failed', 'Data tidak ditemukan!');
            }

            $data->mst_ercm_vessel_subgroup_standard_parameter()->delete();
            $data->mst_ercm_vessel_subgroup_condition_criteria()->delete();

            $this->parameterSubGroup($request, $data);

            DB::commit();
            return redirect()->back()->with('success', 'Berhasil memperbarui sub group parameter!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('failed', $e->getMessage());
        }
    }

    public function copySubGroupParameter(Request $request, Fleet $fleet, MstErcmVesselSubGroup $sub)
    {
        DB::beginTransaction();

        try {
            $source = MstErcmVesselSubGroup::find($request->source_id);
            if (!$source) {
                DB::rollBack();
                return redirect()->back()->with('failed', 'Data yang akan disalin tidak ditemukan!');
            }

            // delete existing data
            $sub->mst_ercm_vessel_subgroup_standard_parameter()->delete();
            $sub->mst_ercm_vessel_subgroup_condition_criteria()->delete();

            $userId = auth()->user()->id;

            // get source data parameter
            $sourceParameters = MstErcmVesselSubgroupStandardParameter::where('mst_ercm_vessel_subgroup_id', $source->id)
                ->with([
                    'subgroup_standard_parameter_number',
                    'subgroup_standard_parameter_areas',
                    'subgroup_standard_parameter_ranges',
                    'subgroup_standard_parameter_approvals'
                ])
                ->orderBy('id')
                ->get();


            // store data parameter
            foreach ($sourceParameters as $param) {
                $stdParam = MstErcmVesselSubgroupStandardParameter::create(array_merge($param->toArray(), [
                    'mst_ercm_vessel_subgroup_id' => $sub->id,
                    'created_by' => $userId
                ]));

                if ($param->subgroup_standard_parameter_number ?? false) {
                    MstErcmVesselSubgroupStandardParameterNumberParam::create(array_merge($param->subgroup_standard_parameter_number->toArray(), [
                        'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id
                    ]));
                }

                foreach ($param->subgroup_standard_parameter_areas ?? [] as $key => $value) {
                    MstErcmVesselSubgroupStandardParameterAreaParam::create(array_merge($value->toArray(), [
                        'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id
                    ]));
                }

                foreach ($param->subgroup_standard_parameter_ranges ?? [] as $key => $value) {
                    MstErcmVesselSubgroupStandardParameterRangeParam::create(array_merge($value->toArray(), [
                        'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id
                    ]));
                }

                foreach ($param->subgroup_standard_parameter_approvals ?? [] as $key => $value) {
                    MstErcmVesselSubgroupStandardParameterApprovalParam::create(array_merge($value->toArray(), [
                        'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id
                    ]));
                }
            }

            $condition = MstErcmVesselSubGroupConditionCriteria::where('mst_ercm_vessel_subgroup_id', $source->id)->first();
            if ($condition) {
                MstErcmVesselSubgroupConditionCriteria::create(array_merge($condition->toArray(), [
                    'mst_ercm_vessel_subgroup_id' => $sub->id,
                    'created_by' => $userId
                ]));
            }

            DB::commit();
            return redirect()->back()->with('success', 'Berhasil menyalin ' . $source->name);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('failed', 'Gagal menyalin data!');
        }
    }

    private function parameterSubGroup(Request $request, MstErcmVesselSubGroup $subGroup, $isUpdate = false)
    {
        $types = MstErcmVesselSubgroupStandardParameter::$assestmentTypes;
        $stdParamModel = new MstErcmVesselSubgroupStandardParameter;
        $userId = auth()->user()->id;
        foreach ($types as $type => $value) {
            if (!isset($request->$type['parameters'])) {
                throw new \Exception('[' . strtoupper($type) . '] Minimal terdapat satu parameter.');
            }

            if ($value == $types['initial']) {
                if (empty($request->$type['type_stage'])) {
                    throw new \Exception('Pilih stage from parameter!');
                }
            }

            foreach ($request->$type['parameters'] as $key => $parameter) {
                if ($parameter['order_type'] ?? false) {
                    if (!($parameter['name_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nama Parameter Ke-' . $key + 1);
                    }

                    if (!($parameter['value_' . $parameter['order_type']] ?? false)) {
                        throw new \Exception('[' . strtoupper($type) . '] Masukan Nilai Parameter Ke-' . $key + 1);
                    } else {
                        if ($parameter['value_' . $parameter['order_type']] == 0) {
                            throw new \Exception('[' . strtoupper($type) . '] Nilai Parameter Ke-' . $key + 1 . ' harus lebih dari 0');
                        }
                    }

                    $stdParam = $stdParamModel::create([
                        'mst_ercm_vessel_subgroup_id'   => $subGroup->id,
                        'name'                          => $parameter['name_' . $parameter['order_type']],
                        'value'                         => $parameter['value_' . $parameter['order_type']],
                        'assestment_type'               => $request->$type['assestment_type'],
                        'order_type'                    => $parameter['order_type'],
                        'type_stage'                    => $request->$type['type_stage'] ?? null,
                        'created_by'                    => $userId,
                    ]);

                    switch ($parameter['order_type']) {
                        case $stdParamModel::$orderTypes['area']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Area Parameter');
                            }

                            foreach ($parameter['order_parameters'] as $param) {
                                MstErcmVesselSubgroupStandardParameterAreaParam::create([
                                    'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id,
                                    'std'               => $param['std'] ?? null,
                                    'up1'               => $param['up1'] ?? null,
                                    'down1'             => $param['down1'] ?? null,
                                    'color1'            => $param['color1'] ?? null,
                                    'up2'               => $param['up2'] ?? null,
                                    'down2'             => $param['down2'] ?? null,
                                    'color2'            => $param['color2'] ?? null,
                                    'up3'               => $param['up3'] ?? null,
                                    'down3'             => $param['down3'] ?? null,
                                    'color3'            => $param['color3'] ?? null,
                                    'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                                ]);
                            }
                            break;
                        case $stdParamModel::$orderTypes['number']:
                            if (!isset($parameter['valuation_type_' . $parameter['order_type']])) {
                                throw new \Exception('[' . strtoupper($type) . '] Pilih tipe (Increased/Decreased) pada Number Parameter');
                            }

                            MstErcmVesselSubgroupStandardParameterNumberParam::create([
                                'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id,
                                'std'               => $parameter['std'] ?? null,
                                'min'               => $parameter['min'] ?? null,
                                'max'               => $parameter['max'] ?? null,
                                'mid1'              => $parameter['mid1'] ?? null,
                                'mid2'              => $parameter['mid2'] ?? null,
                                'less'              => $parameter['less'] ?? null,
                                'valuation_type'    => $parameter['valuation_type_' . $parameter['order_type']] ?? null,
                            ]);
                            break;
                        case $stdParamModel::$orderTypes['ranges']:
                            foreach ($parameter['range_parameters'] as $param) {
                                foreach ($param['criterias'] as $criteria) {
                                    if (!empty($criteria['criteria'])) {
                                        MstErcmVesselSubgroupStandardParameterRangeParam::create([
                                            'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id,
                                            'range'     => $param['range'],
                                            'color'     => $param['color'],
                                            'criteria'  => $criteria['criteria'],
                                        ]);
                                    }
                                }
                            }
                        case $stdParamModel::$orderTypes['approval']:
                            foreach ($parameter['order_parameters'] as $param) {
                                if (!empty($param['param'])) {
                                    MstErcmVesselSubgroupStandardParameterApprovalParam::create([
                                        'mst_ercm_vessel_subgroup_standard_parameter_id' => $stdParam->id,
                                        'param' => $param['param']
                                    ]);
                                }
                            }
                            break;
                        default:
                            throw new \Exception('Undefined Order Type', 500);
                            break;
                    }
                }
            }
        }

        MstErcmVesselSubGroupConditionCriteria::create([
            'mst_ercm_vessel_subgroup_id' => $subGroup->id,
            'up1'           => $request->up1,
            'down1'         => $request->down1,
            'status1'       => MstErcmVesselSubGroupConditionCriteria::$status[strtolower($request->status1)],
            'up2'           => $request->up2,
            'down2'         => $request->down2,
            'status2'       => MstErcmVesselSubGroupConditionCriteria::$status[strtolower($request->status2)],
            'up3'           => $request->up3,
            'down3'         => $request->down3,
            'status3'       => MstErcmVesselSubGroupConditionCriteria::$status[strtolower($request->status3)],
            'created_by'    => $userId
        ]);
    }

    private function datatable(Fleet $fleet)
    {
        $data = MstErcmVesselSystem::where('vessel_id', $fleet->id)->orderBy('created_at')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                return 'On Progress';
            })
            ->addColumn('action', function ($row) use ($fleet) {
                $button = '
                    <a type="button" class="btn btn-sm btn-warning btn-icon btn_sub_group" data-id="' . $row->id . '">
                        <i class="ki-duotone ki-element-8 fs-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </a>

                    <a href="' . route('fleet.form.group.index', [$fleet->id, $row->id]) . '" class="btn btn-sm btn-primary btn-icon">
                        <i class="ki-duotone ki-data fs-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                            <span class="path4"></span>
                            <span class="path5"></span>
                        </i>
                    </a>

                    <a type="button" class="btn btn-sm btn-success btn-icon btn-edit" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen055.svg', 'svg-icon-3') . '
                    </a>

                    <a type="button" class="btn btn-sm btn-danger btn-icon btn-delete" data-id="' . $row->id . '">
                        ' . theme()->getSvgIcon('icons/duotune/general/gen027.svg', 'svg-icon-3') . '
                    </a>
                ';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
