<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreSubBranchRequest;
use App\Http\Requests\MasterData\Fleet\UpdateSubBranchRequest;
use App\Http\Services\CityProvince;
use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\BranchSub;
use DB;
use Illuminate\Http\Request;
use Session;

class SubBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $provinsi = CityProvince::get_province();
        $branch = Branch::orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.sub_branch.index', compact('branch', 'provinsi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubBranchRequest $request)
    {
        DB::beginTransaction();
        try {
            $subbranch = new BranchSub();
            $subbranch->cabang_id = $request->input('cabang_id');
            $subbranch->kota_id = $request->input('kota_id');

            $subbranch->save();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('fleet.sub_branch.index')->with(['failed' => $th->getMessage()]);
        }

        return redirect()->route('fleet.sub_branch.index')->with(['success' => 'Berhasil Tambah Sub Branch!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BranchSub $branch_sub)
    {
        $data = BranchSub::select("mst_cabang_sub.*", "city.name as city_name", "province.id as province_id", "province.name as province_name")
            ->join("indonesia_cities as city", "mst_cabang_sub.kota_id", "city.id")
            ->join("indonesia_provinces as province", "city.province_code", "province.code")
            ->with('cabang')
            ->where('mst_cabang_sub.id', $branch_sub->id)->first();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubBranchRequest $request, BranchSub $branch_sub)
    {
        DB::beginTransaction();
        try {
            $data['cabang_id'] = $request->cabang_id;
            $data['kota_id'] = $request->kota_id;

            $branch_sub->update($data);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();

            return redirect()->route('fleet.sub_branch.index')->with(['failed' => 'Gagal Ubah Sub Branch!']);
        }

        return redirect()->route('fleet.sub_branch.index')->with(['success' => 'Berhasil Ubah Sub Branch!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BranchSub $branch_sub)
    {
        DB::beginTransaction();

        try {
            $branch_sub->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Sub Branch!');
        }

        Session::flash('success', 'Berhasil Hapus Sub Branch!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = BranchSub::select("mst_cabang_sub.*", "city.name as city_name", "province.name as province_name", "province.id as province_id")
            ->join("indonesia_cities as city", "mst_cabang_sub.kota_id", "city.id")
            ->join("indonesia_provinces as province", "city.province_code", "province.code")
            ->with('cabang')
            ->orderBy('mst_cabang_sub.id', 'DESC')->get();
        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('cabang_id', function ($data) {
                return $data->cabang->nama;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-sub-branch"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_sub_branch_modal"
                    data-id=' . $data->id . '>
                        ' . __('Ubah') . '
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-sub-branch ms-2"
                    data-id=' . $data->id . '>
                        ' . __('Hapus') . '
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
