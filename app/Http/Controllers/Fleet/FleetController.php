<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreFleetRequest;
use App\Http\Requests\MasterData\Fleet\UpdateFleetRequest;
use App\Models\Fleet\Fleet;
use App\Models\Fleet\Track;
use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\Region;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class FleetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cabang = Branch::orderBy('nama', 'ASC')->get();
        $region = Region::orderBy('nama', 'ASC')->get();
        $track = Track::orderBy('nama', 'ASC')->get();
        $user = User::orderBy('name', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.fleet.index', compact('cabang', 'region', 'track', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFleetRequest $request)
    {
        $age = Carbon::parse($request->usia_teknis)->age;
        DB::beginTransaction();
        try {
            $data['regional_id'] = $request->regional_id;
            $data['cabang_id'] = $request->cabang_id;
            $data['nama'] = $request->nama;
            $data['lintasan_id'] = $request->lintasan_id;
            $data['grt'] = $request->grt;
            $data['panjang'] = $request->panjang;
            $data['daya'] = $request->daya;
            $data['satuan'] = $request->satuan;
            $data['usia_teknis'] = $request->usia_teknis;
            $data['status_survey'] = $request->status_survey;
            $data['pic'] = $request->pic;
            $data['os'] = $request->os;

            Fleet::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('fleet.fleet.index')->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('fleet.fleet.index')->with(['success' => 'Berhasil Tambah Data Fleet!']);
    }

    public function update(UpdateFleetRequest $request, Fleet $fleet)
    {
        DB::beginTransaction();
        try {
            $data['regional_id'] = $request->regional_id;
            $data['cabang_id'] = $request->cabang_id;
            $data['nama'] = $request->nama;
            $data['lintasan_id'] = $request->lintasan_id;
            $data['grt'] = $request->grt;
            $data['panjang'] = $request->panjang;
            $data['daya'] = $request->daya;
            $data['satuan'] = $request->satuan;
            $data['usia_teknis'] = $request->usia_teknis;
            $data['status_survey'] = $request->status_survey;
            $data['pic'] = $request->pic;
            $data['os'] = $request->os;

            $fleet->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('fleet.fleet.index')->with(['failed' => 'Gagal Ubah Data Fleet!']);
        }

        return redirect()->route('fleet.fleet.index')->with(['success' => 'Berhasil Ubah Data Fleet!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Fleet $fleet
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Fleet $fleet)
    {
        return $fleet;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Fleet $fleet
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fleet $fleet)
    {
        DB::beginTransaction();

        try {
            $fleet->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Data Fleet!');
        }

        Session::flash('success', 'Berhasil Hapus Data Fleet!');
    }

    protected function datatables()
    {
        $data = Fleet::with('lintasan', 'regional', 'cabang', 'user', 'surveyor')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('regional_id', function ($data) {
                return $data->regional->nama;
            })
            ->editColumn('cabang_id', function ($data) {
                return $data->cabang->nama;
            })
            ->editColumn('lintasan_id', function ($data) {
                return $data->lintasan->nama;
            })
            ->editColumn('pic', function ($data) {
                return $data->user->name;
            })
            ->editColumn('os', function ($data) {
                return $data->surveyor->name ?? '';
            })
            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-sm btn-warning"
                    href="' . route('fleet.form.index', $data->id) . '"    
                    >
                    ' . __('Detail') . '
                    </a>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-success btn-edit-fleet ms-2"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_fleet_modal"
                    data-id=' . $data->id . '>
                        ' . __('Ubah') . '
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-fleet ms-2"
                    data-id=' . $data->id . '>
                        ' . __('Hapus') . '
                    </button>';
                return $button;
            })
            ->editColumn('usia_teknis', function ($row) {
                return Carbon::parse($row->usia_teknis)->diff(Carbon::now())->format('%y Tahun, %m Bulan and %d Hari');
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
