<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreTypeOfTrackRequest;
use App\Http\Requests\MasterData\Fleet\UpdateTypeOfTrackRequest;
use App\Models\Fleet\TypeOfTrack;
use DB;
use Illuminate\Http\Request;
use Session;

class TypeOfTrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regions = TypeOfTrack::orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.typeoftrack.index', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTypeOfTrackRequest $request)
    {
        DB::beginTransaction();
        try {

            $rute = new TypeOfTrack();
            $rute->nama = $request->input('nama');
            $rute->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('fleet.typeoftrack.index')->with(['failed' => 'Gagal Menambahkan Tipe Lintasan!']);
        }

        return redirect()->route('fleet.typeoftrack.index')->with(['success' => 'Berhasil Menambahkan Tipe Lintasan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = TypeOfTrack::where('id', $request->typeoftrack)->first();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTypeOfTrackRequest $request)
    {
        DB::beginTransaction();
        try {
            TypeOfTrack::where('id',$request->typeoftrack)->update([
                'nama' => $request->input('nama')
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('fleet.typeoftrack.index')->with(['failed' => 'Gagal mengubah Data Tipe Lintasan']);
        }

        return redirect()->route('fleet.typeoftrack.index')->with(['success' => 'Berhasil Ubah Data Tipe Lintasan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();

        try {
            TypeOfTrack::destroy($request->typeoftrack);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Tipe Lintasan!');
        }

        Session::flash('success', 'Berhasil Hapus Tipe Lintasan!');
    }

    protected function datatables()
    {
        $data = TypeOfTrack::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-typeoftrack"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_typeoftrack_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-typeoftrack ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
