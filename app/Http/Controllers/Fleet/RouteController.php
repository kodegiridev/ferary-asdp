<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreRouteRequest;
use App\Http\Requests\MasterData\Fleet\UpdateRouteRequest;
use App\Models\Fleet\Route;
use DB;
use Illuminate\Http\Request;
use Session;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regions = Route::orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.route.index', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRouteRequest $request)
    {
        DB::beginTransaction();
        try {

            $rute = new Route();
            $rute->nama = $request->input('nama');
            $rute->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('fleet.route.index')->with(['failed' => 'Gagal Menambahkan Rute!']);
        }

        return redirect()->route('fleet.route.index')->with(['success' => 'Berhasil Menambahkan Rute!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Route $route)
    {
        return $route;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRouteRequest $request, Route $route)
    {
        DB::beginTransaction();
        try {
            $data['nama'] = $request->nama;

            $route->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('fleet.route.index')->with(['failed' => 'Gagal Ubah Data Rute!']);
        }

        return redirect()->route('fleet.route.index')->with(['success' => 'Berhasil Ubah Data Rute!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Route $route)
    {
        DB::beginTransaction();

        try {
            $route->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Rute!');
        }

        Session::flash('success', 'Berhasil Hapus Rute!');
    }

    protected function datatables()
    {
        $data = Route::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-route"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_route_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-route ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
