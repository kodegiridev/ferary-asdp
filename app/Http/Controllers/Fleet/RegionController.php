<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreRegionRequest;
use App\Http\Requests\MasterData\Fleet\UpdateRegionRequest;
use App\Models\MasterRegion\Region;
use DB;
use Illuminate\Http\Request;
use Session;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.region.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegionRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['nama'] = $request->nama;
            $data['kategori'] = 2;
            
            Region::create($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            return redirect()->route('fleet.region.index')->with(['failed' => 'Gagal Tambah Region!']);
        }

        return redirect()->route('fleet.region.index')->with(['success' => 'Berhasil Tambah Region!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return $region;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegionRequest $request, Region $region)
    {
        DB::beginTransaction();
        try {
            $data['nama'] = $request->nama;

            $region->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('fleet.region.index')->with(['failed' => 'Gagal Ubah Region!']);
        }

        return redirect()->route('fleet.region.index')->with(['success' => 'Berhasil Ubah Region!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        DB::beginTransaction();

        try {
            $region->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Region!');
        }

        Session::flash('success', 'Berhasil Hapus Region!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Region::orderBy('id', 'DESC')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-region"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_region_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-region ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
