<?php

namespace App\Http\Controllers\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Requests\MasterData\Fleet\StoreTrackRequest;
use App\Http\Requests\MasterData\Fleet\UpdateTrackRequest;
use App\Models\Fleet\Route;
use App\Models\Fleet\Track;
use App\Models\Fleet\TypeOfTrack;
use App\Models\Port\Dock;
use App\Models\Port\Port;
use DB;
use Illuminate\Http\Request;
use Session;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rute = Route::orderBy('nama', 'ASC')->get();
        $type = TypeOfTrack::orderBy('nama', 'ASC')->get();
        $dock = Port::orderBy('nama', 'ASC')->get();
        if ($request->ajax()) {
            return $this->datatables();
        }
        return view('pages.fleet.track.index', compact('rute', 'type', 'dock'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrackRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new Track();
            $data->nama = $request->input('nama');
            $data->pelabuhan_id = $request->input('pelabuhan_id');
            $data->tipe_lintasan_id = $request->input('tipe_lintasan_id');
            $data->rute_id = $request->input('rute_id');

            $data->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('fleet.track.index')->with(['failed' => 'Gagal menambahkan lintasan!']);
        }

        return redirect()->route('fleet.track.index')->with(['success' => 'Berhasil Tambah Lintasan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  Track $track
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        return $track;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Track $track
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTrackRequest $request, Track $track)
    {
        DB::beginTransaction();
        try {
            $data['nama'] = $request->nama;
            $data['tipe_lintasan_id'] = $request->tipe_lintasan_id;
            $data['pelabuhan_id'] = $request->pelabuhan_id;
            $data['rute_id'] = $request->rute_id;

            $track->update($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('fleet.track.index')->with(['failed' => 'Gagal Ubah Lintasan!']);
        }

        return redirect()->route('fleet.track.index')->with(['success' => 'Berhasil Ubah Lintasan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Track $track
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Track $track)
    {
        DB::beginTransaction();

        try {
            $track->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Lintasan!');
        }

        Session::flash('success', 'Berhasil Hapus Lintasan!');
    }

    protected function datatables()
    {
        $data = Track::with('tipe_lintasan', 'rute', 'pelabuhan')->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-track"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_track_modal"
                    data-id=' . $data->id . '>
                        ' . __('Ubah') . '
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-track ms-2"
                    data-id=' . $data->id . '>
                        ' . __('Hapus') . '
                    </button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
