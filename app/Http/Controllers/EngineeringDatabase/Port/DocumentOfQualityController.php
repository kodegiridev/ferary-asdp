<?php

namespace App\Http\Controllers\EngineeringDatabase\Port;

use Auth;
use DB;
use Storage;
use Pdf;
use QrCode;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Port\DocumentOfQuality;
use App\Models\EngineeringDatabase\Port\DocumentOfQualityFile;
use App\Models\Port\Dock;
use App\Models\Port\Port;
use App\Models\User;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Port\StoreDocumentOfQualityRequest;
use App\Http\Requests\EngineeringDatabase\Port\UpdateDocumentOfQualityRequest;

class DocumentOfQualityController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Document Of Quality';
        $this->pathFile = 'EngineeringDatabase/Port/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables();
        }

        return view('pages.engineering-database.port.document-of-quality.index');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = Dock::with(['pelabuhan', 'pelabuhan.cabang'])
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('cabang_id', function ($data) {
                return $data->pelabuhan->cabang->nama;
            })
            ->editColumn('pelabuhan_id', function ($data) {
                return $data->pelabuhan->nama;
            })
            ->editColumn('cabang_kelas', function ($data) {
                return $data->pelabuhan->cabang->cabang_kelas;
            })
            ->addColumn('action', function ($data) {
                $button = '<a
                    class="btn btn-sm btn-info"
                    href="'.route('ed.document-of-quality.detailApproved', $data->id).'">
                        '.__('Detail').'
                    </a>';
                $button .= '<a
                    class="btn btn-sm btn-primary ms-2"
                    href="'.route('ed.document-of-quality.indexDetail', $data->id).'">
                        '.__('Tambah').'
                    </a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDetail(Request $request, $dock_id)
    {
        if ($request->ajax()) {
            return $this->datatablesDetail($dock_id);
        }

        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        return view('pages.engineering-database.port.document-of-quality.index-detail', compact('dock_id','approvers','verificators'));
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesDetail($dock_id)
    {
        $data = DocumentOfQuality::whereNull('approval_id')
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-document-of-quality"
                    data-bs-toggle="modal"
                    data-bs-target="#document_document_of_quality_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-document-of-quality"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-document-of-quality ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Display a total of the resource.
     * @param dock_id
     *
     * @return \Illuminate\Http\Response
     */
    public function countDocumentOfQuality($dock_id)
    {
        $total = DocumentOfQuality::where('dermaga_id', $dock_id)
                ->whereNull('approval_id')
                ->count();

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Get total data dokumen sukses'),
            'data' => $total
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailApproved(Request $request, $dock_id)
    {
        if ($request->ajax()) {
            return $this->datatablesDetailApproved($dock_id);
        }

        return view('pages.engineering-database.port.document-of-quality.detail-approved', compact('dock_id'));
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesDetailApproved($dock_id)
    {
        $data = DocumentOfQuality::query()->with([
            'documents',
            'approval'
        ])
        ->whereHas('approval', function ($query) {
            $query->whereNotNull('tgl_penyetujuan');
        })
        ->where('dermaga_id', $dock_id)
        ->orderBy('updated_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-document-of-quality"
                    data-bs-toggle="modal"
                    data-bs-target="#document_document_of_quality_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-document-of-quality"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tanggal_disetujui', function($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDocumentOfQualityRequest $request, $dock_id)
    {
        DB::beginTransaction();
        try {
            $data['dermaga_id'] = $dock_id;
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['nama_komponen'] = $request->nama_komponen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $documentOfQuality = DocumentOfQuality::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['document_of_quality_id'] = $documentOfQuality->id;
                $data['file'] = $this->pathFile.$dokumenName;

                DocumentOfQualityFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'message' => __('Gagal Tambah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Tambah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentOfQuality $documentOfQuality)
    {
        return $documentOfQuality;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDocumentOfQualityRequest $request, DocumentOfQuality $documentOfQuality)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['nama_komponen'] = $request->nama_komponen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $documentOfQuality->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = DocumentOfQualityFile::where('document_of_quality_id', $documentOfQuality->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['document_of_quality_id'] = $documentOfQuality->id;
                    $data['file'] = $this->pathFile.$dokumenName;

                    DocumentOfQualityFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'message' => __('Gagal Ubah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Ubah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentOfQuality $documentOfQuality)
    {
        DB::beginTransaction();

        try {
            if ($documentOfQuality->approval_id) {
                Approval::where('id', $documentOfQuality->approval_id)->delete();
            }
            
            $dokumenExists = DocumentOfQualityFile::where('document_of_quality_id', $documentOfQuality->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $documentOfQuality->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'status' => false,
                'message' => __('Gagal Hapus Komponen!')
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Berhasil Hapus Komponen')
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Get the specified resource.
     *
     * @param  DocumentOfQuality  $documentOfQuality
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(DocumentOfQuality $documentOfQuality)
    {
        $document = DocumentOfQualityFile::where('document_of_quality_id', $documentOfQuality->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $document_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = DocumentOfQualityFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     * @param  int  $dock_id
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request, $dock_id)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju ?? '';
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                DocumentOfQuality::whereNull('approval_id')
                    ->where('dermaga_id', $dock_id)
                    ->update($data);
            }

            $documentOFQuality = DocumentOfQuality::where('approval_id', $approval->id)
                ->pluck('nama_komponen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $documentOFQuality;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.document-of-quality.indexDetail', $dock_id)
                ->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('ed.document-of-quality.indexDetail', $dock_id)
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Print the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function print($dock_id)
    {
        $dock = Dock::with('pelabuhan')->where('id',$dock_id)->first();

        $documentOfQuality = DocumentOfQuality::query()->with([
            'documents',
            'approval'
        ])
        ->whereHas('approval', function ($query) {
            $query->whereNotNull('tgl_penyetujuan');
        })
        ->where('dermaga_id', $dock_id)
        ->orderBy('updated_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->get();

        $approverName = $documentOfQuality[0]->approval->penyetuju->name;
        $approverRole = $documentOfQuality[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.port.document-of-quality.print', compact(
            'dock',
            'documentOfQuality',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = DocumentOfQuality::where($data)
            ->exists();

        return $isExist;
    }
}
