<?php

namespace App\Http\Controllers\EngineeringDatabase\Port;

use Auth;
use DB;
use Storage;
use Pdf;
use QrCode;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Port\FacilityStandard;
use App\Models\EngineeringDatabase\Port\FacilityStandardFile;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Port\StoreFacilityStandardRequest;
use App\Http\Requests\EngineeringDatabase\Port\UpdateFacilityStandardRequest;

class FacilityStandardController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Facility Standard';
        $this->pathFile = 'EngineeringDatabase/Port/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables();
        }

        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        return view('pages.engineering-database.port.facility-standard.index', compact(
            'approvers',
            'verificators'
        ));
    }

    /**
     * Get count approved technical quality standard
     *
     * @return json
     */
    public function countFacilityStandard()
    {
        $countData = FacilityStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->count();

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Get Total Dokumen Sukses'),
            'data' => $countData
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = FacilityStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-facility-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_facility_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-facility-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#create_facility_standard_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-facility-standard ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFacilityStandardRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $facilityStandard = FacilityStandard::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['facility_standard_id'] = $facilityStandard->id;
                $data['file'] = $this->pathFile.$dokumenName;

                FacilityStandardFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'message' => __('Gagal Tambah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Tambah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Get the specified resource.
     *
     * @param  EquipmentBoard  $equipmentBoard
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(FacilityStandard $facilityStandard)
    {
        $document = FacilityStandardFile::where('facility_standard_id', $facilityStandard->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $document_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = FacilityStandardFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FacilityStandard $facilityStandard)
    {
        return $facilityStandard;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacilityStandard $facilityStandard)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $facilityStandard->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = FacilityStandardFile::where('facility_standard_id', $facilityStandard->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['facility_standard_id'] = $facilityStandard->id;
                    $data['file'] = $this->pathFile.$dokumenName;

                    FacilityStandardFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            $result = [
                'code' => 400,
                'message' => __('Gagal Ubah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Ubah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacilityStandard $facilityStandard)
    {
        DB::beginTransaction();

        try {
            if ($facilityStandard->approval_id) {
                Approval::where('id', $facilityStandard->approval_id)->delete();
            }

            $dokumenExists = FacilityStandardFile::where('facility_standard_id', $facilityStandard->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $facilityStandard->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'status' => false,
                'message' => __('Gagal Hapus Dokumen!')
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Berhasil Hapus Dokumen')
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju ?? '';
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                FacilityStandard::whereNull('approval_id')
                    ->update($data);
            }

            $facilityStandards = FacilityStandard::where('approval_id', $approval->id)
                ->pluck('nama_dokumen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $facilityStandards;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.facility-standard.index')
                ->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('ed.facility-standard.index')
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Display a listing of the resource approved.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentsApproved(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablesApproved();
        }

        return abort(404);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesApproved()
    {
        $data = FacilityStandard::query()->with([
                'approval',
                'documents'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-facility-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_facility_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-facility-standard"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tanggal_disetujui', function($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Print the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print()
    {
        $facilityStandards = FacilityStandard::query()->with([
            'documents',
            'approval'
        ])
        ->whereHas('approval', function ($query) {
            $query->whereNotNull('tgl_penyetujuan');
        })
        ->orderBy('updated_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->get();

        $approverName = $facilityStandards[0]->approval->penyetuju->name;
        $approverRole = $facilityStandards[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.port.facility-standard.print', compact(
            'facilityStandards',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = FacilityStandard::where($data)
            ->exists();

        return $isExist;
    }
}
