<?php

namespace App\Http\Controllers\EngineeringDatabase\Port;

use Auth;
use DB;
use Storage;
use Pdf;
use QrCode;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Port\PortDrawing;
use App\Models\EngineeringDatabase\Port\PortDrawingFile;
use App\Models\Port\Dock;
use App\Models\Port\Port;
use App\Models\User;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Port\StorePortDrawingRequest;
use App\Http\Requests\EngineeringDatabase\Port\UpdatePortDrawingRequest;

class PortDrawingController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Port Drawing';
        $this->pathFile = 'EngineeringDatabase/Port/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPort(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablesPort();
        }

        return view('pages.engineering-database.port.port-drawing.index-port');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $dock_id)
    {
        $imageType = [
            'approval drawing',
            'shop drawing',
            'as build drawing'
        ];

        $imageStatus = [
            'disetujui',
            'belum disetujui'
        ];

        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        $countPortDrawing = PortDrawing::whereNull('approval_id')
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatables($dock_id);
        }

        return view('pages.engineering-database.port.port-drawing.index', compact(
            'dock_id',
            'imageType',
            'imageStatus',
            'approvers',
            'verificators',
            'countPortDrawing'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePortDrawingRequest $request, $dock_id)
    {
        DB::beginTransaction();
        try {
            $data['dermaga_id'] = $dock_id;
            $data['nama_komponen'] = $request->nama_komponen;
            $data['nama_gambar'] = $request->nama_gambar;
            $data['nomor_gambar'] = $request->nomor_gambar;
            $data['tipe_gambar'] = $request->tipe_gambar;
            $data['status_gambar'] = $request->status_gambar;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $portDrawing = PortDrawing::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['port_drawing_id'] = $portDrawing->id;
                $data['file'] = $this->pathFile.$dokumenName;

                PortDrawingFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'message' => __('Gagal Tambah Komponen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Tambah Komponen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePortDrawingRequest $request, PortDrawing $portDrawing)
    {
        DB::beginTransaction();
        try {
            $data['nama_komponen'] = $request->nama_komponen;
            $data['nama_gambar'] = $request->nama_gambar;
            $data['nomor_gambar'] = $request->nomor_gambar;
            $data['tipe_gambar'] = $request->tipe_gambar;
            $data['status_gambar'] = $request->status_gambar;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $portDrawing->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = PortDrawingFile::where('port_drawing_id', $portDrawing->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['port_drawing_id'] = $portDrawing->id;
                    $data['file'] = $this->pathFile.$dokumenName;

                    PortDrawingFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            $result = [
                'code' => 400,
                'message' => __('Gagal Ubah Komponen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Ubah Komponen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailPort(Request $request, $dock_id)
    {
        $countPortDrawing = PortDrawing::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatablesPortDetail($dock_id);
        }

        return view('pages.engineering-database.port.port-drawing.detail-port', compact(
            'dock_id',
            'countPortDrawing'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  EquipmentBoard  $portDrawing
     *
     * @return \Illuminate\Http\Response
     */
    public function show(PortDrawing $portDrawing)
    {
        return $portDrawing;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PortDrawing $portDrawing)
    {
        DB::beginTransaction();

        try {
            if ($portDrawing->approval_id) {
                Approval::where('id', $portDrawing->approval_id)->delete();
            }
            
            $dokumenExists = PortDrawingFile::where('port_drawing_id', $portDrawing->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $portDrawing->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'status' => false,
                'message' => __('Gagal Hapus Komponen!')
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Berhasil Hapus Komponen')
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables($dock_id)
    {
        $data = PortDrawing::whereNull('approval_id')
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-port-drawing"
                    data-bs-toggle="modal"
                    data-bs-target="#document_port_drawing_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-port-drawing"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_port_drawing_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-port-drawing ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesPort()
    {
        $data = Dock::with(['pelabuhan', 'pelabuhan.cabang'])
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('cabang_id', function ($data) {
                return $data->pelabuhan->cabang->nama;
            })
            ->editColumn('pelabuhan_id', function ($data) {
                return $data->pelabuhan->nama;
            })
            ->editColumn('cabang_kelas', function ($data) {
                return $data->pelabuhan->cabang->cabang_kelas;
            })
            ->addColumn('action', function ($data) {
                $button = '<a
                    class="btn btn-sm btn-info"
                    href="'.route('ed.port-drawing.detailPort', $data->id).'">
                        '.__('Detail').'
                    </a>';
                $button .= '<a
                    class="btn btn-sm btn-primary ms-2"
                    href="'.route('ed.port-drawing.index', $data->id).'">
                        '.__('Tambah').'
                    </a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesPortDetail($dock_id)
    {
        $data = PortDrawing::query()->with([
                'approval',
                'documents'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-port-drawing"
                    data-bs-toggle="modal"
                    data-bs-target="#document_port_drawing_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-port-drawing"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tanggal_disetujui', function($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Get the specified resource.
     *
     * @param  EquipmentBoard  $equipmentBoard
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(PortDrawing $portDrawing)
    {
        $document = PortDrawingFile::where('port_drawing_id', $portDrawing->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $document_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = PortDrawingFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     * @param  int  $dock_id
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request, $dock_id)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju ?? '';
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                PortDrawing::whereNull('approval_id')
                    ->where('dermaga_id', $dock_id)
                    ->update($data);
            }

            $portDrawings = PortDrawing::where('approval_id', $approval->id)
                ->pluck('nama_komponen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $portDrawings;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.port-drawing.index', $dock_id)
                ->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('ed.port-drawing.index', $dock_id)
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    public function countPortDrawing($dock_id)
    {
        $countPortDrawing = PortDrawing::whereNull('approval_id')
            ->where('dermaga_id', $dock_id)
            ->orderBy('id', 'DESC')
            ->count();

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Get Total Komponen Sukses'),
            'data' => $countPortDrawing
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Print the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function print($dock_id)
    {
        $dock = Dock::with('pelabuhan')->where('id', $dock_id)->first();

        $portDrawings = PortDrawing::query()->with([
            'documents',
            'approval'
        ])
        ->whereHas('approval', function ($query) {
            $query->whereNotNull('tgl_penyetujuan');
        })
        ->where('dermaga_id', $dock_id)
        ->orderBy('updated_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->get();

        $approverName = $portDrawings[0]->approval->penyetuju->name;
        $approverRole = $portDrawings[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.port.port-drawing.print', compact(
            'dock',
            'portDrawings',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = PortDrawing::where($data)
            ->exists();

        return $isExist;
    }
}
