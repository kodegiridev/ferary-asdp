<?php

namespace App\Http\Controllers\EngineeringDatabase\Port;

use Auth;
use DB;
use Storage;
use Pdf;
use QrCode;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\EngineeringDatabase\Verifikator;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Port\TechnicalQualityStandard;
use App\Models\EngineeringDatabase\Port\TechnicalQualityStandardFile;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Port\StoreTechnicalQualityStandardRequest;
use App\Http\Requests\EngineeringDatabase\Port\UpdateTechnicalQualityStandardRequest;

class TechnicalQualityStandardController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Technical Quality Standard';
        $this->pathFile = 'EngineeringDatabase/Port/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatables();
        }

        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        return view('pages.engineering-database.port.technical-quality-standard.index', compact(
            'approvers',
            'verificators'
        ));
    }

    /**
     * Get count approved technical quality standard
     *
     * @return json
     */
    public function countTechnicalQualityStandard()
    {
        $countData = TechnicalQualityStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->count();

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Get Total Dokumen Sukses'),
            'data' => $countData
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = TechnicalQualityStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-technical-quality-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_technical_quality_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-technical-quality-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#create_technical_quality_standard_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-technical-quality-standard ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTechnicalQualityStandardRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $technicalQualityStandard = TechnicalQualityStandard::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['technical_quality_standard_id'] = $technicalQualityStandard->id;
                $data['file'] = $this->pathFile.$dokumenName;

                TechnicalQualityStandardFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'message' => __('Gagal Tambah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Tambah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Get the specified resource.
     *
     * @param  EquipmentBoard  $equipmentBoard
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(TechnicalQualityStandard $technicalQualityStandard)
    {
        $document = TechnicalQualityStandardFile::where('technical_quality_standard_id', $technicalQualityStandard->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $document_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = TechnicalQualityStandardFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TechnicalQualityStandard $technicalQualityStandard)
    {
        return $technicalQualityStandard;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TechnicalQualityStandard $technicalQualityStandard)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                $result = [
                    'code' => 400,
                    'message' => __('Dokumen Sudah Ada'),
                    'status' => false
                ];
                return response()->json($result, $result['code']);
            }

            $technicalQualityStandard->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = TechnicalQualityStandardFile::where('technical_quality_standard_id', $technicalQualityStandard->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = time().'_'.$dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['technical_quality_standard_id'] = $technicalQualityStandard->id;
                    $data['file'] = $this->pathFile.$dokumenName;

                    TechnicalQualityStandardFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            $result = [
                'code' => 400,
                'message' => __('Gagal Ubah Dokumen'),
                'status' => false
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'message' => __('Berhasil Ubah Dokumen'),
            'status' => true
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TechnicalQualityStandard $technicalQualityStandard)
    {
        DB::beginTransaction();

        try {
            if ($technicalQualityStandard->approval_id) {
                Approval::where('id', $technicalQualityStandard->approval_id)->delete();
            }

            $dokumenExists = TechnicalQualityStandardFile::where('technical_quality_standard_id', $technicalQualityStandard->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $technicalQualityStandard->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $result = [
                'code' => 400,
                'status' => false,
                'message' => __('Gagal Hapus Dokumen!')
            ];
            return response()->json($result, $result['code']);
        }

        $result = [
            'code' => 200,
            'status' => true,
            'message' => __('Berhasil Hapus Dokumen')
        ];
        return response()->json($result, $result['code']);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju ?? '';
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                TechnicalQualityStandard::whereNull('approval_id')
                    ->update($data);
            }

            $technicalQualityStandards = TechnicalQualityStandard::where('approval_id', $approval->id)
                ->pluck('nama_dokumen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $technicalQualityStandards;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.technical-quality-standard.index')
                ->with(['failed' => $e->getMessage()]);
        }

        return redirect()->route('ed.technical-quality-standard.index')
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Display a listing of the resource approved.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentsApproved(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablesApproved();
        }

        return abort(404);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesApproved()
    {
        $data = TechnicalQualityStandard::query()->with([
                'approval',
                'documents'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-technical-quality-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_technical_quality_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-technical-quality-standard"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tanggal_disetujui', function($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Print the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print()
    {
        $technicalQualityStandards = TechnicalQualityStandard::query()->with([
            'documents',
            'approval'
        ])
        ->whereHas('approval', function ($query) {
            $query->whereNotNull('tgl_penyetujuan');
        })
        ->orderBy('updated_at', 'DESC')
        ->orderBy('id', 'DESC')
        ->get();

        $approverName = $technicalQualityStandards[0]->approval->penyetuju->name;
        $approverRole = $technicalQualityStandards[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.port.technical-quality-standard.print', compact(
            'technicalQualityStandards',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

        /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = TechnicalQualityStandard::where($data)
            ->exists();

        return $isExist;
    }
}
