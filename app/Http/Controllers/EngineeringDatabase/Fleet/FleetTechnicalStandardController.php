<?php

namespace App\Http\Controllers\EngineeringDatabase\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\StoreFleetTechnicalStandardRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\UpdateFleetTechnicalStandardRequest;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Fleet\FleetTechnicalStandardFile;
use App\Models\EngineeringDatabase\Fleet\FleetTechnicalStandard;
use App\Models\User;
use App\Models\EngineeringDatabase\Verifikator;
use Auth;
use DB;
use Illuminate\Http\Request;
use Pdf;
use QrCode;
use Session;
use Storage;

class FleetTechnicalStandardController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Fleet Technical Standard';
        $this->pathFile = 'EngineeringDatabase/Fleet/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listTypeStandard = [
            'standard kapal existing',
            'standard kapal baru'
        ];

        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        $countFleetTechnicalStandard = FleetTechnicalStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->count();
        $countFleetTechnicalStandardDetail = FleetTechnicalStandard::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatables();
        }

        return view('pages.engineering-database.fleet.fleet-technical-standard.index', compact(
            'listTypeStandard',
            'approvers',
            'verificators',
            'countFleetTechnicalStandard',
            'countFleetTechnicalStandardDetail'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFleetTechnicalStandardRequest $request)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['tipe_standard'] = $request->tipe_standard;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.fleet-technical-standard.index')
                    ->with(['failed' => 'Standar Teknis Fleet Sudah Ada!']);
            }

            $fleetTechnicalStandard = FleetTechnicalStandard::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = $dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['ed_fleet_technical_standard_id'] = $fleetTechnicalStandard->id;
                $data['file'] = $this->pathFile.$dokumenName;

                FleetTechnicalStandardFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ed.fleet-technical-standard.index')
                ->with(['failed' => 'Gagal Tambah Standar Teknis Fleet!']);
        }

        return redirect()->route('ed.fleet-technical-standard.index')
            ->with(['success' => 'Berhasil Tambah Standar Teknis Fleet!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  FleetTechnicalStandard  $fleetTechnicalStandard
     *
     * @return \Illuminate\Http\Response
     */
    public function show(FleetTechnicalStandard $fleetTechnicalStandard)
    {
        return $fleetTechnicalStandard;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  FleetTechnicalStandard  $fleetTechnicalStandard
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFleetTechnicalStandardRequest $request, FleetTechnicalStandard $fleetTechnicalStandard)
    {
        DB::beginTransaction();
        try {
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['tipe_standard'] = $request->tipe_standard;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.fleet-technical-standard.index')
                    ->with(['failed' => 'Standar Teknis Fleet Sudah Ada!']);
            }

            $fleetTechnicalStandard->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = FleetTechnicalStandardFile::where('ed_fleet_technical_standard_id', $fleetTechnicalStandard->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = $dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['ed_fleet_technical_standard_id'] = $fleetTechnicalStandard->id;
                    $data['jenis_modul'] = $this->moduleName;
                    $data['file'] = $this->pathFile.$dokumenName;

                    FleetTechnicalStandardFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.fleet-technical-standard.index')
                ->with(['failed' => 'Gagal Ubah Standar Teknis Fleet!']);
        }

        return redirect()->route('ed.fleet-technical-standard.index')
            ->with(['success' => 'Berhasil Ubah Standar Teknis Fleet!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  FleetTechnicalStandard  $fleetTechnicalStandard
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(FleetTechnicalStandard $fleetTechnicalStandard)
    {
        DB::beginTransaction();

        try {
            $dokumenExists = FleetTechnicalStandardFile::where('ed_fleet_technical_standard_id', $fleetTechnicalStandard->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $fleetTechnicalStandard->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Standar Teknis Fleet!');
        }

        Session::flash('success', 'Berhasil Hapus Standar Teknis Fleet!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables()
    {
        $data = FleetTechnicalStandard::whereNull('approval_id')
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-fleet-technical-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_fleet_technical_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-fleet-technical-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_fleet_technical_standard_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-fleet-technical-standard ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju;
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                FleetTechnicalStandard::whereNull('approval_id')
                    ->update($data);
            }

            $fleetTechnicalStandards = FleetTechnicalStandard::where('approval_id', $approval->id)
                ->pluck('nama_dokumen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $fleetTechnicalStandards;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.fleet-technical-standard.index')
                ->with(['failed' => 'Gagal Kirim Permintaan!']);
        }

        return redirect()->route('ed.fleet-technical-standard.index')
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Get the specified resource.
     *
     * @param  FleetTechnicalStandard  $fleetTechnicalStandard
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(FleetTechnicalStandard $fleetTechnicalStandard)
    {
        $document = FleetTechnicalStandardFile::where('ed_fleet_technical_standard_id', $fleetTechnicalStandard->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    public function detail()
    {
        $data = FleetTechnicalStandard::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-fleet-technical-standard"
                    data-bs-toggle="modal"
                    data-bs-target="#document_fleet_technical_standard_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-fleet-technical-standard"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tgl_penyetujuan', function ($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Preview the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = FleetTechnicalStandardFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Print the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print()
    {
        $fleetTechnicalStandards = FleetTechnicalStandard::query()->with([
                'dokumen',
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->get();

        $approverName = $fleetTechnicalStandards[0]->approval->penyetuju->name;
        $approverRole = $fleetTechnicalStandards[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.fleet.fleet-technical-standard.print', compact(
            'fleetTechnicalStandards',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = FleetTechnicalStandard::where($data)
            ->exists();

        return $isExist;
    }
}
