<?php

namespace App\Http\Controllers\EngineeringDatabase\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\StoreManualBookRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\UpdateManualBookRequest;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Fleet\ManualBookFile;
use App\Models\EngineeringDatabase\Fleet\ManualBook;
use App\Models\Fleet\Fleet;
use App\Models\User;
use App\Models\EngineeringDatabase\Verifikator;
use Auth;
use DB;
use Illuminate\Http\Request;
use Pdf;
use QrCode;
use Session;
use Storage;

class ManualBookController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Manual Book';
        $this->pathFile = 'EngineeringDatabase/Fleet/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFleet(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablesFleet();
        }

        return view('pages.engineering-database.fleet.manual-book.index-fleet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $vessel_id)
    {
        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        $countManualBook = ManualBook::whereNull('approval_id')
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatables($vessel_id);
        }

        return view('pages.engineering-database.fleet.manual-book.index', compact(
            'vessel_id',
            'approvers',
            'verificators',
            'countManualBook'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreManualBookRequest $request, $vessel_id)
    {
        DB::beginTransaction();
        try {
            $data['vessel_id'] = $vessel_id;
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['komponen'] = $request->komponen;
            $data['merek'] = $request->merek;
            $data['tipe_komponen'] = $request->tipe_komponen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.manual-book.index', $vessel_id)
                    ->with(['failed' => 'Buku Panduan Sudah Ada!']);
            }

            $manualBook = ManualBook::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = $dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['ed_manual_book_id'] = $manualBook->id;
                $data['file'] = $this->pathFile.$dokumenName;

                ManualBookFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ed.manual-book.index', $vessel_id)
                ->with(['failed' => 'Gagal Tambah Buku Panduan!']);
        }

        return redirect()->route('ed.manual-book.index', $vessel_id)
            ->with(['success' => 'Berhasil Tambah Buku Panduan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  ManualBook  $manualBook
     *
     * @return \Illuminate\Http\Response
     */
    public function show($vessel_id, ManualBook $manualBook)
    {
        return $manualBook;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ManualBook  $manualBook
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateManualBookRequest $request, $vessel_id, ManualBook $manualBook)
    {
        DB::beginTransaction();
        try {
            $data['vessel_id'] = $vessel_id;
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['komponen'] = $request->komponen;
            $data['merek'] = $request->merek;
            $data['tipe_komponen'] = $request->tipe_komponen;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.manual-book.index', $vessel_id)
                    ->with(['failed' => 'Buku Panduan Sudah Ada!']);
            }

            $manualBook->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = ManualBookFile::where('ed_manual_book_id', $manualBook->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = $dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['ed_manual_book_id'] = $manualBook->id;
                    $data['jenis_modul'] = $this->moduleName;
                    $data['file'] = $this->pathFile.$dokumenName;

                    ManualBookFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.manual-book.index', $vessel_id)
                ->with(['failed' => 'Gagal Ubah Buku Panduan!']);
        }

        return redirect()->route('ed.manual-book.index', $vessel_id)
            ->with(['success' => 'Berhasil Ubah Buku Panduan!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ManualBook  $manualBook
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($vessel_id, ManualBook $manualBook)
    {
        DB::beginTransaction();

        try {
            $dokumenExists = ManualBookFile::where('ed_manual_book_id', $manualBook->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $manualBook->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Buku Panduan!');
        }

        Session::flash('success', 'Berhasil Hapus Buku Panduan!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesFleet()
    {
        $data = Fleet::with(['regional', 'cabang', 'lintasan'])
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('regional_id', function ($data) {
                return $data->regional->nama;
            })
            ->editColumn('cabang_id', function ($data) {
                return $data->cabang->nama;
            })
            ->editColumn('lintasan_id', function ($data) {
                return $data->lintasan->nama;
            })
            ->addColumn('action', function ($data) {
                $button = '<a
                    class="btn btn-sm btn-info"
                    href="'.route('ed.manual-book.detailFleet', $data->id).'">
                        '.__('Detail').'
                    </a>';
                $button .= '<a
                    class="btn btn-sm btn-primary ms-2"
                    href="'.route('ed.manual-book.index', $data->id).'">
                        '.__('Tambah').'
                    </a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables($vessel_id)
    {
        $data = ManualBook::whereNull('approval_id')
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-manual-book"
                    data-bs-toggle="modal"
                    data-bs-target="#document_manual_book_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-manual-book"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_manual_book_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-manual-book ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request, $vessel_id)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju;
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                ManualBook::whereNull('approval_id')
                    ->where('vessel_id', $vessel_id)
                    ->update($data);
            }

            $manualBooks = ManualBook::where('approval_id', $approval->id)
                ->pluck('nama_dokumen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $manualBooks;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.manual-book.index', $vessel_id)
                ->with(['failed' => 'Gagal Kirim Permintaan!']);
        }

        return redirect()->route('ed.manual-book.index', $vessel_id)
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Get the specified resource.
     *
     * @param  ManualBook  $manualBook
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(ManualBook $manualBook)
    {
        $document = ManualBookFile::where('ed_manual_book_id', $manualBook->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailFleet(Request $request, $vessel_id)
    {
        $countManualBook = ManualBook::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatablesFleetDetail($vessel_id);
        }

        return view('pages.engineering-database.fleet.manual-book.detail-fleet', compact(
            'vessel_id',
            'countManualBook'
        ));
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesFleetDetail($vessel_id)
    {
        $data = ManualBook::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-manual-book"
                    data-bs-toggle="modal"
                    data-bs-target="#document_manual_book_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-manual-book"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tgl_penyetujuan', function ($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = ManualBookFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Print the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function print($vessel_id)
    {
        $fleet = Fleet::find($vessel_id);

        $manualBooks = ManualBook::query()->with([
                'dokumen',
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->get();

        $approverName = $manualBooks[0]->approval->penyetuju->name;
        $approverRole = $manualBooks[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.fleet.manual-book.print', compact(
            'fleet',
            'manualBooks',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = ManualBook::where($data)
            ->exists();

        return $isExist;
    }
}
