<?php

namespace App\Http\Controllers\EngineeringDatabase\Fleet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Requests\EngineeringDatabase\StoreApprovalRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\StoreShipDocumentRequest;
use App\Http\Requests\EngineeringDatabase\Fleet\UpdateShipDocumentRequest;
use App\Models\EngineeringDatabase\Approval;
use App\Models\EngineeringDatabase\Fleet\ShipDocumentFile;
use App\Models\EngineeringDatabase\Fleet\ShipDocument;
use App\Models\Fleet\Fleet;
use App\Models\User;
use App\Models\EngineeringDatabase\Verifikator;
use Auth;
use DB;
use Illuminate\Http\Request;
use Pdf;
use QrCode;
use Session;
use Storage;

class ShipDocumentController extends Controller
{
    private $moduleParent;
    private $moduleName;
    private $pathFile;
    private $notification;
    private $viewMail;

    public function __construct() {
        $this->moduleParent = 'Engineering Database';
        $this->moduleName = 'Ship Document';
        $this->pathFile = 'EngineeringDatabase/Fleet/'.$this->moduleName.'/';
        $this->notification = new NotificationController();
        $this->viewMail = 'pages.engineering-database.mail';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexFleet(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatablesFleet();
        }

        return view('pages.engineering-database.fleet.ship-document.index-fleet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $vessel_id)
    {
        $approvers = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Manager%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();
        $verificators = User::query()->with([
                'role'
            ])
            ->whereHas('role', function ($query) {
                $query->where('name', 'LIKE', 'Manager%')
                    ->orWhere('name', 'LIKE', 'VP%')
                    ->orWhere('name', 'LIKE', 'Vice President%')
                    ->orWhere('name', 'LIKE', 'Staf%');
            })
            ->whereNot('id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        $countShipDocument = ShipDocument::whereNull('approval_id')
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatables($vessel_id);
        }

        return view('pages.engineering-database.fleet.ship-document.index', compact(
            'vessel_id',
            'approvers',
            'verificators',
            'countShipDocument'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShipDocumentRequest $request, $vessel_id)
    {
        DB::beginTransaction();
        try {
            $data['vessel_id'] = $vessel_id;
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['nomor_dokumen'] = $request->nomor_dokumen;
            $data['tgl_dokumen'] = $request->tgl_dokumen;
            $data['instansi'] = $request->instansi;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.ship-document.index', $vessel_id)
                    ->with(['failed' => 'Dokumen Kapal Sudah Ada!']);
            }

            $shipDocument = ShipDocument::create($data);

            foreach ($request->file('dokumen') as $dokumen) {
                $dokumenName = $dokumen->getClientOriginalName();
                $dokumenContent = file_get_contents($dokumen);
                Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                $data = [];
                $data['ed_ship_document_id'] = $shipDocument->id;
                $data['file'] = $this->pathFile.$dokumenName;

                ShipDocumentFile::create($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('ed.ship-document.index', $vessel_id)
                ->with(['failed' => 'Gagal Tambah Dokumen Kapal!']);
        }

        return redirect()->route('ed.ship-document.index', $vessel_id)
            ->with(['success' => 'Berhasil Tambah Dokumen Kapal!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  ShipDocument  $shipDocument
     *
     * @return \Illuminate\Http\Response
     */
    public function show($vessel_id, ShipDocument $shipDocument)
    {
        return $shipDocument;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ShipDocument  $shipDocument
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShipDocumentRequest $request, $vessel_id, ShipDocument $shipDocument)
    {
        DB::beginTransaction();
        try {
            $data['vessel_id'] = $vessel_id;
            $data['nama_dokumen'] = $request->nama_dokumen;
            $data['nomor_dokumen'] = $request->nomor_dokumen;
            $data['tgl_dokumen'] = $request->tgl_dokumen;
            $data['instansi'] = $request->instansi;
            $data['keterangan'] = $request->keterangan;

            if ($this->isExist($data)) {
                return redirect()->route('ed.ship-document.index', $vessel_id)
                    ->with(['failed' => 'Dokumen Kapal Sudah Ada!']);
            }

            $shipDocument->update($data);

            if ($request->hasFile('dokumen')) {
                $dokumenExists = ShipDocumentFile::where('ed_ship_document_id', $shipDocument->id)
                    ->get();

                foreach ($dokumenExists as $dokumenExist) {
                    Storage::disk('public')->delete($dokumenExist->file);
                    $dokumenExist->delete();
                }

                foreach ($request->file('dokumen') as $dokumen) {
                    $dokumenName = $dokumen->getClientOriginalName();
                    $dokumenContent = file_get_contents($dokumen);
                    Storage::disk('public')->put($this->pathFile.$dokumenName, $dokumenContent);

                    $data = [];
                    $data['ed_ship_document_id'] = $shipDocument->id;
                    $data['jenis_modul'] = $this->moduleName;
                    $data['file'] = $this->pathFile.$dokumenName;

                    ShipDocumentFile::create($data);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.ship-document.index', $vessel_id)
                ->with(['failed' => 'Gagal Ubah Dokumen Kapal!']);
        }

        return redirect()->route('ed.ship-document.index', $vessel_id)
            ->with(['success' => 'Berhasil Ubah Dokumen Kapal!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ShipDocument  $shipDocument
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($vessel_id, ShipDocument $shipDocument)
    {
        DB::beginTransaction();

        try {
            $dokumenExists = ShipDocumentFile::where('ed_ship_document_id', $shipDocument->id)
                ->get();

            foreach ($dokumenExists as $dokumenExist) {
                Storage::disk('public')->delete($dokumenExist->file);
                $dokumenExist->delete();
            }

            $shipDocument->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            Session::flash('failed', 'Gagal Hapus Dokumen Kapal!');
        }

        Session::flash('success', 'Berhasil Hapus Dokumen Kapal!');
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesFleet()
    {
        $data = Fleet::with(['regional', 'cabang', 'lintasan'])
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('regional_id', function ($data) {
                return $data->regional->nama;
            })
            ->editColumn('cabang_id', function ($data) {
                return $data->cabang->nama;
            })
            ->editColumn('lintasan_id', function ($data) {
                return $data->lintasan->nama;
            })
            ->addColumn('action', function ($data) {
                $button = '<a
                    class="btn btn-sm btn-info"
                    href="'.route('ed.ship-document.detailFleet', $data->id).'">
                        '.__('Detail').'
                    </a>';
                $button .= '<a
                    class="btn btn-sm btn-primary ms-2"
                    href="'.route('ed.ship-document.index', $data->id).'">
                        '.__('Tambah').'
                    </a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables($vessel_id)
    {
        $data = ShipDocument::whereNull('approval_id')
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-secondary btn-document-ship-document"
                    data-bs-toggle="modal"
                    data-bs-target="#document_ship_document_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </button>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-success btn-edit-ship-document"
                    data-bs-toggle="modal"
                    data-bs-target="#edit_ship_document_modal"
                    data-id='.$data->id.'>
                        '.__('Ubah').'
                    </button>';
                $button .= '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-ship-document ms-2"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Send the specified resource to Approval.
     *
     * @param  \Illuminate\Http\StoreApprovalRequest  $request
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function sendApproval(StoreApprovalRequest $request, $vessel_id)
    {
        DB::beginTransaction();

        try {
            $data['pengaju_id'] = Auth::user()->id;
            $data['penyetuju_id'] = $request->penyetuju_id;
            $data['jenis_modul'] = $this->moduleParent;
            $data['sub_modul'] = $this->moduleName;
            $data['tgl_pengajuan'] = date("Y-m-d");
            $data['keterangan_pengaju'] = $request->keterangan_pengaju;
            $data['status'] = 0;

            $approval = Approval::create($data);

            if ($approval) {
                foreach ($request->user_verifikator_id as $verifikator) {
                    $data = [];
                    $data['approval_id'] = $approval->id;
                    $data['user_verifikator_id'] = $verifikator;
                    $data['status'] = 0;

                    Verifikator::create($data);
                }

                $data = [];
                $data['approval_id'] = $approval->id;

                ShipDocument::whereNull('approval_id')
                    ->where('vessel_id', $vessel_id)
                    ->update($data);
            }

            $shipDocuments = ShipDocument::where('approval_id', $approval->id)
                ->pluck('nama_dokumen');

            $param = [];
            $param['users'] = $request->user_verifikator_id;
            $param['title'] = $this->moduleName;
            $param['data'] = __('app.notification.engineering_database');
            $param['action'] = route('approval.status.detail', encryptor($approval->id));
            $param['view'] = $this->viewMail;
            $param['subject'] = __('app.mail.engineering_database.subject');
            $param['documents'] = $shipDocuments;
            $param['applicant'] = $approval->pengaju;
            $this->notification->send($param);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->route('ed.ship-document.index', $vessel_id)
                ->with(['failed' => 'Gagal Kirim Permintaan!']);
        }

        return redirect()->route('ed.ship-document.index', $vessel_id)
            ->with(['success' => 'Berhasil Kirim Permintaan!']);
    }

    /**
     * Get the specified resource.
     *
     * @param  ShipDocument  $shipDocument
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocument(ShipDocument $shipDocument)
    {
        $document = ShipDocumentFile::where('ed_ship_document_id', $shipDocument->id)
            ->orderBy('id', 'DESC')
            ->get();

        return $document ?? [];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailFleet(Request $request, $vessel_id)
    {
        $countShipDocument = ShipDocument::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->count();

        if ($request->ajax()) {
            return $this->datatablesFleetDetail($vessel_id);
        }

        return view('pages.engineering-database.fleet.ship-document.detail-fleet', compact(
            'vessel_id',
            'countShipDocument'
        ));
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatablesFleetDetail($vessel_id)
    {
        $data = ShipDocument::query()->with([
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('id', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('document', function ($data) {
                $button = '<a type="button"
                    class="btn btn-sm btn-secondary btn-document-ship-document"
                    data-bs-toggle="modal"
                    data-bs-target="#document_ship_document_modal"
                    data-id='.$data->id.'>
                        <i class="fa fa-file"></i>
                    </a>';
                return $button;
            })
            ->addColumn('action', function ($data) {
                $button = '<button type="button"
                    class="btn btn-sm btn-danger btn-delete-ship-document"
                    data-id='.$data->id.'>
                        '.__('Hapus').'
                    </button>';
                return $button;
            })
            ->addColumn('tgl_penyetujuan', function ($data) {
                return $data->approval->tgl_penyetujuan;
            })
            ->rawColumns(['document', 'action'])
            ->make(true);
    }

    /**
     * Preview the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDocument($document_id)
    {
        $document = ShipDocumentFile::find($document_id);

        if (Storage::disk('public')->exists($document->file)) {
            return response()->file(Storage::disk('public')->path('') . $document->file);
        }

        return abort(404);
    }

    /**
     * Print the specified resource.
     *
     * @param  int  $vessel_id
     *
     * @return \Illuminate\Http\Response
     */
    public function print($vessel_id)
    {
        $fleet = Fleet::find($vessel_id);

        $shipDocuments = ShipDocument::query()->with([
                'dokumen',
                'approval'
            ])
            ->whereHas('approval', function ($query) {
                $query->whereNotNull('tgl_penyetujuan');
            })
            ->where('vessel_id', $vessel_id)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->get();

        $approverName = $shipDocuments[0]->approval->penyetuju->name;
        $approverRole = $shipDocuments[0]->approval->penyetuju->role->name;
        $qrcodeMessage = 'Ditandatangani secara digital oleh '.$approverName.' sebagai '.$approverRole;

        $qrcode = base64_encode(QrCode::errorCorrection('H')->size(100)->generate($qrcodeMessage));
        $pdf = Pdf::loadView('pages.engineering-database.fleet.ship-document.print', compact(
            'fleet',
            'shipDocuments',
            'qrcode'
        ))->setPaper("A4", "landscape");

        return $pdf->stream();
    }

    /**
     * Check duplicate data resource.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\Response
     */
    public function isExist($data)
    {
        $isExist = ShipDocument::where($data)
            ->exists();

        return $isExist;
    }
}
