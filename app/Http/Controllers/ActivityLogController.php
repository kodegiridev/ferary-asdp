<?php

namespace App\Http\Controllers;

use App\Models\ActivityLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::orderBy('id', 'DESC')
            ->get();

        if ($request->ajax()) {
            return $this->datatables($request);
        }

        return view('pages.activity-log.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  activityLog  $activityLog
     *
     * @return \Illuminate\Http\Response
     */
    public function show(activityLog $activityLog)
    {
        return view('pages.activity-log.show', compact('activityLog'));
    }

    /**
     * Display data in datatables
     *
     * @return void
     */
    protected function datatables($request)
    {
        $data = ActivityLog::query()->with(['user']);

        if ($request->description) {
            $data->where('description', 'LIKE', '%'.$request->description.'%');
        }

        if ($request->causer_id) {
            $causer_id = $request->causer_id;
            $data->whereHas('user', function($query) use($causer_id) {
                $query->where('id', $causer_id);
            });
        }

        if ($request->start_date) {
            $data->whereDate('created_at', '>=', $request->start_date);
        }

        if ($request->end_date) {
            $data->whereDate('created_at', '<=', $request->end_date);
        }

        if ($request->start_date && $request->end_date) {
            $data->whereBetween('created_at', [$request->start_date, $request->end_date]);
        }

        $data = $data->orderBy('created_at', 'DESC')
            ->get();

        return datatables()
            ->of($data)
            ->addIndexColumn()
            ->editColumn('causer_id', function ($data) {
                return $data->user->name;
            })
            ->editColumn('created_at', function ($data) {
                return Carbon::parse($data->created_at)
                    ->locale('id')
                    ->translatedFormat('d F Y H:i:s');
            })
            ->addColumn('action', function ($data) {
                $button = '<a class="btn btn-sm btn-info"
                href='.route('activity-log.show', $data->id).'>
                    <i class="fa fa-magnifying-glass"></i>
                </a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
