<?php

namespace App\Http\Controllers;

use App\Mail\FeraryMail;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\FeraryNotification;
use Auth;
use DB;
use Mail;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = 50;

        $notifications = Notification::where('notifiable_id', Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->limit($limit)
            ->get();

        $unreadNotification = count(Notification::where('notifiable_id', Auth::user()->id)
            ->whereNull('read_at')
            ->limit($limit)
            ->get());

        $data = $notifications->map(function ($notification) {
            $createdAt = $notification->created_at->diffForHumans();
            return [
                'id' => $notification->id,
                'notifiable_id' => $notification->notifiable_id,
                'title' => $notification->title,
                'data' => str_replace('"', '', $notification->data),
                'action' => $notification->action,
                'created_at' => $createdAt,
                'read_at' => $notification->read_at,
            ];
        });

        $response = [
            'data' => $data,
            'unread_notification' => $unreadNotification
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $notification = Notification::findOrFail($id);

        if (!$notification) {
            $response = [
                'status' => 'error'
            ];
            return response()->json($response);
        }

        if (!$notification->read_at) {
            DB::beginTransaction();
            try {
                $data['read_at'] = date('Y-m-d H:i:s');
                $notification->update($data);

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();

                $response = [
                    'status' => 'error'
                ];
                return response()->json($response);
            }
        }

        $response = [
            'status' => 'success'
        ];
        return response()->json($response);
    }

    /**
     * Send notification to user.
     */
    public function send($param)
    {
        $users = User::whereIn('id', $param['users'])
            ->get();

        foreach ($users as $user) {
            $param['receiver'] = $user;
            Mail::to($user->email)->send(new FeraryMail($param));
            $user->notify(new FeraryNotification($param));
        }
    }
}
