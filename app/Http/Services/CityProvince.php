<?php

namespace App\Http\Services;

class CityProvince {
    public static function get_province() {
        return \Indonesia::allProvinces();
    }

    public static function get_cities($province_id) {
        return \Indonesia::findProvince($province_id, ['cities'])->cities->pluck('name', 'id');
    }
}