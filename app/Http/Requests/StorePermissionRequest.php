<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:permissions',
            'menu_name' => 'required',
            'action_permission' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama permission harus diisi.',
            'name.unique' => 'Nama permission sudah ada.',
            'menu_name.required' => 'Nama menu harus diisi.',
            'action_permission.required' => 'Aksi harus diisi.'
        ];
    }
}
