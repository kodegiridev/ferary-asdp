<?php

namespace App\Http\Requests\EngineeringDatabase;

use Illuminate\Foundation\Http\FormRequest;

class StoreApprovalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'penyetuju_id' => 'required',
            'user_verifikator_id.*' => 'required|distinct|different:penyetuju_id'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'penyetuju_id.required' => 'Penyetuju harus diisi.',
            'user_verifikator_id.*.required' => 'Verifikator harus diisi.',
            'user_verifikator_id.*.distinct' => 'Verifikator tidak boleh sama.',
            'user_verifikator_id.*.different' => 'Verifikator tidak boleh sama dengan penyetuju.'
        ];
    }
}
