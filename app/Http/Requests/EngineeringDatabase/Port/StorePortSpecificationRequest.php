<?php

namespace App\Http\Requests\EngineeringDatabase\Port;

use Illuminate\Foundation\Http\FormRequest;

class StorePortSpecificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama_komponen' => 'required',
            'sub_komponen' => 'required',
            'parameter' => 'required',
            'spesifikasi' => 'required',
            'dokumen' => 'required',
            'dokumen.*' => 'mimes:pdf|max:2048'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama_komponen.required' => 'Nama komponen harus diisi.',
            'sub_komponen.required' => 'Sub komponen harus diisi.',
            'parameter.required' => 'Parameter harus diisi.',
            'spesifikasi.required' => 'Spesifikasi harus diisi.',
            'dokumen.required' => 'Dokumen harus diisi.',
            'dokumen.*.mimes' => 'Dokumen harus berupa pdf.',
            'dokumen.*.max' => 'Dokumen harus kurang dari 2mb.'
        ];
    }
}
