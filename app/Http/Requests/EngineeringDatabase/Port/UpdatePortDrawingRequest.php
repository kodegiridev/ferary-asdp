<?php

namespace App\Http\Requests\EngineeringDatabase\Port;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePortDrawingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama_komponen' => 'required',
            'nama_gambar' => 'required',
            'nomor_gambar' => 'required',
            'tipe_gambar' => 'required',
            'status_gambar' => 'required',
            'dokumen.*' => 'mimes:pdf|max:2048'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama_komponen.required' => 'Nama komponen harus diisi.',
            'nama_gambar.required' => 'Nama gambar harus diisi.',
            'nomor_gambar.required' => 'Nomor gambar harus diisi.',
            'tipe_gambar.required' => 'Tipe gambar harus diisi.',
            'status_gambar.required' => 'Status gambar harus diisi',
            'dokumen.*.mimes' => 'Dokumen harus berupa pdf.',
            'dokumen.*.max' => 'Dokumen harus kurang dari 2mb.'
        ];
    }
}
