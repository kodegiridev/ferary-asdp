<?php

namespace App\Http\Requests\EngineeringDatabase\Port;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePortSpecificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama_komponen' => 'required',
            'sub_komponen' => 'required',
            'parameter' => 'required',
            'spesifikasi' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama_komponen.required' => 'Nama komponen harus diisi.',
            'sub_komponen.required' => 'Sub komponen harus diisi.',
            'parameter.required' => 'Parameter harus diisi.',
            'spesifikasi.required' => 'Spesifikasi harus diisi.'
        ];
    }
}
