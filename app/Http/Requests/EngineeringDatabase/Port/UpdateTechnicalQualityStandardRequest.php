<?php

namespace App\Http\Requests\EngineeringDatabase\Port;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTechnicalQualityStandardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama_dokumen' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama_dokumen.required' => 'Nama dokumen harus diisi.',
        ];
    }
}
