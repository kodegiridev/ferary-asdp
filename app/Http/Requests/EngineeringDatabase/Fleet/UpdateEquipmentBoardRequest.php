<?php

namespace App\Http\Requests\EngineeringDatabase\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEquipmentBoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama_peralatan' => 'required',
            'merek' => 'required',
            'quantity' => 'required',
            'tipe_sertifikat' => 'required',
            'keterangan' => 'required',
            'dokumen.*' => 'mimes:pdf|max:2048'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama_peralatan.required' => 'Nama peralatan harus diisi.',
            'merek.required' => 'Merek harus diisi.',
            'quantity.required' => 'Kuantitas harus diisi.',
            'tipe_sertifikat.required' => 'Tipe sertifikat harus diisi.',
            'dokumen.*.mimes' => 'Dokumen harus berupa pdf.',
            'dokumen.*.max' => 'Dokumen harus kurang dari 2mb.'
        ];
    }
}
