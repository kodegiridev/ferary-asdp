<?php

namespace App\Http\Requests\MasterData\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class StoreTypeOfTrackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required|unique:mst_tipe_lintasan'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi.',
            'nama.unique' => 'Tipe lintasan sudah terdaftar.'
        ];
    }
}
