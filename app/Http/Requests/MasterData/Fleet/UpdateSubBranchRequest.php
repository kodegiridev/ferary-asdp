<?php

namespace App\Http\Requests\MasterData\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'cabang_id' => 'required',
            'kota_id' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cabang_id.required' => 'Cabang harus diisi.',
            'kota_id.required' => 'Kota harus diisi.'
        ];
    }
}
