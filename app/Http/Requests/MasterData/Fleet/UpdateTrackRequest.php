<?php

namespace App\Http\Requests\MasterData\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTrackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'rute_id' => 'required',
            'tipe_lintasan_id' => 'required',
            'pelabuhan_id' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi.',
            'rute_id.required' => 'Rute harus diisi.',
            'tipe_lintasan_id.required' => 'Tipe lintasan harus diisi.',
            'pelabuhan_id.required' => 'Pelabuhan harus diisi.'
        ];
    }
}
