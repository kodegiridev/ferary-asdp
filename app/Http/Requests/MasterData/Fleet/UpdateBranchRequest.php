<?php

namespace App\Http\Requests\MasterData\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'regional_id' => 'required',
            'nama' => 'required',
            'cabang_kelas' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'regional_id.required' => 'Regional harus diisi.',
            'nama.required' => 'Nama harus diisi.',
            'cabang_kelas.required' => 'Kelas cabang harus diisi.'
        ];
    }
}
