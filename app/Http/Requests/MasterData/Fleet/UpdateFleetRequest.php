<?php

namespace App\Http\Requests\MasterData\Fleet;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFleetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'regional_id' => 'required',
            'cabang_id' => 'required',
            'lintasan_id' => 'required',
            'grt' => 'required',
            'panjang' => 'required',
            'daya' => 'required',
            'satuan' => 'required',
            'usia_teknis' => 'required',
            'status_survey' => 'required',
            'pic' => 'required',
            'os' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi.',
            'regional_id.required' => 'Regional harus diisi.',
            'cabang_id.required' => 'Cabang harus diisi.',
            'lintasan_id.required' => 'Lintasan harus diisi.',
            'grt.required' => 'GRT harus diisi.',
            'panjang.required' => 'Panjang harus diisi.',
            'daya.required' => 'Daya harus diisi.',
            'satuan.required' => 'Satuan harus diisi.',
            'usia_teknis.required' => 'Usia teknis harus diisi.',
            'status_survey.required' => 'Status survei harus diisi.',
            'pic.required' => 'PIC harus diisi.',
            'os.required' => 'Pemilik survei harus diisi.',
        ];
    }
}
