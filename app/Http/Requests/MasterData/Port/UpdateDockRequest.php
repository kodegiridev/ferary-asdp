<?php

namespace App\Http\Requests\MasterData\Port;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'pelabuhan_id' => 'required',
            'tipe' => 'required',
            'kapasitas' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi.',
            'pelabuhan_id.required' => 'Pelabuhan harus diisi.',
            'tipe.required' => 'Tipe harus diisi.',
            'kapasitas.required' => 'Kapasitas harus diisi.'
        ];
    }
}
