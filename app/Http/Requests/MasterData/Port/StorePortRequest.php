<?php

namespace App\Http\Requests\MasterData\Port;

use Illuminate\Foundation\Http\FormRequest;

class StorePortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'regional_id' => 'required',
            'cabang_id' => 'required',
            'pic' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Nama harus diisi.',
            'regional_id.required' => 'Regional harus diisi.',
            'cabang_id.required' => 'Cabang harus diisi.',
            'pic.required' => 'PIC harus diisi.'
        ];
    }
}
