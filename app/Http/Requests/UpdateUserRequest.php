<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'username' => 'required|unique:users,username,'.$this->user->id,
            'email' => 'required|email|unique:users,email,'.$this->user->id,
            'password' => 'nullable',
            'role_id' => 'required',
            'is_active' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi.',
            'username.required' => 'Username harus diisi.',
            'username.unique' => 'Username sudah ada.',
            'email.required' => 'Email harus diisi.',
            'email.unique' => 'Email sudah ada.',
            'role_id.required' => 'Role harus diisi.',
            'is_active.required' => 'Status harus diisi.'
        ];
    }
}
