<?php

namespace App\Http\Requests\ProjectMonitoring\Fleet\Progres;

use Illuminate\Foundation\Http\FormRequest;

class StoreProgresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'status_kontrak' => 'required',
            'tgl_update' => 'required',
            'actual_cost' => 'required',
            'remarks' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'project_id.required' => 'Id Proyek harus diisi.',
            'status_kontrak.required' => 'Status Kontrak harus diisi.',
            'tgl_update.required' => 'Tanggal update harus diisi.',
            'actual_cost.required' => 'Actual Cost harus diisi.',
            'remarks.required' => 'Remarks harus diisi.'
        ];
    }
}
