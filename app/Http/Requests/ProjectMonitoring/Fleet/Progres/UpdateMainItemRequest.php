<?php

namespace App\Http\Requests\ProjectMonitoring\Fleet\Progres;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMainItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'main_item' => 'required',
            'batas_bobot' => 'required'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'main_item.required' => 'Main Item update harus diisi.',
            'batas_bobot.required' => 'Batas Bobot harus diisi.'
        ];
    }
}
