<?php

namespace App\Http\Requests\ProjectMonitoring\Fleet\Document;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'judul_dokumen' => 'required',
            'status_pemenuhan' => 'required|in:0,1',
            'nomor_dokumen' => 'nullable',
            'remarks' => 'nullable',
            'file' => 'nullable|file|mimes:pdf'
        ];
    }
}
