<?php

namespace App\Http\Requests\ProjectMonitoring\Fleet\Document;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'jenis_dokumen' => 'required|in:Project Basic,Protocol Document,Engineering Document,Quality Document,Progress Document',
            'sub_dokumen.*' => 'nullable|string',
            'dokumen_tambahan.*.nama_dokumen' => 'nullable|string'
        ];
    }

    /**
     * The validation error messages that apply to the request.
     *
     * @return array<string>
     */
    public function messages()
    {
        return [
            'jenis_dokumen.required' => 'Main subject harus dipilih.',
            'jenis_dokumen.in' => 'Main subject yang dipilih tidak valid.',
        ];
    }
}
