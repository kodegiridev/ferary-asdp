<?php

namespace App\Http\Requests\ProjectMonitoring\Port\Document;

use Illuminate\Foundation\Http\FormRequest;

class StoreSubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'judul_dokumen' => 'required',
            'status_pemenuhan' => 'required|in:0,1',
            'nomor_dokumen' => 'nullable|required_if:status_pemenuhan,1',
            'remarks' => 'nullable',
            'file' => 'nullable|file|mimes:pdf|required_if:status_pemenuhan,1'
        ];
    }
}
