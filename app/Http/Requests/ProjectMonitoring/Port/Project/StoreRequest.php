<?php

namespace App\Http\Requests\ProjectMonitoring\Port\Project;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'nilai_project' => (int) str_replace(',', '', str_replace('Rp. ', '', $this->nilai_project)),
            'pic' => (int) $this->pic,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'kontraktor' => 'required',
            'nama_project' => 'required',
            'nilai_project' => 'required|gt:0',
            'jenis_periode' => 'required|in:hari,minggu,bulan',
            'nomor_kontrak' => 'nullable',
            'supervisor' => 'nullable',
            'pic' => 'required|exists:users,id',
            'jenis_project' => 'nullable'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'kontraktor.required' => 'Kontraktor harus diisi.',
            'nama_project.required' => 'Project harus diisi.',
            'nilai_project.required' => 'Nilai project harus diisi.',
            'jenis_periode.required' => 'Jenis periode harus diisi.',
            'jenis_periode.in' => 'Jenis periode hanya boleh: hari, minggu, bulan.',
            'pic.required' => 'PIC harus diisi.',
            'pic.exists' => 'PIC tidak terdaftar.'
        ];
    }
}
