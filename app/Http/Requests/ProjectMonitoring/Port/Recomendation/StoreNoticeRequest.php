<?php

namespace App\Http\Requests\ProjectMonitoring\Port\Recomendation;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'pic' => (int) $this->pic,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'notice' => 'required',
            'to_do_list' => 'required',
            'due_date' => 'required',
            'status' => 'required|in:open,close',
            'pic' => 'required|exists:users,id',
            'file' => 'nullable|file|mimes:pdf'
        ];
    }

    /**
     * Get the validation error messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'notice.required' => 'Notice harus diisi.',
            'to_do_list.required' => 'To do list harus diisi.',
            'due_date.required' => 'Due date harus diisi.',
            'status.required' => 'Status harus diisi.',
            'status.in' => 'Status hanya boleh: open, close.',
            'pic.required' => 'PIC harus diisi.',
            'pic.exists' => 'PIC tidak terdaftar.',
            'file.mimes' => 'File yang didukung: .pdf '
        ];
    }
}
