<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class CustomDatabaseChannel
{
    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toDatabase($notifiable);

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            'type' => get_class($notification),
            'title' => $data['title'],
            'data' => $data['data'],
            'action' => $data['action'],
            'read_at' => null,
        ]);
    }
}
