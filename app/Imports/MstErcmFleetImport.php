<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class MstErcmFleetImport implements ToModel, WithStartRow
{
    public function model(array $row)
    {
    }

    public function startRow(): int
    {
        return 2;
    }
}
