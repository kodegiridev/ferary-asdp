<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeraryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $param;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->param['subject'])
        ->view($this->param['view'])
        ->from(env("MAIL_FROM_ADDRESS"));
    }
}
