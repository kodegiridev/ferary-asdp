<?php
namespace App\Enums;

enum ERCMStatusEnum: int {
    case ON_PROGRESS = 1;
    case ON_GOING = 2;

    public function getLabelText(): string {
        return match ($this) {
            self::ON_PROGRESS => 'On progress',
            self::ON_GOING => 'On going',
        };
    }

    public static function toArray(): array {
        return [
            [
                'id' => static::ON_PROGRESS,
                'name' => 'On progress',
            ],
            [
                'id' => static::ON_GOING,
                'name' => 'On going',
            ]
        ];
    }
}
