<?php
namespace App\Enums;

enum ERCMPeriodStatusEnum: int {
    case NO_PROGRESS = 1;
    case ON_PROGRESS = 2;
    case WAITING_APPROVAL = 3;
    case COMPLETION = 4;

    public function getLabelText(): string {
        return match ($this) {
            self::NO_PROGRESS => 'No progress',
            self::ON_PROGRESS => 'On progress',
            self::WAITING_APPROVAL => 'Waiting For Approval',
            self::COMPLETION => 'Completion',
        };
    }

    public static function toArray(): array {
        return [
            [
                'id' => static::NO_PROGRESS,
                'name' => 'On progress',
            ],
            [
                'id' => static::ON_PROGRESS,
                'name' => 'On going',
            ],
            [
                'id' => static::WAITING_APPROVAL,
                'name' => 'Waiting For Approval',
            ],
            [
                'id' => static::COMPLETION,
                'name' => 'Completion',
            ]
        ];
    }
}
