<?php

namespace App\Models\MasterRegion;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class BranchSub extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_cabang_sub';
    protected $guarded = [
        'id'
    ];
    protected $fillable = ['cabang_id', 'kota_id'];
    public $timestamps = false;

    protected $moduleName = 'Sub Branch';

    public function cabang()
    {
        return $this->belongsTo(Branch::class, 'cabang_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
