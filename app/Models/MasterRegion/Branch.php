<?php

namespace App\Models\MasterRegion;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Branch extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_cabang';
    protected $guarded = [
        'id'
    ];
    protected $fillable = ['regional_id', 'nama', 'cabang_kelas'];
    public $timestamps = false;

    protected $moduleName = 'Branch';

    public function regional()
    {
        return $this->belongsTo(Region::class, 'regional_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
