<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class DokumenFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'dokumen_ipm_fleet';

    protected $fillable = [
        'sub_dokumen_id',
        'judul_dokumen',
        'status_pemenuhan',
        'remarks',
        'nomor_dokumen',
        'file',
    ];

    protected $moduleName = 'Dokumen Fleet';

    public function scopeStatusYes($query)
    {
        return $query->where('status_pemenuhan', 1);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
