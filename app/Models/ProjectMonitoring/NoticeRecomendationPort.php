<?php

namespace App\Models\ProjectMonitoring;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class NoticeRecomendationPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'data_notice_recomendation_port';

    protected $fillable = [
        'periode_id',
        'notice',
        'to_do_list',
        'due_date',
        'status',
        'pic',
        'file',
    ];

    protected $moduleName = 'Notice Recommendation Port';

    public function user()
    {
        return $this->belongsTo(User::class, 'pic');
    }

    public function periode_recomendation()
    {
        return $this->belongsTo(PeriodeRecomendationPort::class, 'periode_id');
    }

    public function scopeCountStatus($query)
    {
        return $query->selectRaw("
            count(if(status = 'open', 1, null)) as total_open,
            count(if(status = 'close', 1, null)) as total_close,
            count(status) as total
        ");
    }

    public function scopeOpenStatus($query)
    {
        return $query->where('status', 'open');
    }

    public function scopeCloseStatus($query)
    {
        return $query->where('status', 'close');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
