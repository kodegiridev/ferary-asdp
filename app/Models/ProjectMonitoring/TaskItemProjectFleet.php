<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TaskItemProjectFleet extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'task_item_project_fleets';

    protected $fillable = [
        'sub_item_id',
        'task',
        'bobot',
        'plan',
        'progres'
    ];

    protected $moduleName = 'Task Item Project Fleet';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
