<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PeriodeVisualQualityPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'periode_visual_quality_port';

    protected $fillable = [
        'sub_visual_quality_port_id',
        'periode',
        'status_progres',
        'document_drawing',
        'outstanding'
    ];

    protected $moduleName = 'Periode Visual Quality Port';

    public static $outstandings = [
        'YES' => 1,
        'NO' => 0,
    ];

    public function sub_visual_quality()
    {
        return $this->belongsTo(SubVisualQualityPort::class, 'sub_visual_quality_port_id', 'id');
    }

    public function steel_groups()
    {
        return $this->hasMany(VQPSteelGroup::class, 'periode_visual_quality_port_id', 'id');
    }

    public function concrate_groups()
    {
        return $this->hasMany(VQPConcrateGroup::class, 'periode_visual_quality_port_id', 'id');
    }

    public function facility_groups()
    {
        return $this->hasMany(VQPFacilityGroup::class, 'periode_visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
