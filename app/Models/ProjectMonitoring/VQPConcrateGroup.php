<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VQPConcrateGroup extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqp_concrate_group';

    protected $fillable = [
        'visual_quality_port_id',
        'periode_visual_quality_port_id',
        'sub_grup',
        'kode',
        'item',
        'evidence',
        'jenis_data',
    ];

    protected $jenis_data = [
        12 => 'Material Ident.',
        13 => 'DMF',
        14 => 'Uji Tarik',
        15 => 'Slump Test',
        16 => 'Uji Tekan',
        17 => 'Surface Cond.',
        18 => 'Material Ident.',
        19 => 'DMF',
        20 => 'Uji Tarik',
        21 => 'Slump Test',
        22 => 'Uji Tekan'
    ];

    protected $moduleName = 'Visual Quality Port Concrate Group';

    public function remarks()
    {
        return $this->hasMany(VQPConcrateGroupRemark::class, 'vqp_concrate_group_id', 'id');
    }

    public function period()
    {
        return $this->belongsTo(PeriodeVisualQualityPort::class, 'periode_visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
