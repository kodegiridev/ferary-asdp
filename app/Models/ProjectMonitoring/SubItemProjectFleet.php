<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SubItemProjectFleet extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'sub_item_project_fleets';

    protected $fillable = [
        'main_item_id',
        'sub_item'
    ];

    protected $moduleName = 'Sub Item Project Fleet';

    public function task_item()
    {
        return $this->hasMany(TaskItemProjectFleet::class, 'sub_item_id', 'id');
    }

    public function main_item()
    {
        return $this->belongsTo(MainItemProjectFleet::class, 'main_item_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
