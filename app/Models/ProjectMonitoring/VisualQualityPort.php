<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VisualQualityPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'visual_quality_port';

    protected $fillable = [
        'project_id',
        'fasilitas',
        'bobot'
    ];

    protected $moduleName = 'Visual Quality Port';

    public function sub_visual_qualities()
    {
        return $this->hasMany(SubVisualQualityPort::class, 'visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
