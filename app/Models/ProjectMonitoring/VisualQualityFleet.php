<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VisualQualityFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'visual_quality_fleet';

    protected $fillable = ['project_id', 'status_grup', 'nama_grup', 'jenis_project', 'jenis_form'];

    protected $moduleName = 'Visual Quality Fleet';

    public function periode()
    {
        return $this->hasMany(PeriodeVisualQualityFleet::class, 'visual_quality_fleet_id', 'id');
    }

    public function iar()
    {
        return $this->hasMany(InspectionAcceptenceRateFleet::class, 'visual_quality_fleet_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
