<?php

namespace App\Models\ProjectMonitoring;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectMonitoringPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'project_monitoring_port';

    protected $fillable = [
        'kontraktor',
        'nama_project',
        'nilai_project',
        'jenis_periode',
        'nomor_kontrak',
        'supervisor',
        'pic',
        'status',
        'jenis_project'
    ];

    public static $statusLabel = [
        0 => 'On Progress',
        1 => 'Completed'
    ];

    protected $moduleName = 'Project Monitoring Port';

    public function user()
    {
        return $this->belongsTo(User::class, 'pic');
    }

    public function basic_data()
    {
        return $this->belongsTo(BasicDataProjectPort::class, 'project_id');
    }

    public function latest_basic_data()
    {
        return $this->hasOne(BasicDataProjectPort::class, 'project_id')->latest();
    }

    public function oldest_basic_data()
    {
        return $this->hasOne(BasicDataProjectPort::class, 'project_id')->oldest();
    }

    public function latest_progress()
    {
        return $this->hasOne(ProgresProjectPort::class, 'project_id')->latest();
    }

    public function progress()
    {
        return $this->hasMany(ProgresProjectPort::class, 'project_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }

    public function latest_approval()
    {
        return $this->hasOne(ApprovalProjectMonitoringPort::class, 'project_monitoring_port_id')->latest();
    }

    public function approvals()
    {
        return $this->hasMany(ApprovalProjectMonitoringPort::class, 'project_monitoring_port_id');
    }
}
