<?php

namespace App\Models\ProjectMonitoring;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ApprovalProjectMonitoringPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'approval_project_monitoring_port';

    protected $fillable = [
        'approval_id',
        'project_monitoring_port_id',
        'progres_project_port_id',
        'judul_dokumen',
        'catatan_project',
        'type',
        'status'
    ];

    protected $moduleName = 'Approval Project Monitoring Port';

    const APPROVAL_SUMMARY      = 1;
    const APPROVAL_COMPLETION   = 2;
    const STATUS_INACTIVE       = 0;
    const STATUS_ACTIVE         = 1;
    const STATUS_APPROVED       = 2;

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id');
    }

    public function project()
    {
        return $this->belongsTo(ProjectMonitoringPort::class, 'project_monitoring_port_id');
    }

    public function scopeOnProject($query, $projectId)
    {
        return $query->where('project_monitoring_port_id', $projectId);
    }

    public function scopeOnProgress($query, $progressId)
    {
        return $query->where('progres_project_port_id', $progressId);
    }

    public function scopeIsActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('approval_id, project_monitoring_port.nama_project AS item')
            ->join('project_monitoring_port', 'project_monitoring_port.id', 'approval_project_monitoring_port.project_monitoring_port_id')
            ->whereNotNull('approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
