<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MilestoneQualityFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'milestone_quality_fleet';

    protected $fillable = [
        'project_id',
        'tipe_milestone',
        'item_inspeksi',
        'task_list',
        'progres',
        'evidence',
        'remarks',
        'created_at',
        'updated_at',
    ];

    public static $types = [
        1 => 'Kick Off Meeting',
        2 => 'First Steel Culting',
        3 => 'Keel Laying',
        4 => 'Engine Loading',
        5 => 'Launching',
        6 => 'Sea Trial',
        7 => 'Delivery',
    ];

    protected $moduleName = 'Milestone Quality Fleet';

    public function scopeAverageProgress($query)
    {
        return $query->selectRaw('
                AVG(if(tipe_milestone = 1, progres, 0)) AS kick_off_meeting,
                AVG(if(tipe_milestone = 2, progres, 0)) AS first_steel_culting,
                AVG(if(tipe_milestone = 4, progres, 0)) AS keel_laying,
                AVG(if(tipe_milestone = 5, progres, 0)) AS engine_loading,
                AVG(if(tipe_milestone = 6, progres, 0)) AS launching,
                AVG(if(tipe_milestone = 3, progres, 0)) AS sea_trial,
                AVG(if(tipe_milestone = 7, progres, 0)) AS delivery
            ');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
