<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class JenisDokumenFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'jenis_dokumen_ipm_fleet';

    protected $fillable = [
        'project_id',
        'jenis_dokumen',
    ];

    protected $moduleName = 'Jenis Dokumen Fleet';

    public function project()
    {
        return $this->belongsTo(ProjectMonitoringFleet::class, 'project_id', 'id');
    }

    public function sub_dokumen_fleets()
    {
        return $this->hasMany(SubDokumenFleet::class, 'jenis_dokumen_id', 'id');
    }

    public function scopeWhereDoc($query, $nama)
    {
        return $query->where('jenis_dokumen', $nama);
    }

    public function scopeWhereProject($query, $projectId)
    {
        return $query->where('project_id', $projectId);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
