<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class DrawingVisualQuality extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'drawing_visual_quality';

    protected $fillable = [
        'project_id',
        'drawing',
        'deskripsi',
        'tanggal',
        'jenis_drawing'
    ];

    protected $moduleName = 'Drawing Visual Quality';

    public function scopeLastDate($query)
    {
        return $query->orderBy('tanggal', 'desc');
    }

    public function scopeOnProject($query, $id)
    {
        return $query->where('project_id', $id);
    }

    public function scopeIsPort($query)
    {
        return $query->where('jenis_drawing', 2);
    }

    public function scopeIsFleet($query)
    {
        return $query->where('jenis_drawing', 1);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
