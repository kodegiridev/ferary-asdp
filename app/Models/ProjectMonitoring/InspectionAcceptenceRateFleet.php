<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class InspectionAcceptenceRateFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'inspection_acceptence_rate_fleet';

    protected $fillable = [
        'visual_quality_fleet_id',
        'periode',
        'jml_undangan',
        'resinpect',
        'acc_class',
        'acc_owner',
        'acc_both',
        'evidence'
    ];

    protected $moduleName = 'Inspection Acceptance Rate Fleet';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
