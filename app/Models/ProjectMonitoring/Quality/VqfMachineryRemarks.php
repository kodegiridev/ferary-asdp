<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfMachineryRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_machinery_remarks';

    protected $fillable = ['vqf_machinery_id', 'nama_staging', 'status', 'catatan'];

    protected $moduleName = 'Visual Quality Fleet Machinery Remarks';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
