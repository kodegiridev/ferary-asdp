<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfNcEquipment extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'vqf_nc_equipment';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'komponen', 'evidence', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet NC Equipment';

    public function remarks()
    {
        return $this->hasMany(VqfNcEquipmentRemarks::class, 'vqf_nc_equipment_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
