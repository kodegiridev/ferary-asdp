<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfHullConstructionRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_hull_construction_remarks';

    protected $fillable = ['vqf_hull_construction_id', 'remarks', 'status_plan', 'status_inspeksi', 'status_state', 'catatan_inspeksi', 'tanggal'];

    protected $moduleName = 'Visual Quality Fleet Hull Construction Remarks';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
