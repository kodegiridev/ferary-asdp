<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfHcRepairWorkRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $moduleName = 'Visual Quality Fleet HC Repair Work Remarks';

    protected $fillable = ['vqf_hc_repair_work_id', 'remarks', 'status_plan', 'status_inspeksi', 'status_state', 'catatan_inspeksi', 'tanggal'];


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
