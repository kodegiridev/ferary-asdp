<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfFacility extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_facility';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'deck', 'fasilitas', 'sub_fasilitas', 'jenis_proyek', 'evidence'];

    protected $moduleName = 'Visual Quality Fleet Facility';

    public function remarks()
    {
        return $this->hasMany(VqfFacilityRemarks::class, 'vqf_facility_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
