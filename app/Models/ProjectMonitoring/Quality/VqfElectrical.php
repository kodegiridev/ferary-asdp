<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfElectrical extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_electrical';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'komponen', 'evidence', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet Electrical';

    public function remarks()
    {
        return $this->hasMany(VqfElectricalRemarks::class, 'vqf_electrical_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
