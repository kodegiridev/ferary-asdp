<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfPropultion extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_propultion';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'peralatan', 'status_state', 'status_plan', 'status_inspeksi', 'evidence', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet Propultion';

    public function remarks()
    {
        return $this->hasMany(VqfPropultionRemarks::class, 'vqf_propultion_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
