<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfHcProductionWork extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'blok', 'status_plan', 'status_inspeksi', 'evidence', 'status_scantiling_check', 'catatan_scantiling_check', 'status_deformation', 'catatan_deformation', 'status_alignment_check', 'catatan_alignment_check', 'jenis_data', 'jenis_proyek', 'status_ident_material', 'catatan_ident_material'];

    protected $table = 'vqf_hc_production_work';

    protected $moduleName = 'Visual Quality Fleet HC Production Work';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
