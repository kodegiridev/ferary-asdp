<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfHullConstruction extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_hull_construction';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'grup', 'kodefikasi', 'evidence', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet Hull Construction';

    public function remarks()
    {
        return $this->hasMany(VqfHullConstructionRemarks::class, 'vqf_hull_construction_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
