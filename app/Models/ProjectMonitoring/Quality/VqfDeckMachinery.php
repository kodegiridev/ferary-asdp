<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfDeckMachinery extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_deck_machinery';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'komponen', 'evidence', 'status_plan', 'status_inspeksi', 'jenis_proyek', 'status_state'];

    protected $moduleName = 'Visual Quality Fleet Deck Machinery';

    public function remarks()
    {
        return $this->hasMany(VqfDeckMachineryRemarks::class, 'vqf_deck_machinery_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
