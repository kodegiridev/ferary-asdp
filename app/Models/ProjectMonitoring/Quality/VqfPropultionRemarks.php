<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfPropultionRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $moduleName = 'Visual Quality Fleet Propultion Remarks';
    
    protected $fillable = ['vqf_propultion_id', 'nama_staging', 'status', 'catatan'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
