<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfTank extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_tank';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'tank', 'metode_leak_test', 'status_plan', 'status_inspeksi', 'evidence', 'remarks', 'jenis_data', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet Tank';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
