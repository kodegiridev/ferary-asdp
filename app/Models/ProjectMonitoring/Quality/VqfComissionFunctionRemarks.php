<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfComissionFunctionRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_comission_function_remarks';

    protected $fillable = ['vqf_comission_function_id', 'nama_staging', 'status', 'catatan'];

    protected $moduleName = 'Visual Quality Fleet Comission Function Remarks';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
