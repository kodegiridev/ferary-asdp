<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfHcRepairWork extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_hc_repair_work';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'grup', 'kodefikasi', 'evidence', 'jenis_data', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet HC Repair Work';

    public function remarks()
    {
        return $this->hasMany(VqfHcRepairWorkRemarks::class, 'vqf_hc_repair_work_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
