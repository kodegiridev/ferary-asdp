<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfPropultionNbRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_propultion_nb_remarks';

    protected $fillable = ['vqf_propultion_id', 'nama_staging', 'status_plan', 'status_inspeksi', 'catatan_inspeksi', 'tanggal'];

    protected $moduleName = 'Visual Quality Fleet Propultion NB Remarks';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
