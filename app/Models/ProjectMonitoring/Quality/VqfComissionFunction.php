<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfComissionFunction extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_comission_function';

    protected $fillable = ['visual_quality_fleet_id', 'periode_visual_quality_id', 'kode', 'cnft', 'tanggal', 'evidence', 'notice', 'status_plan','status_inspeksi', 'jenis_proyek'];

    protected $moduleName = 'Visual Quality Fleet Comission Function';

    public function remarks()
    {
        return $this->hasMany(VqfComissionFunctionRemarks::class, 'vqf_comission_function_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
