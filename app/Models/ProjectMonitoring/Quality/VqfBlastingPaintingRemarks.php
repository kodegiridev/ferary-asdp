<?php

namespace App\Models\ProjectMonitoring\Quality;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VqfBlastingPaintingRemarks extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqf_blasting_painting_remarks';

    protected $fillable = ['vqf_blasting_painting_id', 'remarks', 'tanggal', 'status_plan', 'status_inspeksi', 'catatan_inspeksi'];

    protected $moduleName = 'Visual Quality Fleet Blasting Painting';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
