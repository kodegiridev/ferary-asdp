<?php

namespace App\Models\ProjectMonitoring;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ApprovalProjectMonitoringFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'approval_project_monitoring_fleet';

    protected $fillable = [
        'approval_id',
        'project_monitoring_fleet_id',
        'progres_project_fleet_id',
        'judul_dokumen',
        'catatan_project',
        'type',
        'status'
    ];

    protected $moduleName = 'Approval Project Monitoring Fleet';

    const APPROVAL_SUMMARY      = 1;
    const APPROVAL_COMPLETION   = 2;
    const STATUS_INACTIVE       = 0;
    const STATUS_ACTIVE         = 1;
    const STATUS_APPROVED       = 2;

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id');
    }

    public function project()
    {
        return $this->belongsTo(ProjectMonitoringFleet::class, 'project_monitoring_fleet_id');
    }

    public function scopeOnProject($query, $projectId)
    {
        return $query->where('project_monitoring_fleet_id', $projectId);
    }

    public function scopeOnProgress($query, $progressId)
    {
        return $query->where('progres_project_fleet_id', $progressId);
    }

    public function scopeIsActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('approval_id, project_monitoring_fleet.nama_project AS item')
            ->join('project_monitoring_fleet', 'project_monitoring_fleet.id', 'approval_project_monitoring_fleet.project_monitoring_fleet_id')
            ->whereNotNull('approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
