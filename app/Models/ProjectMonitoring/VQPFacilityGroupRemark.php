<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VQPFacilityGroupRemark extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqp_facility_group_remarks';

    protected $fillable = [
        'vqp_facility_group_id',
        'remarks',
        'status_plan',
        'status_inspeksi',
        'catatan_inspeksi',
        'tanggal',
    ];

    protected $moduleName = 'Visual Quality Port Facility Group Remark';

    public function group()
    {
        return $this->belongsTo(VQPFacilityGroup::class, 'vqp_facility_group_id');
    }

    public function scopeDefineValue($query)
    {
        return $query->selectRaw('
            vqp_facility_group_id
            ,CASE WHEN status_plan = 3 THEN 100
                WHEN status_plan = 2 THEN 50 
                ELSE 0
            END AS status_plan
            ,CASE WHEN status_inspeksi = 3 THEN 100
                WHEN status_inspeksi = 2 THEN 50 
                ELSE 0
            END AS status_inspeksi
            ,updated_at
        ');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
