<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PeriodeRecomendationFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'data_periode_recomendation_fleet';

    protected $fillable = [
        'project_id',
        'periode',
    ];

    protected $moduleName = 'Periode Recommendation Fleet';

    public function scopeWhereProject($query, $projectId)
    {
        return $query->where('project_id', $projectId);
    }

    public function notices()
    {
        return $this->hasMany(NoticeRecomendationFleet::class, 'periode_id', 'id');
    }

    public function close_notices()
    {
        return $this->hasMany(NoticeRecomendationFleet::class, 'periode_id', 'id')
            ->where('status', 'close');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
