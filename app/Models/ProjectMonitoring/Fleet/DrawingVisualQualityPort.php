<?php

namespace App\Models\ProjectMonitoring\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class DrawingVisualQualityPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'drawing_visual_quality_port';

    protected $moduleName = 'Drawing Visual Quality Port';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
