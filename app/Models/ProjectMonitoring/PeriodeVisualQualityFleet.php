<?php

namespace App\Models\ProjectMonitoring;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PeriodeVisualQualityFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $casts = [
        'updated_at' => 'datetime:d-m-Y',
    ];

    protected $table = 'periode_visual_quality_fleet';

    protected $fillable = ['visual_quality_fleet_id', 'periode', 'status_progres', 'document_drawing'];

    protected $appends = ['created_format'];

    protected $moduleName = 'Periode Visual Quality Fleet';

    public function visual_quality()
    {
        return $this->belongsTo(VisualQualityFleet::class, 'visual_quality_fleet_id', 'id');
    }

    public function getCreatedFormatAttribute()
    {
        return Carbon::parse($this->updated_at)->format('Y-m-d h:i:s');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
