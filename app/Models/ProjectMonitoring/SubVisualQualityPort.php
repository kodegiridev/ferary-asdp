<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SubVisualQualityPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'sub_visual_quality_port';

    protected $fillable = [
        'visual_quality_port_id',
        'sub_fasilitas',
        'bobot'
    ];

    protected $moduleName = 'Sub Visual Quality Port';

    public function visual_quality()
    {
        return $this->belongsTo(VisualQualityPort::class, 'visual_quality_port_id', 'id');
    }

    public function periode_visual_qualities()
    {
        return $this->hasMany(PeriodeVisualQualityPort::class, 'sub_visual_quality_port_id', 'id');
    }

    public function inspection_acceptance_rates()
    {
        return $this->hasMany(InspectionAcceptanceRatePort::class, 'sub_visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
