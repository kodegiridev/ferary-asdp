<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MainItemProjectFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['progres_id', 'main_item', 'batas_bobot'];

    protected $moduleName = 'Main Item Project Fleet';

    public function sub_item()
    {
        return $this->hasMany(SubItemProjectFleet::class, 'main_item_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
