<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VQPFacilityGroup extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqp_facility_group';

    protected $fillable = [
        'visual_quality_port_id',
        'periode_visual_quality_port_id',
        'sub_grup',
        'kode',
        'sub_fasilitas',
        'area',
        'evidence',
        'jenis_data',
    ];

    protected $moduleName = 'Visual Quality Port Facility Group';

    public function remarks()
    {
        return $this->hasMany(VQPFacilityGroupRemark::class, 'vqp_facility_group_id', 'id');
    }

    public function period()
    {
        return $this->belongsTo(PeriodeVisualQualityPort::class, 'periode_visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
