<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class SubDokumenFleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'sub_dokumen_ipm_fleet';

    protected $fillable = [
        'jenis_dokumen_id',
        'sub_dokumen',
        'progres',
    ];

    protected $moduleName = 'Sub Dokumen Fleet';

    public function jenis_dokumen()
    {
        return $this->belongsTo(JenisDokumenFleet::class, 'jenis_dokumen_id', 'id');
    }

    public function dokumen_fleets()
    {
        return $this->hasMany(DokumenFleet::class, 'sub_dokumen_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
