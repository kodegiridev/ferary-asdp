<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ProgresProjectPort extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['project_id', 'status_kontrak', 'tgl_update', 'actual_cost', 'remarks', 'plan', 'aktual'];

    protected $appends = ['ppc'];

    protected $moduleName = 'Progress Project Port';

    public function project()
    {
        return $this->belongsTo(ProjectMonitoringPort::class, 'project_id');
    }

    public function getPpcAttribute()
    {
        if ($this->attributes['aktual'] > 0 && $this->attributes['plan'] > 0) {
            return $this->attributes['aktual'] / $this->attributes['plan'];
        }
        return 0;
    }

    public function approval_project()
    {
        return $this->hasOne(ApprovalProjectMonitoringPort::class, 'progres_project_port_id')->latest();
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
