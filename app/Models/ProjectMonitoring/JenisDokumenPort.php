<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class JenisDokumenPort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'jenis_dokumen_ipm_port';

    protected $fillable = [
        'project_id',
        'jenis_dokumen',
    ];

    protected $moduleName = 'Jenis Dokumen Port';

    public function project()
    {
        return $this->belongsTo(ProjectMonitoringPort::class, 'project_id', 'id');
    }

    public function sub_dokumen_ports()
    {
        return $this->hasMany(SubDokumenPort::class, 'jenis_dokumen_id', 'id');
    }

    public function scopeWhereDoc($query, $nama)
    {
        return $query->where('jenis_dokumen', $nama);
    }

    public function scopeWhereProject($query, $projectId)
    {
        return $query->where('project_id', $projectId);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
