<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class BasicDataProjectPort extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['project_id', 'mulai_kontrak', 'selesai_kontrak', 'jangka_waktu', 'status_kontrak'];

    protected $moduleName = 'Basic Data Project Port';

    public function project() 
    {
        return $this->belongsTo(ProjectMonitoringFleet::class, 'project_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
