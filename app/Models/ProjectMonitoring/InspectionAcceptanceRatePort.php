<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class InspectionAcceptanceRatePort extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "inspection_acceptance_rate_port";

    protected $fillable = [
        'visual_quality_port_id',
        'sub_visual_quality_port_id',
        'periode',
        'jml_undangan',
        'reinspect',
        'acc_konsultan',
        'acc_owner',
        'acc_both',
        'evidence'
    ];

    protected $moduleName = 'Inspection Acceptance Rate Port';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
