<?php

namespace App\Models\ProjectMonitoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VQPSteelGroup extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'vqp_steel_group';

    protected $fillable = [
        'visual_quality_port_id',
        'periode_visual_quality_port_id',
        'sub_grup',
        'kode',
        'item',
        'evidence',
        'jenis_data',
    ];

    protected $jenis_data = [
        1 => 'Material Ident.',
        2 => 'Connection Check 2',
        3 => 'Material Ident.',
        4 => 'Connection Check 1',
        5 => 'Galvanized',
        6 => 'Coating',
        7 => 'Connection Check 2',
        8 => 'Material Ident.',
        9 => 'Connection Check 1',
        10 => 'PDR',
        11 => 'PDA',
    ];

    protected $moduleName = 'Visual Quality Port Steel Group';

    public function remarks()
    {
        return $this->hasMany(VQPSteelGroupRemark::class, 'vqp_steel_group_id', 'id');
    }

    public function period()
    {
        return $this->belongsTo(PeriodeVisualQualityPort::class, 'periode_visual_quality_port_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
