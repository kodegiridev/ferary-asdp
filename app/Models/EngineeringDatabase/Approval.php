<?php

namespace App\Models\EngineeringDatabase;

use App\Models\EngineeringDatabase\Dokumen;
use App\Models\EngineeringDatabase\EquipmentBoard;
use App\Models\ERCM\ErcmApproval;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Approval extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'approval';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Approval';

    /**
     * Available approval status.
     * 
     * @var int
     */
    const NEW = 0;
    const REVIEW_BY_MGR = 1;
    const REVIEW_BY_VP = 2;
    const REJECTED = 3;
    const APPROVED = 4;

    public function equipment_boards()
    {
        return $this->hasMany(equipmentBoard::class, 'approval_id', 'id');
    }

    public function equipment_board()
    {
        return $this->hasOne(EquipmentBoard::class);
    }

    public function verifikators()
    {
        return $this->hasMany(Verifikator::class);
    }

    public function pengaju()
    {
        return $this->belongsTo(User::class, 'pengaju_id');
    }

    public function penyetuju()
    {
        return $this->belongsTo(User::class, 'penyetuju_id');
    }

    public function dokumen()
    {
        return $this->hasMany(Verifikator::class, 'approval_id', 'id');
    }

    public function project_monitoring_port()
    {
        return $this->belongsTo(\App\Models\ProjectMonitoring\ApprovalProjectMonitoringPort::class, 'id', 'approval_id');
    }

    public function project_monitoring_fleet()
    {
        return $this->belongsTo(\App\Models\ProjectMonitoring\ApprovalProjectMonitoringFleet::class, 'id', 'approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }

    public function ercm_approval()
    {
        return $this->hasOne(ErcmApproval::class);
    }
}
