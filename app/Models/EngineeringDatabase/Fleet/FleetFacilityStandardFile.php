<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class FleetFacilityStandardFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_fleet_facility_standard_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Fleet Facility Standard File';

    public function fleetFacilityStandard()
    {
        return $this->belongsTo(FleetFacilityStandard::class, 'ed_fleet_facility_standard_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
