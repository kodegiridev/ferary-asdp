<?php

namespace App\Models\EngineeringDatabase\Fleet;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class EngineeringDocument extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_engineering_document';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Engineering Document';

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id', 'id');
    }

    public function dokumen()
    {
        return $this->hasMany(EngineeringDocumentFile::class, 'ed_engineering_document_id', 'id');
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('approval_id, vessel_id AS item')->whereNotNull('approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
