<?php

namespace App\Models\EngineeringDatabase\Fleet;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ManualBook extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_manual_book';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Manual Book';

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id', 'id');
    }

    public function dokumen()
    {
        return $this->hasMany(ManualBookFile::class, 'ed_manual_book_id', 'id');
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('approval_id, vessel_id AS item')->whereNotNull('approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
