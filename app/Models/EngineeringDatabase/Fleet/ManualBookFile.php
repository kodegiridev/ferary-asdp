<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ManualBookFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_manual_book_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Manual Book File';

    public function manualBook()
    {
        return $this->belongsTo(ManualBook::class, 'ed_manual_book_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
