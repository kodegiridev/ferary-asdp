<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class EngineeringDocumentFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_engineering_document_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Engineering Document File';

    public function engineeringDocument()
    {
        return $this->belongsTo(EngineeringDocument::class, 'ed_engineering_document_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
