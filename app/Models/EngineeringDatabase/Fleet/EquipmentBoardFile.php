<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class EquipmentBoardFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_equipment_board_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Equipment Board File';

    public function equipmentBoard()
    {
        return $this->belongsTo(EquipmentBoard::class, 'ed_equipment_board_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
