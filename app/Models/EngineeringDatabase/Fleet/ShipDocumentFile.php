<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class ShipDocumentFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_ship_document_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Ship Document File';

    public function shipDocument()
    {
        return $this->belongsTo(ShipDocument::class, 'ed_ship_document_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
