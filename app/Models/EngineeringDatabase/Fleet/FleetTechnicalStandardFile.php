<?php

namespace App\Models\EngineeringDatabase\Fleet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class FleetTechnicalStandardFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_fleet_technical_standard_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Fleet Technical Standard File';

    public function fleetTechnicalStandard()
    {
        return $this->belongsTo(FleetTechnicalStandard::class, 'ed_fleet_technical_standard_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
