<?php

namespace App\Models\EngineeringDatabase\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PortDrawingFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_port_drawing_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Port Drawing File';

    public function portDrawing()
    {
        return $this->belongsTo(PortDrawing::class, 'port_drawing_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
