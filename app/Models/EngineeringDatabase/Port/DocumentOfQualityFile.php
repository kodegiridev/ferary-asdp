<?php

namespace App\Models\EngineeringDatabase\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class DocumentOfQualityFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_document_of_quality_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Document of Quality File';

    public function documentOfQuality()
    {
        return $this->belongsTo(DocumentOfQuality::class, 'document_of_quality_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
