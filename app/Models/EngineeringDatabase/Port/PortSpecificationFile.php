<?php

namespace App\Models\EngineeringDatabase\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class PortSpecificationFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_port_specification_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Port Specification File';

    public function portDrawing()
    {
        return $this->belongsTo(PortSpecification::class, 'port_specification_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
