<?php

namespace App\Models\EngineeringDatabase\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TechnicalQualityStandardFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_technical_quality_standard_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Technical Quality Standard File';

    public function technicalQualityStandard()
    {
        return $this->belongsTo(TechnicalQualityStandard::class, 'technical_quality_standard_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
