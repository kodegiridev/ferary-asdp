<?php

namespace App\Models\EngineeringDatabase\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class FacilityStandardFile extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_facility_standard_file';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Facility Standard File';

    public function facilityStandard()
    {
        return $this->belongsTo(FacilityStandard::class, 'facility_standard_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
