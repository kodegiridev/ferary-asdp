<?php

namespace App\Models\EngineeringDatabase\Port;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class DocumentOfQuality extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ed_document_of_quality';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Document of Quality';

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id', 'id');
    }

    public function documents()
    {
        return $this->hasMany(DocumentOfQualityFile::class, 'document_of_quality_id', 'id');
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('approval_id, dermaga_id AS item')->whereNotNull('approval_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
