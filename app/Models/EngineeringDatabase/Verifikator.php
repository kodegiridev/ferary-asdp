<?php

namespace App\Models\EngineeringDatabase;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Verifikator extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'verifikator';

    protected $guarded = [
        'id'
    ];

    protected $moduleName = 'Verifikator';

    /**
     * Available approval status.
     * 
     * @var int
     */
    const NEW = 0;
    const REJECTED = 1;
    const APPROVED = 2;

    public function approval()
    {
        return $this->belongsTo(Approval::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_verifikator_id');
    }

    public function scopeWhereUser($query, $id, $opt = '=')
    {
        return $query->where('user_verifikator_id', $opt, $id);
    }

    public function scopeIsNew($query)
    {
        return $query->where('status', Verifikator::NEW);
    }

    public function scopeIsRejected($query)
    {
        return $query->where('status', Verifikator::REJECTED);
    }

    public function scopeIsReviewed($query)
    {
        return $query->where('status', '!=', Verifikator::NEW);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
