<?php

namespace App\Models\Fleet;

use App\Models\MasterRegion\Branch;
use App\Models\MasterRegion\Region;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Fleet extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_vessel';
    protected $fillable = ['nama', 'regional_id', 'cabang_id', 'lintasan_id', 'grt', 'panjang', 'daya', 'satuan', 'usia_teknis', 'status_survey', 'pic', 'os'];

    protected $moduleName = 'Fleet';

    // protected $appends = ['usia_teknis'];

    function regional(){
        return $this->belongsTo(Region::class, 'regional_id', 'id');
    }

    function cabang(){
        return $this->belongsTo(Branch::class, 'cabang_id', 'id');
    }

    function lintasan(){
        return $this->belongsTo(Track::class, 'lintasan_id', 'id');
    }

    function user(){
        return $this->belongsTo(User::class, 'pic', 'id');
    }

    function surveyor(){
        return $this->belongsTo(User::class, 'os', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }

    // public function getUsiaTeknisAttribute(){
    //     return Carbon::parse($this->attributes['usia_teknis'])->age . 'Tahun';
    // }
}
