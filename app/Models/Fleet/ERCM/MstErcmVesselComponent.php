<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselComponent extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_component';

    protected $fillable = [
        'mst_ercm_vessel_group_id',
        'mst_ercm_vessel_subgroup_id',
        'component3d_id',
        'component',
        'color',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_vessel_group()
    {
        return $this->belongsTo(MstErcmVesselGroup::class, 'mst_ercm_vessel_group_id');
    }

    public function mst_ercm_vessel_subgroup()
    {
        return $this->belongsTo(MstErcmVesselSubGroup::class, 'mst_ercm_vessel_subgroup_id');
    }

    public function mst_ercm_vessel_component_standard_parameters()
    {
        return $this->hasMany(MstErcmVesselComponentStandardParameter::class, 'mst_ercm_vessel_component_id');
    }

    public function mst_ercm_vessel_component_condition_criteria()
    {
        return $this->hasOne(MstErcmVesselComponentConditionCriteria::class, 'mst_ercm_vessel_component_id');
    }
}
