<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselSubgroupStandardParameterNumberParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_subgroup_standard_parameter_number_param';

    protected $fillable = [
        'mst_ercm_vessel_subgroup_standard_parameter_id',
        'std',
        'min',
        'max',
        'mid1',
        'mid2',
        'less',
        'valuation_type',
        'created_at',
        'updated_at'
    ];
}
