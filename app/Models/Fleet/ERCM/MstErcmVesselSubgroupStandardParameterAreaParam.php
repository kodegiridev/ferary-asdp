<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselSubgroupStandardParameterAreaParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_subgroup_standard_parameter_area_param';

    protected $fillable = [
        'mst_ercm_vessel_subgroup_standard_parameter_id',
        'std',
        'up1',
        'down1',
        'color1',
        'up2',
        'down2',
        'color2',
        'up3',
        'down3',
        'color3',
        'valuation_type',
        'created_at',
        'updated_at'
    ];
}
