<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselSubGroup extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_subgroup';

    protected $fillable = [
        'mst_ercm_vessel_system_id',
        'mst_ercm_vessel_group_id',
        'name',
        'value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_vessel_components()
    {
        return $this->hasMany(MstErcmVesselComponent::class, 'mst_ercm_vessel_subgroup_id');
    }

    public function mst_ercm_vessel_system()
    {
        return $this->belongsTo(MstErcmVesselSystem::class, 'mst_ercm_vessel_system_id');
    }

    public function mst_ercm_vessel_group()
    {
        return $this->belongsTo(MstErcmVesselGroup::class, 'mst_ercm_vessel_group_id');
    }

    public function mst_ercm_vessel_subgroup_standard_parameter()
    {
        return $this->hasMany(MstErcmVesselSubgroupStandardParameter::class, 'mst_ercm_vessel_subgroup_id');
    }

    public function mst_ercm_vessel_subgroup_condition_criteria()
    {
        return $this->hasOne(MstErcmVesselSubGroupConditionCriteria::class, 'mst_ercm_vessel_subgroup_id');
    }

    public function scopeIsExistsName($query, $name, $vesselId, $exceptId = null)
    {
        return $query->where('name', $name)
            ->whereHas('mst_ercm_vessel_system', function ($query) use ($vesselId) {
                $query->where('vessel_id', $vesselId);
            })
            ->when($exceptId, function ($query) use ($exceptId) {
                $query->where('id', '!=', $exceptId);
            })
            ->exists();
    }
}
