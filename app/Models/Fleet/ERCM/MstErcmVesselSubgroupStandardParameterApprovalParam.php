<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselSubgroupStandardParameterApprovalParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_subgroup_standard_parameter_approval_param';

    protected $fillable = [
        'mst_ercm_vessel_subgroup_standard_parameter_id',
        'param',
        'created_at',
        'updated_at'
    ];
}
