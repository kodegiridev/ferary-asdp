<?php

namespace App\Models\Fleet\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselGroup extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_group';

    protected $fillable = [
        'mst_ercm_vessel_system_id',
        'name',
        'value',
        'attachment',
        'attachment_temp',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_vessel_system()
    {
        return $this->belongsTo(MstErcmVesselSystem::class, 'mst_ercm_vessel_system_id');
    }

    public function mst_ercm_vessel_components()
    {
        return $this->hasMany(MstErcmVesselComponent::class, 'mst_ercm_vessel_group_id');
    }

    public function mst_ercm_vessel_subgroups()
    {
        return $this->hasMany(MstErcmVesselSubGroup::class, 'mst_ercm_vessel_group_id');
    }

    public function scopeIsExistsName($query, $name, $vesselId, $exceptId = null)
    {
        return $query->where('name', $name)
            ->whereHas('mst_ercm_vessel_system', function ($query) use ($vesselId) {
                $query->where('vessel_id', $vesselId);
            })
            ->when($exceptId, function ($query) use ($exceptId) {
                $query->where('id', '!=', $exceptId);
            })
            ->exists();
    }
}
