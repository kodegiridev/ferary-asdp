<?php

namespace App\Models\Fleet\ERCM;

use App\Models\ERCM\Vessel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmVesselSystem extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_vessel_system';

    protected $fillable = [
        'vessel_id',
        'name',
        'value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_vessel_groups()
    {
        return $this->hasMany(MstErcmVesselGroup::class, 'mst_ercm_vessel_system_id');
    }

    public function mst_ercm_vessel_subgroups()
    {
        return $this->hasMany(MstErcmVesselSubGroup::class, 'mst_ercm_vessel_system_id');
    }

    public function scopeIsExistsName($query, $name, $vesselId, $exceptId = null)
    {
        return $query->where('name', $name)
            ->where('vessel_id', $vesselId)
            ->when($exceptId, function ($query) use ($exceptId) {
                $query->where('id', '!=', $exceptId);
            })
            ->exists();
    }
}
