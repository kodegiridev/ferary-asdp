<?php

namespace App\Models\Fleet;

use App\Models\Port\Port;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Track extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_lintasan';
    protected $fillable = ['pelabuhan_id', 'rute_id', 'tipe_lintasan_id', 'nama'];

    protected $moduleName = 'Track';

    public function pelabuhan() {
        return $this->belongsTo(Port::class, 'pelabuhan_id', 'id');
    }

    public function rute() {
        return $this->belongsTo(Route::class, 'rute_id', 'id');
    }

    public function tipe_lintasan() {
        return $this->belongsTo(TypeOfTrack::class, 'tipe_lintasan_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
