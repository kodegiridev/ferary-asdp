<?php

namespace App\Models\ERCM;

use App\Models\ERCM\ErcmSubGroup;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\ERCM\ErcmComponentStandardParameter;
use App\Models\ERCM\ErcmComponentConditionCriteria;

class ErcmComponent extends Model
{
    use HasFactory;

    protected $table = 'ercm_components';

    protected $fillable = [
        'ercm_subgroups_id',
        'component3d_id',
        'component',
        'color',
        'measurement',
        'remarks',
        'classification',
        'measurement_score',
        'measurement_status',
        'repair_status',
        'postpone_status',
        'approval_status',
        'stage_type',
        'created_by',
        'updated_by'
    ];

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_subgroup(){
        return $this->belongsTo(ErcmSubGroup::class, 'ercm_subgroup_id', 'id');
    }

    public function ercm_standard_parameter()
    {
        return $this->hasMany(ErcmComponentStandardParameter::class, 'ercm_components_id');
    }

    public function ercm_condition_criteria()
    {
        return $this->hasOne(ErcmComponentConditionCriteria::class, 'ercm_components_id');
    }
}
