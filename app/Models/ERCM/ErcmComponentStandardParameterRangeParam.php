<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentStandardParameterRangeParam extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_standard_parameter_range_params';
}
