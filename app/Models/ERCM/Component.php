<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Component extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_component';

    protected $fillable = [
        'ercm_mapping_data_id',
        'component_name',
        'level',
        'key',
        'created_at',
        'updated_at'
    ];

    protected $moduleName = 'ERCM Component';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
