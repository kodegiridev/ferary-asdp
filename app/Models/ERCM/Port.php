<?php

namespace App\Models\ERCM;

use App\Models\Port\Port as MstPort;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ERCMStatusEnum;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Port extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_pelabuhans';

    protected $fillable = [
        'pelabuhan_id',
        'status',
        'updated_by'
    ];

    protected $moduleName = 'ERCM Port';

    protected $casts = [
        'status' => ERCMStatusEnum::class
    ];

    public function port(){
        return $this->belongsTo(MstPort::class, 'pelabuhan_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
