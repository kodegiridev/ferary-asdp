<?php

namespace App\Models\ERCM;

use App\Models\Fleet\Fleet;
use App\Models\Port\Port;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ERCMStatusEnum;

class Ercm extends Model
{
    use HasFactory;

    protected $table = 'base_ercm';

    protected $fillable = [
        'vessel_id',
        'port_id',
        'status'
    ];

    protected $casts = [
        'status' => ERCMStatusEnum::class
    ];

    public function fleet(){
        return $this->belongsTo(Fleet::class, 'vessel_id', 'id');
    }

    public function port(){
        return $this->belongsTo(Port::class, 'port_id', 'id');
    }

    // public function components()
    // {
    //     return $this->hasMany(Component::class, 'ercm_mapping_data_id', 'id');
    // }
}
