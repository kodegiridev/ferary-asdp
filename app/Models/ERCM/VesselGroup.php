<?php

namespace App\Models\ERCM;

use App\Models\ERCM\VesselSystem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VesselGroup extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_vessel_groups';

    protected $fillable = [
        'ercm_vessel_system_id',
        'nama',
        'value',
        'attachment',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'status',
        'created_by',
        'updated_by'
    ];

    protected $moduleName = 'ERCM Vessel Group';

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_system(){
        return $this->belongsTo(VesselSystem::class, 'ercm_vessel_system_id', 'id');
    }

    public function getAttachmentUrlAttribute() {
        return $this->attachment ? url('upload/ercm/fleet/effectiveness-doc/'.$this->attachment) : null;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
