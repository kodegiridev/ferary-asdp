<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MappingData extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_mapping_data';

    protected $fillable = [
        'file',
        'category',
        'created_at',
        'updated_at'
    ];

    protected $moduleName = 'ERCM Mapping Data';

    public function components()
    {
        return $this->hasMany(Component::class, 'ercm_mapping_data_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
