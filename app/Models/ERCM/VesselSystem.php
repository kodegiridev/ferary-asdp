<?php

namespace App\Models\ERCM;

use App\Models\ERCM\VesselPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class VesselSystem extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_vessel_systems';

    protected $fillable = [
        'periode_ercm_id',
        'nama',
        'value',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'status',
        'created_by',
        'updated_by'
    ];

    protected $moduleName = 'ERCM Vessel System';

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_period(){
        return $this->belongsTo(VesselPeriod::class, 'periode_ercm_id', 'id');
    }

    // public function getEffectivenessPercentAttribute() {
    //     return $this->effectiveness.'%';
    // }

    // public function getCostFormatAttribute() {
    //     return 'Rp. '.number_format($this->cost);
    // }

    // public function getFileUrlAttribute() {
    //     return [
    //         'effectiveness' => $this->effectiveness_doc ? url('upload/ercm/fleet/effectiveness-doc/'.$this->effectiveness_doc) : null,
    //         'cost' => $this->cost_doc ? url('upload/ercm/fleet/cost-doc/'.$this->cost_doc) : null
    //     ];
    // }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
