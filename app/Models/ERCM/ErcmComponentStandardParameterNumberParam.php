<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentStandardParameterNumberParam extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_standard_parameter_number_params';
}
