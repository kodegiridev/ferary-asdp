<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\ERCM\ErcmComponentStandardParameterApprovalParam;
use App\Models\ERCM\ErcmComponentStandardParameterAreaParam;
use App\Models\ERCM\ErcmComponentStandardParameterRangeParam;
use App\Models\ERCM\ErcmComponentStandardParameterNumberParam;

class ErcmComponentStandardParameter extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_standard_parameters';

    protected $fillable = [
        "ercm_components_id",
        "name",
        "value",
        "assestment_type",
        "order_type",
        "type_stage",
        "parameter_type",
        "created_by",
        "updated_by",
        "measurement"
    ];

    public static $parameterNames = [
        'kell' => 1,
        'shell' => 2,
        'plate' => 3
    ];

    public static $assestmentTypes = [
        'initial' => '1',
        'before' => '2',
        'after' => '3'
    ];

    public static $orderTypes = [
        'area' => '1',
        'number' => '2',
        'ranges' => '3',
        'approval' => '4'
    ];

    public static $typeStages = [
        'initial' => '1',
        'before' => '2'
    ];

    public static $valuationTypes = [
        'increased' => '1',
        'decreased' => '2',
    ];

    public static $colors = [
        'green' => '1',
        'yellow' => '2',
        'red' => '3',
    ];

    public function standard_parameter_number()
    {
        return $this->hasOne(ErcmComponentStandardParameterNumberParam::class, 'ercm_component_standard_parameters_id');
    }

    public function standard_parameter_areas()
    {
        return $this->hasMany(ErcmComponentStandardParameterAreaParam::class, 'ercm_component_standard_parameters_id');
    }

    public function standard_parameter_ranges()
    {
        return $this->hasMany(ErcmComponentStandardParameterRangeParam::class, 'ercm_component_standard_parameters_id');
    }

    public function standard_parameter_approvals()
    {
        return $this->hasMany(ErcmComponentStandardParameterApprovalParam::class, 'ercm_component_standard_parameters_id');
    }
}
