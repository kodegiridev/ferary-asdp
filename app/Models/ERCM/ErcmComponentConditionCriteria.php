<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentConditionCriteria extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_condition_criterias';

    protected $fillable = [
        "ercm_components_id",
        "up1",
        "down1",
        "status1",
        "up2",
        "down2",
        "status2",
        "up3",
        "down3",
        "status3",
        "created_by",
        "updated_by"
    ];
}
