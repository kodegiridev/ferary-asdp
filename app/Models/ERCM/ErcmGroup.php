<?php

namespace App\Models\ERCM;

use App\Models\ERCM\ErcmSystem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmGroup extends Model
{
    use HasFactory;

    protected $table = 'ercm_groups';

    protected $fillable = [
        'ercm_systems_id',
        'name',
        'value',
        'attachment',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'status',
        'created_by',
        'updated_by'
    ];

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_system(){
        return $this->belongsTo(ErcmSystem::class, 'ercm_system_id', 'id');
    }

    public function ercm_subgroups(){
        return $this->hasMany(ErcmSubGroup::class, 'ercm_group_id');
    }

    public function getAttachmentUrlAttribute() {
        return $this->attachment ? url('upload/ercm/fleet/effectiveness-doc/'.$this->attachment) : null;
    }
}
