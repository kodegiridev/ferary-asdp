<?php

namespace App\Models\ERCM;

use App\Models\EngineeringDatabase\Approval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmApproval extends Model
{
    use HasFactory;

    protected $fillable = [
        'approval_id',
        'ercm_period_id',
        'ercm_component_id'
    ];

    public function approval()
    {
        return $this->belongsTo(Approval::class, 'approval_id');
    }

    public function period()
    {
        return $this->belongsTo(ErcmPeriod::class, 'ercm_period_id');
    }

    public function scopeSelectApproval($query)
    {
        return $query->selectRaw('
                approval_id, 
                CASE 
                    WHEN base_ercm.vessel_id IS NOT NULL THEN mst_vessel.nama
                    ELSE mst_pelabuhan.nama
                END AS item
            ')
            ->join('ercm_periods', 'ercm_periods.id', 'ercm_approvals.ercm_period_id')
            ->join('base_ercm', 'base_ercm.id', 'ercm_periods.ercm_id')
            ->leftJoin('mst_vessel', 'mst_vessel.id', 'base_ercm.vessel_id')
            ->leftJoin('mst_pelabuhan', 'mst_pelabuhan.id', 'base_ercm.port_id');
    }
}
