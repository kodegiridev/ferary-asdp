<?php

namespace App\Models\ERCM;

use App\Models\ERCM\ErcmGroup;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmSubGroup extends Model
{
    use HasFactory;

    protected $table = 'ercm_subgroups';

    protected $fillable = [
        'ercm_groups_id',
        'name',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'status',
        'created_by',
        'updated_by'
    ];

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_group(){
        return $this->belongsTo(ErcmGroup::class, 'ercm_group_id', 'id');
    }

    public function ercm_components()
    {
        return $this->hasMany(ErcmComponent::class, 'ercm_subgroups_id');
    }
}
