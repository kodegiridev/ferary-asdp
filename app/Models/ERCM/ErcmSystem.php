<?php

namespace App\Models\ERCM;

use App\Models\ERCM\ErcmPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ERCM\ErcmGroup;

class ErcmSystem extends Model
{
    use HasFactory;

    protected $table = 'ercm_systems';

    protected $fillable = [
        'periode_ercm_id',
        'name',
        'value',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'status',
        'created_by',
        'updated_by'
    ];

    // protected $casts = [
    //     'period_status' => ERCMPeriodStatusEnum::class
    // ];

    public function ercm_period(){
        return $this->belongsTo(ErcmPeriod::class, 'periode_ercm_id', 'id');
    }

    public function ercm_groups(){
        return $this->hasMany(ErcmGroup::class, 'ercm_systems_id');
    }

    // public function getEffectivenessPercentAttribute() {
    //     return $this->effectiveness.'%';
    // }

    // public function getCostFormatAttribute() {
    //     return 'Rp. '.number_format($this->cost);
    // }

    // public function getFileUrlAttribute() {
    //     return [
    //         'effectiveness' => $this->effectiveness_doc ? url('upload/ercm/fleet/effectiveness-doc/'.$this->effectiveness_doc) : null,
    //         'cost' => $this->cost_doc ? url('upload/ercm/fleet/cost-doc/'.$this->cost_doc) : null
    //     ];
    // }
}
