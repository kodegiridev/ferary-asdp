<?php

namespace App\Models\ERCM;

use App\Models\ERCM\Ercm;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ERCMPeriodStatusEnum;

class ErcmPeriod extends Model
{
    use HasFactory;

    protected $table = 'ercm_periods';

    protected $fillable = [
        'ercm_id',
        'period',
        'contractor',
        'effectiveness',
        'effectiveness_doc',
        'cost',
        'cost_doc',
        'before_score',
        'after_score',
        'delta_score',
        'repair_ticket_execute',
        'repair_ticket_accept',
        'repair_ticket_reject',
        'repair_ticket_postpone',
        'period_status',
        'approval_status',
        'created_by',
        'updated_by'
    ];

    protected $casts = [
        'period_status' => ERCMPeriodStatusEnum::class
    ];

    public function ercm(){
        return $this->belongsTo(Ercm::class, 'ercm_id', 'id');
    }

    public function getEffectivenessPercentAttribute() {
        return $this->effectiveness.'%';
    }

    public function getCostFormatAttribute() {
        return 'Rp. '.number_format($this->cost);
    }

    public function getFileUrlAttribute($ercm_type) {
        $path = 'upload/'.$ercm_type.'/';
        return [
            'effectiveness' => $this->effectiveness_doc ? url($path.'effectiveness-doc/'.$this->effectiveness_doc) : null,
            'cost' => $this->cost_doc ? url($path.'cost-doc/'.$this->cost_doc) : null
        ];
    }

    public function approval()
    {
        return $this->hasOne(ErcmApproval::class);
    }
}
