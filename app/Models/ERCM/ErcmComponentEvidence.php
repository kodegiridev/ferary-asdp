<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentEvidence extends Model
{
    use HasFactory;
}
