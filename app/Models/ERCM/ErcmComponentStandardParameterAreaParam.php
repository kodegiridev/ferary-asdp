<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentStandardParameterAreaParam extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_standard_parameter_area_params';
}
