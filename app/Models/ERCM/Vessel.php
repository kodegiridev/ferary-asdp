<?php

namespace App\Models\ERCM;

use App\Models\Fleet\Fleet;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\ERCMStatusEnum;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Vessel extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ercm_vessels';

    protected $fillable = [
        'vessel_id',
        'status',
        'updated_by',
        // 'created_at',
        // 'updated_at'
    ];

    protected $moduleName = 'ERCM Vessel';

    protected $casts = [
        'status' => ERCMStatusEnum::class
    ];

    public function fleet(){
        return $this->belongsTo(Fleet::class, 'vessel_id', 'id');
    }

    // public function components()
    // {
    //     return $this->hasMany(Component::class, 'ercm_mapping_data_id', 'id');
    // }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
