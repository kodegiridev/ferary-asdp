<?php

namespace App\Models\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErcmComponentStandardParameterApprovalParam extends Model
{
    use HasFactory;

    protected $table = 'ercm_component_standard_parameter_approval_params';
}
