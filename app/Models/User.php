<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use HasFactory, Notifiable, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'role_id',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $moduleName = 'User';

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function scopeAsStaff($query)
    {
        return $query->whereHas('role', function ($q) {
            $q->whereIn('name', Role::$asStaff);
        });
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }

    public function scopeAsApprover($query, $exceptUser = null)
    {
        return $query
            ->when($exceptUser, function ($query) use ($exceptUser) {
                $query->whereNotIn('id', \Illuminate\Support\Arr::wrap($exceptUser));
            })
            ->whereHas('role', function ($query) {
                $query->whereIn('name', Role::$asManager)
                    ->orWhereIn('name', Role::$asVP);
            });
    }

    public function scopeAsVerifikator($query, $exceptUser = null)
    {
        return $query
            ->when($exceptUser, function ($query) use ($exceptUser) {
                $query->whereNotIn('id', \Illuminate\Support\Arr::wrap($exceptUser));
            })
            ->whereHas('role', function ($query) {
                $query->whereIn('name', Role::$asStaff)
                    ->orWhereIn('name', Role::$asManager)
                    ->orWhereIn('name', Role::$asVP);
            });
    }
}
