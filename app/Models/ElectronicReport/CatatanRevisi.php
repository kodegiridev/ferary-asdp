<?php

namespace App\Models\ElectronicReport;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class CatatanRevisi extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ereport_catatan_revisi';

    protected $moduleName = 'E-Report Revision Note';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
