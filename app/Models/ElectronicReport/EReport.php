<?php

namespace App\Models\ElectronicReport;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class EReport extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'e_report';

    protected $fillable = ['judul_penugasan', 'keterangan_penugasan', 'dokumen_penugasan', 'target_waktu', 'target_pencapaian', 'dok_hasil_penugasan', 'pembuat_id', 'penerima1_id', 'penerima2_id', 'status_penugasan', 'status_progres', 'status_penyelesaian', 'keterangan_penolakan_vp', 'file_penolakan_vp', 'tgl_penyetujuan'];

    protected $appends = ['disposisi', 'verify', 'doc_penugasan_filename'];

    public static $statusPenugasan = [
        1 => 'Draft',
        2 => 'Ditolak',
        3 => 'Selesai'
    ];

    public static $statusPenyelesaian = [
        1 => 'Early',
        2 => 'On Time',
        3 => 'Overdue'
    ];

    public static $statusProgres = [
        1 => 'Waiting Approval',
        2 => 'Rejected',
        3 => 'Finish',
        4 => 'Waiting Approval VP (Disposition)'
    ];

    protected $moduleName = 'E-Report';

    public function pembuat()
    {
        return $this->belongsTo(User::class, 'pembuat_id', 'id');
    }

    public function penerima1()
    {
        return $this->belongsTo(User::class, 'penerima1_id', 'id');
    }

    public function penerima2()
    {
        return $this->belongsTo(User::class, 'penerima2_id', 'id');
    }

    public function tanggapan()
    {
        return $this->hasMany(TanggapanPenugasan::class, 'ereport_id', 'id')->latest();
    }

    public function revisi()
    {
        return $this->hasMany(CatatanRevisi::class, 'ereport_id', 'id')->latest();
    }

    public function getDisposisiAttribute()
    {
        if ($this->penerima2_id) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getVerifyAttribute()
    {
        return TanggapanPenugasan::where('ereport_id', $this->id)->count();
    }

    public function getDocPenugasanFilenameAttribute()
    {
        $file = $this->dokumen_penugasan;
        if ($file) {
            $filename = explode('/', $file);
            $file = end($filename);
        }

        return $file;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
