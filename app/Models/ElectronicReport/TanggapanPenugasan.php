<?php

namespace App\Models\ElectronicReport;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class TanggapanPenugasan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'ereport_tanggapan_penugasan';
    protected $fillable = ['ereport_id', 'pembuat_id', 'keterangan', 'status'];

    protected $moduleName = 'E-Report Assignment Response';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
