<?php

namespace App\Models\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Dock extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_dermaga';
    protected $guarded = [
        'id'
    ];
    protected $fillable = ['pelabuhan_id', 'tipe', 'nama', 'kapasitas'];
    public $timestamps = false;

    protected $moduleName = 'Dock';

    public function pelabuhan()
    {
        return $this->belongsTo(Port::class, 'pelabuhan_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
