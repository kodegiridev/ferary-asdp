<?php

namespace App\Models\Port;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\MasterRegion\Branch;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Port extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'mst_pelabuhan';
    protected $guarded = [
        'id'
    ];
    protected $fillable = ['cabang_id', 'nama', 'pic'];
    public $timestamps = false;

    protected $moduleName = 'Port';

    public function cabang()
    {
        return $this->belongsTo(Branch::class, 'cabang_id', 'id');
    }

    public function pic_user()
    {
        return $this->belongsTo(User::class, 'pic', 'id');
    }

    public function dock()
    {
        return $this->hasMany(Dock::class, 'pelabuhan_id', 'id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn (string $eventName) => $this->moduleName . ' has been ' . $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
