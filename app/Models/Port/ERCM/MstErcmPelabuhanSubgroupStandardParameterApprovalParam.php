<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSubgroupStandardParameterApprovalParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_subgroup_standard_parameter_approval_param';

    protected $fillable = [
        'mst_ercm_pelabuhan_subgroup_standard_parameter_id',
        'param',
        'created_at',
        'updated_at'
    ];
}
