<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanComponentStandardParameter extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_component_standard_parameter';

    protected $fillable = [
        'mst_ercm_pelabuhan_component_id',
        'name',
        'value',
        'assestment_type',
        'order_type',
        'type_stage',
        'parameter_type',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public static $parameterNames = [
        'kell' => 1,
        'shell' => 2,
        'plate' => 3
    ];

    public static $assestmentTypes = [
        'initial' => '1',
        'before' => '2',
        'after' => '3'
    ];

    public static $orderTypes = [
        'area' => '1',
        'number' => '2',
        'ranges' => '3',
        'approval' => '4'
    ];

    public static $typeStages = [
        'initial' => '1',
        'before' => '2'
    ];

    public static $valuationTypes = [
        'increased' => '1',
        'decreased' => '2',
    ];

    public static $colors = [
        'green' => '1',
        'yellow' => '2',
        'red' => '3',
    ];

    public function component_standard_parameter_number()
    {
        return $this->hasOne(MstErcmPelabuhanComponentStandardParameterNumberParam::class, 'mst_ercm_pelabuhan_component_standard_parameter_id');
    }

    public function component_standard_parameter_areas()
    {
        return $this->hasMany(MstErcmPelabuhanComponentStandardParameterAreaParam::class, 'mst_ercm_pelabuhan_component_standard_parameter_id');
    }

    public function component_standard_parameter_ranges()
    {
        return $this->hasMany(MstErcmPelabuhanComponentStandardParameterRangeParam::class, 'mst_ercm_pelabuhan_component_standard_parameter_id');
    }

    public function component_standard_parameter_approvals()
    {
        return $this->hasMany(MstErcmPelabuhanComponentStandardParameterApprovalParam::class, 'mst_ercm_pelabuhan_component_standard_parameter_id');
    }
}
