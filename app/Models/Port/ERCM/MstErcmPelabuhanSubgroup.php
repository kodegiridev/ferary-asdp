<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSubgroup extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_subgroup';

    protected $fillable = [
        'mst_ercm_pelabuhan_system_id',
        'mst_ercm_pelabuhan_group_id',
        'name',
        'value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_pelabuhan_components()
    {
        return $this->hasMany(MstErcmPelabuhanComponent::class, 'mst_ercm_pelabuhan_subgroup_id');
    }

    public function mst_ercm_pelabuhan_system()
    {
        return $this->belongsTo(MstErcmPelabuhanSystem::class, 'mst_ercm_pelabuhan_system_id');
    }

    public function mst_ercm_pelabuhan_group()
    {
        return $this->belongsTo(MstErcmPelabuhanGroup::class, 'mst_ercm_pelabuhan_group_id');
    }

    public function mst_ercm_pelabuhan_subgroup_standard_parameter()
    {
        return $this->hasMany(MstErcmPelabuhanSubgroupStandardParameter::class, 'mst_ercm_pelabuhan_subgroup_id');
    }

    public function mst_ercm_pelabuhan_subgroup_condition_criteria()
    {
        return $this->hasOne(MstErcmPelabuhanSubgroupConditionCriteria::class, 'mst_ercm_pelabuhan_subgroup_id');
    }

    public function scopeIsExistsName($query, $name, $pelabuhanId, $exceptId = null)
    {
        return $query->where('name', $name)
            ->whereHas('mst_ercm_pelabuhan_system', function ($query) use ($pelabuhanId) {
                $query->where('pelabuhan_id', $pelabuhanId);
            })
            ->when($exceptId, function ($query) use ($exceptId) {
                $query->where('id', '!=', $exceptId);
            })
            ->exists();
    }
}
