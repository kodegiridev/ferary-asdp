<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSystem extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_system';

    protected $fillable = [
        'pelabuhan_id',
        'name',
        'value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_pelabuhan_groups()
    {
        return $this->hasMany(MstErcmPelabuhanGroup::class, 'mst_ercm_pelabuhan_system_id');
    }

    public function mst_ercm_pelabuhan_subgroups()
    {
        return $this->hasMany(MstErcmPelabuhanSubgroup::class, 'mst_ercm_pelabuhan_system_id');
    }

    public function scopeIsExistsName($query, $name, $pelabuhanId, $exceptId = null)
    {
        return $query->where('name', $name)
            ->where('pelabuhan_id', $pelabuhanId)
            ->when($exceptId, function ($query) use ($exceptId) {
                $query->where('id', '!=', $exceptId);
            })
            ->exists();
    }
}
