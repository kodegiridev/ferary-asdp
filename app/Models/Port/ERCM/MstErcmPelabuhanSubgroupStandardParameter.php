<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSubgroupStandardParameter extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_subgroup_standard_parameter';

    protected $fillable = [
        'mst_ercm_pelabuhan_subgroup_id',
        'name',
        'value',
        'assestment_type',
        'order_type',
        'type_stage',
        'parameter_type',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public static $parameterNames = [
        'kell' => 1,
        'shell' => 2,
        'plate' => 3
    ];

    public static $assestmentTypes = [
        'initial' => '1',
        'before' => '2',
        'after' => '3'
    ];

    public static $orderTypes = [
        'area' => '1',
        'number' => '2',
        'ranges' => '3',
        'approval' => '4'
    ];

    public static $typeStages = [
        'initial' => '1',
        'before' => '2'
    ];

    public static $valuationTypes = [
        'increased' => '1',
        'decreased' => '2',
    ];

    public static $colors = [
        'green' => '1',
        'yellow' => '2',
        'red' => '3',
    ];

    public function subgroup_standard_parameter_number()
    {
        return $this->hasOne(MstErcmPelabuhanSubgroupStandardParameterNumberParam::class, 'mst_ercm_pelabuhan_subgroup_standard_parameter_id');
    }

    public function subgroup_standard_parameter_areas()
    {
        return $this->hasMany(MstErcmPelabuhanSubgroupStandardParameterAreaParam::class, 'mst_ercm_pelabuhan_subgroup_standard_parameter_id');
    }

    public function subgroup_standard_parameter_ranges()
    {
        return $this->hasMany(MstErcmPelabuhanSubgroupStandardParameterRangeParam::class, 'mst_ercm_pelabuhan_subgroup_standard_parameter_id');
    }

    public function subgroup_standard_parameter_approvals()
    {
        return $this->hasMany(MstErcmPelabuhanSubgroupStandardParameterApprovalParam::class, 'mst_ercm_pelabuhan_subgroup_standard_parameter_id');
    }
}
