<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSubgroupStandardParameterRangeParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_subgroup_standard_parameter_range_param';

    protected $fillable = [
        'mst_ercm_pelabuhan_subgroup_standard_parameter_id',
        'range',
        'criteria',
        'color',
        'created_at',
        'updated_at'
    ];
}
