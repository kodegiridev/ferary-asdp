<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanComponent extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_component';

    protected $fillable = [
        'mst_ercm_pelabuhan_group_id',
        'mst_ercm_pelabuhan_subgroup_id',
        'component3d_id',
        'component',
        'color',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function mst_ercm_pelabuhan_group()
    {
        return $this->belongsTo(MstErcmPelabuhanGroup::class, 'mst_ercm_pelabuhan_group_id');
    }

    public function mst_ercm_pelabuhan_subgroup()
    {
        return $this->belongsTo(MstErcmPelabuhanSubgroup::class, 'mst_ercm_pelabuhan_subgroup_id');
    }

    public function mst_ercm_pelabuhan_component_standard_parameters()
    {
        return $this->hasMany(MstErcmPelabuhanComponentStandardParameter::class, 'mst_ercm_pelabuhan_component_id');
    }

    public function mst_ercm_pelabuhan_component_condition_criteria()
    {
        return $this->hasOne(MstErcmPelabuhanComponentConditionCriteria::class, 'mst_ercm_pelabuhan_component_id');
    }
}
