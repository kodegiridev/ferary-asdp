<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanSubgroupConditionCriteria extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_subgroup_condition_criteria';

    protected $fillable = [
        'mst_ercm_pelabuhan_subgroup_id',
        'up1',
        'down1',
        'status1',
        'up2',
        'down2',
        'status2',
        'up3',
        'down3',
        'status3',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public static $status = [
        'accepted' => 1,
        'monitoring' => 2,
        'repair' => 3,
    ];
}
