<?php

namespace App\Models\Port\ERCM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MstErcmPelabuhanComponentStandardParameterNumberParam extends Model
{
    use HasFactory;

    protected $table = 'mst_ercm_pelabuhan_component_standard_parameter_number_param';

    protected $fillable = [
        'mst_ercm_pelabuhan_component_standard_parameter_id',
        'std',
        'min',
        'max',
        'mid1',
        'mid2',
        'less',
        'valuation_type',
        'created_at',
        'updated_at'
    ];
}
