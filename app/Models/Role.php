<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'roles';

    protected $guarded = [
        'id'
    ];

    public static $asAdmin = [
        'admin'
    ];

    public static $asStaff = [
        'Staff Mutu Pelabuhan 1',
        'Staff Mutu Pelabuhan 2',
        'Staff Mutu Pelabuhan 3',
        'Staff Mutu Armada 1',
        'Staff Mutu Armada 2',
        'Staff Mutu Armada 3',
        'Staff Asset Produksi 1',
        'Staff Asset Produksi 2',
        'Staff Asset Produksi 3',
    ];

    public static $asManager = [
        'Manager Mutu Armada & Pelabuhan',
        'Manager Asset Produksi',
    ];

    public static $asVP = [
        'Vice President',
    ];

    protected $moduleName = 'Role';

    public function hasPermission()
    {
        return $this->hasMany(RoleHasPermission::class, 'id', 'role_id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->setDescriptionForEvent(fn(string $eventName) => $this->moduleName. ' has been '. $eventName)
            ->logAll()
            ->logOnlyDirty();
    }
}
